/*
 * 	Cluster API header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Added clusterfstab support
 *	Date   :	Sep 3 2002
 *	Authors:     Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CLUSTER_H
#define _CLUSTER_H

#if defined(__cplusplus)
extern "C" {
#endif

#ifdef __KERNEL__
#include <linux/types.h>
#include <linux/time.h>
#else /* !__KERNEL__ */
#include <sys/types.h>
#include <sys/time.h>
#endif /* !__KERNEL__ */

struct sockaddr;
typedef unsigned int clusternode_t;

/*
 * Special clusternode number values.
 */
#define CLUSTERNODE_THIS  (clusternode_t)0      	/* Current node */
#define CLUSTERNODE_BEST  (clusternode_t)0x100000	/* Select "best" node */
#define CLUSTERNODE_INVAL (clusternode_t)0x100001	/* Invalid node */

/* clusternode state valid values */
#define	CLUSTERNODE_NEVERUP	0x1
#define	CLUSTERNODE_COMINGUP	0x2
#define	CLUSTERNODE_UP		0x3
#define	CLUSTERNODE_SHUTDOWN	0x4
#define	CLUSTERNODE_GOINGDOWN	0x5
#define	CLUSTERNODE_KCLEANUP	0x6
#define	CLUSTERNODE_UCLEANUP	0x7
#define	CLUSTERNODE_DOWN	0x8

/* valid action flags for setnodeinfo() */
#define	CLUSTERNODE_SETSTATE	1

/* valid values for why field in cluster_transinfo */
#define	CLUSTER_TRANSWHY_API		0x1
#define	CLUSTER_TRANSWHY_SYSTEM		0x2
#define	CLUSTER_TRANSWHY_UNEXPECTED	0x3

/* valid values for which field in cluster_detailedtransition() */
#define CLUSTER_TRANSWHICH_FIRST	0x1  /* Get first transition. */
#define CLUSTER_TRANSWHICH_NEXT		0x2  /* Get transition after transid */
#define CLUSTER_TRANSWHICH_THIS		0x3  /* Get this transid's transition */
#define CLUSTER_TRANSWHICH_LAST	0x4  /* Get latest transition */
#define CLUSTER_TRANSWHICH_PREVIOUS	0x5  /* Get transition before transid */

typedef unsigned long transid_t;

struct cluster_transinfo {
	transid_t	transid;
	struct timeval	transtime;
	clusternode_t	transnode;
	unsigned long	fromstate;
	unsigned long	tostate;
	int		why;
};

typedef struct cluster_transinfo cluster_transinfo_t;

struct clusternode_info { 
	clusternode_t		node_num;
	unsigned long		node_state;
	unsigned long		node_previous_state;
	int			node_transwhy;
	transid_t		node_lasttransid;
	struct timeval		node_lasttranstime;
	transid_t		node_firsttransid;
	struct timeval		node_firsttranstime;
	int 			node_totalcpus;
	int			node_onlinecpus;
	int 			node_cpupower;
	int 			node_totalmemory;
};

typedef struct clusternode_info clusternode_info_t;

#ifndef __KERNEL__

#define CLUSTERNAME	"/etc/clustername"
#define NODENAME	"/etc/nodename"

struct sockaddr;

#ifdef __STDC__

/* Prototypes for libcluster API's */
extern clusternode_t clusternode_num(void); 
extern char * cluster_name(void);
extern int cluster_ssiconfig(void);
extern int cluster_membership(unsigned long *, int, clusternode_t *);
extern int cluster_transition(unsigned long *, int, cluster_transinfo_t *);
extern int cluster_detailedtransition(int, unsigned long *, int, cluster_transinfo_t *);
extern int clusternode_info(clusternode_t, int, clusternode_info_t *);
extern int clusternode_setinfo(clusternode_t, int, int, clusternode_info_t *);
extern int clusternode_avail(clusternode_t);
extern int cluster_maxnodes(void);
extern int mount_remote_root(char *path);
extern int discover_mounts(int);
extern int cfs_mount(char *, char *, char *, unsigned long , void *, unsigned long);
extern int cfs_remount(char *, int , char *, unsigned long , void *);
extern int set_mdirector(clusternode_t ,  char *);
extern int set_pdirector(clusternode_t ,  char *);
extern int del_cvip(char *);
extern clusternode_t cluster_set_node_context(clusternode_t);
extern int cluster_events_register_signal(int, int);
extern int set_ipvsportweight(u_short , u_short ,int, u_short, char *);
extern int cluster_get_nodename(char *, int);
extern int cluster_get_clustername(char *, int);
extern clusternode_t node_pid(pid_t);
extern clusternode_t cluster_getnodebyname(char *);
extern int clusternode_get_ip(clusternode_t, struct sockaddr *, int *);

/* System calls */
extern int rexecl(const char *file, const char *arg, ...);
extern int rexecle(const char *file, const char *arg, ...);
extern int rexeclp(const char *file, const char *arg, ...);
extern int rexecv(const char *file, char *const *argv, clusternode_t node);
extern int rexecvp(const char *name, char *const *argv, clusternode_t node);
extern int rfork(clusternode_t node);
extern int migrate(clusternode_t node);

/* Cluster-aware fstab calls */
extern int ssi_thisnode_entry(char *);
extern int ssi_getnodes_entry(char *, clusternode_t *);
extern char * ssi_qualify_dev(char *, clusternode_t);
extern clusternode_t ssi_get_dev_node(char *);
extern int lock_fstab_entry(char *, int *);
extern int unlock_fstab_entry(int);

/* expand CDSL */
extern char *ssi_realpath(const char *, char *, int, int);


#else /*!__STDC__*/

/* Prototypes for libcluster API's */
extern clusternode_t clusternode_num(); 
extern char * cluster_name();
extern int cluster_membership();
extern int cluster_transition();
extern int cluster_detailedtransition();
extern int clusternode_info();
extern int clusternode_setinfo();
extern int clusternode_avail();
extern int cluster_maxnodes();
extern int cluster_set_config();
extern int cluster_init_preroot();
extern int cluster_init_postroot();
extern int mount_remote_root();
extern int discover_mounts();
extern int cfs_mount();
extern int cfs_remount();
extern int set_mdirector();
extern int set_pdirector();
extern clusternode_t cluster_set_node_context();
extern int cluster_events_register_signal();
extern int set_ipvsportweight();
extern int cluster_get_nodename();
extern int cluster_get_clustername();
extern int node_pid();
extern clusternode_t cluster_getnodebyname();
extern int clusternode_get_ip();


/* System calls */
extern int rexecl();
extern int rexecle();
extern int rexeclp();
extern int rexecv();
extern int rexecvp();
extern int rfork();
extern int migrate();

/* Cluster-aware fstab calls */
extern int ssi_thisnode_entry();
extern int ssi_getnodes_entry();
extern char * ssi_qualify_dev();
extern clusternode_t ssi_get_dev_node();
extern int lock_fstab_entry();
extern int unlock_fstab_entry();

/* expand CDSL */
extern char *ssi_realpath();

#endif /*__STDC__*/

#endif /* !__KERNEL__ */

#if defined(__cplusplus)
	}
#endif

#endif /* !_CLUSTER_H */
