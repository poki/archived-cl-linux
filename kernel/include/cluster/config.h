/*
 * 	Cluster config parameters.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CLUSTER_CONFIG_H
#define _CLUSTER_CONFIG_H

#include <linux/param.h>
#include <linux/threads.h>

/*
 * Define the maximum clusternode_t value actually supported, i.e. each
 * subsystem must be able to manage/allocate for nodes 1 - NSC_MAX_NODE_VALUE.
 * Should be a runtime value, but that requires more support not yet
 * available to a) dynamically size all relevant data structures and b)
 * make sure root node's value is passed to joining nodes, and
 * supersedes their own.
 * When changing this value, please be aware of the limits specified below.
 * Setting this smaller saves quite a bit of memory.
 */
/* #define NSC_MAX_NODE_VALUE  125 */ /* default */
#define NSC_MAX_NODE_VALUE  15

/*
 * Define the part of the pid_t reserved for clusternode_t values.
 * Needed regardless of VPROC support in order to generate NSC_NODE_T_RANGE_MAX
 */
#define NODESHIFT	16	/* used by VPROC */
//#define NODESHIFT	23	/* reserve 4 million PIDs per node */

/*
 * Derive the maximum valid clusternode_t value, used ONLY for data-type
 * range checking
 * The PID_MAX defined in threads.h provides a limit to the
 * number of nodes that can be operating at one time.
 */
#if defined(VPROC) || defined(CONFIG_CPID)
#define NSC_NODE_T_RANGE_MAX    (PID_MAX_LIMIT >> NODESHIFT)

#if NSC_MAX_NODE_VALUE > NSC_NODE_T_RANGE_MAX
	#error "NSC_MAX_NODE_VALUE is too large to fit into pid number"
#endif
#endif

#define MAXLOCPID       ((1<<NODESHIFT)-1)  /* number at which pids recycle */
#define LAST_DAEMON_PID 300	/* RESERVED_PIDS */

#define CLUSTER_IFCONFIG_SZ	64
#define CLUSTER_CLMSINFO_SZ	256
#define CLUSTER_ICSROUTE_SZ	64

#endif /* !_CLUSTER_CONFIG_H */
