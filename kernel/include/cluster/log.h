/*
 * 	Cluster logging header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CLUSTER_LOG_H
#define _CLUSTER_LOG_H

typedef struct nsc_logent {
        long     msgnum;
        long     a;
        long     b;
        long     c;
        long     d;
        long     e;
        long     f;
        long     g;
} nsc_logent_t;

typedef struct nsc_logcookie {
	int		magic;
	union {
		struct {
			void		**pages;
			int		wrap;
			int		head;
			int		depth;
			spinlock_t	spinlock;
		};
		struct nsc_logcookie	**cookies;
	};
} nsc_logcookie_t;

#define NSC_LOGENT_PER_PAGE	(PAGE_SIZE / sizeof(nsc_logent_t))

#define NSC_LOGCOOKIE_MAGIC	0x6e73636c	/* "nscl" */
#define NSC_LOGSMPCOOKIE_MAGIC	0x6e736373	/* "nscs" */

extern int nsc_log_search_end;

extern nsc_logcookie_t *nsc_log_init(int, int);
extern nsc_logcookie_t *nsc_logsmp_init(int, int);
extern int nsc_log(nsc_logcookie_t *,
		   long, long, long, long, long, long, long, long);
extern void nsc_log_print(nsc_logcookie_t *, const char **);
extern void nsc_log_print_n(nsc_logcookie_t *, const char **, int);
extern void nsc_log_deinit(nsc_logcookie_t *);
extern void nsc_log_search_va(nsc_logcookie_t *, const char **,
			      long, va_list);
extern void nsc_log_search(nsc_logcookie_t *, const char **, long);

#endif /* !_CLUSTER_LOG_H */
