/*
 * 	Cluster system calls header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CLUSTER_SSISYS_H
#define _CLUSTER_SSISYS_H

#include <linux/types.h>
#include <linux/cluster.h>

#ifdef __KERNEL__
#include <linux/time.h>
#include <linux/param.h>
#include <linux/fs.h>
#include <linux/statfs.h>

/*
 * prototypes for external routines
 */
clusternode_t process_execnode(pid_t);
int process_is_alive(pid_t);
#endif /* __KERNEL__ */

#ifndef __KERNEL__
/*
 * General ssisys() interface
 */
int ssisys();
#endif /* !__KERNEL__ */

/*
 * ssisys() syscall options
 * NOTE: Any changes to this list must also be made to ssisys_ops[]
 * NOTE: Valid options must begin with 1
 */
#define SSISYS_BADOP		0	/* invalid option */
#define SSISYS_LDLVL_INIT	1	/* init kernel load-leveler */
#define SSISYS_LDLVL_GETVEC	2	/* get incoming load info */
#define SSISYS_LDLVL_PUTVEC	3	/* send load info */
#define SSISYS_LDLVL_PUTRCMDS	4	/* rexec'able commands info */
#define SSISYS_LDLVL_SETREXEC	5	/* (dis)enable rexec */
#define SSISYS_CMS_CLUSTERID	6	/* Return cluster id */
#define SSISYS_CFS_STATVFS	7	/* Return statvfs info for fsid */
#define SSISYS_NODE_GETNUM	8	/* Return this node number */
#define SSISYS_NODE_TABLE	10	/* Perform table operation on a node */
#define SSISYS_NODE_DOWN	11	/* Declare a node down */
#define SSISYS_RECLAIM_CHILD	12	/* Reclaim children from init */
#define SSISYS_IPC_GETINFO	13	/* Get SYS V IPC information */
#define SSISYS_ICS_TEST		14	/* Run ICS verification test */
#define SSISYS_NODE_PID		15	/* Get node for a process */
#define SSISYS_ISLOCAL		16	/* Determine if process is local */
#define SSISYS_CFS_ISSTACKED	17	/* Is CFS stacked on this fs type */
#define SSISYS_DNET_SYNC	20      /* After a node joins a
					 * cluster, make sure that it learns
					 * about all the other network
					 * interfaces in the cluster.
					 */
#define SSISYS_CFS_WAIT_MODE	23	/* Set CFS wait mode. */
#define SSISYS_CFS_UMOUNT 	25 	/* Unmount with special options. */
#define SSISYS_IGNORE_HALFUP	33	/* Ignore "half up" state for noded */
#define SSISYS_NODE_ROOTDEV	34	/* Return node's root device number. */
#define SSISYS_GET_PRIMARY	35	/* Get primary node for service */
#define SSISYS_GET_SECONDARY 	36	/* Get secondary node for service */
#define SSISYS_GET_ROOTDISK	37	/* Get the rootdisk info */
#define SSISYS_CLUSTERNODE_NUM	38 	/* API - get node num of exec node */
#define SSISYS_CLUSTER_MEMBERSHIP    39	/* API - get nodes in cluster */
#define SSISYS_CLUSTER_DETAILEDTRANS 40	/* API - get transition info */
#define SSISYS_CLUSTERNODE_INFO	     41	/* API - get node info */
#define SSISYS_CLUSTERNODE_SETINFO   42	/* API - set node info */
#define SSISYS_CLUSTERNODE_AVAIL     43	/* API - determine if node is UP */
#define SSISYS_CLUSTER_MAXNODES	44 	/* API - get max node number */
#define SSISYS_SET_MEMPRIO	45 	/* set memory priority */
#define SSISYS_GET_USERS	46 	/* get user numbers */
#define SSISYS_FORCE_ROOT_NODE	47	/* Force another root node. */
#define SSISYS_CVIP_SET		48	/* set CVIP address */
#define SSISYS_CVIP_GET		49	/* get CVIP address */
#define SSISYS_GET_NODE_COUNTS	50	/* get node number counts */
#define SSISYS_GET_TRANSPORT	51	/* get ICS transport type */
#define SSISYS_CLUSTER_SET_CONFIG	52	/* set cluster configuration */
#define SSISYS_CLUSTER_INIT_PREROOT	53	/* intialize cluster
						   preroot code */
#define SSISYS_CLUSTER_INIT_POSTROOT	54	/* initialize cluster
						   postroot code */
#define SSISYS_CLUSTER_INITPROC		55	/* Start init if needed */
#define	SSISYS_MOUNT_REMOTE_ROOT	56
#define	SSISYS_DISCOVER_MOUNTS		57
#define	SSISYS_CFS_MOUNT		58
#define SSISYS_LVS		        59	/*Set/del   lvs  directors*/
#define	SSISYS_CFS_REMOUNT		60
#define	SSISYS_CFS_SETROOT		61
#define	SSISYS_SET_NODE_CONTEXT		62
#define	SSISYS_SET_NODENAME		63
#define SSISYS_SET_IPVSPORTWEIGHT       64	/* Set the  ipvs port weight
						    for this node */
#define	SSISYS_GET_NODENAME		65
#define	SSISYS_SET_CLUSTERNAME		66
#define	SSISYS_GET_CLUSTERNAME		67

typedef int rval_t;

/*
 * ssisys() versioning support
 */
#define	SSISYS_VER1		1		/* initial version */
#define	SSISYS_VER2		2		/* Adding CFS_MOUNT */
#define	SSISYS_VER3		3		/* cluster_mkinitrd changes */
#define	SSISYS_VER4		4		/* Change discovery */
#define SSISYS_CURVER		SSISYS_VER4	/* current version */

typedef struct ssisys_ops {
	long	(*ts_func)(caddr_t, u_int, caddr_t, u_int, rval_t *);
} ssisys_ops_t;


/*
 * general identification structure for user interfaces
 */
struct identifier {
	int		id_cmd: 16;
	int		id_ver: 8;
	int		id_rsv: 8;
};
typedef struct identifier identifier_t;

/*
 * argument structure used for ssisys()
 */
struct ssisys_iovec {
	caddr_t		tio_udatain;
	u_int		tio_udatainlen;
	caddr_t		tio_udataout;
	u_int		tio_udataoutlen;
	identifier_t    tio_id;
};
typedef struct ssisys_iovec ssisys_iovec_t;

#ifdef __KERNEL__

extern long do_ssisys(ssisys_iovec_t *);
extern long do_discover_mounts(const char *, int);

/*
 * IN argument structure used for SSISYS_LDLVL_INIT
 */
struct ts_ldinit_inargs {
	identifier_t	tldi_id;
	pid_t		tldi_pid;
	int		tldi_maxcnt;
};
typedef struct ts_ldinit_inargs ts_ldinit_inargs_t;

/*
 * IN argument structure used for SSISYS_LDLVL_GETVEC
 */
struct ts_ldgetvec_inargs {
	identifier_t	tldgv_id;
	caddr_t		tldgv_waitp;
};
typedef struct ts_ldgetvec_inargs ts_ldgetvec_inargs_t;

/*
 * IN argument structure used for SSISYS_LDLVL_PUTVEC
 */
struct ts_ldputvec_inargs {
	identifier_t	tldpv_id;
	int		tldpv_node;
	u_int		tldpv_ldveclen;
	caddr_t		tldpv_ldvecdata;
};
typedef struct ts_ldputvec_inargs ts_ldputvec_inargs_t;

/*
 * IN argument structure used for SSISYS_LDLVL_PUTRCMDS
 */
struct ts_ldputrcmds_inargs {
	identifier_t	tldpr_id;
	int		tldpr_flag;
	u_int		tldpr_rcmdslen;
	caddr_t		tldpr_rcmdsdata;
};
typedef struct ts_ldputrcmds_inargs ts_ldputrcmds_inargs_t;

/*
 * IN argument structure used for SSISYS_LDLVL_SETREXEC
 */
struct ts_ldsetrexec_inargs {
	identifier_t	tldsr_id;
	int		tldsr_numnodes;
	caddr_t		tldsr_rninfop;
};
typedef struct ts_ldsetrexec_inargs ts_ldsetrexec_inargs_t;

/*
 * IN argument structure used for SSISYS_CFS_STATVFS
 */
struct ts_cfsstatvfs_inargs {
	identifier_t	tcsv_id;
	int		tcsv_server;
	fsid_t		tcsv_fsid;
};
typedef struct ts_cfsstatvfs_inargs ts_cfsstatvfs_inargs_t;

/*
 * IN argument structure used for SSISYS_CFS_ISSTACKED
 */
struct ts_cfsisstacked_inargs {
	identifier_t	tcis_id;
	int		tcis_server;
	int		tcis_fstype;
};
typedef struct ts_cfsisstacked_inargs ts_cfsisstacked_inargs_t;

/*
 * IN argument structure used for SSISYS_NODE_DOWN
 */
struct ts_nodedown_inargs {
	identifier_t	tnd_id;
	int		tnd_node;
	int		tnd_state;
};
typedef struct ts_nodedown_inargs ts_nodedown_inargs_t;

/*
 * IN argument structure used for SSISYS_DECLARE_NODE_DOWN
 */
struct ts_declare_nd_inargs {
	identifier_t	id;
	int		node;
};
typedef struct ts_declare_nd_inargs ts_declare_nd_inargs_t;

#endif /* __KERNEL__ */

/*
 * IN argument structure used for SSISYS_RECLAIM_CHILD
 */
struct ts_reclaim_child_inargs {
	pid_t		trc_pid;
	time_t		trc_start;
};
typedef struct ts_reclaim_child_inargs ts_reclaim_child_inargs_t;

/*
 * Maximum value allowed for trc_numpids.
 *
 * This is an arbitrary value. It's purpose is to protect the kernel
 * from requesting an excessively large alloc.
 */
#define TS_MAX_TRC_NUMPIDS 1024

/*
 * IN argument structure used for SSISYS_NODE_PID
 */
struct ts_nodepid_inargs {
	identifier_t	tnp_id;
	pid_t	tnp_pid;
};
typedef struct ts_nodepid_inargs ts_nodepid_inargs_t;

#ifdef __KERNEL__

/*
 * IN argument structure used for SSISYS_ISLOCAL
 */
struct ts_islocal_inargs {
	identifier_t	til_id;
	pid_t	til_pid;
};
typedef struct ts_islocal_inargs ts_islocal_inargs_t;

/*
 * IN argument structure used for SSISYS_CFS_WAIT_MODE
 */
typedef enum {
	CFS_WAIT_DEFAULT	= 0x0,	/* All calls will wait for down hard
					   mounts. */
	CFS_WAIT_MOUNT		= 0x1,	/* If the last element in a lookup is a
					   mount-point, the vnode returned
					   will be the mounted-on directory;
					   not the root directory of the new
					   filesystem. */
	CFS_WAIT_NOWAIT	 	= 0x2	/* No waits will be done. */
} cfs_wait_mode_t;

struct ts_cfs_wait_mode_inargs {
	identifier_t	cwm_id;
	int		cwm_mode_bits;
};
typedef struct ts_cfs_wait_mode_inargs ts_cfs_wait_mode_inargs_t;

/*
 * IN argument structure used for SSISYS_CFS_UMOUNT
 */
typedef enum {
	CFS_UMOUNT = 0,		/* Normal unmount. */
	CFS_UMOUNT_FORCE,	/* Force unmount. */
	CFS_UMOUNT_REMOUNT	/* Bring filesystem down for remount. */
} cfs_umount_t;

struct ts_cfs_umount_inargs {
	identifier_t	cu_id;
	const char	*cu_path;
	cfs_umount_t 	cu_mode;
};
typedef struct ts_cfs_umount_inargs ts_cfs_umount_inargs_t;

/*
 * IN/OUT structure for SSISYS_IPC_GETINFO
 */
#define NSCIPC_MAX_ENTRIES	512
struct ssi_ipc_info {
	int	ipc_service;		/* Service type */
	int	ipc_index;		/* Index of first entry */
	int	ipc_server;		/* Node of server or 0 */
	int	ipc_entries_desired;	/* Number of entries desired */
	int	ipc_entries_total;	/* [OUT] Total number of entries */
	u_long	ipc_timestamp;		/* [OUT] Timestamp of database */
};
typedef struct ssi_ipc_info ssi_ipc_info_t;



/*
 * Information returned by SSISYS_CMS_GETINFO
 */

typedef struct tnccms_info_entry {
	fsid_t	cmsent_fsid;
	char	cmsent_fsname[FSTYPSZ];
	fsid_t	cmsent_covered_fsid;
	u_int	cmsent_server;
	u_int	cmsent_cms_server;
	u_int	cmsent_flags;
	int	cmsent_vfs_flags;
	u_long	cmsent_bsize;
	dev_t	cmsent_dev;
	char	*cmsent_mount_path;
	char	*cmsent_mnton_path;
} tnccms_info_entry_t;


#define NSCCMS_MAX_PATH		80
#define NSCCMS_MAX_ENTRIES	32

typedef struct tnccms_info {
	int			cms_index;	  /* Index of first entry */
	int			cms_server;	  /* Node of server or 0 */
	int			cms_num_entries;  /* Number of entries */
	int			cms_total_entries;/* Total number of entries */
	u_long			cms_timestamp;	  /* Timestamp of CMS DB */
	tnccms_info_entry_t    *cms_entries;	  /* Array of entries */
} tnccms_info_t;

/*
 * IN argument structure used for SSISYS_GET_PRIMARY
 */
struct ts_get_primary_inargs {
	int 		tgp_service;
};
typedef struct ts_get_primary_inargs ts_get_primary_inargs_t;

#endif /* __KERNEL__ */

#ifndef __KERNEL__

/*
 *  Flag definitions for the cmsent_flags field
 */

#define CMS_EXPORT	0x01	/* This FS is exportable to remote clusters */
#define CMS_REMOTE	0x02	/* This FS is on a remote cluster */
#define CMS_SUBMOUNTS	0x04	/* This (remote) FS will follow submounts */
#define CMS_DYNAMIC	0x10	/* This (remote) FS was set up dynamically */
#define CMS_UNMOUNTED	0x20	/* This FS has been unmounted but not removed */

/*
 * IPC service names
 */

#define NSCIPC_SERVICE_MSG	0	/* Message queue global name service*/
#define NSCIPC_SERVICE_SEM 	1	/* Semaphores global name service */
#define NSCIPC_SERVICE_SHM	2	/* Shared Memory Global name service*/

#endif	/* !__KERNEL__ */


struct cls_transition_args {
	int	which;
	ulong	*transid;
	int	trans_len;
};
typedef struct cls_transition_args cls_transition_args_t;

struct cls_membership_args {
	clusternode_t	*memberarray;
	int		arraylen;
};
typedef struct cls_membership_args cls_membership_args_t;

struct cls_nodeinfo_args {
	clusternode_t	nodenum;
	int		info_len;
};

typedef struct cls_nodeinfo_args cls_nodeinfo_args_t;

struct cls_setinfo_args {
	clusternode_t		node;
	int			action;
	int			info_len;
	clusternode_info_t	*info;

};
typedef struct cls_setinfo_args cls_setinfo_args_t;

/* Mapping of CLMS states to API states */
#define C_API_NEVERUP		CLMS_STATE_NEVERUP
#define C_API_COMINGUP		CLMS_STATE_COMINGUP
#define C_API_UP		CLMS_STATE_UP
#define C_API_SHUTDOWN		CLMS_STATE_SHUTDOWN
#define C_API_GOINGDOWN		CLMS_STATE_GOINGDOWN
#define C_API_KCLEANUP		CLMS_STATE_KCLEANUP
#define C_API_UCLEANUP		CLMS_STATE_UCLEANUP
#define C_API_DOWN		CLMS_STATE_DOWN

/* input flags for SSISYS_SET_MEMPRIO */
#define	NSC_MEMPRIO_SETCRIT	0x0001
#define	NSC_MEMPRIO_CLRCRIT	0x0002

struct cluster_users {
	int	cur;
	int	max;
};

struct cluster_nodes {
	int	cur;
	int	max;
};

typedef struct cfs_mount_args {
	char *dev_name;
	char *dir_name;
	char *type;
	unsigned long flags;
	void *data;
	unsigned long cfs_flags;
} cfs_mount_args_t;

typedef struct cfs_remount_args {
	char *dev_name;
	int ssidev;
	char *type;
	unsigned long flags;
	void *data;
} cfs_remount_args_t;

typedef struct cfs_setroot_args {
	char *type;
	char *dev_name;
} cfs_setroot_args_t;

/* Possible cfs_flags */
#define CFS_CHARD       1

#endif /* !_CLUSTER_SSISYS_H */
