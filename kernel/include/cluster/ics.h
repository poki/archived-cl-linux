/*
 * 	ICS macro and structure definitions.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *  	Ported to Alpha by Aneesh Kumar K.V(aneesh.kumar@digital.com)
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#ifndef _CLUSTER_ICS_H
#define _CLUSTER_ICS_H

#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <linux/types.h>

/*
 * Basic types used by ICS.
 */
#ifndef _INT32
#define _INT32
typedef s32 int32;
#endif


#ifndef _INT64
#define _INT64
typedef s64 int64;
#endif

#define INT32MASK  3
#define INT32SHIFT 2
#define INT64MASK  7
#define INT64SHIFT 3

typedef struct {
	char icsinfo_string[28];
} icsinfo_t;

#ifdef __KERNEL__

/*
 * Interface to ICS Channels.
 */
typedef int ics_chan_t;

/* Maximum number of channels. */
#define ICS_MAX_CHANNELS	13
extern int ics_num_channels;

extern int ics_clms_chan;

/* Priority Channels */
extern int ics_min_prio_chan;
extern int ics_max_prio_chan;
/* Reply Channels (lower-level Channels) */
extern int ics_reply_chan;
extern int ics_reply_prio_chan;


/*
 * Interface to NSC Cluster Services.
 */
typedef int cluster_svc_t;

/* Maximum number of services per ICS channel */
#define	ICS_MAX_SUBSERVICES	6

extern int cluster_num_svcs;

/*
 * For service definition, a NSC service is (icschannel,subservice).
 * This pair is mapped as 32-bit value using the following macros:
 */
#define	ICS_NSC_SVC(_chan,_subservice)	((_chan << 16) + _subservice)
#define	ICS_CHAN(_nsc_svc)		((_nsc_svc) >> 16)
#define	ICS_SSVC(_nsc_svc)		((_nsc_svc) & ((1 << 16) - 1))


/* Routines for setting and getting priority */
#define ics_getpriority() (current->icsprio)
#define ics_setpriority(_ics_prio) do { current->icsprio = _ics_prio; } while(0)

/*
 * Macros to bracket an RPC call to force its priority up.
 *
 * NOTE: This uses hidden un-matched brackets, so should be
 *	 used carefully).
 */
#define	ICS_SETPRIO_UP(new_prio)			\
	{						\
		int _old_prio_ = ics_getpriority();	\
		if (_old_prio_ < (new_prio))		\
			ics_setpriority((new_prio));

#define	ICS_RESTORE_PRIO()				\
		ics_setpriority(_old_prio_);		\
	}

#endif /* __KERNEL__ */

/*
 * Default number of OOL data segments in an ICS message (either a client
 * send or a server response) that have callback routines associated with
 * them.
 */

#define ICS_DEF_OOL_SEGMENTS	       	4
#define ICS_OOL_SEGMENTS_CHUNKS		16

/*
 * Maximum number of bytes in an out-of-line ICS data segment (either a
 * client send or a server response).
 */

#define ICS_MAX_OOL_DATA_SIZE		0x10000

/*
 * Maximum number of bytes in an in-line ICS message (either a client send
 * or a server response). If more bytes than this need to be sent, then
 * OOL data should be used.
 */
#define ICS_MAX_INLINE_DATA_SIZE	300

/*
 * Number of ICS priority levels supported by ICS server management code
 * (this count includes zero as the normal priority level).
 *
 * Each priority level above zero is treated as a service and has a service
 * number, so changing this number should be done with due consideration of
 * the effect upon the ics_svc_t structure below.
 */
#define ICS_NUM_PRIO			5
#define ICS_MAX_PRIO			(ICS_NUM_PRIO-1)

/*
 * This is the version for all ICS messages within the kernel.  Any message
 * received whose version doesn't match this is dropped, and a warning message
 * is emitted.  Note that this number must fit in 28 bits; the other four
 * are used to distinguish messages from previous kernels lacking this
 * support, which have the icsh_type field as the first field of the message.
 *
 * NSC_XXX - The legacy support should be dropped at some point.  This whole
 * 	     thing needs to be redesigned for rolling update.
 */

/* The layout of the ICS_MESSAGE_VERSION */
#define	IMV_REL_BITS	12	/* For release number */
#define	IMV_REL_MASK	0x0fff0000
#define	IMV_ID_BITS	16	/* For id value */
#define	IMV_ID_MASK	0x0000ffff

#if	(IMV_REL_BITS + IMV_ID_BITS) > (32 - ICS_MTYPE_BITS)
#error	Too many bits for icsh_ver portion of icsh_version
#endif

#define	KERNMASK(id)	(((id) << IMV_ID_BITS) & IMV_REL_MASK)

/* Supported releases */
#define	OLD_KERNEL		0	/* Reserved for previous kernels */
#define	D10IPM30_KERNEL		1
#define	D11_KERNEL		2
#define	EAP_EXX_KERNEL		3
#define	E00_KERNEL		4
#define	NSC771a_KERNEL		5
#define	NSC711b_KERNEL		6
#define	NSC711c_KERNEL		7
#define	LINUX_KERNEL		8

#define ICS_MESSAGE_VERSION	( KERNMASK(LINUX_KERNEL) | 8 )



/*
 * Statistics for ICS subsystem.
 */
struct icsstat {
	/* Global statistics. */
	long	istat_proc[ICS_MAX_CHANNELS][ICS_MAX_SUBSERVICES];
					/* # of op table entries per service */
	char	*istat_svc_name[ICS_MAX_CHANNELS][ICS_MAX_SUBSERVICES];
					/* Name of each service */
	char	**istat_svc_proc_names[ICS_MAX_CHANNELS][ICS_MAX_SUBSERVICES];
					/* Name of each op table entry */

	/* Client statistics. */
	long	istat_clihand;		/* # of handles existing */
	long	istat_clirpc;		/* # of RPCs sent */
	long	istat_climsg;		/* # of messages sent */
	long	istat_clisend[ICS_NUM_PRIO][ICS_MAX_CHANNELS];
					/* # of RPCs and messages sent */
	long	*istat_clisend_proc[ICS_MAX_CHANNELS][ICS_MAX_SUBSERVICES];
					/* # of RPCs and messages sent */
					/* (by service/proc) */
	long	istat_clisigforward;	/* # signals forwarded */
	long	istat_clisigforward_retry; /* # of signal forwarding retries */
	long	istat_clithrottled;	/* # high-level ICS clients throttled */
	long	istat_clithrottled_event; /* # times high-level ICS throttled */
	long	istat_llclithrottled[ICS_MAX_CHANNELS]; /* # times low-level */
					/* ICS clients throttled */

	/* Server statistics. */
	long	istat_svrhand_create;	/* # of handles created */
	long	istat_svrhand;		/* # of handles existing */
	long	istat_svrrpc;		/* # of RPCs received */
	long	istat_svrmsg;		/* # of messages received */
	long	istat_svrrecv[ICS_NUM_PRIO][ICS_MAX_CHANNELS];
					/* # of RPCs and messages received */
	long	*istat_svrrecv_proc[ICS_MAX_CHANNELS][ICS_MAX_SUBSERVICES];
					/* # of RPCs and messages received */
					/* (by service/proc) */
	long	istat_svrsigforward;	/* # signals forwarded */
	long	istat_svrsigforward_retry; /* # of signal forwarding retries */
	long	istat_svrhand_nomem;	/* # times handle alloc deferred */
	long	istat_svrhand_nomem_norecv; /* # times handle alloc deferred */
					/* and handle needed */
	long	istat_svrhand_recv[ICS_NUM_PRIO][ICS_MAX_CHANNELS];
					/* # receives handled */
	long	istat_svrhand_norecv[ICS_NUM_PRIO][ICS_MAX_CHANNELS];
					/* # messages arrived with no handles */
	long	istat_svrdaemon_create;	/* # of daemons created */
	long	istat_svrdaemon;	/* # of daemons existing */
	long	istat_svrdaemon_active;	/* # of daemons processing messages */
	long	istat_svrdaemon_sleep[ICS_NUM_PRIO]; /* # times daemon slept */
					/* before receiving message */
	long	istat_svrdaemon_nosleep[ICS_NUM_PRIO]; /* # times daemon did */
					/* not sleep before receiving message */
	long	istat_svrnanny_run;	/* # times nanny daemon woken up */
	long	istat_svrthrottled[ICS_MAX_CHANNELS]; /* # times high-level */
					/* ICS services throttled */
};

#ifdef __KERNEL__

extern struct icsstat icsstat;

extern SPIN_LOCK_T	icsstat_lock;	/* not used for "totals" */

/*
 * Default low and high watermark free page limits for throttling on the server
 * side of ICS. The throttling is on a per-service basis, and these are the
 * defaults used if the service does not specify any limits.
 */
/* SSI_XXX: not used */
extern int icssvr_flowpages_lwm;
extern int icssvr_flowpages_hwm;

#ifdef SSI_NOTUSED
/*
 * Client low and high watermark free page limits for throttling on the client
 * side of ICS. The throttling is on a system-wide basis (not per-service),
 * and these are the limits that are used.
 */
extern int icscli_flowpages_lwm;
extern int icscli_flowpages_hwm;
#endif

/* /proc/cluster/icsstat file operations */
extern struct file_operations proc_icsstat_operations;
#endif /* __KERNEL__ */

#define ICS_TRANSPORT_NONE	0
#define ICS_TRANSPORT_IP	1
#define ICS_TRANSPORT_LTI	2
#define ICS_LAST_TRANSPORT	ICS_TRANSPORT_LTI

#ifdef __KERNEL__

#define ICS_TRANSPORT_IS_IP() \
	((ics_get_transport() == ICS_TRANSPORT_IP) ? 1 : 0)

#define ICS_TRANSPORT_IS_LTI() \
	((ics_get_transport() == ICS_TRANSPORT_LTI) ? 1 : 0)

#endif /* __KERNEL__ */

#endif /* !_CLUSTER_ICS_H */
