/*
 * Sun RPC is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify Sun RPC without charge, but are not authorized
 * to license or distribute it to anyone else except as part of a product or
 * program developed by the user.
 *
 * SUN RPC IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 *
 * Sun RPC is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 *
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY SUN RPC
 * OR ANY PART THEREOF.
 *
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even if
 * Sun has been advised of the possibility of such damages.
 *
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */

/*
 * xdr.h, External Data Representation Serialization Routines.
 *
 * Copyright (C) 1984, Sun Microsystems, Inc.
 */

#ifndef _RPC_XDR_H
#define _RPC_XDR_H 1

#include <linux/types.h>
#include <linux/fs.h>
#include <linux/statfs.h>
#include <cluster/nsc.h>
#include <cluster/rpc/types.h>
#include <cluster/nodelist.h>
#include <cluster/clms.h>

/* We need FILE.  */
#include <linux/file.h>

#ifndef __P
#define __P(x)		x
#endif

#ifndef __PMT
#define __PMT(x)	x
#endif

#ifndef __BEGIN_DECLS
# define __BEGIN_DECLS
#endif

#ifndef __END_DECLS
# define __END_DECLS
#endif

/* __BEGIN_DECLS */

/*
 * XDR provides a conventional way for converting between C data
 * types and an external bit-string representation.  Library supplied
 * routines provide for the conversion on built-in C data types.  These
 * routines and utility routines defined here are used to help implement
 * a type encode/decode routine for each user-defined type.
 *
 * Each data type provides a single procedure which takes two arguments:
 *
 *      bool_t
 *      xdrproc(xdrs, argresp)
 *              XDR *xdrs;
 *              <type> *argresp;
 *
 * xdrs is an instance of a XDR handle, to which or from which the data
 * type is to be converted.  argresp is a pointer to the structure to be
 * converted.  The XDR handle contains an operation field which indicates
 * which of the operations (ENCODE, DECODE * or FREE) is to be performed.
 *
 * XDR_DECODE may allocate space if the pointer argresp is null.  This
 * data can be freed with the XDR_FREE operation.
 *
 * We write only one procedure per data type to make it easy
 * to keep the encode and decode procedures for a data type consistent.
 * In many cases the same code performs all operations on a user defined type,
 * because all the hard work is done in the component type routines.
 * decode as a series of calls on the nested data types.
 */

/*
 * Xdr operations.  XDR_ENCODE causes the type to be encoded into the
 * stream.  XDR_DECODE causes the type to be extracted from the stream.
 * XDR_FREE can be used to release the space allocated by an XDR_DECODE
 * request.
 */
enum xdr_op {
  XDR_ENCODE = 0,
  XDR_DECODE = 1,
  XDR_FREE = 2
};

/*
 * This is the number of bytes per unit of external data.
 */
#define BYTES_PER_XDR_UNIT	(4)
/*
 * This only works if the above is a power of 2.  But it's defined to be
 * 4 by the appropriate RFCs.  So it will work.  And it's normally quicker
 * than the old routine.
 */
#if 1
#define RNDUP(x)  (((x) + BYTES_PER_XDR_UNIT - 1) & ~(BYTES_PER_XDR_UNIT - 1))
#else /* this is the old routine */
#define RNDUP(x)  ((((x) + BYTES_PER_XDR_UNIT - 1) / BYTES_PER_XDR_UNIT) \
		    * BYTES_PER_XDR_UNIT)
#endif

/*
 * The XDR handle.
 * Contains operation which is being applied to the stream,
 * an operations vector for the particular implementation (e.g. see xdr_mem.c),
 * and two private fields for the use of the particular implementation.
 */
typedef struct XDR XDR;
struct XDR
  {
    enum xdr_op x_op;		/* operation; fast additional param */
    struct xdr_ops
      {
	bool_t (*x_getlong) (XDR *__xdrs, long *__lp);
	/* get a long from underlying stream */
	bool_t (*x_putlong) (XDR *__xdrs, __const long *__lp);
	/* put a long to " */
	bool_t (*x_getbytes) (XDR *__xdrs, caddr_t __addr,
				     u_int __len);
	/* get some bytes from " */
	bool_t (*x_putbytes) (XDR *__xdrs, __const char *__addr,
				     u_int __len);
	/* put some bytes to " */
	u_int (*x_getpostn) (__const XDR *__xdrs);
	/* returns bytes off from beginning */
	bool_t (*x_setpostn) (XDR *__xdrs, u_int pos);
	/* lets you reposition the stream */
	int32_t *(*x_inline) (XDR *__xdrs, int len);
	/* buf quick ptr to buffered data */
	void (*x_destroy) (XDR *__xdrs);
	/* free privates of this xdr_stream */
	bool_t (*x_getint32) (XDR *__xdrs, int32_t *__ip);
	/* get a int from underlying stream */
	bool_t (*x_putint32) (XDR *__xdrs, __const int32_t *__ip);
	/* put a int to " */
      }
     *x_ops;
    caddr_t x_public;		/* users' data */
    caddr_t x_private;		/* pointer to private data */
    caddr_t x_base;		/* private used for position info */
    int x_handy;		/* extra private word */
  };

/*
 * A xdrproc_t exists for each data type which is to be encoded or decoded.
 *
 * The second argument to the xdrproc_t is a pointer to an opaque pointer.
 * The opaque pointer generally points to a structure of the data type
 * to be decoded.  If this pointer is 0, then the type routines should
 * allocate dynamic storage of the appropriate size and return it.
 * bool_t       (*xdrproc_t)(XDR *, caddr_t *);
 */
typedef bool_t (*xdrproc_t) (XDR *, void *,...);


/*
 * Operations defined on a XDR handle
 *
 * XDR          *xdrs;
 * int32_t      *int32p;
 * long         *longp;
 * caddr_t       addr;
 * u_int         len;
 * u_int         pos;
 */
#define XDR_GETINT32(xdrs, int32p)                      \
        (*(xdrs)->x_ops->x_getint32)(xdrs, int32p)
#define xdr_getint32(xdrs, int32p)                      \
        (*(xdrs)->x_ops->x_getint32)(xdrs, int32p)

#define XDR_PUTINT32(xdrs, int32p)                      \
        (*(xdrs)->x_ops->x_putint32)(xdrs, int32p)
#define xdr_putint32(xdrs, int32p)                      \
        (*(xdrs)->x_ops->x_putint32)(xdrs, int32p)

#define XDR_GETLONG(xdrs, longp)			\
	(*(xdrs)->x_ops->x_getlong)(xdrs, longp)
#define xdr_getlong(xdrs, longp)			\
	(*(xdrs)->x_ops->x_getlong)(xdrs, longp)

#define XDR_PUTLONG(xdrs, longp)			\
	(*(xdrs)->x_ops->x_putlong)(xdrs, longp)
#define xdr_putlong(xdrs, longp)			\
	(*(xdrs)->x_ops->x_putlong)(xdrs, longp)

#define XDR_GETBYTES(xdrs, addr, len)			\
	(*(xdrs)->x_ops->x_getbytes)(xdrs, addr, len)
#define xdr_getbytes(xdrs, addr, len)			\
	(*(xdrs)->x_ops->x_getbytes)(xdrs, addr, len)

#define XDR_PUTBYTES(xdrs, addr, len)			\
	(*(xdrs)->x_ops->x_putbytes)(xdrs, addr, len)
#define xdr_putbytes(xdrs, addr, len)			\
	(*(xdrs)->x_ops->x_putbytes)(xdrs, addr, len)

#define XDR_GETPOS(xdrs)				\
	(*(xdrs)->x_ops->x_getpostn)(xdrs)
#define xdr_getpos(xdrs)				\
	(*(xdrs)->x_ops->x_getpostn)(xdrs)

#define XDR_SETPOS(xdrs, pos)				\
	(*(xdrs)->x_ops->x_setpostn)(xdrs, pos)
#define xdr_setpos(xdrs, pos)				\
	(*(xdrs)->x_ops->x_setpostn)(xdrs, pos)

#define	XDR_INLINE(xdrs, len)				\
	(*(xdrs)->x_ops->x_inline)(xdrs, len)
#define	xdr_inline(xdrs, len)				\
	(*(xdrs)->x_ops->x_inline)(xdrs, len)

#define	XDR_DESTROY(xdrs)					\
	do {							\
		if ((xdrs)->x_ops->x_destroy)			\
			(*(xdrs)->x_ops->x_destroy)(xdrs);	\
	} while (0)
#define	xdr_destroy(xdrs)					\
	do {							\
		if ((xdrs)->x_ops->x_destroy)			\
			(*(xdrs)->x_ops->x_destroy)(xdrs);	\
	} while (0)

/*
 * Support struct for discriminated unions.
 * You create an array of xdrdiscrim structures, terminated with
 * a entry with a null procedure pointer.  The xdr_union routine gets
 * the discriminant value and then searches the array of structures
 * for a matching value.  If a match is found the associated xdr routine
 * is called to handle that part of the union.  If there is
 * no match, then a default routine may be called.
 * If there is no match and no default routine it is an error.
 */
#define NULL_xdrproc_t ((xdrproc_t)0)
struct xdr_discrim
{
  int value;
  xdrproc_t proc;
};

/*
 * Inline routines for fast encode/decode of primitive data types.
 * Caveat emptor: these use single memory cycles to get the
 * data from the underlying buffer, and will fail to operate
 * properly if the data is not aligned.  The standard way to use these
 * is to say:
 *      if ((buf = XDR_INLINE(xdrs, count)) == NULL)
 *              return (FALSE);
 *      <<< macro calls >>>
 * where ``count'' is the number of bytes of data occupied
 * by the primitive data types.
 *
 * N.B. and frozen for all time: each data type here uses 4 bytes
 * of external representation.
 */

#define IXDR_GET_INT32(buf)           ((int32_t)ntohl((uint32_t)*(buf)++))
#define IXDR_PUT_INT32(buf, v)        (*(buf)++ = (int32_t)htonl((uint32_t)(v)))
#define IXDR_GET_U_INT32(buf)         ((uint32_t)IXDR_GET_INT32(buf))
#define IXDR_PUT_U_INT32(buf, v)      IXDR_PUT_INT32(buf, (int32_t)(v))

/* WARNING: The IXDR_*_LONG defines are removed by Sun for new platforms
 * and shouldn't be used any longer. Code which use this defines or longs
 * in the RPC code will not work on 64bit Solaris platforms !
 */
#define IXDR_GET_LONG(buf) \
	((long)ntohl((u_long)*__extension__((u_int32_t*)(buf))++))
#define IXDR_PUT_LONG(buf, v) \
	(*__extension__((u_int32_t*)(buf))++ = (long)htonl((u_long)(v)))
#define IXDR_GET_U_LONG(buf)	      ((u_long)IXDR_GET_LONG(buf))
#define IXDR_PUT_U_LONG(buf, v)	      IXDR_PUT_LONG(buf, (long)(v))


#define IXDR_GET_BOOL(buf)            ((bool_t)IXDR_GET_LONG(buf))
#define IXDR_GET_ENUM(buf, t)         ((t)IXDR_GET_LONG(buf))
#define IXDR_GET_SHORT(buf)           ((short)IXDR_GET_LONG(buf))
#define IXDR_GET_U_SHORT(buf)         ((u_short)IXDR_GET_LONG(buf))

#define IXDR_PUT_BOOL(buf, v)         IXDR_PUT_LONG(buf, (long)(v))
#define IXDR_PUT_ENUM(buf, v)         IXDR_PUT_LONG(buf, (long)(v))
#define IXDR_PUT_SHORT(buf, v)        IXDR_PUT_LONG(buf, (long)(v))
#define IXDR_PUT_U_SHORT(buf, v)      IXDR_PUT_LONG(buf, (long)(v))

/*
 * These are the "generic" xdr routines.
 * None of these can have const applied because it's not possible to
 * know whether the call is a read or a write to the passed parameter
 * also, the XDR structure is always updated by some of these calls.
 */
extern bool_t xdr_void (void);
extern bool_t xdr_bool (XDR *__xdrs, bool_t *__bp);
extern bool_t xdr_array (XDR * _xdrs, caddr_t *__addrp, u_int *__sizep,
			      u_int __maxsize, u_int __elsize,
			      void * __elproc);
extern bool_t xdr_bytes (XDR *__xdrs, char **__cpp, u_int *__sizep,
			      u_int __maxsize);
extern bool_t xdr_opaque (XDR *__xdrs, caddr_t __cp, u_int __cnt);
extern bool_t xdr_string (XDR *__xdrs, char **__cpp, u_int __maxsize);
extern bool_t xdr_vector (XDR *__xdrs, char *__basep, u_int __nelem,
			       u_int __elemsize, void * __xdr_elem);
extern bool_t xdr_reference (XDR *__xdrs, caddr_t *__xpp, u_int __size,
				  void * __proc);
extern bool_t xdr_pointer (XDR *__xdrs, char **__objpp,
				u_int __obj_size, void * __xdr_obj);

/* free memory buffers for xdr */
extern void xdr_free (void * __proc, void *__objp);

/*
 * Common opaque bytes objects used by many rpc protocols;
 * declared here due to commonality.
 */
#define MAX_NETOBJ_SZ 1024
struct netobj
{
  u_int n_len;
  char *n_bytes;
};
typedef struct netobj netobj;

/*
 * Common defines for kernel data type xdr routines.
 *
 * SSI_XXX:
 *	XDR is convenient for some things, but it is also painful. Since we
 *	only run between common platforms, we don't need to do network byte-
 *	ordering. Moving to 64 bit platforms (alpha, ia64) is a pain, because
 *	the XDR long is restricted to 32 bits, resulting in data loss.
 *	However, since we are using XDR to move the equivalent of base
 *	structures, we really wanted to use the base types for the fields.
 *
 *	The compromise is to define the arch_int, arch_uint, arch_long, ...
 *	types which represent the architecture specific equivalent type
 *	and are sent across the wire as opaque data. Only the xdr_opaque()
 *	routine is defined, so to try to use the standard types will fail.
 *
 *	Over time, I hope to remove XDR from the code base entirely; however,
 *	it is currently the only method other than out-of-line data to
 *	send a ICS message of over 300 bytes and can handle complex
 *	data structures more easily than native ics.
 *
 *	The "arch_" types are defined <include/cluster/nsc.h>.
 */

#define XDR_TYPE(_type) \
static inline bool_t xdr_##_type(XDR *xdrs, _type *objp) \
{ \
	return xdr_opaque(xdrs, (void *)objp, sizeof(_type)); \
}

XDR_TYPE(arch_char)
XDR_TYPE(arch_uchar)
XDR_TYPE(arch_short)
XDR_TYPE(arch_ushort)
XDR_TYPE(arch_int)
XDR_TYPE(arch_uint)
XDR_TYPE(arch_long)
XDR_TYPE(arch_ulong)
XDR_TYPE(arch_longlong)
XDR_TYPE(arch_ulonglong)
XDR_TYPE(arch_ptr)
XDR_TYPE(enum_t)
#define xdr_enum xdr_enum_t
XDR_TYPE(size_t)
XDR_TYPE(caddr_t)
XDR_TYPE(daddr_t)
XDR_TYPE(vaddr_t)
XDR_TYPE(__u32)
XDR_TYPE(__u64)
XDR_TYPE(u32)
XDR_TYPE(u64)
XDR_TYPE(pid_t)
XDR_TYPE(uid_t)
XDR_TYPE(gid_t)
XDR_TYPE(kernel_cap_t)
XDR_TYPE(clusternode_t)
XDR_TYPE(dev_t)
XDR_TYPE(ino_t)
XDR_TYPE(off_t)
XDR_TYPE(loff_t)
XDR_TYPE(l_off_t)
XDR_TYPE(mode_t)
XDR_TYPE(nlink_t)
XDR_TYPE(clock_t)
XDR_TYPE(time_t)
XDR_TYPE(sigset_t)
XDR_TYPE(boolean_t)
XDR_TYPE(siginfo_t)
XDR_TYPE(nsc_nodelist_t)
XDR_TYPE(int8_t)
XDR_TYPE(uint8_t)
XDR_TYPE(int16_t)
XDR_TYPE(uint16_t)
XDR_TYPE(int32_t)
XDR_TYPE(uint32_t)
XDR_TYPE(int64_t)
XDR_TYPE(uint64_t)
typedef struct iattr iattr_t;
XDR_TYPE(iattr_t)
#define	xdr_iattr	xdr_iattr_t
typedef	struct kstatfs	kstatfs_t;
XDR_TYPE(kstatfs_t)
#define	xdr_kstatfs	xdr_kstatfs_t
typedef struct timespec timespec_t;
XDR_TYPE(timespec_t)
XDR_TYPE(umode_t)
XDR_TYPE(ctv_t)

/* Prototypes for other XDR routines. */

struct qstr;
bool_t xdr_qstr (XDR *, struct qstr *);

#endif /* rpc/xdr.h */
