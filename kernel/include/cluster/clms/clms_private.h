/*
 * 	CLMS private interface header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * The NSC Cluster Membership Service.
 */
#ifndef _CLUSTER_CLMS_CLMS_PRIVATE_H
#define _CLUSTER_CLMS_CLMS_PRIVATE_H

#if defined(__KERNEL__)

#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/clms.h>
#include <cluster/ics.h>

/*
 * Structures for keeping track of  CLMS Services (ICS, CFS, VPROC etc.).
 */
/*
 * Clms Service Data. See also clms.h.
 */
struct clms_svc_data {
	struct clms_svc_data	*s_next;
	struct sinit		*s_sinfo;	/* info about service */
	int		 	s_service;    	/* Service */
	int		 	s_pri;		/* Priority */
};

/*
 * Structure for keeping all services of the same priority together.
 */
struct clms_sband {
	struct clms_sband	*sb_next;
	struct clms_svc_data	*sb_first;  /* All registered services */
	struct clms_svc_data	*sb_last;
	int			 sb_pri;    /* The priority of this band */
};

/*
 * Services are stored in the following structure according to their
 * priority. This structure does not have a lock because unregistering
 * a service is not supported.
 */
struct clms_svc {
	struct clms_sband	*cs_first; /* All bands of svc info */
	int			cs_nbands; /* Number of bands */
};


/*
 * Structure to store all the information about a node.
 * Some per-node statistics are also stored here. This is being used
 * by some NSC Components.
 */

struct clms_node_info {
	clusternode_t	node;		/* Node Number of Node */
	int		current_state;	/* Current State of Node */
	int		prev_state;	/* Previous State of Node */
	int		flags;		/* Flags for this node. */
	clms_api_state_t api_state;	/* The API state of the node. */
	int		total_cpus;
	int		online_cpus;
	unsigned long	cpu_power;
	unsigned long	total_memory;
};

#define CLMS_NODE_ICS_NODEUP_DONE	0x01
#define CLMS_NODE_NODEUP		0x02
#define CLMS_NODE_MASTER_RWLOCK		0x04
#define CLMS_NODE_ICS_READY_DONE	0x08
#define CLMS_NODE_GOING_AWAY		0x10
#define CLMS_NODE_BLOCK_READY		0x20
#define CLMS_NODE_INFORM_NOT_READY	0x40
#define CLMS_NODE_INFORM_NOT_DONE	0x80

/*
 * Definitions for nodedown states and flags.
 */
/* States */
#define CLMS_NDTO_WARN	0	/* Issue a warning message */
#define CLMS_NDTO_PANIC	1	/* Panic, we timed out */
/* Active flags */
#define CLMS_NDTO_FINISHED	0	/* No timeouts outstanding. */
#define CLMS_NDTO_OUTSTANDING	1	/* Timeout outstanding. */
#define CLMS_NDTO_TIMEDOUT	2	/* Timed out, running handler. */

/*
 * Cluster Statistics.
 */
struct clms_cluster_stat {
	int	clms_num_nodes;		/* Number of nodes in the cluster */
	int	clms_num_nodes_up;	/* Number of nodes currently up */
	int	clms_num_cluster_servers;   /* Number of Cluster Servers */
	int	clms_num_up_transitions;    /* Number of occurrences of */
					    /* "Node Up" event */
	int	clms_num_down_transitions;  /* Number of occurrences of */
					    /* "Node Down" event */
	int	clms_save_down_transitions; /* Saved down_trans received */
					    /* from master on nodedown. */
};

typedef struct clms_cluster_stat clms_cluster_stat_t;

/*
 * Node Transitions.
 */
/*
 * The following structure is used to keep track of nodes that are
 * coming up or going down. Currently only one node can come up at a time.
 * However, other nodes will be allowed to come up to a point.
 * The clms coordinator will block transparently.
 * Nodes can go down simultaneously.
 */
struct clms_transition_data {
	struct clms_transition_data   *next;
	struct clms_transition_data   *prev;
	clusternode_t		       node;
	clusternode_t		       surrogate_node;
	int			       transition_callbacks[CLMS_MAX_SUBSYSTEMS];
	/* callbacks for band -1 subsystems. */
	int			       transition_callbacks2[CLMS_MAX_SUBSYSTEMS];
	struct clms_nodelist	      *cnlist;
	struct clms_nodelist	      *ready_cnlist;
	struct clms_nodelist	      *called_cnlist;
	void			      *private_data;
	CONDITION_T		       cond;
	COND_LOCK_T		       lock;
};

/*
 * The following are flags used to indicate type of functions called by clms.
 */
#define CLMS_CALLED_FUNCTION     	 0x1
#define CLMS_FUNCTION_CALLED_BACK	 0x0

#define CLMS_CALL_NODEUP		0x01
#define CLMS_CALL_NODEDOWN		0x02
#define CLMS_CALL_SURROGATE_NODEUP	0x04
#define CLMS_CALL_SURROGATE_NODEDOWN	0x08
#define CLMS_CALL_SURROGATE_FAILOVER	0x10

/*
 * The following structure is used for queuing "Node Up" and "Node Down"
 * events at the clms master. The corresponding daemon is then signaled
 * and it will process the event.
 */
struct clms_transition_queue {
	struct clms_transition_data	*cq_first;
	struct clms_transition_data	*cq_last;
	int				 cq_flag;
	CONDITION_T		 	 cq_cond;
	COND_LOCK_T		 	 cq_lock;
};

/*
 * This is data structure used to process a list of nodes for any purpose.
 * This structure is used by clms only. Other components tend to use the
 * nsc_nodelist structure. CLMS cannot always use the nsc_nodelist structure
 * as it is not possible to maintain order in a bitmask.
 */
struct clms_nodelist {
	struct clms_nodelist	*next;
	struct clms_nodelist	*prev;
	int			status;
	clusternode_t		*nodelist;
	clusternode_t		*n_rptr;
	clusternode_t		*n_wptr;
	clusternode_t		*n_lim;
	int			num_nodes;
};

/*
 * The following structure is used to store information about a transition
 * at the CLMS client.
 */
struct clms_cli_tr_data {
	struct clms_cli_tr_data		*next;
	struct clms_cli_tr_data		*prev;
	clusternode_t			node;
	int				trans;
	int				why;
	clms_ctv_t			ctv;
	union {
		struct {
			u_long		clms_handle;
			clusternode_t	surrogate_node;
			int		wasntup;
			clusternode_t	master_node;
			int		down_trans;
			clusternode_t	servers[CLMS_MAX_KEY_SERVICES];
		} nodedown;
	} data;
};
typedef struct clms_cli_tr_data clms_cli_tr_data_t;

/*
 * This structure is used to queue "Node Up" events at the clms client.
 * The clms_client_nodeup_daemon is signaled and it will process the
 * events.
 */
struct clms_client_trans_queue {
	clms_cli_tr_data_t	*first;
	clms_cli_tr_data_t	*last;
	int			size;
};

/*
 * The CLMS secondary list structure.
 */
typedef struct clms_secondary_list {
	clusternode_t			node;
	icsinfo_t			addr;
	struct clms_secondary_list	*next;
	int				do_probe;
	int				probe_only;
} clms_secondary_list_t;

/*
 * A structure used to pass information to the CLMS reassign-key-services
 * thread.
 */
struct clms_reassign_info_st {
	clusternode_t	from_node;
	clusternode_t	down_node;
	nsc_nodelist_t	*nlist;
	int		flags[CLMS_MAX_KEY_SERVICES];
};

/*
 * A structure used store API state transition intent information.
 */
typedef struct clms_api_trans_intent_st {
	clusternode_t	client_node;
	int		new_state;
	int		why;
	int		present;
	clms_ctv_t	ctv;
} clms_api_trans_intent_t;

/*
 * REQUEST: Whenever this structure is updated, please update the comment
 *          section also.
 *
 * THE CLMS STRUCTURE:
 *
 * clms_node_info keeps track of the status and other information about all
 * 		  the nodes in the cluster.
 *
 * clms_cluster_stat keeps statistics about the cluster.
 *
 * clms_node_lock is used to update node states and cluster statistics.
 *
 * clms_node_cond is used to wait for changes in states of nodes.
 *
 * clms_cluster_server is used to store all the information about key nodes
 *		       in the cluster providing critical services like
 *		       root file system, clms coordinator etc. Currently
 *		       only ROOT FILE SYSTEM SERVICE and CLMS SERVICE are
 *		       distinguished.
 *
 * clms_cluster_svr_cond	condition variable to wait on for cluster
 *				cluster server related events.
 *
 * clms_cluster_svr_lock	lock for cluster server related updates.
 *
 * clms_nodeup_queue		This is the queue in which the clms master
 *				nodeup daemon takes its input from. When
 *				clms gets a "Node Up" indication from ICS,
 *				it queues the relevant information on this
 *				queue and wakes up the daemon.
 *
 * clms_partial_nodeup_queue	This is the queue used for those nodes who
 *				have rebooted before their "Node Down" is
 *				complete. In this case, they are queued in
 *				this queue.  When the "Node Down" processing
 *				has been completed, the entries from this
 *				queue will be moved to the clms_nodeup_queue
 *				and the clms master nodeup daemon is waken up.
 *
 * clms_failover_nodeup_queue	This is the queue used for nodes that try to
 *				come in while we're doing failover; we can
 *				process the nodeup until failover is complete,
 *				so we queue it for that time.
 *
 * clms_nodedown_queue		This is the queue where all the nodes that
 *				have gone down are queued to be processed by
 *				the clms master nodedown daemon. Note that
 *				all "Node Down" processing are initiated
 *				by the clms master only. Other entities like
 *				ICS and client nodes simply inform the clms
 *				master if they think that a node is down.
 *
 * clms_ntrans_flag		The flag along with the lock and the
 * clms_ntrans_lock		synchronization variable are used to`
 * clms_ntrans_cond		synchronize/serialize/freeze node
 *				transitions (particularly "Node Up"s and
 *				"Node Down"s).
 *
 * clms_svc			This structure is used to keep track of
 *				all clms services, their registered routines,
 *				priorities etc.
 *
 * clms_ctv			Cluster transition vector for this node.
 * clms_ctv_failover		"Doing failover" flag.
 * clms_ctv_rw_lock		Shared-mode sleep lock for CTV.
 * clms_ctv_lock		Condition lock for CTV.
 * clms_ctv_cond		Condition to wait for when failover is running.
 *
 * clms_api_rw_lock		Shared-mode sleep lock for the API.
 * clms_api_trans_hist		Pointer to the beginning of the API transition
 *				history circular buffer.
 * clms_api_trans_ptr		Pointer to the current insert point in the API
 *				transition history.
 * clms_api_trans_wrapped	Flag that indicates that the API transition
 *				history wrapped.
 *
 * clms_api_trans_intent	The "API transition pending" intent structure.
 */
struct clms {
	struct clms_node_info		clms_node_info[NSC_MAX_NODE_VALUE + 1];
	clms_cluster_stat_t	 	clms_cluster_stat;
	COND_LOCK_T			clms_node_lock;
	CONDITION_T			clms_node_cond;

	cluster_server_t 		*clms_cluster_server[CLMS_MAX_KEY_SERVICES];
	cluster_server_t 		*clms_cluster_floater[CLMS_MAX_KEY_SERVICES];
	CONDITION_T			clms_cluster_svr_cond;
	COND_LOCK_T			clms_cluster_svr_lock;

	SPIN_LOCK_T			clms_key_service_node_lock;

	struct clms_transition_queue	clms_nodeup_queue;
	struct clms_transition_queue	clms_partial_nodeup_queue;
	struct clms_transition_queue	clms_failover_nodeup_queue;
	struct clms_transition_queue	clms_node_join_queue;
	struct clms_transition_queue	clms_nodedown_queue;
	struct clms_client_trans_queue  clms_client_trans_queue;
	struct clms_client_trans_queue  clms_client_save_queue;

	int				clms_ntrans_flag;
	COND_LOCK_T			clms_ntrans_lock;
	CONDITION_T			clms_ntrans_cond;

	struct clms_svc			clms_svc;

	int				clms_transition_nd;
	int				clms_transition_flag;
	COND_LOCK_T			clms_transition_lock;
	CONDITION_T			clms_transition_cond;

	LOCK_T				clms_failover_lock;
	LOCK_T				clms_parameters_lock;

	clms_ctv_t			clms_ctv;
	int				clms_ctv_failover;
	RW_LOCK_T			clms_ctv_rw_lock;
	COND_LOCK_T			clms_ctv_lock;
	CONDITION_T			clms_ctv_cond;

	RW_LOCK_T			clms_api_rw_lock;
	SPIN_LOCK_T			clms_api_hist_lock;
	clms_transition_t		*clms_api_trans_hist;
	clms_transition_t		*clms_api_trans_ptr;
	int				clms_api_trans_wrapped;
	clms_api_trans_intent_t		clms_api_trans_intent;
};

/*
 * Values for clms_ntrans_flag.
 */
#define CLMS_NTRANS_NODEUP		0x1
#define CLMS_NTRANS_NODEDOWN		0x2
#define CLMS_NTRANS_STATE_CHANGE	0x4

/*
 * Values for clms_transition_flag.
 */
#define CLMS_TRANS_BLOCK_TRANS		0x01
#define CLMS_TRANS_BLOCK_NODEUP_INFO	0x02
#define CLMS_TRANS_SHUTDOWN		0x04
#define CLMS_TRANS_FAILOVER		0x08
#define CLMS_TRANS_BLOCK_REGROUP_ADD	0x10

#define CLMS_TRANS_HIST_SZ		(NSC_MAX_NODE_VALUE * 16)

#define CLMS_NODEDOWN_LOCK()						\
	{								\
		LOCK_COND_LOCK(&clms.clms_transition_lock);		\
		clms.clms_transition_nd++;				\
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);		\
	}

#define CLMS_NODEDOWN_UNLOCK()						\
	{								\
		register int do_broadcast;				\
									\
		LOCK_COND_LOCK(&clms.clms_transition_lock);		\
		do_broadcast = 0;					\
		if (--clms.clms_transition_nd == 0)			\
			do_broadcast = 1;				\
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);		\
		if (do_broadcast)					\
			BROADCAST_CONDITION(&clms.clms_transition_cond); \
	}

#define CLMS_TRANSITION_BLOCK(nd, flag)					\
	{								\
		LOCK_COND_LOCK(&clms.clms_transition_lock);		\
		while (((nd) && clms.clms_transition_nd > 0) ||		\
		       clms.clms_transition_flag & (flag)) {		\
			WAIT_CONDITION(&clms.clms_transition_cond,	\
				       &clms.clms_transition_lock);	\
		}							\
		clms.clms_transition_flag |= flag;			\
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);		\
	}

#define CLMS_TRANSITION_BLOCK2(nd, flag1, flag2)			\
	{								\
		LOCK_COND_LOCK(&clms.clms_transition_lock);		\
		while (((nd) && clms.clms_transition_nd > 0) ||		\
		       clms.clms_transition_flag & (flag1)) {		\
			WAIT_CONDITION(&clms.clms_transition_cond,	\
				       &clms.clms_transition_lock);	\
		}							\
		clms.clms_transition_flag |= flag2;			\
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);		\
	}

#define CLMS_TRANSITION_UNBLOCK()					\
	{								\
		LOCK_COND_LOCK(&clms.clms_transition_lock);		\
		clms.clms_transition_flag = 0;				\
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);		\
		BROADCAST_CONDITION(&clms.clms_transition_cond);	\
	}

#define clms_set_nodeup_inprogress() \
		clms_set_op_inprogress(CLMS_NTRANS_NODEUP)

#define clms_clear_nodeup_inprogress() \
		clms_clear_op_inprogress(CLMS_NTRANS_NODEUP)

#define clms_freeze_nodeup() clms_set_nodeup_inprogress()

#define clms_unfreeze_nodeup() clms_clear_nodeup_inprogress()


#define clms_set_nodedown_inprogress() \
		clms_set_op_inprogress(CLMS_NTRANS_NODEDOWN)
#define clms_clear_nodedown_inprogress() \
		clms_clear_op_inprogress(CLMS_NTRANS_NODEDOWN)

/*
 * Declaration of root structure for clms.
 */
extern struct clms clms;

/*
 * CLMS Tunables.
 */
extern unsigned int clms_run_nodedown_at_fp;
extern unsigned int clms_force_root_dly;
extern unsigned int clms_nodedown_timeout;
extern int clms_service_wait_timeout;
extern int clms_service_timeout_use_2nd;
extern int clms_service_timeout_use_master;

/*
 * Definitions related to keepalive probes
 */
#define CLMS_MASTER_PROBE_INTVL	5		/* client times out after 5
						   seconds inactivity */
#define CLMS_CLIENT_PROBE_INTVL	5

#define CLMS_CLIENT_KEEPALIVE_MAXTIME	2	/* master times out after
						   10-15 seconds inactivity */

extern int clms_keepalive;

extern clock_t clms_ticks_delta;

extern int clms_num_secondaries;
extern clms_secondary_list_t *clms_secondary_list_head;
extern int clms_im_a_secondary;

extern int clms_ready_for_probe;

extern int clms_all_svcs_assigned;

extern LOCK_T clms_key_service_lock;

extern EVENT_T clms_assign_service_event;


/*
 * Prototypes of routines internal to CLMS.
 */
void clms_master_init(int);
void clms_client_init(void);
clusternode_t clms_pick_surrogate_node(clusternode_t);

extern ATOMIC_INT_T clms_client_keepalive_timer;

#define CLMS_NEW_TRANSID(ctv)					\
do {								\
		(ctv)->transid++;				\
		timespec_to_timeval(&xtime, &(ctv)->stamp);	\
} while (0)

/*
 * CLMS logging information.
 */
#if defined(DEBUG) || defined(DEBUG_TOOLS)

extern int clms_log(long, long, long, long, long, long, long, long);
extern void clms_log_init(void);
extern int clms_do_logging;

#define		CLMSLOG(msgnum, a, b, c, d, e, f, g)			\
			((void)((clms_do_logging) &&			\
				clms_log((long)(msgnum), (long)(a), 	\
					 (long)(b), (long)(c),		\
					 (long)(d), (long)(e), 		\
					 (long)(f), (long)(g))))

/*
 * Message numbers; these correspond to entries in the clmslog_msgs array in
 * clms_subr.c (they are indices into the array).
 */
#define CLMSLOG_WANTOJOIN	0
#define CLMSLOG_INFORMING	1
#define CLMSLOG_WTJINFORM	2
#define CLMSLOG_MASNODEUP	3
#define CLMSLOG_SETRGPINFO	4
#define CLMSLOG_NEWNODEDOWN	5
#define CLMSLOG_NODEJOIN	6
#define CLMSLOG_SENDNODEUP	7
#define CLMSLOG_SENDNEWNODE	8
#define CLMSLOG_NDSENT		9
#define CLMSLOG_CLIJOIN		10
#define CLMSLOG_GOTJOIN		11
#define CLMSLOG_STARTNODE	12
#define CLMSLOG_GOTINFO		13
#define CLMSLOG_NODEINFO	14
#define CLMSLOG_CLINODEUP	15
#define CLMSLOG_CLINUDONE	16
#define CLMSLOG_DOWNUP		17
#define CLMSLOG_INFORM		18
#define CLMSLOG_RGPADD		19
#define CLMSLOG_RGPMON		20
#define CLMSLOG_RGPSETINFO	21
#define CLMSLOG_ONLYPROBE	22
#define CLMSLOG_PRONLYRETRY	23
#define CLMSLOG_CLINODEDOWN	24
#define CLMSLOG_CLINDSTART	25
#define CLMSLOG_CLINDFIN	26
#define CLMSLOG_MASNDIND	27
#define CLMSLOG_MASNDSTART	28
#define CLMSLOG_MASNDFIN	29
#define CLMSLOG_NUFAIL		30
#define CLMSLOG_SDTOSS		31
#define CLMSLOG_SHUTDOWN	32
#define CLMSLOG_NEED2ND		33
#define CLMSLOG_CLEARSVC	34
#define CLMSLOG_SAVE2ND		35
#define CLMSLOG_SET2ND		36
#define CLMSLOG_READYING	37
#define CLMSLOG_READY		38
#define CLMSLOG_NDFINSENT	39
#define CLMSLOG_NEWSTATE	40
#define CLMSLOG_INTENT		41
#define CLMSLOG_COMMIT		42
#define CLMSLOG_UNBLOCK		43
#define CLMSLOG_MASNUIND	44
#define CLMSLOG_MASNUFAIL	45
#define CLMSLOG_MASNUFIN	46
#define CLMSLOG_FAILSTART	47
#define CLMSLOG_FAILFIN		48
#define CLMSLOG_QUERYING	49
#define CLMSLOG_QUERY		50
#define CLMSLOG_QUERYREPLY	51
#define CLMSLOG_ROLL		52
#define CLMSLOG_GOINGAWAY	53
#define CLMSLOG_JOINKEYS	54
#define CLMSLOG_DECLMAST	55
#define CLMSLOG_NDIND		56
#define CLMSLOG_STATS		57
#define CLMSLOG_NUIND		58
#define CLMSLOG_INFORMRET	59
#define CLMSLOG_API_LOCK	60
#define CLMSLOG_API_UNLOCK	61

#define CLMSLOG_NUMMSGS		62	/* Number of defined messages */
					/* Number of log entries before wrap */
#define CLMSLOG_DEPTH		(512)

#else /* DEBUG || DEBUG_TOOLS */

#define		CLMSLOG(msgnum, a, b, c, d, e, f, g)

#endif /* DEBUG || DEBUG_TOOLS */

extern clusternode_t clms_find_master(int *, int *, clms_secondary_list_t **);

extern struct clms_transition_data *clms_alloc_trans_data(void);

extern struct clms_nodelist *clms_alloc_nodelist(int, int);

extern struct clms_cli_nu_data *clms_alloc_client_nu_data(clusternode_t,
							  clms_ctv_t);

extern struct clms_newnode_info *clms_alloc_newnode_info(clusternode_t);

extern struct clms_nodelist *clms_get_up_nodelist(void);

extern clusternode_t clms_get_next_node(struct clms_nodelist *);

extern void clms_allocsvcband(int);

extern void clms_init_trans_data(struct clms_transition_data *, clusternode_t);

extern void clms_free_trans_data(struct clms_transition_data *);

extern struct clms_transition_data *clms_getq_trans_l(
					struct clms_transition_queue *);

extern void clms_free_nodelist(struct clms_nodelist *);

extern nsc_nodelist_t *clms_get_nsc_nodelist_fc(struct clms_nodelist *);

extern clusternode_t clms_pick_surrogate_node(clusternode_t);


extern int clms_vset_cluster_svr(int, clusternode_t, int, int);


extern int clms_set_cluster_svr_status(int, clusternode_t, int);

extern int clms_set_cluster_server(int, clusternode_t, int, int);

extern clms_cli_tr_data_t *clms_alloc_client_tr_data(int,
						     clusternode_t,
						     int,
						     clms_ctv_t);

extern clms_cli_tr_data_t *clms_get_trans_queue_l(struct clms_client_trans_queue*);

extern clms_cli_tr_data_t *clms_del_trans_queue_l(
					struct clms_client_trans_queue *qp,
					clms_ctv_t ctv);

extern void clms_copy_trans_queue_l(struct clms_client_trans_queue *,
				    clms_cli_tr_data_t *);

extern void clms_set_op_inprogress(int);

extern int clms_waitfor_nodeup(clusternode_t, boolean_t);

extern void clms_clear_op_inprogress(int);

extern void clms_setup_key_services(void);

extern void clms_assign_key_services(void);

extern void clms_reassign_key_services(clusternode_t, clusternode_t,
				       clusternode_t *);

extern void clms_handle_cluster_svr_timeout(void *);

extern void clms_callback(void *, int, clusternode_t, int);

extern void clms_get_key_servers(clusternode_t *, int *, int *);

extern void clms_reassign_services_thread(void *);

extern void clms_sigcluster_all(void);

extern int clms_api_newstate(clusternode_t, int, int);

extern void clms_api_transition(clusternode_t, int, int, clms_ctv_t);

extern void clms_api_transition_l(clusternode_t, int, int, clms_ctv_t);

extern int clms_api_get_state_l(clusternode_t);

extern void clms_api_get_full_state_l(clusternode_t, clms_api_state_t *);

extern void clms_api_get_hist(clms_transition_t *, int *, u_long);

extern void clms_api_hist(clusternode_t, int, int, clms_ctv_t);

extern void clms_init_handle_registry(void);

extern void clms_register_handle(u_long);

extern void clms_deregister_handle(u_long);

extern int clms_find_handle(u_long);

extern void clms_maintain_statistics(clusternode_t, int, int);

extern void clms_insert_entry(clms_secondary_list_t **,
			      clms_secondary_list_t *);

extern struct clms_sband *clms_get_svcband(int);

extern int clms_set_node(struct clms_nodelist *, clusternode_t);

extern void clms_waitfor_client_callbacks(struct clms_transition_data *, int);

extern int clms_put_trans_queue_l(struct clms_client_trans_queue *,
				  clms_cli_tr_data_t *);

extern void clms_node_trans_ind(clusternode_t, int, icsinfo_t *);

extern int clms_rmvq_trans_l(struct clms_transition_queue *,
			     struct clms_transition_data *);

extern int clms_is_cluster_svr_set(int, clusternode_t);

extern boolean_t clms_is_higher_secondary(clusternode_t, clusternode_t);

extern boolean_t clms_is_secondary(clusternode_t);

extern int clms_waitfor_nodeicsup(clusternode_t);

extern int clms_answer_cluster_svrinfo_request(clusternode_t);

extern int clms_send_cluster_svrinfo(clusternode_t, int, int, int, int);

extern void clms_nodeup_ind(clusternode_t, icsinfo_t *);

extern void clms_nodedown_ind(clusternode_t);

extern int clms_ics_init(void);

extern void clms_going_away(void);

extern void clms_nodedown(clusternode_t);

extern int clms_set_node_status_l(clusternode_t, int);

extern int clms_waitfor_resource_info(void);

extern void clms_surrogate_init(void);

extern int clms_transition_qinit(struct clms_transition_queue *, int);

extern int clms_get_cluster_svr_type(int, clusternode_t);

extern int clms_ins_cluster_server_l(cluster_server_t *);

extern int clms_waitfor_cluster_svr_info(int);

extern int clms_master_waitfor_resource_info(void);

extern int clms_client_waitfor_resource_info(void);

extern int clms_master_join_cluster(void);

extern int clms_client_join_cluster(void);

extern int clms_master_startup_node(void);

extern int clms_client_startup_node(void);

extern void clms_client_setup_key_services(void);

extern int clms_client_request_cluster_svrinfo(void);

extern int clms_set_node_status(clusternode_t, int);

extern void clms_waitfor_key_service_info(void);

extern int clms_call_all_svcs(struct clms_transition_data *, int);

extern int clms_test_and_save_cluster_downtrans(int);

extern void clms_key_service_nodedown(clusternode_t, clusternode_t *, int);

extern void clms_node_halfdown(clusternode_t);

extern void clms_master_setup_key_services(void);

extern int clms_surrogate_nodeup(struct clms_transition_data *);

extern int clms_queue_cluster_svrinfo_request(clusternode_t);

extern int clms_alloc_cluster_svrinfo(clusternode_t *, int **,
				      clusternode_t **, int **, int **, int *);

extern int clms_get_cluster_svrinfo(clusternode_t *, int *, clusternode_t *,
				    int *, int *, int);

extern void clms_ready_key_nodes(clusternode_t);

extern int clms_alloc_key_nodeinfo(clusternode_t **, icsinfo_t **, int *);

extern int clms_get_key_nodeinfo(clusternode_t *, icsinfo_t *, int);

extern int clms_get_node_prev_status(clusternode_t);

extern int clms_putq_trans_l(struct clms_transition_queue *,
			     struct clms_transition_data *);

extern int clms_inform_key_nodes(clusternode_t);

extern int clms_rmv_partial_nodeup(clusternode_t);

extern int clms_surrogate_nodedown(struct clms_transition_data *);

extern int clms_get_cluster_num_downtrans(void);

extern int clms_clear_node(struct clms_nodelist *, clusternode_t);

extern void clms_node_halfdown(clusternode_t);

extern int clms_putbq_trans_l(struct clms_transition_queue *,
			      struct clms_transition_data *);

extern int clms_process_partial_nodeup(clusternode_t);

extern int clms_get_node_num_downtrans(clusternode_t);

extern void clms_attempt_failover(clusternode_t);

extern void clms_master_nodeup(clusternode_t, icsinfo_t *);

extern void clms_master_nodedown(clusternode_t);

extern void clms_client_nodeup(clusternode_t);

extern void clms_client_nodedown(clusternode_t);

extern int clms_parse_nodelist(const char *, clms_secondary_list_t **,
			       int *, int *, char **);

extern void clms_client_update_masterlist(const char *);

struct nm_settings;

extern void clms_client_update_nm_settings(const struct nm_settings *);

#endif /* defined(__KERNEL__) */

#endif /* !_CLUSTER_CLMS_CLMS_PRIVATE_H */
