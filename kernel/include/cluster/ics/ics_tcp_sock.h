/*
 * 	Low-level ICS handle support.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * Header file used internally by the NSC ICS low-level code.
 *
 * 22JUN2001  aneesh.kumar@digital.com
 * Change the ihdr_transid field of struct icshdr_t.
 * This variable is found to carry address
 *
 */

#ifndef	_CLUSTER_ICS_ICS_TCP_SOCK_H
#define	_CLUSTER_ICS_ICS_TCP_SOCK_H

#include <cluster/nsc.h>
#include <cluster/synch.h>

/*
 * A structure that describes an encoded OOL data block. This includes OOL
 * data blocks corresponding to input parameters encoded by the client and
 * OOL data blocks corresponding to output parameters encoded by the server.
 *
 * NSC_XXX: The following pertains to the NSC TCP implementation.
 * 	    The callback code can and needs to be simplified.
 * Note that the callback that corresponds to the OOL data goes through two
 * levels of indirect calls. First, the routine specified by ool_freertn is
 * called, and this routine calls the ool_callback() routine. This is
 * necessary because the TCP subsystem allows routines with only one
 * parameter, and the ICS definition requires two parameters.
 */
typedef struct ooldesc {
	union		{
		struct {
			void		(*_ool_callback_data_t)(
						u_char		*data,
						long		callarg);
			char		*_ool_callparam;
			long		_ool_callarg;
		}		_ool_callback;
	}		_ool_union;
	union		{
		struct svr_handle	*_ool_svr_handle;
		struct cli_handle	*_ool_cli_handle;
	}		_ool_handle;
} ooldesc_t;

#define ool_callback_data_t _ool_union._ool_callback._ool_callback_data_t
#define ool_callparam _ool_union._ool_callback._ool_callparam
#define ool_callarg _ool_union._ool_callback._ool_callarg
#define ool_svr_handle _ool_handle._ool_svr_handle
#define ool_cli_handle _ool_handle._ool_cli_handle

/*
 * The following structure combines the default handle allocations into
 * a single structure.
 */

/*
 * Structure for the portion of the client handle that pertains to the
 * low-level ICS implementation.
 */
typedef struct tcp_cli_llhandle {

	/*
	 * Pointer to msghdr for the outgoing message.
	 */
	struct msghdr	*_ch_msg_msghdr_p;

#ifdef ICS_TCP_SENDPAGE
	/*
	 * Pointer to array of contiguous pages and length of page data.
	 */
	struct page	**_ch_msg_pagevec;
	u_int		_ch_msg_page_len;
#endif

	/*
	 * Part of structure devoted to tracking the in-line buffer for
	 * the outgoing message. Note that the iovec includes the actual
	 * icshdr data in addition to the inline data.
	 */
	struct iovec	*_ch_msg_inline_iov_p;
	u_char		*_ch_msg_inline_data_p;

	/*
	 * Part of structure devoted to tracking OOL buffers that have
	 * their own free routines for the outgoing message.
	 */
	ooldesc_t	*_ch_msg_ooldesc_p;
	int		_ch_msg_ooldesc_cnt;

	/*
	 * Pointer to sk_buff for the incoming reply (if there is an incoming
	 * reply). tskb is a pointer to the last skb in the chain.
	 */
	struct sk_buff	*_ch_rep_skb_p;
	struct sk_buff	*_ch_rep_tskb_p;

	/*
	 * Part of structure devoted to tracking the in-line buffer for the
	 * incoming reply (if there is an incoming reply).
	 */
	struct sk_buff	*_ch_rep_inline_skb_p;
	u_char		*_ch_rep_inline_data_p;

	/*
	 * Part of structure devoted to tracking OOL buffers for the incoming
	 * reply (if there is an incoming reply).
	 */
	struct sk_buff	*_ch_rep_ool_skb_p;
	u_char		*_ch_rep_ool_data_p;

	/*
	 * Reference count for counting number of OOL data block in handle
	 * (plus one for the in-line data and plus one for the reply if
	 * one is expected).
	 */
	int		_ch_ref_cnt;

	/*
	 * Flag for low-level ICS code.
	 */
	int		_ch_llflag;

	/*
	 * Lock used for ch_llflag and ch_ref_cnt.
	 */
	SOFTIRQ_SPIN_LOCK_T	_ch_lllock;

	/* msghdr, inline data, and default iovec and ooldesc. */
	struct msghdr	_ch_msghdr;
	struct iovec	_ch_iovec[ICS_DEF_OOL_SEGMENTS + 1];
	ooldesc_t	_ch_ooldesc[ICS_DEF_OOL_SEGMENTS];
	u_char		_ch_inline[sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE];
} tcp_cli_llhandle_t;

#define ch_msg_msghdr_p ch_llhandle.tcpcli_llhandle._ch_msg_msghdr_p
#ifdef ICS_TCP_SENDPAGE
#define ch_msg_pagevec ch_llhandle.tcpcli_llhandle._ch_msg_pagevec
#define ch_msg_page_len ch_llhandle.tcpcli_llhandle._ch_msg_page_len
#endif
#define ch_msg_inline_iov_p ch_llhandle.tcpcli_llhandle._ch_msg_inline_iov_p
#define ch_msg_inline_data_p ch_llhandle.tcpcli_llhandle._ch_msg_inline_data_p
#define ch_msg_ooldesc_p ch_llhandle.tcpcli_llhandle._ch_msg_ooldesc_p
#define ch_msg_ooldesc_cnt ch_llhandle.tcpcli_llhandle._ch_msg_ooldesc_cnt
#define ch_rep_skb_p ch_llhandle.tcpcli_llhandle._ch_rep_skb_p
#define ch_rep_tskb_p ch_llhandle.tcpcli_llhandle._ch_rep_tskb_p
#define ch_rep_inline_skb_p ch_llhandle.tcpcli_llhandle._ch_rep_inline_skb_p
#define ch_rep_inline_data_p ch_llhandle.tcpcli_llhandle._ch_rep_inline_data_p
#define ch_rep_ool_skb_p ch_llhandle.tcpcli_llhandle._ch_rep_ool_skb_p
#define ch_rep_ool_data_p ch_llhandle.tcpcli_llhandle._ch_rep_ool_data_p
#define ch_ref_cnt ch_llhandle.tcpcli_llhandle._ch_ref_cnt
#define ch_llflag ch_llhandle.tcpcli_llhandle._ch_llflag
#define ch_lllock ch_llhandle.tcpcli_llhandle._ch_lllock
#define ch_msghdr ch_llhandle.tcpcli_llhandle._ch_msghdr
#define ch_iovec ch_llhandle.tcpcli_llhandle._ch_iovec
#define ch_ooldesc ch_llhandle.tcpcli_llhandle._ch_ooldesc
#define ch_inline ch_llhandle.tcpcli_llhandle._ch_inline

/*
 * Definitions for ch_llflag in low-level server handle.
 */
#define CLI_HANDLE_REPLY_DONE	0x01	/* Reply has been processed. */

/*
 * Structure for the portion of the server handle that pertains to the
 * low-level ICS implementation.
 */
typedef struct tcp_svr_llhandle {

	/*
	 * Track information about the incoming message to the server.
	 */
	struct sk_buff	*_sh_msg_skb_p;
	struct sk_buff	*_sh_msg_tskb_p;

	/*
	 * Track information while decoding in-line data within the
	 * incoming message.
	 */
	struct sk_buff	*_sh_msg_inline_skb_p;
	u_char		*_sh_msg_inline_data_p;

	/*
	 * Track information while decoding OOL data within the
	 * incoming message.
	 */
	struct sk_buff	*_sh_msg_ool_skb_p;
	u_char		*_sh_msg_ool_data_p;

	/*
	 * Message block for the response associated with the handle. Note that
	 * the sk_buff is there when an incoming message is received, even if
	 * the incoming message requires no response. For cases where no
	 * response is sent, the sk_buff hangs around and will be used by a
	 * subsequent incoming message for this handle that does require a
	 * response.
	 */
	struct msghdr	*_sh_rep_msghdr_p;

#ifdef ICS_TCP_SENDPAGE
	/*
	 * Pointer to array of contiguous pages and length of page data.
	 */
	struct page	**_sh_rep_pagevec;
	u_int		_sh_rep_page_len;
#endif

	/*
	 * Information for tracking the in-line portion of the response.
	 * Note that the sk_buff includes the actual icshdr data in addition
	 * to the inline data.
	 */
	struct iovec	*_sh_rep_inline_iov_p;
	u_char		*_sh_rep_inline_data_p;

	/*
	 * Information for tracking the OOL buffers of the response.
	 */
	ooldesc_t	*_sh_rep_ooldesc;
	int		_sh_rep_ooldesc_cnt;

	/*
	 * Reference count for OOL data segments with their own free
	 * routines (plus one for the in-line data segment).
	 */
	u_int		_sh_ref_cnt;

	/*
	 * Lock for sh_ref_cnt.
	 */
	SPIN_LOCK_T	_sh_lllock;

	/* msghdr, inline data, and default iovec and ooldesc. */
	struct msghdr	_sh_msghdr;
	struct iovec	_sh_iovec[ICS_DEF_OOL_SEGMENTS + 1];
	ooldesc_t	_sh_ooldesc[ICS_DEF_OOL_SEGMENTS];
	u_char 		_sh_inline[sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE];
} tcp_svr_llhandle_t;

#define sh_msg_skb_p sh_llhandle.tcpsvr_llhandle._sh_msg_skb_p
#define sh_msg_tskb_p sh_llhandle.tcpsvr_llhandle._sh_msg_tskb_p
#define sh_msg_inline_skb_p sh_llhandle.tcpsvr_llhandle._sh_msg_inline_skb_p
#define sh_msg_inline_data_p sh_llhandle.tcpsvr_llhandle._sh_msg_inline_data_p
#define sh_msg_ool_skb_p sh_llhandle.tcpsvr_llhandle._sh_msg_ool_skb_p
#define sh_msg_ool_data_p sh_llhandle.tcpsvr_llhandle._sh_msg_ool_data_p
#define sh_rep_msghdr_p sh_llhandle.tcpsvr_llhandle._sh_rep_msghdr_p
#ifdef ICS_TCP_SENDPAGE
#define sh_rep_pagevec sh_llhandle.tcpsvr_llhandle._sh_rep_pagevec
#define sh_rep_page_len sh_llhandle.tcpsvr_llhandle._sh_rep_page_len
#endif
#define sh_rep_inline_iov_p sh_llhandle.tcpsvr_llhandle._sh_rep_inline_iov_p
#define sh_rep_inline_data_p sh_llhandle.tcpsvr_llhandle._sh_rep_inline_data_p
#define sh_rep_ooldesc sh_llhandle.tcpsvr_llhandle._sh_rep_ooldesc
#define sh_rep_ooldesc_cnt sh_llhandle.tcpsvr_llhandle._sh_rep_ooldesc_cnt
#define sh_ref_cnt sh_llhandle.tcpsvr_llhandle._sh_ref_cnt
#define sh_lllock sh_llhandle.tcpsvr_llhandle._sh_lllock
#define sh_msghdr sh_llhandle.tcpsvr_llhandle._sh_msghdr
#define sh_iovec sh_llhandle.tcpsvr_llhandle._sh_iovec
#define sh_ooldesc sh_llhandle.tcpsvr_llhandle._sh_ooldesc
#define sh_inline sh_llhandle.tcpsvr_llhandle._sh_inline

#endif /* !_CLUSTER_ICS_ICS_TCP_SOCK_H */
