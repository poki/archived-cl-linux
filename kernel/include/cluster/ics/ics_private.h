/*
 * 	High-level ICS private header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Portions Copyright 2007 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains definitions that are private to NSC ICS. It should
 * be included only by files that are part of the ICS implementation.
 */
#ifndef	_CLUSTER_ICS_ICS_PRIVATE_H
#define	_CLUSTER_ICS_ICS_PRIVATE_H

#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/socket.h>
#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>
#include <cluster/ics.h>

/*
 * Use appropriate ifdef's below once multiple transports are supported, to
 * only suck in the appropriate transport's .h file.
 *
 * The important things to be imported are definitions for:
 *		cli_llhandle_t
 *		svr_llhandle_t
 *		ics_llnodeinfo_t
 */
#include <cluster/ics/ics_tcp_sock_private.h>
#include <cluster/ics/ics_tcp_sock.h>

/*
 * General stuff.
 */
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/*
 * Structure for the ICS client handle.
 */

typedef union {
		tcp_cli_llhandle_t	tcpcli_llhandle;
} cli_llhandle_t;

typedef struct cli_handle {
	/*
	 * Flag indicating status of client handle.
	 */
	int		ch_flag;

	/*
	 * Service this handle is being used for.
	 */
	int		ch_service;

	/*
	 * Linked list of handles.
	 */
	struct cli_handle *ch_next;
	struct cli_handle *ch_prev;

	/*
	 * The target node.
	 */
	clusternode_t	ch_node;

	/*
	 * The magic cookie passed to the server (procedure number).
	 */
	int		ch_procnum;

	/*
	 * Information stored in handle so that the callback to
	 * icscli_send() can occur.
	 */
	void		(*ch_callback)(
				struct cli_handle *handle,
				long		callarg);
	long		ch_callarg;

	/*
	 * Status returned for icscli_send() callback.
	 */
	int		ch_status;

	/*
	 * Transaction ID used to identify an operation using this handle.
	 */
	long		ch_transid;

#ifdef NSC_ICSLOG
	/*
	 * Unique ID used to further identify operations for logging.
	 */
	int		ch_uniqueid;
#endif

	/*
	 * Communication channel number of the outgoing message/RPC.
	 */
	int		ch_chan;

	/*
	 * Event structure that is passed back to high-level ICS code.
	 */
	INTR_EVENT_T		ch_event;

	/*
	 * Client proc that is associated with this handle (if there is one).
	 */
	struct task_struct	*ch_procp;

#ifdef NSC_INHERIT_USER_NICE
	/*
	 * User nice level associated with the client proc.
	 */
	long		ch_user_nice;
#endif

	/*
	 * Private portion of client handle.
	 */
	cli_llhandle_t		ch_llhandle;
} cli_handle_t;
/*
 * #define tcp_cli_llhandle     ch_llhandle.tcpcli_llhandle
 * #define lti_cli_llhandle     ch_llhandle.lticli_llhandle
 */

/* Values allowed for ch_flag field. */
#define	CLI_HANDLE_REPLY_WAITING 0x0001		/* Reply has arrived */
#define CLI_HANDLE_NO_REPLY	 0x0002		/* Not an RPC, but an IPC */
#define CLI_HANDLE_NO_BLOCK	 0x0004		/* Non-blocking message */
#define CLI_HANDLE_RPC_SENT	 0x0008		/* RPC was sent */

/*
 * Structure for the ICS server handle.
 */
typedef union {
		tcp_svr_llhandle_t	tcpsvr_llhandle;
} svr_llhandle_t;

typedef struct svr_handle {

	/*
	 * Service to which this handle is assigned.
	 */
	int		sh_service;

	/*
	 * Linked list of server handles that are either waiting for incoming
	 * messages for this service or are processing messages for this
	 * service.
	 */
	struct svr_handle *sh_next;
	struct svr_handle *sh_prev;

	/*
	 * Information stored in handle so that callback to icssvr_recv()
	 * and icssvr_reply() can occur.
	 */
	union {
		void		(*_sh_callback_recv)(
					struct svr_handle *handle,
					long		callarg);
		void		(*_sh_callback_reply)(
					struct svr_handle *handle,
					long		callarg);
	} sh_callback;
	long		sh_callarg;

	/*
	 * Node that performed the icscli_send() call.
	 */
	clusternode_t	sh_node;

	/*
	 * Transaction ID that was assigned to an operation on the client
	 * node (i.e. equal to ch_transid for the client handle that
	 * performed the icscli_send() operation).
	 */
	long		sh_transid;

#ifdef NSC_ICSLOG
	/*
	 * Unique ID used to further identify operations for logging.
	 */
	int		sh_uniqueid;
#endif

	/*
	 * Procedure number corresponding to the message for this handle.
	 */
	int		sh_procnum;

	/*
	 * Value of the flag parameter when the client performed the
	 * call to icscli_send().
	 */
	int		sh_flag;

	/*
	 * Communications channel that was used to send in the message by
	 * the client.
	 */
	int		sh_chan;

	/*
	 * Permanent ICS priority of this handle. A value > 0 indicates the
	 * handle is one of the special handles used only for priority
	 * messages (handles with a permanent priority of 0 can be used for
	 * priority and non-priority messages).
	 */
	int		sh_perm_prio;

	/*
	 * Server proc that is associated with this handle (if there is one).
	 */
	struct task_struct	*sh_procp;

#ifdef NSC_INHERIT_USER_NICE
	/*
	 * User nice level associated with the client proc.
	 */
	long		sh_user_nice;
#endif

	/*
	 * Private portion of server handle.
	 */
	svr_llhandle_t	sh_llhandle;
} svr_handle_t;
/*
 * #define tcp_svr_llhandle     sh_llhandle.tcpsvr_llhandle
 * #define lti_svr_llhandle     sh_llhandle.ltisvr_llhandle
 */

#define sh_callback_recv sh_callback._sh_callback_recv
#define sh_callback_reply sh_callback._sh_callback_reply

/* Values allowed for sh_flag field. */
#define SVR_HANDLE_NO_REPLY	0x0001		/* Not an RPC, but an IPC */
#define SVR_HANDLE_CRIT_MEM	0x0004		/* critical memory priority */

/*
 * Information kept about each ICS service.
 */
typedef struct icsservice {
	int	icssvc_ops_cnt[ICS_MAX_SUBSERVICES];	/* num ops in subsvc */
	int	(**icssvc_ops_tbl[ICS_MAX_SUBSERVICES])(svr_handle_t *);
	int	icssvc_flowpages_lwm;		/* LWM for throttling server */
	int	icssvc_flowpages_hwm;		/* HWM for throttling server */
} icsservice_t;

extern icsservice_t	icsservice[];

/*
 * List of client handles with outstanding RPCs.
 */

#define CLIHAND_ARRAYSZ		32	/* Must be power of two!!! */
#define CLIHAND_HASH(_node)	((_node) & (CLIHAND_ARRAYSZ-1))

typedef struct {
	SPIN_LOCK_T		chlist_lock;
	cli_handle_t		*chlist_first;
} clihand_list_t;

extern clihand_list_t 	clihand_list[];

/*
 * List of server handles with outstanding RPCs.
 */

#define SVRHAND_ARRAYSZ		32	/* Must be power of two!!! */
#define SVRHAND_HASH(_node)	((_node) & (SVRHAND_ARRAYSZ-1))

typedef struct {
	SPIN_LOCK_T		shlist_lock;
	svr_handle_t		*shlist_first;
} svrhand_list_t;

extern svrhand_list_t 	svrhand_list[];

/* Nanny wait event. */

extern EVENT_T icssvr_nanny_event;

/*
 * The ics_nodestatus[] array is used by the upper-level ICS code to track
 * the node status of all nodes in the cluster.
 */

extern int				ics_nodestatus[];

#define ICS_NODE_NEVER_UP		0x0000
#define ICS_NODE_GOING_DOWN		0x0001
#define	ICS_NODE_DOWN			0x0002
#define ICS_NODE_SETICSINFO_DONE	0x0004
#define ICS_NODE_COMING_UP		0x0008
#define ICS_NODE_UP			0x0010
#define ICS_NODE_READY			0x0020
#define ICS_NODE_INVALID		0x8000

#define ICS_STATUS_RETRY_DELAY		50	/* ticks */

extern COND_LOCK_T			ics_nodestatus_lock;
extern CONDITION_T			ics_nodestatus_cond;

/*
 * Prototypes for internal high-level ICS interfaces.
 */

void
icscli_init(void);

void
icssvr_init(void);

void
icssvr_mgmt_earlylock_init(void);

void
icssvr_mgmt_init(void);

void
icssvr_mgmt_nodedown(
	clusternode_t	node);

void
icssvr_svcmgmt_init(
	int		service,
	int		handle_lwm,
	int		handle_hwm);

/*
 * Prototypes for "call-ups" from ICS low-level code to ICS high level code.
 */

void
ics_nodeup_notification(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p);

void
ics_nodedown_notification(
	clusternode_t	node);

cli_handle_t *
icscli_find_transid_handle(
	long		transid);

void
icscli_sendup_reply(
	cli_handle_t	*handle);

svr_handle_t *
icssvr_find_recv_handle(
	int		chan,
	int		svc,
	int		flags);

void
icssvr_sendup_msg(
	svr_handle_t	*handle);

void
icssvr_sendup_replydone(
	svr_handle_t	*handle,
	int		status);

/*
 * Prototypes for interfaces between the ICS high-level code and
 * the ICS low-level code.
 */

void
ics_llinit(void);

void
ics_llaccept_notifications(void);

void
ics_llconnect_master(void);

int
ics_llgeticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p);

int
ics_llseticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p);

int
ics_llnodeup(
	clusternode_t	node);

void
ics_llnodedown(
	clusternode_t	node);

void
ics_llnodedown_async_start(
	clusternode_t	node);

void
ics_llnodedown_async_end(
	clusternode_t	node);

int
ics_llshoot_node(
	clusternode_t	remote_node);

int
ics_llisicsport(
	unsigned short	port);

int
ics_llisicsunackport(
	unsigned short	port);

int
ics_llget_nodenumsize(void);

int
ics_llget_addrlen(void);

int
ics_llprobe_clms(
	clusternode_t		remote_node,
	icsinfo_t		remote_addr,
	clusternode_t		*master_node,
	icsinfo_t		*master_addr,
	int			im_a_secondary,
	int			im_a_member);

int
ics_llunack_send(
	clusternode_t node,
	u_int type,
	void *message,
	size_t length);
void
print_icscli_llhandle(cli_llhandle_t *lh);

void
print_icssvr_llhandle(svr_llhandle_t *lh);

/*
 * Low-level ICS client-side routines.
 */

void
icscli_llinit(void);

int
icscli_llhandle_init(
	cli_handle_t	*handle,
	boolean_t	sleep_flag);

int
icscli_llwouldthrottle(
	clusternode_t	node,
	int		chan);

int
icscli_llwaitfornothrottle(
	clusternode_t	node,
	int		chan);

int
icscli_llsend(
	cli_handle_t	*handle);

void
icscli_llhandle_deinit(
	cli_handle_t	*handle);

/*
 * Low-level ICS client-side data marshalling routines.
 */

void
icscli_llencode_int64(
	cli_handle_t	*handle,
	int64		int64);

void
icscli_lldecode_int64(
	cli_handle_t	*handle,
	int64		*int64);

void
icscli_llencode_int64_array_t(
	cli_handle_t	*handle,
	int64		int64_array[],
	int		nelem);

void
icscli_lldecode_int64_array_t(
	cli_handle_t	*handle,
	int64		int64_array[],
	int		nelem);

void
icscli_llencode_int32(
	cli_handle_t	*handle,
	int32		int32);

void
icscli_lldecode_int32(
	cli_handle_t	*handle,
	int32		*int32);

void
icscli_llencode_int32_array_t(
	cli_handle_t	*handle,
	int32		int32_array[],
	int		nelem);

void
icscli_lldecode_int32_array_t(
	cli_handle_t	*handle,
	int32		int32_array[],
	int		nelem);

void
icscli_llencode_char_array_t(
	cli_handle_t	*handle,
	char		char_array[],
	int		nelem);

void
icscli_lldecode_char_array_t(
	cli_handle_t	*handle,
	char		char_array[],
	int		nelem);

int
icscli_llencode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*data,
	long		len,
	void		(*callback)(
				u_char	*data,
				long	callarg),
	long		callarg);

int
icscli_llencoderesp_ool_data_t(
	cli_handle_t	*handle,
	u_char		*data,
	long		len);

int
icscli_lldecode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*data,
	long		len);

/*
 * Low-level ICS server-side routines.
 */

void
icssvr_llinit(void);

int
icssvr_llhandle_init(
	svr_handle_t	*handle,
	boolean_t	sleep_flag);

int
icssvr_llhandle_init_for_recv(
	svr_handle_t	*handle,
	boolean_t	sleep_flag);

void
icssvr_llhandles_present(
	int		chan);

void
icssvr_lldecode_done(
	svr_handle_t	*handle);

void
icssvr_llrewind(
	svr_handle_t	*handle);

void
icssvr_llreply(
	svr_handle_t	*handle);

void
icssvr_llhandle_deinit(
	svr_handle_t	*handle);

/*
 * Low-level ICS server-side data marshalling routines.
 */

int
icssvr_lldecode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*data,
	long		len);

int
icssvr_llencode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*data,
	long		len,
	void		(*callback)(
				u_char	*data,
				long	callarg),
	long		callarg);

/*
 * Default value low-water-mark and high-water-mark for the throttling control
 * in the ICS client and server. If this value is chosen, then the algorithms
 * for controlling throttling will make use of DESIRED_FREEPAGES to make
 * their decision.
 */
#define DEFAULT_FREEPAGES_LWM	(-1)
#define DEFAULT_FREEPAGES_HWM	(-1)

#ifdef NSC_ICSLOG
/*
 * Support for logging ICS activity
 */
extern int do_icslog;
extern void ics_log(int level, long type, long a, long b, long c, long d,
		    long e, long f);
#define ICSLOG_DEPTH	1024	/* Used to be set to 5000, which is too large
				 * for kmalloc() to handle.
				 */

extern const char *icslog_format[];

/*
 * Log message types:
 *	- these function as the index into the icslog_format[] array
 */
#define ICSLOG_SVRFUNC		0
#define ICSLOG_SVRRPLY		1
#define ICSLOG_SVRCLBK		2
#define ICSLOG_CLISEND		3
#define ICSLOG_CLICLBK		4
#define ICSLOG_CLIWKUP		5
#define ICSLOG_SVRNCBK		6
#define ICSLOG_SVRDOWN		7
#define ICSLOG_LLICSOFF		8
#define ICSLOG_NUM_MSGS		25

#endif /* NSC_ICSLOG */

#endif /* !_CLUSTER_ICS_ICS_PRIVATE_H */
