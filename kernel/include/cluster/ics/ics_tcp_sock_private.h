/*
 * 	Low-level ICS private header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Portions Copyright 2007 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * Header file used internally by the NSC ICS low-level code.
 */

#ifndef _CLUSTER_ICS_ICS_TCP_SOCK_PRIVATE_H
#define _CLUSTER_ICS_ICS_TCP_SOCK_PRIVATE_H

#include <linux/skbuff.h>
#include <asm/uaccess.h>

#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>

#define ICS_NODENUMSIZE 2  /* node number when represented as a string */

/*
 * Communication channels used by low-level ICS. This includes all the
 * high-level ICS channels plus reply channels.
 */
#define ICS_REPLY_CHAN		ics_reply_chan
#define ICS_REPLY_PRIO_CHAN	ics_reply_prio_chan

#define ICS_LL_NUM_CHANNELS	(ics_num_channels+2)
#define ICS_MAX_LL_CHANNELS	(ICS_MAX_CHANNELS+2)

/*
 * Port numbers for NSC services and ICS_PROBE_CLMS channel.
 * These port numbers are chosen based on an RFC which lists the
 * reserved port numbers. Currently, the port numbers start from
 * 900.  But, 911 is reserved for PPP and hence it is skipped.
 * Different numbers may be chosen, if desired.  The port numbers
 * have to be less than 1024. This is because most systems permit
 * the use of port numbers less than 1024 only if the process is
 * privileged.
 *
 * WARNING: If any of the port numbers used here is assigned
 * to any other TCP service, we need to search for another port
 * number.
 *
 * All registered NSC services are connected to port 900.
 * The TCP connections established are guaranteed to be unique because
 * the client side chooses an ephemeral to connect from for each service.
 *
 */
#define ICS_SVC_PORT	900	/* port for nsc svc */
/*
 * Port 901 is used by swat (Samba Web Administration Tool)
 */
#define ICS_PROBE_CLMS_PORT	902	/* port to listen for clms probes */
#define PPP_PORT	911	/* ppp port - see /etc/services */
#define ICS_CLI_PORT	3000	/* port for the first registered nsc client */

extern int	ics_service_ports[];
extern int	ics_client_ports[];

/*
 * Definitions neede to configure the UDP endpoints.
 */
#define ICS_UNACK_SVR_PORT	901	/* port for receiving
					   unacknowledged ICS messages */
#define ICS_UNACK_CLI_PORT	902	/* port for sending
					   unacknowledged ICS messages */

#define NSC_ICS_MAX_ROUTES	32
extern int ics_route_count;
extern char *ics_routes[];

/*
 * ICS unacknowledged message data structures.
 * This is cribbed from the LTI version, although for now, most fields
 * are "don't care"s.
 */

/*
 * The total number of bits in the ics_service field for each subfield.  These
 * should add up to 32.
 */
#define ICS_MTYPE_BITS		4
#define ICS_REQFRAG_BITS	1
#define ICS_REPFRAG_BITS	1
#define ICS_MLENGTH_BITS	26

/*
 * ICS unacknowledged message data structure.
 */
typedef struct {
	/*
	 * For all messages, this is the version of the ICS message.  This is
	 * set from the ICS_MESSAGE_VERSION definition.  Any message not
	 * carrying this message type is dropped.
	 */
	struct {
		u_int	uicsh_ver_type: ICS_MTYPE_BITS;
		u_int	uicsh_ver: (32-ICS_MTYPE_BITS);
	} uicsh_version;
	/*
	 * Type of request message that this is.
	 */
	u_int		uicsh_type;
	/*
	 * For ICS_NORMAL_DATA messages, this is the procedure number for
	 * the service.  For all current unacknowledge messages, this
	 * field is meaningless.
	 */
	int		uicsh_procnum;
	/*
	 * For ICS_NORMAL_DATA messages, this is the service to be invoked.
	 * For all current unacknowledged messages, this field is meaningless.
	 */
	int		uicsh_service;
	/*
	 * For ICS_NORMAL_DATA messages, this associates a client transaction
	 * with a server handle.  For all current unacknowledged messages,
	 * this field is meaningless.
	 */
	long		uicsh_transid;
	/*
	 * For ICS_NORMAL_DATA messages, this is the ICS channel that the
	 * message is being sent to.  For all current unacknowledged messages,
	 * this field is meaningless.
	 */
	int		uicsh_chan;
	/*
	 * This the node number of the sender.
	 */
	clusternode_t	uicsh_sender;
} uics_reqheader;

/*
 * Maximum size of an ICS unacknowledged datagram.
 */
#define ICS_UNACK_MAX_DGRAM_SIZE	2048

/*
 * Definitions for uics_reqheader.uicsh_type
 */
#define ICS_UNACK_IMALIVE	0x01
#define ICS_UNACK_LOAD		0x02
#define ICS_UNACK_SHOOT_NODE	0x04

/*
 * Definitions needed to configure the TCP connections.
 */
#define ICS_IPADDRLEN 16
#define ICS_MAX_IFNAME_SIZE 16

#define ICS_LOOPIFNAME "lo"
#define ICS_LOOPIPADDR "127.0.0.1"
#define ICS_LOOPNETMASK "255.0.0.0"

#define ICS_ROUTE_PARAMS 3

extern char ics_ifdevname[];
extern char ics_ifname[];
extern char ics_netaddr[];
extern char ics_netmask[];

extern int icslog;

/*
 * Header that is placed at the front of every ICS message. This includes both
 * sends and responses.
 */
typedef struct icshdr {
	unsigned int	ihdr_marker:32;
	unsigned int	ihdr_len:32;
	unsigned int	ihdr_version:16;
	unsigned int	ihdr_flag:16;
	unsigned int	ihdr_service:32;
	unsigned int	ihdr_procnum:16;
	unsigned int	ihdr_inline_len:16;
	long		ihdr_transid;
	unsigned int	ihdr_orignode:32;
	unsigned int	ihdr_chan:16;
#ifdef NSC_ICSLOG
	unsigned int	ihdr_uniqueid:16;
#elif defined(NSC_INHERIT_USER_NICE)
        /* FIXME NSC_ICSLOG breaks this.
         * nice value is type long in Linux.
	 */
	int		ihdr_user_nice:16;
#else
	unsigned int	ihdr_unused:16;
#endif
} icshdr_t;

#define ICSHDR_MARKER	0xffffffff	/* only value for ihdr_marker */

extern int 	ics_message_version;

/*
 * The following structure is used to reassemble the ICS messages from
 * transport datagrams. One such structure is kept for each incoming
 * channel (i.e. one per node per service). The ICS message is maintained
 * as a simple linked list of sk_buffs. We keep pointers to the head and
 * tail of the list.
 */
struct ics_reass_buf {
	struct sk_buff	*rb_hskbp;	/* head sk_buff of the ICS message */
	u_char		*rb_hdatap;	/* start of message in head */
	struct sk_buff	*rb_tskbp;	/* tail sk_buff of the ICS message */
	u_char		*rb_tdatap;	/* start of next message in tail */
	struct sk_buff	*rb_rskbp;	/* pointer to read-skb */
	u_char		*rb_rdatap;	/* data pointer into read-skb */
	icshdr_t	rb_icshdr;	/* ICS header */
	unsigned int	rb_len;		/* total length of skbs */
};

/*
 * The ics_llnodeinfo[] array is used by the low-level ICS code to track
 * information about the status of connections to all nodes in the cluster,
 * plus information about whether the node is up or down.
 *
 * The information is used for both incoming datagrams and for outgoing
 * messages.
 *
 * For incoming datagrams the ics_sock_conn_data structure is the socket
 * private data structure (i.e. it is pointed to by sock->user_data).
 *
 * For outgoing messages, the ics_sock_conn_data structure is used to
 * identify the connection to be used to send out the data.
 */

/* Lock to serialize connection attempts. */
extern LOCK_T			ics_llconn_lock;

extern SPIN_LOCK_T		ics_llnodeinfo_lock;

typedef struct ics_sock_conn_data {
	clusternode_t	   	conn_node;	/* node */
	int		   	conn_chan;	/* communication channel */
	struct ics_reass_buf	conn_rbuf;	/* reassembly buf (incoming) */
	struct socket		*conn_sock;	/* transport socket */
	ATOMIC_INT_T		conn_ref_cnt;   /* Number of processes */
						/* throttled, sleeping in */
						/* stp->sd_write */
	LOCK_T			conn_lock;	/* channel serialization lock */
	void			*conn_snd_rand_statep;
	void			*conn_rcv_rand_statep;
						/* /dev/random support */
} ics_sock_conn_data_t;

typedef struct {
	char			*icsni_node_addr;
	ics_sock_conn_data_t	**icsni_conn;
	short			icsni_flag;
	short			icsni_ref_cnt;
	RW_SPIN_LOCK_T		icsni_lock;
	EVENT_T			icsni_event;
	EVENT_T			icsni_chan_event;
} ics_llnodeinfo_t;

extern ics_llnodeinfo_t		*ics_llnodeinfo[];

/* Flag value for icsni_flag. */
#define ICSNI_GOING_DOWN	0x1
#define ICSNI_NODE_DOWN		0x2
#define ICSNI_COMING_UP		0x4
#define ICSNI_NODE_UP		0x8

typedef struct ics_clmsprobe_req {
	clusternode_t	my_node;
	icsinfo_t	my_icsinfo;
	int		imasecondary;
	int		imamember;
} ics_clmsprobe_req_t;

typedef struct ics_clmsprobe_rep {
	/*
	 * The CLMS master node.
	 */
	clusternode_t	cp_master;
	icsinfo_t	cp_icsinfo;
	int		error;
} ics_clmsprobe_rep_t;

/*
 * The definition of the ICS_SHOOT_NODE message data.
 */
typedef struct {
        int             my_ics_version;
        int             i_am_cluster;
} ics_shoot_node_t;

#ifdef NSC_ICSLOG

#define		ICSLOG(level, msgnum, a, b, c, d, e, f)			\
			do {						\
				if (icslog)				\
					ics_log((level), (msgnum),	\
						(a), (b), (c), (d),	\
						(e), (f));		\
			} while(0)

/*
 * Message numbers; these correspond to entries in the icslog_msgs array in
 * ics_llsubr.c (they are indices into the array).
 */
#define	ICSLOG_CLILLSEND	(ICSLOG_LLICSOFF + 0)
#define ICSLOG_SVRLLSEND	(ICSLOG_LLICSOFF + 1)
#define ICSLOG_DGRAMMSGSMALL	(ICSLOG_LLICSOFF + 2)
#define ICSLOG_DGRAMMSGHDRL	(ICSLOG_LLICSOFF + 3)
#define ICSLOG_DGRAMMSGINC	(ICSLOG_LLICSOFF + 4)
#define ICSLOG_DGRAMMULTMSG	(ICSLOG_LLICSOFF + 5)
#define ICSLOG_DGRAMUPCALL	(ICSLOG_LLICSOFF + 6)
#define ICSLOG_SVRLLND		(ICSLOG_LLICSOFF + 7)
#define ICSLOG_SVRLLEQ		(ICSLOG_LLICSOFF + 8)
#define ICSLOG_SVRLLDQ		(ICSLOG_LLICSOFF + 9)
#define ICSLOG_CLILLSENDUP	(ICSLOG_LLICSOFF + 10)
#define ICSLOG_SVRLLPROCMSG	(ICSLOG_LLICSOFF + 11)
#define ICSLOG_SVRLLNDDROP	(ICSLOG_LLICSOFF + 12)

extern int ics_uniqueid;
extern spinlock_t ics_uniqueid_spin_lock;

#else

/*
#define		ICSLOG(A, B, C)
*/
#define		ICSLOG(level, msgnum, a, b, c, d, e, f)

#endif

/*
 * Internal ICS subroutines.
 */
void icscli_llnodedown(clusternode_t);

void icscli_llsendup_reply(struct ics_reass_buf *);
void icssvr_llsendup_msg(struct ics_reass_buf *);

void ics_lltransport_config(void);

int ics_skb_bcopy(struct sk_buff **, u_char **, u_char *, long);

void ics_llnode_to_icsinfo(icsinfo_t *, clusternode_t);

#ifdef ICS_TCP_SENDPAGE
extern int ics_llsendmsg(ics_sock_conn_data_t *, struct msghdr *, int,
			 struct page **, unsigned);
#else
extern int ics_llsendmsg(ics_sock_conn_data_t *, struct msghdr *, int);
#endif

extern void icssvr_llnodedown(clusternode_t, int);

extern int ics_dgram_parse(struct ics_reass_buf *, int, clusternode_t);

extern void __ics_llfreemsg(struct sk_buff *, struct sk_buff *);

static inline void ics_llfreemsg(struct sk_buff **hpp, struct sk_buff *tp)
{
	if (*hpp != NULL) {
		__ics_llfreemsg(*hpp, tp);
		*hpp = NULL;
	}
}

#endif /* !_CLUSTER_ICS_ICS_TCP_SOCK_PRIVATE_H */
