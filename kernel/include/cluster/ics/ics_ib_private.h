/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
 */

/*
 * 	Low-level ICS private header file.
 *	Copyright 2001 Compaq Computer Corporation
 *	Copyright 2006 Intel Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * Header file used internally by the NSC ICS low-level Infiniband (IB) code.
 */

#ifndef _CLUSTER_ICS_ICS_IB_PRIVATE_H
#define _CLUSTER_ICS_ICS_IB_PRIVATE_H

#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/compiler.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/string.h>
#include <linux/stat.h>
#include <linux/errno.h>
#include <linux/dma-mapping.h>

#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <asm/system.h>
#include <asm/errno.h>
#include <asm/semaphore.h>

#include <linux/uio.h>
#include <linux/time.h>
#include <linux/workqueue.h>

#include <linux/fs.h>
#include <linux/file.h>
#include <linux/proc_fs.h>
#include <linux/stat.h>
#include <linux/list.h>

#include <asm/segment.h>

#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>

/* Infiniband (RDMA) Access Layer */
#include <rdma/rdma_cm_ib.h>
#include <rdma/ib_cm.h>
#include <rdma/ib_pack.h>
#include <rdma/ib_mad.h>
#include <rdma/ib_cache.h>

/*
 * total number of pre-posted Infiniband receive work-requests per connection.
 */
#if MUX_ICS_CHANNELS
#define IBA_RECV_WR	(ICS_MAX_LL_CHANNELS+2)
#else
#define IBA_RECV_WR	10
#endif

/*
 * global RX pool element count: starting element count; when growing, grow
 * by this amount.
 */
#define RX_GPOOL_ELEM_CNT 6

/*
 * When a connection's posted RX buffer count is <= this amount, then start
 * cloning received buffers and repost recv buffer immediately. Plan is to
 * ALWAYS have a posted RX buffer so no messages are dropped. FLW.
 * Also the global RX pool replenish threshold: <= this value, start an RX pool
 * grow operation.
 */
#define CONN_RX_BUF_LO_WATER 3

/*
 * total number of blocking Infiniband send work-requests allocated per
 * connection, otherwise refered to as Blocking TX descriptors. TX descriptors
 * are pooled for all IB connections, each new connection adds this amt to the
 * global pool.
 */
#if MUX_ICS_CHANNELS
#define IBA_SEND_WR 8
#else
#define IBA_SEND_WR 8
#endif

/* Number of NON-blocking TX descriptors to be allocated per connection. */
#if MUX_ICS_CHANNELS
#define MAX_NB_WR 2
#else
#define MAX_NB_WR 1
#endif

/*
 * Max IB message payload size in bytes.
 * WARNING: pages are allocated based on this value; as in
 *	get_order(IBA_MAX_BUF_SIZE).
 * If an ics message payload byte count is > IBA_MAX_BUF_SIZE, then RDMA.read
 * transfers are used to retrieve the OOL data. The smaller this value the more
 * RDMA.read ops invoked. 68KB will eliminate any RDMA operations, where 4KB
 * will generate hundreds.
 */
#define IBA_MAX_BUF_SIZE (8*1024)

// per IB connection: shared send & receive Completion Queue Depth
#define IBA_CQ_DEPTH	(IBA_SEND_WR+IBA_RECV_WR+5)

/*
 * max # of CQ events to collect in a single poll, see ib_completion_cb().
 */
#define MAX_CQ_WC 12

/* max # of data segments per IB work-request (both send & recv) */

#define MAX_SGL_ENTRIES         2

#if MUX_ICS_CHANNELS
#define MAX_PENDING_RDMA	3	// per connection
#else
#define MAX_PENDING_RDMA	4	// per connection.
#endif

#define SUCCESS (0)
#define NO_STATUS (~0)	// no status yet, initialization for wait events.

#define	RDMA_TIMEOUT 12	/* RDMA operation will timeout after this number of
			 * seconds if the round-trip RDMA status has not been
			 * returned.
			 */

#define	DO_TIME 0	// 1 enables RDMA timings
#define	CONN_RESTART 0	// 1 enables connection re-establishment on error.

/*
 * Debug support
 */
#define ME "<ics_ib>"
#define WHOAMI ME,__func__

extern u32 ics_ib_debug;

// debug bits in ics_ib_debug
#define DBG_NONE		(0)
#define	DBG_CONN		(1<<0)	// connection setup
#define	DBG_CONN_V		(1<<1)	// connection setup Verbose
#define	DBG_DCONN		(1<<2)  // connection disconnect
#define	DBG_DCONN_V		(1<<3)
#define	DBG_SEND		(1<<4)
#define	DBG_SEND_V		(1<<5)	// send verbose
#define	DBG_ICS			(1<<6)
#define	DBG_RDMA		(1<<7)
#define	DBG_RECV		(1<<8)
#define	DBG_RECV_V		(1<<9)	// recv verbose
#define	DBG_ALLOC		(1<<10)
#define	DBG_INTR		(1<<11)
#define	DBG_INTR_V		(1<<12)
#define	DBG_SHUTDOWN		(1<<13)
#define	DBG_STARTUP		(1<<14)
#define	DBG_STATS		(1<<15)
#define	DBG_TIME_SEND		(1<<16)
#define	DBG_TIME_SEND_PAGES	(1<<17)
#define	DBG_TIME_RECV		(1<<18)
#define DBG_ALL			(~0)

/* to see entire connection life, set CONN & DCONN */
//#define DBG_SET (DBG_NONE)
//#define DBG_SET (DBG_INTR|DBG_SEND|DBG_RECV)

#define DBG_SET (DBG_NONE)

#define _PRODUCTION 0

#ifdef Dprintk
#undef Dprintk
#endif
#ifdef dprintk
#undef dprintk
#endif

#if _PRODUCTION == 0 // _DEBUG_

#define	dprintk(a,b) if ( ics_ib_debug & (a) ) printk b
#define	_dprintk(a,b) printk b	// quick hack to enable message output

#define	Dprintk(a,b) printk b	// quick hack to ALWAYS enable message output

#define	Xdprintk(a,b) 	// quick hack to ALWAYS disable message output

#define FENTER printk("%s Enter\n",__func__)
#define FEXIT printk("%s Exit\n",__func__)

#define DBG_MAGIC 0xDEADBEEF

#define BAD_MAGIC(a) \
	(unlikely((a) == NULL) || (unlikely(((a)->magic != DBG_MAGIC))))
#define SET_MAGIC(a) (a)->magic = DBG_MAGIC
#define CLEAR_MAGIC(a) (a)->magic = 0

#else	// ! _DEBUG_, _PRODUCTION == 1

#define	dprintk(a,b)
#define	_dprintk(a,b)

#define	Dprintk(a,b)

#define	Xdprintk(a,b)

#define DBG_MAGIC 0xDEADBEEF

#define BAD_MAGIC(a) (0)
#define SET_MAGIC(a) 
#define CLEAR_MAGIC(a)

#endif	// ! _DEBUG_

#define CWARN(format, a...) printk("%s %s@line %d: " format, \
					__FILE__, __FUNCTION__, __LINE__, ## a)

#define CERROR(format, a...) printk("%s %s@line %d: " format, \
					__FILE__, __FUNCTION__, __LINE__, ## a)

/*
 * TX resource tri-state on a connection basis (conn->km)
 *	1) NULL, no TX resources allocated.
 *	2) 'TX_RESOURCES_MERGED', TX resources were allocated and merged into
 *	   the global TX pool.
 *	3) (!= 0) TX resources allocated but not yet merged.
 */
#define TX_RESOURCES_MERGED 1


/*
 * ICS defines
 */

#define ICS_NODENUMSIZE 2  /* node number digits when represented as a string */

/*
 * Communication channels used by low-level ICS. This includes all the
 * high-level ICS channels plus reply channels.
 */
#define ICS_REPLY_CHAN		ics_reply_chan
#define ICS_REPLY_PRIO_CHAN	ics_reply_prio_chan

#define ICS_LL_NUM_CHANNELS	(ics_num_channels+2)
#define ICS_MAX_LL_CHANNELS	(ICS_MAX_CHANNELS+2)

/*
 * Port numbers for NSC services and ICS_PROBE_CLMS channel.
 * These port numbers are chosen based on an RFC which lists the
 * reserved port numbers. Currently, the port numbers start from
 * 900.  But, 911 is reserved for PPP and hence it is skipped.
 * Different numbers may be chosen, if desired.  The port numbers
 * have to be less than 1024. This is because most systems permit
 * the use of port numbers less than 1024 only if the process is
 * privileged.
 *
 * All registered NSC services are connected to port 900.
 * The IB connections established are guaranteed to be unique because
 * the QPs (Queue-Pairs) are different for each connection.
 *
 */
#define ICS_SVC_PORT	900	/* port for nsc svc */
/*
 * Port 901 is used by swat (Samba Web Administration Tool)
 */
#define ICS_PROBE_CLMS_PORT 902	/* port to listen for clms probes */

#define PPP_PORT	911	/* ppp port - see /etc/services */
#define ICS_CLI_PORT	3000	/* port for the first registered nsc client */

extern int ics_service_ports[];
extern int ics_client_ports[];

/*
 * Definitions needed to configure the UDP endpoints.
 */
#define ICS_UNACK_SVR_PORT	901	/* port for receiving
					   unacknowledged ICS messages */
#define ICS_UNACK_CLI_PORT	902	/* port for sending
					   unacknowledged ICS messages */

#define NSC_ICS_MAX_ROUTES	32
extern int ics_route_count;
extern char *ics_routes[];

/*
 * ICS unacknowledged message data structures.
 * This is cribbed from the LTI version, although for now, most fields
 * are "don't care"s.
 */

/*
 * The total number of bits in the ics_service field for each subfield.  These
 * should add up to 32.
 */
#define ICS_MTYPE_BITS		4
#define ICS_REQFRAG_BITS	1
#define ICS_REPFRAG_BITS	1
#define ICS_MLENGTH_BITS	26

/*
 * ICS unacknowledged message data structure.
 */
typedef struct {
	/*
	 * For all messages, this is the version of the ICS message.  This is
	 * set from the ICS_MESSAGE_VERSION definition.  Any message not
	 * carrying this message type is dropped.
	 */
	struct {
		u_int	uicsh_ver_type: ICS_MTYPE_BITS;
		u_int	uicsh_ver: (32-ICS_MTYPE_BITS);
	} uicsh_version;
	/*
	 * Type of request message that this is.
	 */
	u_int		uicsh_type;
	/*
	 * For ICS_NORMAL_DATA messages, this is the procedure number for
	 * the service.  For all current unacknowledge messages, this
	 * field is meaningless.
	 */
	int		uicsh_procnum;
	/*
	 * For ICS_NORMAL_DATA messages, this is the service to be invoked.
	 * For all current unacknowledged messages, this field is meaningless.
	 */
	int		uicsh_service;
	/*
	 * For ICS_NORMAL_DATA messages, this associates a client transaction
	 * with a server handle.  For all current unacknowledged messages,
	 * this field is meaningless.
	 */
	long		uicsh_transid;
	/*
	 * For ICS_NORMAL_DATA messages, this is the ICS channel that the
	 * message is being sent to.  For all current unacknowledged messages,
	 * this field is meaningless.
	 */
	int		uicsh_chan;
	/*
	 * This the node number of the sender.
	 */
	clusternode_t	uicsh_sender;
} uics_reqheader;

/*
 * Maximum size of an ICS unacknowledged datagram.
 */
#define ICS_UNACK_MAX_DGRAM_SIZE	2048

/*
 * Definitions for uics_reqheader.uicsh_type
 */
#define ICS_UNACK_IMALIVE	0x01
#define ICS_UNACK_LOAD		0x02
#define ICS_UNACK_SHOOT_NODE	0x04

/*
 * Definitions needed to configure the TCP connections.
 */
#define ICS_IPADDRLEN 16
#define ICS_MAX_IFNAME_SIZE 16

#define ICS_LOOPIFNAME "lo"
#define ICS_LOOPIPADDR "127.0.0.1"
#define ICS_LOOPNETMASK "255.0.0.0"

#define ICS_ROUTE_PARAMS 3

extern char ics_ifdevname[];
extern char ics_ifname[];
extern char ics_netaddr[];
extern char ics_netmask[];

extern int icslog;

/*
 * Header that is placed at the front of every ICS message. This includes both
 * sends and responses.
 */
typedef struct icshdr {
	unsigned int	ihdr_marker:32;
	unsigned int	ihdr_len:32;
	unsigned int	ihdr_version:16;
	unsigned int	ihdr_flag:16;
	unsigned int	ihdr_service:32;
	unsigned int	ihdr_procnum:16;
	unsigned int	ihdr_inline_len:16;
	long		ihdr_transid;
	unsigned int	ihdr_orignode:32;
	unsigned int	ihdr_chan:16;
#ifdef NSC_ICSLOG
	unsigned int	ihdr_uniqueid:16;
#else
	unsigned int	ihdr_unused:16;
#endif
} icshdr_t;

#define ICSHDR_MARKER	0xffffffff	/* only value for ihdr_marker */

#define LL_ICSHDR(a) (icshdr_t*)((a)->payload)

extern int 	ics_message_version;

/*
 * A structure that describes an encoded OOL data block. This includes OOL
 * data blocks corresponding to input parameters encoded by the client and
 * OOL data blocks corresponding to output parameters encoded by the server.
 *
 * NSC_XXX: The following pertains to the NSC TCP implementation.
 * 	    The callback code can and needs to be simplified.
 * Note that the callback that corresponds to the OOL data goes through two
 * levels of indirect calls. First, the routine specified by ool_freertn is
 * called, and this routine calls the ool_callback() routine. This is
 * necessary because the TCP subsystem allows routines with only one
 * parameter, and the ICS definition requires two parameters.
 */
typedef struct ooldesc {
	union {
		struct {
			void		(*_ool_callback_data_t)(
						u_char		*data,
						long		callarg);
			char		*_ool_callparam;
			long		_ool_callarg;
		}		_ool_callback;
	} _ool_union;
	union {
		struct svr_handle	*_ool_svr_handle;
		struct cli_handle	*_ool_cli_handle;
	} _ool_handle;

} ooldesc_t;

#define ool_callback_data_t _ool_union._ool_callback._ool_callback_data_t
#define ool_callparam _ool_union._ool_callback._ool_callparam
#define ool_callarg _ool_union._ool_callback._ool_callarg
#define ool_svr_handle _ool_handle._ool_svr_handle
#define ool_cli_handle _ool_handle._ool_cli_handle

#define IB_LLHANDLE_OOL_SEGS (ICS_DEF_OOL_SEGMENTS*2)

struct _kib_txd;
struct _kib_rxd;

/*
 * one of these follows ICS inline data in the IB message payload.
 */
typedef struct _ib_ool_info {
	struct _kib_txd *txd;		// TX desc for sender
	int	rdma_ool_desc_cnt;	// num RDMA OOL segments in this IB msg.
	int	ool_desc_cnt;		// total # of OOL segs in this IB msg.

}  __attribute__((packed)) ib_ool_info_t;

/*
 *	RDMA read status which the remote connection side has sent in response
 *	to receiving an ICS RPC request/reply which contained OOL data that
 *	that did not fit within the IB message, hence needs to be
 *	RDMA.read/pulled. Status message indicates the status of the remote
 *	side's RDMA read of our memory.  Status message contains a 'cookie'
 *	which is the address of our TX descriptor used for the original RPC
 *	send/reply operation. 
 *	The 'int' which txd->private points at is decrmented when the
 *	status message is received or set to an error from msg.status. The
 *	original sending thread is waiting for *txd->private to become <= 0
 *	to release the wait. The original RCP send started a kernel timer to
 *	cover the case where the remote node fails during our RDMA wait.
 *	The above mechanism prevents a sending node from handing due to remote
 *	node failiure and/or connection failure.
 */

typedef struct _rdma_status {
	struct _kib_txd *txd;	// TX descriptor containing RDMA'able OOL segs.
	u32	magic;
	int	status;		// combined status: != 0 is an error.
	int	rdma_op_cnt;	// # of successful RDMA.read operations so far.

}  __attribute__((packed)) ib_rdma_status_t;

#define RDMA_STATUS_MAGIC 0xcafebabe

// Max number of RDMA.read ops per IB message
#define MAX_RDMA_READ_OPS (ICS_OOL_SEGMENTS_CHUNKS*2)

typedef struct _ib_rdma_seg {
	DECLARE_PCI_UNMAP_ADDR(mapping)
	DECLARE_PCI_UNMAP_LEN(mlen)
	size_t		nob;	// number-of-bytes
	struct ib_mr	*mr;	// DMA memory region
	void		*baddr;	// != 0 implies dma_map_page() instead of
				// dma_map_single().
} ib_rdma_seg_t;

/*
 * IB in-message OOL data. Story here is not all OOL data needs to be RDMA
 * transfered due to the setup cost of an RDMA operation. For OOL data which
 * will fit in the TX desc payload (IBA_MAX_BUF_SIZE) the OOL data is copied
 * into the TX payload following inline data. Each in-message ool data segment
 * is preceeded by this struct. If 'len' > 0 then the OOL data follows for 'len'
 * bytes.
 * If the OOL data would not fit into the remaining TX buffer, 'len' is set to
 * be a negative length with the following 8 bytes representing an IB RDMA rkey
 * which will be used by the decode side to RDMA.read the OOL data.
 * Sending side will receive an RDMA status message when 'ALL' the OOL data
 * segments have been read/pulled.
 */

typedef struct	_ib_ool_desc {
	int	len; // < 0 implies RDMA rkey follows, else OOL data in message.
	union {
	    struct _rdma_info {
		u32	rkey;
		u64	rdma_addr;
		u32	crc;
		u32	magic;
	    } rdma;
	    char	data[sizeof(struct _rdma_info)];	
	} ib_ool_u;

}  __attribute__((packed)) ib_ool_desc_t;

#define ool_data ib_ool_u.data
#define ool_rkey ib_ool_u.rdma.rkey
#define ool_rdma_addr ib_ool_u.rdma.rdma_addr
#define ool_rdma_crc ib_ool_u.rdma.crc
#define ool_magic ib_ool_u.rdma.magic

/*
 * The following structure combines the default handle allocations into
 * a single structure.
 */

/*
 * Structure for the portion of the client handle that pertains to the
 * low-level ICS implementation.
 */
typedef struct ib_cli_llhandle
{
	/*
	 * Pointer to msghdr for the outgoing message.
	 */
	struct msghdr	*_ch_msg_msghdr_p;

	/*
	 * Part of structure devoted to tracking the in-line buffer for
	 * the outgoing message. Note that the iovec includes the actual
	 * icshdr data in addition to the inline data.
	 */
	struct iovec	*_ch_msg_inline_iov_p;
	u_char		*_ch_msg_inline_data_p;

	/*
	 * Part of structure devoted to tracking OOL buffers that have
	 * their own free routines for the outgoing message.
	 */
	ooldesc_t	*_ch_msg_ooldesc_p;
	int		_ch_msg_ooldesc_cnt;

	/*
	 * Pointer to IB message receive (RX) descriptor for the incoming reply
	 * (if there is an incoming reply).
	 */
	struct _kib_rxd *_ch_rep_rxd;

	/*
	 * Part of structure devoted to tracking the in-line buffer for the
	 * incoming reply (if there is an incoming reply).
	 */
	u_char		*_ch_rep_inline_data_p;

	/*
	 * Part of structure devoted to tracking OOL buffers for the incoming
	 * reply (if there is an incoming reply).
	 */
	ib_ool_info_t	*_ch_rep_ib_ool_info;

	/*
	 * track IB mesg ool descriptors
	 */
	ib_ool_desc_t	*_ch_rep_ib_od;

	/*
	 * expected RDMA.read operations required to retrieve all RDMA ool data
	 * segs for this reply.
	 */
	int		_ch_rep_rdma_cnt;

	/*
	 * bytes in actual message - not necessarily # of data bytes represented
	 * by the message as actual RDMA OOL data length is not included.
	 */
	int		_ch_msg_len;

	/*
	 * Reference count for counting number of OOL data blocks in handle
	 * (plus one for the in-line data and plus one for the reply if
	 * one is expected).
	 */
	int		_ch_ref_cnt;

	/*
	 * Flag for low-level ICS code.
	 */
	int		_ch_llflag;

	/*
	 * Lock used for ch_llflag and ch_ref_cnt.
	 */
	SOFTIRQ_SPIN_LOCK_T	_ch_lllock;

	/* msghdr, inline data, and default iovec and ooldesc. */
	struct msghdr	_ch_msghdr;
	struct iovec	_ch_iovec[ICS_DEF_OOL_SEGMENTS + 1];
	ooldesc_t	_ch_ooldesc[ICS_DEF_OOL_SEGMENTS];
	u_char		_ch_inline[sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE];

} ib_cli_llhandle_t;

#define ch_msg_msghdr_p ch_llhandle.ibcli_llhandle._ch_msg_msghdr_p
#define ch_msg_inline_iov_p ch_llhandle.ibcli_llhandle._ch_msg_inline_iov_p
#define ch_msg_inline_data_p ch_llhandle.ibcli_llhandle._ch_msg_inline_data_p
#define ch_msg_ooldesc_p ch_llhandle.ibcli_llhandle._ch_msg_ooldesc_p
#define ch_msg_ooldesc_cnt ch_llhandle.ibcli_llhandle._ch_msg_ooldesc_cnt
#define ch_rep_inline_data_p ch_llhandle.ibcli_llhandle._ch_rep_inline_data_p
#define ch_rep_ool_data_p ch_llhandle.ibcli_llhandle._ch_rep_ool_data_p
#define ch_ref_cnt ch_llhandle.ibcli_llhandle._ch_ref_cnt
#define ch_llflag ch_llhandle.ibcli_llhandle._ch_llflag
#define ch_lllock ch_llhandle.ibcli_llhandle._ch_lllock
#define ch_msghdr ch_llhandle.ibcli_llhandle._ch_msghdr
#define ch_iovec ch_llhandle.ibcli_llhandle._ch_iovec
#define ch_ooldesc ch_llhandle.ibcli_llhandle._ch_ooldesc
#define ch_inline ch_llhandle.ibcli_llhandle._ch_inline

#define ch_txd ch_llhandle.ibcli_llhandle._ch_txd
#define ch_msg_len ch_llhandle.ibcli_llhandle._ch_msg_len
#define ch_ooldesc ch_llhandle.ibcli_llhandle._ch_ooldesc
#define ch_rep_rxd ch_llhandle.ibcli_llhandle._ch_rep_rxd
#define ch_rep_ib_od ch_llhandle.ibcli_llhandle._ch_rep_ib_od
#define ch_rep_ib_ool_info ch_llhandle.ibcli_llhandle._ch_rep_ib_ool_info
#define ch_rep_ooldesc_cnt ch_llhandle.ibcli_llhandle._ch_rep_ooldesc_cnt
#define ch_rep_rdma_cnt ch_llhandle.ibcli_llhandle._ch_rep_rdma_cnt

/*
 * Definitions for ch_llflag in low-level server handle.
 */
#define CLI_HANDLE_REPLY_DONE	0x01	/* Reply has been processed. */

/*
 * Structure for the portion of the server handle that pertains to the
 * low-level ICS implementation.
 */
typedef struct ib_svr_llhandle
{
	/*
	 * Track information about the incoming message to the server.
	 */
	struct _kib_rxd	*_sh_msg_rxd; /* IB RX descriptor */

	/*
	 * Track information while decoding in-line data within the
	 * incoming message.
	 */
	u_char		*_sh_msg_inline_data_p;

	/*
	 * ptr into received msg (rxd) where IB ool descriptors start
	 */
	ib_ool_info_t	*_sh_msg_ib_ool_info;

	/*
	 * Track information while decoding OOL data within the
	 * incoming message.
	 */
	ib_ool_desc_t	*_sh_msg_ib_od;

	/* Number of completed RDMA OOL descriptors for received IB message */
	int		_sh_msg_rdma_cnt;

	/*
	 * Message block for the response associated with the handle. Note that
	 * the sk_buff is there when an incoming message is received, even if
	 * the incoming message requires no response. For cases where no
	 * response is sent, the sk_buff hangs around and will be used by a
	 * subsequent incoming message for this handle that does require a
	 * response.
	 */
	struct msghdr	*_sh_rep_msghdr_p;

	/*
	 * Information for tracking the in-line portion of the response.
	 * Note that the sk_buff includes the actual icshdr data in addition
	 * to the inline data.
	 */
	struct iovec	*_sh_rep_inline_iov_p;
	u_char		*_sh_rep_inline_data_p;

	/*
	 * Information for tracking the OOL buffers of the response.
	 */
	ooldesc_t	*_sh_rep_ooldesc;
	int		_sh_rep_ooldesc_cnt;

	/*
	 * Reference count for OOL data segments with their own free
	 * routines (plus one for the in-line data segment).
	 */
	u_int		_sh_ref_cnt;

	/*
	 * Lock for sh_ref_cnt.
	 */
	SPIN_LOCK_T	_sh_lllock;

	/* msghdr, inline data, and default iovec and ooldesc. */
	struct msghdr	_sh_msghdr;
	struct iovec	_sh_iovec[ICS_DEF_OOL_SEGMENTS + 1];
	ooldesc_t	_sh_ooldesc[ICS_DEF_OOL_SEGMENTS];
	u_char 		_sh_inline[sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE];

} ib_svr_llhandle_t;

#define sh_msg_inline_data_p sh_llhandle.ibsvr_llhandle._sh_msg_inline_data_p
#define sh_msg_ool_data_p sh_llhandle.ibsvr_llhandle._sh_msg_ool_data_p
#define sh_rep_msghdr_p sh_llhandle.ibsvr_llhandle._sh_rep_msghdr_p
#define sh_rep_inline_iov_p sh_llhandle.ibsvr_llhandle._sh_rep_inline_iov_p
#define sh_rep_inline_data_p sh_llhandle.ibsvr_llhandle._sh_rep_inline_data_p
#define sh_rep_ooldesc sh_llhandle.ibsvr_llhandle._sh_rep_ooldesc
#define sh_rep_ooldesc_cnt sh_llhandle.ibsvr_llhandle._sh_rep_ooldesc_cnt
#define sh_ref_cnt sh_llhandle.ibsvr_llhandle._sh_ref_cnt
#define sh_lllock sh_llhandle.ibsvr_llhandle._sh_lllock
#define sh_msghdr sh_llhandle.ibsvr_llhandle._sh_msghdr
#define sh_iovec sh_llhandle.ibsvr_llhandle._sh_iovec
#define sh_ooldesc sh_llhandle.ibsvr_llhandle._sh_ooldesc
#define sh_inline sh_llhandle.ibsvr_llhandle._sh_inline

#define sh_msg_rxd sh_llhandle.ibsvr_llhandle._sh_msg_rxd
#define sh_msg_ib_ool_info sh_llhandle.ibsvr_llhandle._sh_msg_ib_ool_info
#define sh_msg_ib_od sh_llhandle.ibsvr_llhandle._sh_msg_ib_od
#define sh_msg_rdma_cnt sh_llhandle.ibsvr_llhandle._sh_msg_rdma_cnt

/*
* DESCRIPTION -- Host ID
*	Infiniband connection private data is formatted according to
*	this structure. A client will set it's IPv4 address and hostname +
*	ics channel # as a string in the rdma CM connection request operation
*	private data fields. A server will reply to the CM connect request with
*	a rdma_accept() message, with the private data consisting of the
*	server's (hostname:ics_chan). The values exchanged are for
*	informational/debug purposes only! Nice to know who you're talking to.
* FIELDS
*    nid
*	Network ID (IPv4 address in network byte-order)
*    node
*	ICS node number - remote ICS node we are connected to.
*    hostname_len
*	strlen(hostname) + 1 (ASCIZ null termination byte) for hostname.
*    hostname
*	Hostname associated with this host.
* SEE ALSO
*	connect_to_server, listen_for_connection
*****/

// approx. 80 chars
#define MAX_HOSTNAME_SIZE (IB_CM_REQ_PRIVATE_DATA_SIZE - (2*sizeof(u32)+1))

typedef struct
{
    u32		nid;
    u32		node;
    u8		hostname_len;	// includes string termination null.
    char	hostname[MAX_HOSTNAME_SIZE];

} ib_host_id_t;

#define HOST_ID_BASE_SIZE (2*sizeof(u32) + sizeof(u8))

/*
 * IB listen descriptor - used for each persistent server port listen.
 */
typedef struct {
    struct rdma_cm_id	*listen_id;
    int			port;

}   ib_listen_t;

/*
* DESCRIPTION -- Infiniband Nexus; The 'root' of IB Global data.
*
* FIELDS
*    magic
*	Debug - Magic bit pattern to identify if the data has been corrupted.
*
*    status
*	status returned for callback operations which are not connection
*	oriented.
*
*    wq
*	asynchronous connection destroy uses this work queue.
*
*    device;
*	Infiniband device - 1st HCA registered
*
*    pd
*	IB protection domain based on 1st HCA
*
*    kmr
*	DMA'able kernel memory region, IB lkey & rkey for RDMA.reads
*
*    my_id
*	Local host ID: nid/Network_ID (host byte-order), node # & hostname.
*	A nid is the IPv4 address bound to the Infiniband HCA ala IPoIB in
*	intrd/linuxrc. This ID is delivered to an active connection in IB
*	private data, and sent by an active connection requestor.
*
*    svc_port
*	Infiniband service port the local server is listening on in host
*	byte-order. Also the service port a connect(0 is using.
*
*    struct list_head active_conn
*	list of active connection structures; sync access with conn_lock.
*
*    num_connected
*	Atomic count of established IB connections; not necessarily the number
*	of element in the active_conn list, as a connection may not be
*	established yet, although it is still on the active conn list.
*
*    conn_total
*	Total nummber of connections established to/from this node.
*
*    idletxds
*	Global transmit descriptors (txd) free list; sync with idletxd_lock.
*	A thread can block for this list. Threads wait on kib_idletxd_waitq.
*	A txd is required to send IB data (aka ICS message) or RDMA.read.
*
*    nblk_idletxds
*	Non-blocking access, list of free transmit descriptors. Utilized when
*	sending thread can not block.
*
*    idletxd_lock
*	serialize idle/free transmit descriptor list access. Covers both the
*	blocking txd and non-blocking txd lists.
*
*    idletxd_waitq
*	The wait queue upon which a sending thread blocks on, waiting for
*	an idle/free txd.
*
*    rx_pool
*	The Global RX emergency pool list head. Connections acquire an rxd
*	from here when posted RX descriptors fall below the LOW water mark.
*	IB receive operations require a posted buffer in order to suceed.
*
*    rx_pool_lock
*	lock which serializes access to the Global RX pool
*
*    rx_pool_grow_lock
*	serialize growth of the Global RX pool
*
*    rx_pool_free
*	Number of elements in the Global RX pool
*
*    rx_pool_gets
*	Number of times Global RX pool elements were required.
*
*    rx_pool_in_use
*	Number of Global RX pool elements currently in use.
*
*    rx_pool_in_use_max
*	Max number of Global RX pool elements in use.
*
*    txds_in_use
*	count of the blocking TX descriptors in use.
*
*    txds_free
*	count of the blocking TX descriptors which are free
*
*    txds_max_in_use
*	Max count of blocking TX descriptors which are in use
*
*    txds_max_empty
*	Max times the blocking TX descriptor list was empty, hence the sending
*	thread had to block waiting for a txd.
*
*    nblk_txds_in_use
*	Number of non-blocking TX descriptos in use
*
*    nblk_txds_free
*	Number of non-blocking TX descriptos which are free
*
*    nblk_txds_max_in_use
*	Max number of non-blocking TX descriptos in use
*
*    nblk_txds_max_empty
*	Max times the blocking TX descriptor list was empty, hence the sending
*	thread was unable to send.
*
*    unclaimed_tx_resources
*	When an established IB connection is lost, it's per connection allocated
*	TX resources (txd & TX buffers) are returned to the global TX pool.
*	When the next connection is created, it will reclaim the previous
*	connections TX resource allocation hence adding no new TX resources
*	to the global TX pool.
*
*    ksvr_workQ
*	Work queue for Kserver threads. Received packets are processed off this
*	list, as are TX RDMA descriptors to IB deregister memory.
*
*    ksvr_workQ_cnt
*	Number of elements on the Ksvr work queue (backlog).
*
*    ksvr_workQ_max
*	Max backlog ksvr work queue has witnessed.
*
*    ksvr_waitQ
*	Where the ksvr threads wait when there is no work to do.
*
*    ksvr_lock
*	Serialize access to Ksvr work queue and counters.
*
*    ksvr_nthreads
*	Number of ksvr threads processing the ksvr work queue.
*
*    ksvr_run
*	Boolean which ksvr threads watch to determine if they should  continue
*	running or exit.
*
* NOTES
*    There is exactly one of this structure which pertains to all IB
*    connections.  Specific IB connection related information is stored in the
*    list of active connection 'active_conn'.
*****/

typedef struct _ib_nexus
{
    u32			magic;
    volatile u32	status;

    // work queue
    struct workqueue_struct *wq;

    // Infiniband device - 1st HCA only
    struct ib_device	*device;

    // protection domain based on 1st HCA
    struct ib_pd	*pd;

    // DMA'able kernel memory region
    struct ib_mr	*kmr;

    ib_host_id_t	my_hid;
    
    // IB posted listen()s
    ib_listen_t		ics_svc_listen;
    ib_listen_t		ics_probe_listen;
    ib_listen_t		ics_unack_listen;

    // IB connections
    struct list_head	active_conn;
    atomic_t		num_connected;
    u32			conn_total;
    rwlock_t		conn_lock;

    // TX descriptors
    kmem_cache_t	*txd_cache;
    struct list_head	idletxds;
    struct list_head	nblk_idletxds;
    spinlock_t		idletxd_lock;
    wait_queue_head_t	idletxd_waitq;

    // RX emergency free pool
    struct list_head	rx_pool;
    spinlock_t		rx_pool_lock;
    spinlock_t		rx_pool_grow_lock;
    u32			rx_pool_free;
    u32			rx_pool_gets;
    u32			rx_pool_in_use;
    u32			rx_pool_in_use_max;

    u32			txds_in_use;
    u32			txds_free;
    u32			txds_max_in_use;
    u32			txds_empty;

    u32			nblk_txds_in_use;
    u32			nblk_txds_max_in_use;
    u32			nblk_txds_free;
    u32			nblk_txds_empty;

    atomic_t		unclaimed_tx_resources;

#if KSVR || KSVR_TASKLET
    // Kserver work queue
    struct list_head	ksvr_workQ;
    volatile u32	ksvr_workQ_cnt;
    u32			ksvr_workQ_max;
    spinlock_t		ksvr_lock;
    wait_queue_head_t	ksvr_waitQ;
    atomic_t		ksvr_nthreads;
    int			ksvr_run;
#endif

} ib_nexus_t;

extern ib_nexus_t	ib_nexus;


/*
* IB_connection_states -- IB connections transition thru these atomic states.
*/
#define	IBA_DISCONNECTED	0	// ground-zero

#define	IBA_CONNECTING		1	// client: sent CM REQ, expect CM REPLY
					// server: received CM REQ, sent REPLY
					//         waiting for RTU.

#define	IBA_CONNECTED		2	// CM 3-way handshake completed.
					// svr: received RTU, client: sent RTU.

#define	IBA_DISCONNECTING	3	// sent CM disconnect, expect DREPLY.
					// DREPLY destroys the connection.
#define	IBA_CLOSING		4	// recv'ed CM DREQ, sent DREPLY, closing

#define	IBA_REJECT		5	// sent REJECT, expect no reply.

#define	IBA_REJECTED		6	// received REJECT, no reply

#define	IBA_ERROR		7	// QP error callback fired or remote
					// side disconnected abruptly.

#define	IBA_STATE_MAX		8

#define IB_SET_CONN_STATE(a,b) atomic_set(&(a)->state,(b))
#define IB_GET_CONN_STATE(a) atomic_read(&(a)->state)
/*
* NOTES
*	These IB connection states must match the state names in ics_ib_cm.c
*	IB_conn_state_str().
*/
struct ib_conn;
char *IB_conn_state_str( struct ib_conn *conn );

/*
 * Receive descriptor allocation class
 */
#define RXD_CLASS_CONN	0
#define RXD_CLASS_CLONE	1

/*
* NAME
* 	kib_rx_t -- Receive descriptor
* DESCRIPTION
*	Description of a single posted receive
* SYNOPSIS
*/

typedef struct _kib_rxd
{
	/* IB received message data land here */
	char 			*payload; //[IBA_MAX_BUF_SIZE];

	/* DMA address for payload bay */
	dma_addr_t		dma_addr;

	/* RXD free list links */
	struct list_head	link;

	/* IB connection RXD is posted on */
	struct ib_conn		*conn;

	/* Number-Of-Bytes received */
	u32			nob;

	/* IB work-complete type */
	enum ib_wc_opcode	ib_wc_type;

	/* IB status for the completed receive */
	int			status;

	/* RXD Allocation class -
	 *   RXD_CLASS_CONN == allocated from a specific connection's rxd pool.
	 *   RXD_CLASS_POOL == allocated from global receive pool.
	 */
	u8			class;

} ____cacheline_aligned kib_rx_t;


/*
* DESCRIPTION -- An Infiniband Transmit descriptor.
*       The entire TX descriptor lives in Infiniband registered memory. Sending
*       a message requires one of these.
*/
typedef struct _kib_txd
{
	/* message payload lands here */
	char			*payload; //[IBA_MAX_BUF_SIZE];

	/* DMA address for payload bay */
	dma_addr_t		dma_addr;
	DECLARE_PCI_UNMAP_ADDR(mapping)
	DECLARE_PCI_UNMAP_LEN(mlen)

	/* free list */
        struct list_head	ktx_list;

	/* IB connection which currently own's this TXD */
	struct ib_conn		*conn;

	/* adrs of expected RDMA counter */
	void			*private;

	/* RDMA timer info */
	struct timer_list	timeout_timer;

	/* IB local key for the TXD memory */
	u32			lkey;

	/* Number-Of-Bytes to transmit */
	u32			nob;

	/* Transmission status (IB Work-Complete) status is recorded here. */
	volatile int		status;

	/* IB work-request_type, used to patch up Mellanox driver error codes
	 * to match the correct error with work-request type. Specifically RDMA
	 * errors are reported as WC_SEND errors.
	 */
	enum ib_wr_opcode	wrt;

	/* TRUE implies this element came from the non-blocking free list */
	u32			non_blk;

	/* IB work request class */
	u8			class;

} ____cacheline_aligned kib_tx_t;


/*
 * Infiniband (IB) reliable connection information. One of these for each
 * IB reliable connection.
 */
typedef struct ib_conn
{
    struct list_head		conn_list;	// connection list
    wait_queue_head_t		waitQ;		// TX & misc. waitQ
    volatile int		error;		// last IB access layer error.
    struct timer_list		timeout;	// RDMA timeout info
    rwlock_t			conn_lock;
    struct ics_ib_conn_data	*ics_data;	// ICS connection info
    struct _ib_nexus		*ib_nexus;	// global IB data
    u32				magic;		// debug

    // remote host connection information
    u32				r_nid;	  // remote IPv4 addr, ib_connect() to.
    u32	            		svc_port; // remote/local service port
    ib_host_id_t		r_hid;	// remote hostname in CM private data.

    // Infiniband resource handles
    struct ib_cq		*cq;	// handle: Completion Queue
    struct rdma_cm_id		*cm_id;

    // RMQ - Received Message Queue
    int				rmq_receive; // bool (1)direct recv from rmq
    struct list_head		rmq;	// received message queue (rx_t)
    int				rmq_cnt;// count of msgs on rmq
    int				rmq_max;// max observed Q length.
    wait_queue_head_t		rmq_waitQ;// RX waitQ
    spinlock_t			rmq_lock;

    // receive (RX) resources
    void			*recvq;	// RX desc memory alloc
    u32				rbcnt;	// # of recv buffers alloc'ed

    // transmit resource lists - until they are merged with global TX pool
    struct list_head		txds;	// transmit desc list, can block
    struct list_head		nb_txds;// transmit desc list, non-blocking
    ulong			km;	// txd resource state.

    atomic_t			state;	// IB connection state.
    struct work_struct		work;	// async work queue element.

    // data exchange accounting - see /proc/cluster/ics_ib
    atomic_t			tx_posted;
    u32				max_tx_posted;
    u64				buf_tx_bytes;
    u32				buf_tx_op;

    u32				min_rx_posted;
    atomic_t			rx_posted;
    u64				buf_rx_bytes;
    u64				rdma_sink_bytes;
    u64				rdma_src_bytes;
    u32				rdma_src_op;
    u32				rdma_sink_op;
    u32				rdma_crc_errs;
    u32				rdma_crc_ok;

    int				max_rdma_per_msg;
    int				max_rdma_bytes;
    int				min_rdma_bytes;

    u32				cq_max_per_poll;
    u32				cq_polls;
    u32				cq_events;

    /* low-level ICS message stats */
    u32				icsm_cnt;
    u32				icsm_inline_bytes;
    u32				icsm_inline_max;
    u32				icsm_inline_min;

} ____cacheline_aligned	ib_conn_t;

/* Infiniband service routines */
int ib_listen_for_connection(ib_listen_t *ld, int svc_port);
int ib_connect(u32 ipaddr, u32 port, ib_conn_t **conn);
void ib_destroy_connection(ib_conn_t *conn);
void ib_async_destroy_conn(ib_conn_t *conn);

char * nid_str(u32);
char * nid_2_str(u32,char*);
void ib_completion_cb(struct ib_cq *cq, void *cq_context);

int kib_post_recv_wr(kib_rx_t *krx);
kib_tx_t *get_txd(ib_conn_t *conn, int may_block);
void put_idle_txd (kib_tx_t *ktx);

int ib_readmsg(ib_conn_t *ibc, kib_rx_t **rrxd, u32 wait_secs);
int ib_sendmsg(struct _kib_txd *ktx, u32 nob);

int create_ib_proc_fs(void);
void destroy_ib_proc_fs(void);

const char *ib_get_wc_status_str(enum ib_wc_status status);

int
ib_rdma_setup( u_char                  *buf,
               int                     nob,
               enum dma_data_direction dma_dir,
               ulong                   *rdma_adrs,
               ib_rdma_seg_t           *rdma_seg );

int
ib_rdma_read(
	ib_conn_t      *conn,
        char           *buf,
        int            len,
        ulong          *laddr,
        u32            lkey,
        ib_ool_desc_t  *od,
        kib_tx_t       *waiting_txd,
        int            send_status );

/*
 * The ics_llnodeinfo[] array is used by the low-level ICS code to track
 * information about the status of connections to all nodes in the cluster,
 * plus information about whether the node is up or down.
 *
 * The information is used for both incoming datagrams and for outgoing
 * messages.
 *
 * For incoming datagrams the ics_ib_conn_data structure is the socket
 * private data structure (i.e. it is pointed to by sock->user_data).
 *
 * For outgoing messages, the ics_sock_conn_data structure is used to
 * identify the connection to be used to send out the data.
 */

/* Lock to serialize connection attempts. */
extern LOCK_T			ics_llconn_lock;

extern SPIN_LOCK_T		ics_llnodeinfo_lock;

typedef struct ics_ib_conn_data {
	clusternode_t	   	conn_node;	/* node */
	int		   	conn_chan;	/* communication channel */
	struct ib_conn		*conn_ib;	/* transport connection */
	ATOMIC_INT_T		conn_ref_cnt;   /* Number of processes */
						/* throttled, sleeping in */
						/* stp->sd_write */
	LOCK_T			conn_lock;	/* channel serialization lock */
	void			*conn_snd_rand_statep;
	void			*conn_rcv_rand_statep;
						/* /dev/random support */
} ics_ib_conn_data_t;

// one of these for every node

typedef struct {
	char			*icsni_node_addr;
	ics_ib_conn_data_t	**icsni_conn;	// index by ics chan #
	short			icsni_flag;
	short			icsni_ref_cnt;
	RW_SPIN_LOCK_T		icsni_lock;
	EVENT_T			icsni_event;
	EVENT_T			icsni_chan_event;
} ics_llnodeinfo_t;

extern ics_llnodeinfo_t		*ics_llnodeinfo[];

/* Flag value for icsni_flag. */
#define ICSNI_GOING_DOWN	0x1
#define ICSNI_NODE_DOWN		0x2
#define ICSNI_COMING_UP		0x4
#define ICSNI_NODE_UP		0x8

typedef struct ics_clmsprobe_req {
	clusternode_t	my_node;
	icsinfo_t	my_icsinfo;
	int		imasecondary;
	int		imamember;
} ics_clmsprobe_req_t;

typedef struct ics_clmsprobe_rep {
	/*
	 * The CLMS master node.
	 */
	clusternode_t	cp_master;
	icsinfo_t	cp_icsinfo;
	int		error;
} ics_clmsprobe_rep_t;

/*
 * The definition of the ICS_SHOOT_NODE message data.
 */
typedef struct {
        int             my_ics_version;
        int             i_am_cluster;
} ics_shoot_node_t;

#ifdef NSC_ICSLOG

#define		ICSLOG(level, msgnum, a, b, c, d, e, f)			\
			do {						\
				if (icslog)				\
					ics_log((level), (msgnum),	\
						(a), (b), (c), (d),	\
						(e), (f));		\
			} while(0)

/*
 * Message numbers; these correspond to entries in the icslog_msgs array in
 * ics_llsubr.c (they are indices into the array).
 */
#define	ICSLOG_CLILLSEND	(ICSLOG_LLICSOFF + 0)
#define ICSLOG_SVRLLSEND	(ICSLOG_LLICSOFF + 1)
#define ICSLOG_DGRAMMSGSMALL	(ICSLOG_LLICSOFF + 2)
#define ICSLOG_DGRAMMSGHDRL	(ICSLOG_LLICSOFF + 3)
#define ICSLOG_DGRAMMSGINC	(ICSLOG_LLICSOFF + 4)
#define ICSLOG_DGRAMMULTMSG	(ICSLOG_LLICSOFF + 5)
#define ICSLOG_DGRAMUPCALL	(ICSLOG_LLICSOFF + 6)
#define ICSLOG_SVRLLND		(ICSLOG_LLICSOFF + 7)
#define ICSLOG_SVRLLEQ		(ICSLOG_LLICSOFF + 8)
#define ICSLOG_SVRLLDQ		(ICSLOG_LLICSOFF + 9)
#define ICSLOG_CLILLSENDUP	(ICSLOG_LLICSOFF + 10)
#define ICSLOG_SVRLLPROCMSG	(ICSLOG_LLICSOFF + 11)
#define ICSLOG_SVRLLNDDROP	(ICSLOG_LLICSOFF + 12)

extern int ics_uniqueid;
extern spinlock_t ics_uniqueid_spin_lock;

#else

/*
#define		ICSLOG(A, B, C)
*/
#define		ICSLOG(level, msgnum, a, b, c, d, e, f)

#endif

/*
 * Internal ICS subroutines.
 */
void icscli_llnodedown(clusternode_t);

void icscli_llsendup_reply(struct _kib_rxd *);
void icssvr_llsendup_msg(struct _kib_rxd *);

void ics_lltransport_config(void);

void ics_llnode_to_icsinfo(icsinfo_t *, clusternode_t);

int ics_llsendmsg(ics_ib_conn_data_t *, struct msghdr *, int, ib_rdma_seg_t *,
						int *, struct _kib_txd **);

void icssvr_llnodedown(clusternode_t, int);

void ics_dgram(struct _kib_rxd *);

#define ics_llfreemsg(rd) kib_post_recv_wr((rd))

#endif /* _CLUSTER_ICS_ICS_IB_PRIVATE_H */
