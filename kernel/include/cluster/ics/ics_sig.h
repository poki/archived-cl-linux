/*
 * 	ICS signal forwarding support.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This header file contains Linux platform specific signal code
 * for use with ICS signal forwarding.
 */

#ifndef _CLUSTER_ICS_ICS_SIG_H
#define _CLUSTER_ICS_ICS_SIG_H

#include <linux/sched.h>
#include <linux/signal.h>

/*
 * This macro is used to post a SIGKILL on the server.
 */
#define ICSSIG_POST_SERVER_SIGNAL(_procp)				\
	do {								\
		(void) force_sig(SIGKILL, _procp);			\
	} while(0)

/*
 * Discard all the signals for a server process.
 */
#define ICSSIG_DISCARD_SERVER_SIGNALS()					\
	do {								\
		struct task_struct *t = current;			\
		flush_signals(t);					\
	} while(0)

#endif /* !_CLUSTER_ICS_ICS_SIG_H */
