/*
 * 	Cluster locking macros.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file provides macros to map generic NSC locking interfaces
 * to Linux platform specific locking primitives.
 */

#ifndef _CLUSTER_SYNCH_H
#define _CLUSTER_SYNCH_H

#include <linux/sched.h>
#include <linux/rwsem.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/ide.h>
#include <asm/hardirq.h>
#include <cluster/synch_types.h>
#include <cluster/assert.h>

#if __SSI_LOCK_DEBUG

#define SSI_LOCK_ASSERT(_expr)		ssi_assert(_expr)

#define	SSI_LOCK_ASSERT_INITED(_objp)					\
	SSI_LOCK_ASSERT((_objp)->__magic == ((long)&(_objp)->__magic) + 1)

#define	SSI_LOCK_ASSERT_DEINITED(_objp)					\
	SSI_LOCK_ASSERT((_objp)->__magic != ((long)&(_objp)->__magic) + 1)

#define __SSI_LOCK_DEINIT_DEBUG(_objp)					\
do {									\
	(_objp)->__magic = 0;						\
	(_objp)->__creator = (long)current_text_addr();			\
} while (0)

#define SSI_LOCK_DEINIT_DEBUG(_objp)					\
do {									\
	SSI_LOCK_ASSERT_INITED(_objp);					\
	__SSI_LOCK_DEINIT_DEBUG(_objp);					\
} while (0)

#define SSI_LOCK_INIT_DEBUG(_objp)					\
do {									\
	SSI_LOCK_ASSERT_DEINITED(_objp);				\
	(_objp)->__magic = ((long)&(_objp)->__magic) + 1;		\
	(_objp)->__creator = (long)current_text_addr();			\
} while (0)

#else

#define SSI_LOCK_ASSERT(_expr)		((void)0)

#define	SSI_LOCK_ASSERT_INITED(_objp)					\
do {									\
} while (0)

#define	SSI_LOCK_ASSERT_DEINITED(_objp)					\
do {									\
} while (0)

#define __SSI_LOCK_DEINIT_DEBUG(_objp)					\
do {									\
} while (0)

#define SSI_LOCK_DEINIT_DEBUG(_objp)					\
do {									\
} while (0)

#define SSI_LOCK_INIT_DEBUG(_objp)					\
do {									\
} while (0)

#endif

static inline void ssi_block_signals(struct task_struct *_curtask,
				     sigset_t *_old_blocked)
{
	unsigned long _flags;

	spin_lock_irqsave(&_curtask->sighand->siglock, _flags);
	*_old_blocked = _curtask->blocked;
	sigfillset(&_curtask->blocked);
	recalc_sigpending();
	spin_unlock_irqrestore(&_curtask->sighand->siglock, _flags);
}

static inline void ssi_unblock_signals(struct task_struct *_curtask,
				       sigset_t *_old_blocked)
{
	unsigned long _flags;

	spin_lock_irqsave(&_curtask->sighand->siglock, _flags);
	_curtask->blocked = *_old_blocked;
	recalc_sigpending();
	spin_unlock_irqrestore(&_curtask->sighand->siglock, _flags);
}

#ifdef LINUX_SSI_INTERRUPTIBLE
/* Derived from Linux rpc_clnt_sigmask() */
static inline void ssi_mask_signals(struct task_struct *task,
				     sigset_t *oldset, int intr)
{
	unsigned long	sigallow = sigmask(SIGKILL);
	unsigned long	irqflags;

	/* Turn off various signals */
	if (intr) {
		struct k_sigaction *action = task->sighand->action;
		if (action[SIGINT-1].sa.sa_handler == SIG_DFL)
			sigallow |= sigmask(SIGINT);
		if (action[SIGQUIT-1].sa.sa_handler == SIG_DFL)
			sigallow |= sigmask(SIGQUIT);
	}
	spin_lock_irqsave(&task->sighand->siglock, irqflags);
	*oldset = task->blocked;
	siginitsetinv(&task->blocked, sigallow & ~oldset->sig[0]);
	recalc_sigpending();
	spin_unlock_irqrestore(&task->sighand->siglock, irqflags);
}

static inline void ssi_unmask_signals(struct task_struct *task,
				     sigset_t *oldset, int intr)
{
	(void)intr;
	ssi_unblock_signals(task, oldset);
}
#endif /* LINUX_SSI_INTERRUPTIBLE */

/*
 * The following interfaces are provided for atomically manipulating
 * integer values:
 *
 *	INIT_ATOMIC_INT(&int, value);
 *	INCR_ATOMIC_INT(&int);
 *	DECR_ATOMIC_INT(&int);
 *	value = READ_ATOMIC_INT(&int);
 *	WRITE_ATOMIC_INT(&int, value);
 *
 *	where "int" is of type ATOMIC_INT_T.
 *
 *	JAG - Straightforward translation to Linux primitives.
 */

#define INIT_ATOMIC_INT(_intp, _value) \
	atomic_set(_intp, _value)

#define INCR_ATOMIC_INT(_intp) \
	atomic_inc(_intp)

#define DECR_ATOMIC_INT(_intp) \
	atomic_dec(_intp)

#define READ_ATOMIC_INT(_intp) \
	atomic_read(_intp)

#define WRITE_ATOMIC_INT(_intp, _value) \
	atomic_set(_intp, _value)

/*
 * Spin Locks (do not block interrupts)
 * ----------
 *
 * The following interfaces are provided:
 *
 *	INIT_SPIN_LOCK(&lock)
 *
 *		This macro initializes the specified lock. This macro cannot
 *		sleep.
 *
 *	LOCK_SPIN_LOCK(&lock)
 *
 *		This macro locks the spin lock.
 *
 *	locked = TRY_LOCK_SPIN_LOCK(&lock)
 *
 *		This macro does not spin, but instead returns non-zero
 *		if the lock was acquired and 0 if unsuccessful.  On
 *		a uniprocessor this macro always returns non-zero.
 *
 *	SSI_ASSERT_LOCKED_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain spin
 *		lock is currently held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	SSI_ASSERT_UNLOCKED_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain spin
 *		lock is not currently held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	SSI_ASSERT_NO_SPIN_LOCKS_HELD()
 *
 *		This is an assert macro that checks that no spin locks
 *		are currently held on the processor.  This macro will
 *		not fail on systems that cannot support this check.
 *
 *	NO_SPIN_LOCKS_HELD()
 *
 *		This macro can be used to trigger debugging code
 *		(i.e. an informative printf) in the case where spin
 *		locks are held when they should not.
 *
 *	UNLOCK_SPIN_LOCK(&lock)
 *
 *		This macro unlocks a held spin lock.
 *
 *	DEINIT_SPIN_LOCK(&lock)
 *
 *		This macro deallocates any data structures that may be
 *		associated with a spin lock.  On some systems this may
 *		be a no-op.
 *
 */

static inline void ssi_init_spin_lock(SPIN_LOCK_T *lockp)
{
	SSI_LOCK_INIT_DEBUG(lockp);
	spin_lock_init(&lockp->sp_lock);
	lockp->sp_owner = NULL;
}

static inline int ssi_locked_spin_lock(SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	return (lockp->sp_owner == current);
}

static inline void ssi_lock_spin_lock(SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	BUG_ON(ssi_locked_spin_lock(lockp));
	spin_lock(&lockp->sp_lock);
	lockp->sp_owner = current;
}

static inline void ssi_unlock_spin_lock(SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	SSI_LOCK_ASSERT(ssi_locked_spin_lock(lockp));
	BUG_ON(!ssi_locked_spin_lock(lockp));
	lockp->sp_owner = NULL;
	spin_unlock(&lockp->sp_lock);
}

static inline int ssi_try_lock_spin_lock(SPIN_LOCK_T *lockp)
{
	int locked;

	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	locked = spin_trylock(&lockp->sp_lock);
	if (locked)
		lockp->sp_owner = current;

	return locked;
}

static inline void ssi_deinit_spin_lock(SPIN_LOCK_T *lockp)
{
	SSI_LOCK_DEINIT_DEBUG(lockp);
}

#define INIT_SPIN_LOCK(_lockp) \
	ssi_init_spin_lock(_lockp)

#define LOCK_SPIN_LOCK(_lockp) \
	ssi_lock_spin_lock(_lockp)

#define TRY_LOCK_SPIN_LOCK(_lockp) \
	ssi_try_lock_spin_lock(_lock)

#define SSI_ASSERT_LOCKED_SPIN_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_spin_lock(_lockp))

#define SSI_ASSERT_UNLOCKED_SPIN_LOCK(_lockp) \
	SSI_LOCK_ASSERT(!ssi_locked_spin_lock(_lockp))

/*
 * JAG - Global lock counting is not supported in Linux.
 * At least for now, these tests always succeed.
 */
#define SSI_ASSERT_NO_SPIN_LOCKS_HELD() \
	SSI_LOCK_ASSERT(2+2 == 4)
#define NO_SPIN_LOCKS_HELD() \
	1			/* Only use to check affirmative case */

/* RT - spin_unlock() is semi-permeable in Linux.
 */
#define UNLOCK_SPIN_LOCK(_lockp) \
	ssi_unlock_spin_lock(_lockp)

#define DEINIT_SPIN_LOCK(_lockp) \
	ssi_deinit_spin_lock(_lockp)

/*
 * SOFTIRQ Spin Locks (block softirq)
 * ----------
 *
 * The following interfaces are provided:
 *
 *	INIT_SOFTIRQ_SPIN_LOCK(&lock)
 *
 *		This macro initializes the specified lock. This macro cannot
 *		sleep.
 *
 *	LOCK_SOFTIRQ_SPIN_LOCK(&lock)
 *
 *		This macro locks the spin lock.
 *
 *	locked = TRY_SOFTIRQ_LOCK_SPIN_LOCK(&lock)
 *
 *		This macro does not spin, but instead returns non-zero
 *		if the lock was acquired and 0 if unsuccessful.  On
 *		a uniprocessor this macro always returns non-zero.
 *
 *	SSI_ASSERT_LOCKED_SOFTIRQ_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain spin
 *		lock is currently held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	SSI_ASSERT_UNLOCKED_SOFTIRQ_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain spin
 *		lock is not currently held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	UNLOCK_SOFTIRQ_SPIN_LOCK(&lock)
 *
 *		This macro unlocks a held spin lock.
 *
 *	DEINIT_SOFTIRQ_SPIN_LOCK(&lock)
 *
 *		This macro deallocates any data structures that may be
 *		associated with a spin lock.  On some systems this may
 *		be a no-op.
 *
 */

static inline void ssi_init_softirq_spin_lock(SOFTIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_INIT_DEBUG(lockp);
	spin_lock_init(&lockp->isp_lock);
	lockp->isp_owner = NULL;
}

static inline int ssi_locked_softirq_spin_lock(SOFTIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	return (lockp->isp_owner == current);
}

static inline void ssi_lock_softirq_spin_lock(SOFTIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_irq());
	spin_lock_bh(&lockp->isp_lock);
	lockp->isp_owner = current;
}

static inline void ssi_unlock_softirq_spin_lock(SOFTIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_irq());
	SSI_LOCK_ASSERT(ssi_locked_softirq_spin_lock(lockp));
	lockp->isp_owner = NULL;
	spin_unlock_bh(&lockp->isp_lock);
}

static inline int ssi_try_lock_softirq_spin_lock(SOFTIRQ_SPIN_LOCK_T *lockp)
{
	int locked;

	SSI_LOCK_ASSERT_INITED(lockp);
	local_bh_disable();
	locked = spin_trylock(&lockp->isp_lock);
	if (locked)
		lockp->isp_owner = current;
	else
		local_bh_enable();

	return locked;
}

static inline void ssi_deinit_softirq_spin_lock(SOFTIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_DEINIT_DEBUG(lockp);
}

#define INIT_SOFTIRQ_SPIN_LOCK(_lockp) \
	ssi_init_softirq_spin_lock(_lockp)

#define LOCK_SOFTIRQ_SPIN_LOCK(_lockp) \
	ssi_lock_softirq_spin_lock(_lockp)

#define TRY_LOCK_SOFTIRQ_SPIN_LOCK(_lockp) \
	ssi_try_lock_softirq_spin_lock(_lock)

#define SSI_ASSERT_LOCKED_SOFTIRQ_SPIN_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_softirq_spin_lock(_lockp))

#define SSI_ASSERT_UNLOCKED_SOFTIRQ_SPIN_LOCK(_lockp) \
	SSI_LOCK_ASSERT(!ssi_locked_softirq_spin_lock(_lockp))

#define UNLOCK_SOFTIRQ_SPIN_LOCK(_lockp) \
	ssi_unlock_softirq_spin_lock(_lockp)

#define DEINIT_SOFTIRQ_SPIN_LOCK(_lockp) \
	ssi_deinit_softirq_spin_lock(_lockp)

/*
 * HARDIRQ Spin Locks (disables interrupts)
 * ----------
 *
 * The following interfaces are provided:
 *
 *	INIT_HARDIRQ_SPIN_LOCK(&lock)
 *
 *		This macro initializes the specified lock. This macro cannot
 *		sleep.
 *
 *	LOCK_HARDIRQ_SPIN_LOCK(&lock)
 *
 *		This macro locks the spin lock.
 *
 *	locked = TRY_HARDIRQ_LOCK_SPIN_LOCK(&lock)
 *
 *		This macro does not spin, but instead returns non-zero
 *		if the lock was acquired and 0 if unsuccessful.  On
 *		a uniprocessor this macro always returns non-zero.
 *
 *	SSI_ASSERT_LOCKED_HARDIRQ_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain spin
 *		lock is currently held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	SSI_ASSERT_UNLOCKED_HARDIRQ_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain spin
 *		lock is not currently held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	UNLOCK_HARDIRQ_SPIN_LOCK(&lock)
 *
 *		This macro unlocks a held spin lock.
 *
 *	DEINIT_HARDIRQ_SPIN_LOCK(&lock)
 *
 *		This macro deallocates any data structures that may be
 *		associated with a spin lock.  On some systems this may
 *		be a no-op.
 *
 */

static inline void ssi_init_hardirq_spin_lock(HARDIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_INIT_DEBUG(lockp);
	spin_lock_init(&lockp->isp_lock);
	lockp->isp_owner = NULL;
}

static inline int ssi_locked_hardirq_spin_lock(HARDIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	return (lockp->isp_owner == current);
}

static inline void ssi_lock_hardirq_spin_lock(HARDIRQ_SPIN_LOCK_T *lockp)
{
	unsigned long flags;

	SSI_LOCK_ASSERT_INITED(lockp);
	spin_lock_irqsave(&lockp->isp_lock, flags);
	lockp->isp_owner = current;
	lockp->isp_flags = flags;
}

static inline void ssi_unlock_hardirq_spin_lock(HARDIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(ssi_locked_hardirq_spin_lock(lockp));
	lockp->isp_owner = NULL;
	spin_unlock_irqrestore(&lockp->isp_lock, lockp->isp_flags);
}

static inline int ssi_try_lock_hardirq_spin_lock(HARDIRQ_SPIN_LOCK_T *lockp)
{
	unsigned long flags;

	SSI_LOCK_ASSERT_INITED(lockp);

	if (!spin_trylock_irqsave(&lockp->isp_lock, flags))
		return 0;
	lockp->isp_owner = current;
	lockp->isp_flags = flags;
	return 1;
}

static inline void ssi_deinit_hardirq_spin_lock(HARDIRQ_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_DEINIT_DEBUG(lockp);
}

#define INIT_HARDIRQ_SPIN_LOCK(_lockp) \
	ssi_init_hardirq_spin_lock(_lockp)

#define LOCK_HARDIRQ_SPIN_LOCK(_lockp) \
	ssi_lock_hardirq_spin_lock(_lockp)

#define TRY_LOCK_HARDIRQ_SPIN_LOCK(_lockp) \
	ssi_try_lock_hardirq_spin_lock(_lock)

#define SSI_ASSERT_LOCKED_HARDIRQ_SPIN_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_hardirq_spin_lock(_lockp))

#define SSI_ASSERT_UNLOCKED_HARDIRQ_SPIN_LOCK(_lockp) \
	SSI_LOCK_ASSERT(!ssi_locked_hardirq_spin_lock(_lockp))

#define UNLOCK_HARDIRQ_SPIN_LOCK(_lockp) \
	ssi_unlock_hardirq_spin_lock(_lockp)

#define DEINIT_HARDIRQ_SPIN_LOCK(_lockp) \
	ssi_deinit_hardirq_spin_lock(_lockp)

/*
 * Read/Write Spin Locks
 * ---------------------
 *
 * Read/Write spin locks allow for a lock to be locked in either shared
 * or exclusive mode.  NSC code must be implemented to not count on a
 * "true" implementation of shared locks (i.e. on some platforms
 * read/write spin locks may be implemented as regular spin locks).
 *
 * Read/Write spin locks are unnecessary for non-preemptible uniprocessor
 * platforms.
 *
 * The following interfaces are provided:
 *
 *	RW_SPIN_LOCK_T(lock)
 *	STATIC_RW_SPIN_LOCK_T(lock)
 *	EXTERN_RW_SPIN_LOCK_T(lock)
 *
 *		Read/Write spin locks are declared via a macro so that
 *		they can be compiled out of the code for non-preemptible
 *		uniprocessors.
 *
 *	INIT_RW_SPIN_LOCK(&lock)
 *
 *		This macro initializes the specified lock. This macro does
 *		not sleep.
 *
 *	LOCK_EXCL_RW_SPIN_LOCK(&lock)
 *
 *		This macro locks the read/write spin lock exclusive.
 *
 *	TRY_LOCK_EXCL_RW_SPIN_LOCK(&lock)
 *
 *		This macro does not spin, but instead returns
 *		a non-zero value upon successfully acquiring the
 *		lock exclusive, and zero on failure.  On a uniprocessor
 *		this macro always returns success.
 *
 *	SSI_ASSERT_LOCKED_EXCL_RW_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write spin	lock is currently held exclusive by
 *		the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	SSI_ASSERT_UNLOCKED_EXCL_RW_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write spin	lock is not currently held exclusive
 *		by the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	UNLOCK_EXCL_RW_SPIN_LOCK(&lock)
 *
 *		This macro unlocks a read/write spin lock currently
 *		held exclusive.
 *
 *	LOCK_SHR_RW_SPIN_LOCK(&lock)
 *
 *		This macro locks the read/write spin lock shared.
 *
 *	TRY_LOCK_SHR_RW_SPIN_LOCK(&lock)
 *
 *		This macro does not spin, but instead returns a
 *		non-zero value on acquiring the lock shared,
 *		and zero on failure. On a uniprocessor this macro always
 *		returns success.
 *
 *	SSI_ASSERT_LOCKED_SHR_RW_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write spin	lock is currently held shared by
 *		the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	SSI_ASSERT_UNLOCKED_SHR_RW_SPIN_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write spin	lock is not currently held shared by
 *		the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	UNLOCK_SHR_RW_SPIN_LOCK(&lockl)
 *
 *		This macro unlocks a read/write spin lock currently
 *		held shared.
 *
 *	DEINIT_RW_SPIN_LOCK(&lock)
 *
 *		This macro deallocates any data structures that may be
 *		associated with a read/write spin lock.  On some systems
 *		this may be a no-op.
 */

static inline void ssi_init_rw_spin_lock(RW_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_INIT_DEBUG(lockp);
	lockp->rwsp_lock = RW_LOCK_UNLOCKED;
}

static inline void ssi_lock_shr_rw_spin_lock(RW_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	read_lock(&lockp->rwsp_lock);
}

static inline void ssi_unlock_shr_rw_spin_lock(RW_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	read_unlock(&lockp->rwsp_lock);
}

static inline void ssi_lock_excl_rw_spin_lock(RW_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	write_lock(&lockp->rwsp_lock);
}

static inline void ssi_unlock_excl_rw_spin_lock(RW_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	write_unlock(&lockp->rwsp_lock);
}

static inline void ssi_deinit_rw_spin_lock(RW_SPIN_LOCK_T *lockp)
{
	SSI_LOCK_DEINIT_DEBUG(lockp);
}

#define INIT_RW_SPIN_LOCK(_lockp) \
	ssi_init_rw_spin_lock(_lockp)

#define LOCK_EXCL_RW_SPIN_LOCK(_lockp) \
	ssi_lock_excl_rw_spin_lock(_lockp)

#define UNLOCK_EXCL_RW_SPIN_LOCK(_lockp) \
	ssi_unlock_excl_rw_spin_lock(_lockp)

#define LOCK_SHR_RW_SPIN_LOCK(_lockp) \
	ssi_lock_shr_rw_spin_lock(_lockp)

#define UNLOCK_SHR_RW_SPIN_LOCK(_lockp) \
	ssi_unlock_shr_rw_spin_lock(_lockp)

#define DEINIT_RW_SPIN_LOCK(_lockp) \
	ssi_deinit_rw_spin_lock(_lockp)

/*
 * Sleep Locks
 * -----------
 *
 * Sleep locks provide simple mutual exclusion.  Sleep locks cannot
 * be interrupted by signals.
 *
 * The following interfaces are provided:
 *
 *	INIT_LOCK(&lock)
 *
 *		This macro initializes the specified lock. This macro does
 *		not sleep.
 *
 *	LOCK_LOCK(&lock)
 *
 *		This macro locks the sleep lock.
 *
 *	TRY_LOCK_LOCK(&lock)
 *
 *		This macro returns a non-zero value if the lock was
 *		acquired and 0 if unsuccessful.
 *
 *	LOCK_LOCKOWNED(&lock)
 *
 *		This macro returns a non-zero value if the current
 *		context already has the specified lock held.
 *
 *	SSI_ASSERT_LOCKED_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain sleep
 *		lock is currently held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	SSI_ASSERT_UNLOCKED_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain sleep
 *		lock is currently not held by the current context.  This
 *		macro will not fail on systems that cannot support this
 *		check.
 *
 *	UNLOCK_LOCK(&lock)
 *
 *		This macro unlocks a held sleep lock.
 *
 *	DEINIT_LOCK(&lock)
 *
 *		This macro deallocates any data structures that may be
 *		associated with a sleep lock.  On some systems this may
 *		be a no-op.
 *
 *	JAG - Semaphores serve this purpose in Linux.
 */

static inline void ssi_init_lock(LOCK_T *lockp)
{
	SSI_LOCK_INIT_DEBUG(lockp);
	init_MUTEX(&lockp->sl_semaphore);
	lockp->sl_owner = NULL;
}

static inline void ssi_lock_lock(LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	down(&lockp->sl_semaphore);
	lockp->sl_owner = current;
}

static inline int ssi_try_lock_lock(LOCK_T *lockp)
{
	int locked;

	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	locked = !down_trylock(&lockp->sl_semaphore);
	if (locked)
		lockp->sl_owner = current;

	return locked;
}

static inline int ssi_lock_lockowned(LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	return (lockp->sl_owner == current);
}

static inline void ssi_unlock_lock(LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
#if __SSI_LOCK_DEBUG
	if (!ssi_lock_lockowned(lockp)) {
		SSI_LOCK_ASSERT(lockp->sl_owner != NULL);
		if (lockp->sl_owner != NULL)
			printk(KERN_DEBUG
			       "ssi_unlock_lock:"
			       " lock not owned by this process: 0x%p\n",
			       lockp);
	}
#endif
	SSI_LOCK_ASSERT(ssi_lock_lockowned(lockp));
	lockp->sl_owner = NULL;
	up(&lockp->sl_semaphore);
}

static inline void ssi_deinit_lock(LOCK_T *lockp)
{
	SSI_LOCK_DEINIT_DEBUG(lockp);
}

#define INIT_LOCK(_lockp) \
	ssi_init_lock(_lockp)

#define LOCK_LOCK(_lockp) \
	ssi_lock_lock(_lockp)

#define TRY_LOCK_LOCK(_lockp) \
	ssi_try_lock_lock(_lockp)

#define	LOCK_LOCKOWNED(_lockp)\
	ssi_lock_lockowned(_lockp)

#define SSI_ASSERT_LOCKED_LOCK(_lockp) \
	SSI_LOCK_ASSERT(LOCK_LOCKOWNED(_lockp))

#define SSI_ASSERT_UNLOCKED_LOCK(_lockp) \
	SSI_LOCK_ASSERT(!LOCK_LOCKOWNED(_lockp))

#define UNLOCK_LOCK(_lockp) \
	ssi_unlock_lock(_lockp)

#define DEINIT_LOCK(_lockp) \
	ssi_deinit_lock(_lockp)

/*
 * Read/Write Sleep Locks
 * ----------------------
 *
 * Read/Write sleep locks allow for a lock to be locked in either shared
 * or exclusive mode.  Read/write sleep locks cannot be interrupted by
 * signals.
 *
 * The following interfaces are provided:
 *
 *	INIT_RW_LOCK(&lock)
 *
 *		This macro initializes the specified lock. This macro does
 *		not sleep.
 *
 *	LOCK_EXCL_RW_LOCK(&lock)
 *
 *		This macro locks the read/write sleep lock exclusive.
 *
 *	TRY_LOCK_EXCL_RW_LOCK(&lock)
 *
 *		This macro does not sleep, but instead returns
 *		a non-zero value upon successfully acquiring the
 *		sleep lock exclusive, and zero on failure.
 *
 *	SSI_ASSERT_LOCKED_EXCL_RW_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write sleep lock is currently held exclusive by
 *		the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	SSI_ASSERT_UNLOCKED_EXCL_RW_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write sleep lock is not currently held exclusive
 *		by the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	UNLOCK_EXCL_RW_LOCK(&lock)
 *
 *		This macro unlocks a read/write sleep lock currently
 *		held exclusive.
 *
 *	LOCK_SHR_RW_LOCK(&lock)
 *
 *		This macro locks the read/write sleep lock shared.
 *
 *	TRY_LOCK_SHR_RW_LOCK(&lock)
 *
 *		This macro does not sleep, but instead returns a
 *		non-zero value on acquiring the lock shared,
 *		and zero on failure.
 *
 *	SSI_ASSERT_LOCKED_SHR_RW_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write sleep lock is currently held shared by
 *		the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	SSI_ASSERT_UNLOCKED_SHR_RW_LOCK(&lock)
 *
 *		This is an assert macro that checks that a certain
 *		read/write sleep lock is not currently held shared
 *		by the current context.  This macro will not fail on
 *		systems that cannot support this check.
 *
 *	UNLOCK_SHR_RW_LOCK(&lock)
 *
 *		This macro unlocks a read/write sleep lock currently
 *		held shared.
 *
 *	DEINIT_RW_LOCK(&lock)
 *
 *		This macro deallocates any data structures that may be
 *		associated with a read/write sleep lock.  On some systems
 *		this may be a no-op.
 *
 *	JAG - Read/write semaphores serve this purpose in Linux.
 */

static inline void ssi_init_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_INIT_DEBUG(lockp);
	init_rwsem(&lockp->rwsl_semaphore);
	lockp->rwsl_owner = NULL;
}

static inline int ssi_locked_excl_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	return lockp->rwsl_owner == current;
}

static inline void ssi_lock_excl_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	down_write(&lockp->rwsl_semaphore);
	lockp->rwsl_owner = current;
}

static inline int ssi_try_lock_excl_rw_lock(RW_LOCK_T *lockp)
{
	int locked;

	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	locked = down_write_trylock(&lockp->rwsl_semaphore);
	if (locked)
		lockp->rwsl_owner = current;

	return locked;
}

static inline void ssi_unlock_excl_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
#if __SSI_LOCK_DEBUG
	if (!ssi_locked_excl_rw_lock(lockp)) {
		SSI_LOCK_ASSERT(lockp->rwsl_owner != NULL);
		if (lockp->rwsl_owner != NULL)
			printk(KERN_DEBUG
			       "ssi_unlock_excl_rw_lock:"
			       " lock not owned by this process: 0x%p\n",
			       lockp);
	}
#endif
	lockp->rwsl_owner = NULL;
	up_write(&lockp->rwsl_semaphore);
}

static inline void ssi_downgrade_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
#if __SSI_LOCK_DEBUG
	if (!ssi_locked_excl_rw_lock(lockp)) {
		SSI_LOCK_ASSERT(lockp->rwsl_owner != NULL);
		if (lockp->rwsl_owner != NULL)
			printk(KERN_DEBUG
			       "ssi_unlock_excl_rw_lock:"
			       " lock not owned by this process: 0x%p\n",
			       lockp);
	}
#endif
	lockp->rwsl_owner = NULL;
	downgrade_write(&lockp->rwsl_semaphore);
}

static inline int ssi_locked_shr_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	if (down_write_trylock(&lockp->rwsl_semaphore)) {
		up_write(&lockp->rwsl_semaphore);
		return 0;
	}

	/*
	 * The lock may be locked exclusive or shared, but it isn't unlocked;
	 * this is the best we can do for now.
	 */
	return 1;
}

static inline void ssi_lock_shr_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	down_read(&lockp->rwsl_semaphore);
}

static inline int ssi_try_lock_shr_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	return down_read_trylock(&lockp->rwsl_semaphore);
}

static inline void ssi_unlock_shr_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_ASSERT_INITED(lockp);
	SSI_LOCK_ASSERT(!in_interrupt());
	SSI_LOCK_ASSERT(ssi_locked_shr_rw_lock(lockp));
	up_read(&lockp->rwsl_semaphore);
}

static inline void ssi_deinit_rw_lock(RW_LOCK_T *lockp)
{
	SSI_LOCK_DEINIT_DEBUG(lockp);
}

#define INIT_RW_LOCK(_lockp) \
	ssi_init_rw_lock(_lockp)

#define LOCK_EXCL_RW_LOCK(_lockp) \
	ssi_lock_excl_rw_lock(_lockp)

#define TRY_LOCK_EXCL_RW_LOCK(_lockp) \
	ssi_try_lock_excl_rw_lock(_lockp)

#define UNLOCK_EXCL_RW_LOCK(_lockp) \
	ssi_unlock_excl_rw_lock(_lockp)

#define DOWNGRADE_RW_LOCK(_lockp) \
	ssi_downgrade_rw_lock(_lockp)

#define LOCK_SHR_RW_LOCK(_lockp) \
	ssi_lock_shr_rw_lock(_lockp)

#define TRY_LOCK_SHR_RW_LOCK(_lockp) \
	ssi_try_lock_shr_rw_lock(_lockp)

#define UNLOCK_SHR_RW_LOCK(_lockp) \
	ssi_unlock_shr_rw_lock(_lockp)

#define LOCK_EXCL_RW_LOCKOWNED(_lockp) \
	ssi_locked_excl_rw_lock(_lockp)

#define SSI_ASSERT_LOCKED_EXCL_RW_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_excl_rw_lock(_lockp))

#define SSI_ASSERT_LOCKED_SHR_RW_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_shr_rw_lock(_lockp))

#define DEINIT_RW_LOCK(_lockp) \
	ssi_deinit_rw_lock(_lockp)

/*
 * Condition locks
 * ---------------
 *
 * Condition locks are locks associated with a condition variable.
 * These locks may be either spin locks or normal sleep locks,
 * depending on the condition variable primitives supported by the
 * underlying platform.  Locking the condition lock may cause
 * either spinning or sleeping and NSC code should be prepared
 * for either case.  Condition locks compile out on non-preemptible
 * uniprocessor platforms.  A subset of the full spin lock interfaces
 * are provided.
 *
 * Force typechecking even though it is the same as a SPIN_LOCK_T by
 * using wrappers.
 */

static inline void ssi_init_cond_lock(COND_LOCK_T *lockp)
{
	ssi_init_spin_lock(&lockp->lock);
}

static inline void ssi_lock_cond_lock(COND_LOCK_T *lockp)
{
	ssi_lock_spin_lock(&lockp->lock);
}

static inline int ssi_try_lock_cond_lock(COND_LOCK_T *lockp)
{
	return ssi_try_lock_spin_lock(&lockp->lock);
}

static inline int ssi_locked_cond_lock(COND_LOCK_T *lockp)
{
	return ssi_locked_spin_lock(&lockp->lock);
}

static inline void ssi_unlock_cond_lock(COND_LOCK_T *lockp)
{
	ssi_unlock_spin_lock(&lockp->lock);
}

static inline void ssi_deinit_cond_lock(COND_LOCK_T *lockp)
{
	ssi_deinit_spin_lock(&lockp->lock);
}

#define INIT_COND_LOCK(_lockp) \
	ssi_init_cond_lock(_lockp)

#define LOCK_COND_LOCK(_lockp) \
	ssi_lock_cond_lock(_lockp)

#define LOCK_COND_LOCKOWNED(_lockp) \
	ssi_locked_cond_lock(_lockp)

#define TRY_LOCK_COND_LOCK(_lockp) \
	ssi_try_lock_cond_lock(_lockp)

#define SSI_ASSERT_LOCKED_COND_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_cond_lock(_lockp))

#define SSI_ASSERT_UNLOCKED_COND_LOCK(_lockp) \
	SSI_LOCK_ASSERT(!ssi_locked_cond_lock(_lockp))

#define UNLOCK_COND_LOCK(_lockp) \
	ssi_unlock_cond_lock(_lockp)

#define DEINIT_COND_LOCK(_lockp) \
	ssi_deinit_cond_lock(_lockp)

/*
 * SOFTIRQ Condition locks (block soft interrupts)
 * ---------------
 *
 * Condition locks are locks associated with a condition variable.
 * These locks may be either spin locks or normal sleep locks,
 * depending on the condition variable primitives supported by the
 * underlying platform.  Locking the condition lock may cause
 * either spinning or sleeping and NSC code should be prepared
 * for either case.  Condition locks compile out on non-preemptible
 * uniprocessor platforms.  A subset of the full spin lock interfaces
 * are provided.
 *
 * Force typechecking even though it is the same as a SPIN_LOCK_T by
 * using wrappers.
 */

static inline void ssi_init_softirq_cond_lock(SOFTIRQ_COND_LOCK_T *lockp)
{
	ssi_init_softirq_spin_lock(&lockp->lock);
}

static inline void ssi_lock_softirq_cond_lock(SOFTIRQ_COND_LOCK_T *lockp)
{
	ssi_lock_softirq_spin_lock(&lockp->lock);
}

static inline int ssi_try_lock_softirq_cond_lock(SOFTIRQ_COND_LOCK_T *lockp)
{
	return ssi_try_lock_softirq_spin_lock(&lockp->lock);
}

static inline int ssi_locked_softirq_cond_lock(SOFTIRQ_COND_LOCK_T *lockp)
{
	return ssi_locked_softirq_spin_lock(&lockp->lock);
}

static inline void ssi_unlock_softirq_cond_lock(SOFTIRQ_COND_LOCK_T *lockp)
{
	ssi_unlock_softirq_spin_lock(&lockp->lock);
}

static inline void ssi_deinit_softirq_cond_lock(SOFTIRQ_COND_LOCK_T *lockp)
{
	ssi_deinit_softirq_spin_lock(&lockp->lock);
}

#define INIT_SOFTIRQ_COND_LOCK(_lockp) \
	ssi_init_softirq_cond_lock(_lockp)

#define LOCK_SOFTIRQ_COND_LOCK(_lockp) \
	ssi_lock_softirq_cond_lock(_lockp)

#define TRY_LOCK_SOFTIRQ_COND_LOCK(_lockp) \
	ssi_try_lock_softirq_cond_lock(_lockp)

#define SSI_ASSERT_LOCKED_SOFTIRQ_COND_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_softirq_cond_lock(_lockp))

#define SSI_ASSERT_UNLOCKED_SOFTIRQ_COND_LOCK(_lockp) \
	SSI_LOCK_ASSERT(!ssi_locked_softirq_cond_lock(_lockp))

#define UNLOCK_SOFTIRQ_COND_LOCK(_lockp) \
	ssi_unlock_softirq_cond_lock(_lockp)

#define DEINIT_SOFTIRQ_COND_LOCK(_lockp) \
	ssi_deinit_softirq_cond_lock(_lockp)

/*
 * HARDIRQ Condition locks (blocks interrupts)
 * ---------------
 *
 * Condition locks are locks associated with a condition variable.
 * These locks may be either spin locks or normal sleep locks,
 * depending on the condition variable primitives supported by the
 * underlying platform.  Locking the condition lock may cause
 * either spinning or sleeping and NSC code should be prepared
 * for either case.  Condition locks compile out on non-preemptible
 * uniprocessor platforms.  A subset of the full spin lock interfaces
 * are provided.
 *
 * Force typechecking even though it is the same as a SPIN_LOCK_T by
 * using wrappers.
 */

static inline void ssi_init_hardirq_cond_lock(HARDIRQ_COND_LOCK_T *lockp)
{
	ssi_init_hardirq_spin_lock(&lockp->lock);
}

static inline void ssi_lock_hardirq_cond_lock(HARDIRQ_COND_LOCK_T *lockp)
{
	ssi_lock_hardirq_spin_lock(&lockp->lock);
}

static inline int ssi_try_lock_hardirq_cond_lock(HARDIRQ_COND_LOCK_T *lockp)
{
	return ssi_try_lock_hardirq_spin_lock(&lockp->lock);
}

static inline int ssi_locked_hardirq_cond_lock(HARDIRQ_COND_LOCK_T *lockp)
{
	return ssi_locked_hardirq_spin_lock(&lockp->lock);
}

static inline void ssi_unlock_hardirq_cond_lock(HARDIRQ_COND_LOCK_T *lockp)
{
	ssi_unlock_hardirq_spin_lock(&lockp->lock);
}

static inline void ssi_deinit_hardirq_cond_lock(HARDIRQ_COND_LOCK_T *lockp)
{
	ssi_deinit_hardirq_spin_lock(&lockp->lock);
}

#define INIT_HARDIRQ_COND_LOCK(_lockp) \
	ssi_init_hardirq_cond_lock(_lockp)

#define LOCK_HARDIRQ_COND_LOCK(_lockp) \
	ssi_lock_hardirq_cond_lock(_lockp)

#define TRY_LOCK_HARDIRQ_COND_LOCK(_lockp) \
	ssi_try_lock_hardirq_cond_lock(_lockp)

#define SSI_ASSERT_LOCKED_HARDIRQ_COND_LOCK(_lockp) \
	SSI_LOCK_ASSERT(ssi_locked_hardirq_cond_lock(_lockp))

#define SSI_ASSERT_UNLOCKED_HARDIRQ_COND_LOCK(_lockp) \
	SSI_LOCK_ASSERT(!ssi_locked_hardirq_cond_lock(_lockp))

#define UNLOCK_HARDIRQ_COND_LOCK(_lockp) \
	ssi_unlock_hardirq_cond_lock(_lockp)

#define DEINIT_HARDIRQ_COND_LOCK(_lockp) \
	ssi_deinit_hardirq_cond_lock(_lockp)

/*
 * Non-Interruptable Condition Variable Primitives
 * -----------------------------------------------
 *
 * The following interfaces are provided for condition variables
 * that are never interrupted by POSIX signals:
 *
 *	INIT_CONDITION(&cond)
 *
 *		This macro initializes the specified condition
 *		variable. This macro does not sleep.
 *
 *	WAIT_CONDITION(&cond, &cond_lock)
 *
 *		Sleep waiting for a certain condition to be posted
 *		via either SIGNAL_CONDITION() or BROADCAST_CONDITION().
 *		The associated lock should of type COND_LOCK_T.  The
 *		condition lock is atomically unlocked when the
 *		context goes to sleep, and is relocked upon return.
 *		Note that the condition variable is not necessarily
 *		relocked atomically (some platforms may not support
 *		this).
 *		All code that calls WAIT_CONDITION() must tolerate
 *		premature wakeups.  After waking up, the condition
 *		must be rechecked (see the example below).
 *
 *
 *	WAIT_CONDITION_NORELCK(&cond, &cond_lock)
 *
 *		Sleep waiting for a certain condition to be posted
 *		via either SIGNAL_CONDITION() or BROADCAST_CONDITION().
 *		The associated lock should of type COND_LOCK_T.  The
 *		condition lock is atomically unlocked when the
 *		context goes to sleep.  It is NOT relocked upon return.
 *		All code that calls WAIT_CONDITION_NORELCK() must tolerate
 *		premature wakeups.  After waking up, the condition
 *		must be rechecked (see the example below).
 *
 *
 *	SIGNAL_CONDITION(&cond)
 *
 *		Wake up a single context currently waiting on the
 *		specified condition variable via WAIT_CONDITION() or
 *		WAIT_CONDITION_NORELCK().
 *
 *
 *		SIGNAL_CONDITION() should not be called	with the
 *		condition lock held.  On platforms where condition
 *		locks are sleep locks this could cause spurious
 *		scheduling activity.  This would occur if a context
 *		were to wakeup and then go to sleep again on the
 *		condition lock.
 *
 *	BROADCAST_CONDITION(&cond)
 *
 *		Wake up all contexts currently waiting on the
 *		specified condition variable via WAIT_CONDITION() or
 *		WAIT_CONDITION_NORELCK().
 *
 *
 *		BROADCAST_CONDITION() should not be called with the
 *		condition lock held.  On platforms where condition
 *		locks are sleep locks this could cause spurious
 *		scheduling activity.  This would occur if a context
 *		were to wakeup and then go to sleep again on the
 *		condition lock.
 *
 *	DEINIT_CONDITION(&cond)
 *
 *		This macro deallocates any data structures that may be
 *		associated with the condition variable.  On some systems
 *		this may be a no-op.
 *
 *
 * Examples:
 *
 *	Context waiting on a condition:
 *
 *		LOCK_COND_LOCK(&cond_lock);
 *		while (!condition) {
 *			WAIT_CONDITION(&cond, &cond_lock);
 *				:
 *				:
 *		}
 *			:
 *			:
 *		UNLOCK_COND_LOCK(&cond_lock);
 *
 *	Context signalling condition:
 *
 *		LOCK_COND_LOCK(&cond_lock);
 *		<Set the condition>
 *		UNLOCK_COND_LOCK(&cond_lock);
 *		SIGNAL_CONDITION(&cond);
 */

static inline void ssi_init_condition(CONDITION_T *condp)
{
	SSI_LOCK_INIT_DEBUG(condp);
	init_waitqueue_head(&condp->cnd_wqh);
}

/* Modelled after linux/wait.h:wait_event_interruptible() */
static inline void ssi_wait_condition(CONDITION_T *condp, COND_LOCK_T *lockp,
				      int intr, int *was_intrp, int relock)
{
	DEFINE_WAIT(wait);
	sigset_t old_blocked;
	struct task_struct *curtask = current;

	SSI_LOCK_ASSERT_INITED(condp);
	SSI_LOCK_ASSERT(!in_interrupt());
	if (!intr)
		ssi_block_signals(curtask, &old_blocked);
	prepare_to_wait(&condp->cnd_wqh, &wait, TASK_INTERRUPTIBLE);
	UNLOCK_COND_LOCK(lockp);
	if (was_intrp) {
		*was_intrp = intr ? signal_pending(curtask) : 0;
		if (*was_intrp)
			goto out;
	}
	schedule();
out:
	finish_wait(&condp->cnd_wqh, &wait);
	if (!intr)
		ssi_unblock_signals(curtask, &old_blocked);
	if (relock)
		LOCK_COND_LOCK(lockp);
}

/* Modelled after linux/wait.h:wait_event_interruptible() */
static inline void ssi_wait_softirq_condition(CONDITION_T *condp,
					      SOFTIRQ_COND_LOCK_T *lockp,
					      int intr, int *was_intrp,
					      int relock)
{
	DEFINE_WAIT(wait);
	sigset_t old_blocked;
	struct task_struct *curtask = current;

	SSI_LOCK_ASSERT_INITED(condp);
	SSI_LOCK_ASSERT(!in_irq());
	if (!intr)
		ssi_block_signals(curtask, &old_blocked);
	prepare_to_wait(&condp->cnd_wqh, &wait, TASK_INTERRUPTIBLE);
	UNLOCK_SOFTIRQ_COND_LOCK(lockp);
	if (was_intrp) {
		*was_intrp = intr ? signal_pending(curtask) : 0;
		if (*was_intrp)
			goto out;
	}
	schedule();
out:
	finish_wait(&condp->cnd_wqh, &wait);
	if (!intr)
		ssi_unblock_signals(curtask, &old_blocked);
	if (relock)
		LOCK_SOFTIRQ_COND_LOCK(lockp);
}

/* Modelled after linux/wait.h:wait_event_interruptible() */
static inline void ssi_wait_hardirq_condition(CONDITION_T *condp,
					      HARDIRQ_COND_LOCK_T *lockp,
					      int intr, int *was_intrp,
					      int relock)
{
	DEFINE_WAIT(wait);
	sigset_t old_blocked;
	struct task_struct *curtask = current;

	SSI_LOCK_ASSERT_INITED(condp);
	SSI_LOCK_ASSERT(!in_irq());
	if (!intr)
		ssi_block_signals(curtask, &old_blocked);
	prepare_to_wait(&condp->cnd_wqh, &wait, TASK_INTERRUPTIBLE);
	UNLOCK_HARDIRQ_COND_LOCK(lockp);
	if (was_intrp) {
		*was_intrp = intr ? signal_pending(curtask) : 0;
		if (*was_intrp)
			goto out;
	}
	schedule();
out:
	finish_wait(&condp->cnd_wqh, &wait);
	if (!intr)
		ssi_unblock_signals(curtask, &old_blocked);
	if (relock)
		LOCK_HARDIRQ_COND_LOCK(lockp);
}

static inline void ssi_signal_condition(CONDITION_T *condp)
{
	SSI_LOCK_ASSERT_INITED(condp);

	smp_mb(); /* prevent speculative execution */

	wake_up(&condp->cnd_wqh);
}

static inline void ssi_broadcast_condition(CONDITION_T *condp)
{
	SSI_LOCK_ASSERT_INITED(condp);

	smp_mb(); /* prevent speculative execution */

	wake_up_all(&condp->cnd_wqh);
}

static inline void ssi_deinit_condition(CONDITION_T *condp)
{
	SSI_LOCK_DEINIT_DEBUG(condp);
}

#define INIT_CONDITION(_condp) \
	ssi_init_condition(_condp)

#define WAIT_CONDITION_NORELCK(_condp, _cond_lockp) \
	ssi_wait_condition(_condp, _cond_lockp, 0, NULL, 0)

#define WAIT_CONDITION(_condp, _cond_lockp) \
	ssi_wait_condition(_condp, _cond_lockp, 0, NULL, 1)

#define WAIT_SOFTIRQ_CONDITION_NORELCK(_condp, _cond_lockp) \
	ssi_wait_softirq_condition(_condp, _cond_lockp, 0, NULL, 0)

#define WAIT_SOFTIRQ_CONDITION(_condp, _cond_lockp) \
	ssi_wait_softirq_condition(_condp, _cond_lockp, 0, NULL, 1)

#define WAIT_HARDIRQ_CONDITION_NORELCK(_condp, _cond_lockp) \
	ssi_wait_hardirq_condition(_condp, _cond_lockp, 0, NULL, 0)

#define WAIT_HARDIRQ_CONDITION(_condp, _cond_lockp) \
	ssi_wait_hardirq_condition(_condp, _cond_lockp, 0, NULL, 1)

#define SIGNAL_CONDITION(_condp) \
	ssi_signal_condition(_condp)

#define BROADCAST_CONDITION(_condp) \
	ssi_broadcast_condition(_condp)

#define DEINIT_CONDITION(_condp) \
	ssi_deinit_condition(_condp)

/*
 * Interruptable Condition Variable Primitives
 * -------------------------------------------
 *
 * The following interfaces are provided for condition variables
 * that can potentially be interrupted by POSIX signals:
 *
 *	INIT_INTR_CONDITION(&cond)
 *
 *		This macro initializes the specified condition
 *		variable. This macro does not sleep.
 *
 *	WAIT_INTR_CONDITION(&cond, &cond_lock, is_intr, &was_intr)
 *
 *		Sleep waiting for a certain condition to be posted
 *		or for a signal to interrupt the wait.  The is_intr
 *		parameter is a boolean specifying whether the
 *		wait is interruptible by POSIX signals.  The
 *		was_intr flag specifies whether the wait was
 *		interrupted by POSIX signals (B_TRUE is returned)
 *		or woken up by either SIGNAL_INTR_CONDITION() or
 *		BROADCAST_INTR_CONDITION() (B_FALSE is returned).
 *
 *		The associated lock should of type COND_LOCK_T.  The
 *		condition lock is atomically unlocked when the context
 *		goes to sleep, and is relocked upon return.  Note that
 *		the condition variable is not necessarily relocked
 *		atomically (some platforms may not support this).
 *
 *		All code that calls WAIT_INTR_CONDITION() must tolerate
 *		premature wakeups.  After waking up, the condition
 *		must be rechecked (see the example below).
 *
 *	SIGNAL_INTR_CONDITION(&cond)
 *
 *		Wake up a single context currently waiting on the
 *		specified condition variable via WAIT_INTR_CONDITION().
 *
 *		SIGNAL_INTR_CONDITION() should not be called with the
 *		condition lock held.  On platforms where condition locks
 *		are sleep locks this could cause spurious scheduling
 *		activity.  This would occur if a context were to wakeup
 *		and then go to sleep again on the condition lock.
 *
 *	BROADCAST_INTR_CONDITION(&cond)
 *
 *		Wake up all contexts currently waiting on the
 *		specified condition variable via WAIT_INTR_CONDITION().
 *
 *		BROADCAST_INTR_CONDITION() should not be called	with the
 *		condition lock held.  On platforms where condition locks
 *		are sleep locks this could cause spurious scheduling
 *		activity.  This would occur if a context were to wakeup
 *		and then go to sleep again on the condition lock.
 *
 *	DEINIT_INTR_CONDITION(&cond)
 *
 *		This macro deallocates any data structures that may be
 *		associated with the condition variable.  On some systems
 *		this may be a no-op.
 *
 * Examples:
 *
 *	Context waiting on a condition:
 *
 *		LOCK_COND_LOCK(&cond_lock);
 *		while (!condition) {
 *			WAIT_INTR_CONDITION(&cond, &cond_lock);
 *				:
 *				:
 *		}
 *			:
 *			:
 *		UNLOCK_COND_LOCK(&cond_lock);
 *
 *	Context signalling condition:
 *
 *		LOCK_COND_LOCK(&cond_lock);
 *		<Set the condition>
 *		UNLOCK_COND_LOCK(&cond_lock);
 *		SIGNAL_INTR_CONDITION(&cond);
 *
 *	Force typechecking even though it is the same as a CONDITION_T by
 *	using wrappers.
 */

static inline void ssi_init_intr_condition(INTR_CONDITION_T *condp)
{
	ssi_init_condition(&condp->cndi_cond);
}

static inline void ssi_wait_intr_condition(INTR_CONDITION_T *condp,
					   COND_LOCK_T *lockp,
					   int intr, int *was_intrp, int relock)
{
	ssi_wait_condition(&condp->cndi_cond, lockp, intr, was_intrp, relock);
}

static inline void ssi_wait_intr_softirq_condition(INTR_CONDITION_T *condp,
						   SOFTIRQ_COND_LOCK_T *lockp,
						   int intr, int *was_intrp,
						   int relock)
{
	ssi_wait_softirq_condition(&condp->cndi_cond, lockp, intr,
				   was_intrp, relock);
}

static inline void ssi_wait_intr_hardirq_condition(INTR_CONDITION_T *condp,
						   HARDIRQ_COND_LOCK_T *lockp,
						   int intr, int *was_intrp,
						   int relock)
{
	ssi_wait_hardirq_condition(&condp->cndi_cond, lockp, intr,
				   was_intrp, relock);
}

static inline void ssi_signal_intr_condition(INTR_CONDITION_T *condp)
{
	ssi_signal_condition(&condp->cndi_cond);
}

static inline void ssi_broadcast_intr_condition(INTR_CONDITION_T *condp)
{
	ssi_broadcast_condition(&condp->cndi_cond);
}

static inline void ssi_deinit_intr_condition(INTR_CONDITION_T *condp)
{
	ssi_deinit_condition(&condp->cndi_cond);
}

#define INIT_INTR_CONDITION(_condp) \
	ssi_init_intr_condition(_condp)

#define WAIT_INTR_CONDITION_NORELCK(_condp, _cond_lockp,  \
				    _is_intr, _was_intrp) \
	ssi_wait_intr_condition(_condp, _cond_lockp, _is_intr, _was_intrp, 0)

#define WAIT_INTR_CONDITION(_condp, _cond_lockp, _is_intr, _was_intrp) \
	ssi_wait_intr_condition(_condp, _cond_lockp, _is_intr, _was_intrp, 1)

#define WAIT_INTR_SOFTIRQ_CONDITION_NORELCK(_condp, _cond_lockp, \
					    _is_intr, _was_intrp) \
	ssi_wait_intr_softirq_condition(_condp, _cond_lockp, _is_intr, \
					_was_intrp, 0)

#define WAIT_INTR_SOFTIRQ_CONDITION(_condp, _cond_lockp, _is_intr, _was_intrp) \
	ssi_wait_intr_softirq_condition(_condp, _cond_lockp, _is_intr, \
					_was_intrp, 1)

#define WAIT_INTR_HARDIRQ_CONDITION_NORELCK(_condp, _cond_lockp, \
					    _is_intr, _was_intrp) \
	ssi_wait_intr_hardirq_condition(_condp, _cond_lockp, _is_intr, \
					_was_intrp, 0)

#define WAIT_INTR_HARDIRQ_CONDITION(_condp, _cond_lockp, _is_intr, _was_intrp) \
	ssi_wait_intr_hardirq_condition(_condp, _cond_lockp, _is_intr, \
					_was_intrp, 1)

#define SIGNAL_INTR_CONDITION(_condp) \
	ssi_signal_intr_condition(_condp)

#define BROADCAST_INTR_CONDITION(_condp) \
	ssi_broadcast_intr_condition(_condp)

#define DEINIT_INTR_CONDITION(_condp) \
	ssi_deinit_intr_condition(_condp)

/*
 * Non-Interruptable Event Variable Primitives
 * -------------------------------------------
 *
 * The following interfaces are provided for event variables that are
 * never interrupted by POSIX signals. Events are NOT counted, i.e. if
 * SIGNAL_EVENT() is called multiple times between calls to WAIT_EVENT(),
 * WAIT_EVENT() will only be woken-up once.
 *
 *	INIT_EVENT(&event)
 *
 *		This macro initializes the specified event
 *		variable. This macro does not sleep.
 *
 *	WAIT_EVENT(&event)
 *
 *		Sleep waiting for a certain event to be posted
 *		via either SIGNAL_EVENT() or BROADCAST_EVENT().
 *
 *		If there is already an event queued when WAIT_EVENT()
 *		is called, WAIT_EVENT() will not go to sleep.
 *
 *	SIGNAL_EVENT(&event)
 *
 *		Wake up a single context currently waiting on the
 *		specified event variable via WAIT_EVENT().
 *
 *	BROADCAST_EVENT(&event)
 *
 *		Wake up all contexts currently waiting on the
 *		specified event variable via WAIT_EVENT().
 *
 *	DEINIT_EVENT(&event)
 *
 *		This macro deallocates any data structures that may be
 *		associated with the event variable.  On some systems
 *		this may be a no-op.
 *
 */

#ifndef LINUX_SSI_EVENT
#ifdef SSI_NOTUSED
static inline void ssi_add_event_wait(EVENT_T *eventp, EVENT_WAIT_T *ewp)
{
	if (list_empty(&ewp->ew_waitlist))
		list_add_tail(&ewp->ew_waitlist, &eventp->ev_waitlist);
}
#endif

static inline int ssi_wake_event(struct list_head *head)
{
	EVENT_WAIT_T *ewp;

	if (list_empty(head))
		return 0;

	ewp = list_entry(head->next, EVENT_WAIT_T, ew_waitlist);
	list_del_init(&ewp->ew_waitlist);
	ewp->ew_state = 1;
	(void)wake_up_process(ewp->ew_proc);

	return 1;
}
#endif

static inline void ssi_init_event(EVENT_T *eventp)
{
	SSI_LOCK_INIT_DEBUG(eventp);
#ifdef LINUX_SSI_EVENT
	init_completion(&eventp->ev_com);
#else
	INIT_LIST_HEAD(&eventp->ev_waitlist);
	spin_lock_init(&eventp->ev_lock);
	eventp->ev_state = 0;
#endif
}

#ifdef LINUX_SSI_EVENT
static inline int __ssi_try_event_locked(EVENT_T *eventp)
{
	if (eventp->ev_done && list_empty(&eventp->ev_wqh.task_list)) {
		eventp->ev_done = 0;
		return 1;
	}
	return 0;
}
#endif

static inline int ssi_try_event(EVENT_T *eventp)
{
	int retval = 0;
	unsigned long flags;

	spin_lock_irqsave(&eventp->ev_lock, flags);
#ifdef LINUX_SSI_EVENT
	retval = __ssi_try_event_locked(eventp);
#else
	if (eventp->ev_state) {
		eventp->ev_state = 0;
		retval = 1;
	}
#endif
	spin_unlock_irqrestore(&eventp->ev_lock, flags);

	return retval;
}

static void ssi_wait_event(EVENT_T *eventp, int intr, int *was_intrp)
{
#ifdef LINUX_SSI_EVENT
	sigset_t old_blocked;
	unsigned long flags;
	int ret = 0;

	SSI_LOCK_ASSERT_INITED(eventp);
	SSI_LOCK_ASSERT(!in_irq());

	might_sleep();

	spin_lock_irqsave(&eventp->ev_lock, flags);
#ifdef SSI_NOTYET
	/* SSI_XXX: Breaks ssi_broadcast_event(). Subsequent waiters miss signal.
	 * This is a direct functional port of the original code.
	 */
	if (__ssi_try_event_locked(eventp))
		/* Lost race with __ssi_signal_common() */
		goto out;
#endif

	/* Variation of wait_for_completion_interruptible() */
	if (!eventp->ev_done) {
		DECLARE_WAITQUEUE(wait, current);

		if (!intr)
			ssi_block_signals(current, &old_blocked);
		add_wait_queue_exclusive_locked(&eventp->ev_wqh, &wait);
		do {
			if (signal_pending(current)) {
				ret = -ERESTARTSYS;
				remove_wait_queue_locked(&eventp->ev_wqh, &wait);
				if (!intr)
					ssi_unblock_signals(current, &old_blocked);
				goto out;
			}
			__set_current_state(TASK_INTERRUPTIBLE);
			spin_unlock_irqrestore(&eventp->ev_lock, flags);
			schedule();
			spin_lock_irqsave(&eventp->ev_lock, flags);
		} while (!eventp->ev_done);
		remove_wait_queue_locked(&eventp->ev_wqh, &wait);
		if (!intr)
			ssi_unblock_signals(current, &old_blocked);
	}
	eventp->ev_done--;
out:
	spin_unlock_irqrestore(&eventp->ev_lock, flags);
	if (was_intrp != NULL)
		*was_intrp = ret;
#else
	EVENT_WAIT_T evwait;
	sigset_t old_blocked;
	task_t *tsk = current;
	unsigned long flags;
	int interrupted = 0;

	SSI_LOCK_ASSERT_INITED(eventp);
	SSI_LOCK_ASSERT(!in_irq());

	INIT_LIST_HEAD(&evwait.ew_waitlist);
	evwait.ew_proc = tsk;
	evwait.ew_state = 0;

	spin_lock_irqsave(&eventp->ev_lock, flags);
	for (;;) {
		/* SSI_XXX: could have raced when s.t. unexpected woke us up. */
		if (eventp->ev_state) {
			eventp->ev_state = 0;
			spin_unlock_irqrestore(&eventp->ev_lock, flags);
			break;
		} else {
			if (!intr)
				ssi_block_signals(tsk, &old_blocked);
			__set_current_state(TASK_INTERRUPTIBLE);
			list_add_tail(&ewp->ew_waitlist, &eventp->ev_waitlist);
			spin_unlock_irqrestore(&eventp->ev_lock, flags);
			schedule();
			if (!intr)
				ssi_unblock_signals(tsk, &old_blocked);
			if (evwait.ew_state)
				break;
			spin_lock_irqsave(&eventp->ev_lock, flags);
			if (!list_empty(&evwait.ew_waitlist))
				list_del_init(&evwait.ew_waitlist);
			if (intr && signal_pending(tsk)) {
				spin_unlock_irqrestore(&eventp->ev_lock, flags);
				interrupted = 1;
				break;
			}
			/*
			 * Something unexpected woke us up. This
			 * has been seen in the remote I/O ops.
			 */
		}
	}
	if (was_intrp != NULL)
		*was_intrp = interrupted;
#endif /* !LINUX_SSI_EVENT */
}

#ifdef LINUX_SSI_EVENT
static inline void __ssi_signal_common(EVENT_T *eventp, int broadcast)
{
	SSI_LOCK_ASSERT_INITED(eventp);
	SSI_LOCK_ASSERT(!in_irq());

	smp_mb(); /* prevent speculative execution */

	if (broadcast)
		complete_all(&eventp->ev_com);
	else
		complete(&eventp->ev_com);
}

static inline void ssi_signal_event(EVENT_T *eventp)
{
	__ssi_signal_common(eventp, 0);
}

static inline void ssi_broadcast_event(EVENT_T *eventp)
{
	__ssi_signal_common(eventp, 1);
}
#else
static inline void ssi_signal_event(EVENT_T *eventp)
{
	unsigned long flags;

	SSI_LOCK_ASSERT_INITED(eventp);
	SSI_LOCK_ASSERT(!in_irq());
	spin_lock_irqsave(&eventp->ev_lock, flags);
	if (eventp->ev_state == 0) {
		if (!ssi_wake_event(&eventp->ev_waitlist))
			eventp->ev_state = 1;
	} else
		SSI_LOCK_ASSERT(list_empty(&eventp->ev_waitlist));
	spin_unlock_irqrestore(&eventp->ev_lock, flags);
}

static inline void ssi_broadcast_event(EVENT_T *eventp)
{
	unsigned long flags;

	SSI_LOCK_ASSERT_INITED(eventp);
	SSI_LOCK_ASSERT(!in_irq());
	spin_lock_irqsave(&eventp->ev_lock, flags);
	if (eventp->ev_state == 0) {
		if (ssi_wake_event(&eventp->ev_waitlist)) {
			LIST_HEAD(listsnap);
			list_splice_init(&eventp->ev_waitlist, &listsnap);
			for (;;) {
				/*
				 * Allow pending interrupts before next task.
				 * (Dubious.)
				 */
				spin_unlock_irqrestore(&eventp->ev_lock, flags);
				spin_lock_irqsave(&eventp->ev_lock, flags);
				if (!ssi_wake_event(&listsnap))
					break;
			}
		} else
			eventp->ev_state = 1;
	} else
		SSI_LOCK_ASSERT(list_empty(&eventp->ev_waitlist));
	spin_unlock_irqrestore(&eventp->ev_lock, flags);
}
#endif /* !LINUX_SSI_EVENT */

static inline void ssi_deinit_event(EVENT_T *eventp)
{
	SSI_LOCK_DEINIT_DEBUG(eventp);
#ifndef LINUX_SSI_EVENT
	SSI_LOCK_ASSERT(list_empty(&eventp->ev_waitlist));
#endif
}

#define INIT_EVENT(_eventp) \
	ssi_init_event(_eventp)

#define WAIT_EVENT(_eventp) \
	ssi_wait_event(_eventp, 0, NULL)

#define SIGNAL_EVENT(_eventp) \
	ssi_signal_event(_eventp)

#define BROADCAST_EVENT(_eventp) \
	ssi_broadcast_event(_eventp)

#define DEINIT_EVENT(_eventp) \
	ssi_deinit_event(_eventp)

#define TRY_EVENT(_eventp) \
	ssi_try_event(_eventp)

/*
 * Interruptible Event Variable Primitives
 * ---------------------------------------
 *
 * The following interfaces are provided for event variables
 * that can potentially be interrupted by POSIX signals:
 *
 *	INIT_INTR_EVENT(&event)
 *
 *		This macro initializes the specified event
 *		variable. This macro does not sleep.
 *
 *	WAIT_INTR_EVENT(&event, is_intr, &was_intr)
 *
 *		Sleep waiting for a certain event to be posted
 *		or a signal to interrupt the wait.  The is_intr
 *		parameter is a boolean specifying whether the
 *		wait is interruptible by POSIX signals.  The
 *		was_intr flag specifies whether the wait was
 *		interrupted by POSIX signals (B_TRUE is returned)
 *		or woken up by either SIGNAL_INTR_EVENT() or
 *		BROADCAST_INTR_EVENT() (B_FALSE is returned).
 *
 *	SIGNAL_INTR_EVENT(&event)
 *
 *		Wake up a single context currently waiting on the
 *		specified event variable via WAIT_INTR_EVENT().
 *
 *	BROADCAST_INTR_EVENT(&event)
 *
 *		Wake up all contexts currently waiting on the
 *		specified event variable via WAIT_INTR_EVENT().
 *
 *	DEINIT_INTR_EVENT(&event)
 *
 *		This macro deallocates any data structures that may be
 *		associated with the event variable.  On some systems
 *		this may be a no-op.
 *
 *	Force typechecking even though it is the same as a EVENT_T by
 *	using wrappers.
 */

static inline void ssi_init_intr_event(INTR_EVENT_T *eventp)
{
	ssi_init_event(&eventp->evi_event);
}

static inline int ssi_try_intr_event(INTR_EVENT_T *eventp)
{
	return ssi_try_event(&eventp->evi_event);
}

static inline void ssi_wait_intr_event(INTR_EVENT_T *eventp, int intr,
				       int *was_intrp)
{
	ssi_wait_event(&eventp->evi_event, intr, was_intrp);
}

static inline void ssi_signal_intr_event(INTR_EVENT_T *eventp)
{
	ssi_signal_event(&eventp->evi_event);
}

static inline void ssi_broadcast_intr_event(INTR_EVENT_T *eventp)
{
	ssi_broadcast_event(&eventp->evi_event);
}

static inline void ssi_deinit_intr_event(INTR_EVENT_T *eventp)
{
	ssi_deinit_event(&eventp->evi_event);
}

#define INIT_INTR_EVENT(_eventp) \
	ssi_init_intr_event(_eventp)

#define WAIT_INTR_EVENT(_eventp, _is_intr, _was_intrp) \
	ssi_wait_intr_event(_eventp, _is_intr, _was_intrp)

#define SIGNAL_INTR_EVENT(_eventp) \
	ssi_signal_intr_event(_eventp)

#define BROADCAST_INTR_EVENT(_eventp) \
	ssi_broadcast_intr_event(_eventp)

#define DEINIT_INTR_EVENT(_eventp) \
	ssi_deinit_intr_event(_eventp)

#define TRY_INTR_EVENT(_eventp) \
	ssi_try_intr_event(_eventp)

#ifdef CONFIG_CFS_NOT_LINUX

/*
 * Swapout Prevention
 * ------------------
 *
 * Some systems will swap out a process which is holding critical
 * resources like sleep locks.  This can obviously really kill performance,
 * as you wait for a process to be swapped back in.  These macros are meant
 * to control this sort of swapping behavior. If your OS is smart enough not
 * to swap a process in these conditions, you don't need to bother.
 */

static inline void ssi_swaplock(void)
{
	current->swappable = 1;
}

static inline void ssi_swapunlock(void)
{
	current->swappable = 0;
}

#define	NSC_SWAPLOCK() \
	ssi_swaplock()

#define	NSC_SWAPUNLOCK() \
	ssi_swapunlock()

#else /* CONFIG_CFS for LINUX */

#define	NSC_SWAPLOCK() \
	do { } while(0)

#define	NSC_SWAPUNLOCK() \
	do { } while(0)

#endif /* ! CONFIG_CFS */

#endif /* !_CLUSTER_SYNCH_H */
