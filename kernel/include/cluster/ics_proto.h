/*
 * 	ICS function prototypes.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the procedure prototypes of the routines
 * that are exported by the ICS component.
 */
#ifndef _CLUSTER_ICS_PROTO_H
#define _CLUSTER_ICS_PROTO_H

#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/ics.h>

/* Support for generated routines */

#define icscli_encode_simple_type(hndl, x, type) ({			\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(&_x == &_y);						\
	icscli_encode_inline(_h, &(x), _c);				\
})

#define icscli_encode_simple_type_p(hndl, x, type) ({			\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icscli_encode_inline(_h, (x), _c);				\
})

#define icscli_encode_var_simple_type(hndl, x, cnt, type) ({		\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icscli_encode_inline(_h, (x), _c);				\
})

#define icscli_encode_var_ool_simple_type_p(hndl, x, cnt, type) ({	\
	cli_handle_t *_h = (hndl);					\
	const long _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icscli_encode_ool_data_t(_h, (u_char *)(x), _c, NULL, 0);	\
})

#define icscli_encode_msg_var_ool_simple_type_p(hndl, x, cnt, type) ({	\
	cli_handle_t *_h = (hndl);					\
	const long _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icscli_encode_ool_data_t(_h, (u_char *)(x), _c,			\
				 free_data_callback, _c);		\
})

#define icscli_encode_ptr_simple_type_p(hndl, x, type) ({		\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	cli_encode_ptr_object(_h, (x), _c);				\
})

#define icscli_encoderesp_var_ool_simple_type_p_p(hndl, x, cnt, type) ({\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type *_y;							\
	(void)(_x == &_y);						\
	if (*(x) == NULL && _c != 0)					\
		*(x) = kmalloc_nofail(_c);				\
	icscli_encoderesp_ool_data_t(_h, (u_char *)*(x), _c);		\
})

#define icscli_decode_simple_type(hndl, x, type) ({			\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(&_x == &_y);						\
	icscli_decode_inline(_h, &(x), _c);				\
})

#define icscli_decode_simple_type_p(hndl, x, type) ({			\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icscli_decode_inline(_h, (x), _c);				\
})

#define icscli_decode_var_simple_type(hndl, x, cnt, type) ({		\
	cli_handle_t *_h = (hndl);					\
	const int _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icscli_decode_inline(_h, (x), _c);				\
})

#define icscli_decode_var_ool_simple_type_p_p(hndl, x, cnt, type) ({	\
	cli_handle_t *_h = (hndl);					\
	const long _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type *_y;							\
	(void)(_x == &_y);						\
	icscli_decode_ool_data_t(_h, (u_char *)*(x), _c);		\
})

#define icssvr_encode_simple_type(hndl, x, type) ({			\
	svr_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(&_x == &_y);						\
	icssvr_encode_inline(_h, &(x), _c);				\
})

#define icssvr_encode_simple_type_p(hndl, x, type) ({			\
	svr_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icssvr_encode_inline(_h, (x), _c);				\
})

#define icssvr_encode_var_simple_type(hndl, x, cnt, type) ({		\
	svr_handle_t *_h = (hndl);					\
	const int _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icssvr_encode_inline(_h, (x), _c);				\
})

#define icssvr_encode_var_ool_simple_type_p_p(hndl, x, cnt, type) ({	\
	svr_handle_t *_h = (hndl);					\
	const long _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type *_y;							\
	(void)(_x == &_y);						\
	icssvr_encode_ool_data_t(_h, (u_char *)*(x), _c,		\
				 free_data_callback, _c);		\
})

#define icssvr_encode_var_ool_simple_type_p(hndl, x, cnt, type) ({	\
	svr_handle_t *_h = (hndl);					\
	const long _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icssvr_encode_ool_data_t(_h, (u_char *)(x), _c,			\
				 free_data_callback, _c);		\
})

#define icssvr_decode_simple_type(hndl, x, type) ({			\
	svr_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(&_x == &_y);						\
	icssvr_decode_inline(_h, &(x), _c);				\
})

#define icssvr_decode_simple_type_p(hndl, x, type) ({			\
	svr_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	icssvr_decode_inline(_h, (x), _c);				\
})

#define icssvr_decode_var_simple_type(hndl, x, cnt, type) ({		\
	svr_handle_t *_h = (hndl);					\
	const int _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y; 							\
	(void)(_x == &_y);						\
	icssvr_decode_inline(_h, (x), _c);				\
})

#define icssvr_decode_var_ool_simple_type_p(hndl, x, cnt, type) ({	\
	svr_handle_t *_h = (hndl);					\
	const long _c = sizeof(type) * (cnt);				\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	SSI_ASSERT((x) == NULL);					\
	if (_c != 0)							\
		(x) = kmalloc_nofail(_c);				\
	icssvr_decode_ool_data_t(_h, (u_char *)(x), _c);		\
})

#define icssvr_decode_ptr_simple_type_p(hndl, x, type) ({		\
	svr_handle_t *_h = (hndl);					\
	const int _c = sizeof(type);					\
	typeof(x) _x;							\
	type _y;							\
	(void)(_x == &_y);						\
	svr_decode_ptr_object(_h, (void **)&(x), _c);			\
})

#define icssvr_free_var_ool_simple_type_p(hndl, x, cnt, type) ({	\
	typeof(x) _x = (x);						\
	type _y;							\
	(void)(_x == &_y);						\
	kfree(_x);							\
})

#include <cluster/gen/ics_proto_gen.h>

/*
 * High level generic ICS routines.
 */

void
ics_svc_register(
	int		service,
	char		*service_name,
	int		num_ops,
	int		(*service_ops[])(svr_handle_t *),
	char		*service_op_names[],
	int		lwm_service_handles,
	int		hwm_service_handles,
	int		lwm_flowpages,
	int		hwm_flowpages);

void
ics_earlylock_init(void);

void
ics_init(void);

int
register_ics_channel(ics_chan_t *, int);

int
register_cluster_svc(cluster_svc_t *, ics_chan_t, int (*)(void));

void
ics_cluster_services_init(void);

void
ics_nodeinfo_callback(
	void		nodeup_notification(
				clusternode_t	node,
				icsinfo_t	*icsinfo),
	void		nodedown_notification(
				clusternode_t	node));

void
ics_connect_master(void);

void
ics_transport_config(void);

int
ics_geticsinfo(
	clusternode_t   node,
	icsinfo_t       *icsinfo_p);

int
ics_seticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p);

int
ics_nodeup(
	clusternode_t	node);

void
ics_nodeready(
	clusternode_t	node);

void
ics_nodedown(
	clusternode_t	node);

void
ics_nodedown_async_start(
	clusternode_t	node);

void
ics_nodedown_async_end(
	clusternode_t	node);

void
icssvr_nodedown_svc_wait(
	clusternode_t	node,
	int		service);

void icssvr_nodedown_wait(clusternode_t node);

void
ics_shoot_node(clusternode_t node);

int
ics_route_input(char *);

int
ics_get_transport(void);

void
ics_set_transport(int);

int
ics_isicsport(unsigned short port);

int
ics_get_nodenumsize(void);

int
ics_get_addrlen(void);

void
ics_node_to_icsinfo(icsinfo_t *addr, clusternode_t node);

int
ics_probe_clms(clusternode_t node, icsinfo_t addr,
	       clusternode_t *master_node,
	       icsinfo_t *master_addr,
	       int im_a_secondary,
	       int im_a_member);

int
ics_unack_send(clusternode_t node,
		u_int type,
		void *message,
		size_t length);

/*
 * High level ICS client-side routines.
 */

cli_handle_t *
icscli_handle_get(
	clusternode_t	node,
	int		service,
	int		flags);

#define ICS_NO_REPLY	0x0001	/* Value of "flag" for icscli_send() */
#define ICS_NO_BLOCK	0x0004	/* Non-blocking message send */

int
icscli_wouldthrottle(
	clusternode_t	node,
	int		service);

int
icscli_waitfornothrottle(
	clusternode_t	node,
	int		service);

int
icscli_send(
	cli_handle_t	*handle,
	int		procnum,
	void		(*callback)(
				cli_handle_t	*handle,
				long		callarg),
	long		callarg);

void
icscli_wait_callback(
	cli_handle_t	*handle,
	long		callarg);

int
icscli_wait(
	cli_handle_t	*handle,
	int		flag);

int
icscli_get_status(
	cli_handle_t	*handle);

clusternode_t
icscli_getnode(
	cli_handle_t	*handle);

int
icscli_async_sent(
	cli_handle_t	**hpp);

void
icscli_handle_release(
	cli_handle_t	*handle);

void
icscli_handle_release_callback(
	cli_handle_t	*handle,
	long		callarg);

/* Values for flag to icscli_wait(). */
#define ICS_NO_SIG_FORWARD	0x0001		/* Don't forward signals. */
#define ICS_NO_JOB_FORWARD	0x0002		/* Don't forward job signals. */

/*
 * ICS client-side data marshalling routines.
 *
 * Note that data mashalling of in-line parameters are mapped directly
 * to the low-level ICS routines. However, this may change at any
 * time and callers should use the non-low-level names at all times.
 */

#define icscli_encode_inline icscli_llencode_inline

extern void icscli_llencode_inline(cli_handle_t *, const void *p, int size);

#define icscli_decode_inline icscli_lldecode_inline

extern void icscli_lldecode_inline(cli_handle_t *, void *p, int size);

#define icssvr_encode_inline icssvr_llencode_inline

extern void icssvr_llencode_inline(svr_handle_t *, const void *p, int size);

#define icssvr_decode_inline icssvr_lldecode_inline

extern void icssvr_lldecode_inline(svr_handle_t *, void *p, int size);

#define icscli_encode_ool_data_t icscli_llencode_ool_data_t
int
icscli_llencode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*data,
	long		len,
	void		(*callback)(
				u_char	*data,
				long	callarg),
	long		callarg);

#define icscli_encoderesp_ool_data_t icscli_llencoderesp_ool_data_t
int
icscli_llencoderesp_ool_data_t(
	cli_handle_t	*handle,
	u_char		*data,
	long		len);

#define icscli_decode_ool_data_t icscli_lldecode_ool_data_t
int
icscli_lldecode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*data,
	long		len);

#define icscli_encode_ool_struct_msghdr icscli_llencode_ool_struct_msghdr
int
icscli_llencode_ool_struct_msghdr(
	cli_handle_t	*handle,
	struct msghdr	*msg,
	int	rwflag);

#define icscli_decode_ool_struct_msghdr icscli_lldecode_ool_struct_msghdr
int
icscli_lldecode_ool_struct_msghdr(
	cli_handle_t	*handle,
	struct msghdr	**msg,
	int	rwflag);

#ifdef ICS_OOL_STRUCT_PAGES
#define icscli_encode_ool_struct_page_p_p icscli_llencode_ool_struct_page_p_p
int
icscli_llencode_ool_struct_page_p_p(
	cli_handle_t	*handle,
	struct page	**pages,
	unsigned long	len,
	void		(*callback)(
				u_char	*data,
				long	callarg),
	long		callarg);

#define icscli_encoderesp_ool_struct_page_p_p (0)

#define icscli_decode_ool_struct_page_p_p icscli_lldecode_ool_struct_page_p_p
int
icscli_lldecode_ool_struct_page_p_p(
	cli_handle_t	*handle,
	struct page	**pages,
	unsigned long	len);
#endif /* ICS_OOL_STRUCT_PAGES */

/*
 * High level ICS server-side routines.
 */

svr_handle_t *
icssvr_handle_get(
	int		service,
	boolean_t	sleep_flag);

void
icssvr_recv(
	svr_handle_t	*handle,
	void		(*callback)(
				svr_handle_t	*handle,
				long		callarg),
	long		callarg);

#define icssvr_decode_done icssvr_lldecode_done
void
icssvr_lldecode_done(
	svr_handle_t	*handle);

#define icssvr_rewind icssvr_llrewind
void
icssvr_llrewind(
	svr_handle_t	*handle);

void
icssvr_reply(
	svr_handle_t	*handle,
	void		(*callback)(
				svr_handle_t	*handle,
				long		callarg),
	long		callarg);

void
icssvr_handle_release(
	svr_handle_t	*handle);

/*
 * ICS server-side data marshalling routines.
 *
 * Note that data mashalling of in-line parameters are mapped directly
 * to the low-level ICS routines. However, this may change at any
 * time and callers should use the non-low-level names at all times.
 */

#define icssvr_decode_ool_data_t icssvr_lldecode_ool_data_t
int
icssvr_lldecode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*data,
	long		len);

#define icssvr_encode_ool_data_t icssvr_llencode_ool_data_t
int
icssvr_llencode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*data,
	long		len,
	void		(*callback)(
				u_char	*data,
				long	callarg),
	long		callarg);

#define icssvr_decode_ool_struct_msghdr icssvr_lldecode_ool_struct_msghdr
int
icssvr_lldecode_ool_struct_msghdr(
	svr_handle_t	*handle,
	struct msghdr	**msg,
	int	rwflag);

#define icssvr_encode_ool_struct_msghdr icssvr_llencode_ool_struct_msghdr
int
icssvr_llencode_ool_struct_msghdr(
	svr_handle_t	*handle,
	struct msghdr	*msg,
	int	rwflag);

extern void freemsghdr_callback(u_char *, long);
extern void free_data_callback(u_char *, long);
extern void free_cred_callback(u_char *, long);

#ifdef ICS_OOL_STRUCT_PAGES
#define icssvr_encode_ool_struct_page_p_p icssvr_llencode_ool_struct_page_p_p
int
icssvr_llencode_ool_struct_page_p_p(
	svr_handle_t	*handle,
	struct page	**pages,
	unsigned long	len,
	void		(*callback)(
				u_char	*data,
				long	callarg),
	long		callarg);

#define icssvr_decode_ool_struct_page_p_p icssvr_lldecode_ool_struct_page_p_p
int
icssvr_lldecode_ool_struct_page_p_p(
	svr_handle_t	*handle,
	struct page	***pages,
	unsigned long	len);
#endif /* ICS_OOL_STRUCT_PAGES */

#endif /* !_CLUSTER_ICS_PROTO_H */
