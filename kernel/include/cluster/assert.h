/*
 * 	Cluster assertion debugging support.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CLUSTER_ASSERT_H
#define _CLUSTER_ASSERT_H

void ssi_assert_failed(const char *, const char *, int, const char *);

#define ssi_assert(_expr) do { \
	if (unlikely((_expr) == 0)) \
		ssi_assert_failed(#_expr, __FILE__, __LINE__, __FUNCTION__); \
} while (0)

#if defined(CONFIG_CLUSTER) && (defined(DEBUG) || defined (ENABLE_ASSERT)) 
#define SSI_ASSERT_ONLY_DECL(_expr)	_expr
#define SSI_ASSERT(_expr)	ssi_assert(_expr)
#define SSI_MASSERT(_expr)	ssi_assert(_expr)
#else
#define SSI_ASSERT_ONLY_DECL(_expr)
#define SSI_ASSERT(_expr)	((void)0)
#define SSI_MASSERT(_expr)	((void)0)
#endif

#endif /* !_CLUSTER_ASSERT_H */
