/*
 * 	Cluster async header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CLUSTER_ASYNC_H
#define _CLUSTER_ASYNC_H

#ifdef DYNAMIC_NSC_ASYNC_DAEMONS
#include <cluster/config.h>
#define NSC_ASYNC_DAEMONS min_t(int, NSC_MAX_NODE_VALUE+1, 4*num_online_cpus())
#else
#define NSC_ASYNC_DAEMONS 10
#endif

/*
 * Flags passed to the nsc_async_queue() routine.
 */
#define NSC_ASYNC_SLEEP			0x01
#define NSC_ASYNC_DUPCHECK		0x02
#ifdef NSC_ASYNC_SLAB_NOFS__FLAG
#define NSC_ASYNC_SLAB_NOFS		0x04
#endif
#ifdef NSC_ASYNC_ARGS_ZERO_COPY
#define NSC_ASYNC_ARGS_FREE		0x08
#endif
#ifdef CFS_ASYNC_QUEUE
#define NSC_ASYNC_STACK			0x10
#endif

#ifdef SSI_NOTUSED
#define NSC_ASYNC_ALLOCFREE		0x04
#define NSC_ASYNC_USEFREE		0x10
#endif

/*
 * When NSC_ASYNC_NO_MAXARGSZ is passed to nsc_async_queue_init(),
 * the maximum argsz is unlimited and the NSC_ASYNC_PREALLOC and
 * NSC_ASYNC_USEFREE flags are not available for use on the queue.
 */
#define NSC_ASYNC_NO_MAXARGSZ		-1

extern int nsc_async_daemons;

extern void *nsc_async_queue_init(char *, int, int);
extern int nsc_async_queue(void *, void (*)(void *), void *,
			   size_t, int);
extern void nsc_async_dequeue(void *, int);
#ifdef SSI_NOTUSED
extern void nsc_async_freelist_alloc(void *);
extern void nsc_async_freelist_dealloc(void *);

extern void *nsc_async_ref_alloc(void *);
extern void nsc_async_ref_free(void *);
extern int nsc_async_ref_queue(void *, void (*)(void *), void *, size_t, int);
extern void nsc_async_ref_freelist_alloc(void *);
extern void nsc_async_ref_freelist_dealloc(void *);
extern void nsc_async_ref_done(void *);
extern void nsc_async_ref_wait(void *);
extern int nsc_async_ref_check(void *);
extern int nsc_async_ref_incr(void *datap);
#endif /* SSI_NOTUSED */
#ifdef NSC_ASYNC_ENTRY_KMEM_CACHE
extern void nsc_async_init(void);
#endif

#endif /* !_CLUSTER_ASYNC_H */
