#ident "@(#)$Header: /cvsroot/ci-linux/ci/kernel/include/cluster/procfs.h,v 1.1 2003/06/03 22:22:31 bjbrew Exp $"
/*
 * 	Procfs  /proc/cluster  header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CI_PROCFS_H_
#define _CI_PROCFS_H_

extern struct proc_dir_entry *proc_cluster;
extern struct fasync_struct *proc_cluster_events_fasync_list;
extern struct file_operations proc_loadlist_operations;
extern struct file_operations proc_loadlog_operations;

extern void proc_cluster_init(void);
extern void proc_cluster_ssi_init(void);
extern int proc_calc_metrics(char *, char **, off_t, int, int *, int);
extern int proc_node_read_lvsmap(char *, char **, off_t , int ,
				 int *, void *);
extern int node_read_ipvs_portweight(char *, char **, off_t , int ,
				     int *, void *);
extern int ssi_tty_get_pgrp(struct task_struct *, int *);

#ifdef CONFIG_SSI

#define PROC_MAXPIDS 20 /* from Linux fs/proc/base.c */

extern char *proc_ssi_fix_links(struct inode *, char *, char *);

#else

static inline char *
proc_ssi_fix_links(struct inode *inode, char *buf, char *path)
{
	(void)inode;
	(void)buf;
	return path;
}

#endif

#endif /* _CI_PROCFS_H_ */
