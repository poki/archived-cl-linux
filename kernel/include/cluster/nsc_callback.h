#ident "@(#)$Header: /cvsroot/ci-linux/ci/kernel/include/cluster/nsc_callback.h,v 1.1 2003/06/03 22:22:31 bjbrew Exp $"
/*
 * 	Generic callback header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _SSI_NSC_CALLBACK_H
#define _SSI_NSC_CALLBACK_H

/*
 * Callback support
 */

#include <cluster/synch.h>
#include <cluster/nsc.h>

typedef int	cbid_t;

/* SSI_XXX: What really goes here? (was MAXBSIZE + PAGESIZE) */
#define	CALLBACK_DATA_MAX	(512)

#define	CBID_INVAL	-1

#define CB_VALID	0x0001
#define	CB_WAITING	0x0002
#define	CB_RETURNED	0x0004
#define	CB_NODEDOWN	0x0008

typedef struct cbdata {
	cbid_t		cbd_id;
	int		cbd_flags;
	clusternode_t	cbd_node;
	void		*cbd_bufp;
	size_t		cbd_bufsz;
	struct cbdata	*cbd_next;
	struct cbdata	*cbd_prev;
	CONDITION_T	cbd_cond;
	COND_LOCK_T	cbd_lock;
} cbdata_t;

typedef struct cblist {
	uint		cl_curid;
	cbdata_t	*cl_list;
	LOCK_T		cl_lock;
} cblist_t;

extern cblist_t cblist;
#define CBLIST_LOCK()		LOCK_LOCK(&cblist.cl_lock)
#define	CBLIST_UNLOCK()		UNLOCK_LOCK(&cblist.cl_lock)
#define SSI_ASSERT_CBLIST_IS_LOCKED() \
	SSI_ASSERT_LOCKED_LOCK(&cblist.cl_lock);

extern void callback_init_ssi(void);
extern cbid_t callback_getid(clusternode_t, void *, size_t);
extern int callback_wait(cbid_t);
extern void callback_cancel(cbid_t);
extern int callback_wakeup(cbid_t, caddr_t, uint);

#endif /* _SSI_NSC_CALLBACK_H */
