#ident "@(#)$Header: /cvsroot/ci-linux/ci/kernel/include/cluster/ndreg.h,v 1.1 2003/06/03 22:22:31 bjbrew Exp $"
/*
 * 	Generic Nodedown Registration header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _SSI_NDREG_H_
#define _SSI_NDREG_H_

/*
 * Flags for the nsc_nd_reg() interface.
 */
#define NSC_ND_NOSLEEP		0x01
#define NSC_ND_SPAWN		0x02

extern void nsc_nd_init(void);
extern int nsc_nd_reg(clusternode_t, void (*)(clusternode_t, void *), void *,
		      int, int);
extern int nsc_nd_dereg(int, void *, int);
extern void nsc_nd_nodedown(clusternode_t);

#endif /* _SSI_NDREG_H_ */
