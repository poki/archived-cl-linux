/*
 * 	Cluster nodelist manipulation routines.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef _CLUSTER_NODELIST_H
#define _CLUSTER_NODELIST_H

#include <asm/bitops.h>

/*
 * Macros to work with lists of nodes.
 *
 * These interfaces may be implemented as macros.
 * Care must be taken not to pass any arguments with side effects
 * (such as "foo++") since they may be evaluated multiple times.
 *
 * Available operations are:
 *	CLRALL		Clear all nodes in the nodelist_t.
 *	SETALL		Set all nodes in the nodelist_t.
 *	TESTALL		Return non-zero if nodelist_t not empty
 *	CLR1		Clear an individual node.
 *	SET1		Set an individual node.
 *	TEST1		Test one node; return non-zero if node is present.
 *	CLRN		Clear all nodes in dst which are in src.
 *	SETN		Set all nodes in dst which are in src.
 *	ANDN		Clear all nodes in dst which aren't in src.
 *	GET_NEXT	Return the next node in the list (given a cookie)
 *	FFS		"Find First Set"; return the node number of the first
 *			node, starting from 1; if none, return -1.
 *	FFSCLR		"Find First Set and Clear"; return the node number
 *			of the first node, starting from 1, after clearing
 *			that node; if no nodes were set, return -1.
 *
 *	Unimplemented:
 *
 *	CLRALL_SET1	Clear all nodes and set one in the nodelist_t.
 *	FLS		"Find Last Set"; return the node number of the highest
 *			node; if none, return -1.
 *	FLSCLR		"Find Last Set and Clear"; return the node number
 *			of the highest set node, starting from 1, after
 *			clearing that node; if no nodes were set, return -1.
 *	FFC		"Find First Clear"; return the node number of the first
 *			missing node, starting from 1; if none, return -1.
 *	FLC		"Find Last Clear"; return the node number of the
 *			highest missing node; if none, return -1.
 *	FFCSET		"Find First Clear and Set"; return the node number
 *			of the first missing, starting from 1, after
 *			setting that node; if no nodes were clear, return -1.
 *	FLCSET		"Find Last Clear and Set"; return the node number
 *			of the highest missing node, starting from 1, after
 *			adding that node; if no nodes were missing, return -1.
 */

#if defined(__KERNEL__)

#include <linux/threads.h>
#include <linux/types.h>
#include <asm/bitops.h>
#include <cluster/nsc.h>
#include <cluster/config.h>

/*
 * NOTE:
 * These macros are built on top of base routines that accept a "long".
 * The "nodelist" arguments are of type nodelist_t *.
 * The "node" arguments and return values are not type-casted.
 * However, the "node" arguments should never be negative as this may
 * result in a negative bit shift and corrupting memory at lower addresses.
 */


#define BITMASK_NLONGS(nbits)   (((nbits) + BITS_PER_LONG - 1) / BITS_PER_LONG)

#define NSC_NODELIST_MAXNODE		NSC_MAX_NODE_VALUE
#define NSC_NODELIST_MINNODE		1
#define NSC_NODELIST_NLONGS \
	BITMASK_NLONGS(NSC_NODELIST_MAXNODE - NSC_NODELIST_MINNODE + 1)

static inline void __bitmaskn_clrall(unsigned long *dbp, int n)
{
	int i;

	for (i = 0; i < n; i++)
		*dbp++ = 0UL;
}

static inline void __bitmaskn_setall(unsigned long *dbp, int n)
{
	int i;

	for (i = 0; i < n; i++)
		*dbp++ = ~0UL;
}

static inline int __bitmaskn_testall(unsigned long *sbp, int n)
{
	int i;

	for (i = 0; i < n; i++) {
		if (*sbp++ != 0)
			return 1;
	}

	return 0;
}

static inline int __bitmaskn_ffs(unsigned long *sbp, int n)
{
	int ret = -1;
	int i;

	for (i = 0; i < n; i++) {
		if (*sbp != 0) {
			ret = __ffs(*sbp);
			ret += i * BITS_PER_LONG;
			break;
		}
		sbp++;
	}

	return ret;
}

static inline int __bitmaskn_ffsclr(unsigned long *sbp, int n)
{
	int ret = -1;
	int i;

	for (i = 0; i < n; i++) {
		if (*sbp != 0) {
			ret = __ffs(*sbp);
			clear_bit(ret, sbp);
			ret += i * BITS_PER_LONG;
			break;
		}
		sbp++;
	}

	return ret;
}

static inline void __bitmaskn_clrn(unsigned long *dbp, unsigned long *sbp,
				   int n)
{
	int i;

	for (i = 0; i < n; i++)
		*dbp++ &= ~*sbp++;
}

static inline void __bitmaskn_andn(unsigned long *dbp, unsigned long *sbp,
				   int n)
{
	int i;

	for (i = 0; i < n; i++)
		*dbp++ &= *sbp++;
}

static inline void __bitmaskn_setn(unsigned long *dbp, unsigned long *sbp,
				   int n)
{
	int i;

	for (i = 0; i < n; i++)
		*dbp++ |= *sbp++;
}

static inline int __bitmaskn_testn(unsigned long *dbp, unsigned long *sbp,
				   int n)
{
	int i;

	for (i = 0; i < n; i++) {
		if (*dbp++ & *sbp++)
			return 1;
	}

	return 0;
}

static inline void __bitmaskn_init(unsigned long *dbp, int n, int bitno)
{

	__bitmaskn_clrall(dbp, n);
	set_bit(bitno, dbp);
}

#define BITMASKN_CLRALL(bits, n) \
		__bitmaskn_clrall(bits, n)
#define BITMASKN_SETALL(bits, n) \
		__bitmaskn_setall(bits, n)
#define BITMASKN_TESTALL(bits, n) \
		__bitmaskn_testall(bits, n)
#define BITMASKN_FFS(bits, n) \
		__bitmaskn_ffs(bits, n)
#define BITMASKN_FFSCLR(bits, n) \
		__bitmaskn_ffsclr(bits, n)
#define BITMASKN_CLR1(bits, bitno) \
		clear_bit(bitno, bits)
#define BITMASKN_SET1(bits, bitno) \
		set_bit(bitno, bits)
#define BITMASKN_TEST1(bits, bitno) \
		test_bit(bitno, bits)
#define BITMASKN_CLRN(dst_bits, src_bits, n) \
		__bitmaskn_clrn(dst_bits, src_bits, n)
#define BITMASKN_ANDN(dst_bits, src_bits, n) \
		__bitmaskn_andn(dst_bits, src_bits, n)
#define BITMASKN_SETN(dst_bits, src_bits, n) \
		__bitmaskn_setn(dst_bits, src_bits, n)
#define BITMASKN_TESTN(dst_bits, src_bits, n) \
		__bitmaskn_testn(dst_bits, src_bits, n)
#define BITMASKN_INIT(bits, n, bitno) \
		__bitmaskn_init(dst_bits, n, bitno)

/*
 * The node list structure
 * NOTE:
 *	Some code may depend on this being a structure,
 *	e.g. to do structure assignment.
 * FUTURE/PERF:
 *	Add support to use faster fixed-length bitmask support, if possible.
 *	e.g. if NSC_NODELIST_NWORDS == 1, use BITMASK1_XXX() macros.
 */
typedef struct nsc_nodelist {
	unsigned long nl_masks[NSC_NODELIST_NLONGS];
} nsc_nodelist_t;

typedef unsigned long		nsc_nlcookie_t;

/*
 * function prototypes
 */
extern nsc_nodelist_t *nsc_nodelist_alloc(void);
extern void nsc_nodelist_free(nsc_nodelist_t *);
extern int nsc_nodelist_count(nsc_nodelist_t *);
extern nsc_nodelist_t *nsc_nodelist_copy(nsc_nodelist_t *);
extern clusternode_t nsc_nodelist_get_next(nsc_nlcookie_t *, nsc_nodelist_t *);
extern clusternode_t nsc_nodelist_bit2node(int);

#define NSC_NODELIST_CHKNODE(node) \
		((node) >= NSC_NODELIST_MINNODE && \
		 (node) <= NSC_NODELIST_MAXNODE)

#define NSC_NODELIST_BIT2NODE(bit)	nsc_nodelist_bit2node(bit)

/*
 * SET Macros
 */
#define	NSC_NODELIST_SET1(nlp, node) ({ \
		SSI_ASSERT(NSC_NODELIST_CHKNODE(node)); \
		BITMASKN_SET1((nlp)->nl_masks, (node) - NSC_NODELIST_MINNODE); \
	})

#define	NSC_NODELIST_SETALL(nlp) \
		BITMASKN_SETALL((nlp)->nl_masks, NSC_NODELIST_NLONGS)

#define	NSC_NODELIST_SETN(dstp, srcp) \
		BITMASKN_SETN((dstp)->nl_masks, (srcp)->nl_masks, \
				NSC_NODELIST_NLONGS)

/*
 * TEST Macros
 */
#define	NSC_NODELIST_TEST1(nlp, node) ({ \
		SSI_ASSERT(NSC_NODELIST_CHKNODE(node)); \
		BITMASKN_TEST1((nlp)->nl_masks, (node) - NSC_NODELIST_MINNODE); \
	})

#define	NSC_NODELIST_TESTALL(nlp) \
		BITMASKN_TESTALL((nlp)->nl_masks, NSC_NODELIST_NLONGS)

/* CLEAR Macros
 */
#define	NSC_NODELIST_CLR1(nlp, node) ({ \
		SSI_ASSERT(NSC_NODELIST_CHKNODE(node)); \
		BITMASKN_CLR1((nlp)->nl_masks, (node) - NSC_NODELIST_MINNODE); \
	})

#define	NSC_NODELIST_CLRALL(nlp) \
		BITMASKN_CLRALL((nlp)->nl_masks, NSC_NODELIST_NLONGS)

#define	NSC_NODELIST_CLRN(dstp, srcp) \
		BITMASKN_CLRN((dstp)->nl_masks, (srcp)->nl_masks, \
				NSC_NODELIST_NLONGS)

/*
 * AND Macros
 */
#define	NSC_NODELIST_ANDN(dstp, srcp) \
		BITMASKN_ANDN((dstp)->nl_masks, (srcp)->nl_masks, \
				NSC_NODELIST_NLONGS)

/*
 * ALLOCATE a nsc_nodelist_t
 *	- The list returned will already be initialized for immediate use.
 */
#define NSC_NODELIST_ALLOC()		nsc_nodelist_alloc()
#define NSC_NODELIST_FREE(nlp)		nsc_nodelist_free(nlp)

/*
 * INITIALIZE a nsc_nodelist_t
 *	- all entries in supplied list are cleared
 *	- currently nothing more is allocated
 */
#define NSC_NODELIST_INIT(nlp)		NSC_NODELIST_CLRALL(nlp)
#define NSC_NODELIST_DEINIT(nlp)	/* nothing to do */

/*
 * MISC Macros
 */
#define NSC_NLCOOKIE_INIT(cp)		(*(cp) = CLUSTERNODE_INVAL);
#define NSC_NODELIST_COPY(nlp)		nsc_nodelist_copy(nlp)
#define NSC_NODELIST_COUNT(nlp)		nsc_nodelist_count(nlp)

/* FFS - Find first set. */
static inline int nsc_nodelist_ffs(nsc_nodelist_t *nlp)
{
	int ret;

	ret = BITMASKN_FFS(nlp->nl_masks, NSC_NODELIST_NLONGS);
	if (ret != -1)
		ret += NSC_NODELIST_MINNODE;

	return ret;
}

#define	NSC_NODELIST_FFS(_nlp)		nsc_nodelist_ffs(_nlp)

/* FFSCLR - Find first set and clear. */
static inline int nsc_nodelist_ffsclr(nsc_nodelist_t *nlp)
{
	int ret;

	ret = BITMASKN_FFSCLR(nlp->nl_masks, NSC_NODELIST_NLONGS);
	if (ret != -1)
		ret += NSC_NODELIST_MINNODE;

	return ret;
}

#define	NSC_NODELIST_FFSCLR(_nlp)		nsc_nodelist_ffsclr(_nlp)

/*
 * GET_NEXT - return the next node in the list (given a cookie)
 *	- cookie must be initialized (see NSC_NLCOOKIE_INIT)
 */
#define NSC_NODELIST_GET_NEXT(cookie, nlp) \
		nsc_nodelist_get_next((cookie), (nlp))

#endif /* __KERNEL__ */

#endif /* !_CLUSTER_NODELIST_H */
