/*
 * 	Node monitor header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <linux/cluster.h>

struct nm_settings {
	int nm_log_threshold;
	int nm_timeout;
	int nm_max_probes;
	u_int nm_nodedown_disabled:1;
	u_int nm_log_threshold_valid:1;
	u_int nm_rate_valid:1;
	u_int nm_nodedown_disabled_valid:1;
};

/*
 * Variable indicating whether Node Monitoring is turned ON or OFF.
 * Should be set to 1 in production environments, 0 for debugging.
 */

extern void nm_received_imalive(clusternode_t);
extern void nm_init(void);
extern void nm_stop(void);
extern void nm_get_settings(struct nm_settings *);
extern void nm_update_settings(const struct nm_settings *, int);
extern void nm_add_node(clusternode_t);

extern struct file_operations proc_nm_rate_operations;
extern struct file_operations proc_nm_nodedown_disabled_operations;
extern struct file_operations proc_nm_log_threshold_operations;
extern int nm_disabled;
