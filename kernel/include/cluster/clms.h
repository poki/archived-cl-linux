/*
 * 	CLMS public interface header file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * The NSC Cluster Membership Service.
 */
#ifndef _CLUSTER_CLMS_H
#define _CLUSTER_CLMS_H

#include <linux/init.h>
#include <cluster/nsc.h>
#include <cluster/nodelist.h>
#include <cluster/ics.h>
#ifdef CONFIG_LDLVL
#include <cluster/ssi/load_level.h>
#endif /* CONFIG_LDLVL */

/*
 * Parameters for nodedown system call
 */
#define CLMS_NODEDOWN_DOWN 	0
#define CLMS_NODEDOWN_HALT 	1
#define CLMS_NODEDOWN_DEBUG 	2

#if defined(__KERNEL__)

/*
 * Interface to CLMS Subsystems
 */
#define CLMS_MAX_SUBSYSTEMS	15
typedef int	clms_subsys_t;
extern int	clms_num_subsys;

/*
 * Service information Structure. This structure contains nodeup and
 * nodedown functions.  These functions will be called by the CLMS when
 * the corresponding events occur.
 *
 * NOTE: When a new clms service is added, both clms.h (this file) and
 *       clms_conf.c have to be edited to register the service with the
 *       CLMS in a proper way.
 *
 */
struct sinit {
	int	service;
	char	*s_name;
	int	pri;
	int	(*subsys_nodeup)
			(void *, clms_subsys_t,
			    clusternode_t, clusternode_t, void *);
	int	(*subsys_nodedown)
			(void *, clms_subsys_t,
			    clusternode_t, clusternode_t, void *);
	void	(*subsys_init_prejoin)(void);
	void	(*subsys_init_postjoin)(void);
	void	(*subsys_init_postroot)(void);
};
extern struct sinit	*clms_subsystems[CLMS_MAX_SUBSYSTEMS];


/*
 * Interface to CLMS Key Services
 */
#define CLMS_MAX_KEY_SERVICES		10
#define CLMS_LAST_KEY_SERVICE 		(clms_num_key_svcs-1)
typedef int	clms_key_svc_t;
extern int	clms_num_key_svcs;

/*
 * This datatype is used to hide the type of the pull_data buffer
 * from ICS.
 */
typedef u_char data_t;

extern clusternode_t clms_master_node;	   /* CLMS Coordinator Node */
extern char	*cluster_svcinfo;		/* Services for which node is primary */
extern char	*cluster_svcsecondary;	/* Services for which node is secondary */
extern int	clms_master_cold_boot;	/* TRUE => master cold boot. */
extern int	clms_cold_boot;		/* TRUE => local cold boot. */


/*
 * CLMS and NSC Service Providers in the Cluster.
 */
/*
 * NSC defines a set of key services viz. cluster membership service, root
 * file system, system v ipc, cluster mount service, init etc..  More
 * services may be defined in the future.  Currently all of these services
 * are provided by a single node.  This node is called the primary node.
 * If this node goes down, the cluster cannot be operated. However, in the
 * future, these services may be distributed.  In that case, the CLMS has to
 * keep track of the services and the nodes providing the services.  The
 * following data structures are defined for that purpose.
 */
/*
 * The NSC services and the nodes providing the services
 * are stored in the clms master.  This information
 * will be used by individual nodes to decide their
 * booting policy.
 */
struct cluster_server  {
	struct cluster_server  *cls_next; /* Next server for this service */
	int			cls_svc;  /* Service */
	clusternode_t		cls_node; /* Node Providing the Service */
	int			cls_type; /* Type of Server */
	int			cls_status; /* Status of Server */
};

typedef struct cluster_server cluster_server_t;

/*
 * Values for cls_type in struct cluster_server
 */
#define CLUSTER_NOT_A_SERVER	0x0	/* Node no longer provides the svc */
#define CLUSTER_PRIMARY_SERVER	0x1	/* Node is primary server */
#define CLUSTER_SECONDARY_SERVER 0x2	/* Node is a secondary server */
#define CLUSTER_FLOATER_ENTRY	0x3	/* Entry is a "floater" for recovery */
#define CLUSTER_FLOATER_SERVER	0x4	/* Floater is for the server */

/*
 * Values for cls_status in struct cluster_server
 */
#define CLUSTER_SERVER_NOT_READY 0x0	/* Server is not ready */
#define CLUSTER_SERVER_READY     0x1	/* Server is ready */
#define CLUSTER_SECONDARY_UP     0x2	/* Secondary is ready */

/*
 * Values representing node states
 */
#define CLMS_NODE_NEVER_UP		0x001
#define CLMS_NODE_PARTIAL_COMING_UP	0x002
#define CLMS_NODE_COMING_UP		0x004
#define CLMS_NODE_ICS_UP		0x008
#define CLMS_NODE_UP			0x010
#define CLMS_NODE_GOING_DOWN		0x020
#define CLMS_NODE_DOWN			0x040
#define CLMS_NODE_HALF_UP		0x080
#define CLMS_NODE_HALF_DOWN		0x100
#define CLMS_NODE_INFORM_COMING_UP	0x200
#define CLMS_NODE_REGROUP_KNOWN		0x400
#define CLMS_NODE_ANY_STATE		(~0)

/*
 * The number of times a CLMS secondary should allow a node to falsely
 * inform him of the master being down before shooting him.
 */
#define CLMS_MAX_INFORM_COUNT 2

/*
 * Possible error returns from clms_get_probe_info().
 */
#define CLMS_PROBE_ERR_OKAY		0
#define CLMS_PROBE_ERR_NOTREADY		1
#define CLMS_PROBE_ERR_HESNOTSEC	2
#define CLMS_PROBE_ERR_IMNOTSEC		3
#define CLMS_PROBE_ERR_DONTKNOW		4
#define CLMS_PROBE_ERR_NOANSWER		5
#define CLMS_PROBE_ERR_FORCE_ROOT_YOU	6
#define CLMS_PROBE_ERR_FORCE_ROOT_NOTYOU 7
#define CLMS_PROBE_ERR_DIE_ALREADY	8

/*
 * CLMS transition history structure.
 */
struct clms_transition {
	clusternode_t	node;		/* Node that changed state. */
	int		transition;	/* New state for the node. */
	int		oldstate;	/* Previous state for the node. */
	int		why;		/* Why the state changed. */
	unsigned long	transid;	/* ID if this transition. */
	struct timeval	stamp;		/* Timestamp of this transition. */
};
typedef struct clms_transition clms_transition_t;

/*
 * The CLMS internal Cluster Transition Vector structure.
 */
struct clms_ctv {
	int		ups;
	int		downs;
	int		other;
	unsigned long	transid;
	struct timeval	stamp;
};
typedef struct clms_ctv clms_ctv_t;

/*
 * A version of the Cluster Transition Vector structure for use by the rest
 * of the kernel, which isn't interested in "other" transitions or the
 * timestamp.  If this changes, note that rclms_api_newstate() and
 * rclms_api_new_state_commit() must lock clms_ctv_rw_lock.
 */
struct ctv {
	int		ups;
	int		downs;
};
typedef struct ctv ctv_t;

/*
 * Input flag values for clms_compare_[un]locked_ctv().
 */
#define CLMS_CMP_CTV_ANY	0
#define CLMS_CMP_CTV_UPS	1
#define CLMS_CMP_CTV_DOWNS	2

/*
 * CLMS API state structure.
 */
struct clms_api_state {
	int		state;		/* State of the node for the API. */
	int		prev_state;	/* Previous such state. */
	int		why;		/* Why we made the transition. */
	unsigned long	transid;	/* The transid of that transition. */
	struct timeval	stamp;		/* Timestamp of this transition. */
	unsigned long	first_transid;	/* Transid of the first transition */
					/* for this node. */
	struct timeval	first_stamp;	/* Timestamp of that transition. */
	int		up_transitions;	/* Number of transitions to COMINGUP */
					/* for this node. */
	int		down_transitions; /* Number of transitions to */
					  /* KCLEANUP for this node. */
};
typedef struct clms_api_state clms_api_state_t;

#define CLMS_CTV_TRANS_IS_NEXT(ctv1, ctv2)				\
		(((ctv1)->other == (ctv2)->other &&			\
		  (ctv1)->downs == (ctv2)->downs &&			\
		  (ctv1)->ups == (ctv2)->ups + 1) ||			\
		 ((ctv1)->other == (ctv2)->other &&			\
		  (ctv1)->downs == (ctv2)->downs + 1 &&			\
		  (ctv1)->ups == (ctv2)->ups) ||			\
		 ((ctv1)->other == (ctv2)->other + 1 &&			\
		  (ctv1)->downs == (ctv2)->downs &&			\
		  (ctv1)->ups == (ctv2)->ups))

/*
 * Externally-visible node transitions, defined by the API.
 */
#define CLMS_STATE_NEVERUP		0
#define CLMS_STATE_DOWN			1
#define CLMS_STATE_COMINGUP		2
#define CLMS_STATE_UP			3
#define CLMS_STATE_SHUTDOWN		4
#define CLMS_STATE_GOINGDOWN		5
#define CLMS_STATE_KCLEANUP		6
#define CLMS_STATE_UCLEANUP		7
#define CLMS_STATE_DUMMY_INTENT		255
#define CLMS_STATE_DUMMY		256

/*
 * Externally-visible reasons for node transitions, also defined by
 * the API.
 */
#define CLMS_TRANS_WHY_API		1
#define CLMS_TRANS_WHY_SYSTEM		2
#define CLMS_TRANS_WHY_UNEXPECTED	3

/*
 * The "where" parameter for clms_api_get_hist_ent().  See that function for
 * details.
 */
#define CLMS_WHERE_GET_FIRST		0
#define CLMS_WHERE_GET_LATEST		1
#define CLMS_WHERE_GET_EARLIEST		2
#define CLMS_WHERE_GET_THIS		3
#define CLMS_WHERE_GET_NEXT		4
#define CLMS_WHERE_GET_PREV		5

/*
 * Returns from clms_api_get_hist_ent().  See that function for details.
 */
#define CLMS_HIST_GET_FOUND		0
#define CLMS_HIST_GET_NOTPRESENT	1
#define CLMS_HIST_GET_TOOEARLY		2
#define CLMS_HIST_GET_MALFORMED		3
#define CLMS_HIST_GET_NONEXT		4
#define CLMS_HIST_GET_BADWHERE		5
#define CLMS_HIST_GET_INVALID		6
#define CLMS_HIST_GET_NOPREV		7

/*
 * External function declarations.
 */

#if defined(DEBUG) || defined(DEBUG_TOOLS)
extern int clms_nodedebug(clusternode_t);
#endif /* DEBUG || DEBUG_TOOLS */

/*
 * CLMS Kernel Interface declarations (CKI).
 */

/*
 * The following routine is called by clms_init() to
 * register clms subsystems.
 */
extern void clms_subsystems_init_prejoin(void);
extern void clms_subsystems_init_postjoin(void);
extern void clms_subsystems_init_postroot(void);

extern void clms_key_svcs_init_prejoin(void);
extern void clms_key_svcs_init_postjoin(void);
extern void clms_key_svcs_init_postroot(void);

extern void clms_svc_register(struct sinit *);

extern void clms_nodeup(clusternode_t);

extern void register_clms_key_services(void);
extern void register_clms_subsystems(void);
extern int register_clms_subsys(char *,
				int,
				int (*)(void *,
					clms_subsys_t,
					clusternode_t,
					clusternode_t,
					void *),
				int (*)(void *,
					clms_subsys_t,
					clusternode_t,
					clusternode_t,
					void *),
				void (*)(void),
				void (*)(void),
				void (*)(void));
extern int register_clms_key_service(char *, int *, int,
				     void (*)(nsc_nodelist_t *),
				     int  (*)(char *, int, int *),
				     void (*)(clusternode_t, char *, int),
				     void (*)(clusternode_t),
				     void (*)(void),
				     void (*)(void),
				     void (*)(void));
extern int clms_do_nodedown(clusternode_t, int);
extern void clms_nodedown(clusternode_t);


extern void clms_nodeup_callback(void *, int, clusternode_t);
extern void clms_nodedown_callback(void *, int, clusternode_t);
extern void clms_surrogate_nodeup_callback(void *, int, clusternode_t);
extern void clms_surrogate_nodedown_callback(void *, int, clusternode_t);
extern void clms_surrogate_failover_callback(void *, int, clusternode_t);

extern int clms_get_cluster_num_nodes(void);

extern struct clms_nodelist *clms_get_up_nodelist(void);

extern clusternode_t clms_get_next_node(struct clms_nodelist *);

extern nsc_nodelist_t *clms_get_nsc_nodelist(int);

extern nsc_nodelist_t *clms_get_clms_nodelist(int);

extern int clms_waitfor_key_service(int);

extern clusternode_t clms_get_key_server_node(int, int);

extern clusternode_t clms_get_key_secondary_node(int, int);

extern void clms_set_key_service_ready(int);

extern void clms_set_key_secondary_ready(int);

extern void clms_key_service_node_lock(int);

extern void clms_key_service_node_unlock(int);

extern char *clms_next_token(char **, char);

extern void clms_key_svc_pull_data(int, int, nsc_nodelist_t *);

extern int clms_declare_node_new_state(clusternode_t, int);

extern int clms_api_validate_new_state(clusternode_t, int, int *);

#ifdef DEBUG_TOOLS
extern void __clms_api_user_block(const char *file, int line);
extern void __clms_api_user_unblock(const char *file, int line);

#define clms_api_user_block()	__clms_api_user_block(__FILE__, __LINE__)
#define clms_api_user_unblock()	__clms_api_user_unblock(__FILE__, __LINE__)

#else

extern void clms_api_user_block(void);
extern void clms_api_user_unblock(void);

#endif

extern int clms_api_get_state(clusternode_t);

extern void clms_api_get_full_state(clusternode_t, clms_api_state_t *);

extern int clms_get_probe_info(int, int, clusternode_t, clusternode_t *);

extern void clms_snap_locked_ctv(ctv_t *);

extern void clms_snap_unlocked_ctv(ctv_t *);

extern void clms_unlock_ctv(void);

extern int clms_compare_locked_ctv(int, ctv_t);

extern int clms_compare_unlocked_ctv(int, ctv_t);

extern void clms_client_run_trans_queue(void);

extern int clms_api_get_hist_ent(int, u_long, clms_transition_t *);

extern int clms_isnodedown(clusternode_t node);

extern int clms_isnodeup(clusternode_t node);

extern int clms_ishalfup(clusternode_t node);

extern int clms_ishalfdown(clusternode_t node);

extern int clms_isnodeavail(clusternode_t node);

extern int clms_isnodeavailrdev(clusternode_t node);

extern int clms_isnode_inlist(clusternode_t *, int,  clusternode_t);

extern int clms_force_root_node(clusternode_t);

extern void clms_waitfor_nodedown(clusternode_t);

extern int clms_get_node_status_nolock(clusternode_t);
extern int clms_get_node_status(clusternode_t);

extern void clms_shutdown_freeze(void);

extern int clms_iskeynode(clusternode_t);

extern boolean_t clms_is_secondary_by_addr(icsinfo_t *);

extern void clms_init(void);

extern int clms_join_cluster(void);

extern int clms_startup_node(void);

struct nm_settings;
extern int clms_write_nm_settings(struct nm_settings *);

#ifdef CONFIG_LDLVL
extern void move_eligible_processes_off(void);
#endif

/*
 * Stubs
 */
#ifdef CLMS_PREEMPT
#define INCR_MEMPRIO()	do { 				\
	current->flags |= PF_LESS_THROTTLE;		\
} while(0)
#define DECR_MEMPRIO()	do {				\
	current->flags &= ~PF_LESS_THROTTLE;		\
} while(0)
#else
#define INCR_MEMPRIO()	do { } while(0)
#define DECR_MEMPRIO()	do { } while(0)
#endif

extern struct file_operations proc_clms_masterlist_operations;

#endif /* __KERNEL__ */

#endif /* !_CLUSTER_CLMS_H */
