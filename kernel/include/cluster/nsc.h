/*
 * 	NSC compatability support.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef	_CLUSTER_NSC_H
#define	_CLUSTER_NSC_H

#include <linux/types.h>
#include <asm/types.h>
#include <linux/param.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/fcntl.h>
#include <linux/file.h>
#include <linux/signal.h>
#include <linux/time.h>
#include <linux/cluster.h>
#include <linux/mm.h>
#include <cluster/arch/arch_regs.h>

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#define	NODEV	0

typedef char arch_char;
typedef unsigned char arch_uchar;
typedef int arch_short;
typedef unsigned short arch_ushort;
typedef int arch_int;
typedef unsigned int arch_uint;
typedef long arch_long;
typedef unsigned long arch_ulong;
typedef long long arch_longlong;
typedef unsigned long long arch_ulonglong;
typedef void *arch_ptr;
typedef long long dl_t;
typedef loff_t l_off_t;
typedef struct flock flock_t;
typedef enum boolean { B_FALSE, B_TRUE } boolean_t;
typedef boolean_t bool_t;
typedef unsigned long vaddr_t;

/* SSI_XXX: This should be handled based on kernel off_t */
//typedef unsigned long uoff_t;
typedef off_t uoff_t;

typedef struct rpc_cred cred_t;

extern void crfree(cred_t *);
extern void crhold(cred_t *);

#ifndef min
#define min(a,b) (((a)<(b))?(a):(b))
#endif
#ifndef max
#define max(a,b) (((a)>(b))?(a):(b))
#endif

#define MAXEND MAXEND64
#define TICKS()			jiffies
#define NANOSEC_PER_TICK	TICK

/*  SSI_XXX: This should really be in limits.h, shouldn't it? */
#ifndef USHRT_MAX
#define	USHRT_MAX	((u_short)~0U)
#endif
#ifndef SHRT_MAX
#define SHRT_MAX	((short)((u_short)~0U >> 1))
#endif

#define FSTYPSZ 20

#define PID_DATA	(this_node == CLUSTERNODE_INVAL ? 0 : (this_node << NODESHIFT))
#define INVALPID	NOPID

#define	NSC_YIELD()		yield()

#define NSC_COMM(_taskp)	(_taskp)->comm
#define NSC_PSARGS(_taskp)	(_taskp)->arg_start
#define NSC_UID(_taskp)		(_taskp)->uid
#define NSC_PID(_taskp)		(_taskp)->pid
#define	NSC_PFLAG(_taskp)	(_taskp)->flags
#define	NSC_PSYS		P_SYS
#define	NSC_MY_PID		current->pid
#define NSC_MY_EPID		current->epid
#define	NSC_UNIQUE_ID		((long)(current))
#define CRED()			NULL
#define	NSC_CRED()		NULL
#define NSC_SYSCRED()		NULL

#define NUM_ENTRIES(_x)		(sizeof(_x) / sizeof((_x)[0]))

#include <linux/signal.h>
/*
 * Signal set manipulation macros.
 */
#define SIGADDSET(_sigset, _sig)        sigaddset(&(_sigset), (_sig))
#define SIGINITSET(_sigset)             sigemptyset(&(_sigset))

/* JAG_XXX: Hack. */
#define SIGORSET(_sigset1, _sigset2) \
	sigorsets(&(_sigset1), &(_sigset1), &(_sigset2))

#define SIGISMEMBER(_sigset, _sig)      sigismember(&(_sigset), (_sig))

#define bzero(d, n)         memset((d), 0, (n))
#define bcopy(src, dest, n) memcpy ((dest), (src), (n))

#define idelay(_ticks) \
do { \
	__set_current_state(TASK_INTERRUPTIBLE); \
	schedule_timeout(_ticks); \
} while (0)

#define nidelay(_ticks) \
do { \
	__set_current_state(TASK_UNINTERRUPTIBLE); \
	schedule_timeout(_ticks); \
} while (0)

/*
 * NOTE: Code calling the sleep allocate interfaces should be coded
 *	 to check for allocation failures.  Some platforms may not
 *	 return errors in the sleep case, but NSC code should not
 *	 depend on this behavior. (Unfortunately, some does.)
 */
#define KMEM_SLEEP_ALLOC(_size)		kmalloc_nofail(_size)
#define KMEM_SLEEP_FREE(_ptr, _size)	kfree((void *)_ptr)

#define KMEM_NOSLEEP_ALLOC(_size)	kmalloc(_size, GFP_ATOMIC)
#define KMEM_NOSLEEP_FREE(_ptr, _size)	kfree((void *)_ptr)

/*
 * Generic queuing/dequeuing macros for circular, doubly-linked lists
 *	- add "ep" to TAIL of list pointed at by "epp"
 *	- "nextlabel" and "prevlabel" allow any structures with
 *	  arbitrarily named next/previous fields to use this macro.
 * NOTE: The "nextlabel" and "prevlabel" arguments are NOT enclosed
 *	 within parentheses due to incompatibility with "->" notation.
 *	 The caller should only specify the exact name of the next/previous
 *	 fields for these arguments.
 */
#define NSC_CDL_ENQUEUE(epp, ep, nextlabel, prevlabel) do {		\
	if (*(epp) == NULL) {						\
		(ep)->nextlabel = (ep)->prevlabel = (ep);		\
		*(epp) = (ep);						\
	} else {							\
		(ep)->nextlabel = *(epp);				\
		(ep)->prevlabel = (*(epp))->prevlabel;			\
		(*(epp))->prevlabel->nextlabel = (ep);			\
		(*(epp))->prevlabel = (ep);				\
	}								\
} while (0)

#define NSC_CDL_DEQUEUE(epp, ep, nextlabel, prevlabel) do {		\
	SSI_ASSERT(*(epp) != NULL);						\
	SSI_ASSERT((ep)->nextlabel && (ep)->prevlabel);			\
	if (*(epp) == (ep))						\
		*(epp) = (ep)->nextlabel;				\
									\
	if (*(epp) == (ep))						\
		*(epp) = NULL;						\
	else {								\
		(ep)->prevlabel->nextlabel = (ep)->nextlabel;		\
		(ep)->nextlabel->prevlabel = (ep)->prevlabel;		\
	}								\
} while (0)

extern int spawn_daemon_proc(char *, void (*)(void *), void *);
extern void exit_daemon_proc(void);
extern int spawn_daemon_thread(char *, void (*)(void *), void *);
extern void exit_daemon_thread(void);
extern void nsc_daemonize(void);
extern int set_daemon_prio(int prio, int policy);

static inline void allow_reschedule(void)
{
	if (rt_task(current)) {
		yield();
	} else
		cond_resched();
}
#if 0
/* Not needed with CONFIG_PREEMPT / SCHED_RR */
#define allow_reschedule() do {} while (0)
#endif

extern int spawn_daemon_user_proc(const char *, struct file *, const char *);
extern dev_t nsc_getdev(const char *, boolean_t, boolean_t);
extern struct file *nsc_dopen(dev_t, int, struct rpc_cred *, int *);


/* Allocates and initializes memory to zero. May return NULL,
 * even if you pass GFP_KERNEL. This is a Good Thing.
 */
static inline void *kzmalloc(size_t size, int flags)
{
	void *ret;

	/* SSI_XXX: kcalloc() array size limit is INT_MAX */
	ret = kmalloc(size, flags);
	if (ret)
		memset(ret, 0, size);
	return ret;
}

/*
 * This function should replaced with a regular kmalloc plus appropriate
 * error handling if NULL is returned. We have it here just as scaffolding,
 * to accomodate old code that relies on kmalloc() always returning non-NULL.
 *
 */
#define kmalloc_nofail(_size)	kmalloc(_size, GFP_KERNEL|__GFP_NOFAIL)
#define kzmalloc_nofail(_size)	kzmalloc(_size, GFP_KERNEL|__GFP_NOFAIL)

/*
 * array_cpy: like linux struct_cpy(), but it works with arrays.
 */
#define array_cpy(x,y) do {			\
	BUG_ON(sizeof(x) != sizeof(y)); 	\
	memcpy(x, y, sizeof((x)));		\
} while (0)


/* Cluster states */
#define CLUSTER_STATE_OFF	0x00
#define CLUSTER_STATE_CONFIG	0x01
#define CLUSTER_STATE_PREROOT	0x02
#define CLUSTER_STATE_POSTROOT	0x03
#ifdef CONFIG_VPROC
#define CLUSTER_STATE_POSTINIT	0x04
#endif /* VPROC */

/*
 * Cluster configuration information.
 */
extern clusternode_t this_node;
extern char *cluster_ifconfig;
extern char *cluster_clmsinfo;
extern char *cluster_interfaces;

extern int cluster_state;

extern void *nsc_generic_async_queue;

#endif /* !_CLUSTER_NSC_H */
