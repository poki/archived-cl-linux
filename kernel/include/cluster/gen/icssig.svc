/*
 * 	ICSSIG service definitions file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file defines the internal ICS RPCs.
 *
 * It is used as input to the tncgen script for automatic generation
 * of *_gen* source files.
 *
 * The format of each operation definition is (loosely):
 *	operation <op_name> [<op_attribute>]* {
 *		param <parm_attribute><parm_rpc> <parm_type> <parm_name>
 *		[param <parm_attribute><parm_rpc> <parm_type> <parm_name>]*
 *	}
 * where:
 *	<op_name> ::= identifier
 *	<op_attribute> ::= RPC | MSG | ASYNC | SKIP | NO_SIG_FORWARD
 *	<parm_attribute> ::= IN | OUT | INOUT
 *	<parm_rpc> ::= :VAR | :OOL | :XDR
 *	<parm_name> ::= identifier
 *
 * Operation attribute ASYNC requests generation of _SEND and _RECEIVE
 * variant support for an operation. Attribute MSG indicates that
 * no reply message (and thus no return value) is expected.
 *
 * Parameter attributes IN, OUT and INOUT are self-explanatory.
 *
 * This file is pre-processed by the C pre-processor as a preliminary step
 * in the generation process. Hence, C comments and conditional compilation
 * directives (including macros) may be used.
 */

service icssig cluster_icssig_svc 2 4

operation rics_sig_forward NO_SIG_FORWARD {
	param IN clusternode_t	exec_node
					/* execution node of signaled process */
	param IN long	transid		/* ICS transaction ID for RPC */
}
