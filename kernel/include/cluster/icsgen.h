/*
 * 	ICSGEN macros.
 *	Copyright 2001 Compaq Computer Corporation
 *
 * 	Ported to Alpha by Aneesh Kumar K.V(aneesh.kumar@digital.com)
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#ifndef	_CLUSTER_ICSGEN_H
#define	_CLUSTER_ICSGEN_H

typedef struct cli_opaque_handle {
	 	long	dummy;
	} cli_handle_t;
typedef struct svr_opaque_handle {
		long	dummy;
	} svr_handle_t;

#include <linux/types.h>
#include <linux/errno.h>
#include <cluster/rpc/xdr.h>
#include <cluster/synch.h>
#include <cluster/ics.h>
#include <cluster/ics_proto.h>

/*
 * Handy macros to delimit macro definitions.
 */
#define MACRO_BEGIN	do {
#define MACRO_END	} while (0)

/*
 * NSC components shouldn't care about the types that follow (used as
 * opaque pointers).
 */

typedef struct {
	unsigned int	ic_entrylen;
	unsigned int	ic_nentries;
	unsigned int	ic_nchunks;
	unsigned int	ic_nleft;
	unsigned int	ic_chunkorder;
	unsigned int	ic_perchunk;
	unsigned int	ic_chunksalloc;
	int		ic_freeme;
	void		**ic_chunks;
} ics_chunk_t;

typedef struct {
	void		*iu_addr;
	int		iu_len;
	int		iu_user;
} ics_userbuf_t;

/*
 * Note that the ICS-generated stubs contain invocations of marshaling
 * macros cli_encode_<type>() and cli_decode_<type>() on the client-side and
 * svr_decode_<type>() and svr_encode_<type>() on the server-side. <type> is
 * the parameter type. The first argument for all marshaling macros is the
 * ICS handle; the second is the parameter and a third may be the data
 * length.
 *	Client encode macros marshal IN parameter values from
 *	the calling stack (possibly indirectly for pointer types) onto the
 *	transport stream.
 *	Client decode macros marshal OUT parameter values from the
 *	back to the caller via pointers of the stack.
 *	Server decode macros marshal IN parameter values from the
 *	transport stream into local stack (or heap for OOL) space.
 *	Server encode macros marshal OUT parameter values from stack
 *	or heap onto the transport stream.
 *
 * Type names are augmented by further quailifying suffices and prefixes:
 *	prefix "ool_" indicates out-of-line data;
 *	prefix "var_" indicates variable length data;
 *	prefix "ptr_" indicates a potentially NULL reference to input data;
 *	suffix "_p" indicates a pointer to the datatype.
 */

#define cli_encode_as_char_array(hndl,x,type) \
	cli_encode_gen_var_char(hndl,(char *)x,sizeof(type))
#define svr_decode_as_char_array(hndl,x,type) \
	svr_decode_gen_var_char(hndl,(char *)x,sizeof(type))

/*
 * The following macros support input parameters with the PTR attribute
 * - references which may be NULL.
 *
 * param IN:PTR type *
 *
 * Encode the value of the pointer, then encode the structure as a char
 * array only if the pointer is non-NULL.
 * Decode the pointer value and, if non-NULL, decode the structure.
 */
static inline void cli_encode_ptr_object(cli_handle_t *hndl, const void *x,
					 int size)
{
	if (size != 0) {
		/*
		 * Using the indirect macro allows the cast without
		 * the compiler getting annoyed.
		 */
		cli_encode_gen_caddr_t_p(hndl, (caddr_t *)&x);
		if (x != NULL)
			cli_encode_gen_var_char(hndl, (char *)x, size);
	}
}

static inline void cli_decode_ptr_object(cli_handle_t *hndl, void **x, int size)
{
	caddr_t ptr;

	if (size != 0) {
		cli_decode_gen_caddr_t(hndl, ptr);
		if (ptr == NULL)
			*x = NULL;
		else
			cli_decode_gen_var_char(hndl, (char *)*x, size);
	} else
		*x = NULL;
}

static inline void svr_encode_ptr_object(svr_handle_t *hndl, const void *x,
					 int size)
{
	if (size != 0) {
		/*
		 * Using the indirect macro allows the cast without
		 * the compiler getting annoyed.
		 */
		svr_encode_gen_caddr_t_p(hndl, (caddr_t *)&x);
		if (x != NULL)
			svr_encode_gen_var_char(hndl, (char *)x, size);
	}
}

static inline void svr_decode_ptr_object(svr_handle_t *hndl, void **x, int size)
{
	caddr_t ptr;

	if (size != 0) {
		svr_decode_gen_caddr_t(hndl, ptr);
		if (ptr == NULL)
			*x = NULL;
		else
			svr_decode_gen_var_char(hndl, (char *)*x, size);
	} else
		*x = NULL;
}

/*
 * param IN:OOL:FREE cred_t *
 *
 * Note: We also encode a flag indicating if this is the system credential.
 *	 On decoding this flag, if TRUE, we point at the local sys_cred.
 *	 Otherwise, we decode the credentials into space on the local
 *	 stack and then copy into a kernel allocated cred with crdup().
 *	 The given credentials pointer initially points at the local stack
 *	 area, but ends up pointing at a kernel-allocated cred structure.
 *	 When the free is called, a non-NULL, non-system cred is freed.
 */
extern int cli_encode_ool_cred_t_p(cli_handle_t *, cred_t *);
extern int svr_decode_ool_cred_t_p_p(svr_handle_t *, cred_t **);
#define svr_decode_ool_cred_t_p(hndl,x)					\
	svr_decode_ool_cred_t_p_p(hndl,&x)
/* SSI_XXX: creds not yet implemented. */
#define svr_free_ool_cred_t_p(hndl,x)

/*
 * Marshaling macros for msghdr.
 *
 * param IN:OOL  msghdr *
 * param OUT:OOL msghdr **
 */
#define	cli_encode_ool_struct_msghdr_p(hndl,x)			\
	icscli_encode_ool_struct_msghdr(hndl,x,0)
#define	cli_decode_ool_struct_msghdr_p(hndl,x)			\
	icscli_decode_ool_struct_msghdr(hndl,&x,1)
#define	cli_decode_ool_struct_msghdr_p_p(hndl,x)		\
	icscli_decode_ool_struct_msghdr(hndl,x,1)
#define	svr_decode_ool_struct_msghdr_p(hndl,x)			\
	icssvr_decode_ool_struct_msghdr(hndl,&x,1)
#define	svr_decode_ool_struct_msghdr_p_p(hndl,x)		\
	icssvr_decode_ool_struct_msghdr(hndl,x,1)
#define	svr_free_ool_struct_msghdr_p(hndl,x)
	/*
	 * No free here.
	 * Note: This free should be performed by the server routine.
	 */
#define	svr_encode_ool_struct_msghdr_p_p(hndl,x)	\
	icssvr_encode_ool_struct_msghdr(hndl,*x,1)

extern int cli_encode_ool_string_p(cli_handle_t *, char *);
extern int cli_encoderesp_ool_string_p(cli_handle_t *, char *);
extern int svr_decode_ool_string_p_p(svr_handle_t *, char **);
extern int svr_encode_ool_string_p(svr_handle_t *, char *);
extern int cli_decode_ool_string_p(cli_handle_t *, char *);
extern int cli_decode_ool_string_p_p(cli_handle_t *, char **);

/*
 * param IN:OOL ics_chunk_t *
 * param OUT:OOL ics_chunk_t *
 * param INOUT:OOL ics_chunk_t *
 *
 */

extern int cli_encode_ool_ics_chunk_t_p(cli_handle_t *, ics_chunk_t *);
extern int cli_encoderesp_ool_ics_chunk_t_p(cli_handle_t *, ics_chunk_t *);
extern int cli_decode_ool_ics_chunk_t_p(cli_handle_t *, ics_chunk_t *);
#define svr_decode_ool_ics_chunk_t_p(hndl, x) \
	svr_decode_ool_ics_chunk_t_p_p(hndl, &(x));
extern int svr_decode_ool_ics_chunk_t_p_p(svr_handle_t *, ics_chunk_t **);
extern int svr_encode_ool_ics_chunk_t_p(svr_handle_t *, ics_chunk_t *);

extern void ics_chunk_init(ics_chunk_t *, unsigned int);
extern void *ics_chunk_add_entry(ics_chunk_t *);
extern void ics_chunk_free(ics_chunk_t *);
extern void ics_chunk_free_extra(ics_chunk_t *);
extern void ics_chunk_clear(ics_chunk_t *);
extern void ics_chunk_prealloc(ics_chunk_t *, unsigned int);
extern void *ics_chunk_add_entry_noalloc(ics_chunk_t *);
extern void *ics_chunk_get_entry(ics_chunk_t *, unsigned int);

/*
 * param IN:OOL ics_userbuf_t *
 * param OUT:OOL ics_userbuf_t *
 * param INOUT:OOL ics_userbuf_t *
 *
 * Define a generic user buffer descriptor. This is a total hack. icsgen
 * has special handling for this type. Do not use any other form of these
 * than those above.
 */
extern int cli_encode_ool_ics_userbuf_t_p(cli_handle_t *, ics_userbuf_t *);
extern int cli_encoderesp_ool_ics_userbuf_t_p(cli_handle_t *, ics_userbuf_t *);
extern int cli_decode_ool_ics_userbuf_t_p(cli_handle_t *, ics_userbuf_t *);
extern int svr_decode_ool_ics_userbuf_t_p(svr_handle_t *, ics_userbuf_t *);
extern int svr_decode_ics_userbuf_t_p(svr_handle_t *, ics_userbuf_t *);
extern int svr_encode_ool_ics_userbuf_t_p(svr_handle_t *, ics_userbuf_t *);
extern void svr_free_ool_ics_userbuf_t_p(svr_handle_t *, ics_userbuf_t *);

static inline void *ics_userbuf_getaddr(ics_userbuf_t *up)
{
	return up->iu_addr;
}

static inline int ics_userbuf_getlen(ics_userbuf_t *up)
{
	return up->iu_len;
}

extern void ics_userbuf_set(ics_userbuf_t *, const void *, int);

/*
 * Wrappers to get around issues with casts and macros.
 */

static inline int
cli_encode_var_ool_char_p(cli_handle_t *hndl, char *x, int len)
{
	return cli_encode_gen_var_ool_char_p(hndl, x, len);
}

static inline int
svr_decode_var_ool_char_p_p(svr_handle_t *hndl, char **x, int len)
{
	return svr_decode_gen_var_ool_char_p(hndl, *x, len);
}

/*
 * The following section of the file deals with nsc_rcall() emulation and
 * can be removed when nsc_rcall() is no longer supported.
 */
/* START NSC_RCALL STUFF */

#include <linux/sunrpc/types.h>
#include <linux/sunrpc/xdr.h>

/*
 * Call handles given out by nsc_hcreate(), used to place rpc calls to remote
 * nodes. Note that the call handles do no carry any state. They just just
 * provide minimal information about the remote node the service type
 * requested. This type of handle is appropriate for stateless and reliable
 * connections.
 */
typedef	union nsc_handle {
	struct {
		unsigned int		ch_node:16;	/* 2 bytes */
		unsigned int		ch_ics_chan:8;	/* 1 byte */
		unsigned int		ch_ics_ssvc:8;	/* 1 byte */
	} hinfo;
	caddr_t	handle;
} nsc_handle_t;

#define	NSCRPC_MAX_REPLYSIZE	64*1024

extern	nsc_handle_t	*nsc_hcreate(clusternode_t, int, u_short, char *);
extern	void		 nsc_hdestroy(nsc_handle_t *);

#define NSC_ENABLE_SIG_FWD	0x08
#define NSC_DISABLE_SIG_FWD	0x10
#define NSC_ENABLE_JOB_FWD	0x20
#define NSC_DISABLE_JOB_FWD	0x40

/*
 * Each individual service routine is described by a nsc_rpccall_t
 * data structure the information include the service function pointer
 * its corresponding XDR routines and result and argument sizes.
 */
typedef struct nsc_rpccall {
	void		(*trpc_svcfunc)(void *, void *); /* pointer to service function*/
	xdrproc_t	  trpc_xargs;		/* xdr routine for args */
	u_long		  trpc_argsz;		/* average size of arguments */
	xdrproc_t	  trpc_xresults;	/* xdr routine for results */
	u_long		  trpc_ressz;		/* average size of results */
	void		(*trpc_freefunc)(void *); /* pointer to free function*/
	char		 *trpc_name;		/* name of the call */
} nsc_rpccall_t;


/*
 * This is preprocessed-out to avoid compiler warnings in calling code
 * which typically doesn't cast argument and results parameters.
 */

extern int
nsc_rcall(
 	nsc_handle_t    *h,			/* client handle */
 	u_long		proc,			/* procedure number */
 	xdrproc_t       xargs,			/* xdr routine for args */
 	caddr_t		argsp,			/* pointer to args */
 	xdrproc_t	xresults,		/* xdr routine for results */
 	caddr_t		resultsp,		/* pointer to results */
	size_t		repsz_hint);		/* ignored */


/*
 * nsc_svc_register() service flags.
 */
#define NSC_SVC_INITIALIZED	0x01	/* Service is initialized */
#define NSC_SVC_NO_SIG_FWD	0x02	/* Disable signal forwarding */
#define NSC_SVC_NO_JOB_FWD	0x04	/* Disable job signal forwarding */

extern int
nsc_svc_register(
	int		svc,			/* NSC SVC number */
	int		num_procs,		/* num rpcgen-defined procs */
	nsc_rpccall_t	svcs[],			/* rpcgen-defined proc table */
	int		lwm_service_handles,	/* max free ICS svr handles */
	int		hwm_service_handles,	/* min free ICS svr handles */
	int		lwm_flowpages,		/* flow-control lwm */
	int		hwm_flowpages,		/* flow-control hwm */
	int		flag);			/* service flags */
/* END NSC_RCALL STUFF */

extern int icssvr_decode_xdr(svr_handle_t  *, u_char *, void *);
extern int icssvr_encode_xdr(svr_handle_t  *, u_char *, void *);
extern int icscli_decode_xdr(cli_handle_t  *, u_char *, void *);
extern int icscli_encode_xdr(cli_handle_t  *, u_char *, void *);
extern void icssvr_free_xdr(svr_handle_t  *, u_char *, void *);

#endif	/* !_CLUSTER_ICSGEN_H */
