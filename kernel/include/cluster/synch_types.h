/*
 * 	Cluster lock interface to Linux primitives.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file provides types to map generic NSC locking interfaces
 * to Linux platform specific locking primitives.
 */

#ifndef _CLUSTER_SYNCH_TYPES_H
#define _CLUSTER_SYNCH_TYPES_H

#include <linux/config.h>
#include <linux/types.h>
#ifdef LINUX_SSI_EVENT
#include <linux/completion.h>
#else
#include <linux/wait.h>
#endif
#include <linux/spinlock.h>
#include <asm/semaphore.h>
#include <asm/atomic.h>

#define __SSI_LOCK_DEBUG		0

typedef atomic_t ATOMIC_INT_T;

#if __SSI_LOCK_DEBUG

#define	__SSI_LOCK_DECLARE_DEBUG		\
	long			__creator;	\
	long			__magic

#else

#define	__SSI_LOCK_DECLARE_DEBUG

#endif

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	spinlock_t		sp_lock;
	struct task_struct	*sp_owner;
} SPIN_LOCK_T;

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	spinlock_t		isp_lock;
	struct task_struct	*isp_owner;
} SOFTIRQ_SPIN_LOCK_T;

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	spinlock_t		isp_lock;
	struct task_struct	*isp_owner;
	unsigned long		isp_flags;
} HARDIRQ_SPIN_LOCK_T;

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	rwlock_t		rwsp_lock;
} RW_SPIN_LOCK_T;

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	struct semaphore	sl_semaphore;
	struct task_struct	*sl_owner;
} LOCK_T;

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	struct rw_semaphore	rwsl_semaphore;
	struct task_struct	*rwsl_owner;
} RW_LOCK_T;

typedef struct {
	SPIN_LOCK_T lock;
} COND_LOCK_T;

typedef struct {
	SOFTIRQ_SPIN_LOCK_T lock;
} SOFTIRQ_COND_LOCK_T;

typedef struct {
	HARDIRQ_SPIN_LOCK_T lock;
} HARDIRQ_COND_LOCK_T;

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	wait_queue_head_t cnd_wqh;
} CONDITION_T;

typedef struct {
	CONDITION_T	cndi_cond;
} INTR_CONDITION_T;

#ifdef LINUX_SSI_EVENT
typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	struct completion	ev_com;
#define ev_done	ev_com.done
#define ev_wqh	ev_com.wait
#define ev_lock ev_wqh.lock
} EVENT_T;
#else
typedef struct {
	struct list_head	ew_waitlist;
	struct task_struct	*ew_proc;
	char			ew_state;
} EVENT_WAIT_T;

typedef struct {
	__SSI_LOCK_DECLARE_DEBUG;
	struct list_head	ev_waitlist;
	spinlock_t		ev_lock;
	char			ev_state;
} EVENT_T;
#endif /* !LINUX_SSI_EVENT */

typedef struct {
	EVENT_T			evi_event;
} INTR_EVENT_T;

#undef __SSI_LOCK_DECLARE_DEBUG
#undef __SSI_LOCK_DECLARE_LOCK_DEBUG

#endif /* !_CLUSTER_SYNCH_TYPES_H */
