#
#	Copyright (c) 2002 Compaq Computer Corporation
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as
#	published by the Free Software Foundation; either version 2 of
#	the License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
#	or NON INFRINGEMENT.  See the GNU General Public License for more
#	details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
#
#
#	<name>.list:		list of types
#	<name>.template:	sed template for each type
#	type_template.awk:	awk script
#

function do_template_line () {
	if (tsection == 1) {
		if (c_comment)
			sub("^#", " *");
		print $0;
	} else if (tsection == 2)
		blines[bl++] = $0;
	else if (tsection == 3)
		tlines[tl++] = $0;
	else if (tsection == 4)
		elines[el++] = $0;
}

FILENAME ~ /.*\.template/ {
	if ($1 == "#") {
		if ($2 == "%%COMMENT%%") {
			if (tsection == 0) {
				tsection = 1;
				if ($3 == "*") {
					c_comment = 1;
					print "/*";
				}
			}
			next;
		} else if ($2 == "%%BEGIN%%") {
			if (tsection == 1 && c_comment)
				print " */";
			tsection = 2;
			next;
		} else if ($2 == "%%TEMPLATE%%") {
			tsection = 3;
			next;
		} else if ($2 == "%%END%%") {
			tsection = 4;
			next;
		}
	}
	do_template_line();
	next;
}

function do_type () {
	if (lsection == 1) {
		realtype = $1;
		typename = realtype;
		mtypename = realtype;
	} else if (lsection == 2) {
		realtype = $1;
		typename = "arch_"$1;
		mtypename = typename;
	} else if (lsection == 3) {
		realtype = "struct "$1;
		typename = realtype;
		mtypename = "struct_"$1
	} else if (lsection == 4) {
		realtype = "union "$1;
		typename = realtype;
		mtypename = "union_"$1
	}

	for (i = 0; i < tl; i++) {
		outstr = tlines[i];
		if (tlines[i] ~ /%%REALTYPE%%/ && typename == realtype)
			continue;
		gsub("%%REALTYPE%%", realtype, outstr);
		gsub("%%TYPENAME%%", typename, outstr);
		gsub("%%MTYPENAME%%", mtypename, outstr);
		print outstr;
	}
}

FILENAME ~ /.*\.list/ {
	if ($1 ~ "^#") {
		if ($2 == "%%REALTYPE%%")
			lsection = 1;
		else if ($2 == "%%ARCHTYPE%%")
			lsection = 2;
		else if ($2 == "%%STRUCTTYPE%%")
			lsection = 3;
		else if ($2 == "%%UNIONTYPE%%")
			lsection = 4;
		next;
	} else if ($1 == "%%DEFINE%%") {
		define = $2;
		for (i = 0; i < bl; i++) {
			sub("%%DEFINE%%", define, blines[i]);
			print blines[i];
		}
	}
	if (NF != 0 && lsection != 0)
		do_type();
	next;
}

END {
	for (i = 0; i < el; i++) {
		sub("%%DEFINE%%", define, elines[i]);
		print elines[i];
	}
}
