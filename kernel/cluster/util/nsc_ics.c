/*
 * 	ICS nsc_rcall emulation layer.
 *	Copyright 2001 Compaq Computer Corporation
 *	Changes Copyright 2008 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/param.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/limits.h>
#include <linux/sunrpc/auth.h>
#include <asm/uaccess.h>
#include <asm/user.h>

#include <cluster/nsc.h>
#include <cluster/icsgen.h>
#include <cluster/ics.h>
#include <cluster/rpc/rpc.h>
#include <cluster/rpc/xdr.h>
#include <cluster/assert.h>

u_long	nscrpc_bufchunk[] = {
	256,
	1024,
	PAGE_SIZE,
};

extern void xdrmsghdr_init(XDR *, struct msghdr *, enum xdr_op);
extern void freemsghdr(struct msghdr *);

#define	nsc_chcount	(sizeof(nscrpc_bufchunk)/sizeof(nscrpc_bufchunk[0]))

/*
 * This file contains NSC component-level support routines for ICS.
 * This includes data marshaling routines and emulation of the
 * obsolesent nsc_rcall() interface.
 */

void
free_data_callback(u_char *data, long callarg)
{
	(void)callarg;
	kfree((caddr_t) data);
}

/*
 * param IN:OOL char *
 * param OUT:OOL char **
 * param INOUT:OOL char *
 */
int
cli_encode_ool_string_p(
	cli_handle_t	*hndl,
	char		*stringp)
{
	int	len = stringp ? strlen(stringp)+1 : 0;

	cli_encode_gen_int(hndl, len);
	if (len == 0)
		return(0);
	else
		return(icscli_encode_ool_data_t(hndl, (u_char*)stringp, len,
						NULL, 0));
}
int
svr_decode_ool_string_p_p(
	svr_handle_t	*hndl,
	char		**addr)
{
	int	size;

	svr_decode_gen_int(hndl,size);
	if (size == 0) {
		*addr = NULL;
		return(0);
	}
	*addr = kmalloc_nofail(size);
	return(icssvr_decode_ool_data_t(hndl, *addr, size));
}

int
cli_encoderesp_ool_string_p(
    cli_handle_t    *hndl,
    char        *stringp)
{
    int len = stringp ? strlen(stringp)+1 : 0;

    if (len == 0) {
        return(0);
    } else {
        return(icscli_encoderesp_ool_data_t(hndl, stringp, len));
    }
}

int
svr_encode_ool_string_p(
	svr_handle_t	*hndl,
	char		*stringp)
{
	int	len = stringp ? strlen(stringp)+1 : 0;

	svr_encode_gen_int(hndl, len);
	if (len == 0)
		return(0);
	else
		return(icssvr_encode_ool_data_t(hndl, (u_char*)stringp, len,
						free_data_callback, len));
}
int
cli_decode_ool_string_p(
	cli_handle_t	*hndl,
	char		*addr)
{
	int	size;

	cli_decode_gen_int(hndl,size);
	if (size == 0)
		return(0);
	return(icscli_decode_ool_data_t(hndl, addr, size));
}
int
cli_decode_ool_string_p_p(
	cli_handle_t	*hndl,
	char		**addr)
{
	int	size;

	cli_decode_gen_int(hndl,size);
	if (size == 0) {
		*addr = NULL;
		return(0);
	}
	*addr = kmalloc_nofail(size);
	return(icscli_decode_ool_data_t(hndl, *addr, size));
}

/*
 * param IN:OOL:FREE cred_t *
 *
 * Note: We also encode a flag indicating if this is the system credential.
 *	 On decoding this flag, if TRUE, we point at the local sys_cred.
 *	 Otherwise, we decode the credentials into space on the local
 *	 stack and then copy into a kernel allocated cred with crdup().
 *	 The given credentials pointer initially points at the local stack
 *	 area, but ends up pointing at a kernel-allocated cred structure.
 *	 When the free is called, a non-NULL, non-system cred is freed.
 *
 * Note: Cred parameters are always sent out-of-line because the size
 *	 of a cred is governed by a tuneable parameter.  A large cred
 *	 could overrun the inline transport limits.
 */

int
cli_encode_ool_cred_t_p(
	cli_handle_t	*hndl,
	cred_t		*credp)
{
	/* SSI_XXX: creds not yet implemented. */
	return 0;
}

int
svr_decode_ool_cred_t_p_p(
	svr_handle_t	*hndl,
	cred_t		**credpp)
{
	/* SSI_XXX: creds not yet implemented. */
	return 0;
}

void
free_cred_callback(u_char *credp, long callarg)
{
	/* SSI_XXX: creds not yet implemented. */
}


/*
 * Debugging information for xdr-related functions.
 */
#ifdef DEBUG

#define	ICSXDR_DBG_ENCODE	0x01
#define	ICSXDR_DBG_DECODE	0x02
#define	ICSXDR_DBG_FREE		0x04
#define	ICSXDR_DBG_RCALL	0x08

int	icsxdrlog;
#define		ICSXDRLOG(filter,fmt,arg)		\
		if (icsxdrlog & (filter))		\
			printk(fmt,arg)
#else

#define		ICSXDRLOG(filter,fmt,arg)

#endif

extern struct xdr_ops xdrmsghdr_ops;

/*
 * XDR-encode into msghdr.
 */
boolean_t
icsxdr_encode_msghdr(XDR *xdrs,
		     xdrproc_t x_proc,
		     caddr_t objp,
		     struct msghdr **mpp)
{
	boolean_t	rval;

	SSI_ASSERT(objp != NULL || x_proc == (xdrproc_t)xdr_void);
	SSI_ASSERT(x_proc != NULL);
	SSI_ASSERT(mpp != NULL);

	/*
	 * Allocate a msghdr/iovec starting with the standard size.
	 */
	*mpp = kzmalloc_nofail(sizeof(struct msghdr));
	(*mpp)->msg_iov = kzmalloc_nofail(sizeof(struct iovec)
					  * ICS_DEF_OOL_SEGMENTS);
	/* Initialize first iovec of msghdr. */
	(*mpp)->msg_iov[0].iov_base = kzmalloc_nofail(nscrpc_bufchunk[0]);
	(*mpp)->msg_iov[0].iov_len = 0;
	(*mpp)->msg_iovlen = 1;

	/*
	 * Initialize the xdr struct for encoding into this msghdr
	 */
	xdrmsghdr_init(xdrs, *mpp, XDR_ENCODE);

	/*
	 * Encode using the given xdr calling tree.
	 */
	rval = (*x_proc)(xdrs, (u_char *) objp);

	return rval;
}

/*
 * XDR-decode from msghdr.
 */
boolean_t
icsxdr_decode_msghdr(XDR *xdrs,
		     xdrproc_t x_proc,
		     caddr_t objp,
		     struct msghdr *mp)
{
	boolean_t	rval;

	SSI_ASSERT(objp != NULL || x_proc == (xdrproc_t)xdr_void);
	SSI_ASSERT(x_proc != NULL);
	SSI_ASSERT(mp != NULL);

	/*
	 * Initialize the xdr struct for decoding from this msghdr.
	 */
	xdrmsghdr_init(xdrs, mp, XDR_DECODE);

	/*
	 * Decode using the given xdr calling tree.
	 */
	rval = (*x_proc)(xdrs, (u_char *) objp);

	return rval;
}



int
icscli_encode_xdr(cli_handle_t	*hndl,
		  u_char	*objp,
		  void 		*x_proc)
{
	XDR	x;
	struct msghdr *m = NULL;

	SSI_ASSERT(objp != NULL || x_proc == xdr_void);
	SSI_ASSERT(x_proc != NULL);

	ICSXDRLOG(ICSXDR_DBG_ENCODE, "%s", "icscli_encode_xdr: called\n");

	/*
	 * Encode using the given xdr calling tree.
	 * If successful, add the msghdr to the ICS output stream.
	 */
	if (icsxdr_encode_msghdr(&x, x_proc, (caddr_t) objp, &m)) {
		return icscli_llencode_ool_struct_msghdr(hndl, m, 0);
	}
	else {
		freemsghdr(m);
		return 1;
	}
}

int
icscli_decode_xdr(cli_handle_t	*hndl,
		  u_char	*objp,
		  void		*x_proc)
{
	XDR	x;
	struct msghdr *m = NULL;
	int	ret;

	SSI_ASSERT(objp != NULL || x_proc == xdr_void);
	SSI_ASSERT(x_proc != NULL);

	ICSXDRLOG(ICSXDR_DBG_DECODE, "%s", "icscli_decode_xdr: called\n");

	/*
	 * Get a msghdr from ICS containing XDR encoded data.
	 */
	ret = icscli_lldecode_ool_struct_msghdr(hndl, &m, 1);
	if (ret || m ==  NULL)
		return ret;

	/*
	 * XDR-decode from the msghdr and free it whatever the outcome.
	 */
	ret = !icsxdr_decode_msghdr(&x, x_proc, (caddr_t) objp, m);
	freemsghdr(m);
	return ret;
}

int
icssvr_decode_xdr(svr_handle_t	*hndl,
		  u_char	*objp,
		  void		*x_proc)
{
	XDR	x;
	struct msghdr *m = NULL;
	int	ret;

	SSI_ASSERT(objp != NULL || x_proc == xdr_void);
	SSI_ASSERT(x_proc != NULL);

	ICSXDRLOG(ICSXDR_DBG_DECODE, "%s", "icssvr_decode_xdr: called\n");

	/*
	 * Get a msghdr from ICS containing XDR encoded data.
	 */
	ret = icssvr_lldecode_ool_struct_msghdr(hndl, &m, 1);
	if (ret || m == NULL)
		return ret;

	/*
	 * Decode using the given xdr calling tree.
	 * And whatever the outcome, free the msghdr.
	 */
	ret = !icsxdr_decode_msghdr(&x, x_proc, (caddr_t) objp, m);
	freemsghdr(m);
	return ret;
}

int
icssvr_encode_xdr(svr_handle_t	*hndl,
		  u_char	*objp,
		  void		*x_proc)
{
	XDR	x;
	struct msghdr *m = NULL;

	SSI_ASSERT(objp != NULL || x_proc == xdr_void);
	SSI_ASSERT(x_proc != NULL);

	ICSXDRLOG(ICSXDR_DBG_ENCODE, "%s", "icssvr_encode_xdr: called\n");

	/*
	 * Encode using the given xdr calling tree.
	 * If successful, add the msghdr to the ICS output stream.
	 */
	if (icsxdr_encode_msghdr(&x, x_proc, (caddr_t) objp, &m)) {
		return icssvr_llencode_ool_struct_msghdr(hndl, m, 0);
	}
	else {
		freemsghdr(m);
		panic("icssvr_encode_xdr: failure is not an option\n");
		return 0;
	}
}

void
icssvr_free_xdr(svr_handle_t	*hndl,
		u_char		*objp,
		void		*x_proc)
{
	XDR	x;

	SSI_ASSERT(objp != NULL || x_proc == xdr_void);
	SSI_ASSERT(x_proc != NULL);

	ICSXDRLOG(ICSXDR_DBG_FREE, "%s", "icssvr_free_xdr: called\n");

	/*
	 * Initialize the xdr struct for a free call.
	 */
	x.x_op = XDR_FREE;
	x.x_public = (caddr_t) hndl;

/* JAG_XXX: NEEDS WORK! */
	(void) (((xdrproc_t)x_proc)(&x, objp));

}


/*
 * ICS nsc_rcall() emulation...
 */
static nsc_rpccall_t *icsnsc_rpccall_table[ICS_MAX_CHANNELS]
					  [ICS_MAX_SUBSERVICES];

/*
 * Table of per-subservice flags set when a subservice is registered
 * via nsc_svc_register().
 */
int nsc_svc_flags[ICS_MAX_CHANNELS][ICS_MAX_SUBSERVICES];
CONDITION_T ics_svc_flags_cond;
COND_LOCK_T ics_svc_flags_mutex __cacheline_aligned_in_smp;

int icsnsc_rpc_dispatch(int, int, cred_t *, struct msghdr *, struct msghdr **);

/*
 * Initialize the nsc_rcall() transport interface at boot time.
 */
int nsc_rcall_init(void)
{
	INIT_CONDITION(&ics_svc_flags_cond);
	INIT_COND_LOCK(&ics_svc_flags_mutex);

	return 0;
}


/*
 * Hand-coded client-side ICS stub for all emulated RPC services:
 */
int
cli_icsnsc_rcall(
	clusternode_t node,
	int	*rval,
	int	svc,
	int	procnum,
	cred_t	*credp,
	struct msghdr *margsp,
	struct msghdr **mrespp,
	size_t	mrespsz_hint,
	int	flag)
{
	nsc_rpccall_t	*funcinfoptr;
	int cli_stat = 0;
	cli_handle_t *handle = icscli_handle_get(node, svc, 0);
	int chan, ssvc;

	if (handle == NULL) {
		cli_stat = -EAGAIN;
		goto err_out;
	}

	cli_encode_gen_int(handle,svc);
	cli_encode_gen_int(handle,procnum);
	cli_stat = cli_encode_ool_cred_t_p(handle,credp);
	if (cli_stat)
		goto out;
	cli_stat = cli_encode_ool_struct_msghdr_p(handle,margsp);
	if (cli_stat)
		goto out;

	chan = ICS_CHAN(svc);
	ssvc = ICS_SSVC(svc);

	/*
	 * We must handle the race where we attempt to send a message
	 * on a channel service that hasn't been initialized yet.
	 * We sleep here until the channel is initialized.
	 */
	LOCK_COND_LOCK(&ics_svc_flags_mutex);
	while (!(nsc_svc_flags[chan][ssvc] & NSC_SVC_INITIALIZED))
		WAIT_CONDITION(&ics_svc_flags_cond, &ics_svc_flags_mutex);
	UNLOCK_COND_LOCK(&ics_svc_flags_mutex);

	/*
	 * Translate (svc,procnum) to rpccall entry and look up size
	 * of return data to expect.
	 */
	funcinfoptr = icsnsc_rpccall_table[chan][ssvc] + procnum;
	SSI_ASSERT(funcinfoptr != NULL);

	(void)icscli_send(handle,
			  procnum,
			  icscli_wait_callback,
			  0);

	cli_stat = icscli_wait(handle,flag);
	if (cli_stat)
		goto out;

	cli_decode_gen_int_p(handle,rval);

	cli_stat = cli_decode_ool_struct_msghdr_p_p(handle,mrespp);

	if (cli_stat)
		goto out;
out:
	icscli_handle_release(handle);
	if (cli_stat != 0)
		cli_stat = -EREMOTE;
err_out:

	return(cli_stat);
}


/*
 * Hand-coded client-side ICS stub for all emulated MSG services:
 */
int
cli_icsnsc_rcall_msg(
	clusternode_t node,
	int	svc,
	int	procnum,
	cred_t	*credp,
	struct msghdr *margsp)
{
	int cli_stat = 0;
	cli_handle_t *handle = icscli_handle_get(node, svc, ICS_NO_REPLY);

	if (handle == NULL) {
		cli_stat = -EAGAIN;
		goto err_out;
	}

	cli_encode_gen_int(handle,svc);
	cli_encode_gen_int(handle,procnum);
	cli_stat = cli_encode_ool_cred_t_p(handle,credp);
	if (cli_stat)
		goto out;
	cli_stat = cli_encode_ool_struct_msghdr_p(handle,margsp);
	if (cli_stat)
		goto out;

	cli_stat = icscli_send(
		handle,
		procnum,
		icscli_handle_release_callback,
		0);

out:
	if (cli_stat != 0)
		cli_stat = -EREMOTE;
err_out:
	return(cli_stat);

}


/*
 * Hand-coded server-side ICS stub for all emulated services:
 */
int
svr_icsnsc_rcall(svr_handle_t *handle)
{
	int	svr_stat = 0;
	int	rval = 0;
	cred_t	credp_val;
	cred_t	*credp = &credp_val;
	int	procnum;
	int	svc;
	struct msghdr *margsp = NULL;
	struct msghdr *mrespp_val = NULL;
	struct msghdr **mrespp = &mrespp_val;

	svr_decode_gen_int(handle,svc);
	svr_decode_gen_int(handle,procnum);
	svr_stat = svr_decode_ool_cred_t_p(handle,credp);
	if (svr_stat)
		goto out;
	svr_stat = svr_decode_ool_struct_msghdr_p(handle,margsp);
	if (svr_stat)
		goto out;

	icssvr_decode_done(handle);

	rval = icsnsc_rpc_dispatch(svc, procnum, credp, margsp, mrespp);

	if (rval != 0)
		panic("svr_icsnsc_rcall: icsnsc_rpc_dispatch returned %d",rval);

	if (*mrespp == NULL)
		goto out;
	svr_encode_gen_int(handle,rval);
	svr_stat = svr_encode_ool_struct_msghdr_p_p(handle,mrespp);
	if (svr_stat)
		goto out;

out:
	svr_free_ool_cred_t_p(handle,credp);
	svr_free_ool_struct_msghdr_p(handle,margsp);
	if (svr_stat) {
		icssvr_rewind(handle);
		svr_encode_gen_int(handle,svr_stat);
	}
	return(0);
}

/*
 * At nsc_rcall service registration, we must register the associated
 * ICS service. This has special-purpose client and server stubs to
 * package the rcall interface and call the corresponding rpcgen-defined
 * server routine. We need to build a service dispatch table for ICS to
 * call a special stub to perform the nsc_rcall() dispatch.
 */
int
nsc_svc_register(
	int		svc,
	int		num_procs,
	nsc_rpccall_t	svcs[],
	int		lwm_service_handles,
	int		hwm_service_handles,
	int		lwm_flowpages,
	int		hwm_flowpages,
	int		flag)
{
	int	(**ics_svr_table)(svr_handle_t *);
	char	**ics_opnames;
	char	*ics_svc_name;
	int	i;

	SSI_ASSERT(ICS_CHAN(svc) >= 0 && ICS_CHAN(svc) < ics_num_channels);
	SSI_ASSERT(ICS_SSVC(svc) >= 0 && ICS_SSVC(svc) < ICS_MAX_SUBSERVICES);

	/*
	 * Store the subservice's registration flags.
	 */
	nsc_svc_flags[ICS_CHAN(svc)][ICS_SSVC(svc)] = flag;

	/*
	 * Allocate space for the service name string which is manufactured
	 * in the form "nsc_rcall[<chan>,<subservice>]".
	 */
	ics_svc_name = kmalloc_nofail(PATH_MAX * sizeof(char));
	sprintf(ics_svc_name, "nsc_rcall[%d,%d]", ICS_CHAN(svc), ICS_SSVC(svc));

	/*
	 * Allocate space for the ICS server dispatch table mirroring
	 * the rpcgen-defined routines and initialize all entries to
	 * point at the common server stub.
	 */
	ics_svr_table = kmalloc_nofail(num_procs * sizeof(int (*)));
	for (i = 0; i < num_procs; i++)
		ics_svr_table[i] = &svr_icsnsc_rcall;

	/*
	 * Allocate space for the operation name table
	 * initialize to point to the rpcgen-defined table entries.
	 */
	ics_opnames = kmalloc_nofail(num_procs * sizeof(char **));
	for (i = 0; i < num_procs; i++)
		ics_opnames[i] = svcs[i].trpc_name;

	/*
	 * Record the rpcgen-erated call table address in a translation
	 * table for use for rcall dispatch in the server stub.
	 */
	icsnsc_rpccall_table[ICS_CHAN(svc)][ICS_SSVC(svc)] = svcs;

	/*
	 * Register with ICS.
	 */
	ics_svc_register(svc, ics_svc_name, num_procs, ics_svr_table,
			 ics_opnames, lwm_service_handles, hwm_service_handles,
			 lwm_flowpages, hwm_flowpages);

	/*
	 * Set flag indicating channel subservice is fully initialized
	 * and signal any RPCs that may be blocked waiting for the
	 * subservice to be initialized.
	 */
	LOCK_COND_LOCK(&ics_svc_flags_mutex);
	nsc_svc_flags[ICS_CHAN(svc)][ICS_SSVC(svc)] |= NSC_SVC_INITIALIZED;
	UNLOCK_COND_LOCK(&ics_svc_flags_mutex);
	SIGNAL_CONDITION(&ics_svc_flags_cond);

	return 0;
}


/*
 * This routine emulates nsc_rcall() using ICS to perform the
 * remote function call.
 */
int
nsc_rcall(
	nsc_handle_t    *h,		/* client handle */
	u_long		proc,		/* procedure number */
	xdrproc_t	xargs,		/* xdr routine for args */
	caddr_t		argsp,		/* pointer to args */
	xdrproc_t	xresults,	/* xdr routine for results */
	caddr_t		resultsp,	/* pointer to results */
	size_t		repsz_hint)	/* result size hint */
{
	XDR		x;
	nsc_handle_t	th;
	struct msghdr	*margsp = NULL;
	struct msghdr	*mresp = NULL;
	cred_t		*credp = !in_interrupt() ? CRED() :
						  NSC_SYSCRED();
	clusternode_t	node;
	int		rval;
	int		svc;
	int		ics_stat = 0;
	int		ics_flag, svc_flag;
	mm_segment_t	save_fs = get_fs();

	ICSXDRLOG(ICSXDR_DBG_RCALL, "nsc_rcall: called, proc %d\n", (int)proc);

	/*
	 * Extract target node number from the nsc_handle.
	 */
	th.handle = (caddr_t) h;
	node = th.hinfo.ch_node;
	svc = ICS_NSC_SVC(th.hinfo.ch_ics_chan, th.hinfo.ch_ics_ssvc);

	/*
	 * Using the given xdr routine encode the arguments into
	 * a msghdr.
	 */
	if (!icsxdr_encode_msghdr(&x, xargs, argsp, &margsp)) {
		ics_stat = CLUSTER_RPC_CANTENCODEARGS;
		freemsghdr(margsp);
		ICSXDRLOG(ICSXDR_DBG_RCALL,
			  "nsc_rcall: returning %d\n", ics_stat);
		goto out;
	}

	/*
	 * If results are expected (xresults != xdr_void) then
	 * perform an ICS RPC. Otherwise send an ICS message.
	 */

	set_fs(KERNEL_DS);
	if (xresults != (xdrproc_t)xdr_void) {
		ics_flag = 0;
		svc_flag = nsc_svc_flags[ICS_CHAN(svc)][ICS_SSVC(svc)];

		if (svc_flag & NSC_SVC_NO_SIG_FWD)
			ics_flag |= ICS_NO_SIG_FORWARD;
		if (svc_flag & NSC_SVC_NO_JOB_FWD)
			ics_flag |= ICS_NO_JOB_FORWARD;

		ics_stat = cli_icsnsc_rcall(node,
					    &rval,
					    svc,
					    proc,
					    credp,
					    margsp,
					    &mresp,
					    repsz_hint,
					    ics_flag);

		if (ics_stat == 0)
			ics_stat = rval;	/* rval is the remote stat */

		if (ics_stat == 0) {
			/*
			 * Decode results and free the returned msghdr.
			 */
			SSI_ASSERT(mresp != NULL);
			if (!icsxdr_decode_msghdr(&x, xresults, resultsp, mresp))
				ics_stat = CLUSTER_RPC_CANTDECODERES;
			freemsghdr(mresp);
		}
	} else {
		ics_stat = cli_icsnsc_rcall_msg(node,
						svc,
						proc,
						credp,
						margsp);
	}
	set_fs(save_fs);

	ICSXDRLOG(ICSXDR_DBG_RCALL, "nsc_rcall: returning %d\n", ics_stat);

out:

	return ics_stat;

}


/*
 * This routine is called by an ICS server stub specific to a service for
 * emulation of the nsc_rcall() interface: an rpcgen defined server routine
 * is dispatched with arguments/results marshalled from/to msghdrs.
 */
int
icsnsc_rpc_dispatch(
	int		svc,
	int		procnum,
	cred_t		*credp,
	struct msghdr	*margsp,
	struct msghdr	**mrespp)
{
	nsc_rpccall_t	*funcinfoptr;
	caddr_t		argp = 0;
	caddr_t		resp = 0;
	XDR		x;
	XDR		*xdrs = &x;
	int		error = 0;

	ICSXDRLOG(ICSXDR_DBG_RCALL,
		  "icsnsc_rpc_dispatch: called for svc 0x%x, ", svc);
	ICSXDRLOG(ICSXDR_DBG_RCALL, "procnum %d, ", procnum);

	/*
	 * Translate (svc,procnum) to rpccall entry.
	 */
	funcinfoptr = icsnsc_rpccall_table[ICS_CHAN(svc)][ICS_SSVC(svc)]
			+ procnum;

	ICSXDRLOG(ICSXDR_DBG_RCALL, "\"%s\"\n", funcinfoptr->trpc_name);

	/*
	 * Allocate initial memory for the arguments and the result.
	 */
	if (funcinfoptr->trpc_xargs != (xdrproc_t)xdr_void) {
		argp = kzmalloc_nofail(funcinfoptr->trpc_argsz);
	}
	if (funcinfoptr->trpc_xresults != (xdrproc_t)xdr_void) {
		resp = kzmalloc_nofail(funcinfoptr->trpc_ressz);
	}

	/*
	 * Decode any arguments and free the associated mblks.
	 */
	if (margsp) {
		if (!icsxdr_decode_msghdr(xdrs, funcinfoptr->trpc_xargs,
					argp, margsp)) {
			error = CLUSTER_RPC_CANTDECODEARGS;
			freemsghdr(margsp);
			goto out;
		}
		freemsghdr(margsp);
	}


	/* SSI_XXX: Figure out what to use for creds in Linux */
	/* See nfsd_setuser() */
	/*
	 * Switch to credentials received in requested.
	 */

	/*
	 * Dispatch the server routine.
	 * Note that no authentication or service request info is passed
	 * since these parameters are not used.
	 */
	(*funcinfoptr->trpc_svcfunc)(argp, resp);

	/* SSI_XXX: Figure out what to use for creds in Linux */
	/*
	 * Reset credentials.
	 */

	/*
	 * Encode the results into outgoing msghdr.
	 */
	if (mrespp) {
		/*
		 * Encode the results into msghdr ... returning nothing
		 * if this fails.
		 */
		if (funcinfoptr->trpc_xresults != (xdrproc_t)xdr_void &&
		    !icsxdr_encode_msghdr(xdrs,funcinfoptr->trpc_xresults,
					resp,mrespp)) {
			error = CLUSTER_RPC_SYSTEMERROR;
			freemsghdr(*mrespp);
			*mrespp = NULL;
		}
        }

out:
	/*
	 * Free the argument memory.
	 */
	xdrs->x_op = XDR_FREE;
	if (argp) {
		if (!(*funcinfoptr->trpc_xargs)(xdrs, argp, NULL))
			ICSXDRLOG(ICSXDR_DBG_RCALL, "%s",
				  "icsnsc_rpc_dispatch: bad free args!\n");
		kfree(argp);
	}

	/*
	 * Free the results memory if a free routine has been associated
	 * with this operation.
	 */
	if (resp) {
		if (funcinfoptr->trpc_freefunc)
			(*funcinfoptr->trpc_freefunc)(resp);
		kfree(resp);
	}

	ICSXDRLOG(ICSXDR_DBG_RCALL,
		  "icsnsc_rpc_dispatch: returning %d\n", error);

	return error;
}


/*
 * nsc_rcall() support routines relocated from net/nsc_rpc.
 */
/*
 * nsc_hcreate()
 *	Create a call handle to be used in subsequent nsc_rcall() calls.
 *
 * arguments:
 *	IN  clusternode_t	 node	Node number of target rpc call.
 *	IN  int			 svc	Service number at the target node.
 *	IN  u_short		 vers	Service version number at the target
 *					node.
 *	IN  char		*type	Service transport type icf/udp/tcp.
 *
 * return:
 *	nsc_handle_t	*handle	Pointer to a handle used in nsc_rcall.
 *
 * algorithm:
 *	This routine does not return a valid pointer, it just stuffs the
 *	the data into a long and returns it as a pointer. For connection
 *	mode transport providers such as a tcp, a real handle pointing
 *	to the information describing the connection is returned.
 */

nsc_handle_t *
nsc_hcreate(clusternode_t node, int svc, u_short version, char *nettype)
{
	nsc_handle_t	handle;

	SSI_ASSERT(ICS_CHAN(svc) >= 0 && ICS_CHAN(svc) < ics_num_channels);
	SSI_ASSERT(ICS_SSVC(svc) >= 0 && ICS_SSVC(svc) < ICS_MAX_SUBSERVICES);

	handle.hinfo.ch_node = node;
	handle.hinfo.ch_ics_chan = ICS_CHAN(svc);
	handle.hinfo.ch_ics_ssvc = ICS_SSVC(svc);

	return (nsc_handle_t *)handle.handle;
}

/*
 * nsc_hdestroy()
 *	Destroy the call handle passed in.
 *
 * arguments:
 *	IN nsc_handle_t	*handle	Pointer to the handle to be destoryed.
 *
 * return:
 *	none.
 *
 * algorithm:
 *	This routine does nothing at the moment since the connection type
 *	supported is connectionless, which means a call handle has no state
 *	associated with it. If and when a connection oriented mode transport
 *	provider is supported this routine will close the connection and free
 *	the associated memory.
 */

void
nsc_hdestroy(nsc_handle_t *handle)
{
	return ;
}

bool_t
xdr_msghdrmoremem(XDR *xdrs)
{
	int	 i, allocsize;
	struct msghdr *m = (struct msghdr *)xdrs->x_public;
	struct iovec *iov;

	i = m->msg_iovlen;
	SSI_ASSERT(xdrs->x_ops == &xdrmsghdr_ops);
	SSI_ASSERT(i >= 0);

	/* Make sure there is room. */
	if ((i - ICS_DEF_OOL_SEGMENTS) % ICS_OOL_SEGMENTS_CHUNKS == 0) {
		allocsize = (i + ICS_OOL_SEGMENTS_CHUNKS) * sizeof(*iov);
		iov = kmalloc_nofail(allocsize);
		memcpy(iov, m->msg_iov, i * sizeof(*iov));
		kfree(m->msg_iov);
		m->msg_iov = iov;
	}
	/* Allocate base for next iovec in msghdr */
	if (i >= nsc_chcount)
		i = nsc_chcount - 1;
	allocsize = nscrpc_bufchunk[i];

	iov = &m->msg_iov[m->msg_iovlen];
	iov->iov_base = kzmalloc_nofail(allocsize);
	iov->iov_len = 0;
	m->msg_iovlen++;

	/* Setup the continuation block. */
	xdrs->x_base = (caddr_t)iov;
	xdrs->x_private = (caddr_t)iov->iov_base;
	xdrs->x_handy = allocsize;
	return TRUE;
}


/*
 * This routine is called to free the msghdr struct
 * passed to icscli_llencode_ool_struct_msghdr.
 */
void freemsghdr_callback(u_char *data, long callarg)
{
	struct msghdr *msg = (struct msghdr *)callarg;

	freemsghdr(msg);
}


void freemsghdr(struct msghdr *msg)
{
	int i;

	if (msg == NULL)
		return;
	if (msg->msg_iov != NULL) {
		for (i = 0; i < msg->msg_iovlen; i++) {
			if (msg->msg_iov[i].iov_base != NULL)
				kfree(msg->msg_iov[i].iov_base);
		}
		kfree(msg->msg_iov);
	}
	kfree(msg);
}


void ics_chunk_init(ics_chunk_t *icp, unsigned int entrylen)
{
	unsigned int chunksize = PAGE_ALIGN(entrylen);

	icp->ic_entrylen = entrylen;
	icp->ic_nentries = 0;
	icp->ic_nchunks = 0;
	icp->ic_nleft = 0;
	if (entrylen != 0) {
		icp->ic_chunkorder = get_order(chunksize);
		icp->ic_perchunk = chunksize / entrylen;
	} else {
		icp->ic_chunkorder = 0;
		icp->ic_perchunk = 0;
	}
	icp->ic_chunksalloc = 0;
	icp->ic_freeme = 0;
	icp->ic_chunks = NULL;
}

/*
 * This function can be used to pre-allocate all chunks.  This allows
 * either ics_chunk_add_entry() to be called where it will only allocate
 * if not enough was preallocated.  If ics_chunk_add_entry_noalloc() is called
 * then a NULL is returned if the preallocation runs out.
 */
void ics_chunk_prealloc(ics_chunk_t *icp, unsigned int count)
{
	unsigned int newalloc;
	void **newchunks;
	unsigned int chunk;

	newalloc = ((count - 1)/ icp->ic_perchunk) + 1;
	/* For now we never shrink ic_chunksalloc */
	if (newalloc > icp->ic_chunksalloc) {
		newchunks = kmalloc_nofail(newalloc * sizeof(void *));
		/* Normally this will be NULL, but to be general */
		if (icp->ic_chunks != NULL)
			memcpy(newchunks, icp->ic_chunks,
			       icp->ic_chunksalloc * sizeof(void *));
		memset(newchunks + icp->ic_chunksalloc, 0,
		       (newalloc - icp->ic_chunksalloc) * sizeof(void *));
		kfree(icp->ic_chunks);
		icp->ic_chunks = newchunks;
		icp->ic_chunksalloc = newalloc;
	}
	/* Get all the pages set-up in advance */
	for (chunk = 0 ; chunk < newalloc ; chunk++) {
	    /* Normally in a prealloc case it'll be NULL, but to be general.. */
	    if (icp->ic_chunks[chunk] == NULL) {
		icp->ic_chunks[chunk] =
			(void *)__get_free_pages(GFP_KERNEL|__GFP_NOFAIL,
						 icp->ic_chunkorder);
		BUG_ON(icp->ic_chunks[chunk] == 0);
	    }
	}

	return;
}

static inline void *ics_chunk_alloc(ics_chunk_t *icp, unsigned int chunk)
{
	unsigned int newalloc;
	void **newchunks;

	if (chunk >= icp->ic_chunksalloc) {
		newalloc = icp->ic_chunksalloc + 16;
		newchunks = kmalloc_nofail(newalloc * sizeof(void *));
		if (icp->ic_chunks != NULL)
			memcpy(newchunks, icp->ic_chunks,
			       icp->ic_chunksalloc * sizeof(void *));
		memset(newchunks + icp->ic_chunksalloc, 0,
		       (newalloc - icp->ic_chunksalloc) * sizeof(void *));
		kfree(icp->ic_chunks);
		icp->ic_chunks = newchunks;
		icp->ic_chunksalloc = newalloc;
	}
	if (icp->ic_chunks[chunk] == NULL) {
		icp->ic_chunks[chunk] =
			(void *)__get_free_pages(GFP_KERNEL|__GFP_NOFAIL,
						 icp->ic_chunkorder);
		BUG_ON(icp->ic_chunks[chunk] == 0);
	}

	return icp->ic_chunks[chunk];
}

static inline void *__ics_chunk_add_entry(void *retval, ics_chunk_t *icp)
{
	SSI_ASSERT(retval != NULL);
	retval = (char *)retval + icp->ic_nleft * icp->ic_entrylen;
	icp->ic_nleft++;
	if (icp->ic_nleft == icp->ic_perchunk) {
		icp->ic_nleft = 0;
		icp->ic_nchunks++;
	}
	icp->ic_nentries++;

	return retval;
}

void *ics_chunk_add_entry(ics_chunk_t *icp)
{
	void *retval;

	SSI_ASSERT(icp->ic_nentries ==
	       icp->ic_nchunks * icp->ic_perchunk + icp->ic_nleft);
	SSI_ASSERT(icp->ic_nleft <= icp->ic_perchunk);
	SSI_ASSERT(icp->ic_nchunks <= icp->ic_chunksalloc);

	retval = ics_chunk_alloc(icp, icp->ic_nchunks);

	return __ics_chunk_add_entry(retval, icp);
}

void *ics_chunk_add_entry_noalloc(ics_chunk_t *icp)
{
	void *retval;

	/* Needed another chunk */
	if (icp->ic_nchunks >= icp->ic_chunksalloc)
		return NULL;
	/* Somehow he had the chunk ptr but didn't preallocate the page */
	if (icp->ic_chunks[icp->ic_nchunks] == NULL)
		return NULL;

	SSI_ASSERT(icp->ic_nentries ==
	       icp->ic_nchunks * icp->ic_perchunk + icp->ic_nleft);
	SSI_ASSERT(icp->ic_nleft <= icp->ic_perchunk);
	SSI_ASSERT(icp->ic_nchunks < icp->ic_chunksalloc);

	retval = (void *)(icp->ic_chunks[icp->ic_nchunks]);

	return __ics_chunk_add_entry(retval, icp);
}

void *ics_chunk_get_entry(ics_chunk_t *icp, unsigned int entry)
{
	unsigned int chunk, pos;
	void *retval;

	if (entry >= icp->ic_nentries)
		return NULL;

	chunk = entry / icp->ic_perchunk;
	retval = (void *)(icp->ic_chunks[chunk]);
	SSI_ASSERT(retval != NULL);
	pos = entry - (chunk * icp->ic_perchunk);
	retval = (char *)retval + pos * icp->ic_entrylen;
	return retval;
}

void ics_chunk_free(ics_chunk_t *icp)
{
	unsigned int i;

	for (i = 0; i < icp->ic_chunksalloc; i++) {
		if (icp->ic_chunks[i] != NULL)
			free_pages((unsigned long)icp->ic_chunks[i],
				   icp->ic_chunkorder);
		else
			break;
	}
	kfree(icp->ic_chunks);
	if (icp->ic_freeme) {
		kfree(icp);
	} else {
		icp->ic_nentries = 0;
		icp->ic_nchunks = 0;
		icp->ic_nleft = 0;
		icp->ic_chunksalloc = 0;
		icp->ic_chunks = NULL;
	}
}

void ics_chunk_free_extra(ics_chunk_t *icp)
{
	unsigned int i;

	i = icp->ic_nchunks;
	if (icp->ic_nleft != 0)
		i++;
	if (i == 0) {
		ics_chunk_free(icp);
		return;
	}
	for (; i < icp->ic_chunksalloc; i++) {
		if (icp->ic_chunks[i] != NULL) {
			free_pages((unsigned long)icp->ic_chunks[i],
				   icp->ic_chunkorder);
			icp->ic_chunks[i] = NULL;
		}
	}
}

void ics_chunk_clear(ics_chunk_t *icp)
{
	icp->ic_nentries = 0;
	icp->ic_nchunks = 0;
	icp->ic_nleft = 0;
}

void ics_chunk_free_data_callback(u_char *data, long order)
{
	free_pages((unsigned long)data, order);
}

int cli_encode_ool_ics_chunk_t_p(cli_handle_t *hp, ics_chunk_t *icp)
{
	int error;
	unsigned int i;
	unsigned int clen;

	cli_encode_gen_u_int(hp, icp->ic_entrylen);
	cli_encode_gen_u_int(hp, icp->ic_nentries);
	cli_encode_gen_u_int(hp, icp->ic_nchunks);
	cli_encode_gen_u_int(hp, icp->ic_nleft);
	cli_encode_gen_u_int(hp, icp->ic_chunkorder);
	cli_encode_gen_u_int(hp, icp->ic_perchunk);

	clen = icp->ic_perchunk * icp->ic_entrylen;
	for (i = 0; i < icp->ic_nchunks; i++) {
		error = icscli_encode_ool_data_t(hp, icp->ic_chunks[i],
						 clen, NULL, 0);
		SSI_ASSERT(!error);
	}
	if (icp->ic_nleft > 0) {
		clen = icp->ic_nleft * icp->ic_entrylen;
		error = icscli_encode_ool_data_t(hp, icp->ic_chunks[i],
						 clen, NULL, 0);
		SSI_ASSERT(!error);
	}

	return 0;
}

int cli_encoderesp_ool_ics_chunk_t_p(cli_handle_t *hp, ics_chunk_t *icp)
{
	/*
	 * XXX: An empty encoderesp may not work on anything other than TCP/IP
	 */
	return 0;
}


int svr_encode_ool_ics_chunk_t_p(svr_handle_t *hp, ics_chunk_t *icp)
{
	int error = 0;
	unsigned int i;
	unsigned int clen;

	svr_encode_gen_u_int_p(hp, &icp->ic_entrylen);
	svr_encode_gen_u_int_p(hp, &icp->ic_nentries);
	svr_encode_gen_u_int_p(hp, &icp->ic_nchunks);
	svr_encode_gen_u_int_p(hp, &icp->ic_nleft);
	svr_encode_gen_u_int_p(hp, &icp->ic_chunkorder);
	svr_encode_gen_u_int_p(hp, &icp->ic_perchunk);

	clen = icp->ic_perchunk * icp->ic_entrylen;
	for (i = 0; i < icp->ic_nchunks; i++) {
		error = icssvr_encode_ool_data_t(hp, icp->ic_chunks[i], clen,
						 ics_chunk_free_data_callback,
						 icp->ic_chunkorder);
		SSI_ASSERT(!error);
	}
	if (icp->ic_nleft > 0) {
		clen = icp->ic_nleft * icp->ic_entrylen;
		error = icssvr_encode_ool_data_t(hp, icp->ic_chunks[i++], clen,
						 ics_chunk_free_data_callback,
						 icp->ic_chunkorder);
		SSI_ASSERT(!error);
	}
	for (; i < icp->ic_chunksalloc; i++) {
		if (icp->ic_chunks[i] != NULL)
			free_pages((unsigned long)icp->ic_chunks[i],
				   icp->ic_chunkorder);
	}
	kfree(icp->ic_chunks);
	if (icp->ic_freeme)
		kfree(icp);
	else
		icp->ic_chunks = NULL;

	return 0;
}

int cli_decode_ool_ics_chunk_t_p(cli_handle_t *hp, ics_chunk_t *icp)
{
	int error;
	unsigned int i;
	unsigned int clen;
	unsigned int el;
	unsigned int ne;
	unsigned int nc;
	unsigned int nl;
	unsigned int co;
	unsigned int pc;
	void *cp = NULL;

	cli_decode_gen_u_int_p(hp, &el);
	cli_decode_gen_u_int_p(hp, &ne);
	cli_decode_gen_u_int_p(hp, &nc);
	cli_decode_gen_u_int_p(hp, &nl);
	cli_decode_gen_u_int_p(hp, &co);
	cli_decode_gen_u_int_p(hp, &pc);
	if (icp->ic_entrylen == 0)
		ics_chunk_init(icp, el);
	SSI_ASSERT(icp->ic_entrylen == el);
	SSI_ASSERT(icp->ic_chunkorder == co);
	SSI_ASSERT(icp->ic_perchunk == pc);
	if (icp->ic_entrylen != el || icp->ic_chunkorder != co
	    || icp->ic_perchunk != pc)
		ics_chunk_free(icp);
	icp->ic_entrylen = el;
	icp->ic_nentries = ne;
	icp->ic_nchunks = nc;
	icp->ic_nleft = nl;
	icp->ic_chunkorder = co;
	icp->ic_perchunk = pc;

#ifdef ICS_CHUNKS_LIMIT
	if (!ne) return 0;
	ics_chunk_prealloc(icp, ne); 
#endif
	clen = icp->ic_perchunk * icp->ic_entrylen;
	for (i = 0; i < icp->ic_nchunks; i++) {
#ifdef ICS_CHUNKS_LIMIT
		cp = icp->ic_chunks[i];
#else
		cp = ics_chunk_alloc(icp, i);
#endif
		SSI_ASSERT(cp != NULL);
		error = icscli_decode_ool_data_t(hp, cp, clen);
		SSI_ASSERT(!error);
	}
	if (icp->ic_nleft > 0) {
#ifdef ICS_CHUNKS_LIMIT
		cp = icp->ic_chunks[i];
#else
		cp = ics_chunk_alloc(icp, i);
#endif
		SSI_ASSERT(cp != NULL);
		clen = icp->ic_nleft * icp->ic_entrylen;
		error = icscli_decode_ool_data_t(hp, icp->ic_chunks[i], clen);
		SSI_ASSERT(!error);
	}

	return 0;
}

int svr_decode_ool_ics_chunk_t_p_p(svr_handle_t *hp, ics_chunk_t **icpp)
{
	int error;
	unsigned int i;
	unsigned int clen;
	unsigned int el;
	unsigned int ne;
	unsigned int nc;
	unsigned int nl;
	unsigned int co;
	unsigned int pc;
	void *cp;
	ics_chunk_t *icp;

	SSI_ASSERT(*icpp == NULL);
	*icpp = icp = kmalloc_nofail(sizeof(*icp));
	svr_decode_gen_u_int(hp, el);
	svr_decode_gen_u_int(hp, ne);
	svr_decode_gen_u_int(hp, nc);
	svr_decode_gen_u_int(hp, nl);
	svr_decode_gen_u_int(hp, co);
	svr_decode_gen_u_int(hp, pc);
	ics_chunk_init(icp, el);
	SSI_ASSERT(icp->ic_entrylen == el);
	SSI_ASSERT(icp->ic_chunkorder == co);
	SSI_ASSERT(icp->ic_perchunk == pc);
	icp->ic_entrylen = el;
	icp->ic_nentries = ne;
	icp->ic_nchunks = nc;
	icp->ic_nleft = nl;
	icp->ic_chunkorder = co;
	icp->ic_perchunk = pc;
	icp->ic_freeme = 1;

#ifdef ICS_CHUNKS_LIMIT
	if (!ne) return 0;
	ics_chunk_prealloc(icp, ne);
#endif
	clen = icp->ic_perchunk * icp->ic_entrylen;
	for (i = 0; i < icp->ic_nchunks; i++) {
#ifdef ICS_CHUNKS_LIMIT
		cp = icp->ic_chunks[i];
#else
		cp = ics_chunk_alloc(icp, i);
#endif
		SSI_ASSERT(cp != NULL);
		error = icssvr_decode_ool_data_t(hp, cp, clen);
		SSI_ASSERT(!error);
	}
	if (icp->ic_nleft > 0) {
		clen = icp->ic_nleft * icp->ic_entrylen;
#ifdef ICS_CHUNKS_LIMIT
		cp = icp->ic_chunks[i];
#else
		cp = ics_chunk_alloc(icp, i);
#endif
		SSI_ASSERT(cp != NULL);
		error = icssvr_decode_ool_data_t(hp, cp, clen);
		SSI_ASSERT(!error);
	}

	return 0;
}

void __deprecated svr_free_ics_chunk_t_p(svr_handle_t *hp, ics_chunk_t *icp)
{
	ics_chunk_free(icp);
	/* kfree(icp); */ /* Done in ics_chunk_free() */
}

void
ics_userbuf_set(ics_userbuf_t *up, const void *addr, int len)
{
	up->iu_addr = (void *)addr;
	up->iu_len = len;
	up->iu_user = !segment_eq(get_fs(), KERNEL_DS);

#ifdef DEBUG_TOOLS
	if (!up->iu_user) {
		if (up->iu_len > ICS_MAX_OOL_DATA_SIZE)
			panic("%s:kernel buffer len %d exceeds maximum\n",
			      __FUNCTION__, up->iu_len);
#ifndef CONFIG_X86_4G
		if ((unsigned long)addr <= TASK_SIZE && addr != NULL)
			panic("%s:user address %p not allowed with KERNEL_DS\n",
			      __FUNCTION__, addr);
#endif
	}
#ifndef CONFIG_X86_4G
	else {
		if ((unsigned long)addr >= TASK_SIZE)
			printk(KERN_WARNING "%s:kernel address %p not allowed"
			       " with USER_DS\n", __FUNCTION__, addr);
	}
#endif
#endif
}

int cli_encode_ool_ics_userbuf_t_p(cli_handle_t *hp, ics_userbuf_t *up)
{
	int error;

	cli_encode_gen_ics_userbuf_t_p(hp, up);
	if (!up->iu_user && up->iu_addr) {
		error = icscli_encode_ool_data_t(hp, up->iu_addr, up->iu_len,
						 NULL, 0);
		SSI_ASSERT(!error);
	}

	return 0;
}

int cli_encoderesp_ool_ics_userbuf_t_p(cli_handle_t *hp, ics_userbuf_t *up)
{
	if (!up->iu_user && up->iu_addr)
		return icscli_encoderesp_ool_data_t(hp, up->iu_addr,
						    up->iu_len);
	else
		return 0;
}

int cli_decode_ool_ics_userbuf_t_p(cli_handle_t *hp, ics_userbuf_t *up)
{
	int error;

	cli_decode_gen_int(hp, up->iu_len);
	if (!up->iu_user) {
		error = icscli_decode_ool_data_t(hp, up->iu_addr, up->iu_len);
		SSI_ASSERT(!error);
	}

	return 0;
}

int svr_decode_ool_ics_userbuf_t_p(svr_handle_t *hp, ics_userbuf_t *up)
{
	int error;

	svr_decode_gen_ics_userbuf_t_p(hp, up);
	if (!up->iu_user && up->iu_addr) {
		up->iu_addr = kmalloc_nofail(up->iu_len);
		error = icssvr_decode_ool_data_t(hp, up->iu_addr, up->iu_len);
		SSI_ASSERT(!error);
	}

	return 0;
}

int svr_decode_ics_userbuf_t_p(svr_handle_t *hp, ics_userbuf_t *up)
{
	svr_decode_gen_ics_userbuf_t_p(hp, up);
	if (!up->iu_user && up->iu_addr)
		up->iu_addr = kmalloc_nofail(up->iu_len);

	return 0;
}

int svr_encode_ool_ics_userbuf_t_p(svr_handle_t *hp, ics_userbuf_t *up)
{
	int error;

	svr_encode_gen_int(hp, up->iu_len);
	if (!up->iu_user) {
		error = icssvr_encode_ool_data_t(hp, up->iu_addr, up->iu_len,
						 free_data_callback,
						 up->iu_len);
		SSI_ASSERT(!error);
		up->iu_addr = NULL;
	}

	return 0;
}

void svr_free_ool_ics_userbuf_t_p(svr_handle_t *hp, ics_userbuf_t *up)
{
	if (!up->iu_user && up->iu_addr)
		kfree(up->iu_addr);
}
