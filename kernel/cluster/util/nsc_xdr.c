/*
 * 	XDR routines.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <linux/time.h>
#include <linux/types.h>
#include <cluster/clms.h>
#include <cluster/nodelist.h>
#include <cluster/rpc/rpc.h>

#define LASTUNSIGNED	((u_int)0-1)

/*
 * XDR an indirect pointer
 * xdr_reference is for recursively translating a structure that is
 * referenced by a pointer inside the structure that is currently being
 * translated.  pp references a pointer to storage. If *pp is null
 * the  necessary storage is allocated.
 * size is the sizeof the referneced structure.
 * proc is the routine to handle the referenced structure.
 */
bool_t
xdr_reference(XDR *xdrs, caddr_t *pp, u_int size, void *proc)
{
	register caddr_t loc = *pp;
	register bool_t stat;

	if (loc == NULL)
		switch (xdrs->x_op) {
		case XDR_FREE:
			return (TRUE);

		case XDR_DECODE:
			*pp = loc = kzmalloc_nofail(size);
			break;
		case XDR_ENCODE:
			break;
	}

	stat = ((xdrproc_t)proc)(xdrs, loc, LASTUNSIGNED);

	if (xdrs->x_op == XDR_FREE) {
		kfree(loc);
		*pp = NULL;
	}
	return (stat);
}

/*
 * xdr_pointer():
 *
 * XDR a pointer to a possibly recursive data structure. This
 * differs with xdr_reference in that it can serialize/deserialiaze
 * trees correctly.
 *
 *  What's sent is actually a union:
 *
 *  union object_pointer switch (boolean b) {
 *  case TRUE: object_data data;
 *  case FALSE: void nothing;
 *  }
 *
 * > objpp: Pointer to the pointer to the object.
 * > obj_size: size of the object.
 * > xdr_obj: routine to XDR an object.
 *
 */
bool_t
xdr_pointer(XDR *xdrs, char **objpp, u_int obj_size, void *xdr_obj)
{

	bool_t more_data;

	more_data = (*objpp != NULL);
	if (! xdr_bool(xdrs,&more_data)) {
		return (FALSE);
	}
	if (! more_data) {
		*objpp = NULL;
		return (TRUE);
	}
	return (xdr_reference(xdrs,objpp,obj_size,xdr_obj));
}

/*
 * xdr_array()
 *
 */
bool_t
xdr_array(XDR *xdrs, caddr_t *basep, u_int *sizep, u_int maxsize,
	  u_int elemsize, void *xdr_obj)
{
	caddr_t	target = *basep;
	bool_t	stat = TRUE;
	u_int	nodesize;
	u_int	i;
	u_int	c;

	/*
	 * like strings, arrays are really counted arrays
	 */
	if (! xdr_arch_uint(xdrs, sizep)) {

		printk ("xdr_array: size FAILED\n");

		return (FALSE);
	}
	c = *sizep;
	if ((c > maxsize) && (xdrs->x_op != XDR_FREE)) {

		printk( "xdr_array: bad size FAILED\n");

		return (FALSE);
	}

	nodesize = c * elemsize;

	/*
	 * if we are deserializing, we may need to allocate an array.
	 * We also save time by checking for a null array if we are freeing.
	 */
	if (target == NULL)
		switch (xdrs->x_op) {
		case XDR_DECODE:
			if (c == 0)
				return (TRUE);
			*basep = target = kzmalloc_nofail(nodesize);
			break;

		case XDR_FREE:
			return (TRUE);
		case XDR_ENCODE:
			break;
	}

	/*
	 * now we xdr each element of array
	 */
	for (i = 0; (i < c) && stat; i++) {
		stat = ((xdrproc_t)xdr_obj)(xdrs, target, LASTUNSIGNED);
		target += elemsize;
	}

	/*
	 * the array may need freeing
	 */
	if (xdrs->x_op == XDR_FREE) {
		kfree(*basep);
		*basep = NULL;
	}

	return (stat);
}

/*
 * xdr_vector():
 *
 * XDR a fixed length array. Unlike variable-length arrays,
 * the storage of fixed length arrays is static and unfreeable.
 * > basep: base of the array
 * > size: size of the array
 * > elemsize: size of each element
 * > xdr_elem: routine to XDR each element
 */
bool_t
xdr_vector(XDR *xdrs, char *basep, u_int nelem, u_int elemsize,
	   void * xdr_elem)
{
	register u_int i;
	register char *elptr;

	elptr = basep;
	for (i = 0; i < nelem; i++) {
		if (! ((xdrproc_t)xdr_elem)(xdrs, elptr, LASTUNSIGNED)) {
			return(FALSE);
		}
		elptr += elemsize;
	}
	return(TRUE);
}

bool_t
xdr_qstr (XDR *xdrs, struct qstr *objp)
{
	 if (!xdr_string (xdrs, (char **)&objp->name, PATH_MAX + 1))
		 return FALSE;
	 if (!xdr_arch_uint (xdrs, &objp->len))
		 return FALSE;
	 if (!xdr_arch_uint (xdrs, &objp->hash))
		 return FALSE;
	return TRUE;
}
