/*
 * 	Cluster logging support code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/slab.h>
#include <linux/param.h>
#include <cluster/assert.h>
#include <cluster/log.h>
#include <cluster/synch.h>

/*
 * This routine initializes the circular log.  'Depth' is the log depth, the
 * number of entries in the log, and 'slpflg' is a flag to be passed to
 * kmalloc() to indicate whether it should sleep.  It returns a pointer
 * to a 'nsc_logcookie_t' (defined in nsc_log.h), which is required for the
 * other routines; this may be NULL if kmalloc() didn't allocate anything.
 */
nsc_logcookie_t *
nsc_log_init(int depth, int slpflg)
{
	nsc_logcookie_t *cookie;
	int pages = (depth + NSC_LOGENT_PER_PAGE - 1) / NSC_LOGENT_PER_PAGE;
	int i;

	if (depth >= NSC_LOGENT_PER_PAGE)
		depth = pages * NSC_LOGENT_PER_PAGE;
	cookie = kmalloc(sizeof(*cookie) + (pages * sizeof(cookie->pages)),
			 slpflg);
	if (cookie) {
		cookie->magic = NSC_LOGCOOKIE_MAGIC;
		cookie->pages = (void *)((char *)cookie + sizeof(*cookie));
		cookie->wrap = 0;
		cookie->head = -1;
		cookie->depth = depth;
		spin_lock_init(&cookie->spinlock);
		if (depth >= NSC_LOGENT_PER_PAGE) {
			for (i = 0; i < pages; i++) {
				cookie->pages[i] =
					(void *)__get_free_page(slpflg);
				if (!cookie->pages[i])
					break;
			}
		} else {
			cookie->pages[0] = kmalloc(depth *
						   sizeof(nsc_logent_t),
						   slpflg);
			if (cookie->pages[0])
				i = 1;
			else
				i = 0;
		}
		if (i < pages) {
			for (; i < pages; i++)
				cookie->pages[i] = NULL;
			nsc_log_deinit(cookie);
			cookie = NULL;
		}
	}

	return cookie;
}

nsc_logcookie_t *
nsc_logsmp_init(int depth, int slpflg)
{
	nsc_logcookie_t *cookie;
	int i;

	cookie = kmalloc(sizeof(*cookie) +
			 (num_possible_cpus() * sizeof(*(cookie->cookies))),
			 slpflg);

	if (cookie) {
		cookie->magic = NSC_LOGSMPCOOKIE_MAGIC;
		cookie->cookies = (void *)((char *)cookie + sizeof(*cookie));
		for (i = 0; i < num_possible_cpus(); i++) {
			cookie->cookies[i] = nsc_log_init(depth, slpflg);
			if (!cookie->cookies[i])
				break;
		}
		if (i < num_possible_cpus()) {
			for (; i < num_possible_cpus(); i++)
				cookie->cookies[i] = NULL;
			nsc_log_deinit(cookie);
			cookie = NULL;
		}
	}

	return cookie;
}

/*
 * This routine adds an entry to the circular log.  'cookie' is as returned
 * by nsc_log_init(), msgnum is the message number of the log entry (used
 * in nsc_printlog(), below), and 'a' through 'g' are arbitrary integers.
 * Returns -1 on error, zero otherwise.
 */

static int
nsc_logsmp(nsc_logcookie_t *smp_cookie, long msgnum,
	    long a, long b, long c, long d, long e, long f, long g)
{
	int cpu = smp_processor_id();
	nsc_logcookie_t *cookie;
	int myidx;
	nsc_logent_t *ep;
	unsigned long flags;

	cookie = smp_cookie->cookies[cpu];
	if (!cookie)
		return 0;
	if (cookie->magic != NSC_LOGCOOKIE_MAGIC)
		return 0;
	local_irq_save(flags);
	if ((myidx = ++cookie->head) >= cookie->depth) {
		myidx = cookie->head = 0;
		cookie->wrap = 1;
	}
	local_irq_restore(flags);
	ep = cookie->pages[myidx / NSC_LOGENT_PER_PAGE];
	ep += (myidx % NSC_LOGENT_PER_PAGE);
	ep->msgnum = msgnum;
	ep->a = a;
	ep->b = b;
	ep->c = c;
	ep->d = d;
	ep->e = e;
	ep->f = f;
	ep->g = g;

	return myidx;
}

int
nsc_log(nsc_logcookie_t *cookie, long msgnum,
	long a, long b, long c, long d, long e, long f, long g)
{
	int myidx;
	nsc_logent_t *ep;
	unsigned long flags;

	if (!cookie)
		return 0;
	if (cookie->magic == NSC_LOGSMPCOOKIE_MAGIC)
		return nsc_logsmp(cookie, msgnum, a, b, c, d, e, f, g);
	if (cookie->magic != NSC_LOGCOOKIE_MAGIC)
		return 0;
	spin_lock_irqsave(&cookie->spinlock, flags);
	if ((myidx = ++cookie->head) >= cookie->depth) {
		myidx = cookie->head = 0;
		cookie->wrap = 1;
	}
	spin_unlock_irqrestore(&cookie->spinlock, flags);
	ep = cookie->pages[myidx / NSC_LOGENT_PER_PAGE];
	ep += (myidx % NSC_LOGENT_PER_PAGE);
	ep->msgnum = msgnum;
	ep->a = a;
	ep->b = b;
	ep->c = c;
	ep->d = d;
	ep->e = e;
	ep->f = f;
	ep->g = g;

	return myidx;
}

/* Search the log */

static void
nsc_logsmp_search_va(nsc_logcookie_t *smp_cookie, const char *messages[],
		     long find, va_list ap)
{
	int i;

	for (i = 0; i < num_possible_cpus(); i++) {
		printk("cpu %2d:\n", i);
		nsc_log_search_va(smp_cookie->cookies[i], messages,
				  find, ap);
	}
}

int nsc_log_search_end;

static int
nsc_logent_search(nsc_logent_t *ep, long find, va_list ap)
{
	int ret;
	va_list cap;

	if (ep->a == find ||
	    ep->b == find ||
	    ep->c == find ||
	    ep->d == find ||
	    ep->e == find ||
	    ep->f == find ||
	    ep->g == find)
		return 1;
	ret = 0;
	va_copy(cap, ap);
	while ((find = va_arg(cap, long)) != (long)&nsc_log_search_end) {
		if (ep->a == find ||
		    ep->b == find ||
		    ep->c == find ||
		    ep->d == find ||
		    ep->e == find ||
		    ep->f == find ||
		    ep->g == find) {
			ret = 1;
			break;
		}
	}
	va_end(cap);

	return ret;
}

void
nsc_log_search_va(nsc_logcookie_t *cookie, const char *messages[],
		  long find, va_list ap)
{
	int i;
	nsc_logent_t *ep;

	if (!cookie)
		return;
	if (cookie->magic == NSC_LOGSMPCOOKIE_MAGIC) {
		nsc_logsmp_search_va(cookie, messages, find, ap);
		return;
	}
	if (cookie->magic != NSC_LOGCOOKIE_MAGIC)
		return;
	/* Do nothing if list is empty */
	if (cookie->head == -1)
		return;
	for (i = cookie->head; i >= 0; i--) {
		ep = cookie->pages[i / NSC_LOGENT_PER_PAGE];
		ep += (i % NSC_LOGENT_PER_PAGE);
		if (nsc_logent_search(ep, find, ap))
			printk(messages[ep->msgnum],
			       ep->a, ep->b,
			       ep->c, ep->d,
			       ep->e, ep->f,
			       ep->g);
	}
	if (!cookie->wrap)
		return;
	for (i = cookie->depth - 1; i > cookie->head; i--) {
		ep = cookie->pages[i / NSC_LOGENT_PER_PAGE];
		ep += (i % NSC_LOGENT_PER_PAGE);
		if (nsc_logent_search(ep, find, ap))
			printk(messages[ep->msgnum],
			       ep->a, ep->b,
			       ep->c, ep->d,
			       ep->e, ep->f,
			       ep->g);
	}
}

static void
nsc_log_search_va_glue(nsc_logcookie_t *cookie, const char *messages[],
		       long find, ...)
{
	va_list ap;

	va_start(ap, find);
	nsc_log_search_va(cookie, messages, find, ap);
	va_end(ap);
}

void
nsc_log_search(nsc_logcookie_t *cookie, const char *messages[], long find)
{
	nsc_log_search_va_glue(cookie, messages, find, &nsc_log_search_end);
}

/*
 * This routine frees the cookie and circular buffer.
 */

static void
nsc_logsmp_deinit(nsc_logcookie_t *smp_cookie)
{
	int i;

	for (i = 0; i < num_possible_cpus(); i++)
		nsc_log_deinit(smp_cookie->cookies[i]);

	kfree(smp_cookie);
}

void
nsc_log_deinit(nsc_logcookie_t *cookie)
{
	int pages;
	int i;

	if (!cookie)
		return;
	if (cookie->magic == NSC_LOGSMPCOOKIE_MAGIC) {
		nsc_logsmp_deinit(cookie);
		return;
	}
	if (cookie->magic != NSC_LOGCOOKIE_MAGIC)
		return;

	pages = (cookie->depth + NSC_LOGENT_PER_PAGE - 1) /
		NSC_LOGENT_PER_PAGE;
	if (cookie->depth >= NSC_LOGENT_PER_PAGE) {
		for (i = 0; i < pages; i++)
			free_page((ulong)cookie->pages[i]);
	} else
		kfree(cookie->pages[0]);

	kfree(cookie);
}

/*
 * This routine prints the messages in the log, most recent to least recent
 * (that is, in reverse chronological order).  'cookie' is as returned by
 * nsc_log_init(), and 'messages' is an array of char pointers which point
 * at format strings used to print the values; this array is indexed by
 * 'msgnum', and must have a valid entry for each message logged.
 */

static void
nsc_logsmp_print_n(nsc_logcookie_t *cookie, const char *messages[], int cnt)
{
	int i;

	for (i = 0; i < num_possible_cpus(); i++) {
		printk("cpu %2d:\n", i);
		nsc_log_print_n(cookie->cookies[i], messages, cnt);
	}
}

void
nsc_log_print_n(nsc_logcookie_t *cookie, const char *messages[], int cnt)
{
	int i;
	nsc_logent_t *ep;
	const char *fmtp;

	if (!cookie)
		return;
	if (cookie->magic == NSC_LOGSMPCOOKIE_MAGIC) {
		nsc_logsmp_print_n(cookie, messages, cnt);
		return;
	}
	if (cookie->magic != NSC_LOGCOOKIE_MAGIC)
		return;
	/* Do nothing if list is empty */
	if (cookie->head == -1)
		return;
	for (i = cookie->head; cnt > 0; i--, cnt--) {
		ep = cookie->pages[i / NSC_LOGENT_PER_PAGE];
		ep += (i % NSC_LOGENT_PER_PAGE);
		if (messages && messages[ep->msgnum]) {
			fmtp = messages[ep->msgnum];
			printk(fmtp, ep->a, ep->b, ep->c, ep->d,
			       ep->e, ep->f, ep->g);
		} else {
			fmtp = "0x%lx 0x%lx 0x%lx 0x%lx 0x%lx "
			       "0x%lx 0x%lx 0x%lx 0x%lx\n";
			printk(fmtp, ep->msgnum,
			       ep->a, ep->b, ep->c, ep->d,
			       ep->e, ep->f, ep->g);
		}
		if (i == 0) {
			if (cookie->wrap)
				i = cookie->depth;
			else
				return;
		}
	}
}

/*
 * Print entire log.
 */

static void
nsc_logsmp_print(nsc_logcookie_t *cookie, const char *messages[])
{
	int i;

	for (i = 0; i < num_possible_cpus(); i++) {
		printk("cpu %2d:\n", i);
		nsc_log_print(cookie->cookies[i], messages);
	}
}

void
nsc_log_print(nsc_logcookie_t *cookie, const char *messages[])
{
	if (!cookie)
		return;
	if (cookie->magic == NSC_LOGSMPCOOKIE_MAGIC) {
		nsc_logsmp_print(cookie, messages);
		return;
	}
	nsc_log_print_n(cookie, messages, cookie->depth);
}
