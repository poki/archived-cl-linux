/*
 * 	Cluster nodelist code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/param.h>
#include <linux/errno.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/assert.h>
#include <cluster/nodelist.h>


/*
 * nsc_nodelist_alloc()
 *	Allocate a list of nodes.
 *
 * Description
 *	This function allocates and returns a pointer to structure
 * 	nsc_nodelist. This structure is used to represent a list of
 *	nodes as an array of bits. The input parameters is the number
 *	of nodes.  Note that the node numbers have to be less than the
 *	number of nodes.
 *
 *	The list returned will already be initialized for immediate use.
 *
 * Parameters:
 *	None
 *
 * Returns
 *	If successful A pointer to nsc_nodelist_t is returned. Otherwise
 *	it asserts.
 *
 */
nsc_nodelist_t *
nsc_nodelist_alloc()
{
	nsc_nodelist_t *tnlist;

	tnlist = (nsc_nodelist_t *)
		  kmalloc_nofail(sizeof(nsc_nodelist_t));

	SSI_ASSERT(tnlist != NULL);

	NSC_NODELIST_INIT(tnlist);

	return tnlist;
}

/*
 * nsc_nodelist_free()
 *	Frees a nsc_nodelist_t.
 *
 * Description
 *	This function frees a nsc_nodelist_t allocated using
 *	nsc_nodelist_alloc().
 *
 * Parameters:
 *	nsc_nodelist_t *	- pointer to structure to be freed.
 *
 * Returns
 * 	None.
 *
 */
void
nsc_nodelist_free(nsc_nodelist_t *tnlist)
{

	if (!tnlist)
		return;

	NSC_NODELIST_DEINIT(tnlist);

	kfree((caddr_t)tnlist);

	return;
}

/*
 * nsc_nodelist_count()
 *	Count the number of nodes in the nodelist.
 *
 * Description
 *	This function steps through the node bitmask and counts the
 *	number of node bits set.  Nothing is done to the structure
 *	itself.
 *
 * Parameters:
 *	nsc_nodelist_t *	- pointer to structure
 *
 * Returns
 * 	Count of nodes.
 */
int
nsc_nodelist_count(nsc_nodelist_t *tnlist)
{
	nsc_nlcookie_t cookie = CLUSTERNODE_INVAL;
	int num_nodes = 0;

	SSI_ASSERT(tnlist);

	while (NSC_NODELIST_GET_NEXT(&cookie, tnlist) != CLUSTERNODE_INVAL)
		num_nodes++;

	return num_nodes;
}

/*
 * nsc_nodelist_copy()
 *	Allocate a nsc_nodelist as a copy of an existing one.
 *
 * Description
 *	This function allocates and returns a pointer to structure
 * 	nsc_nodelist. This structure is filled from an existing
 *	nsc_nodelist.
 *
 * Parameters:
 *	None
 *
 * Returns
 *	If successful A pointer to nsc_nodelist_t is returned. Otherwise
 *	it asserts.
 *
 */
nsc_nodelist_t *
nsc_nodelist_copy(nsc_nodelist_t *otnlist)
{
	nsc_nodelist_t *tnlist;

	SSI_ASSERT(otnlist);

	tnlist = NSC_NODELIST_ALLOC();
	NSC_NODELIST_SETN(tnlist, otnlist);

	return(tnlist);
}

/*
 * nsc_nodelist_get_next()
 *	Get the next node in the list.
 *
 * Description:
 *	This function returns the next node in the list. If no more nodes
 *	are set, it returns CLUSTERNODE_INVAL. It also sets a magic cookie.
 *	The cookie has to be passed on every call. To start at the first
 *	node in the list, the value of cookie has to be CLUSTERNODE_INVAL.
 *	Note that the cookie is a POINTER to an unsigned long.
 * Parameters:
 *	unsigned long *cookie	- pointer to an unsigned long used
 *				  in getting the next node.
 *				  (*cookie = CLUSTERNODE_INVAL) gets the
				  first node.
 *	nsc_nodelist_t	*tnlist	- pointer to the nsc_nodelist_t
 *
 * Returns
 *	The next node number in the list. CLUSTERNODE_INVAL if no more nodes.
 */
clusternode_t
nsc_nodelist_get_next(nsc_nlcookie_t *cookie, nsc_nodelist_t *tnlist)
{
	int i;
	int starting_node;

	SSI_ASSERT(cookie);

	if (!tnlist || ((*cookie != CLUSTERNODE_INVAL) &&
			!NSC_NODELIST_CHKNODE(*cookie)))
		return CLUSTERNODE_INVAL;

	if (*cookie == CLUSTERNODE_INVAL)
		starting_node = 1;
	else
		starting_node = *cookie;

	for (i = starting_node; i <= NSC_NODELIST_MAXNODE; i++) {
		if (NSC_NODELIST_TEST1(tnlist, i)) {
			*cookie = i+1;
			return ((clusternode_t)i);
		}
	}
	*cookie = i+1;

	return CLUSTERNODE_INVAL;
}

/*
 * nsc_nodelist_bit2node()
 *	Convert a bitmask index into a node number.
 *
 * Description
 *	The bit number for the actual macros manipulating the bitmasks
 *	starts at 0.  For us, the node numbers start at 1. Hence this
 *	routine.
 */
clusternode_t
nsc_nodelist_bit2node(int bit)
{
	if (bit != -1)
		bit += NSC_NODELIST_MINNODE;
	return (clusternode_t)bit;
}

#if	defined(DEBUG) || defined(DEBUG_TOOLS)
void
print_nsc_nodelist(nsc_nodelist_t *tnlist)
{
	nsc_nlcookie_t 	cookie;
	clusternode_t	node;

	NSC_NLCOOKIE_INIT(&cookie);
	while ((node = NSC_NODELIST_GET_NEXT(&cookie,tnlist))
							!= CLUSTERNODE_INVAL)
			printk(KERN_DEBUG " %d", (int) node);
	printk(KERN_DEBUG "\n");

	return;
}
#endif /* DEBUG || DEBUG_TOOLS */
