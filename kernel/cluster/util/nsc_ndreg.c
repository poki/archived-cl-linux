#ident "@(#)$Header: /cvsroot/ci-linux/ci/kernel/cluster/util/nsc_ndreg.c,v 1.1 2003/06/03 22:22:29 bjbrew Exp $"
/*
 * 	Generic nodedown registration code.
 *	Copyright 2001 Compaq Computer Corporation
 *	Changes Copyright 2008 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This module implements interfaces for registering and deregistering
 * nodedown callback routines.
 */

#include <linux/types.h>
#include <linux/errno.h>
#include <linux/sched.h>

#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>
#include <cluster/async.h>
#include <cluster/clms.h>
#include <cluster/ndreg.h>

/* #include <util/sysmacros.h> */

#define NSC_ND_ID_HASHSZ	16
#define NSC_ND_HASH(_id)						\
	((_id) % NSC_ND_ID_HASHSZ)

#define NSC_ND_MAKE_ID(_node, _ups, _seqno)				\
	((((_node) & 0xff) << 24)| (((_ups) & 0xff) << 16) |		\
	 ((_seqno) & 0xffff))
#define NSC_ND_GET_ID_NODE(_id)						\
	((_id) >> 24)

#define NSC_ND_IDLINK2ENTRY(_link)					\
	((nsc_nd_entry_t *) (((char *)_link)				\
			 - offsetof(nsc_nd_entry_t, nde_id_link)))
#define NSC_ND_NODELINK2ENTRY(_link)					\
	((nsc_nd_entry_t *) (((char *)_link)				\
			 - offsetof(nsc_nd_entry_t, nde_node_link)))

typedef struct nsc_nd_link {
	struct nsc_nd_link		*ndl_prev;
	struct nsc_nd_link		*ndl_next;
} nsc_nd_link_t;

typedef struct nsc_nd_entry {
	int				nde_id;
	struct nsc_nd_link		nde_id_link;
	struct nsc_nd_link		nde_node_link;
	clusternode_t			nde_node;
	int				nde_flags;
	void				(*nde_func)(clusternode_t, void *);
	int				nde_argsize;
	char				nde_args[1];
} nsc_nd_entry_t;

typedef struct {
	clusternode_t			nda_node;
	struct nsc_nd_entry		*nda_ep;
	struct nsc_nd_link		*nda_list;
} nsc_nd_args_t;

static void nsc_nd_nodedown_cleanup(clusternode_t, nsc_nd_link_t *);
static void nsc_nd_nodedown_daemon(void *);
static nsc_nd_entry_t *nsc_nd_entry_alloc(int, int);
static void nsc_nd_entry_dealloc(nsc_nd_entry_t *);
static void nsc_nd_add_list(nsc_nd_link_t **, nsc_nd_link_t *);
static void nsc_nd_rmv_list(nsc_nd_link_t **, nsc_nd_link_t *);
static nsc_nd_entry_t *nsc_nd_find_id_hash(int);

static SPIN_LOCK_T nsc_nd_mutex __cacheline_aligned_in_smp;
static nsc_nd_link_t *nsc_nd_id_hash[NSC_ND_ID_HASHSZ];
static nsc_nd_link_t *nsc_nd_nodelist[NSC_MAX_NODE_VALUE];
static int nsc_nd_seqno = 0;

int nsc_nd_alloc_fails = 0;

/*
 * void nsc_nd_init()
 *	Initialize the generic nodedown registration facility.
 */
void nsc_nd_init(void)
{
	INIT_SPIN_LOCK(&nsc_nd_mutex);
}

/*
 * int nsc_nd_reg(clusternode_t node, void (*func)(clusternode_t, void *),
 *		  void *args, int argsize, int flags)
 *	Called to register a nodedown callback function and arguments
 *	for a particular node.  On success, this routine returns a
 *	unique callback id that can be used later for deregistration.
 *	On failure, 0 is returned.
 */
int
nsc_nd_reg(
	clusternode_t node,
	void (*func)(clusternode_t, void *),
	void *args,
	int argsize,
	int flags)
{
	clms_api_state_t clms_state;
	nsc_nd_entry_t *ep;
	int id, hindex;

	ep = nsc_nd_entry_alloc(argsize, !(flags & NSC_ND_NOSLEEP));
	if (ep == NULL)
		return 0;

	ep->nde_node = node;
	ep->nde_flags = flags;
	ep->nde_func = func;
	if (args != NULL)
		bcopy((caddr_t)args, (caddr_t)&ep->nde_args[0], argsize);

	clms_api_get_full_state(this_node, &clms_state);

	LOCK_SPIN_LOCK(&nsc_nd_mutex);

	/*
	 * Check to make sure the node we're registering for is up.
	 * We do this under the nsc_nd_mutex lock so that the node
	 * going down after this check will not result in a race
	 * between nodedown processing and registration.
	 */
	if (!clms_isnodeavail(node)) {
		UNLOCK_SPIN_LOCK(&nsc_nd_mutex);
		nsc_nd_entry_dealloc(ep);
		return 0;
	}

	/*
	 * Create a nodedown registration id that is unique among nodes
	 * and across nodedowns.
	 */
	ep->nde_id = id = NSC_ND_MAKE_ID(this_node,
					 clms_state.up_transitions,
					 ++nsc_nd_seqno);
	hindex = NSC_ND_HASH(id);

	nsc_nd_add_list(&nsc_nd_id_hash[hindex], &ep->nde_id_link);
	nsc_nd_add_list(&nsc_nd_nodelist[node-1], &ep->nde_node_link);

	UNLOCK_SPIN_LOCK(&nsc_nd_mutex);

	return id;
}

/*
 * int nsc_nd_dereg(int id)
 *	Called with a nodedown registration id to deregister a nodedown
 *	callback.  On success, B_TRUE is returned and B_FALSE on failure.
 *
 *	The arg and argsize parameters can be used by the caller to read
 *	back the argument data specified during the original nsc_nd_reg()
 *	call.  This can be useful in the situation where the deregistration
 *	is being done in a different context than the one that did the
 *	registration (i.e. out of a server proc).  Pass NULL for arg and
 *	0 for argsize if the argument data is not needed.
 */
int
nsc_nd_dereg(
	int id,
	void *args,
	int argsize)
{
	nsc_nd_entry_t *ep;
	clusternode_t id_node;
	int hindex;

	id_node = NSC_ND_GET_ID_NODE(id);

	if (id_node != this_node) {
		printk(KERN_WARNING "nsc_nd_dereg: id from node %d on %d.",
			(int)id_node, (int)this_node);
		return B_FALSE;
	}

	LOCK_SPIN_LOCK(&nsc_nd_mutex);
	ep = nsc_nd_find_id_hash(id);
	if (ep == NULL) {
		UNLOCK_SPIN_LOCK(&nsc_nd_mutex);
		return B_FALSE;
	}
	else {
		hindex = NSC_ND_HASH(id);
		nsc_nd_rmv_list(&nsc_nd_id_hash[hindex],
				&ep->nde_id_link);
		nsc_nd_rmv_list(&nsc_nd_nodelist[ep->nde_node-1],
				&ep->nde_node_link);
		UNLOCK_SPIN_LOCK(&nsc_nd_mutex);

		if (args != NULL) {
			if (argsize != ep->nde_argsize) {
				printk(KERN_WARNING "nsc_nd_dereg: argsize(%d)"
						 " != nde_argsize(%d).",
						 argsize, ep->nde_argsize);
			}
			bcopy((caddr_t)&ep->nde_args[0], (caddr_t)args,
			      min(argsize, ep->nde_argsize));
		}
		nsc_nd_entry_dealloc(ep);

		return B_TRUE;
	}
}

/*
 * static void nsc_nd_nodedown_cleanup(clusternode_t node, nsc_nd_link_t *list)
 *	Called internally to run through the specified node list
 *	invoking callbacks.
 */
static void
nsc_nd_nodedown_cleanup(
	clusternode_t node,
	nsc_nd_link_t *list)
{
	nsc_nd_entry_t *ep;
	nsc_nd_link_t *next;
	nsc_nd_args_t *argsp;
	void *func_args;
	int error;

	/*
	 * Run through the list and invoke each callback.
	 */
	while (list) {
		next = list->ndl_next;
		ep = NSC_ND_NODELINK2ENTRY(list);

		if (ep->nde_flags & NSC_ND_SPAWN) {
			argsp = kmalloc_nofail(sizeof(nsc_nd_args_t));
			argsp->nda_node = node;
			argsp->nda_list = list;

			/*
			 * Make sure that the fresh spawn doesn't spawn
			 * again or traverse the list beyond the single
			 * element.
			 */
			ep->nde_flags &= ~NSC_ND_SPAWN;
			list->ndl_next = NULL;

			smp_mb(); /* barrier for argsp */
			error = spawn_daemon_thread("NDREG Daemon",
						    nsc_nd_nodedown_daemon,
						    (void *)argsp);
			if (error == 0) {
				list = next;
				continue;
			}
			else {
				printk(KERN_WARNING "nsc_nd_nodedown_cleanup:"
						 " unable to spawn NDREG"
						 " daemon.");
				kfree((caddr_t)argsp);
				/* FALLTHROUGH: invoke in current context */
			}
		}

		/*
		 * Invoke the nodedown callback routine.
		 */
		func_args = (ep->nde_argsize == 0) ? NULL : ep->nde_args;
		(*ep->nde_func)(node, func_args);

		nsc_nd_entry_dealloc(ep);

		list = next;
	}
}

/*
 * static void nsc_nd_nodedown_daemon(nsc_nd_link_t *list)
 *	Called internally in a spawned thread via the nsc_nd_nodedown()
 *	routine.
 */
static void
nsc_nd_nodedown_daemon(
	void *void_args)
{
	nsc_nd_args_t *argsp = (nsc_nd_args_t *)void_args;

	nsc_daemonize();

	nsc_nd_nodedown_cleanup(argsp->nda_node, argsp->nda_list);
	kfree((caddr_t)argsp);
	exit_daemon_thread();
}

/*
 * void nsc_nd_nodwdown(clusternode_t node)
 *	Called via CLMS when a node goes down.  This routine runs through
 *	and executes the registered callbacks.
 */
void
nsc_nd_nodedown(
	clusternode_t node)
{
	nsc_nd_entry_t *ep;
	nsc_nd_link_t *list, *next;
	nsc_nd_args_t *argsp;
	int hindex, error;

	/*
	 * Under lock, snap the pointer to the beginning of the list for
	 * the node going down and then NULL out the list.  Also remove
	 * each entry on the node list from the id hash.
	 */
	LOCK_SPIN_LOCK(&nsc_nd_mutex);
	list = nsc_nd_nodelist[node-1];
	nsc_nd_nodelist[node-1] = NULL;
	for (next = list; next != NULL; next = next->ndl_next) {
		ep = NSC_ND_NODELINK2ENTRY(next);
		hindex = NSC_ND_HASH(ep->nde_id);
		nsc_nd_rmv_list(&nsc_nd_id_hash[hindex],
				&ep->nde_id_link);
	}
	UNLOCK_SPIN_LOCK(&nsc_nd_mutex);

	if (list) {
		argsp = kmalloc_nofail(sizeof(nsc_nd_args_t));
		argsp->nda_node = node;
		argsp->nda_list = list;

		smp_mb();
		error = spawn_daemon_thread("NDREG Daemon",
					    nsc_nd_nodedown_daemon,
					    (void *)argsp);
		if (error) {
			printk(KERN_WARNING "nsc_nd_nodedown: unable to spawn"
					 " NDREG daemon.");
			kfree((caddr_t)argsp);
			nsc_nd_nodedown_cleanup(node, list);
		}
	}
}

/*
 * static nsc_nd_entry_t *
 * nsc_nd_entry_alloc(int argsize, int sleep)
 *	Called internally to allocate an nd entry.
 */
static nsc_nd_entry_t *
nsc_nd_entry_alloc(
	int argsize,
	int sleep)
{
	nsc_nd_entry_t *ep;
	int size;

	size = sizeof(nsc_nd_entry_t) - sizeof(char) + argsize;

	if (sleep)
		ep = kmalloc_nofail(size);
	else
		ep = kmalloc(size, GFP_ATOMIC);
	if (ep == NULL) {
		nsc_nd_alloc_fails++;
		return NULL;
	}

	bzero(ep, size);
	ep->nde_argsize = argsize;
	return ep;
}

/*
 * static void nsc_nd_entry_dealloc(nsc_nd_entry_t *ep)
 *	Called internally to deallocate an nd entry.
 */
static void
nsc_nd_entry_dealloc(
	nsc_nd_entry_t *ep)
{
	int size;

	size = sizeof(nsc_nd_entry_t) - sizeof(char) + ep->nde_argsize;

	kfree(ep);
}

/*
 * static void nsc_nd_add_list(nsc_nd_link_t **list, nsc_nd_link_t *link)
 *	Called internally to add an nd entry to a list.
 */
static void
nsc_nd_add_list(
	nsc_nd_link_t **list,
	nsc_nd_link_t *link)
{
	if (*list == NULL) {
		*list = link;
		link->ndl_prev = NULL;
		link->ndl_next = NULL;
	}
	else {
		link->ndl_next = *list;
		link->ndl_prev = NULL;
		(*list)->ndl_prev = link;
		*list = link;
	}
}

/*
 * static void nsc_nd_rmv_list(nsc_nd_link_t **list, nsc_nd_link_t *link)
 *	Called internally to remove an nd entry from a list.
 */
/* Called under nsc_nd_mutex spinlock */
static void
nsc_nd_rmv_list(
	nsc_nd_link_t **list,
	nsc_nd_link_t *link)
{
	if (link->ndl_prev)
		link->ndl_prev->ndl_next = link->ndl_next;
	else
		*list = link->ndl_next;

	if (link->ndl_next)
		link->ndl_next->ndl_prev = link->ndl_prev;

	rmb();
	link->ndl_prev = NULL;
	link->ndl_next = NULL;
}

/*
 * static void nsc_nd_find_id_hash(int id)
 *	Called internally to find a particular callback entry by id.
 */
static nsc_nd_entry_t *
nsc_nd_find_id_hash(
	int id)
{
	nsc_nd_entry_t *ep;
	nsc_nd_link_t *link;
	int hindex;

	hindex = NSC_ND_HASH(id);

	for (link = nsc_nd_id_hash[hindex]; link != NULL;
	     link = link->ndl_next) {
		ep = NSC_ND_IDLINK2ENTRY(link);
		if (ep->nde_id == id)
			return ep;
	}

	return NULL;
}

static int
nsc_nodeup(
	void *clms_handle,
	int service,
	clusternode_t node,
	clusternode_t surrogate,
	void *private)
{
	clms_nodeup_callback(clms_handle, service, node);
	return 0;
}


static int
nsc_nodedown(
	void *clms_handle,
	int service,
	clusternode_t node,
	clusternode_t surrogate,
	void *private)
{
	nsc_nd_nodedown(node);

	clms_nodedown_callback(clms_handle, service, node);
	return 0;
}


/*
 * Register NSC components with CI.
 */

static int __init ci_init_nsc(void)
{
	/* Register NSC_ND CLMS subsystem. */
	return register_clms_subsys("nsc",
				    1,
				    nsc_nodeup,
				    nsc_nodedown,
				    nsc_nd_init,
				    NULL,
				    NULL);
}

/* XXX: There is currently no option to compile this as a module. */
module_init(ci_init_nsc)
