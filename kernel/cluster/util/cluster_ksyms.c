/*
 * 	Symbol table for Cluster Infrastructure code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <linux/config.h>
#include <linux/module.h>

#include <cluster/nsc.h>
#include <cluster/nodelist.h>
#include <cluster/clms.h>
#include <cluster/icsgen.h>
#include <cluster/ics_proto.h>
#include <cluster/async.h>

EXPORT_SYMBOL(spawn_daemon_proc);
EXPORT_SYMBOL(exit_daemon_proc);
EXPORT_SYMBOL(spawn_daemon_thread);
EXPORT_SYMBOL(exit_daemon_thread);
EXPORT_SYMBOL(nsc_daemonize);
EXPORT_SYMBOL_GPL(this_node);

EXPORT_SYMBOL(nsc_nodelist_alloc);
EXPORT_SYMBOL(nsc_nodelist_free);
EXPORT_SYMBOL(nsc_nodelist_count);
EXPORT_SYMBOL(nsc_nodelist_copy);
EXPORT_SYMBOL(nsc_nodelist_get_next);
EXPORT_SYMBOL(nsc_nodelist_bit2node);

EXPORT_SYMBOL(clms_master_node);
EXPORT_SYMBOL(register_clms_subsys);
EXPORT_SYMBOL(register_clms_key_service);
EXPORT_SYMBOL(clms_nodeup_callback);
EXPORT_SYMBOL(clms_nodedown_callback);
EXPORT_SYMBOL(clms_get_cluster_num_nodes);
EXPORT_SYMBOL(clms_get_up_nodelist);
EXPORT_SYMBOL(clms_get_next_node);
EXPORT_SYMBOL(clms_get_nsc_nodelist);
EXPORT_SYMBOL(clms_get_clms_nodelist);
EXPORT_SYMBOL(clms_waitfor_key_service);
EXPORT_SYMBOL(clms_get_key_server_node);
EXPORT_SYMBOL(clms_get_key_secondary_node);
EXPORT_SYMBOL(clms_set_key_service_ready);
EXPORT_SYMBOL(clms_set_key_secondary_ready);
EXPORT_SYMBOL(clms_key_service_node_lock);
EXPORT_SYMBOL(clms_key_service_node_unlock);
EXPORT_SYMBOL(clms_api_get_state);
EXPORT_SYMBOL(clms_api_get_full_state);
EXPORT_SYMBOL(clms_isnodedown);
EXPORT_SYMBOL(clms_isnodeup);
EXPORT_SYMBOL(clms_ishalfup);
EXPORT_SYMBOL(clms_ishalfdown);
EXPORT_SYMBOL(clms_isnodeavail);
EXPORT_SYMBOL(clms_isnodeavailrdev);
EXPORT_SYMBOL(clms_isnode_inlist);
EXPORT_SYMBOL(clms_waitfor_nodedown);
EXPORT_SYMBOL(clms_get_node_status);
EXPORT_SYMBOL(clms_iskeynode);
EXPORT_SYMBOL(clms_is_secondary_by_addr);

EXPORT_SYMBOL(nsc_hcreate);
EXPORT_SYMBOL(nsc_hdestroy);
EXPORT_SYMBOL(nsc_svc_register);
EXPORT_SYMBOL(icssvr_decode_xdr);
EXPORT_SYMBOL(icssvr_encode_xdr);
EXPORT_SYMBOL(icscli_decode_xdr);
EXPORT_SYMBOL(icscli_encode_xdr);
EXPORT_SYMBOL(icssvr_free_xdr);

EXPORT_SYMBOL(ics_svc_register);
EXPORT_SYMBOL(register_ics_channel);
EXPORT_SYMBOL(register_cluster_svc);
EXPORT_SYMBOL(ics_geticsinfo);
EXPORT_SYMBOL(ics_unack_send);
EXPORT_SYMBOL(icscli_handle_get);
EXPORT_SYMBOL(icscli_send);
EXPORT_SYMBOL(icscli_wait_callback);
EXPORT_SYMBOL(icscli_wait);
EXPORT_SYMBOL(icscli_get_status);
EXPORT_SYMBOL(icscli_getnode);
EXPORT_SYMBOL(icscli_handle_release);
EXPORT_SYMBOL(icscli_handle_release_callback);

EXPORT_SYMBOL_GPL(nsc_async_queue);
EXPORT_SYMBOL_GPL(nsc_generic_async_queue);
