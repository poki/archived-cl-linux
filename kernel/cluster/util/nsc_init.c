/*
 * 	Cluster initialization code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/param.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/module.h>
#ifdef CONFIG_VPROC
#include <linux/dpvproc.h>
#endif /* VPROC */
#include <cluster/nsc.h>
#include <cluster/async.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/icsgen.h>
#include <cluster/ics_proto.h>
#include <cluster/procfs.h>
#ifdef CONFIG_SSI
#include <cluster/ssi/ssidev.h>
#include <cluster/ssi/cfs/cfs_mount.h>
#endif /* CONFIG_SSI */
#ifdef CONFIG_MOSIX_LL
#include <linux/mosix.h>
#endif

/*
 * Cluster configuration global variables.
 * These get set in call to ssisys_cluster_set_config().
 */
char *cluster_ifconfig = NULL;
char *cluster_clmsinfo = NULL;
char *cluster_svcinfo = NULL;
char *cluster_svcsecondary = NULL;
char *cluster_interfaces = NULL;
clusternode_t this_node = CLUSTERNODE_INVAL;

int cluster_state = CLUSTER_STATE_OFF;

/*
 * Global variable indicating whether NSC is enabled.
 * This variable is queried by the NSC_*_ENABLED()
 * macros in svc/systm.h.  (Set to B_FALSE in the
 * stubs version of this module.
 */
int nsc_enabled = B_TRUE;

clusternode_t root_fs_node = CLUSTERNODE_INVAL;

void *nsc_generic_async_queue;

/*
 * This file contains the NSC initialization code.
 */

extern void nsc_rcall_init(void);

/*
 * This function is called from main().
 */
int cluster_main_init_preroot(void)
{
	extern int nsc_async_daemons;

	sigset_t old_blocked;

	/*
	 * KS_XXX:
	 * The inet_*() routines will check for the current task's sigpending
	 * flag and fail with the error -ERESTARTSYS (-512) if it is set.
	 * Therefore, we need to clear this flag and block all incoming
	 * signals for the duration of NSC initialization to avoid returning
	 * this error.
	 */
        spin_lock_irq(&current->sighand->siglock);
	clear_thread_flag(TIF_SIGPENDING);
	old_blocked = current->blocked;
	sigfillset(&current->blocked);
        spin_unlock_irq(&current->sighand->siglock);

	/*
	 * Initialize some ICS locks that are used by other services'
	 * startup routines before ics_init gets called
	 */
	ics_earlylock_init();

	/*
	 * Initialize the generic async event queue.
	 */
#ifdef DYNAMIC_NSC_ASYNC_DAEMONS
	nsc_async_daemons = NSC_ASYNC_DAEMONS;
#endif
	nsc_generic_async_queue = nsc_async_queue_init("nsc_async",
						       nsc_async_daemons,
						       NSC_ASYNC_NO_MAXARGSZ);

	/*
	 * Initalize NSC ICS.
	 */
	ics_init();

	/*
	 * Initialize the nsc_rcall() RPC interface.
	 */
	nsc_rcall_init();

	/*
	 * Register all cluster services with ICS.
	 */
	ics_cluster_services_init();

	/*
	 * Initialize NSC CLMS.  This finds the CLMS master and all the
	 * key services and leaves the node in "half up" state.
	 */
	clms_init();

	printk("\n");
	printk("This is a CI/OpenSSI kernel.\n");
	printk("    This Cluster Node: %d\n", (int) this_node);
	printk("    Potential Initnode(s): %s\n", cluster_clmsinfo);
	printk("\n");

	clms_subsystems_init_prejoin();

	clms_key_svcs_init_prejoin();

	/*
	 * Join the cluster.
	 */
	clms_join_cluster();

	/*
	 * The routines below must be called after the node has joined,
	 * but before anything else happens.
	 */
	clms_subsystems_init_postjoin();

	clms_key_svcs_init_postjoin();

#ifdef CONFIG_SSI
	/* Needs to be initialiazed after ssidev key service for Lustre */
	cfs_shm_init();
#endif

	/* Create /proc/cluster directory in /proc */
#ifdef CONFIG_CLUSTER
	proc_cluster_init();
#endif

#ifdef CONFIG_MOSIX_LL
	init_mosix();
#endif

	/*
	 * KS_XXX:
	 * Restore signal mask for this task
	 * See comment above.
	 */
        spin_lock_irq(&current->sighand->siglock);
	current->blocked = old_blocked;
	recalc_sigpending();
        spin_unlock_irq(&current->sighand->siglock);

	return 0;
}

int cluster_main_init_postroot(void)
{

	clms_subsystems_init_postroot();

	clms_key_svcs_init_postroot();

	/* Cold boot is over. */
	clms_master_cold_boot = FALSE;
	clms_cold_boot = FALSE;

	/*
	 * The following function moves the current node to COMINGUP state.
	 * For a dependent node, init will be triggered to rexec() user
	 * mode initialization.
	 * This has to be done after the root has been mounted on this
	 * dependent node.
	 */
#ifndef CONFIG_SSI
	clms_startup_node();
#endif

	return 0;
}

EXPORT_SYMBOL_GPL(cluster_state);
