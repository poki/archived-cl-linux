#ident "@(#)$Header: /cvsroot/ci-linux/ci/kernel/cluster/util/nsc_callback.c,v 1.1 2003/06/03 22:22:29 bjbrew Exp $"
/*
 * 	Generic callback code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/kernel.h>
#include <linux/errno.h>
#include <cluster/rpc/xdr.h>
#include <cluster/rpc/svc.h>
#include <cluster/ics.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/nsc_callback.h>

/*
 * Callback Support
 *	- provides a simple interface for implementing a callback
 *	  for an asynchronous message, thus making it synchronous
 *
 * callback_getid:
 *	- initializes a callback and returns a unique id with which
 *	  it can be identified
 *
 * callback_wait:
 *	- wait for a callback to wake me up (or nodedown)
 *
 * callback_cancel:
 *	- cancel a callback, removing all traces of it
 *
 * callback_wakeup:
 *	- wake up a thread waiting on the specified id
 *
 * Example (part 1):
 *
 *	id = callback_getid(...);
 *	...send async message...
 *	if (!err)
 *		callback_wait(id);
 *	else
 *		callback_cancel(id);
 *
 * The example shows how a caller who has to send an asynchronous message
 * can provide synchronous behavior via the callback interface.
 *
 * It is assumed the async message will ultimately cause callback_wakeup()
 * to be called to wake up the waiting context.
 *
 * It is necessary to cancel the callback if there is an error sending
 * the message, otherwise the waiting context might never get woken up.
 *
 * Note that on nodedown, threads waiting for a callback will get woken up.
 * This is the reason why it is necessary to specify the target node
 * of the async message.
 */

static cblist_t cblist;

void
callback_init_ssi(void)
{
	INIT_LOCK(&cblist.cl_lock);
	cblist.cl_curid = 1;
	cblist.cl_list = NULL;
}


static cbdata_t *
callback_alloccb(void)
{
	cbdata_t	*cbp;

	cbp = KMEM_SLEEP_ALLOC(sizeof(cbdata_t));
	memset(cbp, 0, sizeof(cbdata_t));
	INIT_CONDITION(&cbp->cbd_cond);
	INIT_COND_LOCK(&cbp->cbd_lock);

	return cbp;
}

static cbdata_t *
callback_getcb(cbid_t id)
{
	cbdata_t	*cbp = NULL;

	CBLIST_LOCK();
	for (cbp = cblist.cl_list; cbp != NULL; cbp = cbp->cbd_next) {
		if (cbp->cbd_id == id) {
			LOCK_COND_LOCK(&cbp->cbd_lock);
			break;
		}
	}
	CBLIST_UNLOCK();

	return cbp;
}

static void
callback_freecb(cbdata_t *cbp)
{
	DEINIT_COND_LOCK(&cbp->cbd_lock);
	DEINIT_CONDITION(&cbp->cbd_cond);
	KMEM_SLEEP_FREE(cbp, sizeof(*cbp));
}

/*
 * Add callback to global list.
 *	- doubly-linked, linear, null-terminated list
 *	- callback id's are assigned incrementally
 *
 * Assumption:
 *	Caller has exclusive access to callback so we don't bother
 *	locking it.
 */
static cbid_t
callback_addcb(cbdata_t *cbp)
{
	CBLIST_LOCK();
	cbp->cbd_id = cblist.cl_curid++;
	cbp->cbd_next = cblist.cl_list;
	cbp->cbd_prev = NULL;
	if (cbp->cbd_next != NULL)
		cbp->cbd_next->cbd_prev = cbp;
	cblist.cl_list = cbp;
	CBLIST_UNLOCK();

	return cbp->cbd_id;
}

/*
 * Remove callback from global list.
 * Callback must be inactive (~CB_VALID).
 *
 * Assumption:
 *	Caller has exclusive access to callback so we don't bother
 *	locking it.
 */
static void
callback_delcb_l(cbdata_t *cbp)
{
	SSI_ASSERT_CBLIST_IS_LOCKED();
	SSI_ASSERT(!(cbp->cbd_flags & CB_VALID));

	if (cbp->cbd_prev == NULL) {
		SSI_ASSERT(cblist.cl_list == cbp);
		cblist.cl_list = cbp->cbd_next;;
	} else
		cbp->cbd_prev->cbd_next = cbp->cbd_next;
	if (cbp->cbd_next != NULL)
		cbp->cbd_next->cbd_prev = cbp->cbd_prev;
}

static void
callback_delcb(cbdata_t *cbp)
{
	CBLIST_LOCK();
	callback_delcb_l(cbp);
	CBLIST_UNLOCK();
}

cbid_t
callback_getid(
	clusternode_t	node,
	void		*bufp,
	size_t		bufsz)
{
	cbid_t		id;
	cbdata_t	*cbp;

	SSI_ASSERT(bufsz <= CALLBACK_DATA_MAX);
	SSI_ASSERT(bufsz == 0 || bufp != NULL);

	cbp = callback_alloccb();
	if (bufp || bufsz) {
		SSI_ASSERT(bufp && bufsz);
		cbp->cbd_bufp = bufp;
		cbp->cbd_bufsz = bufsz;
	}
	cbp->cbd_flags = CB_VALID;
	cbp->cbd_node = node;

	id = callback_addcb(cbp);

	return id;
}

int
callback_wait(cbid_t id)
{
	int		rval;
	cbdata_t	*cbp;

	cbp = callback_getcb(id);
	SSI_ASSERT(cbp != NULL);
	SSI_ASSERT_LOCKED_COND_LOCK(&cbp->cbd_lock);
	SSI_ASSERT(cbp->cbd_flags & CB_VALID);
	SSI_ASSERT(!(cbp->cbd_flags & CB_WAITING));

	if (!(cbp->cbd_flags & (CB_RETURNED|CB_NODEDOWN))) {
		SSI_ASSERT(cbp->cbd_flags == CB_VALID);
		cbp->cbd_flags |= CB_WAITING;
		WAIT_CONDITION(&cbp->cbd_cond, &cbp->cbd_lock);
	}

	if (cbp->cbd_flags & CB_RETURNED)
		rval = 0;
	else {
		SSI_ASSERT(cbp->cbd_flags & CB_NODEDOWN);
		rval = -EREMOTE;
	}
	cbp->cbd_flags &= ~CB_VALID;
	UNLOCK_COND_LOCK(&cbp->cbd_lock);

	/*
	 * Remove callback from list.
	 * Callback is no longer needed.
	 */
	callback_delcb(cbp);
	callback_freecb(cbp);

	return rval;
}

void
callback_cancel(cbid_t id)
{
	cbdata_t	*cbp;

	CBLIST_LOCK();

	for (cbp = cblist.cl_list; cbp != NULL; cbp = cbp->cbd_next) {
		if (cbp->cbd_id == id) {
			LOCK_COND_LOCK(&cbp->cbd_lock);
			break;
		}
	}
	SSI_ASSERT(cbp != NULL);
	SSI_ASSERT(cbp->cbd_flags & CB_VALID);
	cbp->cbd_flags &= ~CB_VALID;
	UNLOCK_COND_LOCK(&cbp->cbd_lock);

	/*
	 * Remove callback from list.
	 * Must do this holding callback list locked to prevent
	 * another lookup from finding it before we free it.
	 */
	callback_delcb_l(cbp);
	callback_freecb(cbp);

	CBLIST_UNLOCK();
}

int
callback_wakeup(
	cbid_t		id,
	caddr_t		datap,
	uint		datalen)
{
	cbdata_t	*cbp;
	int do_wakeup = 0;

	SSI_ASSERT(datalen <= CALLBACK_DATA_MAX);
	SSI_ASSERT(datalen == 0 || datap != NULL);

	cbp = callback_getcb(id);
	if (cbp == NULL) {
		/*
		 * This may happen if the callback returned but a subsequent
		 * nodedown was processed first and allowed the waiter to
		 * remove the callback structure before this return
		 * was processed.
		 * NSC_XXX: This may also indicate programming error.
		 *	    Need a way to differentiate b/t the two.
		 */
#ifdef DEBUG
		printk(KERN_WARNING
			"Callback %d unable to locate caller\n",
			id);
#endif /* DEBUG */
		return -EINVAL;
	}

	SSI_ASSERT_LOCKED_COND_LOCK(&cbp->cbd_lock);
	SSI_ASSERT(!(cbp->cbd_flags & CB_RETURNED));
	SSI_ASSERT(cbp->cbd_flags & CB_VALID);
	SSI_ASSERT(datalen == cbp->cbd_bufsz);
	cbp->cbd_flags |= CB_RETURNED;
	bcopy(datap, cbp->cbd_bufp, datalen);
	if (cbp->cbd_flags & CB_WAITING) {
		cbp->cbd_flags &= ~CB_WAITING;
		do_wakeup = 1;
	}
	UNLOCK_COND_LOCK(&cbp->cbd_lock);
	if (do_wakeup)
		SIGNAL_CONDITION(&cbp->cbd_cond);

	return 0;
}

void
callback_nodedown(clusternode_t node)
{
	cbdata_t	*cbp;
	int do_wakeup = 0;

	CBLIST_LOCK();
	for (cbp = cblist.cl_list; cbp != NULL; cbp = cbp->cbd_next) {
		if (cbp->cbd_node == node) {
			LOCK_COND_LOCK(&cbp->cbd_lock);
			SSI_ASSERT(!(cbp->cbd_flags & CB_NODEDOWN));
			cbp->cbd_flags |= CB_NODEDOWN;
			if (cbp->cbd_flags & CB_WAITING) {
				cbp->cbd_flags &= ~CB_WAITING;
				do_wakeup = 1;
			}
			UNLOCK_COND_LOCK(&cbp->cbd_lock);
			if (do_wakeup) {
				do_wakeup = 0;
				SIGNAL_CONDITION(&cbp->cbd_cond);
			}
		}
	}
	CBLIST_UNLOCK();
}
#if defined(DEBUG) || defined(DEBUG_TOOLS)

void
print_cb(cbdata_t *cbp)
{
	printk(KERN_INFO "cb 0x%p id 0x%x flags 0x%x node %u\n",
		cbp, cbp->cbd_id, cbp->cbd_flags, cbp->cbd_node);
	printk(KERN_INFO " bufp 0x%p bufsz 0x%p next 0x%p prev 0x%p\n",
		cbp->cbd_bufp, (void *)cbp->cbd_bufsz,
	       cbp->cbd_next, cbp->cbd_prev);
}

void
print_cblist(void)
{
	cbdata_t	*cbp = NULL;

	printk(KERN_INFO "cblist 0x%p curid %d\n",
	       cblist.cl_list, cblist.cl_curid);
	for (cbp = cblist.cl_list; cbp != NULL; cbp = cbp->cbd_next)
		print_cb(cbp);
}

#endif /* DEBUG || DEBUG_TOOLS */
