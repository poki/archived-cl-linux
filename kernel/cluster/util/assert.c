/*
 * 	Debugging assertion support.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * Debugging assertion support.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <cluster/assert.h>

/*
 * panic() in linux has the "noreturn" attribute; this causes the optimizer
 * to get confused under some circumstances when it was in the "assert"
 * macros themselves. This is a workaround for the problem.
 */

int ssi_assert_panic = 0;

int ssi_assert_panic_hook(void)
{
	return ssi_assert_panic;
}

void ssi_assert_failed(const char *assertion, const char *file, int line,
		       const char *function)
{
	if (ssi_assert_panic_hook())
		panic("Assertion failed! %s, %s, %s, line=%d\n",
		      assertion, file, function, line);
	else
		printk(KERN_DEBUG "Assertion failed! %s, %s, %s, line=%d\n",
		       assertion, file, function, line);
}

EXPORT_SYMBOL(ssi_assert_failed);

