/*
 * 	Cluster system calls.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <linux/types.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/ctype.h>
#include <linux/mount.h>
#include <linux/utsname.h>
#include <linux/syscalls.h>
#include <linux/namespace.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#ifdef CONFIG_VPROC
#include <linux/vproc.h>
#include <linux/dpvproc.h>
#endif /* VPROC */

#include <cluster/config.h>
#include <cluster/nsc.h>
#include <cluster/assert.h>
#include <cluster/rpc/rpc.h>
#include <cluster/icsgen.h>
#include <cluster/ics.h>
#include <cluster/clms.h>
#include <cluster/synch.h>
#include <cluster/ssisys.h>
#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
#include <cluster/ssi/cfs/cfs_mount.h>
#include <cluster/ssi/ssidev.h>
#endif

#include <cluster/gen/ics_cluster_api_protos_gen.h>
#include <cluster/gen/ics_cluster_api_macros_gen.h>

#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
#include <cluster/ssi/clreg.h>
#endif /* CONFIG_SSI */

extern int copy_mount_options (const void __user *, unsigned long *);

#if defined(CONFIG_IP_VS) && defined (CONFIG_SSI)
#include <cluster/ssi/net.h>
#endif

extern clusternode_t root_fs_node;

/*
 * Prototypes for routines called from ssisys()
 * Functions called from ssisys() take the following form:
 *	static int <func_name>(
 *			caddr_t		uaddrin,
 *			u_int		uaddrinlen,
 *			caddr_t		uaddrout,
 *			u_int		uaddroutlen,
 *			rval_t		*rvp);
 */
static long ssisys_badop(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_node_getnum(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_node_down(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_reclaim_child(caddr_t, u_int, caddr_t, u_int, rval_t *);
#ifdef CONFIG_VPROC
static long ssisys_node_pid(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_islocal(caddr_t, u_int, caddr_t, u_int, rval_t *);
#endif /* VPROC */
#ifdef CONFIG_CFS_XXX
static long ssisys_cfs_wait_mode(caddr_t, u_int, caddr_t, u_int, rval_t *);
#endif /* CONFIG_CFS_XXX */
static long ssisys_ignore_halfup(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_set_memprio(caddr_t, u_int, caddr_t, u_int, rval_t *);
#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
long  ssisys_ipc_getinfo(caddr_t, u_int, caddr_t, u_int, rval_t *);
#endif /* CONFIG_SSI */

static long ssisys_get_primary(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_get_secondary(caddr_t, u_int, caddr_t, u_int, rval_t *);

static long clusternode_num(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long cluster_membership(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long cluster_detailedtransition(caddr_t, u_int, caddr_t,
				       u_int, rval_t *);
static long clusternode_info(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long clusternode_setinfo(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long cluster_maxnodes(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long clusternode_avail(caddr_t, u_int, caddr_t, u_int, rval_t *);

static long ssisys_get_node_counts(caddr_t, u_int, caddr_t, u_int, rval_t *);

static long ssisys_force_root_node(caddr_t, u_int, caddr_t, u_int, rval_t *);

static long ssisys_get_transport(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_cluster_set_config(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_cluster_init_preroot(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_cluster_init_postroot(caddr_t, u_int, caddr_t, u_int, rval_t *);
#ifdef CONFIG_VPROC
static long ssisys_cluster_initproc(caddr_t, u_int, caddr_t, u_int, rval_t *);
#endif /* VPROC */
#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
static long ssisys_mount_remote_root(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_discover_mounts(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_set_node_context(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_set_nodename(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_set_clustername(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_get_nodename(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_get_clustername(caddr_t, u_int, caddr_t, u_int, rval_t *);
#endif /* CONFIG_SSI */
#ifdef CONFIG_CFS
static long ssisys_cfs_mount(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_cfs_remount(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_cfs_setroot(caddr_t, u_int, caddr_t, u_int, rval_t *);
#endif
#if defined(CONFIG_IP_VS ) && defined (CONFIG_SSI)
static long ssisys_lvs(caddr_t, u_int, caddr_t, u_int, rval_t *);
static long ssisys_set_ipvsportweight(caddr_t, u_int, caddr_t, u_int, rval_t *);
#endif

/*
 * ssisys() syscall ops table
 * NOTE: Any changes to this list must also be made to corresponding indices
 */
ssisys_ops_t ssisys_ops[] = {
	{ ssisys_badop },
	{ ssisys_badop },		/* placeholder == 1 */
	{ ssisys_badop },		/* placeholder == 2 */
	{ ssisys_badop },		/* placeholder == 3 */
	{ ssisys_badop },		/* placeholder == 4 */
	{ ssisys_badop },		/* placeholder == 5 */
	{ ssisys_badop },		/* placeholder == 6 */
	{ ssisys_badop },		/* placeholder == 7 */
	{ ssisys_node_getnum },		/* SSISYS_NODE_GETNUM == 8 */
	{ ssisys_badop },
	{ ssisys_badop },		/* placeholder == 10 */
	{ ssisys_node_down },		/* SSISYS_NODE_DOWN == 11 */
	{ ssisys_reclaim_child },	/* SSISYS_RECLAIM_CHILD == 12 */
#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
	{ ssisys_ipc_getinfo },		/* SSISYS_IPC_GETINFO == 13 */
#else  /* !CONFIG_SSI */
	{ ssisys_badop },		/* placeholder == 13 */
#endif /* !CONFIG_SSI */
	{ ssisys_badop },		/* placeholder == 14 */
#ifdef CONFIG_VPROC
	{ ssisys_node_pid },		/* SSISYS_NODE_PID == 15 */
	{ ssisys_islocal },		/* SSISYS_ISLOCAL == 16 */
#else
	{ ssisys_badop },		/* placeholder == 15 */
	{ ssisys_badop },		/* placeholder == 16 */
#endif /* VPROC */
	{ ssisys_badop },		/* placeholder == 17 */
	{ ssisys_badop },		/* placeholder == 18 */
	{ ssisys_badop },		/* placeholder == 19 */
	{ ssisys_badop },		/* placeholder == 20 */
	{ ssisys_badop },		/* placeholder == 21 */
	{ ssisys_badop },		/* placeholder == 22 */
#ifdef CONFIG_CFS_XXX
	{ ssisys_cfs_wait_mode },	/* SSISYS_CFS_WAIT_MODE == 23 */
#else /* !CONFIG_CFS_XXX */
	{ ssisys_badop },		/* placeholder == 23 */
#endif /* CONFIG_CFS_XXX */
	{ ssisys_badop },		/* placeholder == 24 */
	{ ssisys_badop },		/* placeholder == 25 */
	{ ssisys_badop },		/* placeholder == 26 */
	{ ssisys_badop },		/* placeholder == 27 */
	{ ssisys_badop },		/* placeholder == 28 */
	{ ssisys_badop },		/* placeholder == 29 */
	{ ssisys_badop },		/* placeholder == 30 */
	{ ssisys_badop },		/* was SSISYS_DECLARE_NODE_UP == 31 */
	{ ssisys_badop },		/* was SSISYS_DECLARE_NODE_DOWN == 32 */
	{ ssisys_ignore_halfup },	/* SSISYS_IGNORE_HALFUP == 33 */
	{ ssisys_badop },		/* placeholder == 34 */
	{ ssisys_get_primary },		/* SSISYS_GET_PRIMARY == 35 */
	{ ssisys_get_secondary },	/* SSISYS_GET_SECONDARY == 36 */
	{ ssisys_badop },		/* placeholder == 37 */
	{ clusternode_num },		/* API - SSISYS_CLUSTERNODE_NUM == 38 */
	{ cluster_membership },		/* API - SSISYS_CLUSTER_MEMBERSHIP == 39 */
	{ cluster_detailedtransition },	/* API - SSISYS_CLUSTER_DETAILEDTRANS== 40 */
	{ clusternode_info },		/* API - SSISYS_CLUSTERNODE_INFO== 41 */
	{ clusternode_setinfo },	/* API - SSISYS_CLUSTERNODE_SETINFO== 42 */
	{ clusternode_avail },		/* API - SSISYS_CLUSTERNODE_AVAIL== 43 */
	{ cluster_maxnodes },		/* API - SSISYS_CLUSTER_MAXNODES== 44 */
	{ ssisys_set_memprio },		/* SSISYS_SET_MEMPRIO == 45 */
	{ ssisys_badop },		/* placeholder == 46 */
	{ ssisys_force_root_node },	/* SSISYS_FORCE_ROOT_NODE == 47 */
	{ ssisys_badop },		/* placeholder == 48 */
	{ ssisys_badop },		/* placeholder == 49 */
	{ ssisys_get_node_counts },	/* SSISYS_GET_NODE_COUNTS == 50 */
	{ ssisys_get_transport },	/* SSISYS_GET_TRANSPORT == 51 */
	{ ssisys_cluster_set_config },	/* SSISYS_CLUSTER_SET_CONFIG == 52 */
	{ ssisys_cluster_init_preroot },
					/* SSISYS_CLUSTER_INIT_PREROOT == 53 */
	{ ssisys_cluster_init_postroot },
					/* SSISYS_CLUSTER_INIT_POSTROOT == 54 */
#ifdef CONFIG_VPROC
	{ ssisys_cluster_initproc },	/* SSISYS_CLUSTER_INITPROC == 55 */
#else /* !VPROC */
	{ ssisys_badop },		/* placeholder == 55 */
#endif /* !VPROC */
#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
	{ ssisys_mount_remote_root},	/* SSISYS_MOUNT_REMOTE_ROOT = 56 */
	{ ssisys_discover_mounts},	/* SSISYS_DISCOVER_MOUNTS = 57 */
#else
	{ ssisys_badop },		/* placeholder == 56 */
	{ ssisys_badop },		/* placeholder == 57 */
#endif
#ifdef CONFIG_CFS
	{ ssisys_cfs_mount},		/* SSISYS_CFS_MOUNT = 58 */
#else
	{ ssisys_badop },		/* placeholder == 58 */
#endif
#if defined(CONFIG_IP_VS ) && defined (CONFIG_SSI)
	{ ssisys_lvs },      		/* SSISYS_LVS == 59 */
#else
	{ ssisys_badop },                /* placeholder == 59 */
#endif
#ifdef CONFIG_CFS
	{ ssisys_cfs_remount },		/* SSISYS_CFS_REMOUNT = 60 */
	{ ssisys_cfs_setroot },		/* SSISYS_CFS_SET_ROOT = 61 */
#else
	{ ssisys_badop },		/* placeholder == 60 */
	{ ssisys_badop },		/* placeholder == 61 */
#endif
#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
	{ ssisys_set_node_context },	/* SSISYS_SET_NODE_CONTEXT = 62 */
	{ ssisys_set_nodename },	/* SSISYS_SET_NODENAME = 63 */
#else
	{ ssisys_badop },		/* placeholder == 62 */
	{ ssisys_badop },		/* placeholder == 63 */
#endif
#if defined(CONFIG_IP_VS ) && defined (CONFIG_SSI)
	{ ssisys_set_ipvsportweight },  /* SSISYS_SET_IPVSPORTWEIGHT == 64 */
#else
	{ ssisys_badop },                /* placeholder == 64 */
#endif
#if defined(CONFIG_SSI) || defined(CONFIG_CFS)
	{ ssisys_get_nodename },	/* SSISYS_GET_NODENAME = 65 */
	{ ssisys_set_clustername },	/* SSISYS_SET_CLUSTERNAME = 66 */
	{ ssisys_get_clustername },	/* SSISYS_GET_CLUSTERNAME = 67 */
#else
	{ ssisys_badop },		/* placeholder == 65 */
	{ ssisys_badop },		/* placeholder == 66 */
	{ ssisys_badop },		/* placeholder == 67 */
#endif

};

/*
 * ssisys() op table size
 */
uint ssisys_numops = sizeof(ssisys_ops)/sizeof(ssisys_ops_t);


/* Forward declaration */
int rcluster_api_setstate(clusternode_t, int *, clusternode_t, int);

/*
 * ssisys
 *	system call for NSC-specific options
 *
 * Description
 *	This routine calls the appropriate function to handle the
 *	user-specified NSC option. It provides a generalized interface for
 *	calling subroutines and supporting input/output arguments.
 *
 *	Additional functionality may be added and is supported by
 *	version numbering.
 *
 * Parameters
 *	uap	pointer to input arguments (in user space)
 *	rvp	pointer to return value structure
 */

long do_ssisys(ssisys_iovec_t *iovec)
{
	ssisys_ops_t		*entryp;
	long			error = 0;
	int			opnum;

	/*
	 * Check version
	 *	- additional versions may be supported here,
	 *	  e.g. via callout to version-specific functionality
	 */
	switch(iovec->tio_id.id_ver) {
	case SSISYS_VER1:			/* initial version */
	case SSISYS_VER2:
	case SSISYS_VER3:
		printk(KERN_CRIT "do_ssisys: cluster-tools out of sync\n");
		break;
	case SSISYS_CURVER:			/* current version */
		/*
		 * do nothing
		 */
		break;
	default:				/* invalid version */
		error = -EINVAL;
#ifdef DEBUG
		printk(KERN_DEBUG "ssisys: Invalid Version\n");
#endif
		goto out;
	}

	if ((opnum = iovec->tio_id.id_cmd) >= ssisys_numops)
		opnum = 0;			/* index to ssisys_badop() */

	switch(opnum) {
		case SSISYS_CLUSTER_SET_CONFIG:
			if (!capable(CAP_SYS_ADMIN))
				return -EPERM;	/* only root can configure
						   the cluster. */
			else if (cluster_state != CLUSTER_STATE_OFF &&
				 cluster_state != CLUSTER_STATE_CONFIG)
				return -EEXIST;	/* cluster already
						   configured. */
			break;
		case SSISYS_CLUSTER_INIT_PREROOT:
			if (!capable(CAP_SYS_ADMIN))
				return -EPERM;	/* only root can configure
						   the cluster. */
			else if (cluster_state == CLUSTER_STATE_OFF)
				return -EAGAIN;	/* cluster not yet
						   configured. */
			else if (cluster_state != CLUSTER_STATE_CONFIG)
				return -EEXIST;	/* cluster already ran
						   pre-root initialization. */
			break;
		case SSISYS_CLUSTER_INIT_POSTROOT:
			if (!capable(CAP_SYS_ADMIN))
				return -EPERM;	/* only root can configure
						   the cluster. */
#ifdef CONFIG_VPROC
			else if (cluster_state == CLUSTER_STATE_POSTROOT ||
				 cluster_state == CLUSTER_STATE_POSTINIT)
#else /* !VPROC */
			else if (cluster_state == CLUSTER_STATE_POSTROOT)
#endif /* !VPROC */
				return -EEXIST; /* cluster already ran
						   post-root initialization. */
			else if (cluster_state != CLUSTER_STATE_PREROOT)
				return -EAGAIN; /* cluster hasn't yet run
						   pre-root initialization. */
			break;
#ifdef CONFIG_VPROC
		case SSISYS_CLUSTER_INITPROC:
			if (!capable(CAP_SYS_ADMIN))
				return -EPERM;	/* only root can configure
						   the cluster. */
			else if (cluster_state == CLUSTER_STATE_POSTINIT)
				return -EEXIST;	/* cluster already ran
						   initproc. */
			else if (cluster_state != CLUSTER_STATE_POSTROOT)
				return -EAGAIN;	/* cluster hasn't yet run
						   post-root initialization. */
			break;
#endif /* VPROC */
		case SSISYS_MOUNT_REMOTE_ROOT:
			if (!capable(CAP_SYS_ADMIN))
				return -EPERM;	/* only root can configure
						   the cluster. */
			if (cluster_state != CLUSTER_STATE_PREROOT)
				return -EAGAIN;
			break;
		case SSISYS_CLUSTER_MAXNODES:
			/* This is used for ssiconfig, so always call func */
			break;
#ifdef CONFIG_CFS
		case SSISYS_CFS_MOUNT:
#endif
		case SSISYS_CLUSTERNODE_NUM:
			/* This is ok once at PREROOT state */
			if (cluster_state >= CLUSTER_STATE_PREROOT)
				break;
			/* FALLSTHROUGH */
		default:
#ifdef CONFIG_VPROC
			if ((cluster_state != CLUSTER_STATE_POSTINIT) &&
			    (cluster_state != CLUSTER_STATE_POSTROOT))
#else /* !VPROC */
			if (cluster_state != CLUSTER_STATE_POSTROOT)
#endif /* !VPROC */
			{
				printk("do_ssisys: Illegal op %d in state %d\n", opnum, cluster_state);
				return -EAGAIN; /* cluster not yet up. */
			}
			break;
	}

	entryp = &ssisys_ops[opnum];

	error = (*entryp->ts_func)(iovec->tio_udatain,
				   iovec->tio_udatainlen,
				   iovec->tio_udataout,
				   iovec->tio_udataoutlen,
				   NULL);
out:
	return error;
}

asmlinkage long
sys_ssisys(
	caddr_t		ts_udata,
	ssize_t		ts_udata_len)
{
	ssisys_iovec_t		iovec;

	/*
	 * Sanity check(s)
	 */
	if (ts_udata_len < 0 || ts_udata_len > sizeof(iovec)) {
#ifdef DEBUG
		printk(KERN_DEBUG "ssisys: size of input/output vector %d invalid\n",
		       ts_udata_len);
#endif
		return -EINVAL;
	}
	if (ts_udata_len != sizeof(iovec))
		memset(&iovec, 0, sizeof(iovec));

	/*
	 * Copy in arguments
	 */
	if (copy_from_user((void *)&iovec, (void *)ts_udata, ts_udata_len)) {
#ifdef DEBUG
		printk(KERN_DEBUG "ssisys: failed to copy in input/output vector\n");
#endif
		return -EFAULT;
	}

	return do_ssisys(&iovec);
}

/*****************************************************************************/
/*
 * BEGIN: routines called from general ssisys() interface
 */
/*****************************************************************************/

long
ssisys_badop(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	(void)uaddrin;
	(void)uaddrin_len;
	(void)uaddrout;
	(void)uaddrout_len;
	(void)rvp;

	return -EINVAL;
}


/*
 * Get the number of this node
 */
long
ssisys_node_getnum(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	(void)uaddrin;
	(void)uaddrin_len;
	(void)uaddrout;
	(void)uaddrout_len;
	(void)rvp;

	return this_node;
}

/*
 * Temporary test harness for testing nodedown.
 */
long
ssisys_node_down(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	extern int clms_do_nodedown(clusternode_t, int);
	extern int clms_nodedebug(clusternode_t);
	ts_nodedown_inargs_t	inargs;
	int			error = 0;

	(void) uaddrout;
	(void) rvp;

	if (!capable(CAP_SYS_ADMIN))
		return -EPERM;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0)		/* no out arg */
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len)) {
		error = -EFAULT;
		goto out;
	}

	if (inargs.tnd_state == CLMS_NODEDOWN_DEBUG)
#if defined(DEBUG) || defined (DEBUG_TOOLS)
		error = clms_nodedebug(inargs.tnd_node);
#else /* NOT DEBUG || DEBUG_TOOLS */
		error = -EINVAL;
#endif /* DEBUG || DEBUG_TOOLS */
	else if ((inargs.tnd_state == CLMS_NODEDOWN_HALT)
			|| (inargs.tnd_state == CLMS_NODEDOWN_DOWN)) {
		if (inargs.tnd_node == 0)
			error = -EINVAL;
		else
			error = clms_do_nodedown(inargs.tnd_node,
							inargs.tnd_state);
	} else
		error = -EINVAL;

out:
	return error;
}

/*
 * Interface for reclaiming child from INIT.
 */
long
ssisys_reclaim_child(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{

#ifdef CONFIG_VPROC_ND
	ts_reclaim_child_inargs_t inargs;
	struct vproc *vc;
	struct vproc *vp = current->p_vproc;
	int error;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len != sizeof(inargs) || uaddrout_len != 0)
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	vc = LOCATE_VPROC_PID(inargs.trc_pid, "ssisys_reclaim_child");
	if (vc == NULL)
		return -ESRCH;

	error = VPOP_RECLAIM_CHILD(vc, vp->vp_pid, inargs.trc_start, 0);

	VPROC_RELE(vc, "ssisys_reclaim_child");
	return error;
#else /* VPROC_ND */
	return -EINVAL;
#endif /* !VPROC_ND */
}

#ifdef CONFIG_VPROC
long
ssisys_node_pid(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	ts_nodepid_inargs_t	inargs;
	int			error;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0)		/* no out arg */
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	if (inargs.tnp_pid < 0)
		return -EINVAL;

	error = process_execnode(inargs.tnp_pid);
	if (error == CLUSTERNODE_INVAL)
		error = -ESRCH;

	return error;
}

long
ssisys_islocal(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	ts_islocal_inargs_t	inargs;
	struct vproc		*vp;
	int			error;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0)		/* no out arg */
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	if (inargs.til_pid < 0)
		return -EINVAL;

	vp = LOCATE_VPROC_PID(inargs.til_pid, "temp islocal");
	if (vp == NULL)
		return -EINVAL;

#ifdef VPROC_HOLD_ZERO_GET_TASK
        if (PVP(vp)->pvp_flag & PV_IS_LOCAL)
#else
        if (PVP(vp)->pvp_flag & PV_PROCESS)
#endif
                error = 1;
        else
                error = 0;

	VPROC_RELE(vp, "temp islocal");
	return(error);
}
#endif /* VPROC */

#ifdef CONFIG_CFS_XXX
long
ssisys_cfs_wait_mode(caddr_t			uaddrin,
		     u_int			uaddrin_len,
		     caddr_t			uaddrout,
		     u_int			uaddrout_len,
		     rval_t			*rvp)
{
	ts_cfs_wait_mode_inargs_t inargs;
	int new_setting;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0)		/* no out arg */
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	/* SSI_XXX: check creds for permission */

	/* Validate setting. */
	if ((inargs.cwm_mode_bits & ~(CFS_WAIT_MOUNT | CFS_WAIT_NOWAIT)) != 0)
		return -EINVAL;

	new_setting = 0;
	if (inargs.cwm_mode_bits & CFS_WAIT_MOUNT)
		new_setting |= PF_CFS_MOUNT;
	if (inargs.cwm_mode_bits & CFS_WAIT_NOWAIT)
		new_setting |= PF_CFS_NOWAIT;
	/* Update setting. */
#ifdef NOTYET
	current->flags &= ~PF_CFS_ALL;
	current->flags |= new_setting;
#endif /* NOTYET */
	return 0;
} /* end ssisys_cfs_wait_mode() */
#endif /* CONFIG_CFS_XXX */

/*****************************************************************************/


/*
 * Name: ssisys_ignore_halfup()
 *
 * Function:
 *
 * After a node joins a cluster, it sets its CLMS state as half-up
 * to allow connections, but to fail rexecs/rforks/migrates.  This
 * system call marks a process as special so that it can perform
 * rexecs/rforks/migrates to a half-up nodes.  This is used to
 * allow node initialization before declaring the node full-up.
 */
long
ssisys_ignore_halfup(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
#ifdef CONFIG_VPROC
	struct vproc *vp = current->p_vproc;
#endif
	(void) uaddrin;
	(void) uaddrin_len;
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	if (!capable(CAP_SYS_ADMIN))
		return(-EPERM);

#ifdef CONFIG_VPROC
	VPROC_LOCK_FLAG(vp, "ssisys_ignore_halfup");
	PVP(vp)->pvp_flag |= PV_IGNORE_HALFUP;
	VPROC_UNLOCK_FLAG(vp, "ssisys_ignore_halfup");
#endif /* VPROC */

	return(0);
}

long
ssisys_get_primary(caddr_t		uaddrin,
		  u_int			uaddrin_len,
		  caddr_t		uaddrout,
		  u_int			uaddrout_len,
		  rval_t		*rvp)
{
	ts_get_primary_inargs_t inargs;
	int error;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0)		/* no out arg */
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	if ((inargs.tgp_service < 0) ||
	    (inargs.tgp_service > CLMS_LAST_KEY_SERVICE))
		return -EINVAL;

	/* Wait for service if none exists */
	error = clms_get_key_server_node(inargs.tgp_service, 1);
	if (error == CLUSTERNODE_INVAL)
		return -EINVAL;

	return error;
} /* end ssisys_cfs_get_primary() */

long
ssisys_get_secondary(caddr_t		uaddrin,
		  u_int			uaddrin_len,
		  caddr_t		uaddrout,
		  u_int			uaddrout_len,
		  rval_t		*rvp)
{
	ts_get_primary_inargs_t inargs;
	int error;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0)		/* no out arg */
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	/* Wait for service if none exists */
	error = clms_get_key_secondary_node(inargs.tgp_service, 0);
	if (error == CLUSTERNODE_INVAL)
		return -EINVAL;

	return error;
} /* end ssisys_cfs_get_secondary() */

/*****************************************************************************/

/*
 * CI_XXX: do these routines need a new home?
 */

#ifdef CONFIG_VPROC
/*
 * This routine is called to determine the execution node of a
 * process.  CLUSTERNODE_INVAL is returned if the process is
 * nolonger executing.
 */
clusternode_t
process_execnode(
	pid_t		pid)
{
	struct vproc	*vp;
	clusternode_t	node;
	int		error;

	might_sleep();

	if (pid == 0)
		return this_node;

	vp = LOCATE_VPROC_PID(pid, "temp proc_execnode");
	if (vp == NULL)
		return CLUSTERNODE_INVAL;

	error = VPOP_IS_ALIVE(vp, &node);

	VPROC_RELE(vp, "temp proc_execnode");

#ifdef VPROC_HOLD_ZERO_GET_TASK
	if (error == -ESRCH || (error & PF_EXITING))
#else
	if (error)
#endif
		return CLUSTERNODE_INVAL;

	return node;
}

/*
 * Interface routine called by various NSC services to determine
 * if a process is still alive.
 */
int
process_is_alive(
	pid_t	pid)
{
	return (process_execnode(pid) != CLUSTERNODE_INVAL);
}

#ifdef VPROC_IS_ALIVE
/*
 * Synonymous with process_is_alive() except in this case caller
 * already has pointer to virtual process in question.
 */
int
vproc_is_alive(
	struct vproc *vp)
{
#ifndef VPROC_HOLD_ZERO_GET_TASK
	if (vp->vp_pid == 0)
#else
	/* Linux pid_alive() semantics. */
	if (vp->vp_pid == 0 || PV_IS_ALIVE(PVP(vp)))
#endif
		return B_TRUE;

	return (VPOP_IS_ALIVE(vp, NULL) != -ESRCH);
}
#endif /* VPROC_IS_ALIVE */

#endif /* VPROC */

/*
 * clusternode_num():
 *
 *	Return the node number of the executing node
 *
 */
long
clusternode_num(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)
{
	(void)uaddrin;
	(void)uaddrin_len;
	(void)uaddrout;
	(void)uaddrout_len;
	(void)rvp;

	return this_node;
}

/*
 * cluster_membership():
 *
 *	Get node membership (only nodes that are in the CLUSTERNODE_UP state
 *	are returned in the array)
 */
long
cluster_membership(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)

{
	cls_membership_args_t args;
	clms_transition_t info;
	clusternode_t *node_array;
	int node, ind;
	int nodesz, numnodes;
	int ret=0;

	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(args) ||
	    uaddrout_len <= 0) {		/* must have out arg */
		return -EINVAL;
	}

	if (uaddrin_len != sizeof(args))
		memset(&args, 0, sizeof(args));
	if (copy_from_user((caddr_t)&args, uaddrin, uaddrin_len)) {
		return -EFAULT;
	}

	nodesz = (NSC_MAX_NODE_VALUE + 1) * sizeof(clusternode_t);
	node_array = (clusternode_t *)kzmalloc(nodesz, GFP_KERNEL);
	if (!node_array)
		return -ENOMEM;

	memset(&info, 0, sizeof(info));

#ifdef SSI_SKIP
	clms_api_user_block();
#endif

	for(node = 1, ind = 0; node <= NSC_MAX_NODE_VALUE; node++) {
		if (node == this_node ||
		    clms_api_get_state(node) == CLMS_STATE_UP) {
			node_array[ind] = node;
			ind++;
		}
	}
	numnodes = ind;
	/*
	 * Account for the terminating zero field.
	 */
	if (ind < NSC_MAX_NODE_VALUE)
		ind++;

	ret = clms_api_get_hist_ent(CLMS_WHERE_GET_LATEST, 0, &info);

#ifdef SSI_SKIP
	clms_api_user_unblock();
#endif

	if (ret != 0) {
#ifdef DEBUG
		printk(KERN_WARNING "cluster_membership: failed to get transid\n");
#endif
		ret = -EINVAL;
		goto out;
	}
	ret = -EINVAL;

	/*
	 * Need to see if arraylength is smaller than current max_array
	 * Return -EINVAL if UP nodes does not fit in the array passed in.
	 */
	if (ind > args.arraylen) {
#ifdef DEBUG
		printk(KERN_WARNING "cluster_membership: size too small %d\n",ind);
#endif
		goto out;
	}

	/*
	 * Copy out the member array and the last transid.
	 */
	if (copy_to_user(args.memberarray, (caddr_t)node_array,
						(ind * sizeof(clusternode_t)))) {
		goto out;
	}
	if (copy_to_user(uaddrout, (caddr_t)&(info.transid),
						sizeof(info.transid))) {
		goto out;
	}

	ret = 0;
out:
	kfree(node_array);

	/* return the number of UP nodes in the array */
	return ret ? : numnodes;
}


/*
 * cluster_get_api_state():
 *
 *	Maps the CLMS state to the CLUSTER API's state
 *
 * input:
 *	ulong clms_state 	- as defined in clms.h
 *
 * output:
 *	ulong 			- as defined in cluster.h
 */
ulong
cluster_get_api_state(ulong clms_state)
{
	switch(clms_state) {
		case CLMS_STATE_NEVERUP:
			return CLUSTERNODE_NEVERUP;
		case CLMS_STATE_COMINGUP:
			return CLUSTERNODE_COMINGUP;
		case CLMS_STATE_UP:
			return CLUSTERNODE_UP;
		case CLMS_STATE_SHUTDOWN:
			return CLUSTERNODE_SHUTDOWN;
		case CLMS_STATE_GOINGDOWN:
			return CLUSTERNODE_GOINGDOWN;
		case CLMS_STATE_KCLEANUP:
			return CLUSTERNODE_KCLEANUP;
		case CLMS_STATE_UCLEANUP:
			return CLUSTERNODE_UCLEANUP;
		case CLMS_STATE_DOWN:
			return CLUSTERNODE_DOWN;
		default:
			/* invalid state */
			return 0x0;
	}
}

/*
 * cluster_detailedtransition()
 *
 *	Get detailed cluster membership transition information
 *
 */
long
cluster_detailedtransition(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)
{
	cls_transition_args_t args;
	clms_transition_t info;
	cluster_transinfo_t outargs;
	ulong transid;
	int which;
	int ret;
	int rval1;
	int trans_size;

	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(args) ||
	    uaddrout_len == 0) {		/* must have out arg */
		return -EINVAL;
	}

	if (uaddrin_len != sizeof(args))
		memset(&args, 0, sizeof(args));
	if (copy_from_user((caddr_t)&args, uaddrin, uaddrin_len)) {
		return -EFAULT;
	}

	if (copy_from_user(&transid, args.transid, sizeof(transid_t)))
		return -EFAULT;
	which = args.which;
	trans_size = args.trans_len;
	rval1 = 1;

	/* If cluster_transinfo_t structure has changed use the
	 * smallest size to copy structure
	 */
	trans_size = min_t(int, trans_size, sizeof(outargs));
	memset(&outargs, 0, trans_size);

	memset(&info, 0, sizeof(info));

	/*
	 * Determine which transid to get.  Default will always get the
	 * next transid
	 */
	clms_api_user_block();

	switch (which) {
		case CLUSTER_TRANSWHICH_FIRST:
			ret = clms_api_get_hist_ent(CLMS_WHERE_GET_FIRST,
								0, &info);
			break;
		case CLUSTER_TRANSWHICH_THIS:
			ret = clms_api_get_hist_ent(CLMS_WHERE_GET_THIS,
							transid, &info);
			break;
		case CLUSTER_TRANSWHICH_LAST:
			ret = clms_api_get_hist_ent(CLMS_WHERE_GET_LATEST,
								0, &info);
			break;
		case CLUSTER_TRANSWHICH_PREVIOUS:
			ret = clms_api_get_hist_ent(CLMS_WHERE_GET_PREV,
							transid, &info);
			break;
		case CLUSTER_TRANSWHICH_NEXT:
			ret = clms_api_get_hist_ent(CLMS_WHERE_GET_NEXT,
							transid, &info);
			break;
		default:
			clms_api_user_unblock();
			return -EINVAL;
	}

	/*
	 * If reached end of log history, then return the latest
	 */
	if (ret == CLMS_HIST_GET_NONEXT) {
		ret = clms_api_get_hist_ent(CLMS_WHERE_GET_LATEST, 0, &info);
		rval1 = 0;
	}

	/*
	 * If reached beginning of log history, then return the
	 * first. Only works if the history has not wrapped.
	 */
	if (ret == CLMS_HIST_GET_NOPREV) {
		ret = clms_api_get_hist_ent(CLMS_WHERE_GET_FIRST, 0, &info);
		rval1 = 0;
	}

	clms_api_user_unblock();

	/*
	 * Copy out the transid
	 */
	switch (ret) {
		case CLMS_HIST_GET_FOUND:
			if (copy_to_user(args.transid, &info.transid,
							sizeof(transid_t)))
				return -EFAULT;
			outargs.transid = info.transid;
			outargs.transnode = info.node;
			outargs.tostate =
					cluster_get_api_state(info.transition);
			outargs.fromstate =
					cluster_get_api_state(info.oldstate);
			outargs.transtime.tv_sec = info.stamp.tv_sec;
			outargs.transtime.tv_usec = info.stamp.tv_usec;
			outargs.why = info.why;
			if (copy_to_user(uaddrout, (caddr_t)&outargs, trans_size)) {
				return -EFAULT;
			}
			else {
				ret = 0;
			}
			break;
		default:
			return -EINVAL;
	}
	if (ret)
		return ret;	/* Error set */
	else
		return rval1;	/* Indicate if more available */
}

/*
 * clusternode_info():
 *
 *	Get clusternode state information
 */
long
clusternode_info(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)
{
	cls_nodeinfo_args_t inargs;
	clusternode_info_t outargs;
	clms_api_state_t info;
	clusternode_t node;
	int error = 0;
	int online = 0;
	int cpus = 0;
	int status;
	int info_size;

	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len == 0) {		/* no out arg */
#ifdef  DEBUG
		printk(" Error in uaddrin_len or uaddrout_len\n");
#endif /* DEBUG */
		return -EINVAL;
	}

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len)) {
		return -EFAULT;
	}
	node = inargs.nodenum;
	if (node < 1 || node > NSC_MAX_NODE_VALUE)
		return -EINVAL;

	/* validate size of args */
	info_size = min_t(int, uaddrout_len, sizeof(outargs));
	info_size = min_t(int, info_size, inargs.info_len);

	memset(&outargs, 0, info_size);
	memset(&info, 0, sizeof(info));

	/* call clms to get info on node */
#ifdef SSI_SKIP
	clms_api_user_block();
#endif
	clms_api_get_full_state(node, &info);
#ifdef SSI_SKIP
	clms_api_user_unblock();
#endif

	outargs.node_num = node;

	/* copy info into clusternode_info structure */
	outargs.node_state = cluster_get_api_state(info.state);
	outargs.node_previous_state = cluster_get_api_state(info.prev_state);
	outargs.node_lasttransid = info.transid;
	outargs.node_transwhy = info.why;
	outargs.node_lasttranstime.tv_sec = info.stamp.tv_sec;
	outargs.node_lasttranstime.tv_usec = info.stamp.tv_usec;
	outargs.node_firsttransid = info.first_transid;
	outargs.node_firsttranstime.tv_sec = info.first_stamp.tv_sec;
	outargs.node_firsttranstime.tv_usec = info.first_stamp.tv_usec;


	/*
	 * Get information about the CPU's
	 * This should be function ship to the specified node
	 */
	if (this_node == node) {
		cpus =  num_possible_cpus();
		online = num_online_cpus();
	} else {
		error = RCLUSTER_API_GETCPUINFO(node, &status,
						this_node, &cpus, &online);

		if (error == -EREMOTE) {
			cpus = online = 0;
			error = 0;
		}
		SSI_ASSERT(error == 0);
	}
	outargs.node_totalcpus = cpus;
	outargs.node_onlinecpus = online;

	/*
	 * CI_XXX:
	 *
	 *	These two fields are not currently supported.
	 */
	outargs.node_cpupower = 0;
	outargs.node_totalmemory = 0;

	if (copy_to_user(uaddrout, (caddr_t)&outargs, info_size)) {
		return -EFAULT;
	}

	return 0;

}

/*
 * clusternode_setinfo()
 *
 * 	Set clusternode information
 *		- currently only node_state can be set
 */
long
clusternode_setinfo(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)
{
	cls_setinfo_args_t args;
	int ret = 0;
	int status = 0;
	int new_state;
	int go_remote;
	int current_state;
	clusternode_info_t *myinfo;

	(void) uaddrout;
	(void) rvp;

	/* check for permissions */
	if (!capable(CAP_SYS_ADMIN))
		return -EPERM;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(args) ||
	    uaddrout_len != 0) {		/* no out arg */
		return -EINVAL;
	}

	if (uaddrin_len != sizeof(args))
		memset(&args, 0, sizeof(args));
	if (copy_from_user((caddr_t)&args, uaddrin, uaddrin_len))
		return -EFAULT;

	/* check action is to change node_state */
	if (args.action != CLUSTERNODE_SETSTATE)
		return -EINVAL;

	/* validate size of args */
	if (args.info_len < 0 || args.info_len > sizeof(*args.info))
		return -EINVAL;

	(void) (&args.info == &myinfo); /* type check */

	myinfo = kzmalloc(sizeof(*myinfo), GFP_KERNEL);
	if (!myinfo)
		return -ENOMEM;

	if (copy_from_user(myinfo, args.info, args.info_len)) {
		ret = -EFAULT;
		goto out;
	}

	ret = -EINVAL;
	/* validate node and current state */
	if (args.node < 1 || args.node > NSC_MAX_NODE_VALUE)
		goto out;

#ifdef SSI_SKIP
	clms_api_user_block();
#endif
	current_state = cluster_get_api_state(clms_api_get_state(args.node));
#ifdef SSI_SKIP
	clms_api_user_unblock();
#endif
	if (current_state == CLUSTERNODE_NEVERUP ||
	    current_state == CLUSTERNODE_DOWN ||
	    current_state == CLUSTERNODE_KCLEANUP)
		goto out;

	/*
	 * Map new state to CLMS states.  While we're at it, notice invalid
	 * states for this interface.
	 */
	go_remote = 1;
	if (myinfo->node_state == CLUSTERNODE_NEVERUP ||
	    myinfo->node_state == CLUSTERNODE_COMINGUP ||
	    myinfo->node_state == CLUSTERNODE_UCLEANUP)
		goto out;

	switch(myinfo->node_state) {
		case CLUSTERNODE_UP:
			new_state = CLMS_STATE_UP;
			break;
		case CLUSTERNODE_SHUTDOWN:
			new_state = CLMS_STATE_SHUTDOWN;
			break;
		case CLUSTERNODE_GOINGDOWN:
			new_state = CLMS_STATE_GOINGDOWN;
			break;
		case CLUSTERNODE_KCLEANUP:
			new_state = CLMS_STATE_KCLEANUP;
			go_remote = 0;
			break;
		case CLUSTERNODE_DOWN:
			new_state = CLMS_STATE_DOWN;
			go_remote = 0;
			break;
		default:
			/* invalid state change */
			goto out;
	}
	/* if node is remote, then function ship call to node */
	if (args.node == this_node || go_remote == 0) {
		ret = rcluster_api_setstate(args.node, &status,
						this_node, new_state);
	}
	else {
		ret = RCLUSTER_API_SETSTATE(args.node, &status, this_node,
						new_state);
	}

	if (ret == 0)
		ret = status;
out:
	kfree(myinfo);
	return ret;
}

int
rcluster_api_setstate(clusternode_t node,
			 int *ret,
			 clusternode_t from_node,
			 int new_state)
{
	int error;

	error = clms_declare_node_new_state(node, new_state);

	if (error)
		*ret = error;

	return 0;

}

int
rcluster_api_getcpuinfo(clusternode_t 	node,
			int	*rval,
			clusternode_t 	my_node,
			int	*num_cpus,
			int 	*onlinecpus)
{
	*num_cpus   =  num_possible_cpus();
	*onlinecpus =  num_online_cpus();
	return 0;
}

/*
 * cluster_maxnodes()
 *
 *	Return maximum number of nodes a cluster can have
 */
long
cluster_maxnodes(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)
{
	(void) uaddrin;
	(void) uaddrin_len;
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	if (cluster_state >= CLUSTER_STATE_PREROOT)
		return NSC_MAX_NODE_VALUE;
	
	/* Say we aren't an SSI kernel for apps calling cluster_ssiconfig
	 * without kernel messages.
	 */
	return -EAGAIN;
}

/*
 * clusternode_avail()
 *
 *	Returns 1 if node passed in is UP
 *	Returns 0 if node passed in is not UP
 */
long
clusternode_avail(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)
{
	clusternode_t nodenum;
	int	state;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(clusternode_t) ||
	    uaddrout_len != 0) {		/* no out arg */
		return -EINVAL;
	}

	nodenum = 0;
	if (copy_from_user((caddr_t)&nodenum, uaddrin, uaddrin_len)) {
		return -EFAULT;
	}
	if (nodenum < 1 || nodenum > NSC_MAX_NODE_VALUE)
		return -EINVAL;
	if (nodenum == this_node)
		return 1;
	state = clms_api_get_state(nodenum);
	return (cluster_get_api_state(state) == CLUSTERNODE_UP);
#ifdef SSI_SKIP
/* Keep this for history */
	clms_api_user_block();
	state = clms_api_get_state(nodenum);
	clms_api_user_unblock();
	if (nodenum == this_node ||
	    cluster_get_api_state(state) == CLUSTERNODE_UP)
		return 1;
	else
		return 0;
#endif
}

/*
 * Set/clear memory priority.
 *	- returns 1 if caller is critical, 0 otherwise.
 */
long
ssisys_set_memprio(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t*			rvp)
{
	int	flags;

	(void) uaddrout;
	(void) rvp;

	/* Only init is allowed call this routine */
	if (current->pid != 1)
		return -ENOSYS;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(int) ||
	    uaddrout_len != 0) {		/* no out arg */
		return -EINVAL;
	}

	flags = 0;
	if (copy_from_user((caddr_t)&flags, uaddrin, uaddrin_len)) {
		return -EFAULT;
	}

	if ((flags & (NSC_MEMPRIO_SETCRIT|NSC_MEMPRIO_CLRCRIT)) ==
	    (NSC_MEMPRIO_SETCRIT|NSC_MEMPRIO_CLRCRIT))
		return -EINVAL;

	if (flags & NSC_MEMPRIO_SETCRIT) {
#ifdef DEBUG
		printk(KERN_NOTICE "Setting critical memory priority for pid %d\n", current->pid ? current->pid : -1);
#endif /* DEBUG */
		/* CI_XXX: Set critical flag on process */
	} else if (flags & NSC_MEMPRIO_CLRCRIT) {
#ifdef DEBUG
		printk(KERN_NOTICE "Clearing critical memory priority for pid %d\n", current->pid ? current->pid : -1);
#endif /* DEBUG */
		/* CI_XXX: Clear critical flag on process */
	} else
		return -EINVAL;

	/* CI_XXX: *rvp = critical flag from process */;
	return 0;
}

/*
 * Stolen from common_nsc/subsys/license/i4/iforpm/includes/licenseIDs.h
 */
#define LIC_ID_NSC	163

/*
 * ssisys_get_node_counts
 *	Returns the current and maximum number of nodes in the cluster.
 */
long
ssisys_get_node_counts(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	struct cluster_nodes	nodes;

	(void) uaddrin;
	(void) uaddrin_len;
	(void) rvp;

	if (uaddrout_len != sizeof(struct cluster_nodes))
		return -EINVAL;
	nodes.max = 0;
	/* NSC can always boot 1 node even with no licenses */
	if (nodes.max == 0)
		nodes.max = 1;
	nodes.cur = clms_get_cluster_num_nodes();
	if (copy_to_user(uaddrout, (caddr_t)&nodes, sizeof(struct cluster_nodes)) != 0)
		return -EFAULT;
	return 0;
}

/*
 *	ssisys_force_root_node()
 *
 *	_cluster_force_root_node(clusternode_t node)
 *
 *		Force the other root node to become CLMS master in a
 *		failover configuration. (Needed for CNM when it recognizes
 *		it is booted off the wrong plex.) node is currently not
 *		needed, but may be in the future if we ever have more than
 *		two root nodes. It must be either CLUSTERNODE_INVAL or the
 *		node number of the other root node.
 */

long
ssisys_force_root_node(caddr_t uaddrin,
		       u_int uaddrin_len,
		       caddr_t uaddrout,
		       u_int uaddrout_len,
		       rval_t *rvp)
{
	clusternode_t node;
	int error;

	(void) uaddrin;
	(void) uaddrout_len;
	(void) rvp;

	if (!capable(CAP_SYS_ADMIN)) {
		error = -EPERM;
		goto out;
	}

	if (uaddrin_len != sizeof(clusternode_t) ||
	    uaddrout_len != 0) {		/* no out arg */
		error = -EINVAL;
		goto out;
	}

	if (copy_from_user((caddr_t)&node, uaddrin, uaddrin_len)) {
		error = -EFAULT;
		goto out;
	}

	error = clms_force_root_node(node);
out:
	return error;
}


/*
 * ssisys_get_transport()
 *	Returns the type of transport being used in this kernel.
 */
long
ssisys_get_transport(
	caddr_t 		uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	(void) uaddrin_len;
	(void) uaddrout_len;
	(void) rvp;

	if (uaddrin_len != 0 ||			/* no in arg */
	    uaddrout_len != 0)			/* no out arg */
		return -EINVAL;
	return ics_get_transport();
}

long
ssisys_cluster_set_config(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	char *oline;
	char *line;
	int i;
	char ch;
	clusternode_t nodenum;

	(void) uaddrout;
	(void) rvp;

	if (!uaddrin_len || uaddrin_len >= PAGE_SIZE || uaddrout_len)
		return -EINVAL;

	line = (char *) __get_free_page(GFP_KERNEL);
	if (!line)
		return -ENOMEM;
	memset(line, 0, PAGE_SIZE);

	if (copy_from_user(line, uaddrin, uaddrin_len) != 0) {
		free_page((unsigned long)line);
		return -EFAULT;
	}
	oline = line;

	/* SSI_XXX: This is going away in a future release */
	if (unlikely(!strncmp(line, "IFCONFIG=", 9))) {
		printk(KERN_WARNING "Use of deprecated IFCONFIG= SSI param\n");
		line += 9;
		/* If previously set, kfree it and re-malloc */
		if (cluster_ifconfig)
			kfree(cluster_ifconfig);
		cluster_ifconfig = kzmalloc_nofail(CLUSTER_IFCONFIG_SZ);
		for (i = 0; i < CLUSTER_IFCONFIG_SZ; i++) {
			ch = line[i];
			if ( isspace(ch) || (ch == '\0') ) break;
			cluster_ifconfig[i] = ch;
		}
#ifdef DEBUG
		printk("cluster_ifconfig: %s\n", cluster_ifconfig);
#endif /* DEBUG */
		goto out;
	}

	if (!strncmp(line, "INTERFACES=", 11)) {
		line += 11;
		/* If previously set, kfree it and re-malloc */
		if (cluster_interfaces)
			kfree(cluster_interfaces);
		cluster_interfaces = kzmalloc_nofail(CLUSTER_IFCONFIG_SZ);
		for (i = 0; i < CLUSTER_IFCONFIG_SZ; i++) {
			ch = line[i];
			if ( isspace(ch) || (ch == '\0') ) break;
			cluster_interfaces[i] = ch;
		}
#ifdef DEBUG
		printk("cluster_interfaces: %s\n", cluster_interfaces);
#endif /* DEBUG */
		goto out;
	}

	if (!strncmp(line, "CLUSTER_MASTER=", 15)) {
		line += 15;
		/* If previously set, kfree it and re-malloc */
		if (cluster_clmsinfo)
			kfree(cluster_clmsinfo);
		cluster_clmsinfo = kzmalloc_nofail(CLUSTER_CLMSINFO_SZ);
		for (i = 0; i < CLUSTER_CLMSINFO_SZ - 1; i++) {
			ch = line[i];
			if ( isspace(ch) || (ch == '\0') ) break;
			cluster_clmsinfo[i] = ch;
		}
#ifdef DEBUG
		printk("cluster_clmsinfo: %s\n", cluster_clmsinfo);
#endif /* DEBUG */
		goto out;
	}

	if (!strncmp(line, "CLUSTER_NODENUM=", 16)) {
		line += 16;
		nodenum = simple_strtoul(line, NULL, 10);
		if (this_node == nodenum)
			goto out;
		if (this_node == CLUSTERNODE_INVAL) {
			this_node = nodenum;
#ifdef DEBUG
			printk("this_node: %d\n", (int)this_node);
#endif /* DEBUG */
#ifdef CONFIG_VPROC_ND
			vproc_nodedown_init();
#endif /* VPROC_ND */
#ifdef CONFIG_VPROC
			rvp_init();
#endif /* VPROC */
			goto out;
		}
	}

	/* SSI_XXX: This is going away in a future release */
	/* Ignore parameter without error since we have no network set-up */
	if (!strncmp(line, "ICS_ROUTE=", 10)) {
		goto out;
	}

	free_page((unsigned long)oline);
	return -EINVAL;

out:
	free_page((unsigned long)oline);
	if ((cluster_interfaces != NULL || cluster_ifconfig != NULL) &&
	    cluster_clmsinfo != NULL && this_node != CLUSTERNODE_INVAL) {
		cluster_state = CLUSTER_STATE_CONFIG;
		return 1;
	}
	return 0;
}

long
ssisys_cluster_init_preroot(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	int error;
	extern int cluster_main_init_preroot(void);

	(void) uaddrin;
	(void) uaddrin_len;
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	error = cluster_main_init_preroot();
	if (!error)
		cluster_state = CLUSTER_STATE_PREROOT;
	return error;
}

long
ssisys_cluster_init_postroot(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	int error;
	extern int cluster_main_init_postroot(void);

	(void) uaddrin;
	(void) uaddrin_len;
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	cluster_state = CLUSTER_STATE_POSTROOT;
	error = cluster_main_init_postroot();
	if (error)
		cluster_state = CLUSTER_STATE_PREROOT;
	return error;
}

#ifdef CONFIG_VPROC
long
ssisys_cluster_initproc(
	caddr_t			uaddrin,
	u_int			uaddrin_len,
	caddr_t			uaddrout,
	u_int			uaddrout_len,
	rval_t			*rvp)
{
	(void) uaddrin;
	(void) uaddrin_len;
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	cluster_state = CLUSTER_STATE_POSTINIT;
	/* Never returns. */
	initproc_postroot_init();
	return 0;
}
#endif /* VPROC */

#if defined(CONFIG_SSI)
long
ssisys_mount_remote_root(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
#ifndef CONFIG_CFS
	(void) uaddrin;
	return -EINVAL;
#else /* CONFIG_CFS */
	char *path;
	int error;
	/* SSI_XXX: Assume init_node is the primary root node too */
	clusternode_t server = init_node;

	(void) uaddrin_len;
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	if (this_node == server)
		return -ENODEV;

	path = getname(uaddrin);
	if (IS_ERR(path))
		return PTR_ERR(path);

	error = cfs_mount_root(path, server);
	if (!error)
		root_fs_node = server;
	putname(path);
	return error;
#endif /* CONFIG_CFS */
}


long
do_discover_mounts(const char *mntpt, int donfs)
{
	int error = 0, status;
	int cookie = 1;		/* Skip the root */
	extern clusternode_t init_node, this_node;
	int dir_name_len, payload_len, type_len, dev_name_len;
	char *dir_name, *payload, *type, *dev_name;
	int flags;

	if (this_node == init_node)
		return -EINVAL;

	if (!(dir_name = (char *)__get_free_page(GFP_KERNEL)))
		return -ENOMEM;

	if (!(payload = (char *)__get_free_pages(GFP_KERNEL, 1))) {
		free_page((unsigned long)dir_name);
		return -ENOMEM;
	}

	if (!(type = (char *)__get_free_page(GFP_KERNEL))) {
		free_page((unsigned long)dir_name);
		free_pages((unsigned long)payload, 1);
		return -ENOMEM;
	}

	if (!(dev_name = (char *)__get_free_page(GFP_KERNEL))) {
		free_page((unsigned long)dir_name);
		free_pages((unsigned long)payload, 1);
		free_page((unsigned long)type);
		return -ENOMEM;
	}

	ssi_down_global_mount_sem();

	while(cookie != -ESRCH) {
		dir_name_len = PAGE_SIZE;
		payload_len = 2 * PAGE_SIZE;
		type_len = PAGE_SIZE;
		dev_name_len = PAGE_SIZE;

		error = RCLUSTER_GET_MOUNT(init_node, &status, this_node, &cookie, &dir_name, &dir_name_len, &type, &type_len, &flags, &payload, &payload_len, &dev_name, &dev_name_len);
		if (error == 0)
			error = status;
		if (error == -EREMOTE) {
			printk(KERN_ERR "Communication error during mount discovery\n");
			BUG();
			break;
		}

		/* For now assume other errors mean that this mount doesn't get
		 * distributed.
		 */
		if (error < 0)
			break;

		if (mntpt != NULL && strcmp(mntpt, dir_name))
			continue;
		/* Do everything but nfs if !donfs */
		if (!donfs && strcmp(type, "nfs") == 0)
			continue;
		/* Do nfs only if donfs */
		if (donfs && strcmp(type, "nfs") != 0)
#ifndef CLMS_CLIENT_PERFORM_NODEUP_DISCOVER_MOUNTS
			continue;
#else
			if (!(flags & MS_UDEV))
				continue;
		/* Skip non-init nodes before clms_startup_node(). 
		 * Otherwise do_mount() will wait indefinitely.
		 *	-Roger
		 */
		/* SSI_XXX: only udev? */
		if (!donfs && (flags & MS_UDEV)) {
			char mntpt[32];

			mntpt[sizeof(mntpt)-1] = '\0';
			snprintf(mntpt, sizeof(mntpt) - 1,
				 "/cluster/node%u/dev", init_node);
			if (strcmp(dir_name, mntpt))
				continue;
		}
#endif
		flags |= MS_REMOTE;
#ifdef DEBUG
		printk(KERN_INFO "do_discover_mounts: mounting %s\n", dir_name);
#endif
		if (dev_name_len > 0)
			error =  do_mount(dev_name, dir_name, type, flags, payload);
		else
			error =  do_mount(NULL, dir_name, type, flags, payload);
		if (mntpt != NULL)
			goto out;
		if (error != 0) {
#ifdef CLMS_CLIENT_PERFORM_NODEUP_DISCOVER_MOUNTS_NOTYET
		  if (donfs != this_node) /* else this node in POSTROOT */
#endif
			printk(KERN_ERR "Discovery of %s error %d\n", dir_name,
				error);
			/* SSI_XXX: Discovery is performed multiple times,
			 * ignore errors.  Assume it will be eventually done.
			 */
			error = 0;
		}
#ifdef DEBUG
		printk(KERN_INFO "do_discover_mounts: mounted %s\n", dir_name);
#endif
	}

	/* Processed all the mounts */
	if (error == -ESRCH)
		error = 0;

	if (error) {
		printk("cfs_discover_mounts error %d\n", error);
		BUG();
	}
 out:
	ssi_up_global_mount_sem();
	free_page((unsigned long)dir_name);
	free_pages((unsigned long)payload, 1);
	free_page((unsigned long)type);
	free_page((unsigned long)dev_name);

	return error;
}

long
ssisys_discover_mounts(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	int			donfs;
	long			ret = 0;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len != sizeof(int) ||
	    uaddrout_len != 0) {
		ret = -EINVAL;
		goto out;
	}

	if (copy_from_user((caddr_t)&donfs, uaddrin, uaddrin_len)) {
		ret = -EFAULT;
		goto out;
	}

	(void)do_discover_mounts(NULL, donfs);
out:
	return 0;
}

int
rcluster_get_mount(
	clusternode_t node,
	int *rval,
	int my_node,
	int *cookie,
	char **dir_name,
	int *dir_name_len,
	char **type,
	int *type_len,
	int *flags,
	char **payload,
	int *payload_len,
	char **dev_name,
	int *dev_name_len)
{
	struct vfsmount *mnt = NULL, *rootmnt = NULL;
	char *path_buf = NULL, *path;
	int n = *cookie;
	int newcookie = n + 1;
	int error;

#ifdef CONFIG_VPROC
	if (cluster_state != CLUSTER_STATE_POSTROOT &&
	    cluster_state != CLUSTER_STATE_POSTINIT)
#else
	if (clusterstate != CLUSTER_STATE_POSTROOT)
#endif
	{
		error = -EINVAL;
		goto out;
	}

	down_read(&current->namespace->sem);

	read_lock(&current->fs->lock);
	rootmnt = mntget(current->fs->rootmnt);
	read_unlock(&current->fs->lock);

	spin_lock(&vfsmount_lock);
	for (mnt = rootmnt ; mnt ; mnt = next_mnt(mnt, rootmnt)) {
		if (n--)
			continue;
		if (mnt->mnt_uniqueid == 0 || mnt->mnt_ops == NULL ||
		     mnt->mnt_ops->get_mount == NULL) {
#ifdef DEBUG
			printk(KERN_INFO "Skipping vfsmount %p\n", mnt);
#endif
			newcookie++;
			n = 0;	/* Force loop to look at next */
			continue;
		}
		mntget(mnt);
		break;
	}
	spin_unlock(&vfsmount_lock);
	up_read(&current->namespace->sem);
	mntput(rootmnt);
	if (!mnt) {
		error = -ESRCH;
		goto out;
	}

	path_buf = (char *) __get_free_page(GFP_KERNEL);
	if (!path_buf) {
		error = -ENOMEM;
		goto out;
	}
	path = d_path(mnt->mnt_root, mnt, path_buf, PAGE_SIZE);
	if (IS_ERR(path)) {
		error = PTR_ERR(path);
		goto out;
	}

	/* Process response */
#ifdef DEBUG
	printk(KERN_INFO "Discovery of mount %s\n", path);
#endif
	error = (*(mnt->mnt_ops->get_mount))(mnt, my_node, type, type_len,
						flags, payload, payload_len);
	if (error) {
		SSI_ASSERT(type == NULL);
		SSI_ASSERT(payload == NULL);
		printk(KERN_ERR "ERROR %d on mount %s\n", error, path);
		goto out;
	}

	*dir_name_len = strlen(path) + 1;
	*dir_name = kmalloc((*dir_name_len), GFP_KERNEL);
	if (!(*dir_name)) {
		error = -ENOMEM;
		goto out;
	}

	memcpy((*dir_name), path, (*dir_name_len));

	if (mnt->mnt_devname == NULL) {
		*dev_name_len = 0;
		*dev_name = NULL;
		goto skip_dev;
	}

#ifdef CONFIG_CFS
	if (strcmp(*type, "cfs") == 0)
		*dev_name_len = 9;
	else
#endif
		*dev_name_len = strlen(mnt->mnt_devname) + 1;

	*dev_name = kmalloc((*dev_name_len), GFP_KERNEL);
	if (!(*dev_name)) {
		kfree(dir_name);
		error = -ENOMEM;
		goto out;
	}

#ifdef CONFIG_CFS
	if (strcmp(*type, "cfs") == 0)
		sprintf(*dev_name, "%8.8x",
			mnt->mnt_sb->s_ssidev);
	else
#endif
		memcpy((*dev_name), mnt->mnt_devname,
			 (*dev_name_len));

skip_dev:
	*cookie = newcookie;

out:
	mntput(mnt);
	if (path_buf)
		free_page((unsigned long)path_buf);
	if (error) {
		*dir_name_len = 0;
		*dir_name = NULL;
		*type_len = 0;
		if (*type) {
			kfree(*type);
			*type = NULL;
		}
		*payload_len = 0;
		if (*payload) {
			kfree(*payload);
			*payload = NULL;
		}
		*dev_name_len = 0;
		*dev_name = NULL;
	}
	*rval = error;

	return 0;
}

long
ssisys_set_node_context(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	long			ret = 0;
	clusternode_t		newnode;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len != sizeof(clusternode_t) ||
	    uaddrout_len != 0) {
		ret = -EINVAL;
		goto out;
	}

	if (copy_from_user((caddr_t)&newnode, uaddrin, uaddrin_len)) {
		ret = -EFAULT;
		goto out;
	}

	if (newnode == CLUSTERNODE_THIS)
		newnode = this_node;

	ret = current->node_context;
	if (newnode != 0) {
		if (newnode == this_node || clms_isnodeup(newnode) ||
		    ((PVP(current->p_vproc)->pvp_flag & PV_IGNORE_HALFUP) &&
		     (clms_ishalfup(newnode) || clms_ishalfdown(newnode))))
			current->node_context = newnode;
		else {
			ret = -EINVAL;
			goto out;
		}
	}

out:
	return ret;
}

static void *ssi_nodename = NULL;

long
ssisys_set_nodename(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	if (!capable(CAP_SYS_ADMIN))
		return -EPERM;

	/* Make sure that hostname isn't too big. */
	if (uaddrin_len > sizeof(system_utsname.nodename))
		return -EINVAL;
	if (uaddrin_len == 0)
		return -EINVAL;

	if (ssi_nodename)
		kfree(ssi_nodename);
	ssi_nodename = kzmalloc(uaddrin_len, GFP_KERNEL);
	if (!ssi_nodename)
		return -ENOMEM;

	if (copy_from_user(ssi_nodename, uaddrin, uaddrin_len) != 0)
		return -EFAULT;

	return 0;
}

long
ssisys_get_nodename(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	(void) uaddrin;
	(void) uaddrin_len;
	(void) rvp;

	if (ssi_nodename == NULL)
		return -ENOENT;

	if (uaddrout_len < strlen(ssi_nodename)+1) {
		return -EINVAL;
	}

	if (copy_to_user(uaddrout, (caddr_t) ssi_nodename, strlen(ssi_nodename)+1) != 0) {
		return -EFAULT;
	}
	return 0;
}

static void *ssi_clustername = NULL;

long
ssisys_set_clustername(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	(void) uaddrout;
	(void) uaddrout_len;
	(void) rvp;

	if (!capable(CAP_SYS_ADMIN))
		return -EPERM;

	/* Make sure that hostname isn't too big. */
	if (uaddrin_len > sizeof(system_utsname.nodename))
		return -EINVAL;
	if (uaddrin_len == 0)
		return -EINVAL;

	if (ssi_clustername)
		kfree(ssi_clustername);
	ssi_clustername = kzmalloc(uaddrin_len, GFP_KERNEL);
	if (!ssi_clustername)
		return -ENOMEM;

	if (copy_from_user(ssi_clustername, uaddrin, uaddrin_len) != 0)
		return -EFAULT;

	return 0;
}

long
ssisys_get_clustername(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	(void) uaddrin;
	(void) uaddrin_len;
	(void) rvp;

	if (ssi_clustername == NULL)
		return -ENOENT;

	if (uaddrout_len < strlen(ssi_clustername)+1) {
		return -EINVAL;
	}

	if (copy_to_user(uaddrout, (caddr_t) ssi_clustername, strlen(ssi_clustername)+1) != 0) {
		return -EFAULT;
	}
	return 0;
}
#endif /* CONFIG_SSI */

#ifdef CONFIG_CFS
static unsigned long
cfs_sb_convert_flags(unsigned long syscall_flags)
{
	unsigned long ms_flags = 0;	/* Returned flags. */

	/* Get special file system mount semantics */
	if (syscall_flags & CFS_CHARD)
		ms_flags |= MS_CHARD;

	return ms_flags;
}

static long
ssisys_cfs_mount(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	cfs_mount_args_t	inargs;
	int			error = 0;
	unsigned long flags;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0) {		/* no out arg */
		error = -EINVAL;
		goto out;
	}

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));

	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len)) {
		error = -EFAULT;
		goto out;
	}

	flags = inargs.flags;
	flags |= cfs_sb_convert_flags(inargs.cfs_flags);
	flags |= MS_CFS;

	error = sys_mount(inargs.dev_name, inargs.dir_name, inargs.type,
			  flags, inargs.data);
	if (!error && root_fs_node == CLUSTERNODE_INVAL)
		root_fs_node = this_node;

out:
	return error;
}

static long
ssisys_cfs_remount(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	cfs_remount_args_t	inargs;
	int			error = 0;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0) {		/* no out arg */
		error = -EINVAL;
		goto out;
	}

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));

	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len)) {
		error = -EFAULT;
		goto out;
	}

	error = cfs_remount(inargs.dev_name, inargs.ssidev, inargs.type,
			    inargs.flags, inargs.data);
out:
	return error;
}

/* protect root_fs, root_fs_type, root_dev_name, root_bdev */
DECLARE_MUTEX(root_fs_sem);
char *root_fs = NULL;
struct file_system_type *root_fs_type = NULL;
char *root_dev_name = NULL;
struct block_device *root_bdev = NULL;

/* Why not in fs.h? */
extern void put_filesystem(struct file_system_type *);

static long
ssisys_cfs_setroot(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	unsigned long type_page;
	unsigned long dev_page;
	cfs_setroot_args_t	inargs;
	int			error = 0;
	struct file_system_type*	fstype;

	(void) uaddrout;
	(void) rvp;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0) {		/* no out arg */
		return -EINVAL;
	}

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));

	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	error = copy_mount_options (inargs.type, &type_page);
	if (error < 0)
		return error;

	error = copy_mount_options (inargs.dev_name, &dev_page);
	if (error < 0)
		goto out2;

	down(&root_fs_sem);

	root_fs = (char *)type_page;
	fstype = get_fs_type (root_fs);

	if (!fstype) {
		error = -ENOENT;
		goto out1;
	}

	if (root_fs_type) put_filesystem (root_fs_type);

	root_fs_type = fstype;

	root_dev_name = (char *)dev_page;

	if (root_dev_name && !memchr(root_dev_name, 0, PAGE_SIZE)) {
		error = -EINVAL;
		goto out1;
	}

	root_bdev = lookup_bdev(root_dev_name);
	if (IS_ERR(root_bdev))
		error = PTR_ERR(root_bdev);

out1:
	if (error)
		free_page(dev_page);

out2:
	if (error) {
		free_page(type_page);

		if (root_fs_type) {
			put_filesystem (root_fs_type);
			root_fs_type = NULL;
		}
		root_fs = NULL;
		root_dev_name = NULL;
		root_bdev = NULL;
	}
	up(&root_fs_sem);

	return error;
}
#endif /* CONFIG_CFS */

#if defined(CONFIG_IP_VS ) && defined (CONFIG_SSI)
long
ssisys_lvs(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	struct ipvs_op inargs;
	int error = 0 ;

	(void) uaddrout;
	(void) rvp;

	if (!capable(CAP_SYS_ADMIN))
		return -EPERM;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0) {		/* no out arg */
		error = -EINVAL;
		goto out;
	}

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));

	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	switch (inargs.type) {
		case SET_MASTER_DIRECTOR_NODE:
			return set_master_director(inargs.daddr, inargs.dnode);
		case SET_POSSIBLE_DIRECTOR_NODE:
			return set_pos_director(inargs.daddr, inargs.dnode);
		case DEL_CVIP:
			return del_cvip(inargs.daddr);
		case DEL_POSSIBLE_DIRECTORS:
			return del_pos_directors(inargs.daddr);
	}
	error = -EINVAL;

out:
	return error;

}

static long
ssisys_set_ipvsportweight(
	caddr_t                 uaddrin,
	u_int                   uaddrin_len,
	caddr_t                 uaddrout,
	u_int                   uaddrout_len,
	rval_t*                 rvp)
{
	struct portweight {
		u_short type;
		u_short start_port;
		u_short end_port;
		int weight;
		char lvs_sched[IP_VS_SCHEDNAME_MAXLEN];
	} inargs;
	int error;

	extern long update_ipvs_portweight_list(u_short, u_short, int, u_short, char *);
	extern void register_ports(u_short, u_short, int, u_short, char *);

	(void) uaddrout;
	(void) rvp;

	if (!capable(CAP_NET_ADMIN))
		return -EPERM;

	if (uaddrin_len <= 0 ||
	    uaddrin_len > sizeof(inargs) ||
	    uaddrout_len != 0)		/* no out arg */
		return -EINVAL;

	if (uaddrin_len != sizeof(inargs))
		memset(&inargs, 0, sizeof(inargs));
	if (copy_from_user((caddr_t)&inargs, uaddrin, uaddrin_len))
		return -EFAULT;

	error = update_ipvs_portweight_list(inargs.start_port,
						inargs.end_port,
						inargs.weight,
						inargs.type, inargs.lvs_sched);
#ifndef IPVS_SERVICE_RACE_FIX
	/*
	 * Register the already listening ports to the 
	 * director node if weight is non zero
	 */
	if (inargs.weight)
#endif
#ifdef IPVS_REGISTER_PORTS_FIX
	/* We still want to quiesce the real server if weight is zero */
	if (!error)
#endif
		register_ports(inargs.start_port, inargs.end_port,
				inargs.weight, inargs.type, inargs.lvs_sched);

	return error;
}
#endif
