#include <linux/sched.h>
#include <linux/timer.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/inet.h>
#include <net/sock.h>
#include <net/ip.h>
#include <asm/uaccess.h>
#include <asm/checksum.h>

#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/icsgen.h>
#include <cluster/ics_proto.h>
#include <cluster/ics/ics_tcp_sock_private.h>
#include <cluster/nodelist.h>
#include <cluster/node_monitor.h>
#ifdef CONFIG_LDLVL
#include <cluster/ssi/load_level.h>
#endif
#ifdef CONFIG_MOSIX_LL
#include <cluster/ssi/mosixll/defs.h>
#include <cluster/ssi/mosixll/routines.h>
#endif

/*
 * Node monitor is always running; however setting nm_disabled will prevent
 * it from recognizing a node has gone down. Clearing it will allow nodedown
 * processing to take place.
 */
int nm_disabled;
static int nm_timeout;
static int nm_max_probes;
static int nm_log_threshold;
static int nm_probe_interval;
static int nm_log_interval;

static void nm_nodedown_daemon(void *);
static void nm_send_daemon(void *);
static void nm_log_daemon(void *);
static void nm_kick_send_daemon(unsigned long);

/* Node Monitor variables. */

static int nm_inited;				/* Daemons inited */
static int nm_nodes;				/* Number of nodes */
static short nm_node_state[NSC_MAX_NODE_VALUE + 1];
						/* Maintains timeout
						 * count for each node.
						 */
static short nm_log_state[NSC_MAX_NODE_VALUE + 1];
						/* Maintains number of
						 * missed packets.
						 */
static nsc_nodelist_t nm_monitor_nodelist;	/* List of nodes we're
						 * monitoring.
						 */
static nsc_nodelist_t nm_down_nodelist;		/* List of nodes that need
						 * to be declared down.
						 */
static nsc_nodelist_t nm_warned_nodelist;	/* List of nodes
						 * that would have gone down.
						 */
static nsc_nodelist_t nm_warn_nodelist;		/* List of nodes we need
						 * to warn about.
						 */
static nsc_nodelist_t nm_log_nodelist;		/* List of nodes
						 * that have log info.
						 */
static __cacheline_aligned_in_smp DEFINE_SPINLOCK(nm_lock); /* All-purpose spinlock to
						 * protect critical sections
						 * of Node Monitor code.
						 */
static int nm_log_pending;			/* Logging pending. */
static unsigned long nm_log_nextlog;		/* Jiffies after which
						 * next log run takes
						 * place.
						 */
#ifdef CONFIG_LDLVL
static unsigned long next_ics_unack_load = INITIAL_JIFFIES; /* Jiffies after
						 * which ICS_UNACK_LOAD flag
						 * is set. And load array is
						 * sent.
						 */
#endif

static struct timer_list nm_timer;

static EVENT_T nm_nodedown_event;
static EVENT_T nm_send_event;
static EVENT_T nm_log_event;

/*
 * Called when we receive an "imalive" packet from another node.
 * Reset the node's state.
 *
 * XXX: THIS ROUTINE _MAY_ RUN IN INTERRUPT CONTEXT IN THE FUTURE!
 * (once we implement processing of UDP packets in interrupt context)
 */
void
nm_received_imalive(clusternode_t node)
{
	if (node <= 0 || node > NSC_MAX_NODE_VALUE) {
		printk(KERN_INFO "%s:packet from invalid node %u\n",
		       __FUNCTION__, node);
		return;
	}
	if (nm_nodes == 0) {
		/*
		 * Client nodes start node monitoring when they receive
		 * the first packet from the master.
		 */
		if (nm_inited && this_node != clms_master_node &&
		    node == clms_master_node)
			nm_add_node(node);
		else
			printk(KERN_INFO
			       "%s:unexpected packet from node %u\n",
			       __FUNCTION__, node);
		return;
	}
	if (!NSC_NODELIST_TEST1(&nm_monitor_nodelist, node))
		printk(KERN_INFO
		       "%s:unexpected packet from node %u\n",
		       __FUNCTION__, node);
	spin_lock_bh(&nm_lock);
	nm_node_state[node] = 0;
	spin_unlock_bh(&nm_lock);
}

/******************************
 * MASTER ROUTINES.
 ******************************/
/*
 * Initialize Node Monitoring on Master node.
 * reinit_flag is set if this is a CLMS secondary becoming the Master.
 */

static void nm_do_init(void)
{
	int error;
	struct nm_settings nm_settings;

	INIT_EVENT(&nm_nodedown_event);
	INIT_EVENT(&nm_send_event);
	INIT_EVENT(&nm_log_event);

	init_timer(&nm_timer);
	nm_timer.function = nm_kick_send_daemon;
	nm_timer.data = 0;

	error = spawn_daemon_proc("nm nd daemon", nm_nodedown_daemon, NULL);
	if (error)
		panic("%s: couldn't spawn nm_nodedown_daemon %d\n",
		      __FUNCTION__, error);

	error = spawn_daemon_proc("nm send daemon", nm_send_daemon, NULL);
	if (error)
		panic("%s: couldn't spawn nm_send_daemon %d\n",
		      __FUNCTION__, error);

	error = spawn_daemon_proc("nm log daemon", nm_log_daemon, NULL);
	if (error)
		panic("%s: couldn't spawn nm_log_daemon %d\n",
		      __FUNCTION__, error);

	nm_inited = 1;

	nm_settings.nm_timeout = CONFIG_NODE_MONITOR_TIMEOUT_MS;
	nm_settings.nm_max_probes = CONFIG_NODE_MONITOR_FREQ;
	nm_settings.nm_rate_valid = 1;
	nm_settings.nm_log_threshold = CONFIG_NODE_MONITOR_LOG_THRESHOLD;
	nm_settings.nm_log_threshold_valid = 1;
	nm_settings.nm_nodedown_disabled = 0;
	nm_settings.nm_nodedown_disabled_valid = 1;
	nm_update_settings(&nm_settings, 1);
}

void
nm_init(void)
{
	if (!nm_inited)
		nm_do_init();
	else {
		/* CLMS takeover */
		spin_lock_bh(&nm_lock);
		if (NSC_NODELIST_TEST1(&nm_monitor_nodelist, this_node)) {
			NSC_NODELIST_CLR1(&nm_monitor_nodelist, this_node);
			nm_node_state[this_node] = 0;
			nm_nodes--;
		}
		spin_unlock_bh(&nm_lock);
	}
}

/*
 * Start monitoring a node by adding it to the nm_monitor_nodelist.
 * Startup the nm_timer if not already on.
 */
void
nm_add_node(clusternode_t new_node)
{
	int kick_it;

	printk(KERN_INFO "%s: Node %u added\n",
	       __FUNCTION__, new_node);

	spin_lock_bh(&nm_lock);
	if (!NSC_NODELIST_TEST1(&nm_monitor_nodelist, new_node)) {
		NSC_NODELIST_SET1(&nm_monitor_nodelist, new_node);
		nm_nodes++;
	}
	nm_node_state[new_node] = 0;
	nm_log_state[new_node] = 0;
	kick_it = (nm_nodes == 1 && nm_probe_interval > 0 &&
		   !timer_pending(&nm_timer));
	if (kick_it)
		nm_kick_send_daemon(1);
	spin_unlock_bh(&nm_lock);
}

/*
 * This function is run by the nm_timer every nm_probe_interval jiffies.
 *
 * Wakes up nm_send_daemon and re-adds the timer.
 *
 *
 * THIS ROUTINE RUNS IN SOFT-INTERRUPT CONTEXT!
 */
static void
nm_kick_send_daemon(unsigned long locked)
{
	/* Wakeup the nm_send_daemon to send imalive packets. */
	SIGNAL_EVENT(&nm_send_event);

	/* Wait until the next episode...*/
	if (!locked)
		spin_lock(&nm_lock);
	if (nm_nodes > 0 && nm_probe_interval > 0)
		mod_timer(&nm_timer, jiffies + nm_probe_interval);
	if (!locked)
		spin_unlock(&nm_lock);
}

static void
nm_nodedown_daemon(void *dummy)
{
	nsc_nlcookie_t cookie;
	clusternode_t node;

	nsc_daemonize();

	for (;;) {
		WAIT_EVENT(&nm_nodedown_event);
		NSC_NLCOOKIE_INIT(&cookie);
		for (;;) {
			spin_lock_bh(&nm_lock);
			node = NSC_NODELIST_GET_NEXT(&cookie,
						     &nm_down_nodelist);
			if (node == CLUSTERNODE_INVAL) {
				spin_unlock_bh(&nm_lock);
				break;
			}
			NSC_NODELIST_CLR1(&nm_down_nodelist, node);
			spin_unlock_bh(&nm_lock);
			if (this_node == clms_master_node)
				clms_nodedown(node);
			else if (node == clms_master_node)
				clms_attempt_failover(node);
			else
				printk(KERN_NOTICE "%s: Node %u went down\n",
				       __FUNCTION__, node);
		}
		NSC_NLCOOKIE_INIT(&cookie);
		for (;;) {
			spin_lock_bh(&nm_lock);
			node = NSC_NODELIST_GET_NEXT(&cookie,
						     &nm_warn_nodelist);
			if (node == CLUSTERNODE_INVAL) {
				spin_unlock_bh(&nm_lock);
				break;
			}
			NSC_NODELIST_CLR1(&nm_warn_nodelist, node);
			spin_unlock_bh(&nm_lock);
			printk(KERN_NOTICE
			       "%s: Node %u would have gone down"
			       " (nm_nodedown_disabled)\n",
			       __FUNCTION__, node);
		}
		allow_reschedule();
	}
}

/* Called with spin_lock_bh on nm_lock */
static int
nm_node_down(clusternode_t node)
{
	int retval = 1;

	if (!nm_disabled) {
		nm_node_state[node] = 0;
		NSC_NODELIST_CLR1(&nm_monitor_nodelist, node);
		nm_nodes--;
		NSC_NODELIST_SET1(&nm_down_nodelist, node);
	} else {
		nm_node_state[node] = nm_max_probes;
		if (!NSC_NODELIST_TEST1(&nm_warned_nodelist, node)) {
			NSC_NODELIST_SET1(&nm_warn_nodelist, node);
			NSC_NODELIST_SET1(&nm_warned_nodelist, node);
		} else
			retval = 0;
	}

	return retval;
}

static int
nm_send(nsc_nodelist_t *nlp, int flags, void *dataptr, int datalen)
{
	int nodedown = 0;
	clusternode_t node;
	nsc_nlcookie_t cookie;
	int ret;

	/* XXX: broadcast would be good for master */
	NSC_NLCOOKIE_INIT(&cookie);
	for (;;) {
		node = NSC_NODELIST_GET_NEXT(&cookie, nlp);
		if (node == CLUSTERNODE_INVAL)
			break;
		ret = ics_unack_send(node, flags, dataptr, datalen);
		if (ret < 0) {
			if (ret == -EREMOTE) {
				spin_lock_bh(&nm_lock);
				nodedown |= nm_node_down(node);
				spin_unlock_bh(&nm_lock);
			} else
				printk(KERN_WARNING
				       "%s: Error %d sending imalive!\n",
				       __FUNCTION__, -ret);
		}
	}

	return nodedown;
}

#ifdef CONFIG_MOSIX_LL
static void nm_master_send_wq(void *dummy)
{
	mosix_calc_load(0);

	if (atomic_read(&loadlevel_on)) {
		update_load_array(this_node, &export_load,
				  (unsigned long *) &latest_free_mem);

		/* call MOSIX's balancing algorithms */
#ifdef REXEC_LOADTABLE_RACE_FIX
		if (atomic_read(&load_cnt) > 1) {
#else
		if (load_cnt > 1) {
#endif
			load_balance();
			memory_balance();
		}
	}
	else
		update_load_array(this_node, &export_load, 0);
}
#endif /* CONFIG_MOSIX_LL */

static int
nm_master_send(nsc_nodelist_t *nlp)
{
	void *dataptr	 = NULL;
	int datalen	 = 0;
	int flags	 = ICS_UNACK_IMALIVE;
#ifdef CONFIG_LDLVL
#ifdef CONFIG_MOSIX_LL
static DECLARE_WORK(work, nm_master_send_wq, NULL);
#endif
	if (time_before(jiffies, next_ics_unack_load))
		goto do_send;
	next_ics_unack_load = jiffies + MOSIX_SNAP_FREQ;

#ifdef REXEC_LOADTABLE_RACE_FIX
	dataptr	 = master_load_array;
#ifndef MASTER_LOAD_ARRAY_SEND_SIZE
	datalen	 = sizeof(*master_load_array) * (NSC_MAX_NODE_VALUE+1);
#endif
#else
	dataptr	 = &master_load_array[0];
	datalen	 = sizeof(master_load_array);
#endif /* !REXEC_LOADTABLE_RACE_FIX */
	flags |= ICS_UNACK_LOAD;

#ifdef CONFIG_MOSIX_LL
	if (!atomic_read(&load_cnt))
		work.func(work.data);
	else
		schedule_work(&work);
#else
	update_load_array(this_node, &avenrun[0], 0);
#endif /* !CONFIG_MOSIX_LL */
#ifdef MASTER_LOAD_ARRAY_SEND_SIZE
	master_load_array[0].load = master_load_array_max_index;
	datalen = sizeof(*master_load_array) * (1 + master_load_array[0].load);
#else
#ifdef REXEC_LOADTABLE_RACE_FIX
	master_load_array[0].load = atomic_read(&load_cnt);
#else
	master_load_array[0].load = load_cnt;
#endif
#endif /* !MASTER_LOAD_ARRAY_SEND_SIZE */
do_send:
#endif /* CONFIG_LDLVL */

	return nm_send(nlp, flags, dataptr, datalen);
}

static int
nm_client_send(nsc_nodelist_t *nlp)
{
#ifdef CONFIG_LDLVL
	load_array_t clnt_info;
#endif
	void *dataptr	 = NULL;
	int datalen	 = 0;
	int flags	 = ICS_UNACK_IMALIVE;

#ifdef CONFIG_LDLVL
	if (time_before(jiffies, next_ics_unack_load))
		goto do_send;
	next_ics_unack_load = jiffies + MOSIX_SNAP_FREQ;

	dataptr	 = &clnt_info;
	datalen	 = sizeof(clnt_info);
	flags |= ICS_UNACK_LOAD;

	/* Fill in loadinfo message. */
	clnt_info.node = this_node;
#ifdef CONFIG_MOSIX_LL
	clnt_info.load = export_load;
	clnt_info.mem = atomic_read(&loadlevel_on) ? latest_free_mem : 0;
#else
	clnt_info.load = avenrun[0];
	clnt_info.mem = 0;
#endif /* CONFIG_MOSIX_LL */
do_send:
#endif /* CONFIG_LDLVL */

	return nm_send(nlp, flags, dataptr, datalen);
}

static void
nm_send_daemon(void *dummy)
{
	clusternode_t node;
	nsc_nlcookie_t cookie;
	int nodedown;
	nsc_nodelist_t send_nodelist;
	int log_pending;

	nsc_daemonize();
	(void)set_daemon_prio(MAX_RT_PRIO-2, SCHED_FIFO);

	for (;;) {
		WAIT_EVENT(&nm_send_event);
		nodedown = 0;
		log_pending = 0;
		NSC_NLCOOKIE_INIT(&cookie);
		spin_lock_bh(&nm_lock);
		for (;;) {
			node = NSC_NODELIST_GET_NEXT(&cookie,
						     &nm_monitor_nodelist);
			if (node == CLUSTERNODE_INVAL)
				break;
			if (nm_node_state[node] >= nm_log_state[node])
				nm_log_state[node] = nm_node_state[node];
			else if (nm_log_state[node] >= nm_log_threshold &&
				 nm_log_threshold > 0) {
				NSC_NODELIST_SET1(&nm_log_nodelist, node);
				nm_log_pending = 1;
			}
			nm_node_state[node]++;
			if (nm_node_state[node] > nm_max_probes)
				nodedown |= nm_node_down(node);
		}
		send_nodelist = nm_monitor_nodelist;
		if (nm_log_pending && time_after(jiffies, nm_log_nextlog)) {
			log_pending = 1;
			nm_log_nextlog = jiffies + nm_log_interval;
			nm_log_pending = 0;
		}
		spin_unlock_bh(&nm_lock);
		if (this_node == clms_master_node)
			nodedown |= nm_master_send(&send_nodelist);
		else
			nodedown |= nm_client_send(&send_nodelist);
		if (nodedown)
			SIGNAL_EVENT(&nm_nodedown_event);
		if (log_pending)
			SIGNAL_EVENT(&nm_log_event);

		allow_reschedule();
	}
}

static void
nm_log_daemon(void *dummy)
{
	nsc_nlcookie_t cookie;
	clusternode_t node;
	int missed;

	nsc_daemonize();

	for (;;) {
		WAIT_EVENT(&nm_log_event);
		NSC_NLCOOKIE_INIT(&cookie);
		for (;;) {
			spin_lock_bh(&nm_lock);
			node = NSC_NODELIST_GET_NEXT(&cookie,
						     &nm_log_nodelist);
			if (node == CLUSTERNODE_INVAL) {
				spin_unlock_bh(&nm_lock);
				break;
			}
			NSC_NODELIST_CLR1(&nm_log_nodelist, node);
			missed = nm_log_state[node];
			nm_log_state[node] = 0;
			spin_unlock_bh(&nm_lock);
			if (missed > 0)
				printk(KERN_NOTICE
				       "%s: Missed %d packets from node %u\n",
				       __FUNCTION__, missed, node);
		}
		allow_reschedule();
	}
}

void
nm_get_settings(struct nm_settings *nm_settings)
{
	spin_lock_bh(&nm_lock);
	nm_settings->nm_log_threshold = nm_log_threshold;
	nm_settings->nm_log_threshold_valid = 1;
	nm_settings->nm_nodedown_disabled = (nm_disabled != 0);
	nm_settings->nm_nodedown_disabled_valid = 1;
	nm_settings->nm_timeout = nm_timeout;
	nm_settings->nm_max_probes = nm_max_probes;
	nm_settings->nm_rate_valid = 1;
	spin_unlock_bh(&nm_lock);
}

void
nm_stop(void)
{
	spin_lock_bh(&nm_lock);
	nm_probe_interval = 0;
	spin_unlock_bh(&nm_lock);
}

void
nm_update_settings(const struct nm_settings *nsp, int silent)
{
	int interval = 0;
	int changed_rate = 0;
	int changed_threshold = 0;
	int changed_disabled = 0;
	int old_threshold = 0;
	int old_timeout = 0;
	int old_max_probes = 0;
	int i;

	if (nsp->nm_rate_valid) {
		if (nsp->nm_timeout > 0) {
			interval = (nsp->nm_timeout * HZ) /
				nsp->nm_max_probes / 1000;
			if (interval == 0)
				interval = 1;
		}
	}
	spin_lock_bh(&nm_lock);
	if (nsp->nm_nodedown_disabled_valid &&
	    ((nsp->nm_nodedown_disabled != 0) ^ (nm_disabled != 0))) {
		changed_disabled = 1;
		if (!nsp->nm_nodedown_disabled) {
			for (i = 1; i <= NSC_MAX_NODE_VALUE; i++)
				nm_log_state[i] = 0;
			NSC_NODELIST_CLRALL(&nm_down_nodelist);
			NSC_NODELIST_CLRALL(&nm_warn_nodelist);
			NSC_NODELIST_CLRALL(&nm_warned_nodelist);
		}
		nm_disabled = nsp->nm_nodedown_disabled;
	}
	if (nsp->nm_log_threshold_valid &&
	    nm_log_threshold != nsp->nm_log_threshold) {
		changed_threshold = 1;
		old_threshold = nm_log_threshold;
		nm_log_threshold = nsp->nm_log_threshold;
	}
	if (nsp->nm_rate_valid &&
	    (nsp->nm_max_probes != nm_max_probes ||
	     nsp->nm_timeout != nm_timeout)) {
		changed_rate = 1;
		old_timeout = nm_timeout;
		old_max_probes = nm_max_probes;
		nm_timeout = nsp->nm_timeout;
		nm_max_probes = nsp->nm_max_probes;
		nm_probe_interval = interval;
		nm_log_interval = nm_timeout * HZ / 1000;
		for (i = 1; i <= NSC_MAX_NODE_VALUE; i++)
			nm_node_state[i] = 0;
	}
	if (changed_rate && interval > 0)
		nm_kick_send_daemon(1);
	spin_unlock_bh(&nm_lock);
	if (silent)
		return;
	if (changed_disabled)
		printk(KERN_INFO "%s: nm_nodedown_disabled %s\n",
		       __FUNCTION__,
		       nsp->nm_nodedown_disabled ? "set" : "cleared");
	if (changed_threshold)
		printk(KERN_INFO "%s: nm_log_threshold changed from %d to %d\n",
		       __FUNCTION__, old_threshold, nsp->nm_log_threshold);
	if (changed_rate)
		printk(KERN_INFO "%s: nm_rate changed from %d/%d to %d/%d\n",
		       __FUNCTION__, old_timeout, old_max_probes,
		       nsp->nm_timeout, nsp->nm_max_probes);
}

static ssize_t
read_nm_rate(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int len;
	struct nm_settings nm_settings;
	unsigned long page;

	if (*ppos < 0 || count < 0)
		return -EINVAL;
	nm_get_settings(&nm_settings);
	if (!(page = __get_free_page(GFP_KERNEL)))
		return -ENOMEM;
	len = snprintf((void *)page, PAGE_SIZE,
		       "timeout (ms) %d, probes per timeout %d\n",
		       nm_settings.nm_timeout, nm_settings.nm_max_probes);
	if (len >= 0) {
		if (*ppos > len)
			count = 0;
		else if (*ppos + count > len)
			count = len - *ppos;
		if (count > 0 &&
		    copy_to_user(buf, (char *)page + *ppos, count))
			count = -EFAULT;
		else
			*ppos += count;
	} else
		count = -EIO;
	free_page(page);

	return count;
}

static ssize_t
write_nm_rate(struct file * file, const char * buf, size_t count,
	      loff_t *ppos)
{
	int error;
	struct nm_settings nm_settings;
	unsigned long page;
	int len;

	if (!(page = __get_free_page(GFP_KERNEL)))
		return -ENOMEM;
	if (copy_from_user((char *)page, buf, count)) {
		free_page(page);
		return -EFAULT;
	}
	len = sscanf((char *)page, "timeout (ms) %d, probes per timeout %d\n",
		     &nm_settings.nm_timeout, &nm_settings.nm_max_probes);
	if (len == 0)
		len = sscanf((char *)page, "%d %d\n",
			     &nm_settings.nm_timeout,
			     &nm_settings.nm_max_probes);
	free_page(page);
	/*
	 * Don't allow the timeout to be set to more than a day;
	 * entirely arbitrary on my part.
	 */
	if (len != 2 ||
	    nm_settings.nm_timeout < 0 || nm_settings.nm_max_probes < 0 ||
	    nm_settings.nm_timeout > (24 * 60 * 60 * 1000) ||
	    nm_settings.nm_max_probes > SHRT_MAX - 1 ||
	    (nm_settings.nm_timeout != 0 && nm_settings.nm_max_probes == 0))
		return -EINVAL;
	nm_settings.nm_log_threshold_valid = 0;
	nm_settings.nm_nodedown_disabled_valid = 0;
	nm_settings.nm_rate_valid = 1;
	error = clms_write_nm_settings(&nm_settings);
	if (error >= 0)
		*ppos += count;
	else
		count = error;

	return count;
}

struct file_operations proc_nm_rate_operations = {
	read:		read_nm_rate,
	write:		write_nm_rate,
};

static ssize_t
read_nm_nodedown_disabled(struct file *file, char *buf, size_t count,
			  loff_t *ppos)
{
	int len;
	struct nm_settings nm_settings;
	unsigned long page;

	if (*ppos < 0 || count < 0)
		return -EINVAL;
	nm_get_settings(&nm_settings);
	if (!(page = __get_free_page(GFP_KERNEL)))
		return -ENOMEM;
	len = snprintf((void *)page, PAGE_SIZE, "%d\n",
		       nm_settings.nm_nodedown_disabled);
	if (len >= 0) {
		if (*ppos > len)
			count = 0;
		else if (*ppos + count > len)
			count = len - *ppos;
		if (count > 0 &&
		    copy_to_user(buf, (char *)page + *ppos, count))
			count = -EFAULT;
		else
			*ppos += count;
	} else
		count = -EIO;
	free_page(page);

	return count;
}

static ssize_t
write_nm_nodedown_disabled(struct file * file, const char * buf, size_t count,
			   loff_t *ppos)
{
	int error;
	struct nm_settings nm_settings;
	unsigned long page;
	int len;
	int disabled;

	if (!(page = __get_free_page(GFP_KERNEL)))
		return -ENOMEM;
	if (copy_from_user((char *)page, buf, count)) {
		free_page(page);
		return -EFAULT;
	}
	len = sscanf((char *)page, "%d\n", &disabled);
	free_page(page);
	if (len != 1 || (disabled != 0 && disabled != 1))
		return -EINVAL;
	nm_settings.nm_log_threshold_valid = 0;
	nm_settings.nm_rate_valid = 0;
	nm_settings.nm_nodedown_disabled = disabled;
	nm_settings.nm_nodedown_disabled_valid = 1;
	error = clms_write_nm_settings(&nm_settings);
	if (error >= 0)
		*ppos += count;
	else
		count = error;

	return count;
}

struct file_operations proc_nm_nodedown_disabled_operations = {
	read:		read_nm_nodedown_disabled,
	write:		write_nm_nodedown_disabled,
};

static ssize_t
read_nm_log_threshold(struct file *file, char *buf, size_t count,
		      loff_t *ppos)
{
	int len;
	struct nm_settings nm_settings;
	unsigned long page;

	if (*ppos < 0 || count < 0)
		return -EINVAL;
	nm_get_settings(&nm_settings);
	if (!(page = __get_free_page(GFP_KERNEL)))
		return -ENOMEM;
	len = snprintf((void *)page, PAGE_SIZE, "%d\n",
		       nm_settings.nm_log_threshold);
	if (len >= 0) {
		if (*ppos > len)
			count = 0;
		else if (*ppos + count > len)
			count = len - *ppos;
		if (count > 0 &&
		    copy_to_user(buf, (char *)page + *ppos, count))
			count = -EFAULT;
		else
			*ppos += count;
	} else
		count = -EIO;
	free_page(page);

	return count;
}

static ssize_t
write_nm_log_threshold(struct file * file, const char * buf, size_t count,
		       loff_t *ppos)
{
	int error;
	struct nm_settings nm_settings;
	unsigned long page;
	int len;

	if (!(page = __get_free_page(GFP_KERNEL)))
		return -ENOMEM;
	if (copy_from_user((char *)page, buf, count)) {
		free_page(page);
		return -EFAULT;
	}
	len = sscanf((char *)page, "%d\n", &nm_settings.nm_log_threshold);
	free_page(page);
	if (len != 1 || nm_settings.nm_log_threshold < 0)
		return -EINVAL;
	nm_settings.nm_rate_valid = 0;
	nm_settings.nm_nodedown_disabled_valid = 0;
	nm_settings.nm_log_threshold_valid = 1;
	error = clms_write_nm_settings(&nm_settings);
	if (error >= 0)
		*ppos += count;
	else
		count = error;

	return count;
}

struct file_operations proc_nm_log_threshold_operations = {
	read:		read_nm_log_threshold,
	write:		write_nm_log_threshold,
};
