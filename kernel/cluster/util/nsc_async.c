/*
 * 	Cluster asynch support code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Portions Copyright 2007,2008 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/async.h>


/* Number of daemons to be started */
#ifdef DYNAMIC_NSC_ASYNC_DAEMONS
int nsc_async_daemons;
#else
int nsc_async_daemons __initdata = NSC_ASYNC_DAEMONS;
#endif


typedef struct nsc_async_queue {
#ifdef NSC_ASYNC_LINUX_LIST
	struct list_head		taq_list;
#else
	struct nsc_async_entry		*taq_head;
	struct nsc_async_entry		*taq_tail;
#endif
	HARDIRQ_COND_LOCK_T		taq_lock;
	CONDITION_T			taq_cond;
	int				taq_maxargsz;
#ifdef SSI_NOTUSED
	struct nsc_async_entry		*taq_freelist;
	int				taq_freelist_cnt;
#endif
} nsc_async_queue_t;

typedef struct nsc_async_entry {
#ifdef NSC_ASYNC_LINUX_LIST
	struct list_head		tae_list;
#else
	struct nsc_async_entry		*tae_prev;
	struct nsc_async_entry		*tae_next;
#endif
	void				(*tae_func)(void *);
#ifdef SSI_NOTUSED
	int				tae_allocsize;
#endif
	int				tae_argsize;
#ifdef NSC_ASYNC_ARGS_ZERO_COPY
	caddr_t				*tae_args;
	int				tae_flags;
#else
	char				tae_args[1];
#endif
#ifdef NSC_INHERIT_USER_NICE
	long				tae_user_nice;
#endif
} nsc_async_entry_t;

/*
 * Maximum number of async events to examine for a
 * dupclicate async event.
 */
#define NSC_ASYNC_MAX_DUPCHECKS		10

#ifdef SSI_NOTUSED
typedef struct {
	void				*ref_queue;
	int				ref_count;
	int				ref_flags;
	COND_LOCK_T			ref_mutex;
	CONDITION_T			ref_cond;
} nsc_async_ref_t;

#define NSC_ASYNC_REF_CLOSING		0x01
#endif

static void nsc_async_daemon(void *);
static nsc_async_entry_t *nsc_async_entry_alloc(nsc_async_queue_t *, int, int);
static void nsc_async_entry_dealloc(nsc_async_entry_t *);
static void nsc_async_entry_addq(nsc_async_queue_t *, nsc_async_entry_t *);
#ifndef NSC_ASYNC_LINUX_LIST
static nsc_async_entry_t *nsc_async_entry_deq(nsc_async_queue_t *);
#endif
static int nsc_async_entry_isdup(nsc_async_queue_t *, nsc_async_entry_t *);

int nsc_async_alloc_fails = 0;

#ifdef NSC_ASYNC_ENTRY_KMEM_CACHE
static kmem_cache_t *nsc_async_entry_cachep;
#endif

/*
 * void *nsc_async_queue_init(char *name, int num_daemons, int max_argsz)
 *	Initialize an async queue.
 */
void *
nsc_async_queue_init(
	char *name,
	int num_daemons,
	int max_argsz)
{
	nsc_async_queue_t *async_queue;
	int i, error;

#ifdef NSC_ASYNC_LINUX_LIST
	async_queue = kmalloc_nofail(sizeof(nsc_async_queue_t));

	INIT_LIST_HEAD(&async_queue->taq_list);
#else
	async_queue = kzmalloc_nofail(sizeof(nsc_async_queue_t));
#endif

	INIT_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
	INIT_CONDITION(&async_queue->taq_cond);

	smp_mb(); /* barrier for async_queue */
	for (i = 0; i < num_daemons; i++) {
		error = spawn_daemon_thread(name,
					    nsc_async_daemon,
					    (void *)async_queue);
		if (error)
			panic("Unable to spawn nsc async daemon, error %d.", error);
	}

	async_queue->taq_maxargsz = max_argsz;
#ifdef SSI_NOTUSED
	async_queue->taq_freelist = NULL;
	async_queue->taq_freelist_cnt = 0;
#endif

	return (void *)async_queue;
}

/*
 * int nsc_async_queue(void *queue, void (*func)(void *), void *args,
 *		       size_t argsize, int flags)
 *	Called to queue an asynchronous event.  The specified function
 *	is called asynchronously with the provided arguments.  A NULL
 *	argument pointer with a 0 argsize is permitted.  An error is
 *	returned if the event was unable to be queued.  This function
 *	does not block unless the NSC_ASYNC_SLEEP flag is specified.
 *	The NSC_ASYNC_DUPCHECK flag indicates that the event should
 *	only be queued if an identical async event isn't already in
 *	the queue.  The NSC_ASYNC_ALLOCFREE flag indicates that an
 *	async event structure should be pre-allocated and placed on
 *	the freelist for use later when an event is queued with the
 *	NSC_ASYNC_USEFREE flag.
 */
int
nsc_async_queue(
	void *queue,
	void (*func)(void *),
	void *args,
	size_t argsize,
	int flags)
{
	nsc_async_queue_t *async_queue = (nsc_async_queue_t *)queue;
	nsc_async_entry_t *ep;
#ifdef SSI_NOTUSED
	nsc_async_entry_t *fep = NULL;

	if (flags & NSC_ASYNC_ALLOCFREE) {
		SSI_ASSERT(async_queue->taq_maxargsz != NSC_ASYNC_NO_MAXARGSZ);
		SSI_ASSERT(async_queue->taq_maxargsz >= 0);
		SSI_ASSERT(async_queue->taq_maxargsz >= argsize);

		fep = nsc_async_entry_alloc(async_queue,
					    async_queue->taq_maxargsz,
					    (flags & NSC_ASYNC_SLEEP));
		if (fep == NULL && async_queue->taq_freelist == NULL)
			return -ENOMEM;
	}

	LOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);

	if ((flags & NSC_ASYNC_USEFREE) &&
	    (async_queue->taq_freelist != NULL)) {
		SSI_ASSERT(async_queue->taq_maxargsz >= argsize);

		ep = async_queue->taq_freelist;
		async_queue->taq_freelist = ep->tae_next;
		ep->tae_next = NULL;
		async_queue->taq_freelist_cnt--;
		ep->tae_argsize = argsize;
	}
	else {
		UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);

		if (flags & NSC_ASYNC_USEFREE) {
			printk(KERN_WARNING "nsc_async_queue: freelist"
					 " empty!");
		}
#endif /* SSI_NOTUSED */

#ifdef NSC_ASYNC_SLAB_NOFS__FLAG
		ep = nsc_async_entry_alloc(async_queue,
					   argsize,
					   flags);
#else
		ep = nsc_async_entry_alloc(async_queue,
					   argsize,
					   (flags & NSC_ASYNC_SLEEP));
#endif
		if (ep == NULL) {
#ifdef SSI_NOTUSED
			if (fep)
				nsc_async_entry_dealloc(fep);
#endif
#ifdef NSC_ASYNC_ARGS_ZERO_COPY
			if (flags & NSC_ASYNC_ARGS_FREE)
				kfree(args);
#endif
			return -ENOMEM;
		}

#ifdef NSC_INHERIT_USER_NICE
		ep->tae_user_nice = TASK_NICE(current);
#endif
		ep->tae_func = func;
#ifdef NSC_ASYNC_ARGS_ZERO_COPY
		ep->tae_args = args;
		ep->tae_flags = flags;
#else
		if (args != NULL)
			bcopy((caddr_t)args, (caddr_t)&ep->tae_args[0], argsize);
#endif
		LOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
#ifdef SSI_NOTUSED
	}

	if (flags & NSC_ASYNC_ALLOCFREE) {
		/* Add the preallocated async entry to the freelist. */
		fep->tae_next = async_queue->taq_freelist;
		async_queue->taq_freelist = fep;
		async_queue->taq_freelist_cnt++;
	}

	ep->tae_func = func;
	if (args != NULL)
		bcopy((caddr_t)args, (caddr_t)&ep->tae_args[0], argsize);
#endif /* SSI_NOTUSED */

	if ((flags & NSC_ASYNC_DUPCHECK) &&
	    nsc_async_entry_isdup(async_queue, ep)) {
		UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
		nsc_async_entry_dealloc(ep);
		return 0;
	}

#ifdef NSC_ASYNC_ARGS_ZERO_COPY
	/* Barrier ensures values referenced by "args"
	 * become visible to async handler thread.
	 */
	if (ep->tae_args != NULL)
		smp_mb();
#endif

	/*
	 * Add the entry to the daemon queue.
	 */
	nsc_async_entry_addq(async_queue, ep);

	UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);

	SIGNAL_CONDITION(&async_queue->taq_cond);

	return 0;
}

#ifdef SSI_NOTUSED
/*
 * void nsc_async_freelist_alloc(void *queue)
 *	Called to prealloc an async event structure.  This interface
 *	can be called from a sleeping context when it is anticpated
 *	that an async operation will be required to be queued from
 *	a non-blocking context.  This routine, combined with the
 *	NSC_ASYNC_USEFREE flag specified when queueing, will
 *	avoid the possibility of the queueing operation failing.
 */
void
nsc_async_freelist_alloc(void *queue)
{
	nsc_async_queue_t *async_queue = (nsc_async_queue_t *)queue;
	nsc_async_entry_t *fep;

	fep = nsc_async_entry_alloc(async_queue,
				    async_queue->taq_maxargsz,
				    B_TRUE);

	/*
	 * Add the preallocated async entry to the freelist.
	 */
	LOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
	fep->tae_next = async_queue->taq_freelist;
	async_queue->taq_freelist = fep;
	async_queue->taq_freelist_cnt++;
	UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
}

/*
 * void nsc_async_freelist_dealloc(void *queue)
 *	Called to free a preallocated async event structure from the
 *	queue's freelist.  This counters a corresponding call to
 *	nsc_async_free_alloc() or nsc_async_queue() with the
 *	NSC_ASYNC_ALLOCFREE flag specified.
 */
void
nsc_async_freelist_dealloc(void *queue)
{
	nsc_async_queue_t *async_queue = (nsc_async_queue_t *)queue;
	nsc_async_entry_t *ep = NULL;

	LOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
	if (async_queue->taq_freelist != NULL) {
		ep = async_queue->taq_freelist;
		async_queue->taq_freelist = ep->tae_next;
		ep->tae_next = NULL;
		async_queue->taq_freelist_cnt--;
		UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
		nsc_async_entry_dealloc(ep);
	}
	else {
		UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
		printk(KERN_WARNING "nsc_async_freelist_dealloc: freelist empty!");
	}
}
#endif /* SSI_NOTUSED */

#ifdef NSC_ASYNC_ENTRY_KMEM_CACHE
void
nsc_async_init(void)
{
	nsc_async_entry_cachep = kmem_cache_create("nsc_async_entry",
				sizeof(nsc_async_entry_t), 0,
				SLAB_HWCACHE_ALIGN|SLAB_RECLAIM_ACCOUNT|SLAB_PANIC,
				NULL, NULL);
}
#endif

/*
 * static nsc_async_entry_t *
 * nsc_async_entry_alloc(nsc_async_queue_t *async_queueint argsize, int sleep)
 *	Called internally to allocate an async entry.
 */
static nsc_async_entry_t *
#ifdef NSC_ASYNC_SLAB_NOFS__FLAG
nsc_async_entry_alloc(
	nsc_async_queue_t *async_queue,
	int argsize,
	int flags)
#else
nsc_async_entry_alloc(
	nsc_async_queue_t *async_queue,
	int argsize,
	int sleep)
#endif
{
	nsc_async_entry_t *ep;
#ifdef NSC_ASYNC_ENTRY_KMEM_CACHE
	int gfp_flags;

	if (flags & NSC_ASYNC_SLEEP) {
		if (flags & NSC_ASYNC_SLAB_NOFS)
			gfp_flags = SLAB_NOFS;
		else
			gfp_flags = SLAB_KERNEL;
	} else
		gfp_flags = SLAB_ATOMIC;

	ep = kmem_cache_alloc(nsc_async_entry_cachep, gfp_flags);
#else /* !NSC_ASYNC_ENTRY_KMEM_CACHE */
	int size;

#ifdef NSC_ASYNC_ARGS_ZERO_COPY
	size = sizeof(nsc_async_entry_t);
#else
	size = sizeof(nsc_async_entry_t) - sizeof(char) + argsize;
#endif

#ifdef NSC_ASYNC_SLAB_NOFS__FLAG
	if (flags & NSC_ASYNC_SLEEP) {
		if (flags & NSC_ASYNC_SLAB_NOFS)
			ep = kmalloc(size, GFP_NOFS|__GFP_NOFAIL);
		else
			ep = kmalloc_nofail(size);
	}
#else
	if (sleep)
		ep = kmalloc_nofail(size);
#endif
	else
		ep = kmalloc(size, GFP_ATOMIC);
#endif /* !NSC_ASYNC_ENTRY_KMEM_CACHE */
	if (ep == NULL) {
		nsc_async_alloc_fails++;
		return NULL;
	}
#ifdef NSC_ASYNC_LINUX_LIST
	INIT_LIST_HEAD(&ep->tae_list);
#else
	bzero(ep, size);
#endif

#ifdef SSI_NOTUSED
	ep->tae_allocsize = argsize;
#endif
	ep->tae_argsize = argsize;
	return ep;
}

/*
 * static void nsc_async_entry_dealloc(nsc_async_entry_t *ep)
 *	Called internally to deallocate an async entry.
 */
static void
nsc_async_entry_dealloc(
	nsc_async_entry_t *ep)
{
#ifdef SSI_NOTUSED
	int size;

	size = sizeof(nsc_async_entry_t) - sizeof(char) + ep->tae_allocsize;
#endif /* SSI_NOTUSED */
#ifdef NSC_ASYNC_ARGS_ZERO_COPY
	if (ep->tae_flags & NSC_ASYNC_ARGS_FREE)
		kfree(ep->tae_args);
#endif
#ifdef NSC_ASYNC_ENTRY_KMEM_CACHE
	kmem_cache_free(nsc_async_entry_cachep, ep);
#else
	kfree(ep);
#endif
}

/*
 * static void nsc_async_entry_addq(nsc_async_queue_t *async_queue,
 *				    nsc_async_entry_t *ep)
 *	Called internally to add an async entry to the queue of events
 *	to be processed.
 */
static void
nsc_async_entry_addq(
	nsc_async_queue_t *async_queue,
	nsc_async_entry_t *ep)
{
#ifdef NSC_ASYNC_LINUX_LIST
#ifdef CFS_ASYNC_QUEUE
	if (ep->tae_flags & NSC_ASYNC_STACK)
		list_add(&ep->tae_list, &async_queue->taq_list);
	else
#endif
	list_add_tail(&ep->tae_list, &async_queue->taq_list);
#else
	if (async_queue->taq_head == NULL) {
		async_queue->taq_head = ep;
		async_queue->taq_tail = ep;
	}
	else {
		async_queue->taq_tail->tae_next = ep;
		ep->tae_prev = async_queue->taq_tail;
		async_queue->taq_tail = ep;
	}
#endif
}

#ifdef CFS_ASYNC_QUEUE
/*
 * static nsc_async_entry_t *
 * nsc_async_entry_deq_cookie(nsc_async_queue_t *async_queue, unsigned long *cookie)
 *	Called internally to dequeue the top async entry with the cookie
 *	from the queue of events to be processed. If no entries with the cookie
 *	exist the very top async entry is returned.
 *
 *	Called with taq_lock spinlock held.
 */
static nsc_async_entry_t *
nsc_async_entry_deq_cookie(
	nsc_async_queue_t *async_queue,
	unsigned long *cookie)
{
	nsc_async_entry_t *ep;

	for (;;) {
		list_for_each_entry(ep, &async_queue->taq_list, tae_list) {
			if (cookie) {
				/* SSI_XXX: Only for CFS events. */
				/* SSI_XXX: This should use tae_cookie. */
				if (*cookie &&
				    *cookie != (unsigned long) *ep->tae_args)
					continue;
				if (*cookie == 0)
					*cookie = (unsigned long) *ep->tae_args;
			}
			list_del(&ep->tae_list);
			return ep;
		}

		if (!cookie || *cookie == 0)
			return NULL;

		*cookie = 0;
	}
}
#define nsc_async_entry_deq(_q) nsc_async_entry_deq_cookie(_q, NULL)

#else /* CFS_ASYNC_QUEUE */
/*
 * static nsc_async_entry_t *nsc_async_entry_deq(nsc_async_queue_t *async_queue)
 *	Called internally to dequeue the top async entry from the
 *	queue of events to be processed.
 */
#ifdef NSC_ASYNC_LINUX_LIST
static nsc_async_entry_t *
#else
static nsc_async_entry_t *
#endif
nsc_async_entry_deq(
	nsc_async_queue_t *async_queue)
{
	nsc_async_entry_t *ep;
#ifdef NSC_ASYNC_LINUX_LIST
	struct list_head *pos;

	__list_for_each(pos, &async_queue->taq_list) {
		ep = list_entry(pos, nsc_async_entry_t, tae_list);
		list_del(pos);
		return ep;
	}
	BUG();
	/* not reached */
	return NULL;
#else
	ep = async_queue->taq_head;
	async_queue->taq_head = async_queue->taq_head->tae_next;
	if (async_queue->taq_head == NULL)
		async_queue->taq_tail = NULL;
	else
		async_queue->taq_head->tae_prev = NULL;

	return ep;
#endif /* !NSC_ASYNC_LINUX_LIST */
}
#endif /* !CFS_ASYNC_QUEUE */

/*
 * static int nsc_async_entry_isdup(nsc_async_queue_t *async_queue,
 *				    nsc_async_entry_t *ep)
 *	Called internally to check for a duplicate async entry.
 */
static int
nsc_async_entry_isdup(
	nsc_async_queue_t *async_queue,
	nsc_async_entry_t *ep)
{
	nsc_async_entry_t *tep;
	int checks;

#ifdef NSC_ASYNC_LINUX_LIST
	checks = 0;
	list_for_each_entry_reverse(tep, &async_queue->taq_list, tae_list) {
		if (++checks > NSC_ASYNC_MAX_DUPCHECKS)
			break;
#else
	for (tep = async_queue->taq_tail, checks = 0;
	     tep != NULL && checks < NSC_ASYNC_MAX_DUPCHECKS;
	     tep = tep->tae_prev, checks++) {
#endif
		if (tep->tae_func == ep->tae_func &&
		    tep->tae_argsize == ep->tae_argsize &&
		    memcmp((void *) tep->tae_args, (void *) ep->tae_args,
		    		tep->tae_argsize) == 0)
			return B_TRUE;
	}

	return B_FALSE;
}

#ifdef CONFIG_VPROC
extern void *nsc_async_nocldwait_queue;

void
async_cleanup_task_structs(void)
{
	nsc_async_queue_t *async_queue = (nsc_async_queue_t *)nsc_async_nocldwait_queue;
	nsc_async_entry_t *ep;
#ifndef NSC_ASYNC_ARGS_ZERO_COPY
	void *func_args;
#endif

	LOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
#ifdef NSC_ASYNC_LINUX_LIST
	while (!list_empty(&async_queue->taq_list)) {
#else
	while (async_queue->taq_head != NULL) {
#endif /* !NSC_ASYNC_LINUX_LIST */
		/* Dequeue head entry. */
		ep = nsc_async_entry_deq(async_queue);

		UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);

#ifdef NSC_ASYNC_ARGS_ZERO_COPY
		(*ep->tae_func)(ep->tae_args);
#else
		func_args = ep->tae_args;
		(*ep->tae_func)(func_args);
#endif

		nsc_async_entry_dealloc(ep);

		LOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
	}
	UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
}
#endif

/*
 * static void nsc_async_daemon(nsc_async_queue_t *async_queue)
 *	This routine processes asynchronous events and is executed by
 *	each of the spawned async daemon threads.
 */
static void
nsc_async_daemon(
	void *args)
{
	nsc_async_queue_t *async_queue = (nsc_async_queue_t *)args;
	nsc_async_entry_t *ep;
#ifndef NSC_ASYNC_ARGS_ZERO_COPY
	void *func_args;
#endif
#ifdef CFS_ASYNC_QUEUE
	int priority = 0;

	extern void *nsc_async_cfs_queue[3]; /* RPC_NR_PRIORITY */
#endif

	nsc_daemonize();

#ifdef CONFIG_CFS
	if (!strncmp(current->comm, "cfs_async", 9)) {
#ifndef CFS_ASYNC_LOCAL_SYNC
		/*
		 * We want less throttling in balance_dirty_pages() so that cfs to
		 * localhost doesn't cause cfs_async to lock up due to all the client's
		 * dirty pages.
		 */
		current->flags |= PF_LESS_THROTTLE;
#endif
#ifdef NSC_INHERIT_USER_NICE
		(void) set_daemon_prio(0, SCHED_NORMAL);
#endif
#ifdef CFS_ASYNC_QUEUE
		/*
		 * "cfs_async_0" RPC_PRIORITY_LOW
		 * "cfs_async_1" RPC_PRIORITY_NORMAL
		 * "cfs_async_2" RPC_PRIORITY_HIGH
		 */
		if (!strncmp(current->comm, "cfs_async_1", 11))
			priority = 1;
		else if (!strncmp(current->comm, "cfs_async_2", 11))
			priority = 2;
#endif
	}
#endif /* CONFIG_CFS */

	while(1) {
#ifdef CFS_ASYNC_QUEUE
		nsc_async_queue_t *queue;
		unsigned long cookie = 0;
		int prio;
#endif
		allow_reschedule();

		LOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);

#ifdef NSC_ASYNC_LINUX_LIST
		while (list_empty(&async_queue->taq_list)) {
#else
		while (async_queue->taq_head == NULL) {
#endif
			WAIT_HARDIRQ_CONDITION(&async_queue->taq_cond,
					       &async_queue->taq_lock);
		}

#ifdef CFS_ASYNC_QUEUE
		if (priority == 1) {
			/* Start with queue for RPC_PRIORITY_HIGH */
			queue = nsc_async_cfs_queue[2];
			UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);
			LOCK_HARDIRQ_COND_LOCK(&queue->taq_lock);
			prio = 2;
		} else {
			/* Non-"cfs_async" threads are certainly priority 0 */
			queue = async_queue;
			prio = priority;
		}

		/*
		 * Process priority queues
		 */
		for (;;) {
			if (prio == 2) {
				/* Dequeue entries with batching. */
				ep = nsc_async_entry_deq_cookie(queue, &cookie);
			} else {
				/* Dequeue head entry. */
				ep = nsc_async_entry_deq(queue);
			}

			UNLOCK_HARDIRQ_COND_LOCK(&queue->taq_lock);

			if (ep == NULL) {
				if (priority != 1 || prio == 0)
					break; /* Done. */

				/* Traverse next priority queue */
				queue = nsc_async_cfs_queue[--prio];

				LOCK_HARDIRQ_COND_LOCK(&queue->taq_lock);
				continue;
			}

#ifdef NSC_INHERIT_USER_NICE
			if (!rt_task(current))
				set_user_nice(current, ep->tae_user_nice);
#endif
			(*ep->tae_func)(ep->tae_args);

			nsc_async_entry_dealloc(ep);

			if (priority == 1 && prio != 2) {
				/* Re-check queue for RPC_PRIORITY_HIGH */
				prio = 2;
				queue = nsc_async_cfs_queue[prio];
			}

			LOCK_HARDIRQ_COND_LOCK(&queue->taq_lock);
		}
#else /* CFS_ASYNC_QUEUE */

		/* Dequeue head entry. */
		ep = nsc_async_entry_deq(async_queue);

		UNLOCK_HARDIRQ_COND_LOCK(&async_queue->taq_lock);

#ifdef NSC_INHERIT_USER_NICE
		if (!rt_task(current))
			set_user_nice(current, ep->tae_user_nice);
#endif
#ifdef NSC_ASYNC_ARGS_ZERO_COPY
		(*ep->tae_func)(ep->tae_args);
#else
		func_args = (ep->tae_argsize == 0) ? NULL: ep->tae_args;
		(*ep->tae_func)(func_args);
#endif

		nsc_async_entry_dealloc(ep);
#endif /* !CFS_ASYNC_QUEUE */
	}
	/* NOT REACHED */
}

#ifdef SSI_NOTUSED
/*
 * The following routines provide support for performing asynchronous
 * events with reference counting.  This provides support for waiting
 * for the reference counted async events to complete.
 *
 * The nsc_async_ref_alloc() and nsc_async_ref_free() routines allocate
 * and free the reference counting data structure.  This data structure
 * must be passed as a parameter to the reference counting async interfaces.
 *
 * The nsc_async_ref_queue() interface supports queueing async events.
 * The nsc_async_ref_done() interface is called by async routines to
 * indicate they have completed processing.  The nsc_async_ref_wait()
 * routine is called to wait for all reference counted async events
 * to complete.
 */

/*
 * void nsc_async_ref_alloc(void *queue)
 *	Called to allocate the async reference counting data structure.
 *	This interface does not sleep and may return NULL if memory
 *	is unavailable.
 */
void *
nsc_async_ref_alloc(void *queue)
{
	nsc_async_ref_t *refp;

	refp = (nsc_async_ref_t *)kmalloc(sizeof(nsc_async_ref_t),
					      GFP_ATOMIC);
	if (refp == NULL) {
		nsc_async_alloc_fails++;
	}
	else {
		refp->ref_queue = queue;
		refp->ref_count = 0;
		refp->ref_flags = 0;
		INIT_COND_LOCK(&refp->ref_mutex);
		INIT_CONDITION(&refp->ref_cond);
	}

	return (void *)refp;
}

/*
 * void nsc_async_ref_free(void *datap)
 *	Called to deallocate the async reference counting data structure.
 */
void
nsc_async_ref_free(
	void *datap)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;

	SSI_ASSERT(refp->ref_count == 0);

	DEINIT_COND_LOCK(&refp->ref_mutex);
	DEINIT_CONDITION(&refp->ref_cond);
	kfree((caddr_t)refp);
}

/*
 * int nsc_async_ref_queue(void *datap, void (*func)(void *),
 *			    void *args, size_t argsize, int flags)
 *	Called to queue an asynchronous event and hold a reference count
 *	to track the event.
 */
int
nsc_async_ref_queue(
	void *datap,
	void (*func)(void *),
	void *args,
	size_t argsize,
	int flags)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;
	int error = 0;

	LOCK_COND_LOCK(&refp->ref_mutex);

	if (refp->ref_flags & NSC_ASYNC_REF_CLOSING) {
#ifdef DEBUG
		printk(KERN_WARNING "nsc_async_ref_queue: ref queue closing!");
#endif
		error = -EBUSY;
		goto out;
	}

	error = nsc_async_queue(refp->ref_queue, func, args, argsize, flags);
	if (error)
		goto out;

	refp->ref_count++;

out:
	UNLOCK_COND_LOCK(&refp->ref_mutex);
	return error;
}

/*
 * void nsc_async_ref_freelist_alloc(void *datap)
 *	Called to prealloc an async event structure.  This interface
 *	can be called from a sleeping context when it is anticpated
 *	that an async operation will be required to be queued from
 *	a non-blocking context.  This routine, combined with the
 *	NSC_ASYNC_USEFREE flag specified when queueing, will
 *	avoid the possibility of the queueing operation failing.
 */
void
nsc_async_ref_freelist_alloc(void *datap)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;

	nsc_async_freelist_alloc(refp->ref_queue);
}

/*
 * void nsc_async_ref_freelist_dealloc(void *datap)
 *	Called to free a preallocated async event structure from the
 *	queue's freelist.  This counters a corresponding call to
 *	nsc_async_free_alloc() or nsc_async_queue() with the
 *	NSC_ASYNC_ALLOCFREE flag specified.
 */
void
nsc_async_ref_freelist_dealloc(void *datap)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;

	nsc_async_freelist_dealloc(refp->ref_queue);
}

/*
 * void nsc_async_ref_done(void *datap)
 *	Called by an asynchronous event routine to indicate that it
 *	has completed processing.
 */
void
nsc_async_ref_done(
	void *datap)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;

	LOCK_COND_LOCK(&refp->ref_mutex);
	refp->ref_count--;
	SSI_ASSERT(refp->ref_count >= 0);
	if ((refp->ref_count == 0) &&
	    (refp->ref_flags & NSC_ASYNC_REF_CLOSING))
		SIGNAL_CONDITION(&refp->ref_cond);
	UNLOCK_COND_LOCK(&refp->ref_mutex);
}

/*
 * void nsc_async_ref_wait(void *datap)
 *	Called to wait for all reference counted asynchronous events to
 *	complete.
 */
void
nsc_async_ref_wait(
	void *datap)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;

	LOCK_COND_LOCK(&refp->ref_mutex);
	refp->ref_flags |= NSC_ASYNC_REF_CLOSING;
	while(refp->ref_count)
		WAIT_CONDITION(&refp->ref_cond, &refp->ref_mutex);
	UNLOCK_COND_LOCK(&refp->ref_mutex);
}

/*
 * int nsc_async_ref_check(void *datap)
 *	Called to check to see if all reference counted asynchronous
 *	events have completed.  If they have, TRUE is returned and
 *	the closing flag is set to prevent any further events from
 *	being queued.
 */
int
nsc_async_ref_check(
	void *datap)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;
	int done;

	LOCK_COND_LOCK(&refp->ref_mutex);
	done = (refp->ref_count == 0);
	if (done)
		refp->ref_flags |= NSC_ASYNC_REF_CLOSING;
	UNLOCK_COND_LOCK(&refp->ref_mutex);

	return done;
}

/*
 * void nsc_async_ref_incr(void *datap)
 *	Called to increment the reference count without calling
 *	any asynchronous function.
 */
int
nsc_async_ref_incr(
	void *datap)
{
	nsc_async_ref_t *refp = (nsc_async_ref_t *)datap;
	int error = 0;

	LOCK_COND_LOCK(&refp->ref_mutex);

	if (refp->ref_flags & NSC_ASYNC_REF_CLOSING) {
#ifdef DEBUG
		printk(KERN_WARNING "nsc_async_ref_incr: ref queue closing!");
#endif
		error = -EBUSY;
		goto out;
	}

	refp->ref_count++;

out:
	UNLOCK_COND_LOCK(&refp->ref_mutex);
	return error;
}
#endif /* SSI_NOTUSED */
