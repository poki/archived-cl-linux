/*
 * 	Cluster daemon support code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains an implementation of the generic CI daemon
 * process and thread daemon creation/exit routines.
 *
 * The spawn_daemon_proc(), exit_daemon_proc(), spawn_daemon_thread()
 * and exit_daemon_thread() functions are provided.  Systems that do
 * not have threads will implement spawn_daemon_thread() as a wrapper
 * for spawn_daemon_proc().
 *
 * It should also be noted that some base systems may not handle
 * the spawning of server process or thread daemons correctly
 * when initiated from a system call.
 *
 * Another caveat: Some systems (notably SVR4.0) implement process
 * creation such that the child process inherits the parent's
 * kernel stack.  For this reason, daemon processes should avoid
 * spawning other daemon processes.  Recursive spawning of daemons
 * can lead to stack crawl and overflow.
 */

#include <linux/types.h>
#include <linux/sched.h>
#include <linux/smp_lock.h>
#include <linux/string.h>
#include <linux/file.h>
#ifdef CONFIG_VPROC
#include <linux/vproc.h>
#include <linux/dpvproc.h>
#endif /* VPROC */
#include <asm/uaccess.h>
#include <cluster/async.h>

extern struct task_struct *child_reaper;

/*
 * int spawn_daemon_proc()
 *	Generic TNC interface for spawning daemon processes.
 *
 * Calling/Exit State:
 *	No locks held upon entry or exit.
 *
 *	Processes spawned via this routine should avoid spawning
 *	other daemon processes to avoid the change of kernel
 *	overflow on some platforms.
 *
 *	The 'name' parameter must be a pointer to a string in
 *	memory that won't be free'd (i.e. typically a constant
 *	string expression).
 */
static int
__spawn_daemon_proc(char *name, void (*daemon_func)(void *),
		    void *daemon_args, char *funcname)
{
	int retval = 0;

#ifdef CONFIG_VPROC
	unsigned long ptrace;

	ptrace = current->ptrace;
	current->ptrace = 0;
	retval = kernel_thread((int (*)(void *))daemon_func,
		      daemon_args, CLONE_FS);
	current->ptrace = ptrace;
	if (retval < 0)
		return retval;
	else {
		struct vproc *v = LOCATE_VPROC_PID_ORIGIN(retval, funcname);
		struct pvproc *pvp = NULL;

		SSI_ASSERT(v);
		if (!v)
			return 0;

		pvp = PVP(v);
		VPROC_LOCK_EXCL(v, funcname);
#ifdef VPROC_HOLD_ZERO_GET_TASK
		if (PV_IS_ALIVE(pvp)) {
#else
		if (pvp->pvp_pproc) {
#endif
			if (strlen(name) < TASK_COMM_LEN) {
				strcpy(pvp->pvp_pproc->comm, name);
			} else {
				strncpy(pvp->pvp_pproc->comm, name, TASK_COMM_LEN-1);
				pvp->pvp_pproc->comm[TASK_COMM_LEN-1]= '\0';
#ifdef DEBUG
				printk(KERN_WARNING "%s: "
					"Truncated daemon name %s\n",
				       funcname, name);
#endif
			}
		}
		VPROC_UNLOCK_EXCL(v, funcname);
		VPROC_RELE(v, funcname);
		return 0;
	}
#else /* !VPROC */
	retval = kernel_thread((int (*)(void *))daemon_func,
		      daemon_args, CLONE_FS | SIGCHLD);
	if (retval < 0)
		return retval;
	else {
		struct task_struct *p;

		read_lock(&tasklist_lock);
		p = find_task_by_pid(retval);
		if (p) {
			if (strlen(name) < TASK_COMM_LEN) {
				strcpy(p->comm, name);
			}
			else {
				strncpy(p->comm, name, TASK_COMM_LEN-1);
				p->comm[TASK_COMM_LEN-1]= '\0';
#ifdef DEBUG
				printk(KERN_DEBUG "%s: "
				       "Truncated daemon name %s\n",
				       funcname, name);
#endif /* DEBUG */
			}
		}
		read_unlock(&tasklist_lock);
		return 0;
	}
#endif /* !VPROC */
}

int
spawn_daemon_proc(char *name, void (*daemon_func)(void *), void *daemon_args)
{
	return __spawn_daemon_proc(name, daemon_func, daemon_args,
				   (char *)__FUNCTION__);
}

static inline struct task_struct *
find_task_by_name(char *name)
{
	struct task_struct *p, *tsk = NULL;

	for_each_process(p) {
		if (strcmp(name, p->comm) == 0) {
			tsk = p;
			break;
		}
	}
	return tsk;
}

/*
 * void exit_daemon_proc()
 *	Generic TNC interface for exiting daemon processes.
 *
 * Calling/Exit State:
 *	No locks held upon entry or exit.
 */
void exit_daemon_proc(void)
{
#ifdef CONFIG_VPROC
	struct vproc *vp = current->p_vproc;

	/* pid 2 will reap system processes so change its parent to 2 */
	if (current->ppid != 2)
		VPOP_RECLAIM_CHILD(vp, 2, 0, 0);
 	do_exit(0);
#else /* !VPROC */
	write_lock_irq(&tasklist_lock);
	REMOVE_LINKS(current);
	current->parent = current->real_parent = child_reaper;
	SET_LINKS(current);
	write_unlock_irq(&tasklist_lock);
	do_exit(0);
#endif /* !VPROC */
}

/*
 * int spawn_daemon_thread()
 *	Generic TNC interface for spawning daemon threads.
 *
 * Calling/Exit State:
 *	No locks held upon entry or exit.
 *
 *	On some platforms, this routine may actually wrap for
 *	spawn_daemon_proc().
 *
 *	Processes spawned via this routine should avoid spawning
 *	other daemon processes to avoid the change of kernel
 *	overflow on some platforms.
 *
 *	The 'name' parameter must be a pointer to a string in
 *	memory that won't be free'd (i.e. typically a constant
 *	string expression).
 */

int
spawn_daemon_thread(char *name, void (*daemon_func)(void *), void *daemon_args)
{
	return __spawn_daemon_proc(name, daemon_func, daemon_args,
				   (char *)__FUNCTION__);
}

/*
 * void exit_daemon_thread()
 *	Generic TNC interface for exiting daemon threads.
 *
 * Calling/Exit State:
 *	No locks held upon entry or exit.
 */
void
exit_daemon_thread(void)
{
	exit_daemon_proc();
}

void nsc_daemonize(void)
{
#if 0
	struct task_struct *p = current;

	/*
	 * If we were started as result of loading a module, close all of the
	 * user space pages.  We don't need them, and if we didn't close them
	 * they would be locked into memory.
	 */
	exit_mm(p);

	set_special_pids(2, 2);

	CLEAR_TTY(p);

	/*
	 * Become as two with the init task: copy the fs struct so
	 * the daemon has it's own umask which is zero. We can use the
	 * init_task's files.
	 */

	exit_fs(p);	/* p->fs->count--; */
	p->fs = copy_fs_struct(init_task.fs);
	if (!p->fs)
		panic("%s: Cannot copy init_task.fs\n", __FUNCTION__);
	p->fs->umask = 0;
 	exit_files(p);
	p->files = init_task.files;
	atomic_inc(&p->files->count);

	set_fs(KERNEL_DS);

	/* Increase PRIO for all nsc daemons */
	(void)set_daemon_prio(MAX_RT_PRIO-4, SCHED_FIFO);

#ifdef CONFIG_VPROC
	VPOP_RECLAIM_CHILD(p->p_vproc, 2, 0, 0);
#endif
#else
	daemonize("%s", current->comm);

	set_fs(KERNEL_DS);

	/* Increase PRIO for all nsc daemons */
	(void)set_daemon_prio(MAX_RT_PRIO-4, SCHED_FIFO);
#endif
}

int set_daemon_prio(int prio, int policy)
{
	int ret;
	struct sched_param param;
	struct task_struct *t = current;
	kernel_cap_t save_cap;

	save_cap = t->cap_effective;
	t->cap_effective = CAP_FULL_SET;
	param.sched_priority = prio;
	ret = sys_sched_setscheduler(0, policy, &param);
	t->cap_effective = save_cap;
	if (ret < 0)
		printk(KERN_WARNING
		       "%s: error %d setting priority for pid %d to %d\n",
		       __FUNCTION__, -ret, current->pid, prio);

	return ret;
}
