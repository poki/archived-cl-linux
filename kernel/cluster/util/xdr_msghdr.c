/*
 * 	XDR implementation on kernel msghdr structs.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 *	Copyright (c) 1982, 1986, 1988
 *	The Regents of the University of California
 *	All Rights Reserved.
 *	Portions of this document are derived from
 *	software developed by the University of
 *	California, Berkeley, and its contributors.
 */

/*
 *	xdr_msghdr.c, xdr implementation on kernel msghdr structs.
 *
 */

/*
 * Modification History:
 * 22jun01 aneesh.kumar@digital.com
 *	xdrmsghdr_getpos()
 *	xdrmsghdr_setpos()
 *	Removed  long - int  casting issues while finding the
 *	difference  between two address
 *
 * 01apr01	ksung
 * - ported to use Linux msghdr structs.
 *
 * 16jun99	nadeem		ul99-16905
 * - fixed problem whereby xdrmblk_getlong() could not handle an XDR unit
 *   (ie: a long) crossing an mblk boundary.  Fix by calling xdrmblk_getbytes()
 *   to handle that circumstance.  Also fixed xdrmblk_putlong() in a
 *   corresponding way.  Manifested as errors in osmlog of:
 *   "xdr_mblk: long crosses mblks!".
 *
 *
 */

#include <linux/types.h>
#include <cluster/ics.h>
#include <cluster/rpc/xdr.h>

extern u_long	nscrpc_bufchunk[];
extern bool_t	xdr_msghdrmoremem(XDR *);

bool_t	xdrmsghdr_getlong(XDR *, long *);
bool_t	xdrmsghdr_putlong(XDR *, const long *);
bool_t	xdrmsghdr_getbytes(XDR *, char *, u_int);
bool_t	xdrmsghdr_putbytes(XDR *, const char *, u_int);
u_int	xdrmsghdr_getpos(const XDR *);
bool_t	xdrmsghdr_setpos(XDR *, u_int);
int32_t *xdrmsghdr_inline(XDR *, int);
void	xdrmsghdr_destroy(XDR *);

/*
 * Xdr on mblks operations vector.
 */
struct xdr_ops xdrmsghdr_ops = {
	xdrmsghdr_getlong,
	xdrmsghdr_putlong,
	xdrmsghdr_getbytes,
	xdrmsghdr_putbytes,
	xdrmsghdr_getpos,
	xdrmsghdr_setpos,
	xdrmsghdr_inline,
	xdrmsghdr_destroy
};

/*
 * xdrmsghdr_init(xdrs, m, op)
 *	Initialize an xdr stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns void.
 *
 * Description:
 *	This routine initializes an xdr stream from/to
 *	a kernel message header.
 *
 * Parameters:
 *
 *	xdrs			# stream to initialize
 *	m			# message header
 *	op			# decode or encode
 *
 */
void
xdrmsghdr_init(XDR *xdrs, struct msghdr *m, enum xdr_op op)
{
	xdrs->x_op = op;
	xdrs->x_ops = &xdrmsghdr_ops;
	SSI_ASSERT(m->msg_iov[0].iov_base);
	xdrs->x_base = (char *)m->msg_iov;
	xdrs->x_public = (char *)m;

	if (op == XDR_DECODE) {
		/*
		 * The msghdr has data, and we want to
		 * read it.
		 */
		xdrs->x_private = m->msg_iov[0].iov_base;
		xdrs->x_handy = nscrpc_bufchunk[0];
	} else {
		/*
		 * The msghdr is for writing into.
		 */
		xdrs->x_private = m->msg_iov[0].iov_base;
		xdrs->x_handy = nscrpc_bufchunk[0];
	}
}

/*
 * xdrmsghdr_destroy(xdrs)
 *	Destroy an xdr stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns a void.
 *
 * Description:
 *	This routine destroys an xdr stream.
 *	It actually does nothing as we did not
 * 	allocate anything on its initialization.
 *
 * Parameters:
 *
 *	xdrs			# stream to destroy
 *
 */
/* ARGSUSED */
void
xdrmsghdr_destroy(XDR *xdrs)
{
}

/*
 * xdrmsghdr_getlong(xdrs, lp)
 *	Decode a long from stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns TRUE on success, FALSE on failure.
 *
 * Description:
 *	This routine reads a long from an xdr stream
 * 	which implies decoding it.
 *
 * Parameters:
 *
 *	xdrs			# stream to decode long from
 *	lp			# storage for long
 *
 */
bool_t
xdrmsghdr_getlong(XDR *xdrs, long *lp)
{
	if ((xdrs->x_handy -= sizeof(int32_t)) < 0) {
		int32_t tmp_long;

		xdrs->x_handy += sizeof(int32_t);

		/*
		 * Let xdrmsghdr_getbytes() take care of crossing over
		 * iovec boundaries.
		 */

		if (xdrmsghdr_getbytes(xdrs, (char *)&tmp_long,
				       sizeof(int32_t)) == FALSE)
			return(FALSE);

		*lp = ntohl(tmp_long);
		return (TRUE);
	}

	/* LINTED pointer alignment */
	*lp = ntohl(*((int32_t *)(xdrs->x_private)));
	xdrs->x_private += sizeof(int32_t);

	return (TRUE);
}

/*
 * xdrmsghdr_putlong(xdrs, lp)
 *	Encode a long from stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns TRUE on success, FALSE on failure.
 *
 * Description:
 *	This routine writes a long to an xdr stream
 * 	which also implies encoding it.
 *
 * Parameters:
 *
 *	xdrs			# stream to encode long into
 *	lp			# long to encode
 *
 */
bool_t
xdrmsghdr_putlong(XDR *xdrs, const long *lp)
{
	if ((xdrs->x_handy -= sizeof(int32_t)) < 0) {
		int32_t tmp_long;

		xdrs->x_handy += sizeof(int32_t);

		/*
		 * Let xdrmsghdr_putbytes() take care of crossing over
		 * iovec boundaries.
		 */

		tmp_long = htonl(*lp);
		return (xdrmsghdr_putbytes(xdrs, (char *)&tmp_long,
					   sizeof(int32_t)));
	}

	/* LINTED pointer alignment */
	*(int32_t *)xdrs->x_private = htonl(*lp);
	xdrs->x_private += sizeof(int32_t);
	/*
	 * CI_XXX:
	 * When encoding, we keep track of exact length of encoded data in
	 * the msghdr iovecs. This allows us to send only the encoded data
	 * rather than the whole nsc_bufchunk length.
	 */
	((struct iovec *)xdrs->x_base)->iov_len += sizeof(int32_t);

	return (TRUE);
}

/*
 * xdrmsghdr_getbytes(xdrs, addr, len)
 *	Decode bytes from a xdr stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns TRUE on success, FALSE on failure.
 *
 * Description:
 *	This routine decodes bytes from an xdr stream.
 *
 * Parameters:
 *
 *	xdrs			# stream to decode bytes from
 *	addr			# address of buffer to get bytes in
 *	len			# number of bytes to get
 *
 */
bool_t
xdrmsghdr_getbytes(XDR *xdrs, char *addr, u_int len)
{
	while ((xdrs->x_handy -= len) < 0) {
		if ((xdrs->x_handy += len) > 0) {
			memcpy(addr, xdrs->x_private, (u_int)xdrs->x_handy);
			addr += xdrs->x_handy;
			len -= xdrs->x_handy;
		}
		if ((struct iovec *)xdrs->x_base != NULL) {
			xdrs->x_base += sizeof(struct iovec);
			xdrs->x_private = (char *)((struct iovec *)xdrs->x_base)->iov_base;
			xdrs->x_handy = ((struct iovec *)xdrs->x_base)->iov_len;
		} else {
			return (FALSE);
		}
	}

	memcpy(addr, xdrs->x_private, (u_int)len);
	xdrs->x_private += len;

	return (TRUE);
}

/*
 * xdrmsghdr_putbytes(xdrs, addr, len)
 *	Encode bytes into an xdr stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns TRUE on success, FALSE on failure.
 *
 * Description:
 *	This routine encodes bytes nto an xdr stream.
 *
 * Parameters:
 *
 *	xdrs			# stream to encode bytes into
 *	addr			# addr of bytes to encode
 *	len			# number of bytes to encode
 *
 */
bool_t
xdrmsghdr_putbytes(XDR *xdrs, const char *addr, u_int len)
{
	while ((xdrs->x_handy -= len) < 0) {
		if ((xdrs->x_handy += len) > 0) {
			memcpy(xdrs->x_private, addr, (u_int)xdrs->x_handy);
			addr += xdrs->x_handy;
			len -= xdrs->x_handy;
			((struct iovec *)xdrs->x_base)->iov_len += xdrs->x_handy;
		}
		if ((struct iovec *)xdrs->x_base != NULL) {
			if (xdr_msghdrmoremem(xdrs) == FALSE)
				return FALSE;
		} else {
			return (FALSE);
		}
	}

	memcpy(xdrs->x_private, addr, len);
	xdrs->x_private += len;
	((struct iovec *)xdrs->x_base)->iov_len += len;

	return (TRUE);
}

/*
 * xdrmsghdr_getpos(xdrs)
 *	Get byte offset in xdr stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns the byte offset in xdr stream.
 *
 * Description:
 *	Get byte offset in xdr stream.
 *
 * Parameters:
 *
 *	xdrs			# xdr stream
 *
 */
u_int
xdrmsghdr_getpos(const XDR *xdrs)
{
	return ((u_int)((u_long)xdrs->x_private -
		(u_long)((struct iovec *)(xdrs->x_base))->iov_base));
}

/*
 * xdrmsghdr_setpos(xdrs, pos)
 *	Skip pos bytes in xdr stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns TRUE on success, FALSE on failure.
 *
 * Description:
 *	Skip pos bytes in xdr stream.
 *
 * Parameters:
 *
 *	xdrs			# xdr stream to skip bytes in
 *	pos			# number of bytes to skip
 *
 */
bool_t
xdrmsghdr_setpos(XDR *xdrs, u_int pos)
{
	/*
	 * calculate the new address from the base
	 */
	char *newaddr = (char *)(xdrs->x_private + pos);

	/*
	 * calculate the last valid address in the mblk
	 */
	char *lastaddr = (char *)(xdrs->x_private + xdrs->x_handy);

	if (newaddr > lastaddr)
		return (FALSE);

	xdrs->x_private = newaddr;
	xdrs->x_handy = lastaddr - newaddr;

	/*
	 * Maintain coherency between the x_handy buffer size and
	 * the iov_len for the iovec.
	 */
	if (xdrs->x_op == XDR_ENCODE) {
		((struct iovec *)xdrs->x_base)->iov_len += pos;
	}

	return (TRUE);
}

/*
 * xdrmsghdr_inline(xdrs, len)
 *	Check if there is enough space for encoding len bytes
 *	in xdr stream.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *
 *	Returns NULL if space is not available, else returns
 *	address to start from.
 *
 * Description:
 *	This routine checks if there is enough space for
 *	encoding len bytes in an xdr stream.
 *
 * Parameters:
 *
 *	xdrs			# xdr stream
 *	len			# length to check for
 *
 */
int32_t *
xdrmsghdr_inline(XDR *xdrs, int len)
{
	int32_t	*buf = 0;

	if (xdrs->x_handy >= len) {
		xdrs->x_handy -= len;
		/* LINTED pointer alignment */
		buf = (int32_t *) xdrs->x_private;
		xdrs->x_private += len;
		if (xdrs->x_op == XDR_ENCODE) {
			((struct iovec *)xdrs->x_base)->iov_len += len;
		}
	}

	return (buf);
}
