/*
 * 	CLMS client support code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains code that runs on the CLMS client node.
 *
 * The CLMS client responds to RPCs from the CLMS master, and keeps a
 * local database about services provided by nodes of the cluster.
 */

#include <linux/delay.h>
#include <linux/reboot.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/nodelist.h>
#include <cluster/icsgen.h>
#include <cluster/node_monitor.h>
#include <linux/timer.h>
#include <linux/sched.h>
#ifdef CLMS_CLIENT_PERFORM_NODEUP_DISCOVER_MOUNTS
#include <cluster/ssisys.h>
#endif

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>


static void clms_client_declare_master_up(void);
static void clms_client_nodeup_proc(void *);
static void clms_client_nodedown_proc(void *);
static void clms_client_intent_proc(void *);
static void clms_client_perform_nodeup(clusternode_t, int);
static void clms_client_sync_masterparams(void);

/*
 * clms_client_init()
 *	Initialize the client side of the CLMS subsystem.
 *
 * Description:
 *	This initialization routine is called from clms_init().
 *
 *	A timeout routine for the clms client is scheduled. This will be
 *	called periodically and it will increment a timer. This timer is
 *	reset when the clms master probe arrives. If this timer value
 *	exceeds a certain predetermined value, the clms master is assumed to be
 *	to have gone down and the necessary action is taken (reboot for now).
 *
 * 	The clms_client_keepalive_timer value is set to -1. This means that
 * 	until the clms master contacts the cient, the client will probe
 *	the master.
 *
 *	The clms_client_keepalive_daemon is started. This is responsible for
 *	probing the clms master and take the necessary action should the
 *	clms master node go down.
 *
 * Calling/Exit State:
 *	Must be called after the ICS Initialization has been done.
 *
 * Parameters:
 *	None.
 */
void
clms_client_init()
{
	SSI_ASSERT(this_node != clms_master_node);

	/*
	 * Master starts monitoring us when we call
	 * ics_nodeup(clms_master_node) in clms_client_declare_master_up().
	 *
	 * Initialize the Client-side Node Monitoring daemons.
	 * "imalive" packets aren't sent until the first "imalive" packet
	 * is received from the Master node.
	 */
	nm_init();

	clms_client_declare_master_up();

	clms_client_sync_masterparams();
	/*
	 * Go try to offer key services, if we have any.
	 */
	clms_client_setup_key_services();
	clms_client_request_cluster_svrinfo();

	return;
}

void
clms_client_update_masterlist(const char *masterinfo)
{
	int error;
	clms_secondary_list_t *savelist;
	int num_secondaries;
	clms_secondary_list_t *ent;
	clms_secondary_list_t *nent;
	int mismatch;

	mismatch = 0;
	savelist = clms_secondary_list_head;
	clms_secondary_list_head = NULL;
	num_secondaries = clms_num_secondaries;
	clms_im_a_secondary = 1;
	error = clms_parse_nodelist(masterinfo, &clms_secondary_list_head,
				    &clms_num_secondaries,
				    &clms_im_a_secondary,
				    &cluster_clmsinfo);
	if (error < 0)
		panic("%s:Error %d parsing master list: %s\n",
		      __FUNCTION__, -error, masterinfo);
	if (num_secondaries != clms_num_secondaries) {
		mismatch = 1;
	} else {
#ifdef DEBUG
		printk(KERN_DEBUG "num_secondaries: %d\n", num_secondaries);
#endif /* DEBUG */
		for (ent = clms_secondary_list_head, nent = savelist;
		     ent != NULL && nent != NULL;
		     ent = ent->next, nent = nent->next) {
			if (nent->node != ent->node)
				mismatch = 1;
			if (memcmp(&(nent->addr), &(ent->addr),
				   sizeof(icsinfo_t)))
				mismatch = 1;
		}
	}
	if (mismatch)
		printk(KERN_WARNING
		       "CLMS secondary list does not match master's!"
		       " Using master's version.\n");
	for (ent = savelist; ent != NULL; ent = nent) {
		nent = ent->next;
		kfree(ent);
	}
}

static void
clms_client_sync_masterparams(void)
{
	int error, tries = 0;
	int rval;
	char *masterinfo;
	int masterinfo_len;
	struct nm_settings nm_settings;
	clusternode_t to_node;

	/* Update the master list */
	masterinfo_len = CLUSTER_CLMSINFO_SZ;
	masterinfo = kmalloc_nofail(sizeof(*masterinfo) * masterinfo_len);
	for (;;) {
		to_node = clms_master_node;
		if (to_node == this_node)
			goto out;
		else if (to_node != CLUSTERNODE_INVAL) {
			error = RCLMS_READ_MASTERPARAMS(to_node, &rval,
							&masterinfo,
							&masterinfo_len,
							&nm_settings);
		} else
			error = -EREMOTE;
		if (error == -EAGAIN) {
			nidelay(HZ/10);
			continue;
		}
		if (error == -EREMOTE) {
			nidelay(HZ);
			if (++tries > CONFIG_NODE_MONITOR_TIMEOUT_MS / 1000 + 1) {
				printk(KERN_WARNING
				       "Failed reading master list, master went down!\n");
				machine_restart(NULL);
			}
			continue;
		}
		if (error >= 0)
			error = rval;
		if (error < 0)
			/* XXX: Never reached */
			panic("%s:Error %d reading master list\n",
			      __FUNCTION__, -error);
		break;
	}
	clms_client_update_masterlist(masterinfo);
	nm_update_settings(&nm_settings, 0);
 out:
	kfree(masterinfo);
}

/*
 * clms_client_declare_master_up()
 *	Declare the clms master node as up.
 *
 * Description:
 *	Declare the clms master node as up. Call ics_nodeup().
 *	The clms master will not be contacted by the client until
 *	sometime later (after the component inits) to join the
 *	cluster.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
static void
clms_client_declare_master_up(void)
{
	int	error, printed, tries;

	SSI_ASSERT(this_node != clms_master_node);

	CLMSLOG(CLMSLOG_DECLMAST, clms_master_node, 0, 0, 0, 0, 0, 0);
	clms_set_node_status(clms_master_node, CLMS_NODE_COMING_UP);
	/*
	 * Wait for the CLMS master to do an ics_nodeup() for us.  If
	 * we have to retry, print something.  If we have to retry
	 * five times, the master probably isn't going to do it, so
	 * reprobe for the master; odds are he failed over.  If he was
	 * just very slow, this is till safe.  If we find out _we're_
	 * the master, reboot, since we can't handle this here.
	 */
	printed = tries = 0;
	do {
		error = ics_nodeup(clms_master_node);
		if (error == -EAGAIN) {
			if (++tries > 5) {
				clusternode_t old_master;

				clms_set_node_status(clms_master_node,
						     CLMS_NODE_NEVER_UP);
				ics_nodedown(clms_master_node);
				old_master = clms_master_node;
				clms_master_node = clms_find_master(
						     &clms_im_a_secondary,
						     &clms_num_secondaries,
						     &clms_secondary_list_head);
				if (clms_master_node == this_node) {
					printk(KERN_WARNING
					       "Root node changed to me!\n");
					machine_restart(NULL);
				}
				if (old_master != clms_master_node)
					printk(KERN_WARNING
					       "Root node changed from %u to %u.",
					       old_master,
					       clms_master_node);
				/* We can now answer probes from other nodes */
				clms_ready_for_probe = 2;
				clms_set_node_status(clms_master_node,
						     CLMS_NODE_COMING_UP);
				tries = 0;
				printed = 0;
			}
			else {
                               	if (!printed) {
					printed = 1;
					printk(KERN_NOTICE "Waiting to join the cluster...\n");
                               	}
                               	idelay(5 * HZ);
			}
		}
		if (error == 0 && printed) {
			printk(KERN_WARNING "Coming up...\n");
		}
	} while (error == -EAGAIN);
	if (error) {
		panic("clms_client_declare_master_up: ics_nodeup failed %d\n",
		      error);
	}
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_node_info[clms_master_node].current_state |=
					CLMS_NODE_ICS_UP;
	clms.clms_node_info[clms_master_node].flags |=
					CLMS_NODE_ICS_NODEUP_DONE;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return;
}


/*
 * clms_client_request_cluster_svrinfo()
 *	Request the clms master for cluster service information.
 *
 * Description:
 *	Sends a request to the clms master to get the information about
 *	CLMS key services (like root fs).
 *
 * Parameters:
 *	None.
 */
int
clms_client_request_cluster_svrinfo(void)
{
	int error;

	do {
		error = RCLMS_CLUSTER_SVRINFO_REQUEST_MSG(clms_master_node,
							  this_node);
		if (error == -EREMOTE)
			idelay(10);
	} while(error == -EREMOTE);
	if (error) {
		panic("clms_client_request_cluster_svrinfo: "
		      "Msg to request cluster svrinfo failed "
		      "error = %d \n", error);
	}

	return 0;
}

/*
 * clms_client_waitfor_resource_info()
 *	Wait until information about all the key services are available.
 *
 * Description:
 *	Waits until information about key cluster resources (like root
 *	file system) are available. Note that it does not wait for the
 *	resources to become available for use.
 *
 * Parameters:
 *	None.
 */
int
clms_client_waitfor_resource_info(void)
{
	clms_waitfor_key_service_info();
	return 0;
}

/*
 * clms_client_join_cluster()
 *	Make a request to the clms master to join the cluster.
 *
 * Description:
 *	This routine is called after all services have called
 *	their init routines to register with clms and ics.
 *
 *	At this point connections to clms channel, reply channel, and
 *	a few other priority channel have been made.
 *
 *	Send an rpc to the clms master to join the cluster. The clms master
 *	drives the process of joining from there.
 *
 *	The clms master will start the "Node Up" processing upon receiving
 *	rpc. This include all the ics connections being established.
 *
 * Parameters
 *	None.
 */
int
clms_client_join_cluster()
{

	int rval;
	int error, i;
	clock_t ticks;
	clusternode_t *key_servers = NULL;
	int num_key_servers = 0;
	int num_key_servers_ics;
	icsinfo_t *key_icsinfo = NULL;

	CLMSLOG(CLMSLOG_CLIJOIN, 0, 0, 0, 0, 0, 0, 0);
	ics_nodeready(clms_master_node);

	/*
	 * The LTI requires that for any out of line reply data that might
	 * be returned, the address and length of the buffer into which the
	 * data will be received must be known at the time an RPC is sent.
	 * We use the worst-case values, since joining a cluster isn't a
	 * performance-critical operation.
	 */
	num_key_servers = num_key_servers_ics = NSC_MAX_NODE_VALUE + 1;
	key_servers = kmalloc_nofail(sizeof(clusternode_t) * num_key_servers);
	key_icsinfo = kmalloc_nofail(sizeof(icsinfo_t) * num_key_servers_ics);

	error = RCLMS_WANT_TO_JOIN(clms_master_node,
				   &rval,
				   &ticks,
				   &key_servers,
				   &num_key_servers,
				   &key_icsinfo,
				   &num_key_servers_ics,
				   this_node,
				   num_possible_cpus(),
				   num_online_cpus(),
				   loops_per_jiffy,
				   num_physpages);
	if ( (error) || (rval) ) {
		panic("clms_client_join_cluster: rpc to clms master failed error %d rval %d\n", error, rval);
	}

	/*
	 * Compute the difference between our lbolt and the lbolt of the
	 * first booted root node, as passed to us from the CLMS master.
	 * See the comment at the declaration of clms_ticks_delta in
	 * clms_master.c.
	 */
	clms_ticks_delta = ticks - TICKS();
	for (i = 0; i < num_key_servers; i++) {
		icsinfo_t dummy;

		if (ics_geticsinfo(key_servers[i], &dummy) == -EINVAL) {
			error = ics_seticsinfo(key_servers[i],
					       &key_icsinfo[i]);
			if (error != 0) {
				panic("clms_client_join_cluster(): ics_seticsinfo:  error %d node %u\n", error, key_servers[i]);
			}
		}
	}
	/*
	 * Perform nodeups for the list of key server nodes we received from
	 * the CLMS master.
	 */
	CLMSLOG(CLMSLOG_JOINKEYS,
		num_key_servers,
		num_key_servers > 0 ? key_servers[0] : 0,
		num_key_servers > 1 ? key_servers[1] : 0,
		num_key_servers > 2 ? key_servers[2] : 0,
		num_key_servers > 3 ? key_servers[3] : 0,
		num_key_servers > 4 ? key_servers[4] : 0,
		num_key_servers > 5 ? key_servers[5] : 0);
	for (i = 0; i < num_key_servers; i++) {
		/* Redundant if the key server is the master or this node. */
		if (key_servers[i] == clms_master_node ||
		    key_servers[i] == this_node)
			continue;
		clms_client_perform_nodeup(key_servers[i],
					   CLMS_NODE_INFORM_COMING_UP);
	}

	kfree(key_servers);
	kfree(key_icsinfo);
	return 0;
}

/*
 * Routines that handle the CLMS Client joining the cluster.
 */
/*
 * rclms_cluster_svrinfo()
 *	Get information about key cluster services/servers in the NSC
 *	cluster.
 *
 * Description:
 *	This routine gets called when the clms master sends the information
 *	about the CLMS key services in the cluster. The information is
 *	processed. This routine gets called very early. The information
 *	gathered here helps the NSC components to initialize. For example,
 *	once this routine is called (may be) root file system node is known.
 *
 * Calling/Exit State:
 *	Server routine for the RPC RCLMS_CLUSTER_SVRINFO sent by the clms
 *	master.
 *
 * Parameters:
 *	node			this node
 *
 *	rval_p			[OUT] return value
 *
 *	svclist			List of key services in the cluster according
 *				to the CLMS Master.
 *
 *	num_svcs		Number of key services in svclist
 *
 *	svrlist			The key servers providing each key service
 *
 *	num_svrs		Number of key servers in svrlist
 *
 *	svr_typelist		type of each key server
 *
 *	num_types		number of entries in the above array
 *
 *	svr_statuslist  	status of each key server/service
 *
 *	num_statuses    	number of entries in the above array
 *
 * Returns:
 *	0.
 */
int
rclms_cluster_svrinfo(
	clusternode_t	node,
	int		*rval_p,
	int		*svclist,
	int		num_svcs,
	clusternode_t	*svrlist,
	int		num_svrs,
	int		*svr_typelist,
	int		num_types,
	int		*svr_statuslist,
	int		num_statuses)
{
	int i;

	/*
	 * Put the information about services provided by other nodes into
	 * the local database. This also signals everybody waiting for
 	 * such information.
	 */
	SSI_ASSERT(num_svcs == num_svrs);
	SSI_ASSERT(num_svcs == num_types);
	SSI_ASSERT(num_svcs == num_statuses);

	for (i = 0; i < num_svcs; i++) {
		clms_vset_cluster_svr(svclist[i],      svrlist[i],
				      svr_typelist[i], svr_statuslist[i]);
	}

	return 0;
}


/*
 * rclms_join()
 *	CLMS interaction to allow a node to join a cluster
 *
 * Description:
 *	This is the RPC service procedure for the RPC that the CLMS master
 *	node sends out to each CLMS client node as it comes up.
 *
 *	In this RPC, the clms master gives only the information about key
 *	server nodes (nodes providing key srvices like ROOT FS). The
 *	information about other nodes are given when this node finally
 *	declares itself fully up.
 *
 *	This node may contact the key server nodes to avail the services
 *	provided by them. This will enable the client node to come up. The
 *	CLMS Master also informs the key nodes that this node is coming up.
 *	Hence, the key nodes can allow this node to contact them.
 *
 *	The list of key server nodes are queued for the clms client nodeup
 *	daemon to handle.
 *
 * Parameters:
 *
 *	node			this node
 *
 *	rval_p			[OUT] return value
 *
 *	key_nodelist		key server nodes of the cluster that are
 *				currently up
 *
 *	num_nodes		size of above array
 *
 *	key_icsinfolist		ICS information for each node in key_nodelist
 *
 *	num_icsinfos		Number of entries in the above array
 *
 *	svclist			List of key services in the cluster according
 *				to the CLMS Master.
 *
 *	num_svcs		Number of key services in svclist
 *
 *	svrlist			The key servers providing each key service
 *
 *	num_svrs		Number of key servers in svrlist
 *
 *	svr_typelist		type of each key server
 *
 *	num_types		number of entries in the above array
 *
 *	svr_statuslist  	status of each key server/service
 *
 *	num_statuses    	number of entries in the above array
 *
 *
 * Return value:
 *	Must be zero.
 */
int
rclms_join(
	clusternode_t	node,
	int		*rval_p,
	int		*svclist,
	int		num_svcs,
	clusternode_t	*svrlist,
	int		num_svrs,
	int		*svr_typelist,
	int		num_types,
	int		*svr_statuslist,
	int		num_statuses,
	clusternode_t	*servers,
	int		num_servers)
{
	int		i;

	CLMSLOG(CLMSLOG_GOTJOIN, 0, 0, 0, 0, 0, 0, 0);
	/*
	 * Put the information about key services provided by other nodes into
	 * the local database.
	 */
	SSI_ASSERT(num_svcs == num_svrs);
	SSI_ASSERT(num_svcs == num_types);
	SSI_ASSERT(num_svcs == num_statuses);
	SSI_ASSERT(num_servers == clms_num_key_svcs);

	for (i = 0; i < num_svcs; i++) {
		clms_vset_cluster_svr(svclist[i],      svrlist[i],
				      svr_typelist[i], svr_statuslist[i]);
	}

	/*
	 * Mark this node as half up, so that clms_client_startup_node() can
	 * proceed.
	 */
	clms_set_node_status(this_node, CLMS_NODE_HALF_UP);

	/*
	 * Do failover for any formerly unserved key services we might have
	 * received by coming up.
	 *
	 * NSC_XXX	This currently doesn't work because of ordering
	 *		constraints.  Additionally, we don't want to do
	 *		this here, this should be a side effect of our
	 *		joining, and should be done via the
	 *		RCLMS_REASSIGN_SERVICES RPC.
	 *
	for (i = 0; i < clms_num_key_svcs; i++) {
		if (servers[i] != CLUSTERNODE_INVAL) {
			SSI_ASSERT(servers[i] == this_node);
			clms_vset_cluster_svr(i,
					      this_node,
					      CLUSTER_PRIMARY_SERVER,
					      CLUSTER_SERVER_NOT_READY);
			clms_key_service_failover(i, CLUSTERNODE_INVAL);
		}
	}
	 */
	*rval_p = 0;
	return 0;
}

/*
 * clms_client_perform_nodeup()
 *	Perform a nodeup for the given node.
 *
 * Description:
 *	This function is called to do a nodeup for a given node.
 *
 *	The ics_nodeup() routine is called to establish all the connections
 *	to the node, all the nodeup routines of the clms services are called,
 *	and the node is marked as either CLMS_NODE_UP or CLMS_NODE_HALF_UP,
 *	depending on the parameter.  Note that only one node can be handled
 *	at a time; this is enforced by locking.
 *
 * Parameters:
 *	None.
 */
static void
clms_client_perform_nodeup(
	clusternode_t new_node,
	int node_status)
{
	int error, status, do_icsnodeup, do_icsready;
	struct clms_transition_data *cdp;

	cdp = clms_alloc_trans_data();
	clms_init_trans_data(cdp, new_node);

	clms_set_nodeup_inprogress();
	LOCK_COND_LOCK(&clms.clms_node_lock);
	status = clms_get_node_status(new_node);
	CLMSLOG(CLMSLOG_CLINODEUP, new_node, status, node_status, 0, 0, 0, 0);

	/*
	 * If the status of this node is CLMS_NODE_HALF_UP and it's not
	 * this_node, we've been here already.  Just skip it.  If the status
	 * is CLMS_NODE_UP, we've definitely been here before.
	 */
	if ((status & CLMS_NODE_UP) ||
	    ((status & CLMS_NODE_HALF_UP) &&
	     new_node != this_node)) {
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		clms_free_trans_data(cdp);
		clms_clear_nodeup_inprogress();
		return;
	}

	/*
	 * If the status of this node is "going down", the
	 * new_node must have gone down while coming up.
	 * Don't bother doing any ICS stuff.
	 */
	if (status & CLMS_NODE_GOING_DOWN) {
		CLMSLOG(CLMSLOG_DOWNUP,
			new_node,
			status,
			node_status,
			0, 0, 0, 0);
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		clms_free_trans_data(cdp);
		clms_clear_nodeup_inprogress();
		return;
	}

	if (new_node != this_node &&
	    new_node != clms_master_node)
		clms_set_node_status_l(new_node,
				       CLMS_NODE_COMING_UP);

	/*
	 * Do an ics_nodeup() for this node; this may race
	 * with rclms_inform_key_nodes(), thus the interlock.
	 */
	do_icsnodeup = 0;
	do_icsready = 0;
	error = 0;
	if (new_node != this_node &&
	    !(clms.clms_node_info[new_node].flags &
	   			 CLMS_NODE_ICS_NODEUP_DONE)) {
		clms.clms_node_info[new_node].flags |= CLMS_NODE_ICS_NODEUP_DONE;
		do_icsnodeup = 1;
	}
	if (new_node != clms_master_node && new_node != this_node &&
	    !(clms.clms_node_info[new_node].flags & CLMS_NODE_ICS_READY_DONE)) {
		clms.clms_node_info[new_node].flags |= CLMS_NODE_ICS_READY_DONE;
		do_icsready = 1;
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	if (do_icsnodeup) {
		/*
 		 * Tell ICS that this node is coming up.
 		 */
		error = ics_nodeup(new_node);
		if (error == -EAGAIN) {
			CLMSLOG(CLMSLOG_DOWNUP,
				new_node,
				status,
				node_status,
				0, 0, 0, 0);
			/*
			 * new_node must have gone down
			 * while coming up.
			 */
			clms_free_trans_data(cdp);
			clms_clear_nodeup_inprogress();
			return;
		}
		SSI_ASSERT(error == 0);
	}
	if (do_icsready)
		ics_nodeready(new_node);

	/*
 	 * If the node hasn't already gone down, call the nodeup routine of
 	 * the services and finish up.
 	 */
	if (error == 0) {
		/*
		 * one node at a time.
		 */
		error = clms_call_all_svcs(cdp, CLMS_CALL_NODEUP);
		if (error) {
			printk(KERN_WARNING"clms_client_perform_nodeup: "
			       " nodeup call failed error is %d"
			       " node = %u\n",
			       error,
			       new_node);
		}
		if (new_node != this_node) {
			/*
			 * Mark the node with the passed state.
			 */
			LOCK_COND_LOCK(&clms.clms_node_lock);
			status = clms_get_node_status(new_node);
			/*
			 * If we raced with nodedown, don't set the
			 * status.
			 */
			if ((status & CLMS_NODE_GOING_DOWN) == 0) {
				clms_set_node_status_l(new_node,
						       node_status);
				BROADCAST_CONDITION(&clms.clms_node_cond);
			}
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
		}
	}
	clms_free_trans_data(cdp);
	clms_clear_nodeup_inprogress();
	CLMSLOG(CLMSLOG_CLINUDONE, new_node, status, node_status, 0, 0, 0, 0);
}

/*
 * clms_client_nodeup_proc()
 *	This process is run to process a nodeup sent from the CLMS master.
 *
 * Description:
 *	Calls clms_client_perform_nodeup() to do the real work.  This is
 *	only used when we need to handle nodeups that were deferred because
 *	we were missing a transition, as determined by the CTV.
 *
 * Calling/Exit State:
 *	Process spawned when a queued nodeup event is to be processed.
 *
 * Parameters:
 *	argp	Pointer to clms_cli_tr_data_t.
 */
static void
clms_client_nodeup_proc(void *argp)
{
	clusternode_t new_node;

	nsc_daemonize();

	new_node = (long)argp;
	/*
	 * Just call clms_client_perform_nodeup(); there is interlocking
	 * there (via a call to clms_set_nodeup_inprogress()) that prevents
	 * more than one from running at a time.
	 */
	clms_client_perform_nodeup(new_node, CLMS_NODE_HALF_UP);
	/*
	 * The caller gets this lock for each instance of the process.
	 * Unlock it now.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
#ifdef CLMS_CLIENT_PERFORM_NODEUP_DISCOVER_MOUNTS
	if (new_node != clms_master_node) 
		/* Re-discover udev on non-init nodes because
		 * non-init nodes mount udev before postroot.
		 */
		(void) do_discover_mounts(NULL, new_node);
#endif
	exit_daemon_proc();
}

/*
 * clms_client_startup_node()
 *	Declare that this node is fully up.
 *
 * Description:
 *	This function is called from the end of main(). It waits for
 *	any local resources to be ready (for example, if this node is the
 *	root file system node, it waits for the root file system to be
 *	verified using fsck and mounted read/write). It then issues the
 *	rclms_send_nodeup_info rpc to the clms master node to declare itself
 *	fully up. It gets back a list of all the "Up" nodes in the cluster.
 *	The list is queued for the clms client nodeup daemon to handle.
 *
 * Parameters:
 *	None.
 */
int
clms_client_startup_node()
{
	int 		error;
	int 		rval;
	clusternode_t 	*downlist, *nodelist, *halflist;
	clms_api_state_t *apistate;
	clms_transition_t *hist;

	icsinfo_t  	*icsinfolist, *halficsinfolist;
	/* char		*masterinfo = NULL; */

	int 		*total_cpus, *online_cpus;
	unsigned long 	*cpupwr, *total_mem;
	int		tot_len, on_len, pwr_len, mem_len;

	int		down_count;
	int		node_count;
	int		half_count;
	int		i;
	int		trans_ents;
	clms_ctv_t	ctv;
	clms_transition_t *tr;
	clms_cluster_stat_t stats;
	int		api_count;
	int		ics_count;
	int		half_ics_count;

	CLMSLOG(CLMSLOG_STARTNODE, 0, 0, 0, 0, 0, 0, 0);

	/*
	 * The LTI requires that for any out of line reply data that might
	 * be returned, the address and length of the buffer into which the
	 * data will be received must be known at the time an RPC is sent.
	 * We use the worst-case values, since joining a cluster isn't a
	 * performance-critical operation.
	 */
#define NLIST_SZ	(sizeof(clusternode_t)*(NSC_MAX_NODE_VALUE + 1))
	downlist = kmalloc_nofail(NLIST_SZ);
	nodelist = kmalloc_nofail(NLIST_SZ);
	halflist = kmalloc_nofail(NLIST_SZ);
	icsinfolist = kmalloc_nofail(sizeof(icsinfo_t)
				     * (NSC_MAX_NODE_VALUE + 1));
	halficsinfolist = kmalloc_nofail(sizeof(icsinfo_t)
					 * (NSC_MAX_NODE_VALUE + 1));
	down_count = node_count = half_count =
			ics_count = half_ics_count = NSC_MAX_NODE_VALUE + 1;

	hist = kmalloc_nofail(sizeof(clms_transition_t) * CLMS_TRANS_HIST_SZ);
	trans_ents = CLMS_TRANS_HIST_SZ;

	apistate = kmalloc_nofail(sizeof(clms_api_state_t)
				  * NSC_MAX_NODE_VALUE);
	total_cpus = kmalloc_nofail(sizeof(*total_cpus) * (NSC_MAX_NODE_VALUE));
	online_cpus = kmalloc_nofail(sizeof(*online_cpus) * (NSC_MAX_NODE_VALUE));
	cpupwr = kmalloc_nofail(sizeof(*cpupwr) * (NSC_MAX_NODE_VALUE));
	total_mem = kmalloc_nofail(sizeof(*total_mem) * (NSC_MAX_NODE_VALUE));
	api_count = tot_len = on_len = pwr_len = mem_len = NSC_MAX_NODE_VALUE;

	/*
	 * Wait for this node to be half up before continuing; this happens
	 * in rclms_join().
	 */
	(void)clms_waitfor_nodeup(this_node, B_FALSE);
	/*
	 * Don't let regular nodeup or nodedown update the Cluster Transition
	 * Vector until we get it from the CLMS master.
	 */
	LOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock);
	/*
	 * Ditto for the API.
	 */
	LOCK_EXCL_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_LOCK, 1, __FILE__, __LINE__, current->pid, 0, 0, 0);
	/*
	 * Send an RPC back to the CLMS master saying that this node is
	 * up and ready for action. This RPC wakes up noded and hence
	 * must be sent only after all the initializations have been
	 * completed, including mounting the root file system.
	 */
	do {
		error = RCLMS_SEND_NODEUP_INFO(clms_master_node,
					       &rval,
					       this_node,
					       &ctv,
					       &downlist,
					       &down_count,
					       &nodelist,
					       &node_count,
					       &icsinfolist,
					       &ics_count,
					       &halflist,
					       &half_count,
					       &halficsinfolist,
					       &half_ics_count,
					       &apistate,
					       &api_count,
					       &hist,
					       &trans_ents,
					       &stats,
					       &total_cpus,
					       &tot_len,
					       &online_cpus,
					       &on_len,
					       &cpupwr,
					       &pwr_len,
					       &total_mem,
					       &mem_len);
		if (error == -EREMOTE)
			idelay(10);
		if (error != -EREMOTE && error != 0) {
			panic("clms_client_startup_node:  RCLMS_SEND_NODEUP_INFO error %d\n", error);
		}
	} while (error == -EREMOTE);
	clms_client_sync_masterparams();
	/*
	 * Get the failover lock, so that if the CLMS master dies before we
	 * mark ourselves as CLMS_STATE_COMINGUP, we won't kill ourselves;
	 * we're committed to coming up and we know about the cluster, so
	 * we should just come on up; failover will roll our transition
	 * forward, if necessary.  Note that there is a small window here
	 * between the return of the RCLMS_SEND_NODEUP_INFO RPC and our
	 * getting this lock where we might kill ourselves anyway; this is
	 * unavoidable without more work.
	 */
	LOCK_LOCK(&clms.clms_failover_lock);
	/*
	 * Set our notion of the CTV from the master's.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	clms.clms_ctv = ctv;
	/*
	 * Initialize our cluster stats and per-node API states from the
	 * master's.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_cluster_stat = stats;
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		clms.clms_node_info[i].api_state = apistate[i-1];
		clms.clms_node_info[i].total_cpus = total_cpus[i-1];
		clms.clms_node_info[i].online_cpus = online_cpus[i-1];
		clms.clms_node_info[i].cpu_power = cpupwr[i-1];
		clms.clms_node_info[i].total_memory = total_mem[i-1];
	}
	/*
	 * Fill our API history from the master's.
	 */
	LOCK_SPIN_LOCK(&clms.clms_api_hist_lock);
	for (i = 0, tr = clms.clms_api_trans_hist; i < trans_ents; i++, tr++) {
		*tr = hist[i];
	}
	if (tr == (clms.clms_api_trans_hist + CLMS_TRANS_HIST_SZ)) {
		clms.clms_api_trans_wrapped = 1;
		tr = clms.clms_api_trans_hist;
	}
	clms.clms_api_trans_ptr = tr;
	UNLOCK_SPIN_LOCK(&clms.clms_api_hist_lock);

	/*
	 * Now add the "half up" transition for this node to the transition
	 * history (this was done on the master after he packaged up his
	 * transition history for us).  Nodeups racing for new transitions
	 * are blocked because we hold the api rw lock exclusive, so things
	 * will stay in order.
	 */
	clms_api_transition_l(this_node,
			      CLMS_STATE_COMINGUP,
			      CLMS_TRANS_WHY_SYSTEM,
			      ctv);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	BROADCAST_CONDITION(&clms.clms_node_cond);
	/*
	 * Now failover can proceed, since we're in the proper state.
	 */
	UNLOCK_LOCK(&clms.clms_failover_lock);

	for (i = 0; i < node_count; i++) {
		icsinfo_t dummy;

		if (ics_geticsinfo(nodelist[i], &dummy) == -EINVAL) {
			error = ics_seticsinfo(nodelist[i], &icsinfolist[i]);
			if (error == -EAGAIN) {
				/*
				 * The node must have gone down after
				 * we received the snapshot from the
				 * master. Remove node from list so we
				 * don't perform nodeup on it.
				 */
				nodelist[i] = CLUSTERNODE_INVAL;
			}
			if (error != 0 && error != -EAGAIN) {
				panic("clms_client_startup_node():  ics_seticsinfo: node %u error %d", nodelist[i], error);
			}
		}
	}
	for (i = 0; i < half_count; i++) {
		icsinfo_t dummy;

		if (ics_geticsinfo(halflist[i], &dummy) == -EINVAL) {
			error = ics_seticsinfo(halflist[i], &halficsinfolist[i]);
			if (error == -EAGAIN) {
				/*
				 * The node must have gone down after
				 * we received the snapshot from the
				 * master. Remove node from list so we
				 * don't perform nodeup on it.
				 */
				halflist[i] = CLUSTERNODE_INVAL;
			}
			if (error != 0 && error != -EAGAIN) {
				panic("clms_client_startup_node():  ics_seticsinfo: node %u error %d", halflist[i], error);
			}
		}
	}
	/*
	 * Mark the list of down nodes from the CLMS master as down locally.
	 * Set them each as going down, then down, so the previous status
	 * looks right.  Also do an ics_nodedown() for each in case we found
	 * out about the node early, but missed the nodedown message while we
	 * were coming up.
	 */
	if (down_count > 0) {
		/*
		 * Block rclms_rgp_add_node() to prevent a race between this
		 * process and that routine.  We might be doing an
		 * ics_nodedown() here for a node we were told was down, and
		 * racing with getting Regroup info for it as it comes back
		 * up.  Don't be blocked by nodedown, just be blocked if
		 * rclms_rgp_add_node() is running.
		 */
		CLMS_TRANSITION_BLOCK(B_FALSE, CLMS_TRANS_BLOCK_REGROUP_ADD);
		for (i = 0; i < down_count; i++) {
			LOCK_COND_LOCK(&clms.clms_node_lock);
			/*
			 * Only do this if the state if a node is Never Up;
			 * if it's anything else, rclms_rgp_add_node() or
			 * someone else beat us to it.
			 */
			if (clms_get_node_status(downlist[i]) !=
							    CLMS_NODE_NEVER_UP)
				continue;
			clms_set_node_status_l(downlist[i],
					       CLMS_NODE_GOING_DOWN);
			clms_set_node_status_l(downlist[i], CLMS_NODE_DOWN);
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			ics_nodedown(downlist[i]);
		}
		CLMS_TRANSITION_UNBLOCK();
	}
	/*
	 * Perform nodeups for the list of up nodes we received from the
	 * CLMS master.
	 */
	if (node_count > 0) {
		CLMSLOG(CLMSLOG_GOTINFO,
			node_count,
			node_count > 0 ? nodelist[0] : 0,
			node_count > 1 ? nodelist[1] : 0,
			node_count > 2 ? nodelist[2] : 0,
			node_count > 3 ? nodelist[3] : 0,
			node_count > 4 ? nodelist[4] : 0,
			node_count > 5 ? nodelist[5] : 0);
		for (i = 0; i < node_count; i++) {
			if (nodelist[i] == CLUSTERNODE_INVAL)
				continue;
			clms_client_perform_nodeup(nodelist[i], CLMS_NODE_UP);
		}
	}

	/*
	 * Perform nodeups for the list of half-up nodes we received from the
	 * CLMS master.
	 */
	if (half_count > 0) {
		CLMSLOG(CLMSLOG_GOTINFO,
			half_count,
			half_count > 0 ? halflist[0] : 0,
			half_count > 1 ? halflist[1] : 0,
			half_count > 2 ? halflist[2] : 0,
			half_count > 3 ? halflist[3] : 0,
			half_count > 4 ? halflist[4] : 0,
			half_count > 5 ? halflist[5] : 0);
		for (i = 0; i < half_count; i++) {
			if (halflist[i] == CLUSTERNODE_INVAL)
				continue;
			clms_client_perform_nodeup(halflist[i],
						   CLMS_NODE_HALF_UP);
		}
	}
	UNLOCK_EXCL_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_UNLOCK, 1, __FILE__, __LINE__, current->pid,
		0, 0, 0);
	UNLOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock);


	kfree((caddr_t)downlist);
	kfree((caddr_t)nodelist);
	kfree((caddr_t)halflist);
#undef NLIST_SZ
	kfree((caddr_t)apistate);
	kfree((caddr_t)hist);
	/* kfree((caddr_t)masterinfo); */
	kfree(icsinfolist);
	kfree(halficsinfolist);
	kfree(total_cpus);
	kfree(online_cpus);
	kfree(cpupwr);
	kfree(total_mem);

	return 0;
}


/*
 * rclms_newnode_info()
 *	Receive information about a new node that has joined the cluster.
 *
 * Description:
 *	This is the RPC service procedure for the RPC that the CLMS master
 *	node sends out to all the other CLMS clients when a new node joins
 *	the cluster.
 *	The information includes: the node number of the new node, ICS
 *	information necessary for ICS to establish communication with the
 *	new node, and services provided by the new node.
 *
 *	The key services offered by the new node are stored in the local
 *	data base.
 *
 *	The clms client nodeup daemon is notified to handle the new node.
 *
 * Parameters:
 *	node		this node
 *	rval		[OUT] return value; must be set to zero
 *	clms_node	The CLMS master node that sent us the RPC.
 *	why		The reason for the transition
 *	ctv		The CTV of the transition
 *	new_node	The node that just joined the cluster.
 *	icsinfo_p	information acquired from the ICS subsystem
 *			of the new node that is required so that the ICS
 *			subsystem on this node can contact the ICS subsystem
 *			on the new node
 *	svcs		key services provided by the new node
 *	num_svcs	number of key services in above array
 *	svr_types	Type of key server for each key service
 *	num_types	Number of entries
 *	svr_statuses 	status of each of the key services
 *	num_statuses 	number of statuses in above array
 *
 * Return value:
 *	Must be zero.
 */
int
rclms_newnode_info(
	clusternode_t	node,
	int		*rval,
	clusternode_t	clms_node,
	int		why,
	clms_ctv_t	ctv,
	clusternode_t	new_node,
	icsinfo_t	*icsinfo_p,
	int		*svcs,
	int		num_svcs,
	int		*svr_types,
	int		num_types,
	int		*svr_statuses,
	int		num_statuses,
	int		total_cpus,
	int		online_cpus,
	unsigned long	cpupwr,
	unsigned long	total_mem)
{
	int		i;
	int		error;
	int		status;
	clms_cli_tr_data_t *datap;

	SSI_ASSERT(this_node != clms_master_node);

	LOCK_COND_LOCK(&clms.clms_node_lock);
	status = clms_get_node_status(new_node);
	SSI_ASSERT(status & (CLMS_NODE_NEVER_UP |
			 CLMS_NODE_DOWN |
			 CLMS_NODE_INFORM_COMING_UP |
			 CLMS_NODE_REGROUP_KNOWN));

	CLMSLOG(CLMSLOG_NODEINFO, new_node, status, ctv.transid, 0, 0, 0, 0);

	clms.clms_node_info[new_node].total_cpus = total_cpus;
	clms.clms_node_info[new_node].online_cpus = online_cpus;
	clms.clms_node_info[new_node].cpu_power = cpupwr;
	clms.clms_node_info[new_node].total_memory = total_mem;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
#ifdef CONFIG_MOSIX_LL
	initialize_loadinfo(new_node, online_cpus, cpupwr);
#endif
	if (!clms_iskeynode(this_node)) {
		/*
		 * Set the ics information for new node.
		 */
		error = ics_seticsinfo(new_node, icsinfo_p);
		if (error != 0) {
			printk(KERN_WARNING "rclms_newnode_info: ics_seticsinfo: node %u error %d", new_node, error);
		}
	}
	/*
	 * Store the key services provided by the new node in the local
 	 * database.
	 */
	SSI_ASSERT(num_svcs == num_types);
	SSI_ASSERT(num_svcs == num_statuses);
	for (i = 0; i < num_svcs; i++) {
		clms_vset_cluster_svr(svcs[i], new_node, svr_types[i],
			     		svr_statuses[i]);
	}
	datap = clms_alloc_client_tr_data(CLMS_STATE_COMINGUP,
					  new_node,
					  why,
					  ctv);
	if (!datap)
		panic("rclms_newnode_info: No Memory\n");
	/*
	 * Block users from seeing the transition until we're finished.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_LOCK, 0, __FILE__, __LINE__, current->pid, 0, 0, 0);
	/*
	 * Get the Cluster Transition Vector lock and update the vector.
	 * The shared lock holds off clients of the CTV until nodeup
	 * is complete.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
	/*
	 * If the nodeup is from a node that isn't now the CLMS master,
	 * drop it.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	if (clms_node != clms_master_node) {
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		kfree(datap);
		return(0);
	}
	/*
	 * If the CTV on the nodeup message doesn't match the value we
	 * expect, we've missed a transition; queue this one and wait
	 * for it to be processed.  We queue it so that failover can find
	 * out about it; we wait because we can't return until the
	 * transition is finished.  When the missing transition (which
	 * must be either a nodedown or an API-driven transition, since
	 * there is only one outstanding nodeup at a time) comes in, the
	 * transition we queue here will be processed as well, when that
	 * code runs clms_client_run_trans_queue().
	 */
	if (!CLMS_CTV_TRANS_IS_NEXT(&ctv, &(clms.clms_ctv))) {
		clms_put_trans_queue_l(&clms.clms_client_trans_queue, datap);
		while (clms_node == clms_master_node &&
		       ctv.ups < clms.clms_ctv.ups + 1) {
			WAIT_CONDITION(&clms.clms_ctv_cond,
				       &clms.clms_ctv_lock);
		}
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
		if (clms_node != clms_master_node) {
			UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
			CLMSLOG(CLMSLOG_API_UNLOCK, 0,
				__FILE__, __LINE__, current->pid, 0, 0, 0);
		}
		return(0);
	}
	clms.clms_ctv = ctv;
	/*
	 * Save the transition on our save queue for failover.
	 */
	clms_put_trans_queue_l(&clms.clms_client_save_queue, datap);
	/*
	 * Record the transition.
	 */
	clms_api_transition(new_node, CLMS_STATE_COMINGUP, why, ctv);
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * Wake up anyone waiting for a missing transition.
	 */
	BROADCAST_CONDITION(&clms.clms_ctv_cond);
	/*
	 * Process the nodeup.  This releases the shared CTV lock when it's
	 * finished.
	 */
	do {
		error = spawn_daemon_proc("cc nodeup p",
					  clms_client_nodeup_proc,
					  (void *)(long)new_node);
		if (error != 0)
			idelay(HZ/10);
	} while(error != 0);
	/*
	 * Run the transition queue and process any entries there.
	 */
	clms_client_run_trans_queue();
	return(0);
}

/*
 * rclms_inform_key_nodes()
 *      Receive information about a node coming up.
 *
 * Description:
 *	This is the service procedure for an ics message from the clms
 *	master informing about a new node that is coming up. This ics
 *	message will be received only by key server nodes (that is nodes
 *	that are providing key services (like root file system)). Ordinary
 *	nodes will not receive this message unless a key server node is
 *	coming up into an existing cluster.
 *
 * Calling/Exit State:
 *	Called from ics server proc to handle an ICS message.
 *
 * Parameters:
 *	clusternode_t	node	 - this node
 *	node_t      new_node	 - the node that is coming up
 *	icsinfo_t  *new_icsinfo	 - the ics information for the new node
 *	int         state	 - the state of the new node ("coming up")
 *
 * Returns
 *	0.
 */
int
rclms_inform_key_nodes(
	clusternode_t	node,
	clusternode_t	new_node,
	icsinfo_t	*new_icsinfo)
{
	int error, do_icsnodeup;
	icsinfo_t dummy;

	CLMSLOG(CLMSLOG_INFORM,
		new_node,
		CLMS_NODE_INFORM_COMING_UP,
		0, 0, 0, 0, 0);
	/*
	 * Set the ics information for new node.
	 */
	if (ics_geticsinfo(new_node, &dummy) == -EINVAL) {
		error = ics_seticsinfo(new_node, new_icsinfo);
		if (error != 0) {
			printk(KERN_WARNING "rclms_inform_key_nodes: ics_seticsinfo:  error %d", error);
		}
	}

        /*
	 * Set the status of the new node.  Avoid a possible race with
	 * other (redundant) calls to rclms_inform_key_nodes() for this
	 * node.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (clms_get_node_status(new_node) != CLMS_NODE_INFORM_COMING_UP)
		clms_set_node_status_l(new_node, CLMS_NODE_INFORM_COMING_UP);
	/*
	 * Inform ICS that the node is coming up; it will establish all
	 * connections to the node.  This may race with the nodeup daemon,
	 * thus the interlock.
	 */
	do_icsnodeup = 0;
	if (!(clms.clms_node_info[new_node].flags & CLMS_NODE_ICS_NODEUP_DONE)) {
		do_icsnodeup = 1;
		clms.clms_node_info[new_node].flags |= CLMS_NODE_ICS_NODEUP_DONE;
	}
	/*
	 * Don't race with RCLMS_READY_KEY_NODES.
	 */
	clms.clms_node_info[new_node].flags |= CLMS_NODE_BLOCK_READY;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	if (do_icsnodeup) {
		int retries = 0;
		int state;

		do {
			error = ics_nodeup(new_node);
			if (error == -EAGAIN && ++retries < 5) {
				CLMSLOG(CLMSLOG_INFORMRET,
					error,
					new_node,
					retries, 0, 0, 0, 0);
				idelay(HZ);
			}
			state = clms_get_node_status(new_node);
		} while(error == -EAGAIN &&
			retries < 10 &&
			state == CLMS_NODE_INFORM_COMING_UP);
	}
	LOCK_COND_LOCK(&clms.clms_node_lock);
	/*
	 * Let RCLMS_READY_KEY_NODES happen.
	 */
	clms.clms_node_info[new_node].flags &= ~CLMS_NODE_BLOCK_READY;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	BROADCAST_CONDITION(&clms.clms_node_cond);
	CLMSLOG(CLMSLOG_INFORM,
		new_node,
		CLMS_NODE_INFORM_COMING_UP,
		1,
		do_icsnodeup,
		0, 0, 0);
	return(0);
}

/*
 * rclms_ready_key_nodes()
 *      Find out when a coming-up node is ready for communication.
 *
 * Description:
 *	This is the service procedure for an ics message from the clms
 *	master telling us that a new node (that we first found out about
 *	via RCLMS_INFORM_KEY_NODES()) is ready for communication.  This
 *	message will be received only by key nodes (that is nodes that
 *	are providing key services (like root file system)). Ordinary
 *	nodes will not receive this message.
 *
 * Calling/Exit State:
 *	Called from ics server proc to handle an ICS message.
 *
 * Parameters:
 *	clusternode_t node	 - this node
 *	clusternode_t new_node	 - the node that is ready
 *
 * Returns
 *	0.
 */
int
rclms_ready_key_nodes(
	clusternode_t	node,
	clusternode_t	new_node)
{
	int do_ics;

	CLMSLOG(CLMSLOG_READY, new_node, 0, 0, 0, 0, 0, 0);
        /*
	 * If we're racing with RCLMS_INFORM_KEY_NODES, wait until the
	 * node is marked appropriately.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	while (clms_get_node_status(new_node) != CLMS_NODE_INFORM_COMING_UP)
		WAIT_CONDITION(&clms.clms_node_cond, &clms.clms_node_lock);
	do_ics = 0;
	if (!(clms.clms_node_info[new_node].flags & CLMS_NODE_ICS_READY_DONE)) {
		do_ics = 1;
		clms.clms_node_info[new_node].flags |= CLMS_NODE_ICS_READY_DONE;
	}
	/*
	 * Serialize this with the ics_nodeup() in RCLMS_INFORM_KEY_NODES.
	 */
	while (clms.clms_node_info[new_node].flags & CLMS_NODE_BLOCK_READY) {
		WAIT_CONDITION(&clms.clms_node_cond, &clms.clms_node_lock);
	}
	/*
	 * If the node went down while we were asleep, don't do the ics
	 * operation.
	 */
	if (clms_get_node_status(new_node) & (CLMS_NODE_GOING_DOWN |
					      CLMS_NODE_DOWN |
					      CLMS_NODE_NEVER_UP))
		do_ics = 0;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	if (do_ics)
		ics_nodeready(new_node);
	return(0);
}


/*
 * clms_client_nodeup()
 *	Handle nodeup notifications from ICS on CLMS client nodes.
 *
 * Description:
 *	This routine is called as a result of ICS calling the routine
 *	registered by CLMS through ics_nodeinfo_callback() on the CLMS
 *	client node.
 *
 *	For CLMS clients, the "real" notification actually comes from the
 *	CLMS master node in an RPC, and not from the ICS subsystem. So there
 *	is nothing to do and any notifications are just ignored. However,
 *	the routine MUST be called, since ICS expects to be able to
 *	perform callbacks when it detects these situations.
 *
 * Calling/Exit State:
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	clms_node	client node that came up and was noticed
 *			by the ICS subsystem.
 */
void
clms_client_nodeup(
	clusternode_t 	clms_node)
{
	SSI_ASSERT(this_node != clms_master_node);

	/* Do Nothing ... */
}


/*
 * CLMS Client Procedures to deal with "Node Down" events.
 */
/*
 * clms_client_nodedown()
 *	Handle nodedown notifications from ICS.
 *
 * Description:
 *	This routine is called when ICS or the user(through ssisys),
 *	detects that a node has gone down and informs CLMS.
 *
 *	This routine is called on the clms client node.
 *
 *	An rpc is sent to the CLMS Master node to inform that a node
 *	has gone down. The CLMS Master makes the final decision.
 *
 * Calling/Exit State:
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node	node that ICS thinks is down
 *
 */
void
clms_client_nodedown(
	clusternode_t	node)
{
	int 	error;
	int 	rval;

	SSI_ASSERT(this_node != clms_master_node);
	SSI_ASSERT(node != clms_master_node);	/* Handled elsewhere. */
	/*
	 * Let the CLMS Coordinator know that a node has gone down.
	 */
	error = RCLMS_INFORM_NODEDOWN(clms_master_node,
					   &rval,
					   node);
	if (error != 0) {
		printk(KERN_WARNING "clms_client_nodedown:"
		       "RCLMS_INFORM_NODEDOWN: rpc error = %d"
		       , error);
	}
	SSI_ASSERT(rval == 0);
	return;
}

/*
 * rclms_nodedown()
 *	Receive information about a node that has gone down from CLMS Master.
 *
 * Description:
 *	This is the RPC service procedure for the ICS MSG that the CLMS master
 *	node sends out to each CLMS client node when a node goes down.
 *
 *	The node number of the node that has gone down and the surrogate node
 *	are given as parameters.
 *
 *	The node is marked as "going down".
 *
 *	The node information is queued for the clms client nodedown daemon
 *	to handle.
 *
 * Calling/Exit State:
 *	This is called from ics server proc to service a message.
 *
 * Parameters:
 *	node		this node
 *	clms_handle	This is a clms handle which is used when calling back
 *			the clms master after cleaning up
 *	clms_node	the clms master node that sent us the message.
 *	down_node	node that went down
 *	surrogate_node  node that is acting as the surrogate for the node that
 *			went down (if surrogate service is not present, this
 *			node is simply the CLMS master node).
 *	down_trans	the CLMS master's nodedown transition count
 *	wasntup		indicates that the node failed while it was still in
 *			CLMS_NODE_COMING_UP state, so don't tell the API
 *	why		the reason for the transition
 *	ctv		the CTV of this transition
 *	servers		the servers to which key services were reassigned
 *			because the down node was a key server
 *	num_servers	the number of new key servers (should be the same as
 *			clms_num_key_svcs)
 *
 * Return value:
 *	Must be zero.
 */
int
rclms_nodedown(
	clusternode_t	node,
	u_long		clms_handle,
	clusternode_t	clms_node,
	clusternode_t	down_node,
	clusternode_t	surrogate_node,
	int		down_trans,
	int		wasntup,
	int		why,
	clms_ctv_t	ctv,
	clusternode_t	*servers,
	int		num_servers)
{
	int		error;
	int		reassign_svc;
	clms_cli_tr_data_t *datap, *datap1;

	CLMSLOG(CLMSLOG_CLINODEDOWN,
		down_node,
		surrogate_node,
		clms_handle,
		down_trans,
		ctv.transid,
		0, 0);
	SSI_ASSERT(num_servers == clms_num_key_svcs);
	INCR_MEMPRIO();		/* avoid blocking for memory */
	/*
	 * Do the following early to free up any resources and blocked
	 * RPCs for the node; the actual nodedown may be processed a
	 * bit later.
	 */
	clms_set_node_status(down_node, CLMS_NODE_GOING_DOWN);

	printk(KERN_WARNING "OpenSSI: Node %u has gone down!!!\n", down_node);
	/*
	 * Set the shared nodedown lock so failover can tell when nodedown
	 * finishes.
	 */
	CLMS_NODEDOWN_LOCK();
	/*
	 * Call ICS "Node Down Handler" before we do anything else, to
	 * free any resource that we may be holding for the dead node.
	 * Do RDEV nodedown early in case swap devices depend upon it.
	 */
	/* NSC_XXX: Not yet supported.
	 rdev_nodedown(down_node);
	 */
	/*
	 * We process ICS nodedown in two steps.  The start routine
	 * puts the high and low-level ICS states to "going down".
	 * We set the states to fully "down" only after grabbing the CTV
	 * lock.  This prevents a race condition from occurning when a
	 * node goes down while we are trying to bring it up.
	 */
	ics_nodedown_async_start(down_node);
	/*
	 * Do key service nodedown and reassignment (if necessary).
	 */
	reassign_svc = clms_test_and_save_cluster_downtrans(down_trans);
	clms_key_service_nodedown(down_node, servers, reassign_svc);

	datap = clms_alloc_client_tr_data(CLMS_STATE_KCLEANUP,
					  down_node,
					  why,
					  ctv);
	SSI_ASSERT(datap != NULL);
	datap->data.nodedown.clms_handle = clms_handle;
	datap->data.nodedown.surrogate_node = surrogate_node;
	datap->data.nodedown.wasntup = wasntup;
	datap->data.nodedown.master_node = clms_node;
	datap->data.nodedown.down_trans = down_trans;
	memcpy(datap->data.nodedown.servers,
	       servers,
	       sizeof(clusternode_t) * clms_num_key_svcs);
	/*
	 * Get the Cluster Transition Vector lock and update the vector.
	 * The shared lock holds off clients of the CTV until nodedown
	 * is complete.  This lock will be released by this context if
	 * we don't spawn a nodedown process, else it will be released by
	 * the process.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
	/*
	 * Block users from seeing the transition until we're finished.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_LOCK, 0, __FILE__, __LINE__, current->pid, 0, 0, 0);
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * Set the high and low-level ICS states of the down node to
	 * fully "down".
	 */
	ics_nodedown_async_end(down_node);
	/*
	 * If the nodedown is from a node that isn't now the CLMS master,
	 * drop it.
	 */
	if (clms_node != clms_master_node) {
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		CLMS_NODEDOWN_UNLOCK();
		kfree(datap);
		DECR_MEMPRIO();
		return(0);
	}
	/*
	 * If the CTV on the nodedown message doesn't match the value we
	 * expect, we've missed a transition; just queue this one to be
	 * processed when the one we've missed comes in.  Note that we
	 * don't release the nodedown lock or the API RW lock, because the
	 * transition will be processed when any missing transitions come
	 * in.
	 */
	if (!CLMS_CTV_TRANS_IS_NEXT(&ctv, &(clms.clms_ctv))) {
		clms_put_trans_queue_l(&clms.clms_client_trans_queue, datap);
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
		DECR_MEMPRIO();
		return(0);
	}
	clms.clms_ctv = ctv;
	/*
	 * Save the transition on our save queue for failover.
	 */
	clms_put_trans_queue_l(&clms.clms_client_save_queue, datap);
	/*
	 * Remember the transition in the history, unless the node wasn't all
	 * the way up.
	 */
	if (!wasntup)
		clms_api_transition(down_node,
				    CLMS_STATE_KCLEANUP,
				    why,
				    ctv);
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	BROADCAST_CONDITION(&clms.clms_node_cond);
	BROADCAST_CONDITION(&clms.clms_ctv_cond);
	/*
	 * Copy the data structure for the nodedown proc; the original will
	 * be tossed by the unblock, and we need one to keep around.
	 */
	datap1 = clms_alloc_client_tr_data(CLMS_STATE_KCLEANUP,
					   down_node,
					   why,
					   ctv);
	SSI_ASSERT(datap1 != NULL);
	datap1->data = datap->data;
	/*
	 * Tell the CLMS master (the one that sent us the transition) that
	 * we've started.
	 */
	error = RCLMS_NODEDOWN_STARTED_MSG(datap->data.nodedown.master_node,
					   datap->data.nodedown.clms_handle,
					   this_node);
	if (error != 0 && error != -EREMOTE) {
		printk(KERN_WARNING "rclms_nodedown:  RCLMS_NODEDOWN_STARTED_MSG failed error = %d\n", error);
	}
	do {
		error = spawn_daemon_proc("cc nodedown",
				   	  clms_client_nodedown_proc,
				    	  (void *)datap1);
		if (error != 0)
			idelay(HZ/10);
	} while(error != 0);
	/*
	 * Run the transition queue and process any entries there.
	 */
	clms_client_run_trans_queue();
	DECR_MEMPRIO();
	return(0);
}

/*
 * clms_client_run_trans_queue
 *	Process entries in the client transition queue.
 *
 * Description:
 *	If, when we processed a nodeup or nodedown, there were transitions
 *	that hadn't yet arrived, the nodeup or nodedown is queued.  This
 *	routine processes the entries in the queue, until it is empty or
 *	we hit another missing transition.
 *
 * Parameters:
 *	None.
 *
 * Return value:
 *	None.
 */
void
clms_client_run_trans_queue(void)
{
	int error;
	clms_cli_tr_data_t *datap, *datap1;

	/*
	 * Hold the CTV stable over the next call.  Note that the shared
	 * lock will be released by any spawned process.  We'll have a
	 * redundant one, though, which we'll release when we're finished.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	while ((datap = clms_get_trans_queue_l(&(clms.clms_client_trans_queue))) != NULL) {
		/*
		 * Update the CTV; it's guaranteed by clms_get_trans_queue_l()
		 * to be the next one in sequence.
		 */
		clms.clms_ctv = datap->ctv;
		/*
		 * Record the transition, unless this is a nodedown and
		 * the node wasn't all the way up.
		 */
		if (datap->trans != CLMS_STATE_KCLEANUP ||
		    datap->data.nodedown.wasntup == 0)
			clms_api_transition(datap->node,
					    datap->trans,
					    datap->why,
					    datap->ctv);
		/*
		 * If it's a dummy transition just go to the next transition.
		 * Free the structure, since it's not going on the save queue.
		 */
		if (datap->trans == CLMS_STATE_DUMMY) {
			kfree(datap);
			continue;
		}
		/*
		 * Save the transition on our save queue for failover.
		 */
		clms_put_trans_queue_l(&clms.clms_client_save_queue, datap);
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		/*
		 * Wake up anyone waiting for a missing transition.
		 */
		BROADCAST_CONDITION(&clms.clms_ctv_cond);
		/*
		 * Signal process that a transition happened, unless this is
		 * a nodedown and the node wasn't all the way up.
		 */
		if (datap->trans != CLMS_STATE_KCLEANUP ||
		    datap->data.nodedown.wasntup == 0)
			clms_sigcluster_all();
		/*
		 * Process a nodeup.
		 */
		if (datap->trans == CLMS_STATE_COMINGUP) {
			do {
				error = spawn_daemon_proc("cc nodeup p",
							  clms_client_nodeup_proc,
							  (void *)(long)datap->node);
				if (error != 0)
					idelay(HZ/10);
			} while(error != 0);
		}
		/*
		 * Process a nodedown.
		 */
		if (datap->trans == CLMS_STATE_KCLEANUP) {
			int state;

			/*
			 * If this is a transition rolled forward by failover,
			 * we haven't done the early processing for it.  Check,
			 * and do this if necessary.  If the state of the node
			 * is already CLMS_NODE_GOING_DOWN, we must have gone
			 * through rclms_nodedown(), else this is a
			 * roll-forward.
			 */
			LOCK_COND_LOCK(&clms.clms_node_lock);
			state = clms_get_node_status(datap->node);
			if (state != CLMS_NODE_GOING_DOWN) {
				clms_set_node_status_l(datap->node,
						       CLMS_NODE_GOING_DOWN);
				CLMS_NODEDOWN_LOCK();
				ics_nodedown(datap->node);
				state = clms_test_and_save_cluster_downtrans(
					       datap->data.nodedown.down_trans);
				clms_key_service_nodedown(
						   datap->node,
						   datap->data.nodedown.servers,
						   state);
			}
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			/*
			 * Copy the data structure for the nodedown proc; the
			 * original will be tossed by the unblock, and we
			 * need one to keep around.
			 */
			datap1 = clms_alloc_client_tr_data(
							CLMS_STATE_KCLEANUP,
							datap->node,
							datap->why,
							datap->ctv);
			SSI_ASSERT(datap1 != NULL);
			datap1->data = datap->data;
			/*
			 * Tell the CLMS master that we've started.
			 */
			error = RCLMS_NODEDOWN_STARTED_MSG(
					      datap->data.nodedown.master_node,
					      datap->data.nodedown.clms_handle,
					      this_node);
			if (error != 0 && error != -EREMOTE) {
				printk(KERN_WARNING
				       "clms_client_run_trans_queue: "
				       "RCLMS_NODEDOWN_STARTED_MSG failed error = %d\n",
				       error);
			}
			do {
				error = spawn_daemon_proc("cc nodedown p",
						   	  clms_client_nodedown_proc,
						    	  (void *)datap1);
				if (error != 0)
					idelay(HZ/10);
			} while(error != 0);
		}
		/*
		 * Process an intent.  Intents here are only processed as a
		 * result of CLMS failover.  If this node missed an intent
		 * (i.e. never received an intent from the old CLMS master
		 * that other nodes _did_ receive), CLMS failover inserts
		 * a DUMMY_INTENT transition in the transition queue; it's
		 * put there so it will be executed in the proper order, and
		 * so earlier missed transitions (including dummies) will be
		 * processed first.  Failover leaves the processing itself
		 * to this routine, which it calls after filling the trans
		 * queue.  Note that there can only ever be one of these.
		 */
		if (datap->trans == CLMS_STATE_DUMMY_INTENT) {
			do {
				error = spawn_daemon_proc("cc intent p",
						   	  clms_client_intent_proc,
						    	  (void *)datap);
				if (error != 0)
					idelay(HZ/10);
			} while(error != 0);
		}
		LOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
		LOCK_COND_LOCK(&clms.clms_ctv_lock);
	}
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
}

/*
 * clms_client_nodedown_proc
 *	This process is run to process a nodedown on the client.
 *
 * Description
 *	This process does the service-oriented nodedown processing on the
 *	current node.
 *
 * Calling/Exit State:
 *	Process spawned to process a nodedown.
 *
 * Parameters
 *	argp	 pointer to clms_cli_tr_data_t.
 */
static void
clms_client_nodedown_proc(void *argp)
{
	int	error;
	u_long	clms_handle;
	clusternode_t	down_node;
	clusternode_t	surrogate_node;
	clusternode_t	master_node;
	int	wasntup;
	struct clms_transition_data *cdp;

	nsc_daemonize();

	down_node = ((clms_cli_tr_data_t *)argp)->node;
	clms_handle = ((clms_cli_tr_data_t *)argp)->data.nodedown.clms_handle;
	surrogate_node =
		((clms_cli_tr_data_t *)argp)->data.nodedown.surrogate_node;
	master_node = ((clms_cli_tr_data_t *)argp)->data.nodedown.master_node;
	wasntup = ((clms_cli_tr_data_t *)argp)->data.nodedown.wasntup;
	kfree(argp);

	CLMSLOG(CLMSLOG_CLINDSTART,
		down_node,
		clms_handle,
		surrogate_node,
		0, 0, 0, 0);

	cdp = clms_alloc_trans_data();
	cdp->node = down_node;
	cdp->surrogate_node = surrogate_node;

	/*
 	 * Call the Node Down Handlers for all other services,
 	 * This function returns only after all services have
	 * completed cleaning up.
 	 */
	clms_call_all_svcs(cdp, CLMS_CALL_NODEDOWN);

	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_node_info[down_node].flags = 0;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	/*
	 * Wait for all servers for all services to complete.  This should
	 * avoid the possibility of having outstanding messages when a new
	 * incarnation of the node appears.
	 */
	icssvr_nodedown_wait(down_node);
	/*
	 * Release the shared nodedown lock so failover will know when we're
	 * done.
	 */
	CLMS_NODEDOWN_UNLOCK();
	/*
	 * Tell the CLMS master that we're done.
	 */
	error = RCLMS_CLIENT_NODEDOWN_CALLBACK_MSG(master_node,
						   clms_handle,
						   this_node);
	if (error != 0 && error != -EREMOTE) {
		printk(KERN_WARNING
		       "clms_client_nodedown_proc: RCLMS_CLIENT_NODEDOWN_CALLBACK failed error = %d\n",
		       error);
	}
	/*
	 * If the node wasn't all the way up when it went down, set it to
	 * fully down now.  Otherwise this happens when it goes to
	 * CLMS_STATE_DOWN later.
	 */
	if (wasntup != 0) {
		if (wasntup == 1)
			clms_set_node_status(down_node, CLMS_NODE_NEVER_UP);
		else
			clms_set_node_status(down_node, CLMS_NODE_DOWN);
	}
	/*
	 * Clients of the CTV can run now.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);

	CLMSLOG(CLMSLOG_CLINDFIN, down_node, 0, 0, 0, 0, 0, 0);

	(void)clms_free_trans_data(cdp);

	exit_daemon_proc();
}

/*
 * clms_client_intent_proc
 *	This process is run as a result of CLMS failover to process a state
 *	transition intent.
 *
 * Description
 *	During failover, if there's an outstanding intent that this node
 *	doesn't have, the roll-forward code will queue a dummy intent on
 *	the trans queue, so that the missing intent will be processed.
 *	The roll-forward code calls the clms_client_run_trans_queue
 *	routine to perform all outstanding transitions, including the
 *	missing intent.  This process is used to process the intent without
 *	blocking later nodedowns.
 *
 * Calling/Exit State:
 *	Process spawned when a queued intent has to be processed.
 *
 * Parameters
 *	argp	 pointer to clms_cli_tr_data_t.
 */
static void
clms_client_intent_proc(void *argp)
{
	int	new_state;
	int	status;
	clusternode_t	node;

	nsc_daemonize();

	node = ((clms_cli_tr_data_t *)argp)->node;
	new_state = ((clms_cli_tr_data_t *)argp)->trans;
	if (new_state == CLMS_STATE_UP) {
		/*
		 * Get the status of the transitioning node.  If it's
		 * CLMS_NODE_NEVER_UP or CLMS_NODE_COMING_UP, we're racing
		 * with clms_client_perform_nodeup().  Just sleep until the
		 * node is marked as half up.  This is safe since the nodeup
		 * was earlier in sequence than the transition we're
		 * processing, so it will be being processed concurrently.
		 * During CLMS failover, if the node has gone down, we'll
		 * still get the nodedown since the roll-forward routine
		 * doesn't block waiting for this function to complete;
		 * we're running as an independent process.
		 */
		status = clms_get_node_status(node);
		if (status == CLMS_NODE_NEVER_UP ||
		    status == CLMS_NODE_COMING_UP)
			status = clms_waitfor_nodeup(node,
						     B_TRUE);
		/*
		 * Only set the node as up if it's now half up; it might
		 * have gone down or still be fully up internally.
		 */
		if (status == CLMS_NODE_HALF_UP ||
		    status == CLMS_NODE_HALF_DOWN)
			clms_nodeup(node);
	}
	if (new_state == CLMS_STATE_GOINGDOWN ||
	    new_state == CLMS_STATE_SHUTDOWN) {
		clms_node_halfdown(node);
#ifdef CONFIG_LDLVL
		if (new_state == CLMS_STATE_SHUTDOWN &&
						node == this_node)
			move_eligible_processes_off();
#endif
	}
	/*
	 * The caller gets this lock for each instance of the process.
	 * Unlock it now.
	 */
	/* SSI_XXX: we obtained lock for CLMS_STATE_DUMMY_INTENT ? */
	/* Yes. Obtained in clms_client_run_trans_queue(). -RT */
	UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
	/*
	 * Unblock the API; allow users to see the transition.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
		0, 0, 0);
	exit_daemon_proc();
}

/*
 * rclms_new_state_intent
 *	The CLMS master is informing us that a node is changing state.
 *
 * Description
 *	A node is making an API-driven transition.  (Some states are
 *	set via the API from a process, or are of interest only to the
 *	API.)  When this happens, the CLMS master sends each node an
 *	"intent" on behalf of the client node.  This routine processes
 *	that "intent," by blocking users from seeing the transition,
 *	then recording the intent in the clms data.
 *
 * Calling/Exit State:
 *	Returns with the API rw lock held shared.
 *
 * Parameters
 *	node		This node.
 *	rval		Return value of the RPC.
 *	clms_node	Node sending us the RPC (clms_master_node).
 *	client_node	The node that is changing state.
 *	ctv		The Cluster Transition Vector for this transition.
 */
int
rclms_new_state_intent(
	clusternode_t	node,
	int		*rval,
	clusternode_t	clms_node,
	clusternode_t	client_node,
	int		new_state,
	int		why,
	clms_ctv_t	ctv)
{
	CLMSLOG(CLMSLOG_INTENT, clms_node, client_node, new_state, why,
				ctv.ups, ctv.downs, ctv.other);
	/*
	 * Block users from seeing the transition until we're finished.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_LOCK, 0, __FILE__, __LINE__, current->pid, 0, 0, 0);
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * Intents cannot be processed out of sequence, but commit messages
	 * can. So, do not process the next intent until the commit
	 * has been processed.
	 */
	while (clms.clms_api_trans_intent.present) {
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		WAIT_CONDITION_NORELCK(&clms.clms_ctv_cond,
				       &clms.clms_ctv_lock);
		LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_LOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		LOCK_COND_LOCK(&clms.clms_ctv_lock);
	}
	/*
	 * If the transition is from a node that isn't now the CLMS master,
	 * we've done failover; drop it.
	 */
	if (clms_node != clms_master_node) {
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		return(0);
	}
	/*
	 * Record the intent.  We don't actually process the information
	 * until we receive the commit.  Only one intent can be active at
	 * a time; this is prevented at the CLMS master.
	 */
	clms.clms_api_trans_intent.client_node = client_node;
	clms.clms_api_trans_intent.new_state = new_state;
	clms.clms_api_trans_intent.why = why;
	clms.clms_api_trans_intent.ctv = ctv;
	clms.clms_api_trans_intent.present = 1;
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	*rval = 0;
	return(0);
}

/*
 * rclms_new_state_commit
 *	The CLMS master is informing us we can process a state change.
 *
 * Description
 *	After all nodes get the "intent" (see the description of
 *	rclms_new_state_intent(), above), the CLMS master sends each
 *	node a "commit" message.  This routine processes that message,
 *	by actually performing the transition and then allowing users
 *	actually see it.
 *
 *	Two states require special handling here.  For CLMS_STATE_UP,
 *	when a node initializes, it starts in "half up" state, where
 *	all rexecs/rforks/migrates are failed back to the requester
 *	with -EAGAIN, unless the requester ignores "half up" state, as
 *	in the case of noded/init.  When the node goes from "half up"
 *	to fully up state (indicated by an API-driven transition to
 *	CLMS_STATE_UP state, done when user-level initialization is
 *	complete), it does the usual API state transition with the
 *	side effect that all nodes set the CLMS state for the node in
 *	question to CLMS_NODE_UP.
 *
 *	For CLMS_STATE_GOINGDOWN, when a node is shut down, it first
 *	goes to "half down" state (CLMS_NODE_HALF_DOWN, indicated by an
 *	API-driven transition to CLMS_STATE_GOINGDOWN) in which the
 *	node is still part of the cluster, but in which all rexecs/rforks/
 *	migrates are failed back to the requester, unless the requester
 *	ignores "half down" state, as in the case of init.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters
 *	clusternode_t	node		Us.
 *	clusternode_t	clms_node	Node sending us the RPC (clms_master_node).
 */
int
rclms_new_state_commit(
	clusternode_t	node,
	clusternode_t	clms_node,
	clms_ctv_t	passed_ctv)
{
	clusternode_t	client_node;
	int	new_state;
	int	why;
	int	status;
	clms_ctv_t ctv;

	CLMSLOG(CLMSLOG_COMMIT, clms_node,
				passed_ctv.ups,
				passed_ctv.downs,
				passed_ctv.other,
				passed_ctv.transid, 0, 0);
	SSI_ASSERT_LOCKED_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	/*
	 * Note that we don't get the Cluster Transition Vector lock before
	 * we update the vector.  Other parts of the kernel aren't interested
	 * in "other" transitions.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * Get the new CTV from the intent.
	 */
	ctv = clms.clms_api_trans_intent.ctv;
	SSI_ASSERT(!memcmp(&ctv, &passed_ctv, sizeof(ctv)));
	/*
	 * If we've missed a transition, wait for it to come in before
	 * proceeding.  Hold the rw locks; this just holds off clients
	 * for a bit until the missing transition comes in.  We drop out
	 * if we've done failover, since clms_master_node will have changed,
	 * and failover processing will wake us up.
	 */
	while (!CLMS_CTV_TRANS_IS_NEXT(&ctv, &(clms.clms_ctv)) &&
	       clms_node == clms_master_node) {
		WAIT_CONDITION(&clms.clms_ctv_cond, &clms.clms_ctv_lock);
	}
	/*
	 * If the transition is from a node that isn't now the CLMS master,
	 * we've done failover; drop it.
	 */
	if (clms_node != clms_master_node) {
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		/* SSI_XXX: we obtained this lock for "intent" ? */
		/* UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock); */
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		return(0);
	}
	clms.clms_ctv = ctv;
	client_node = clms.clms_api_trans_intent.client_node;
	new_state = clms.clms_api_trans_intent.new_state;
	why = clms.clms_api_trans_intent.why;
	clms_api_transition(client_node, new_state, why, ctv);
	clms.clms_api_trans_intent.present = 0;
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	BROADCAST_CONDITION(&clms.clms_ctv_cond);
	if (new_state == CLMS_STATE_UP) {
		/*
		 * Get the status of the transitioning node.  If it's
		 * CLMS_NODE_NEVER_UP or CLMS_NODE_COMING_UP, we're racing
		 * with clms_client_perform_nodeup().  Just sleep until the
		 * node is marked as half up.  This is safe since the nodeup
		 * was earlier in sequence than the transition we're
		 * processing, so it will be being processed concurrently.
		 * If the node goes down, we'll fall out of the sleep.
		 */
		status = clms_get_node_status(client_node);
		if (status == CLMS_NODE_NEVER_UP ||
		    status == CLMS_NODE_COMING_UP)
			status = clms_waitfor_nodeup(client_node, B_TRUE);
		/*
		 * Only set the node as up if it's now half up; it might have
		 * gone down or still be fully up internally.
		 */
		if (status == CLMS_NODE_HALF_UP ||
		    status == CLMS_NODE_HALF_DOWN)
			clms_nodeup(client_node);
	}
	if (new_state == CLMS_STATE_GOINGDOWN ||
	    new_state == CLMS_STATE_SHUTDOWN) {
		clms_node_halfdown(client_node);
#ifdef CONFIG_LDLVL
		if (new_state == CLMS_STATE_SHUTDOWN &&
						client_node == this_node)
			move_eligible_processes_off();
#endif
	}
	if (new_state == CLMS_STATE_DOWN)
		clms_set_node_status(client_node, CLMS_NODE_DOWN);
	/*
	 * Allow users to see the transition we just finished.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
		0, 0, 0);
	/*
	 * Send a SIGCLUSTER signal to all processes that need to find out
	 * about cluster topology changes.
	 */
	clms_sigcluster_all();
	/*
	 * Run the transition queue and process any entries there, just in
	 * case the transition we just processed was out of order.
	 */
	clms_client_run_trans_queue();
	return(0);
}

/*
 * rclms_transition_unblock
 *	The CLMS master is informing us we can unblock consumers of the
 *	API.
 *
 * Description:
 *	When the CLMS master has informed all nodes of a transition and
 *	has received replies (either implicitly in the case of a nodeup
 *	or explicitly in the case of a nodedown) from all nodes, he sends
 *	each the RCLMS_TRANSITION_UNBLOCK message.  This routine unblocks
 *	consumers of the API.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters
 *	clusternode_t	node		Us.
 *	clusternode_t	clms_node	The CLMS master.
 *	clms_ctv_t ctv			The CTV of the transition to unblock.
 */
int
rclms_transition_unblock(
	clusternode_t	node,
	clusternode_t	clms_node,
	clms_ctv_t ctv)
{
	clms_cli_tr_data_t *datap;

	CLMSLOG(CLMSLOG_UNBLOCK, clms_node, ctv.transid, 0, 0, 0, 0, 0);
	SSI_ASSERT_LOCKED_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * If the message is from a node that isn't now the CLMS master,
	 * we've done failover; drop it.
	 */
	if (clms_node != clms_master_node) {
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		return(0);
	}
	datap = clms_del_trans_queue_l(&clms.clms_client_save_queue, ctv);
	SSI_ASSERT(datap != NULL);
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	kfree(datap);
	/*
	 * Allow users to see the transition we just finished.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
		0, 0, 0);
	/*
	 * Send a SIGCLUSTER signal to all processes that need to find out
	 * about cluster topology changes.
	 */
	clms_sigcluster_all();
	return(0);
}

/*
 * rclms_you_are_debug()
 *	CLMS master forces this node into kdb.
 * Description:
 *	CLMS Master nodedown -d <this_node> issued
 *
 * Calling/Exit State:
 *	This is called from ics server proc to service an ICS message.
 *
 * Parameters
 *	clusternode_t	node	- this node
 *
 * Returns
 *	0.
 */
int
rclms_you_are_debug(clusternode_t node)
{
#if defined(DEBUG) || defined(DEBUG_TOOLS)
	SSI_ASSERT(node == this_node);
#endif /* DEBUG || DEBUG_TOOLS */
	return 0;
}
