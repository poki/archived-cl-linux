/*
 *	CLMS debugging code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains miscellaneous (mostly debugging) routines for CLMS.
 */

#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/nodelist.h>
#include <cluster/async.h>
#include <cluster/icsgen.h>
#include <cluster/log.h>

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>


#if defined(DEBUG) || defined(DEBUG_TOOLS)

int		clms_do_logging = 0x1;
static const char *clmslog_msgs[CLMSLOG_NUMMSGS] = {
	"Node %ld wants to join, and is%c%c%c%ca key node.\n",		/* 0 */
	"Informing node %ld flags %lx of node %ld.\n",			/* 1 */
	"Informing joining node %ld of node %ld, state %lx flags %lx.\n",
									/* 2 */
	"Master nodeup of %ld state %lx flags %lx\n",			/* 3 */
	"Error %ld rval %ld from RCLMS_RGP_SETRGPINFO to %ld\n",	/* 4 */
	"New node %ld declared down!\n",				/* 5 */
	"Nodejoin of %ld state %lx\n",					/* 6 */
	"Sending nodeup info to %ld\n",					/* 7 */
	"Sending newnode info for %ld to %ld\n",			/* 8 */
	"Nodedown sent to %ld down node %ld surrogate %ld trans %ld\n",	/* 9 */
	"Joining cluster\n",						/* 10 */
	"Got rclms_join\n",						/* 11 */
	"Starting node\n",						/* 12 */
	"Got nodeup info for %ld nodes: %ld %ld %ld %ld %ld %ld\n",	/* 13 */
	"New node info for %ld, status %lx, transid %lx\n",		/* 14 */
	"Nodeup for %ld status %lx new status %lx\n",			/* 15 */
	"Nodeup finished for %ld status %lx new status %lx\n",		/* 16 */
	"Node %ld went down while coming up.\n",			/* 17 */
	"Informed of node %ld state %lx done %ld icsnu %ld\n",		/* 18 */
	"Adding node %ld to regroup (done %ld)\n",			/* 19 */
	"Monitoring node %ld in regroup\n",				/* 20 */
	"Adding rgpinfo for %ld nodes: %ld %ld %ld %ld %ld %ld.\n",	/* 21 */
	"Doing probeonly to node %ld\n",				/* 22 */
	"Retrying probeonly in one second...\n",			/* 23 */
	"Client nodedown for %ld, surr %ld, handle %lx, trans %ld,"
		" transid %lx\n",					/* 24 */
	"Client nodedown started for %ld handle %lx surr %ld trans %ld\n",
									/* 25 */
	"Client nodedown finished for %ld\n",				/* 26 */
	"Master nodedown indication for %ld, status %lx\n",		/* 27 */
	"Master nodedown started for %ld\n",				/* 28 */
	"Master nodedown finished for %ld\n",				/* 29 */
	"Halfup->up failed, flags %lx up %ld down %ld, cur st %lx prev %lx\n",
									/* 30 */
	"Tossing nodeup for %ld during shutdown.\n",			/* 31 */
	"Informed of shutdown from node %ld.\n",			/* 32 */
	"Need secondary for svc %ld (node down %ld new %ld).\n",	/* 33 */
	"Clearing key service %ld for node %ld.\n",			/* 34 */
	"Saving entry for key service %ld node %ld type %ld status %lx.\n",
									/* 35 */
	"Set key secondary ready, key service %ld type %ld.\n",		/* 36 */
	"Readying node %ld for node %ld flags %lx\n",			/* 37 */
	"Ready for node %ld\n",						/* 38 */
	"Send nodedown finish for node %ld to node %ld.\n",		/* 39 */
	"API newstate from node %ld trans node %ld state %ld why %ld.\n",
									/* 40 */
	"Intent from %ld client %ld state %ld why %ld ups %ld downs %ld"
		" other %ld.\n",					/* 41 */
	"Commit from %ld ups %ld downs %ld other %ld transid %lx.\n",	/* 42 */
	"Unblock from %ld transid %lx.\n",				/* 43 */
	"Master nodeup indication for node %ld.\n",			/* 44 */
	"Master nodeup seticsinfo failed for node %ld.\n",		/* 45 */
	"Master nodeup of %ld finished, state %lx flags %lx.\n",	/* 46 */
	"Failover started, taking over from node %ld.\n",		/* 47 */
	"Failover finished.\n",						/* 48 */
	"Querying cluster.\n",						/* 49 */
	"Got query from node %ld.\n",					/* 50 */
	"Got query reply from node %ld, trans %ld, save %ld.\n",	/* 51 */
	"Got roll-forward of %ld entries..\n",				/* 52 */
	"Node %ld is going away.\n",					/* 53 */
	"Got info for %ld key nodes: %ld %ld %ld %ld %ld %ld.\n",	/* 54 */
	"Declaring master %ld up.\n",					/* 55 */
	"Nodedown indication for %ld; master is %ld.\n",		/* 56 */
	"Updating stats, node %ld, state %ld, prev_state %ld.\n",	/* 57 */
	"Nodeup indication for node %ld.\n",				/* 58 */
	"Inform ics_nodeup() failed error %ld node %ld retry %ld.\n",	/* 59 */
	"clms_api_lock   (excl %ld) file %s line %ld pid %ld\n",	/* 60 */
	"clms_api_unlock (excl %ld) file %s line %ld pid %ld\n",	/* 61 */
	};

nsc_logcookie_t	*clmslog_cookie = NULL;

/*
 * clms_log_init()
 *	Initialize CLMS logging.
 *
 * Description:
 *	Calls nsc_log_init() to initialize the logging support for CLMS.
 *	Called from a single-threaded context (clms_init()), so we don't
 *	have to worry about races.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
void
clms_log_init(void)
{
	clmslog_cookie = nsc_log_init(CLMSLOG_DEPTH, GFP_KERNEL);
#ifdef DEBUG
	clms_do_logging = 0xffff;
#endif
}

/*
 * clms_log(msgnum, a, b, c, d, e, f, g)
 *	CLMS logging support.
 *
 * Calling/Exit State:
 *	No locking assumptions made.
 *	Always returns 0.
 *	Warning:  Not SMP safe.
 *
 * Description:
 *	Uses the nsc_log() support to do logging.
 */
int
clms_log(
	long msgnum,
	long a,
	long b,
	long c,
	long d,
	long e,
	long f,
	long g)
{
	SSI_ASSERT(clmslog_cookie != NULL);
	(void)nsc_log(clmslog_cookie, msgnum, a, b, c, d, e, f, g);
	if (clms_do_logging == 0xffff)
/* NSC_XXX: printk() won't accept variable as a format string. */
		printk(clmslog_msgs[msgnum], a, b, c, d, e, f, g);

	return 0;
}

void
print_clmslog(void)
{
	printk(KERN_DEBUG "CLMS log @ 0x%p\n", clmslog_cookie);
	nsc_log_print(clmslog_cookie, clmslog_msgs);
}

void
search_clmslog(
	int find)
{
	nsc_log_search(clmslog_cookie, clmslog_msgs, find);
}

void
print_transition_data(
	struct clms_transition_data *cdp)
{
	int i;

	if (cdp == NULL)
		return;
	printk(KERN_DEBUG "Transition data @ 0x%p:\n", cdp);
	printk(KERN_DEBUG "\tnode %u, surrogate node %u\n",
	       cdp->node,
	       cdp->surrogate_node);
	printk(KERN_DEBUG "\ttransition callbacks: ");
	for (i = 0; i < clms_num_subsys; i++)
		printk(KERN_DEBUG "%d ", cdp->transition_callbacks[i]);
	printk(KERN_DEBUG "\ttransition callbacks2: ");
	for (i = 0; i < clms_num_subsys; i++)
		printk(KERN_DEBUG "%d ", cdp->transition_callbacks2[i]);
	printk(KERN_DEBUG "\n");
	printk(KERN_DEBUG "\tcnlist %p, ready_cnlist %p, called_cnlist %p, private_data %p\n",
	       cdp->cnlist,
	       cdp->ready_cnlist,
	       cdp->called_cnlist,
	       cdp->private_data);
	printk(KERN_DEBUG "\tnext %p prev %p\n", cdp->next, cdp->prev);
}

void
print_clms_nodelist(
	struct clms_nodelist *nlist)
{
	int i;
	clusternode_t *nodelist;

	if (nlist == NULL)
		return;
	printk(KERN_DEBUG "Nodelist @ 0x%p:\n", nlist);
	printk(KERN_DEBUG "\tnext 0x%p, prev 0x%p, nodelist 0x%p, num_nodes %d\n",
	       nlist->next,
	       nlist->prev,
	       nlist->nodelist,
	       nlist->num_nodes);
	printk(KERN_DEBUG "\tn_rptr 0x%p, n_wptr 0x%p, n_lim 0x%p\n",
	       nlist->n_rptr,
	       nlist->n_wptr,
	       nlist->n_lim);
	nodelist = nlist->nodelist;
	printk(KERN_DEBUG "\tnodelist");
	for (i = 0; i < nlist->num_nodes; i++)
		printk(KERN_DEBUG " %u", nodelist[i]);
	printk(KERN_DEBUG "\n");
}

void
print_master_nodeup_queue(void)
{
	struct clms_transition_data *cdp;

	if (clms.clms_nodeup_queue.cq_first == NULL)
		printk(KERN_DEBUG "Nothing in nodeup queue.\n");

	for (cdp = clms.clms_nodeup_queue.cq_first;
	     cdp != NULL;
	     cdp = cdp->next)
		print_transition_data(cdp);
}

void
print_master_failover_nodeup_queue(void)
{
	struct clms_transition_data *cdp;

	if (clms.clms_failover_nodeup_queue.cq_first == NULL)
		printk(KERN_WARNING "Nothing in failover nodeup queue.\n");

	for (cdp = clms.clms_failover_nodeup_queue.cq_first;
	     cdp != NULL;
	     cdp = cdp->next)
		print_transition_data(cdp);
}

void
print_master_partial_queue(void)
{
	struct clms_transition_data *cdp;

	if (clms.clms_partial_nodeup_queue.cq_first == NULL)
		printk(KERN_WARNING "Nothing in partial nodeup queue.\n");

	for (cdp = clms.clms_partial_nodeup_queue.cq_first;
	     cdp != NULL;
	     cdp = cdp->next)
		print_transition_data(cdp);
}

void
print_master_nodejoin_queue(void)
{
	struct clms_transition_data *cdp;

	printk(KERN_WARNING "clms_all_svcs_assigned %d\n",
	       clms_all_svcs_assigned);
	if (clms.clms_node_join_queue.cq_first == NULL)
		printk(KERN_WARNING "Nothing in node join queue.\n");
	for (cdp = clms.clms_node_join_queue.cq_first;
	     cdp != NULL;
	     cdp = cdp->next)
		print_transition_data(cdp);
}

void
print_master_nodedown_queue(void)
{
	struct clms_transition_data *cdp;

	if (clms.clms_nodedown_queue.cq_first == NULL)
		printk(KERN_WARNING "Nothing in node down queue.\n");
	for (cdp = clms.clms_nodedown_queue.cq_first;
	     cdp != NULL;
	     cdp = cdp->next)
		print_transition_data(cdp);
}

void
print_clms_ctv(clms_ctv_t *ctv)
{
	printk(KERN_WARNING "\tctv.ups %d, ctv.downs %d, ctv.other %d, ctv.transid %lx\n",
	       ctv->ups,
	       ctv->downs,
	       ctv->other,
	       ctv->transid);
	printk(KERN_WARNING "\tctv.stamp.tv_sec %lu, ctv.stamp.tv_usec %lu\n",
	       ctv->stamp.tv_sec,
	       ctv->stamp.tv_usec);
}

void
print_clms_cli_tr_data(clms_cli_tr_data_t *cnp)
{
	int i;

	printk(KERN_DEBUG "clms_cli_tr_data_t @ 0x%p\n", cnp);
	printk(KERN_DEBUG "\tnext %p, prev %p, node %u, trans %d, why %d\n",
	       cnp->next,
	       cnp->prev,
	       cnp->node,
	       cnp->trans,
	       cnp->why);
	print_clms_ctv(&cnp->ctv);
	if (cnp->trans == CLMS_STATE_KCLEANUP) {
		printk(KERN_DEBUG "\tnodedown data:\n");
		printk(KERN_DEBUG "\t\tclms_handle %lx, surrogate_node %u\n",
		       cnp->data.nodedown.clms_handle,
		       cnp->data.nodedown.surrogate_node);
		printk(KERN_DEBUG "\t\twasntup %d, master %u, down_trans %d\n",
		       cnp->data.nodedown.wasntup,
		       cnp->data.nodedown.master_node,
		       cnp->data.nodedown.down_trans);
		printk(KERN_DEBUG "\t\tservers");
		for (i = 0; i < clms_num_key_svcs; i++) {
			printk(KERN_DEBUG " %u", cnp->data.nodedown.servers[i]);
		}
		printk(KERN_DEBUG "\n");
	}
}

void
print_client_intent(void)
{
	if (clms.clms_api_trans_intent.present == 0) {
		printk(KERN_DEBUG "No intent outstanding\n");
		return;
	}
	printk(KERN_DEBUG "Intent node %u, state %d, why %d\n",
	       clms.clms_api_trans_intent.client_node,
	       clms.clms_api_trans_intent.new_state,
	       clms.clms_api_trans_intent.why);
	printk(KERN_DEBUG "Intent ctv:\n");
	print_clms_ctv(&(clms.clms_api_trans_intent.ctv));
}

void
print_client_trans_queue(void)
{
	clms_cli_tr_data_t *cnp;

	if (clms.clms_client_trans_queue.first == NULL) {
		printk(KERN_DEBUG "Nothing in client trans queue.\n");
		return;
	}
	printk(KERN_DEBUG "clms_client_trans_queue first %p last %p size %d\n",
	       clms.clms_client_trans_queue.first,
	       clms.clms_client_trans_queue.last,
	       clms.clms_client_trans_queue.size);
	for (cnp = clms.clms_client_trans_queue.first;
	     cnp != NULL;
	     cnp = cnp->next) {
		print_clms_cli_tr_data(cnp);
	}
}

void
print_client_save_queue(void)
{
	clms_cli_tr_data_t *cnp;

	if (clms.clms_client_save_queue.first == NULL) {
		printk(KERN_DEBUG "Nothing in client save queue.\n");
		return;
	}
	printk(KERN_DEBUG "clms_client_save_queue first %p last %p size %d\n",
	       clms.clms_client_save_queue.first,
	       clms.clms_client_save_queue.last,
	       clms.clms_client_save_queue.size);
	for (cnp = clms.clms_client_save_queue.first;
	     cnp != NULL;
	     cnp = cnp->next) {
		print_clms_cli_tr_data(cnp);
	}
}

void
print_clms_current_ctv(void)
{
	printk(KERN_DEBUG "Current CTV:\n");
	print_clms_ctv(&(clms.clms_ctv));
}

void
print_clms_trans_hist(void)
{
	clms_transition_t *tr;
	int i, size, trans;
	static char *str_trans[] = { "Never Up",
				     "Down",
				     "Coming Up",
				     "Up",
				     "Shutdown",
				     "Going Down",
				     "KCleanup",
				     "UCleanup",
				     "Dummy Intent",
				     "Dummy",
				     "",
				   };
	static char *str_why[] = { "Invalid",
				   "API",
				   "System",
				   "Unexpected",
				 };

	if (clms.clms_api_trans_wrapped == 0 &&
	    clms.clms_api_trans_hist == clms.clms_api_trans_ptr) {
		printk(KERN_DEBUG "Current API history is empty!\n");
		return;
	}
	if (clms.clms_api_trans_wrapped) {
		size = CLMS_TRANS_HIST_SZ;
		tr = clms.clms_api_trans_ptr;
	}
	else {
		size = ((char *)clms.clms_api_trans_ptr -
			(char *)clms.clms_api_trans_hist) /
				        sizeof(clms_transition_t);
		tr = clms.clms_api_trans_hist;
	}
	printk(KERN_DEBUG "Current API history (%d entries):\n", size);
	for (i = 0; i < size; i++) {
		printk(KERN_DEBUG "Entry %d @ %p:\n", i, tr);
		trans = tr->transition;
		if (trans >= CLMS_STATE_DUMMY_INTENT)
			trans -= (CLMS_STATE_DUMMY_INTENT -
						    CLMS_STATE_UCLEANUP - 1);
		printk(KERN_DEBUG "\tnode %u, transition %d (%s), why %d (%s)\n",
		       tr->node,
		       tr->transition,
		       str_trans[trans],
		       tr->why,
		       str_why[tr->why]);
		printk(KERN_DEBUG "\ttransid %lx, stamp secs %lx usecs %lx\n",
		       tr->transid,
		       tr->stamp.tv_sec,
		       tr->stamp.tv_usec);
		tr++;
		if (clms.clms_api_trans_wrapped &&
		    tr == (clms.clms_api_trans_hist + CLMS_TRANS_HIST_SZ))
			tr = clms.clms_api_trans_hist;
	}
}

void
print_clms_cluster_stat(void)
{
	printk(KERN_DEBUG ":\n");
	printk(KERN_DEBUG "\tnum_nodes %d, num_nodes_up %d, num_cluster_servers %d\n",
	       clms.clms_cluster_stat.clms_num_nodes,
	       clms.clms_cluster_stat.clms_num_nodes_up,
	       clms.clms_cluster_stat.clms_num_cluster_servers);
	printk(KERN_DEBUG "\tnum_up_transitions %d, num_down_transitions %d, save_down_transitions %d\n",
	       clms.clms_cluster_stat.clms_num_up_transitions,
	       clms.clms_cluster_stat.clms_num_down_transitions,
	       clms.clms_cluster_stat.clms_save_down_transitions);
	printk(KERN_DEBUG "\tclms.clms_transition_flag %x\n",
	       clms.clms_transition_flag);
}

void
print_clms_node_info_1(clusternode_t node)
{
	int j;
	static char *states[] = { "never up", "partial coming up",
				  "coming up", "ics up", "up",
				  "going down", "down", "half up",
				  "half down", "inform coming up",
				  "regroup known" };
	static char *str_trans[] = { "Never Up",
				     "Down",
				     "Coming Up",
				     "Up",
				     "Shutdown",
				     "Going Down",
				     "KCleanup",
				     "UCleanup",
				   };
	static char *str_why[] = { "Invalid",
				   "API",
				   "System",
				   "Unexpected",
				 };

	printk(KERN_DEBUG "\tnode %u, flags %x, ups %d, downs %d\n",
	       clms.clms_node_info[node].node,
	       clms.clms_node_info[node].flags,
	       clms.clms_node_info[node].api_state.up_transitions,
	       clms.clms_node_info[node].api_state.down_transitions);
	printk(KERN_DEBUG "\t\tstate");
	for (j = 0; j < 11; j++) {
		if (clms.clms_node_info[node].current_state & (1 << j))
			printk(KERN_DEBUG " %s", states[j]);
	}
	printk(KERN_DEBUG ", prev");
	for (j = 0; j < 11; j++) {
		if (clms.clms_node_info[node].prev_state & (1 << j))
			printk(KERN_DEBUG " %s", states[j]);
	}
	printk(KERN_DEBUG "\n");
	printk(KERN_DEBUG "\t\tAPI state %s, prev %s, why %s\n",
	       str_trans[clms.clms_node_info[node].api_state.state],
	       str_trans[clms.clms_node_info[node].api_state.prev_state],
	       str_why[clms.clms_node_info[node].api_state.why]);
	printk(KERN_DEBUG "\t\t    transid %lx\n",
	       clms.clms_node_info[node].api_state.transid);
	printk(KERN_DEBUG "\t\t    stamp sec %lu usec %lu\n",
	       clms.clms_node_info[node].api_state.stamp.tv_sec,
	       clms.clms_node_info[node].api_state.stamp.tv_usec);
	printk(KERN_DEBUG "\t\t    first transid %lx\n",
	       clms.clms_node_info[node].api_state.first_transid);
	printk(KERN_DEBUG "\t\t    first stamp sec %lu usec %lu\n",
	       clms.clms_node_info[node].api_state.first_stamp.tv_sec,
	       clms.clms_node_info[node].api_state.first_stamp.tv_usec);
	printk(KERN_DEBUG "\t\t    cpus %d speed %ld\n",
	       clms.clms_node_info[node].online_cpus,
	       clms.clms_node_info[node].cpu_power);
}

void
print_clms_node_info(void)
{
	int i;

	printk(KERN_DEBUG "CLMS node info:\n");
	for (i=1; i <= NSC_MAX_NODE_VALUE; i++) {
		print_clms_node_info_1(i);
	}
}

void
print_clms(void)
{
	printk(KERN_DEBUG "clms_node_info: 0x%p\n", clms.clms_node_info);
	printk(KERN_DEBUG "clms_cluster_stat: 0x%p\n", &clms.clms_cluster_stat);
	printk(KERN_DEBUG "clms_node_lock: 0x%p\n", &clms.clms_node_lock);
	printk(KERN_DEBUG "clms_node_cond: 0x%p\n", &clms.clms_node_cond);
	printk(KERN_DEBUG "clms_cluster_server: 0x%p\n", clms.clms_cluster_server);
	printk(KERN_DEBUG "clms_cluster_floater: 0x%p\n", clms.clms_cluster_floater);
	printk(KERN_DEBUG "clms_cluster_svr_cond: 0x%p\n", &clms.clms_cluster_svr_cond);
	printk(KERN_DEBUG "clms_cluster_svr_lock: 0x%p\n", &clms.clms_cluster_svr_lock);
	printk(KERN_DEBUG "clms_key_service_node_lock: 0x%p\n", &clms.clms_key_service_node_lock);
	printk(KERN_DEBUG "clms_nodeup_queue: 0x%p\n", &clms.clms_nodeup_queue);
	printk(KERN_DEBUG "clms_partial_nodeup_queue: 0x%p\n", &clms.clms_partial_nodeup_queue);
	printk(KERN_DEBUG "clms_failover_nodeup_queue: 0x%p\n", &clms.clms_failover_nodeup_queue);
	printk(KERN_DEBUG "clms_node_join_queue: 0x%p\n", &clms.clms_node_join_queue);
	printk(KERN_DEBUG "clms_nodedown_queue: 0x%p\n", &clms.clms_nodedown_queue);
	printk(KERN_DEBUG "clms_client_trans_queue: 0x%p\n", &clms.clms_client_trans_queue);
	printk(KERN_DEBUG "clms_client_save_queue: 0x%p\n", &clms.clms_client_save_queue);
	printk(KERN_DEBUG "clms_ntrans_flag: %d\n", clms.clms_ntrans_flag);
	printk(KERN_DEBUG "clms_ntrans_lock: 0x%p\n", &clms.clms_ntrans_lock);
	printk(KERN_DEBUG "clms_ntrans_cond: 0x%p\n", &clms.clms_ntrans_cond);
	printk(KERN_DEBUG "clms_svc: 0x%p\n", &clms.clms_svc);
	printk(KERN_DEBUG "clms_transition_nd: %d\n", clms.clms_transition_nd);
	printk(KERN_DEBUG "clms_transition_flag: %d\n", clms.clms_transition_flag);
	printk(KERN_DEBUG "clms_transition_lock: 0x%p\n", &clms.clms_transition_lock);
	printk(KERN_DEBUG "clms_transition_cond: 0x%p\n", &clms.clms_transition_cond);
	printk(KERN_DEBUG "clms_failover_lock: 0x%p\n", &clms.clms_failover_lock);
	printk(KERN_DEBUG "clms_ctv: 0x%p\n", &clms.clms_ctv);
	printk(KERN_DEBUG "clms_ctv_failover: %d\n", clms.clms_ctv_failover);
	printk(KERN_DEBUG "clms_ctv_rw_lock: 0x%p\n", &clms.clms_ctv_rw_lock);
	printk(KERN_DEBUG "clms_ctv_lock: 0x%p\n", &clms.clms_ctv_lock);
	printk(KERN_DEBUG "clms_ctv_cond: 0x%p\n", &clms.clms_ctv_cond);
	printk(KERN_DEBUG "clms_api_rw_lock: 0x%p\n", &clms.clms_api_rw_lock);
	printk(KERN_DEBUG "clms_api_hist_lock: 0x%p\n", &clms.clms_api_hist_lock);
	printk(KERN_DEBUG "clms_api_trans_hist: 0x%p\n", clms.clms_api_trans_hist);
	printk(KERN_DEBUG "clms_api_trans_ptr: 0x%p\n", clms.clms_api_trans_ptr);
	printk(KERN_DEBUG "clms_api_trans_wrapped: %d\n", clms.clms_api_trans_wrapped);
	printk(KERN_DEBUG "clms_api_trans_intent: 0x%p\n", &clms.clms_api_trans_intent);
}

#endif /* DEBUG || DEBUG_TOOLS */
