/*
 * 	CLMS bootup code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the CLMS Management code. It provides CLMS
 * initialization and some common subroutines.
 */

#include <linux/module.h>
#include <linux/delay.h>
#include <linux/reboot.h>
#include <linux/init.h>
#include <linux/seq_file.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/nodelist.h>
#include <cluster/icsgen.h>
#include <cluster/ics.h>
#include <cluster/ics_proto.h>
#include <cluster/node_monitor.h>

#include <asm/uaccess.h>

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>


clusternode_t clms_master_node = CLUSTERNODE_INVAL;
					/* Node Number of CLMS Coordinator */
struct clms clms;			/* Structure for the CLMS */

int clms_master_cold_boot = TRUE;	/* TRUE => global cold boot. */
int clms_cold_boot = TRUE;		/* TRUE => local cold boot. */

LOCK_T clms_key_service_lock;

EVENT_T	clms_assign_service_event;

int clms_im_a_secondary;
int clms_num_secondaries;
clms_secondary_list_t *clms_secondary_list_head = NULL;

static clusternode_t clms_force_root = CLUSTERNODE_INVAL;

int clms_ready_for_probe = 0;
static int clms_doing_probeonly = 0;

/*
 * clms_init()
 *	Start the clms by initializing data structures and registering
 *	CLMS subsystems.
 *
 * Description:
 *	Initialize data structures, locks, synchronization variables,
 *	spawn daemons, etc.  Find the CLMS master, prepare to accept
 *	connections from clients, start assigning key services to their
 *	respective nodes.  Mark the node as up for CLMS purposes, do
 *	master or client initialization, and wait for all key services
 *	to be assigned.
 *
 *	This routine is called from nsc_main_init() (which is called
 *	from main()) after ICS initialization.
 *
 * Parameters:
 *	None.
 */
void
clms_init(void)
{
	int i;
	int error;
	int rval;
	void clms_space_init(void);

	/*
	 * Initialize Data Structures.
	 */
	clms_space_init();

#if defined(DEBUG) || defined(DEBUG_TOOLS)
	clms_log_init();
#endif

	/*
	 * Initialize CLMS Handle Registry
	 */
	clms_init_handle_registry();

	/*
	 * Initialize CLMS w.r.t. ICS
	 */
	clms_ics_init();

	/*
	 * Locate the CLMS master.  This also gets the list of potential
	 * secondaries.
	 */
	clms_master_node = clms_find_master(&clms_im_a_secondary,
					    &clms_num_secondaries,
					    &clms_secondary_list_head);
	/*
	 * Build a floater entry for each service.
	 */
	for (i = 0; i < clms_num_key_svcs; i++)
		clms_set_cluster_server(i,
					CLUSTERNODE_INVAL,
					CLUSTER_FLOATER_ENTRY,
					CLUSTER_SERVER_NOT_READY);

	/*
	 * Call ics_nodeup() to initialize ICS for this node.
	 */
	ics_nodeup(this_node);
	/*
	 * Get the CTV and node state locks; we don't need to get the CTV or
	 * API shared locks, since we're still booting and there's no one
	 * looking at the state.  (We probably don't need these spinlocks,
	 * either, but better safe.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	LOCK_COND_LOCK(&clms.clms_node_lock);
	/*
	 * Set the node state to CLMS_NODE_COMING_UP; if we're a client, we
	 * use this to synchronize with the master when we fully join the
	 * cluster.
	 */
	clms_set_node_status_l(this_node, CLMS_NODE_COMING_UP);
	/*
	 * If we're the master, set our state to CLMS_NODE_HALF_UP, bump the
	 * CTV (this is the first time it's bumped), and set the API state.
	 */
	if (this_node == clms_master_node) {
		clms_set_node_status_l(this_node, CLMS_NODE_HALF_UP);
		++clms.clms_ctv.ups;
		CLMS_NEW_TRANSID(&(clms.clms_ctv));
		clms_api_transition_l(this_node,
				      CLMS_STATE_COMINGUP,
				      CLMS_TRANS_WHY_SYSTEM,
				      clms.clms_ctv);
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);

	/* We can now answer probes from other nodes. */
	clms_ready_for_probe = 2;


	if (this_node == clms_master_node)
		clms_master_init(0);
	else
		clms_client_init();

	/*
	 * Wait for information about critical resources (CLMS key services)
	 * to be available.  Note that at this point, we do not wait for the
	 * resource to be ready for use. We just need the information, like
	 * who is providing the root file system.
	 */
	clms_waitfor_resource_info();

	/*
	 * Determine if this is a cold boot.
	 */
	if (this_node == clms_master_node)
		error = rclms_cold_boot(clms_master_node, &rval,
					&clms_cold_boot);
	else
		error = RCLMS_COLD_BOOT(clms_master_node, &rval,
					&clms_cold_boot);
	if (error != 0)
		panic("clms_init: RCLMS_COLD_BOOT: rpc error = %d\n", error);
	SSI_ASSERT(rval == 0);

	return;
}

/*
 * clms_space_init()
 *	Initialize clms data structures, locks, synchronization variables etc.
 *
 * Description:
 *	This function is called at the beginning of nsc_main_init() to
 *	initialize all clms data structures, spawn daemons etc. This is
 *	necessary to prepare to accept registrations from services.
 *
 * Parameters:
 *	None.
 */
void
clms_space_init(void)
{
	int	i;

	/*
	 * Currently it is assumed that if the primary node goes down,
	 * the cluster needs to be rebooted. If this assumption changes
	 * the following initialization has to be done only once during
	 * the first time when the cluster is brought up.
	 * NOTE: entry 0 is not used; but let's initialize it, anyway.
	 */
	for (i=0; i <= NSC_MAX_NODE_VALUE; i++) {
		clms.clms_node_info[i].node = i;
		clms.clms_node_info[i].flags = 0;
		clms.clms_node_info[i].current_state = CLMS_NODE_NEVER_UP;
		clms.clms_node_info[i].prev_state    = CLMS_NODE_NEVER_UP;
		clms.clms_node_info[i].api_state.state = CLMS_STATE_NEVERUP;
		clms.clms_node_info[i].api_state.prev_state =
							  CLMS_STATE_NEVERUP;
		clms.clms_node_info[i].total_cpus = 0;
		clms.clms_node_info[i].online_cpus = 0;
		clms.clms_node_info[i].total_memory = 0;
		clms.clms_node_info[i].cpu_power = 0;
	}
	/*
	 * Lock and synchronization variable for clms node information
	 * and cluster statistics information.
	 */
	INIT_CONDITION(&clms.clms_node_cond);
	INIT_COND_LOCK(&clms.clms_node_lock);

	/*
 	 * Lock and condition variable for information about cluster key
	 * servers.
	 */
	INIT_CONDITION(&clms.clms_cluster_svr_cond);
	INIT_COND_LOCK(&clms.clms_cluster_svr_lock);

	INIT_SPIN_LOCK(&clms.clms_key_service_node_lock);

	/*
	 * Initialize the failover nodeup queue, used when a node tries to
	 * come in during failover.
	 */
	clms_transition_qinit(&clms.clms_failover_nodeup_queue, 0);

	/*
	 * Initialize node transition queues used by clms master.
	 */
	clms_transition_qinit(&clms.clms_nodeup_queue, CLMS_NODE_UP);

	clms_transition_qinit(&clms.clms_partial_nodeup_queue,
					CLMS_NODE_PARTIAL_COMING_UP);

	clms_transition_qinit(&clms.clms_node_join_queue, CLMS_NODE_UP);

	clms_transition_qinit(&clms.clms_nodedown_queue, CLMS_NODE_DOWN);

	/*
	 * Lock and synchronization variable for node transitions
	 * (up and down).
	 */
	INIT_CONDITION(&clms.clms_ntrans_cond);
	INIT_COND_LOCK(&clms.clms_ntrans_lock);

	INIT_LOCK(&clms.clms_failover_lock);

	INIT_LOCK(&clms.clms_parameters_lock);

	INIT_LOCK(&clms_key_service_lock);

	INIT_EVENT(&clms_assign_service_event);

	INIT_CONDITION(&clms.clms_transition_cond);
	INIT_COND_LOCK(&clms.clms_transition_lock);
	clms.clms_transition_flag = 0;

	clms.clms_client_trans_queue.first = NULL;
	clms.clms_client_trans_queue.last = NULL;
	clms.clms_client_trans_queue.size = 0;

	clms.clms_api_trans_intent.present = 0;

	clms.clms_ctv.ups = clms.clms_ctv.downs = clms.clms_ctv.other = 0;
	INIT_RW_LOCK(&clms.clms_ctv_rw_lock);
	INIT_COND_LOCK(&clms.clms_ctv_lock);
	INIT_CONDITION(&clms.clms_ctv_cond);

	INIT_RW_LOCK(&clms.clms_api_rw_lock);
	INIT_SPIN_LOCK(&clms.clms_api_hist_lock);

	clms.clms_api_trans_hist = kmalloc_nofail(
		sizeof(clms_transition_t) * CLMS_TRANS_HIST_SZ);
	clms.clms_api_trans_ptr = clms.clms_api_trans_hist;
	clms.clms_api_trans_wrapped = 0;
}

/*
 * clms_ics_init()
 *	Initialize CLMS with respect to ICS.
 *
 * Description:
 *	Register with ics and if this is a clms client, then contact the
 *	master and declare the clms master as "Up".  The
 *	ics_nodeinfo_callback() currently establishes all connections
 *	to the CLMS Master. This will change in the future and the clms
 *	will explicitly contact the clms master using, probably, ics
 *	functions.
 *
 * Parameters:
 *	None.
 *
 */
int
clms_ics_init(void)
{
	/*
	 * Register the routines which are expected to be called by the
	 * ICS when the ICS detects a node coming up or going down. Once these
	 * callbacks are registered, RPCs and messages will start arriving.
	 *
	 * It is necessary to register ICS callbacks for normal ICS
	 * communication to proceed.
	 *
 	 * In TCP-based ICS, the following function also contacts the clms
 	 * master and establishes all the ICS connections.
	 */
	/*
	 * The node down indication normally comes from the Tandem
	 * Regroup Module, but if Regroup is turned off it may also
	 * come from ICS.
	 */
	ics_nodeinfo_callback(clms_nodeup_ind, clms_nodedown_ind);

	return 0;
}

/*
 * clms_waitfor_resource_info()
 *	Wait for key services information to be available.
 *
 * Description:
 * 	Called at the end of clms_init() to wait for information about
 *	key services like the root file system.
 *
 *	This function does NOT wait for the key services to be available
 *	(i.e. in the READY state).  It simply waits for the information.
 *	For example, this function will wait until the identity of the node
 *	providing the root file system is known. At this point, the root file
 *	system may or may not be actually ready for use.
 *
 * Parameters:
 *	None.
 */
int
clms_waitfor_resource_info(void)
{

	if (this_node == clms_master_node) {
		clms_master_waitfor_resource_info();
	}
	else {
		clms_client_waitfor_resource_info();
	}

	return 0;
}

/*
 * clms_join_cluster()
 *	Join the cluster by contacting the clms master.
 *
 * Description:
 *	This function is called by nsc_main_init() after all the
 *	necessary initializations of all the NSC components have
 *	been done.
 *
 *	On the clms master node, there is nothing to do. But, at the
 *	clms client nodes the nodes have to send an rpc to the
 *	clms master to join the cluster.
 *
 *	The appropriate routine is called to join the cluster.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	0.
 */
int
clms_join_cluster(void)
{
	if (this_node == clms_master_node)
		clms_master_join_cluster();
	else
		clms_client_join_cluster();
	return 0;
}

/*
 * clms_startup_node()
 *	Function which declares the node as "Fully Up".
 *
 * Description:
 * 	Called from nsc_main_init_postroot() to finally declare the node as
 * 	fully up. If the node is the clms master, currently there is nothing
 * 	to do. If the node is the clms client, then inform the master that the
 *	node is fully up (all necessary resources are available, root
 *	has been mounted etc.).
 *
 * Parameters:
 *	None.
 */
int
clms_startup_node(void)
{
	if (this_node == clms_master_node)
		return clms_master_startup_node();

	return clms_client_startup_node();
}

/*
 * clms_is_higher_secondary()
 *	Does node1 have higher precedence over node2 in the CLMS secondary list?
 *
 * Description:
 *	The CLMS secondary list is sorted by precedence according to
 *	the ssi boot configuration file. If the first passed node, node1,
 *	is located in front of the second passed node, node2, in the CLMS
 *	secondary list, returns TRUE. Otherwise, returns FALSE. If we're not
 *	ready to check, or if either of the two nodes isn't in the CLMS
 *	secondary list, returns FALSE.
 *
 * Parameters:
 *	node1	The node to test for higher precedence.
 *	node2	The node to test for lower precedence.
 */
boolean_t
clms_is_higher_secondary(
	clusternode_t	node1,
	clusternode_t	node2)
{
	clms_secondary_list_t	*ent;
	int found_node1 = 0;

	if (clms_ready_for_probe == 0)
		return(B_FALSE);
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next) {
		if (ent->node == node1)
			found_node1 = 1;
		else if (ent->node == node2 && found_node1)
			return(B_TRUE);
		else if (ent->node == node2 && !found_node1)
			return(B_FALSE);
	}
	return(B_FALSE);
}

/*
 * clms_is_secondary()
 *	Is the passed node a CLMS secondary?
 *
 * Description:
 *	If the passed node is in the CLMS secondary list, returns TRUE,
 *	else returns FALSE.  If we're not ready to check, returns FALSE.
 *
 * Parameters:
 *	node	The node to check.
 */
boolean_t
clms_is_secondary(
	clusternode_t	node)
{
	clms_secondary_list_t	*ent;

	if (clms_ready_for_probe == 0)
		return(B_FALSE);
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next)
		if (ent->node == node) {
			if (node == this_node && !clms_im_a_secondary)
				break;
			return(B_TRUE);
		}
	return(B_FALSE);
}

/*
 * clms_is_secondary_by_addr()
 *	Is the node with the specified address a CLMS secondary?
 *
 * Description:
 *	If the passed node is in the CLMS secondary list, returns TRUE,
 *	else returns FALSE.  If we're not ready to check, returns FALSE.
 *
 * Parameters:
 *	node	The node to check.
 */
boolean_t
clms_is_secondary_by_addr(
	icsinfo_t	*icsinfo)
{
	clms_secondary_list_t	*ent;

	if (clms_ready_for_probe == 0)
		return(B_FALSE);
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next)
		if (memcmp((void *)&ent->addr, (void *)icsinfo,
			 sizeof (icsinfo_t)) == 0)
			return(B_TRUE);
	return(B_FALSE);
}

/*
 * clms_getclmsinfo()
 * 	Parses the clms information from the parameter line.
 *
 * Description:
 * 	The format of the clms rootnode information is
 *	<clms node #>:<IP address>	(for TCP-based ICS)
 *
 * 	This routine parses the string and stores the node number and
 *	icsinfo in the given parameters. In the case of TCP-based ICS,
 *	the icsinfo is returned as it was in the string.
 *
 *	The clms information is provided through the parameter line.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	char		*clmsinfo	- ptr to clms information to be parsed
 *	clusternode_t	*cnodenum	- ptr to clms master node number
 *	long		*cicsinfo	- ptr to clms master icsinfo
 *
 * Returns
 *	None.  May panic with an appropriate message if there was an error.
 *
 */
static int
clms_getclmsinfo(
	char		*clmsinfo,
	clusternode_t	*cnodenum,
	char		*cicsinfo)
{
	char	*s, *ptr1, *ptr;
	char	*t;
	int	ipaddrlen;

	s = clmsinfo;
	if (s == NULL || *s == '\0')
		return -EINVAL;
	/*
	 * Save the current pointer; if there's a ':' in the token, we have
	 * an IP address, so we've got TCP/IP transport and we have to get
	 * the address.
	 */
	t = s;
	ptr1 = clms_next_token(&t, ':');
	/*
	 * First get the nodenum that we just parsed out.
	 */
	*cnodenum = (clusternode_t)simple_strtoul(ptr1, &ptr, 10);
	if ((*ptr != '\0') || (*cnodenum < 1 || *cnodenum > NSC_MAX_NODE_VALUE)
	    || t == NULL)
		return 0;
	ics_set_transport(ICS_TRANSPORT_IP);
	if (strlen(t) < 1)
		return EINVAL;
	ipaddrlen = ics_get_addrlen();
	memset(cicsinfo, 0, ipaddrlen);
	strncpy(cicsinfo, t, ipaddrlen);

	return 0;
}

static int clms_working_secondaries;

/*
 * clms_only_probe()
 *	Tell CLMS to only probe the passed CLMS secondary.
 *
 * Description:
 *	If we get a probe from a lower-numbered node and he's a CLMS
 *	secondary, we start _only_ probing him until he either tells
 *	us the CLMS master or stops responding.  Does nothing if we're
 *	not ready for this, or if the node isn't in the list.
 *
 * Parameters:
 *	node	The node to probe.
 */
static void
clms_only_probe(
	clusternode_t	node)
{
	clms_secondary_list_t   *ent;

	if (clms_ready_for_probe == 0)
		goto ret;
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next)
		if (ent->node == node)
			break;
	if (ent != NULL) {
		if (clms_doing_probeonly == 0) {
			CLMSLOG(CLMSLOG_ONLYPROBE, ent->node, 0, 0, 0, 0, 0, 0);
			ent->probe_only = 1;
			clms_doing_probeonly = 1;
		}
	}
ret:	;
	return;
}

/*
 * clms_dont_probe()
 *	Tell CLMS not to probe the passed CLMS secondary.
 *
 * Description:
 *	If we get a probe from a higher-numbered node and he's a CLMS
 *	secondary, we stop probing him, since he will definitely not
 *	become the master; either we will or a lower-numbered secondary
 *	will.  If we're not ready for this or if the node isn't in the
 *	list, we do nothing.
 *
 * Parameters:
 *	node	The node to probe.
 */
static void
clms_dont_probe(
	clusternode_t	node)
{
	clms_secondary_list_t   *ent;

	if (clms_ready_for_probe == 0)
		goto ret;
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next)
		if (ent->node == node)
			break;
	if (ent != NULL && ent->do_probe != 0) {
		ent->do_probe = 0;
		clms_working_secondaries--;
		SSI_ASSERT(clms_working_secondaries > 0);
	}
ret:	;
	return;
}

/*
 * clms_get_probe_info()
 *	Get the success or failover of a CLMS probe from another node.
 *
 * Description:
 *	This is called from low-level ICS to get the success or failure
 *	of a CLMS probe from another node.  See clms_find_master() for
 *	more information.
 *
 * Parameters:
 *	hesasecondary	The probing node is a secondary.
 *	hesamember	The node thinks he is a member.
 *	his_node	The number of the probing node.
 *	master_node	Pointer to an area to receive the master node number.
 */
int
clms_get_probe_info(
	int		hesasecondary,
	int		hesamember,
	clusternode_t	his_node,
	clusternode_t	*master_node)
{
	LOCK_LOCK(&clms.clms_failover_lock);
	/*
         * Return an error if we're not ready to handle probes.
         */
	if (clms_ready_for_probe == 0) {
		UNLOCK_LOCK(&clms.clms_failover_lock);
		*master_node = CLUSTERNODE_INVAL;
		return(CLMS_PROBE_ERR_NOTREADY);
	}
	/*
	 * We're ready to handle probes, but we're still in
	 * the middle of probing, and don't yet know the CLMS
	 * master.  If he thinks he's a secondary but we don't,
	 * or if we're not a secondary, complain and send back
	 * an error.
	 */
	if (clms_ready_for_probe == 1) {
		/* Are we trying to force the root? */
		if (clms_force_root != CLUSTERNODE_INVAL) {
			/* Yes. */
			UNLOCK_LOCK(&clms.clms_failover_lock);
			if (clms_force_root == his_node)
				return CLMS_PROBE_ERR_FORCE_ROOT_YOU;
			else
				return CLMS_PROBE_ERR_FORCE_ROOT_NOTYOU;
		}
		if (hesasecondary != 0 && !clms_is_secondary(his_node)) {
			UNLOCK_LOCK(&clms.clms_failover_lock);
			printk(KERN_WARNING "Node %u claims to be a root secondary, but isn't!", his_node);
			return(CLMS_PROBE_ERR_HESNOTSEC);
		}
		if (!clms_is_secondary(this_node)) {
			UNLOCK_LOCK(&clms.clms_failover_lock);
			printk(KERN_WARNING "Node %u thinks I'm a root secondary, but I'm not!", his_node);
			return(CLMS_PROBE_ERR_IMNOTSEC);
		}
		if (clms_is_higher_secondary(his_node, this_node)) {
			clms_only_probe(his_node);
			UNLOCK_LOCK(&clms.clms_failover_lock);
			return(CLMS_PROBE_ERR_DONTKNOW);
		}
		if (clms_is_higher_secondary(this_node, his_node)) {
			clms_dont_probe(his_node);
			UNLOCK_LOCK(&clms.clms_failover_lock);
			return(CLMS_PROBE_ERR_DONTKNOW);
		}
		UNLOCK_LOCK(&clms.clms_failover_lock);
		return(CLMS_PROBE_ERR_DONTKNOW);
	}
	SSI_ASSERT(clms_ready_for_probe == 2);
	*master_node = clms_master_node;
	UNLOCK_LOCK(&clms.clms_failover_lock);
	/* Someone probed us that thinks he is a member and we don't. */
	if (clms_master_node == this_node && hesamember &&
	    !clms_isnodeavail(his_node))
		return CLMS_PROBE_ERR_DIE_ALREADY;
	return(CLMS_PROBE_ERR_OKAY);
}

/*
 * clms_insert_entry()
 *	Insert an entry into the CLMS secondary list.
 *
 * Description:
 *	Insert an entry into its proper position in the sorted CLMS
 *	secondary list.
 *
 * Parameters:
 *	head		Pointer to the head of the list.
 *	ent		Pointer to the entry to insert.
 */
void
clms_insert_entry(
	clms_secondary_list_t	**head,
	clms_secondary_list_t	*ent)
{
	clms_secondary_list_t	*last, *tent;

	ent->next = NULL;
	/*
	 * If the list is empty, create it.
	 */
	if (*head == NULL) {
		*head = ent;
		return;
	}
	/*
	 * Find an entry with a higher node number.  If there is one, insert
	 * the new entry before it (this won't be the very first entry, we
	 * eliminated that one above).  If there isn't one, append the new
	 * entry to the end.  If we don't sort the list, this just finds
	 * the last entry.
	 */
	last = NULL;
	for (tent = *head; tent != NULL; tent = tent->next) {
		last = tent;	/* Remember the last entry we saw. */
	}
	/*
	 * There was none.  Append the new entry to the list.
	 */
	last->next = ent;
	return;
}

int
clms_parse_nodelist(const char *newinfo, clms_secondary_list_t **second_list,
		    int *num_seconds, int *im_a_secondary, char **info)
{
	int		ret = 0;
	clusternode_t	master_node;
	icsinfo_t	master_addr;
	clms_secondary_list_t *new_list = NULL, *new_entry;
	char		*sv_clmsinfo = NULL, *nextsvc, *svc;
	int		im_a_2ndary = 0, num_2nds = 0;
	int		info_sz;

	/*
	 * Build the list of potential secondaries from the info in the
	 * boot file.  Note whether we're one of them.
	 */
	info_sz = strlen(newinfo) + 1;
	if (info_sz > CLUSTER_CLMSINFO_SZ) {
		ret = -ENOSPC;
		goto out;
	}
	sv_clmsinfo = kmalloc(info_sz, GFP_KERNEL);
	if (sv_clmsinfo == NULL) {
		ret = -ENOMEM;
		goto out;
	}
	memcpy(sv_clmsinfo, newinfo, info_sz);
	nextsvc = sv_clmsinfo;
	while ((svc = clms_next_token(&nextsvc, ','))) {
		memset((char *)&master_addr, 0, sizeof(icsinfo_t));
		ret = clms_getclmsinfo(svc, &master_node, (char *)&master_addr);
		if (ret < 0)
			break;
		num_2nds++;
		new_entry = kzmalloc_nofail(sizeof(clms_secondary_list_t));
		new_entry->node = master_node;
		memcpy((char *)&(new_entry->addr), (char *)&master_addr,
		       sizeof(icsinfo_t));
		clms_insert_entry(&new_list, new_entry);
		if (master_node == this_node) {
			im_a_2ndary = num_2nds;
			new_entry->do_probe = 0;
		}
	}
 out:
	if (ret == 0 && num_2nds == 0)
		ret = -EINVAL;
	if (ret == 0) {
		memcpy(sv_clmsinfo, newinfo, info_sz);
		LOCK_LOCK(&clms.clms_failover_lock);
		if (info != NULL) {
			kfree(*info);
			*info = sv_clmsinfo;
			sv_clmsinfo = NULL;
		}
		*num_seconds = num_2nds;
		if (*second_list == NULL || !im_a_2ndary)
			*im_a_secondary = im_a_2ndary;
		new_entry = *second_list;
		*second_list = new_list;
		new_list = new_entry;
		UNLOCK_LOCK(&clms.clms_failover_lock);
	}
	kfree(sv_clmsinfo);
	while (new_list != NULL) {
		new_entry = new_list->next;
		kfree(new_list);
		new_list = new_entry;
	}

	return ret;
}

/*
 * clms_find_master()
 *      Find the CLMS master node.
 *
 * Description:
 *	This routine gets the list of CLMS secondaries as specified in the
 *	bootfile, and allocates and fills the passed array with that list.
 *	It attempts to probe each node in the list; any node that responds
 *	will indicate the CLMS master, and this routine will return that
 *	node (the master node, not necessarily the node probed), after adding
 *	it to the list of secondaries, if necessary.
 *
 *	Start by probing each node in the list.  If we get a reply and
 *	we're not a CLMS secondary, continue to probe only that node
 *	until we find a CLMS master or until the node stops responding
 *	(at which point restart the algorithm).
 *
 *	Otherwise, use the following rules:
 *
 *	If we get a probe and we don't yet know the CLMS master:
 *		If he thinks he's a secondary but we don't think so,
 *		 complain and send back an error to the probing node.
 *
 *		If we don't think we're a secondary, complain and send back
 *		an error to the probing node.
 *
 *		If we're a higher number and the other guy is on the list,
 *		we start _just_ probing him until he tells us the master
 *		or he stops responding (in which case restart the algorithm).
 *
 *		If we're a lower number and the other guy is on the list,
 *		we take him off the list.  If the list becomes empty (except
 *		for us), become the CLMS master.
 *
 *	If we probe a node and he doesn't know the CLMS master:
 *		If we're a higher number and we don't have higher precedence
 *		(our node doesn't precede his in the secondary list),
 *		probe that node only, until he responds with a CLMS master
 *		(which may not be that node), or until we get no response
 *		from the node, at which point we start the probe algorithm
 *		over.
 *
 *		If we have higher precedence (our node precedes his in the
 *		secondary list) stop probing that node.  If we're the
 *		only secondary left in our list, become the CLMS master.
 *		If there are other secondaries,	continue to probe them until
 *		we time out, find a CLMS master, or get a probe from one of
 *		them (and follow one of the other rules here).
 *
 *	If we get no replies, run the loop of secondaries once per our
 *	position in the list, then become the CLMS secondary.
 *
 * Parameters:
 *      num_seconds	Pointer to an int to receive the number of CLMS
 *			secondaries in the list.
 *	second_list	Pointer to the head of the list of CLMS secondaries.
 */
clusternode_t
clms_find_master(
	int *im_a_secondary,
	int *num_seconds,
	clms_secondary_list_t **second_list)
{
	clusternode_t	master_node, tmp_node;
	icsinfo_t master_addr, tmp_addr;
	clms_secondary_list_t *last_entry = NULL, *new_entry, *ent;
	int	ix, found;
	int	error, retrydelay, got_answer, retry_iter;
	clusternode_t	force_node = CLUSTERNODE_INVAL;
	icsinfo_t	force_addr;
	int	force_ok;

	/*
	 * If second_list is NULL we need to build the secondary list.
	 * Otherwise, we've been here before, and we're being called
	 * again to reprobe for the master.
	 */
	if (*second_list == NULL) {
		retrydelay = 1;
		error = clms_parse_nodelist(cluster_clmsinfo, second_list,
					    num_seconds, im_a_secondary,
					    NULL);
		if (error < 0) {
			if (error == -EINVAL)
				panic("%s:Invalid CLMS master list: %s\n",
				      __FUNCTION__, cluster_clmsinfo);
			else
				panic("%s:Error %d pasting CLMS master list:"
				      " %s\n",
				      __FUNCTION__, -error, cluster_clmsinfo);
		}
	}
	/*
	 * If there's only one secondary and we're it, just return with
	 * ourselves as the master; we're done.
	 */
	if (*num_seconds == 1 && *im_a_secondary) {
		return(this_node);
	}
	/*
	 * Probe each node in the list (skipping ourselves since we know
	 * we're not [yet] the master).
	 */
	printk(KERN_NOTICE "Searching for an existing root node...\n");
restart_algorithm: ;
	LOCK_LOCK(&clms.clms_failover_lock);
	clms_ready_for_probe = 0;
	clms_working_secondaries = *num_seconds;
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next) {
		ent->do_probe = 1;
		ent->probe_only = 0;
	}
	clms_doing_probeonly = 0;
	clms_ready_for_probe = 1;
	got_answer = 0;
	retry_iter = *im_a_secondary + 3;
	UNLOCK_LOCK(&clms.clms_failover_lock);
restart_loop: ;
	found = 0;
	for (ent = *second_list; ent != NULL; ent = ent->next) {
		if (ent->node == this_node ||
		    ent->do_probe == 0 ||
		    (clms_doing_probeonly != 0 && ent->probe_only == 0))
			continue;
#ifdef DEBUG
		printk(KERN_DEBUG
		       "Probing node %u addr %s\n",
		       ent->node,
		       (char *)(&ent->addr));
#endif
		memset((char *)&master_addr, 0, sizeof(icsinfo_t));
		error = ics_probe_clms(ent->node,
				       ent->addr,
				       &master_node,
				       &master_addr,
				       *im_a_secondary,
				       0);
		if (error == 0) {
			found = 1;
			break;
		}
		if (error != CLMS_PROBE_ERR_NOANSWER)
			got_answer = 1;
#ifdef DEBUG
		printk(KERN_DEBUG
		       "clms_find_master: Probe of %u/%s failed, error %d\n",
		       ent->node, (char *)&ent->addr, error);
#endif
		/*
		 * He didn't know, but he's a lower-numbered node, and we
		 * don't have precedence over him.  Just probe
		 * him from now on.
		 */
		if ((error == CLMS_PROBE_ERR_DONTKNOW &&
		     (ent->node < this_node) &&
		     !clms_is_higher_secondary(this_node, ent->node)) ||
		    (error == CLMS_PROBE_ERR_DONTKNOW &&
		     *im_a_secondary == 0)) {
			LOCK_LOCK(&clms.clms_failover_lock);
			if (clms_doing_probeonly == 0) {
				CLMSLOG(CLMSLOG_ONLYPROBE, ent->node, 0, 0, 0, 0, 0, 0);
				ent->probe_only = 1;
				clms_doing_probeonly = 1;
			}
			UNLOCK_LOCK(&clms.clms_failover_lock);
		}
		if (error == CLMS_PROBE_ERR_DONTKNOW &&
		    clms_is_higher_secondary(this_node, ent->node)) {
			LOCK_LOCK(&clms.clms_failover_lock);
			ent->do_probe = 0;
			clms_working_secondaries--;
			UNLOCK_LOCK(&clms.clms_failover_lock);
		}
		if (error == CLMS_PROBE_ERR_IMNOTSEC) {
			printk(KERN_WARNING "clms_find_master: Probed node %u claims not to be a root secondary!", ent->node);
			printk(KERN_WARNING "Please check your configuration!");
			LOCK_LOCK(&clms.clms_failover_lock);
			ent->do_probe = 0;
			clms_working_secondaries--;
			UNLOCK_LOCK(&clms.clms_failover_lock);
		}
		if (error == CLMS_PROBE_ERR_FORCE_ROOT_YOU) {
			/* He wants us to be the root. */
			LOCK_LOCK(&clms.clms_failover_lock);
			ent->do_probe = 0;
			clms_working_secondaries--;
			force_ok = (clms_working_secondaries == 1
				    && *im_a_secondary);
			UNLOCK_LOCK(&clms.clms_failover_lock);
			if (force_ok) {
				printk(KERN_NOTICE
				       "Probed node %u forced us"
				       " to become root", ent->node);
				force_node = ent->node;
				force_addr = ent->addr;
			}
			else {
				printk(KERN_WARNING
				       "clms_find_master: Probed node %u wants to force us"
				       "to become root,\n"
				       "but our configuration is invalid\n",
				       ent->node);
			}

		}
		if (error == CLMS_PROBE_ERR_FORCE_ROOT_NOTYOU) {
			/* He someone else to be the root. */
			LOCK_LOCK(&clms.clms_failover_lock);
			ent->do_probe = 0;
			clms_working_secondaries--;
			UNLOCK_LOCK(&clms.clms_failover_lock);
			if (*im_a_secondary) {
				printk(KERN_WARNING
				      "clms_find_master: Probed node %u wants to force another"
				      "node to become root\n",
				      ent->node);
				machine_restart(NULL);
			}
		}
		if (error == CLMS_PROBE_ERR_HESNOTSEC) {
			printk(KERN_WARNING
			       "clms_find_master: Probed node %u tells me I'm not a root secondary, but I think I am!",
			       ent->node);
			printk(KERN_WARNING "Please check your configuration!");
			*im_a_secondary = 0;
			LOCK_LOCK(&clms.clms_failover_lock);
			clms_working_secondaries--;
			UNLOCK_LOCK(&clms.clms_failover_lock);
		}
		if (error == CLMS_PROBE_ERR_NOANSWER) {
			LOCK_LOCK(&clms.clms_failover_lock);
			if (clms_doing_probeonly != 0) {
				UNLOCK_LOCK(&clms.clms_failover_lock);
				goto restart_algorithm;
			}
			UNLOCK_LOCK(&clms.clms_failover_lock);
		}
	}

	if (found == 1) {
		/*
		 * We may have been the master before, and we may have
		 * rebooted before failover processing had time to act.
		 * If so, just delay and retry.
		 */
		if (master_node == this_node) {
#if defined(DEBUG) || defined(DEBUG_TOOLS)
			printk(KERN_DEBUG
			       "clms_find_master: Probed root node is me!  Retrying in 15 seconds.\n");
#endif
			idelay(15*HZ);
			goto restart_algorithm;
		}
		/* See if returned node is in the list; if not, add it. */
		found = 0;
		new_entry = *second_list;
		for (ix = 0; ix < *num_seconds; ix++) {
			register int cmp_val, bcmp_val;

			cmp_val = (new_entry->node == master_node);
			bcmp_val = memcmp((char *)&(new_entry->addr),
					  (char *)&master_addr,
					  sizeof(icsinfo_t));
			if (cmp_val && (bcmp_val == 0)) {
				found = 1;
				break;
			}
			if ((cmp_val && (bcmp_val != 0)) ||
			    (!cmp_val && (bcmp_val == 0))) {
				panic("Node/address conflict for root node, %u/%s versus %u/%s",
				      new_entry->node,
				      (char *)&new_entry->addr,
				      master_node,
				      (char *)&master_addr);
			}
			last_entry = new_entry;
			new_entry = new_entry->next;
		}
		if (found == 0) {
			new_entry = kmalloc_nofail(
				sizeof(clms_secondary_list_t));
			SSI_ASSERT(last_entry->next == NULL);
			printk(KERN_WARNING
			       "Given root node %u/%s not in list!",
			       master_node,
			       (char *)&master_addr);
			last_entry->next = new_entry;
			new_entry->node = master_node;
			memcpy((char *)&(new_entry->addr),
			       (char *)&master_addr,
			       sizeof(icsinfo_t));
			new_entry->next = NULL;
			(*num_seconds)++;
		}
		/*
		 * Try to probe the master we were told about; that information
		 * might have been obsolete if that master had just failed
		 * and we raced with CLMS failover.  If the probe fails or if
		 * it succeeds and the return node and address doesn't match
		 * what we were told, retry the probe loop above.  If it
		 * succeeds and the returned node and address do match what
		 * we were told, use it.
		 */
		memset((char *)&tmp_addr, 0, sizeof(icsinfo_t));
		error = ics_probe_clms(master_node,
				       master_addr,
				       &tmp_node,
				       &tmp_addr,
				       *im_a_secondary,
				       0);
		if (error != 0 ||
		    (master_node != tmp_node ||
		     memcmp(&master_addr, &tmp_addr, sizeof(icsinfo_t)) != 0)) {
			if (error == 0) {
				printk(KERN_WARNING
				       "Master node %u/%s doesn't match his (%u/%s)",
				       master_node,
				       (char *)&master_addr,
				       tmp_node,
				       (char *)&tmp_addr);
			}
			else {
				printk(KERN_WARNING
				       "Can't contact given root node %u/%s, error %d\n",
				       master_node, (char *)&master_addr, error);
			}
			found = 0;
			idelay(10*HZ);
			goto restart_algorithm;
		}
		error = ics_seticsinfo(master_node, &master_addr);
		if (error != 0)
			panic("clms_find_master: ics_seticsinfo: error %d", error);
		printk(KERN_NOTICE "Found node %u as the root node.\n", master_node);
		return(master_node);
	}

	LOCK_LOCK(&clms.clms_failover_lock);
	/*
	 * We didn't find a master.  If we're not a secondary and we're not
	 * probing only one node, complain, then rerun the list after a delay.
	 */
	if (*im_a_secondary == 0 && clms_doing_probeonly == 0) {
#ifdef DEBUG
		printk(KERN_DEBUG "Failed to find a root node; retrying in 10 seconds...");
#endif
		UNLOCK_LOCK(&clms.clms_failover_lock);
		idelay(10*HZ);
		goto restart_loop;
	}
	/*
	 * If we're probing only one node, retry in a second.
	 */
	if (clms_doing_probeonly) {
		CLMSLOG(CLMSLOG_PRONLYRETRY, 0, 0, 0, 0, 0, 0, 0);
		UNLOCK_LOCK(&clms.clms_failover_lock);
		idelay(HZ);
		goto restart_loop;
	}
	/*
	 * We're a secondary and we haven't gotten any answers.  If we're
	 * the only one left, select ourselves.
	 */
	if (clms_working_secondaries == 1) {
		printk(KERN_NOTICE "Selected myself as the root node.\n");
		UNLOCK_LOCK(&clms.clms_failover_lock);
		/* Were we forced? */
		if (force_node != CLUSTERNODE_INVAL) {
			/* Yes: shoot the node that forced us. */
			ics_seticsinfo(force_node, &force_addr);
			(void) ics_shoot_node(force_node);
			ics_nodedown(force_node);
		}
		return(this_node);
	}
	/*
	 * There are other secondaries lurking out there which we haven't
	 * heard back from to eliminate or solely include.  Keep retrying
	 * until the number of retries reaches our position on the list.
	 * This is a nod to the fact that we may race, and makes it more
	 * likely that nodes booted at the same time will find each other
	 * without racing.  If nodes are booted in the worst possible order,
	 * they still won't race, since the ones booted later will discover
	 * the ones booted earlier via error returns on the probe messages.
	 */
	if (retry_iter-- > 0) {
		register int secs;

		UNLOCK_LOCK(&clms.clms_failover_lock);
		/*
		 * If we're using IP transport, we have to wait a little
		 * longer, since the timing issues are different.
		 */
		if (ics_get_transport() == ICS_TRANSPORT_IP)
			secs = 5;
		else
			secs = 1;
#ifdef DEBUG
		printk(KERN_DEBUG "Delaying for %d second(s) for other secondaries to catch up.\n",
			     secs);
#endif
		idelay(secs * HZ);
		goto restart_loop;
	}

	/*
	 * We failed to find a CLMS master, or even any CLMS secondaries that
	 * are themselves probing, and we are a potential secondary.  Select
	 * ourselves as the CLMS master.
	 */
	UNLOCK_LOCK(&clms.clms_failover_lock);

	printk(KERN_NOTICE "Selected myself as the root node.\n");
	return(this_node);
}

/*
 * clms_force_root_node()
 *	Force the cluster to boot with another root node.
 *
 * Description:
 * 	Shoot desired secondary; wait for it to rejoin; shoot ourselves.
 *
 * Parameters:
 *	node		May be CLUSTERNODE_INVAL; if it is and one secondary
 *			is present it is used; otherwise it must match
 *			a valid secondary.
 *
 * Returns:
 *	-EINVAL is the node specified is invalid; -EREMOTE if we aren't the
 *	master; -ENOENT if there is no secondary; otherwise, it doesn't.
 */

int
clms_force_root_node(clusternode_t node)
{
	int error = 0;
	int started = 0;
	u_long start_ticks = TICKS();
	clms_secondary_list_t *nent = NULL;
	clms_secondary_list_t *ent;
	int i;

	/* Are we the master or are we too late? */
	if (!error && (clms_master_node != this_node || !clms_cold_boot))
		/* No. */
		error = -EREMOTE;
	/* Validate the node or choose the secondary. */
	if (!error) {
		LOCK_LOCK(&clms.clms_failover_lock);
		for (ent = clms_secondary_list_head; ent != NULL;
		     ent = ent->next) {
			/* Choose and perish? */
			if (node == CLUSTERNODE_INVAL) {
				/* Yes: is this some other node? */
				if (ent->node != this_node) {
					/* Yes: have we already chosen? */
					if (nent != NULL) {
						/*
						 * Yes: are there too many
						 * choices?
						 */
						if (ent->node != nent->node) {
							/* Yes. */
							error = -EINVAL;
							break;
						}
					}
					/* No: choose this entry. */
					else
						nent = ent;
				}
			}
			/* No: is this the entry for the node? */
			else if (ent->node == node) {
				/* Yes. */
				nent = ent;
				break;
			}
		}
		if (!error) {
			started = 1;
			clms_force_root = nent->node;
			clms_master_node = CLUSTERNODE_INVAL;
			clms_ready_for_probe = 1;
		}
		UNLOCK_LOCK(&clms.clms_failover_lock);
	}
	/* Shoot all the nodes except us. */
	if (!error) {
		for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
			if (i == this_node)
				continue;
			if (clms.clms_node_info[i].current_state
			    & ~(CLMS_NODE_NEVER_UP | CLMS_NODE_DOWN)) {
				(void)ics_shoot_node(i);
				ics_nodedown(i);
			}
		}
	}
	/* Wait for the new master. */
	while (!error) {
		printk(KERN_NOTICE "Waiting for node %u to become root\n",
		       clms_force_root);
		if (TICKS() - start_ticks >= clms_force_root_dly * HZ) {
			/* Yes. */
			printk(KERN_WARNING
			       "Giving up waiting for node %u;"
			       " rebooting\n",
			       clms_force_root);
			break;
		}
		/* Sleep before retrying. */
		idelay(15 * HZ);
	}
	/* Did we start? */
	if (started) {
		/* Yes: reboot. */
		machine_restart(NULL);
	}

	return error;
}

int
rclms_read_masterparams(clusternode_t to_node, int *rerror,
			char **masterinfo, int *masterinfo_len,
			struct nm_settings *nm_settings)
{
	int len;

	*rerror = 0;
	LOCK_LOCK(&clms.clms_parameters_lock);
	if (*masterinfo == NULL) {
		*masterinfo = kmalloc(*masterinfo_len, GFP_KERNEL);
		if (*masterinfo == NULL) {
			*masterinfo_len = 0;
			*rerror = -ENOMEM;
			goto out;
		}
	}
	len = strlen(cluster_clmsinfo) + 1;
	if (len < *masterinfo_len) {
		strcpy(*masterinfo, cluster_clmsinfo);
		*masterinfo_len = len;
	} else
		*rerror = -E2BIG;
	nm_get_settings(nm_settings);
 out:
	UNLOCK_LOCK(&clms.clms_parameters_lock);

	return 0;
}

int
rclms_write_masterlist(clusternode_t to_node, int *rerror,
		       char *masterinfo, int masterinfo_len)
{
	int error;
	int rerr;
	cli_handle_t **hlistp = NULL;
	int len;
	int sent;
	int i;

	*rerror = 0;
	LOCK_LOCK(&clms.clms_parameters_lock);
	*rerror = clms_parse_nodelist(masterinfo, &clms_secondary_list_head,
				      &clms_num_secondaries,
				      &clms_im_a_secondary, &cluster_clmsinfo);
	if (*rerror < 0 || this_node != clms_master_node)
		goto out;
	hlistp = kmalloc_nofail(sizeof(*hlistp) * NSC_MAX_NODE_VALUE);
	len = strlen(cluster_clmsinfo) + 1;
	sent = 0;
	for (to_node = 1; to_node <= NSC_MAX_NODE_VALUE; to_node++) {
		if (to_node == this_node)
			continue;

		if (!clms_isnodeavail(to_node))
			continue;

		hlistp[sent] = NULL;
		error = RCLMS_WRITE_MASTERLIST_SEND(to_node, &hlistp[sent],
						    cluster_clmsinfo,  len);
		if (icscli_async_sent(&hlistp[sent]))
			sent++;
		else {
			SSI_ASSERT(error < 0);
			if (error != -EREMOTE) {
				if (*rerror >= 0)
					*rerror = error;
				printk(KERN_WARNING
				       "%s: Unexpected error %d\n",
				       __FUNCTION__, error);
			}
		}
	}
	for (i = 0; i < sent; i++) {
		error = RCLMS_WRITE_MASTERLIST_RECEIVE(hlistp[i], &rerr,
						       NULL, 0);
		if (!error)
			error = rerr;
		if (error < 0) {
			if (error != -EREMOTE) {
				if (*rerror >= 0)
					*rerror = error;
				printk(KERN_WARNING
				       "%s: Unexpected error %d\n",
				       __FUNCTION__, error);
			}
		}
	}
 out:
	UNLOCK_LOCK(&clms.clms_parameters_lock);
	kfree(hlistp);

	return 0;
}

static ssize_t
read_clms_masterlist(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int newline = 0;
	int len;

	if (*ppos < 0)
		return -EINVAL;
	LOCK_LOCK(&clms.clms_parameters_lock);
	len = strlen(cluster_clmsinfo) + 1;
	if (*ppos >= len)
		count = 0;
	else if (*ppos + count >= len) {
		newline = 1;
		count = len - *ppos;
	}
	if (count - newline > 0 &&
	    copy_to_user(buf, cluster_clmsinfo + *ppos, count - newline))
		count = -EFAULT;
	if (count > 0 && newline && copy_to_user(buf + len - 1, "\n", 1))
		count = -EFAULT;
	if (count > 0)
		*ppos += count;
	UNLOCK_LOCK(&clms.clms_parameters_lock);

	return count;
}

static ssize_t
write_clms_masterlist(struct file * file, const char * buf, size_t count,
		      loff_t *ppos)
{
	int error;
	int rerror;
	clusternode_t to_node;
	cli_handle_t *chp;
	char *masterinfo;
	char *cp;
	int len;

	masterinfo = kmalloc(count + 1, GFP_KERNEL);
	if (masterinfo == NULL) {
		error = -ENOMEM;
		goto out;
	}
	if (copy_from_user(masterinfo, buf, count)) {
		error = -EFAULT;
		goto out;
	}
	masterinfo[count] = '\0';
	cp = strchr(masterinfo, '\n');
	if (cp != NULL)
		*cp = '\0';
	len = strlen(masterinfo);
	if (len == 0) {
		error = -EINVAL;
		goto out;
	}

	for (;;) {
		to_node = clms_master_node;
		if (to_node == this_node)
			error = rclms_write_masterlist(to_node, &rerror,
						       masterinfo, len);
		else if (to_node != CLUSTERNODE_INVAL) {
			chp = NULL;
			error = RCLMS_WRITE_MASTERLIST_SEND(to_node, &chp,
							    masterinfo, len);
			if (icscli_async_sent(&chp))
				error = RCLMS_WRITE_MASTERLIST_RECEIVE(
						chp, &rerror, NULL, 0);
		} else
			error = -EREMOTE;
		if (error == -EREMOTE) {
			nidelay(HZ);
			continue;
		}
		if (error >= 0)
		       error = rerror;
		break;
	}
 out:
	if (error >= 0)
		*ppos += count;
	else
		count = error;
	kfree(masterinfo);

	return count;
}

struct file_operations proc_clms_masterlist_operations = {
	read:		read_clms_masterlist,
	write:		write_clms_masterlist,
};


static int icsstat_show(struct seq_file *m, void *p)
{
	int i,j,k;
	seq_printf(m, "Global statistics.\n");
	seq_printf(m, "Channel Number\tService Name\t\tOperation Name\t\t\n");
	for (i=0; i < ICS_MAX_CHANNELS; i++) {
		for (j=0; j < ICS_MAX_SUBSERVICES; j++) {
			for (k=0; k < icsstat.istat_proc[i][j]; k++) 
					seq_printf(m, "%d\t%s\t%s\n",
						i,
						icsstat.istat_svc_name[i][j],
						icsstat.istat_svc_proc_names[i][j][k]);
		}
	}
	seq_printf(m, "\n\nClient statistics.\n");
	seq_printf(m, "Number of existing handles: %ld\n", icsstat.istat_clihand);
	seq_printf(m, "Number of RPC sent: %ld\n", icsstat.istat_clirpc); 
	seq_printf(m, "Number of messages sent: %ld\n", icsstat.istat_climsg);
	seq_printf(m, "Number of RPC messages sent per channel at different ICS priority\n");
	seq_printf(m, "Priority\tChannel\tNo of RPC messages sent\n");
	for (i=0; i < ICS_NUM_PRIO; i++) {
		for (j=0; j < ICS_MAX_CHANNELS; j++) {
				seq_printf(m, "%d\t%d\t%ld\n", i, j, icsstat.istat_clisend[i][j]);
		}
	}
	seq_printf(m, "Number of RPC messages sent per channel Per Operation\n");
	seq_printf(m, "Channel\tService\tOperation\tNo of RPC messages sent\n");
	for (i=0; i < ICS_MAX_CHANNELS; i++) {
		for (j=0; j < ICS_MAX_SUBSERVICES; j++) {
			for (k=0; k <  icsstat.istat_proc[i][j]; k++) 
				seq_printf(m, "%d\t%s\t%s\t%ld\n", i,
						icsstat.istat_svc_name[i][j],
						icsstat.istat_svc_proc_names[i][j][k],
						icsstat.istat_clisend_proc[i][j][k]);
		}
	}
	seq_printf(m, "No of signals forwarded: %ld\n", icsstat.istat_clisigforward);
	seq_printf(m, "No of signals forwarding retries: %ld\n",
			icsstat.istat_clisigforward_retry);
#if 0 
	/* SSI_XXX We don't support throttling as of now */
	long	istat_clithrottled;	/* # high-level ICS clients throttled */
	long	istat_clithrottled_event; /* # times high-level ICS throttled */
	long	istat_llclithrottled[ICS_MAX_CHANNELS]; /* # times low-level */
					/* ICS clients throttled */
#endif 

	seq_printf(m, "\n\nServer statistics\n");
	seq_printf(m, "No of handles created: %ld\n", icsstat.istat_svrhand_create);
	seq_printf(m, "No of handles existing: %ld\n", icsstat.istat_svrhand);
	seq_printf(m, "No of RPCs received: %ld\n", icsstat.istat_svrrpc);
	seq_printf(m, "No of messages received: %ld\n", icsstat.istat_svrmsg);

	seq_printf(m, "Number of RPC messages received  per channel at different ICS priority\n");
	seq_printf(m, "Priority\tChannel\tNo of RPC messages sent\n");
	for (i=0; i < ICS_NUM_PRIO; i++) {
		for (j=0; j < ICS_MAX_CHANNELS; j++) {
				seq_printf(m, "%d\t%d\t%ld\n", i, j, icsstat.istat_svrrecv[i][j]);
		}
	}
	seq_printf(m, "Number of RPC messages Received per channel Per Operation\n");
	seq_printf(m, "Channel\tService\tOperation\tNo of RPC messages sent\n");
	for (i=0; i < ICS_MAX_CHANNELS; i++) {
		for (j=0; j < ICS_MAX_SUBSERVICES; j++) {
			for (k=0; k <  icsstat.istat_proc[i][j]; k++) 
				seq_printf(m, "%d\t%s\t%s\t%ld\n", i,
						icsstat.istat_svc_name[i][j],
						icsstat.istat_svc_proc_names[i][j][k],
						icsstat.istat_svrrecv_proc[i][j][k]);
		}
	}
	seq_printf(m, "No of signals forwarded: %ld\n", icsstat.istat_svrsigforward);
	seq_printf(m, "No of signal forwarding retries: %ld\n",
			icsstat.istat_svrsigforward_retry);
	seq_printf(m, "No of times handle alloc deferred : %ld\n", icsstat.istat_svrhand_nomem);
	seq_printf(m, "No of times handle alloc deferred and handle needed : %ld\n",
		icsstat.istat_svrhand_nomem_norecv);
#if 0
	long	istat_svrhand_recv[ICS_NUM_PRIO][ICS_MAX_CHANNELS];
					/* # receives handled */
					/* (by service/proc) */
	long	istat_svrhand_norecv[ICS_NUM_PRIO][ICS_MAX_CHANNELS];
					/* # messages arrived with no handles */
#endif
	seq_printf(m, "No of daemons created: %ld\n", icsstat.istat_svrdaemon_create);
	seq_printf(m, "No of daemons existing: %ld\n", icsstat.istat_svrdaemon);
	seq_printf(m, "No of daemons processing messages: %ld\n", icsstat.istat_svrdaemon_active);
#if 0
	long	istat_svrdaemon_sleep[ICS_NUM_PRIO]; /* # times daemon slept */
					/* before receiving message */
	long	istat_svrdaemon_nosleep[ICS_NUM_PRIO]; /* # times daemon did */
					/* not sleep before receiving message */
#endif
	seq_printf(m, "No times nanny daemon woken up: %ld\n", icsstat.istat_svrnanny_run);
#if 0
	long	istat_svrthrottled[ICS_MAX_CHANNELS]; /* # times high-level */
#endif
	return 0;
}


		
static int icsstat_open(struct inode *inode, struct file *file)
{
	return single_open(file,  icsstat_show, NULL);
};


struct file_operations proc_icsstat_operations = {
	.owner	= 	THIS_MODULE,
	.open	= 	icsstat_open,
	.read	=	seq_read,
	.llseek	=	seq_lseek,
	.release	= single_release,

};

int
rclms_write_nm_settings(clusternode_t to_node, int *rerror,
			struct nm_settings *nm_settings)
{
	int error;
	int rerr;
	cli_handle_t **hlistp = NULL;
	int sent;
	int i;
	struct nm_settings nm_loc;

	*rerror = 0;

	/* Re-enable on master node early to allow deadlocks to be broken. */
	if (this_node == clms_master_node &&
	    nm_settings->nm_nodedown_disabled_valid &&
	    !nm_settings->nm_nodedown_disabled) {
		nm_loc.nm_log_threshold_valid = 0;
		nm_loc.nm_rate_valid = 0;
		nm_loc.nm_nodedown_disabled_valid = 1;
		nm_loc.nm_nodedown_disabled = 0;
		nm_update_settings(&nm_loc, 0);
	}
	LOCK_LOCK(&clms.clms_parameters_lock);
	if (this_node != clms_master_node) {
		nm_update_settings(nm_settings, 0);
		goto out;
	} else if (nm_settings->nm_rate_valid) {
		nm_get_settings(&nm_loc);
		if (nm_loc.nm_timeout < nm_settings->nm_timeout)
			nm_loc.nm_timeout = nm_settings->nm_timeout;
		if (nm_loc.nm_max_probes < nm_settings->nm_max_probes)
			nm_loc.nm_max_probes = nm_settings->nm_max_probes;
		nm_update_settings(&nm_loc, 0);
	}
	hlistp = kmalloc_nofail(sizeof(*hlistp) * NSC_MAX_NODE_VALUE);
	sent = 0;
	for (to_node = 1; to_node <= NSC_MAX_NODE_VALUE; to_node++) {
		if (to_node == this_node)
			continue;

		if (!clms_isnodeavail(to_node))
			continue;

		hlistp[sent] = NULL;
		error = RCLMS_WRITE_NM_SETTINGS_SEND(to_node, &hlistp[sent],
						     nm_settings);
		if (icscli_async_sent(&hlistp[sent]))
			sent++;
		else {
			SSI_ASSERT(error < 0);
			if (error != -EREMOTE) {
				if (*rerror >= 0)
					*rerror = error;
				printk(KERN_WARNING
				       "%s: Unexpected error %d\n",
				       __FUNCTION__, error);
			}
		}
	}
	for (i = 0; i < sent; i++) {
		error = RCLMS_WRITE_NM_SETTINGS_RECEIVE(hlistp[i], &rerr,
							NULL);
		if (!error)
			error = rerr;
		if (error < 0) {
			if (error != -EREMOTE) {
				if (*rerror >= 0)
					*rerror = error;
				printk(KERN_WARNING
				       "%s: Unexpected error %d\n",
				       __FUNCTION__, error);
			}
		}
	}
	if (this_node == clms_master_node)
		nm_update_settings(nm_settings, 0);
 out:
	UNLOCK_LOCK(&clms.clms_parameters_lock);
	kfree(hlistp);

	return 0;
}

int
clms_write_nm_settings(struct nm_settings *nsp)
{
	int error;
	int rerror;
	clusternode_t to_node;
	cli_handle_t *chp;

	for (;;) {
		to_node = clms_master_node;
		if (to_node == this_node)
			error = rclms_write_nm_settings(to_node, &rerror,
							nsp);
		else if (to_node != CLUSTERNODE_INVAL) {
			chp = NULL;
			error = RCLMS_WRITE_NM_SETTINGS_SEND(to_node, &chp,
							     nsp);
			if (icscli_async_sent(&chp))
				error = RCLMS_WRITE_NM_SETTINGS_RECEIVE(chp,
							&rerror, nsp);
		} else
			error = -EREMOTE;
		if (error == -EREMOTE) {
			nidelay(HZ);
			continue;
		}
		if (error >= 0)
			error = rerror;
		break;
	}

	return error;
}

/*
 * Register CLMS components with CI.
 */
ics_chan_t ics_clms_chan;
EXPORT_SYMBOL(ics_clms_chan);
ics_chan_t ics_probe_clms_chan;
extern cluster_svc_t cluster_clms_svc;

int ics_clms_svc_init(void);

static int __init ci_init_clms(void)
{
	int ret;

	/* Register CLMS ICS channels */
	/* XXX: The ICS channel numbers are hard-coded to ensure consistency
	 * among cluster nodes. This should go away once dynamic channels
	 * are implemented. In the meantime, be sure to update this when
	 * channels are added/deleted. */
	ret = register_ics_channel(&ics_probe_clms_chan, 6);
	if (ret < 0)
		return ret;
	ret = register_ics_channel(&ics_clms_chan, 7);
	if (ret < 0)
		return ret;

	/* Register CLMS cluster service */
	return register_cluster_svc(&cluster_clms_svc,
				    ics_clms_chan,
				    ics_clms_svc_init);
}

/* XXX: There is currently no option to compile this as a module. */
module_init(ci_init_clms)
