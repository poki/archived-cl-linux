/*
 *	CLMS node-transition API.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 *	This file contains code that supports the CLMS node-transition API.
 */

#include <linux/delay.h>
#include <linux/reboot.h>
#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/synch.h>
#include <cluster/nodelist.h>
#include <cluster/icsgen.h>

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>

/*
 * clms_api_user_block()
 *	Synchronization between node status transitions and API node status
 *	calls.
 *
 * Description:
 *	This is used by the node/cluster status API to synchronize between
 *	that and CLMS node status transitions.  If we're in the middle of a
 *	transition, we block until it's complete, and we then block further
 *	such transitions.  The calling routines must call
 *	clms_api_user_unblock() when it is finished to allow those transitions
 *	to proceed.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
#ifdef DEBUG_TOOLS
void
__clms_api_user_block(const char *file, int line)
#else
void
clms_api_user_block(void)
#endif
{
	LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_LOCK, 1, file, line, current->pid, 0, 0, 0);
}

/*
 * clms_api_user_unblock()
 *	Synchronization between node status transitions and API node status
 *	calls.
 *
 * Description:
 *	This is used by the node/cluster status API (after calling
 *	clms_api_user_block() and completing processing) to allow
 *	CLMS node status transitions to complete.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
#ifdef DEBUG_TOOLS
void
__clms_api_user_unblock(const char *file, int line)
#else
void
clms_api_user_unblock(void)
#endif
{
	UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_UNLOCK, 1, file, line, current->pid, 0, 0, 0);
}

/*
 * clms_api_validate_new_state()
 *	Determine if the state transition is valid.
 *
 * Description:
 *	Check the new state against the current state of the node,
 *	returning zero if it's a valid transition and -EINVAL if not.
 *	If it's a valid transition, fill the "why" parameter with
 *	the appropriate "why" value for the transition.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	node	The node that's changing state.
 *	state	The new state.
 *	why	A pointer to an int to receive the "why" value.
 *
 * Returns:
 *	-EINVAL if the state change was illegal.  Zero otherwise.
 */
int
clms_api_validate_new_state(
	clusternode_t node,
	int state,
	int *why)
{
	int oldstate;

	SSI_ASSERT(why != NULL);
	oldstate = clms_api_get_state(node);
	switch(state) {
		case CLMS_STATE_UP:
			if (oldstate != CLMS_STATE_COMINGUP &&
			    oldstate != CLMS_STATE_SHUTDOWN) {
				CLMSLOG(CLMSLOG_NUFAIL,
					node,
					clms.clms_node_info[node].flags,
					clms.clms_node_info[node].api_state.up_transitions,
					clms.clms_node_info[node].api_state.down_transitions,
					clms.clms_node_info[node].current_state,
					clms.clms_node_info[node].prev_state,
					0);
				return(-EINVAL);
			}
			*why = CLMS_TRANS_WHY_API;
			break;
		case CLMS_STATE_DOWN:
			if (oldstate != CLMS_STATE_UCLEANUP)
				return(-EINVAL);
			*why = CLMS_TRANS_WHY_API;
			break;
		case CLMS_STATE_GOINGDOWN:
			if (oldstate != CLMS_STATE_UP &&
			    oldstate != CLMS_STATE_SHUTDOWN)
				return(-EINVAL);
			*why = CLMS_TRANS_WHY_API;
			break;
		case CLMS_STATE_SHUTDOWN:
			if (oldstate != CLMS_STATE_UP)
				return(-EINVAL);
			*why = CLMS_TRANS_WHY_API;
			break;
		case CLMS_STATE_UCLEANUP:
			if (oldstate != CLMS_STATE_KCLEANUP)
				return(-EINVAL);
			*why = CLMS_TRANS_WHY_SYSTEM;
			break;
		default:
			/*
			 * These states aren't performed by this interface:
			 * CLMS_STATE_NEVERUP, CLMS_STATE_COMINGUP,
			 * CLMS_STATE_KCLEANUP.  Return -EINVAL for them,
			 * or for any illegal state.
			 */
			return(-EINVAL);
	}
	return(0);
}

/*
 * clms_api_newstate()
 *	Tell the CLMS master that we're changing state.
 *
 * Description:
 *	This tells the CLMS master that the current node is changing state
 *	via the API.  Called internally by clms_declare_node_new_state()
 *	and elsewhere in CLMS.
 *
 *	NSC_XXX - Revisit this when we do eviction.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	node	The node that's changing state.
 *	state	The new state.
 *	why	The reason for the state change.
 *
 * Returns:
 *	-EINVAL if the state change was illegal.  Zero otherwise.
 */
int
clms_api_newstate(
	clusternode_t node,
	int state,
	int why)
{
	int rval, error;

	SSI_ASSERT(node >= 1 && node <= NSC_MAX_NODE_VALUE);
	do {
		if (this_node == clms_master_node) {
			error = rclms_api_newstate(clms_master_node,
						   &rval,
						   this_node,
						   node,
						   state,
						   why);
			SSI_ASSERT(error == 0);
			return(rval);
		}
		LOCK_COND_LOCK(&clms.clms_ctv_lock);
		clms.clms_api_trans_intent.client_node = CLUSTERNODE_INVAL;
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		error = RCLMS_API_NEWSTATE(clms_master_node,
					   &rval,
					   this_node,
					   node,
					   state,
					   why);
		/*
		 * If we got an -EREMOTE, the CLMS master must have
		 * failed.  If we got the intent, just wait for the
		 * transition to complete (by getting the API exclusive
		 * lock [which works because the intent locked the API
		 * shared lock], then releasing it) then exit.  The
		 * failover code will roll the transition forward.  If
		 * we didn't receive the intent then _nobody_ received
		 * the intent, so retry the transition.  This works
		 * because the CLMS master will send us the intent before
		 * it sends it to any other node.  When we get an -EREMOTE,
		 * we've either received the intent or not.  If we have,
		 * we wait for the transition to complete; if not, we
		 * retry it.
		 */
		if (error == -EREMOTE) {
			LOCK_COND_LOCK(&clms.clms_ctv_lock);
			if (clms.clms_api_trans_intent.client_node == node) {
				UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
				LOCK_EXCL_RW_LOCK(&clms.clms_api_rw_lock);
				CLMSLOG(CLMSLOG_API_LOCK, 1,
					__FILE__, __LINE__, current->pid,
					0, 0, 0);
				UNLOCK_EXCL_RW_LOCK(&clms.clms_api_rw_lock);
				CLMSLOG(CLMSLOG_API_UNLOCK, 1,
					__FILE__, __LINE__, current->pid,
					0, 0, 0);
				return(0);
			}
			UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
			idelay(HZ/10);
		}
	} while (error == -EREMOTE);
	if (error != 0)
		panic("clms_api_newstate:  RCLMS_API_NEWSTATE failed, error %d\n", error);
	return(rval);
}

/*
 * clms_declare_node_new_state()
 *	Change the state of a node for the API.
 *
 * Description:
 *	This validates the state change and calls clms_api_newstate() to
 *	do it.  The new state cannot be CLMS_STATE_UCLEANUP; that
 *	state can only be reached from within the kernel.  The state
 *	change must be on behalf of the running node unless the new
 *	state is CLMS_STATE_DOWN.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	node	The node that's changing state.
 *	state	The new state.
 *
 * Returns:
 *	-EINVAL if the state change was illegal.  Zero otherwise.
 */
int
clms_declare_node_new_state(
	clusternode_t node,
	int state)
{
	int error, why;

	if (state == CLMS_STATE_UCLEANUP)
		return(-EINVAL);
	/*
	 * If we're forcing the node into KCLEANUP and we're not the node in
	 * question, we need to shoot it and wait for the transition.  If we
	 * _are_ the node in question, just commit suicide.
	 */
	if (state == CLMS_STATE_KCLEANUP) {

		if (node == this_node) {
			clms_going_away();
			machine_restart(NULL);
			/*NOTREACHED*/
		}
		/*
		 * Shoot the node using low-level ICS.
		 */
		(void)ics_shoot_node(node);
		return(0);
	}
	if (node != this_node && state != CLMS_STATE_DOWN)
		return(-EINVAL);
	error = clms_api_validate_new_state(node, state, &why);
	if (error != 0)
		return(error);
	error = clms_api_newstate(node, state, why);
	return(error);
}

/*
 * clms_api_transition()
 *	Process a node transition for the API.
 *
 * Description:
 *	This routine keeps the current API node state for each node,
 *	and adds entries to the transition history.  We lock the
 *	clms_node_lock and call clms_api_transition_l() to do the
 *	actual work.
 *
 * Parameters:
 *	node	The node that made the transition.
 *	state	The new state.
 *	why	Why the transition happened.
 *	ctv	The CLMS Cluster Transition Vector for this state change.
 *
 * Returns:
 *	None.
 */
void
clms_api_transition(
	clusternode_t node,
	int state,
	int why,
	clms_ctv_t ctv)
{
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms_api_transition_l(node, state, why, ctv);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	BROADCAST_CONDITION(&clms.clms_node_cond);
}

/*
 * clms_api_transition_l()
 *	Process a node transition for the API.
 *
 * Description:
 *	This routine keeps the current API node state for each node,
 *	and adds entries to the transition history.  Assumes that the
 *	clms_node_lock has been held by the caller.
 *
 * Parameters:
 *	node	The node that made the transition.
 *	state	The new state.
 *	why	Why the transition happened.
 *	ctv	The CLMS Cluster Transition Vector for this state change.
 *
 * Returns:
 *	None.
 */
void
clms_api_transition_l(
	clusternode_t node,
	int state,
	int why,
	clms_ctv_t ctv)
{
	int prev_state;

	SSI_ASSERT(node >= 1 && node <= NSC_MAX_NODE_VALUE);
	SSI_ASSERT_LOCKED_COND_LOCK(&clms.clms_node_lock);
	if (state == CLMS_STATE_COMINGUP &&
	    clms.clms_node_info[node].api_state.state == CLMS_STATE_NEVERUP) {
		clms.clms_node_info[node].api_state.first_transid = ctv.transid;
		clms.clms_node_info[node].api_state.first_stamp = ctv.stamp;
	}
	prev_state = clms.clms_node_info[node].api_state.prev_state =
				     clms.clms_node_info[node].api_state.state;
	clms.clms_node_info[node].api_state.state = state;
	clms.clms_node_info[node].api_state.why = why;
	clms.clms_node_info[node].api_state.transid = ctv.transid;
	clms.clms_node_info[node].api_state.stamp = ctv.stamp;
	clms_maintain_statistics(node, state, prev_state);
	clms_api_hist(node, state, why, ctv);
}

/*
 * clms_api_get_state()
 *	Get the current API state for the passed node.
 *
 * Description:
 *	This routine gets the current API state for the passed node.
 *
 * Parameters:
 *	node	The node for which to get state.
 *
 * Returns:
 *	The state of the node.
 */
int
clms_api_get_state(
	clusternode_t node)
{
	int state;

	LOCK_COND_LOCK(&clms.clms_node_lock);
	state = clms_api_get_state_l(node);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return(state);
}

/*
 * clms_api_get_state_l()
 *	Get the current API state for the passed node.
 *
 * Description:
 *	This routine gets the current API state for the passed node.
 *	Called with the clms_node_lock held.
 *
 * Parameters:
 *	node	The node for which to get state.
 *
 * Returns:
 *	The state of the node.
 */
int
clms_api_get_state_l(
	clusternode_t node)
{
	SSI_ASSERT(node >= 1 && node <= NSC_MAX_NODE_VALUE);
	SSI_ASSERT_LOCKED_COND_LOCK(&clms.clms_node_lock);
	return(clms.clms_node_info[node].api_state.state);
}

/*
 * clms_api_get_full_state()
 *	Get the full API state for the passed node.
 *
 * Description:
 *	This routine gets the full API state for the passed node.
 *
 * Parameters:
 *	node	The node for which to get state.
 *	state	A pointer to a clms_api_state_t to receive the state.
 *
 * Returns:
 *	None.
 */
void
clms_api_get_full_state(
	clusternode_t node,
	clms_api_state_t *state)
{
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms_api_get_full_state_l(node, state);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
}

/*
 * clms_api_get_full_state_l()
 *	Get the full API state for the passed node.
 *
 * Description:
 *	This routine gets the full API state for the passed node.
 *	Called with the clms_node_lock held.
 *
 * Parameters:
 *	node	The node for which to get state.
 *	state	A pointer to a clms_api_state_t to receive the state.
 *
 * Returns:
 *	None.
 */
void
clms_api_get_full_state_l(
	clusternode_t node,
	clms_api_state_t *state)
{
	SSI_ASSERT(node >= 1 && node <= NSC_MAX_NODE_VALUE);
	SSI_ASSERT_LOCKED_COND_LOCK(&clms.clms_node_lock);
	SSI_ASSERT(state != NULL);
	*state = clms.clms_node_info[node].api_state;
}

/*
 * clms_api_hist()
 *	Append an entry to the transition history.
 *
 * Description:
 *	This routine just appends a transition to the transition history,
 *	wrapping if necessary.
 *
 * Parameters:
 *	node	The node that made the transition.
 *	state	The new state.
 *	why	Why the transition happened.
 *	ctv	The CLMS Cluster Transition Vector for this state change.
 *
 * Returns:
 *	None.
 */
void
clms_api_hist(
	clusternode_t node,
	int state,
	int why,
	clms_ctv_t ctv)
{
	clms_transition_t *tr;

	SSI_ASSERT_LOCKED_COND_LOCK(&clms.clms_node_lock);
	LOCK_SPIN_LOCK(&clms.clms_api_hist_lock);
	tr = clms.clms_api_trans_ptr;
	tr->node = node;
	tr->transition = state;
	tr->oldstate = clms.clms_node_info[node].api_state.prev_state;
	tr->why = why;
	tr->transid = ctv.transid;
	tr->stamp = ctv.stamp;
	if (++tr == (clms.clms_api_trans_hist + CLMS_TRANS_HIST_SZ)) {
		clms.clms_api_trans_wrapped = 1;
		tr = clms.clms_api_trans_hist;
	}
	clms.clms_api_trans_ptr = tr;
	UNLOCK_SPIN_LOCK(&clms.clms_api_hist_lock);
}

/*
 * clms_api_get_hist()
 *	Get entries from the transition history.
 *
 * Description:
 *	This routine fills the passed buffer with entries from the transition
 *	history.  All entries with a transid greater than the passed transid
 *	are returned.
 *
 * Parameters:
 *	hist_buf	A passed buffer to receive the history.  Must be big
 *			enough for the entire history.
 *	hist_size	The number of entries returned.
 *	transid		The transid after which to start.
 *
 * Returns:
 *	None.
 */
void
clms_api_get_hist(
	clms_transition_t	*histbuf,
	int			*hist_size,
	u_long			transid)
{
	clms_transition_t *tr;
	int i, size;

	*hist_size = 0;
	LOCK_SPIN_LOCK(&clms.clms_api_hist_lock);
	if (clms.clms_api_trans_wrapped) {
		size = CLMS_TRANS_HIST_SZ;
		tr = clms.clms_api_trans_ptr;
	}
	else {
		size = clms.clms_api_trans_ptr - clms.clms_api_trans_hist;
		SSI_ASSERT(size <= CLMS_TRANS_HIST_SZ);
		tr = clms.clms_api_trans_hist;
	}
	for (i = 0; i < size; i++) {
		if (tr->transid <= transid) {
			tr++;
			if (tr == clms.clms_api_trans_hist+CLMS_TRANS_HIST_SZ) {
				SSI_ASSERT(clms.clms_api_trans_wrapped != 0);
				tr = clms.clms_api_trans_hist;
			}
			continue;
		}
		histbuf[(*hist_size)++] = *tr;
		tr++;
		if (tr == (clms.clms_api_trans_hist + CLMS_TRANS_HIST_SZ)) {
			SSI_ASSERT(clms.clms_api_trans_wrapped != 0);
			tr = clms.clms_api_trans_hist;
		}
	}
	UNLOCK_SPIN_LOCK(&clms.clms_api_hist_lock);
}

/*
 * clms_api_get_hist_ent()
 *	Get an entry from the transition history.
 *
 * Description:
 *	This routine gets a specific entry from the API transition history
 *	based upon the passed "where" value.
 *
 * Parameters:
 *	where	An indication of the entry to return:
 *		CLMS_WHERE_GET_FIRST
 *			Get the first entry in the history of the cluster;
 *			if the history has wrapped, return an error.
 *		CLMS_WHERE_GET_LATEST
 *			Get the latest entry in the API history (this will
 *			always be found).
 *		CLMS_WHERE_GET_EARLIEST
 *			Get the earliest entry still present in the API
 *			history (this, also, will always be found).
 *		CLMS_WHERE_GET_THIS
 *			Get the entry with the passed transid; if no such
 *			entry, return an error.
 *		CLMS_WHERE_GET_PREV
 *			Get the entry before the one with the passed transid.
 *			If the passed transid doesn't exist, return
 *			CLMS_HIST_GET_NOTPRESENT.  If there is no entry
 *			before the one with the passed transid, return
 *			CLMS_HIST_GET_NOPREV.
 *		CLMS_WHERE_GET_NEXT
 *			Get the entry _after_ the one with the passed
 *			transid; if no such entry, return an error.  If the
 *			passed transid is earlier than that of the first
 *			entry in the history, this is equivalent to
 *			CLMS_WHERE_GET_EARLIEST.
 *	transid	The transid for CLMS_WHERE_GET_THIS, CLMS_WHERE_GET_PREV, or
 *		CLMS_WHERE_GET_NEXT; ignored otherwise.
 *	ent	A pointer to a clms_transition_t to receive the
 *		found entry.
 *
 * Returns:
 *	CLMS_HIST_GET_FOUND
 *		Found the specified entry.
 *	CLMS_HIST_GET_NOTPRESENT
 *		The first entry is no longer present, for CLMS_WHERE_GET_FIRST,
 *		or the previous entry is no longer present, for
 *		CLMS_WHERE_GET_PREV.
 *	CLMS_HIST_GET_TOOEARLY
 *		The entry with the passed transid is earlier than the first
 *		entry in the API history.  Returned only for
 *		CLMS_WHERE_GET_THIS and CLMS_WHERE_GET_PREV.
 *	CLMS_HIST_GET_MALFORMED
 *		The entry with the passed transid could not be found, although
 *		it should have been.  This might happen because of a malformed
 *		transid.  Returned only for CLMS_WHERE_GET_THIS.
 *	CLMS_HIST_GET_NONEXT
 *		There are no entries after the one with the passed transid.
 *		We hit the end of the API history.  Returned only for
 *		CLMS_WHERE_GET_NEXT.
 *	CLMS_HIST_GET_INVALID
 *		The entry with the passed transid could not be found, for
 *		CLMS_WHERE_GET_PREV.
 *	CLMS_HIST_GET_NOPREV
 *		There was no previous entry for CLMS_WHERE_GET_PREV.
 *	CLMS_HIST_GET_BADWHERE
 *		The "where" value is invalid.
 */
int
clms_api_get_hist_ent(
	int where,
	u_long transid,
	clms_transition_t *ent)
{
	clms_transition_t *tr, *prev;
	int ret, size, i;

	SSI_ASSERT(clms.clms_api_trans_wrapped != 0 ||
	       (clms.clms_api_trans_ptr - clms.clms_api_trans_hist) > 0);
	SSI_ASSERT(ent != NULL);
	LOCK_SPIN_LOCK(&clms.clms_api_hist_lock);
	tr = NULL;
	ret = CLMS_HIST_GET_FOUND;
	switch(where) {
		case CLMS_WHERE_GET_FIRST:
			/*
			 * Get the first transition in the lifetime of the
			 * cluster.  If the history has wrapped, that entry
			 * is no longer present, so return an error.
			 */
			if (clms.clms_api_trans_wrapped) {
				ret = CLMS_HIST_GET_NOTPRESENT;
				break;
			}
			tr = clms.clms_api_trans_hist;
			break;
		case CLMS_WHERE_GET_LATEST:
			/*
			 * Get the latest entry in the history.
			 */
			tr = clms.clms_api_trans_ptr;
			if (clms.clms_api_trans_wrapped &&
			    tr == clms.clms_api_trans_hist)
				tr += (CLMS_TRANS_HIST_SZ - 1);
			else
				tr--;
			break;
		case CLMS_WHERE_GET_EARLIEST:
			/*
			 * Get the earliest entry still present in the history.
			 */
			if (clms.clms_api_trans_wrapped)
				tr = clms.clms_api_trans_ptr;
			else
				tr = clms.clms_api_trans_hist;
			break;
		case CLMS_WHERE_GET_THIS:
		case CLMS_WHERE_GET_PREV:
		case CLMS_WHERE_GET_NEXT:
			/*
			 * Find the entry with the passed transid, or just
			 * after the passed transid.
			 */
			if (clms.clms_api_trans_wrapped) {
				size = CLMS_TRANS_HIST_SZ;
				tr = clms.clms_api_trans_ptr;
			}
			else {
				size = clms.clms_api_trans_ptr -
						       clms.clms_api_trans_hist;
				tr = clms.clms_api_trans_hist;
			}
			/*
			 * If we're searching for a specific entry and it's
			 * before the beginning of our history, return the
			 * appropriate error.
			 */
			if ((where == CLMS_WHERE_GET_THIS ||
			     where == CLMS_WHERE_GET_PREV) &&
			    transid < tr->transid) {
				ret = CLMS_HIST_GET_TOOEARLY;
				break;
			}
			/*
			 * Search the history for an entry with the passed
			 * transid.
			 */
			prev = NULL;
			i = 0;
			while (tr->transid < transid && i < size) {
				prev = tr;
				tr++;
				if (tr == (clms.clms_api_trans_hist +
							  CLMS_TRANS_HIST_SZ)) {
					SSI_ASSERT(clms.clms_api_trans_wrapped);
					tr = clms.clms_api_trans_hist;
				}
				i++;
			}
			/*
			 * If we were looking for a specific entry and we
			 * scanned the entire history without finding it,
			 * or the next entry doesn't have the expected
			 * transid, return the error.  Otherwise we found
			 * the entry.
			 */
			if (where == CLMS_WHERE_GET_THIS) {
				if (i == size || tr->transid != transid)
					ret = CLMS_HIST_GET_MALFORMED;
				break;
			}
			/*
			 * If we were looking for an entry previous to a
			 * specific entry and we scanned the entire history
			 * without finding it, or the next entry doesn't
			 * have the expected transid, return the error.
			 * Otherwise we found the entry, and we need to keep
			 * looking.
			 */
			if (where == CLMS_WHERE_GET_PREV) {
				if (i == size || tr->transid != transid) {
					ret = CLMS_HIST_GET_INVALID;
					break;
				}
				/*
				 * If we hit the first transition in the list,
				 * then either the previous one is no longer
				 * around, or it just doesn't exist.
				 */
				if (prev == NULL) {
					if (clms.clms_api_trans_wrapped)
						ret = CLMS_HIST_GET_NOTPRESENT;
					else
						ret = CLMS_HIST_GET_NOPREV;
					break;
				}
				/*
				 * If none of the above is true, we found it.
				 * Point tr at it.
				 */
				tr = prev;
				break;
			}
			/*
			 * We must have been looking for the entry after the
			 * one with the passed transid.  If we've scanned the
			 * entire list, it isn't there.  If the entry we're
			 * pointing at has the passed transid, bump the
			 * pointer; if we go off the end of the list, the
			 * entry isn't there.  Otherwise, we've found the one
			 * we're looking for.
			 */
			 if (i == size) {
				ret = CLMS_HIST_GET_NONEXT;
				break;
			 }
			if (tr->transid == transid) {
				tr++;
				if (tr == (clms.clms_api_trans_hist +
		    					  CLMS_TRANS_HIST_SZ)) {
					SSI_ASSERT(clms.clms_api_trans_wrapped);
					tr = clms.clms_api_trans_hist;
				}
				i++;
				/*
				 * If we passed the last entry, it wasn't
				 * there.
				 */
				if (i == size) {
					ret = CLMS_HIST_GET_NONEXT;
					break;
				}
			}
			/*
			 * We found the entry, and tr points to it.
			 */
			break;
		default:
			ret = CLMS_HIST_GET_BADWHERE;
			break;
	}
	if (ret == CLMS_HIST_GET_FOUND)
		*ent = *tr;
	UNLOCK_SPIN_LOCK(&clms.clms_api_hist_lock);
	return(ret);
}

void
clms_wait_for_shutdown(void)
{
	register int state;

	/*
	 * Wait for the local state to change to SHUTDOWN.
	 * When this routine returns, the transition is complete.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	state = clms.clms_node_info[this_node].api_state.state;
	while (state != CLMS_STATE_SHUTDOWN) {
		WAIT_CONDITION(&clms.clms_node_cond, &clms.clms_node_lock);
		state = clms.clms_node_info[this_node].api_state.state;
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
}
