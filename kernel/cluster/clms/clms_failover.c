/*
 * 	CLMS failover code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains code to support CLMS failover.
 */

#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/reboot.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/icsgen.h>
#include <cluster/node_monitor.h>

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>


static void clms_failover_proc(void *);
static void clms_take_over_master(void);

static struct inform_node_st {
	clusternode_t		node;
	int			count;
	struct inform_node_st	*next;
} *inform_list_head = NULL;

static int clms_doing_failover = 0;

struct query_data_st {
	struct query_data_st	*next;
	clusternode_t		node;
	clms_ctv_t		ctv;
	clms_api_trans_intent_t	intent;
	clms_cli_tr_data_t	*trans_buf;
	int			trans_ents;
	clms_cli_tr_data_t	*save_buf;
	int			save_ents;
	clusternode_t		servers[CLMS_MAX_KEY_SERVICES];
	int			statuses[CLMS_MAX_KEY_SERVICES];
	int			types[CLMS_MAX_KEY_SERVICES];
};

struct query_private_st {
	struct query_data_st	*data_head;
	clms_ctv_t		low_ctv;
	clusternode_t		low_ctv_node;
	clms_ctv_t		high_ctv;
	clusternode_t		high_ctv_node;
};

/*
 * clms_attempt_failover()
 *	See if we need to do CLMS failover, and start it if necessary.
 *
 * Description:
 *	This first probes the master with ics_probe_clms(); if the probe
 *	fails or if the returned address doesn't match, we start CLMS failover,
 *	either by contacting the first up CLMS secondary in the list via the
 *	RCLMS_CLIENT_INFORM_SECONDARY RPC, or by doing it ourselves, if we
 *	happen to be that secondary.
 *
 *	If there were no up secondaries, we panic, since failover will
 *	never happen.
 *
 *	The RCLMS_CLIENT_INFORM_SECONDARY RPC may cause us to be shot, if
 *	the secondary that we contacted can still talk to the master.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
void
clms_attempt_failover(clusternode_t cur_master)
{
	clusternode_t tmp_node;
	icsinfo_t tmp_addr, master_addr;
	clms_secondary_list_t *ent;
	int error, status;

#ifdef DEBUG
	printk(KERN_DEBUG "May have lost master, attempting failover.\n");
#endif

#ifdef DEBUG
	printk(KERN_DEBUG "!clms_attempt_failover (start): %ld\n", TICKS());
#endif

	LOCK_LOCK(&clms.clms_failover_lock);
	/*
	 * If the master changed after we got the lock, a query got in
	 * and changed it; we're already going through failover, so we
	 * have nothing to do.
	 */
	if (clms_master_node != cur_master) {
#ifdef DEBUG
		printk(KERN_NOTICE "Failover already in progress, ceasing failover attempt.\n");
#endif
		UNLOCK_LOCK(&clms.clms_failover_lock);
		return;
	}
	/*
	 * We lost the master.  If we aren't up enough, we can't handle this.
	 * "Aren't up enough" means that we haven't made it to the API
	 * COMINGUP state (we're still in the NEVERUP state, as it was set
	 * during initialization).  This means that other nodes don't know
	 * about us.  Panic.
	 */
	status = clms_api_get_state(this_node);
	if (status == CLMS_STATE_NEVERUP)
		panic("Lost CLMS master while trying to join cluster!\n");
	/*
	 * Try to contact the master with a new RPC for that purpose.  If we
	 * can, just return.  If we can't, try to contact the first secondary
	 * in the list to let him perform failover.  If we're using regroup,
	 * we don't need to do this, since regroup is authoritative.
	 */
	ics_geticsinfo(cur_master, &master_addr);
	error = ics_probe_clms(cur_master, master_addr, &tmp_node, &tmp_addr,
			       clms_im_a_secondary, 1);
	if (error == CLMS_PROBE_ERR_DIE_ALREADY) {
		printk(KERN_WARNING
		       "%s: CLMS master (%u) has kicked us out of the cluster,"
		       " rebooting\n", __FUNCTION__, tmp_node);
		machine_restart(NULL);
		/*UNREACHED*/
	}
	if (error == 0 && tmp_node == cur_master) {
		printk(KERN_WARNING
		       "CLMS master timed out, but probed successfully.");
		nm_add_node(clms_master_node);
		UNLOCK_LOCK(&clms.clms_failover_lock);
		return;
	}
	/*
	 * If he indicated a different master, we must be going through CLMS
	 * failover; he rebooted and found a new master.  Just return; we'll
	 * find out about it soon enough when the new master tells us.
	 */
	if (error == 0 && tmp_node != cur_master) {
		UNLOCK_LOCK(&clms.clms_failover_lock);
		return;
	}
	/*
	 * Find the first up secondary in the list.  If that's us, do
	 * failover here, else inform that node.
	 */
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next) {
		int state;

		if (ent->node == clms_master_node)
			continue;
		state = clms_get_node_status(ent->node);
		if (ent->node == this_node && (state & CLMS_NODE_UP)) {
			/*
			 * If we're the first up node in the list, we get to
			 * be the new master.  Start the CLMS failover thread.
			 */
			LOCK_COND_LOCK(&clms.clms_node_lock);
			if (clms_doing_failover) {
				UNLOCK_COND_LOCK(&clms.clms_node_lock);
				UNLOCK_LOCK(&clms.clms_failover_lock);
				return;
			}

			clms_doing_failover = 1;
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			UNLOCK_LOCK(&clms.clms_failover_lock);

			error = 1;
			while (error != 0) {
				error = spawn_daemon_proc("clms failover",
							  clms_failover_proc,
							  NULL);
				if (error)
					idelay(2*HZ);
			}
			return;
		}
		if (state & CLMS_NODE_UP)
		{
#ifdef DEBUG
			printk(KERN_NOTICE
			       "We did lose the master, informing node %u.\n",
			       ent->node);
#endif
			error = RCLMS_CLIENT_INFORM_SECONDARY_MSG(ent->node,
								  this_node,
								  clms_master_node);
			if (error == 0) {
				UNLOCK_LOCK(&clms.clms_failover_lock);
				return;
			}
			printk(KERN_WARNING
			       "clms_attempt_failover:  Error %d from RCLMS_CLIENT_INFORM_SECONDARY_MSG!\n",
			       error);
			return;
		}
	}
	/*
	 * We may have gone through failover via being informed from another
	 * node, or because another node already took over as master.  If
	 * so, the global idea of the master will have changed.  Just return.
	 */
	if (cur_master != clms_master_node) {
		UNLOCK_LOCK(&clms.clms_failover_lock);
		return;
	}
	/*
	 * No secondaries were available.  Panic, since we can't ever talk to
	 * a master again.
	 */
	panic("Lost network connection to all potential root nodes!");
}

/*
 * rclms_client_inform_secondary()
 *	Inform a CLMS secondary that the master is dead.
 *
 * Description:
 *
 * Parameters:
 *	node		This node.
 *	from_node	The node that noticed the loss.
 *	his_master_node	The master as known by that node.
 *
 * Returns:
 *	0.
 */
int
rclms_client_inform_secondary(
	clusternode_t node,
	clusternode_t from_node,
	clusternode_t his_master_node)
{
	struct inform_node_st *ent, *prev;
	clusternode_t old_master, tmp_node;
	icsinfo_t master_addr, tmp_addr;
	int error;
static DECLARE_MUTEX(inform_list_lock);

#ifdef DEBUG
	printk(KERN_DEBUG
	       "Informed of loss of master from node %u.\n",
	       from_node);
#endif
	SSI_ASSERT(from_node != clms_master_node);
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (clms_doing_failover) {
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
#ifdef DEBUG
		printk(KERN_DEBUG "Already doing failover.\n");
#endif
		return(0);
	}
	old_master = clms_master_node;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	ics_geticsinfo(old_master, &master_addr);
	error = ics_probe_clms(old_master, master_addr, &tmp_node, &tmp_addr,
			       clms_im_a_secondary, 1);
	if (error == CLMS_PROBE_ERR_DIE_ALREADY) {
		printk(KERN_WARNING
		       "%s: CLMS master (%u) has kicked us out of the cluster,"
		       " rebooting\n", __FUNCTION__, tmp_node);
		machine_restart(NULL);
		/*UNREACHED*/
	}

	down(&inform_list_lock);

	if (error == 0 && tmp_node == old_master) {
		printk(KERN_WARNING "Node %u said that the CLMS master was down, but it's up!", from_node);
		/* Add node to list with count; if count > 1, shoot node */
		prev = NULL;
		for (ent = inform_list_head; ent != NULL; ent = ent->next) {
			if (ent->node == from_node)
				break;
			prev = ent;
		}
		if (ent == NULL) {
			ent = kmalloc_nofail(sizeof(struct inform_node_st));
			ent->count = 1;
			ent->node = from_node;
			ent->next = inform_list_head;
			inform_list_head = ent;

			up(&inform_list_lock);
		}
		else {
			if (ent->count++ > CLMS_MAX_INFORM_COUNT) {
				if (prev == NULL) /* Ent is first */
					inform_list_head = ent->next;
				else
					prev->next = ent->next;

				up(&inform_list_lock);
				kfree(ent);

				printk(KERN_WARNING "Shooting node %u", from_node);
				ics_shoot_node(from_node);
			}
		}
		return(0);
	}
	/*
	 * The master appears to really be down.  Start failover processing.
	 */
	if (inform_list_head != NULL) {
		ent = inform_list_head;
		while (ent != NULL) {
			prev = ent->next;
			kfree(ent);
			ent = prev;
		}
	}
	up(&inform_list_lock);

	/*
	 * We might already be doing failover, or it might have already
	 * completed while we were hanging around in ics_probe_clms(),
	 * or one or more of the other nodes might have redundantly informed
	 * us.  Just return if so.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (clms_doing_failover || old_master != clms_master_node) {
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		return(0);
	}

	clms_doing_failover = 1;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	error = 1;
	while (error != 0) {
		error = spawn_daemon_proc("clms failover",
					  clms_failover_proc,
					  NULL);
		if (error)
			idelay(10*HZ);
	}
	return(0);
}

/*
 * clms_query_cluster()
 *	Get the cluster state from all nodes in the cluster.
 *
 * Description:
 *	This routine gets the cluster state as known by the running node,
 *	then sends an RCLMS_QUERY_CLUSTER message to each node in the
 *	cluster and waits for them to call back with their state.  When
 *	all nodes have called back, we have a complete snapshot of the
 *	cluster state as known by each node in the cluster.
 *
 *	This routine also starts monitoring surviving nodes in the cluster.
 *
 * Parameters:
 *	old_master	The failed CLMS master node.
 *	cnlist		The list of nodes to contact.
 *
 * Returns:
 *	A pointer to the head of the linked list containing all the data
 *	received.
 */
struct query_private_st *
clms_query_cluster(
	clusternode_t old_master,
	struct clms_nodelist *cnlist)
{
	struct clms_transition_data *cdp;
	struct query_private_st	*private;
	struct query_data_st	*data_head;
	clusternode_t		ni;
	int			error;
	struct nm_settings	nm_settings;
	int			masterlen;

	CLMSLOG(CLMSLOG_QUERYING, 0, 0, 0, 0, 0, 0, 0);

	cdp = clms_alloc_trans_data();
	SSI_ASSERT(cdp != NULL);
	cdp->node = old_master;
	cdp->cnlist = cnlist;
	cdp->called_cnlist = clms_alloc_nodelist(cdp->cnlist->num_nodes, CLMS_NODE_UP);
	if (!(cdp->called_cnlist))
		panic("clms_query_cluster:  No memory for clms_alloc_nodelist!\n");

	data_head = kmalloc_nofail(sizeof(struct query_data_st));
	data_head->node = this_node;
	/*
	 * Get the list of key server nodes.
	 */
	clms_get_key_servers(data_head->servers,
			     data_head->statuses,
			     data_head->types);
	data_head->trans_ents = clms.clms_client_trans_queue.size;
	data_head->save_ents = clms.clms_client_save_queue.size;
	data_head->trans_buf = data_head->save_buf = NULL;
	/*
	 * The following is okay to do without locking, since the caller
	 * got the CTV lock and changed clms_master_node.  Any transition
	 * will get the CTV lock and check clms_master_node against the
	 * master_node value of the transition; if there's no match, the
	 * transition is dropped.
	 */
	if (data_head->trans_ents > 0) {
		data_head->trans_buf = kmalloc_nofail(
			     sizeof(clms_cli_tr_data_t)
			     * data_head->trans_ents);
		clms_copy_trans_queue_l(&clms.clms_client_trans_queue,
					data_head->trans_buf);
	}
	if (data_head->save_ents > 0) {
		data_head->save_buf = kmalloc_nofail(
			     sizeof(clms_cli_tr_data_t)
			     * data_head->save_ents);
		clms_copy_trans_queue_l(&clms.clms_client_save_queue,
					data_head->save_buf);
	}
	data_head->intent = clms.clms_api_trans_intent;
	data_head->ctv = clms.clms_ctv;
	data_head->next = NULL;

	private = kmalloc_nofail(sizeof(*private));
	private->data_head = data_head;
	private->low_ctv = clms.clms_ctv;
	private->low_ctv_node = this_node;
	private->high_ctv = clms.clms_ctv;
	private->high_ctv_node = this_node;

	cdp->private_data = (void *)private;

	masterlen = strlen(cluster_clmsinfo) + 1;
	nm_get_settings(&nm_settings);
	/*
	 * Register the handle we'll use for callbacks.
	 */
	clms_register_handle((u_long)cdp);
	while ((ni = clms_get_next_node(cnlist)) != CLUSTERNODE_INVAL) {
		if (ni == this_node || ni == old_master)
			continue;
		if (!clms_isnodeavail(ni))
			continue;
		error = RCLMS_QUERY_CLUSTER_MSG(ni,
						(u_long)cdp,
						clms_master_node,
						cluster_clmsinfo, masterlen,
						&nm_settings);
		if (error == 0) {
			if (clms_set_node(cdp->called_cnlist, ni))
				panic("clms_query_cluster:  clms_set_node error\n");
			/* Start monitoring surviving nodes in the cluster. */
			nm_add_node(ni);
		}
		else {
			if (error != -EREMOTE)
				printk(KERN_WARNING
				       "clms_query_cluster:  RCLMS_QUERY_CLUSTER: error %d querying node %u\n",
				       error,
				       ni);
		}
	}
	/*
	 * Wait for all the clients to call back or go down.  This is the
	 * same interface as for nodedown, but used slightly differently.
	 */
	clms_waitfor_client_callbacks(cdp, 0);
	/*
	 * Deregister the handle we used for callbacks.
	 */
	clms_deregister_handle((u_long)cdp);

	/* Don't free struct query_private_st */
	cdp->private_data = NULL;

	/* clms_free_nodelist(cdp->called_cnlist); */
	clms_free_trans_data(cdp);

	return(private);
}

/*
 * rclms_query_cluster()
 *	CLMS failover is asking us for our cluster state.
 *
 * Description:
 *	This routine gets the cluster state as known by the running node,
 *	and sends it to the new master node with the RCLMS_QUERY_REPLY
 *	message.
 *	The Node Monitor is restarted to monitor the new CLMS Master node.
 *
 * Calling/Exit State:
 *	This is called from ics server proc to service an ICS message.
 *
 * Parameters:
 *	my_node		This node.
 *      clms_handle	A handle used when sending the RCLMS_QUERY_REPLY
 *			message.
 *	new_master_node	The new master node.
 *
 * Returns:
 *	Zero.
 */
int
rclms_query_cluster(
	clusternode_t	my_node,
	u_long		clms_handle,
	clusternode_t	new_master_node,
	char		*masterinfo,
	int		masterinfo_len,
	struct nm_settings *nm_settings)
{
	clusternode_t *serverlist;
	int *statuslist, *typelist;
	int error;
	clms_ctv_t ctv;
	int trans_ents, save_ents;
	clusternode_t intent_node;
	int intent_state, intent_why, intent_present;
	clms_ctv_t intent_ctv;
	clms_cli_tr_data_t *trans_buf, *save_buf;

	SSI_ASSERT(new_master_node != clms_master_node);

	CLMSLOG(CLMSLOG_QUERY, new_master_node, 0, 0, 0, 0, 0, 0);
	INCR_MEMPRIO();			/* avoid blocking for memory */
	/*
	 * Do an ICS nodedown on the old CLMS master node to flush any messages
	 * that might be queued or in flight from it.
	 */
	ics_nodedown(clms_master_node);

	LOCK_LOCK(&clms.clms_failover_lock);
	for (;;) {
		trans_ents = clms.clms_client_trans_queue.size;
		save_ents = clms.clms_client_save_queue.size;

		/* Allocate ahead of the spin lock below */
		if (trans_ents > 0)
			trans_buf = kmalloc_nofail(sizeof(clms_cli_tr_data_t)
						   * trans_ents);
		else
			trans_buf = NULL;
		if (save_ents > 0)
			save_buf = kmalloc_nofail(sizeof(clms_cli_tr_data_t)
						  * save_ents);
		else
			save_buf = NULL;
		LOCK_COND_LOCK(&clms.clms_ctv_lock);
		if (trans_ents == clms.clms_client_trans_queue.size &&
		    save_ents == clms.clms_client_save_queue.size)
			break;

		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		kfree(trans_buf);
		kfree(save_buf);
	}
	clms_master_node = new_master_node;

	ctv = clms.clms_ctv;
	/*
	 * Get any intent extant on the local node.
	 */
	intent_node = clms.clms_api_trans_intent.client_node;
	intent_state = clms.clms_api_trans_intent.new_state;
	intent_ctv = clms.clms_api_trans_intent.ctv;
	intent_why = clms.clms_api_trans_intent.why;
	intent_present = clms.clms_api_trans_intent.present;
	/*
	 * Get any outstanding unprocessed transitions.
	 */
	clms_copy_trans_queue_l(&clms.clms_client_trans_queue, trans_buf);
	clms_copy_trans_queue_l(&clms.clms_client_save_queue, save_buf);

	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);

	/*
	 * Wake up any sleeping transitions so they notice the failover and
	 * go away.
	 */
	BROADCAST_CONDITION(&clms.clms_ctv_cond);
	/*
	 * Get the list of key server nodes.
	 */
	serverlist = kmalloc_nofail(sizeof(clusternode_t) * clms_num_key_svcs);
	statuslist = kmalloc_nofail(sizeof(int) * clms_num_key_svcs);
	typelist = kmalloc_nofail(sizeof(int) * clms_num_key_svcs);
	clms_get_key_servers(serverlist, statuslist, typelist);
	error = RCLMS_QUERY_REPLY_MSG(new_master_node,
				      clms_handle,
				      ctv,
				      this_node,
				      intent_node,
				      intent_state,
				      intent_why,
				      intent_present,
				      intent_ctv,
				      trans_ents,
				      save_ents,
				      serverlist,
				      clms_num_key_svcs,
				      statuslist,
				      clms_num_key_svcs,
				      typelist,
				      clms_num_key_svcs,
				      trans_buf,
				      trans_ents,
				      save_buf,
				      save_ents);
	if (error && error != -EREMOTE)
		panic("rclms_query_cluster:  RCLMS_QUERY_REPLY failed with error %d\n", error);
	UNLOCK_LOCK(&clms.clms_failover_lock);
	clms_client_update_masterlist(masterinfo);
	nm_update_settings(nm_settings, 0);
	DECR_MEMPRIO();
	/*
	 * Clean up
	 */
	kfree(typelist);
	kfree(statuslist);
	kfree(serverlist);
	kfree(save_buf);
	kfree(trans_buf);
	return(0);
}

/*
 * rclms_query_reply()
 *	A client is giving CLMS failover its cluster state.
 *
 * Description:
 *	This routine receives the cluster state from a client node and
 *	adds it to the list.
 *
 * Calling/Exit State:
 *	This is called from ics server proc to service an ICS message.
 *
 * Parameters:
 *	my_node		This node.
 *	clms_handle	A handle used when sending the RCLMS_QUERY_REPLY
 *			message.
 *	ctv		The CTV of the client.
 *	client_node	The client node.
 *	intent_node	The node in any intent pending at the client.
 *	intent_state	The state in any intent pending at the client.
 *	intent_why	The why in any intent pending at the client.
 *	intent_present	The "intent present" flag in any intent pending at
 *			the client.
 *	intent_ctv	The CTV in any intent pending at the client.
 *	trans_ents	The number of meaningful entries in the trans queue
 *			of the client.
 *	save_ents	The number of meaningful entries in the save queue
 *			of the client.
 *	serverlist	The list of key servers as known by the client.
 *	num_servers	The length of that array.
 *	statuslist	The status of each key server.
 *	num_statuses	The length of that array.
 *	typelist	The type of each key server.
 *	num_types	The length of that array.
 *	trans_buf	The contents of the trans queue.
 *	trans_sz	The length of that array.
 *	save_buf	The contents of the save queue.
 *	save_sz		The length of that array.
 *
 * Returns:
 *	Zero.
 */
int
rclms_query_reply(
	clusternode_t	my_node,
	u_long		clms_handle,
	clms_ctv_t	ctv,
	clusternode_t	client_node,
	clusternode_t	intent_node,
	int		intent_state,
	int		intent_why,
	int		intent_present,
	clms_ctv_t	intent_ctv,
	int		trans_ents,
	int		save_ents,
	clusternode_t	*serverlist,
	int		num_servers,
	int		*statuslist,
	int		num_statuses,
	int		*typelist,
	int		num_types,
	clms_cli_tr_data_t *trans_buf,
	int		trans_sz,
	clms_cli_tr_data_t *save_buf,
	int		save_sz)
{
	struct clms_transition_data *cdp;
	struct query_data_st *query_data;
	struct query_private_st *private;
	int i;
	clusternode_t *tmp;

	SSI_ASSERT(num_servers == num_statuses &&
	       num_servers == num_types &&
	       num_servers == clms_num_key_svcs);
	SSI_ASSERT(trans_sz == CLMS_TRANS_HIST_SZ &&
	       save_sz == CLMS_TRANS_HIST_SZ);
	CLMSLOG(CLMSLOG_QUERYREPLY,
		client_node,
		trans_ents,
		save_ents,
		0, 0, 0, 0);
	INCR_MEMPRIO();			/* avoid blocking for memory */
	cdp = (struct clms_transition_data *)clms_handle;

	query_data = kmalloc_nofail(sizeof(struct query_data_st));
	query_data->node = client_node;
	if (trans_ents > 0)
		query_data->trans_buf = trans_buf;
	else
		query_data->trans_buf = NULL;
	if (save_ents > 0)
		query_data->save_buf = save_buf;
	else
		query_data->save_buf = NULL;
	query_data->trans_ents = trans_ents;
	query_data->save_ents = save_ents;
	query_data->intent.client_node = intent_node;
	query_data->intent.new_state = intent_state;
	query_data->intent.why = intent_why;
	query_data->intent.ctv = intent_ctv;
	query_data->ctv = ctv;
	for (i = 0; i < clms_num_key_svcs; i++) {
		query_data->servers[i] = serverlist[i];
		query_data->statuses[i] = statuslist[i];
		query_data->types[i] = typelist[i];
	}

	LOCK_COND_LOCK(&cdp->lock);

	private = (struct query_private_st *)cdp->private_data;

	query_data->next = private->data_head;
	private->data_head = query_data;
	if (ctv.transid < private->low_ctv.transid) {
		private->low_ctv = ctv;
		private->low_ctv_node = client_node;
	}
	if (ctv.transid > private->high_ctv.transid) {
		private->high_ctv = ctv;
		private->high_ctv_node = client_node;
	}
	UNLOCK_COND_LOCK(&cdp->lock);

	LOCK_COND_LOCK(&clms.clms_node_lock);
	for (tmp = cdp->called_cnlist->n_rptr;
	     tmp != cdp->called_cnlist->n_wptr ; tmp++) {
		if (*tmp == client_node) {
			*tmp = CLUSTERNODE_INVAL;
			break;
		}
	}
	SSI_ASSERT(tmp != NULL);
	BROADCAST_CONDITION(&clms.clms_node_cond);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

#ifdef SKIP
	kfree(trans_buf);
	kfree(save_buf);
#endif
	DECR_MEMPRIO();
	return(0);
}

/*
 * rclms_roll_forward
 *	The new CLMS master is telling us to roll unprocessed transitions
 *	forward.
 *
 * Description:
 *	The new CLMS master has polled the cluster and built a list of
 *	transitions that haven't yet been processed by various nodes in
 *	the cluster.  It's now giving us this list so we can roll any
 *	outstanding transitions forward.  When we're done, our CTV will
 *	match his, and that of all other nodes in the cluster.
 *
 *	If there are no outstanding transitions in the cluster, this
 *	routine will not be called.
 *
 *	There is no locking here, since the cluster is idle; all old
 *	transitions have been tossed or dropped by the ics_nodedown()
 *	and related processing during clms_query_cluster() and
 *	rclms_query_cluster(), and the new master isn't sending any new
 *	transitions yet.  We can safely assume we're completely single-
 *	threaded.
 *
 * Calling/Exit State:
 *	This is called from an ICS server proc to service an ICS RPC.
 *
 * Parameters:
 *	node		This node.
 *	rval		Return value of the RPC.
 *	trans_buf	The list of outstanding transitions.
 *	trans_ents	The number of outstanding transitions.
 *
 * Returns:
 *	Zero.
 */
int
rclms_roll_forward(
	clusternode_t	node,
	int	*rval,
	clms_cli_tr_data_t *trans_buf,
	int	trans_ents)
{
	int i;
	clms_cli_tr_data_t *datap;

	CLMSLOG(CLMSLOG_ROLL, trans_ents, 0, 0, 0, 0, 0, 0);
	INCR_MEMPRIO();			/* avoid blocking for memory */
	for (i = 0; i < trans_ents; i++) {
		int freeit;

		if (trans_buf[i].ctv.transid <= clms.clms_ctv.transid)
			continue;
		datap = clms_alloc_client_tr_data(trans_buf[i].trans,
						  trans_buf[i].node,
						  trans_buf[i].why,
						  trans_buf[i].ctv);
		if (trans_buf[i].trans == CLMS_STATE_DUMMY_INTENT) {
			if (clms.clms_api_trans_intent.present == 0)
				rclms_new_state_intent(this_node,
						       rval,
						       clms_master_node,
						       trans_buf[i].node,
						       trans_buf[i].trans,
						       trans_buf[i].why,
						       trans_buf[i].ctv);
		}
		if (trans_buf[i].trans == CLMS_STATE_KCLEANUP)
			datap->data = trans_buf[i].data;
		LOCK_COND_LOCK(&clms.clms_ctv_lock);
		/*
		 * Put the transition on the queue.  If the transition was
		 * already there (indicated by a nonzero return from the
		 * clms_put_trans_queue_l() routine), free it.
		 */
		freeit = clms_put_trans_queue_l(&(clms.clms_client_trans_queue),
						datap);
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		if (freeit != 0)
			kfree(datap);
	}
	/*
	 * Now run the clms_client_run_trans_queue() routine to process
	 * the entries we just put on the trans queue.  This is asynchronous
	 * (except for the queue and CTV processing), and we don't need to
	 * hang around for everything to complete, since existing interlocking
	 * will put future transitions in their proper order.  We also need
	 * to let the new CLMS master run, so nodes that go down will be
	 * noticed and processed.
	 */
	clms_client_run_trans_queue();
	/*
	 * Now go through the transitions we just processed; they were all put
	 * on the "save" queue synchronously by clms_client_run_trans_queue(),
	 * so we need to get rid of them.  The rest of the contents of the
	 * "save" queue (if any) will be taken care of by the new master
	 * after we're done here.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	for (i = 0; i < trans_ents; i++) {
		if (trans_buf[i].ctv.transid <= clms.clms_ctv.transid)
			continue;
		datap = clms_del_trans_queue_l(&clms.clms_client_save_queue,
	 				       trans_buf[i].ctv);
		SSI_ASSERT(datap != NULL);
		kfree(datap);
	}
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * Let all interested processes know about the cluster topology change.
	 */
	clms_sigcluster_all();
	DECR_MEMPRIO();
	*rval = 0;
	return(0);
}

/*
 * rclms_waitfor_nodedowns
 *	The new CLMS master is telling us to wait for all nodedown processing
 *	to complete.
 *
 * Description:
 *	The new CLMS master has completed failover, but can't allow new nodes
 *	into the cluster because there may still be nodedown processing on
 *	client nodes initiated by the old CLMS master.  If one of those nodes
 *	tries to rejoin before that processing is complete, we may deadlock.
 *	Since we can't easily determine this particular case, we just wait for
 *	_all_ nodedown processing to complete.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	rval		Return value of the RPC.
 *
 * Returns:
 *	Zero.
 */
int
rclms_waitfor_nodedowns(
	clusternode_t	node,
	int		*rval)
{
	/*
	 * This macro waits for the clms_transition_nd field to go to zero;
	 * this field is incremented for each nodedown and decremented when
	 * the nodedown completes.  The second parameter is a flag to set in
	 * the clms_transition_flag field; since it's zero here, this is a
	 * no-op.
	 */
	CLMS_TRANSITION_BLOCK(B_TRUE, 0);
	*rval = 0;

	return 0;
}

static void
clms_failover_proc(
	void *argp)
{
	nsc_daemonize();

	INCR_MEMPRIO();			/* avoid blocking for memory */

#ifdef DEBUG
	printk(KERN_NOTICE "!clms_failover_proc (START): %ld\n", TICKS());
#endif

	clms_take_over_master();
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms_doing_failover = 0;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

#ifdef DEBUG
	printk(KERN_NOTICE "!clms_failover_proc (END): %ld\n", TICKS());
#endif

	exit_daemon_proc();
}

/*
 * clms_unblock_clients()
 *	Send unblocks to any clients that need them.
 *
 * Description:
 *	Go through the client data and send unblock messages to the clients
 *	that need them.
 *
 * Calling/Exit State:
 *	None
 *
 * Parameters:
 *	data_head	Pointer to the beginning of the linked list of data
 *			from all nodes.
 *
 * Returns:
 *	None.
 */
static void
clms_unblock_clients(
	struct query_data_st *data_head)
{
	struct query_data_st *cur;
	int i;

	/*
	 * Now go through the list and send any unblock messages corresponding
	 * to entries in the "save" list for each node.
	 */
	for (cur = data_head; cur != NULL; cur = cur->next) {
		for (i = 0; i < cur->save_ents; i++) {
			if (cur->node == this_node)
				(void)rclms_transition_unblock(
							  this_node,
							  this_node,
							  cur->save_buf[i].ctv);
			else
				(void)RCLMS_TRANSITION_UNBLOCK_MSG(
							  cur->node,
							  this_node,
							  cur->save_buf[i].ctv);
		}
	}
}

/*
 * clms_process_transitions()
 *	Process the transition list gathered earlier.
 *
 * Description:
 *	Run through the list of transitions as known by all nodes, build a
 *	list of all potentially outstanding transitions in the cluster and
 *	roll them forward, if necessary.
 *
 * Calling/Exit State:
 *	None
 *
 * Parameters:
 *	private		Pointer to the top of the data from all nodes.
 *
 * Returns:
 *	None.
 */
static void
clms_process_transitions(
	struct query_private_st *private,
	clusternode_t old_master)
{
	int i, num_nodes, error, status;
	struct query_data_st *data_head, *cur;
	struct clms_client_trans_queue mytransl;
	clms_api_trans_intent_t myintent;
	clms_cli_tr_data_t *miss_start, *trans, *ntrans, *trans_buf;
	cli_handle_t **handle_list, **handle_list_ptr;

	data_head = private->data_head;
	/*
	 * Get any outstanding intent on this node, for use later.
	 */
	myintent = clms.clms_api_trans_intent;
	/*
	 * Find the node with the highest CTV, and use that node's history
	 * to build our list.  Start the list with the lowest CTV in the
	 * cluster.
	 */
	mytransl.last = mytransl.first = NULL;
	mytransl.size = 0;
	cur = data_head;
	while (cur->node != private->high_ctv_node)
		cur = cur->next;
	SSI_ASSERT(cur != NULL);
	for (i = 0; i < cur->save_ents; i++) {
		if (cur->save_buf[i].ctv.transid <= private->low_ctv.transid)
			continue;
		clms_put_trans_queue_l(&mytransl, &(cur->save_buf[i]));
	}
	/*
	 * Now add the all unprocessed transitions to the list.  Scan the
	 * list for each node and insert any entries later than the highest
	 * CTV we know about into our list of transitions.  Also find any
	 * outstanding intent; there's only one across the cluster, we just
	 * find the first occurrence.
	 */
	cur = data_head;
	while (cur != NULL) {
		if (cur->intent.present == 1 &&
		    myintent.present == 0)
			myintent = cur->intent;
		for (i = 0; i < cur->trans_ents; i++) {
			if (cur->trans_buf[i].ctv.transid <= private->high_ctv.transid)
				continue;
			/*
			 * Insert the transition into our list; this routine
			 * detects duplicates and drops them.
			 */
			clms_put_trans_queue_l(&mytransl, &(cur->trans_buf[i]));
		}
		cur = cur->next;
	}
	/*
	 * Remember where we are, so we can start looking for missing
	 * transitions here.
	 */
	miss_start = mytransl.first;
	/*
	 * Now see if we have an intent, and we're missing it from our list.
	 * If so, add a dummy intent entry to our list; this lets us get the
	 * intent to any node that might have missed it so we can roll the
	 * transition forward.  Note that there can only ever be one intent
	 * outstanding in the cluster.
	 */
	if (myintent.present == 1) {
		for (trans = miss_start; trans != NULL; trans = trans->next) {
			if (trans->ctv.ups == myintent.ctv.ups &&
			    trans->ctv.downs == myintent.ctv.downs &&
			    trans->ctv.other + 1 == myintent.ctv.other) {
				clms_cli_tr_data_t *dummy;

				dummy = clms_alloc_client_tr_data(
						    CLMS_STATE_DUMMY_INTENT,
						    myintent.client_node,
						    myintent.why,
						    myintent.ctv);
				clms_put_trans_queue_l(&mytransl, dummy);
				break;
			}
		}
	}
	/*
	 * Now scan for missing transitions and add any needed dummies.
	 */
	for (trans = miss_start;
	     trans != NULL && trans->next != NULL;
	     trans = ntrans) {
		ntrans = trans->next;
		if (!CLMS_CTV_TRANS_IS_NEXT(&(trans->ctv), &(ntrans->ctv))) {
			clms_ctv_t tctv;
			clms_cli_tr_data_t *dummy;
			int miss_others, miss_ups, miss_downs;
			u_long miss_transid;

			miss_others = ntrans->ctv.other - trans->ctv.other;
			miss_ups = ntrans->ctv.ups - trans->ctv.ups;
			miss_downs = ntrans->ctv.downs - trans->ctv.downs;
			miss_transid = trans->ctv.transid;
			tctv.other = trans->ctv.other;
			tctv.ups = trans->ctv.ups;
			tctv.downs = trans->ctv.downs;
			for (i = 0; i < miss_others; i++) {
				tctv.other++;
				tctv.transid++;
				SSI_ASSERT(tctv.transid < ntrans->ctv.transid);
				dummy = clms_alloc_client_tr_data(
						     CLMS_STATE_DUMMY,
						     CLUSTERNODE_INVAL,
						     CLMS_TRANS_WHY_UNEXPECTED,
						     tctv);
				clms_put_trans_queue_l(&mytransl, dummy);
			}
			for (i = 0; i < miss_ups; i++) {
				tctv.ups++;
				tctv.transid++;
				SSI_ASSERT(tctv.transid < ntrans->ctv.transid);
				dummy = clms_alloc_client_tr_data(
						     CLMS_STATE_DUMMY,
						     CLUSTERNODE_INVAL,
						     CLMS_TRANS_WHY_UNEXPECTED,
						     tctv);
				clms_put_trans_queue_l(&mytransl, dummy);
			}
			for (i = 0; i < miss_downs; i++) {
				tctv.downs++;
				tctv.transid++;
				SSI_ASSERT(tctv.transid < ntrans->ctv.transid);
				dummy = clms_alloc_client_tr_data(
						     CLMS_STATE_DUMMY,
						     CLUSTERNODE_INVAL,
						     CLMS_TRANS_WHY_UNEXPECTED,
						     tctv);
				clms_put_trans_queue_l(&mytransl, dummy);
			}
		}
	}
	/*
	 * If we have no transitions in our list, there's nothing to do,
	 * we didn't even allocate anything.  Get out.
	 */
	if (mytransl.size == 0) {
		clms_unblock_clients(data_head);
		return;
	}
	SSI_ASSERT(mytransl.size <= CLMS_TRANS_HIST_SZ*2);
	/*
	 * We have the complete list of transitions, including any outstanding
	 * intent and dummies to fill in any transitions missing in the CTV
	 * sequence.  Communicate this to all nodes.  Since we potentially
	 * combined both the "save" and "trans" queues, both of which can be
	 * CLMS_TRANS_HIST_SZ, our worst case, probably never actually
	 * realized, is double that.
	 */
	trans_buf = kmalloc_nofail(sizeof(clms_cli_tr_data_t) *
				   mytransl.size);

	clms_copy_trans_queue_l(&mytransl, trans_buf);
	num_nodes = 0;
	handle_list = kmalloc_nofail(NSC_MAX_NODE_VALUE *
				     sizeof(cli_handle_t *));
	handle_list_ptr = handle_list;
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		if (i == this_node || i == old_master)
			continue;
		if (!clms_isnodeavail(i))
			continue;
		*handle_list_ptr = NULL;
		error = RCLMS_ROLL_FORWARD_SEND(i,
						handle_list_ptr,
						trans_buf,
						mytransl.size);
		if (icscli_async_sent(handle_list_ptr)) {
			num_nodes++;
			handle_list_ptr++;
		} else if (error != -EREMOTE)
			printk(KERN_WARNING
			       "%s: Error %d from RCLMS_ROLL_FORWARD_SEND"
			       " to node %d", __FUNCTION__, error, i);
	}
	handle_list_ptr = handle_list;
	for (i = 0; i < num_nodes; i++) {
		error = RCLMS_ROLL_FORWARD_RECEIVE(*handle_list_ptr++,
						   &status,
						   trans_buf,
						   mytransl.size);
		if (error == 0)
			error = status;
		if (error != -EREMOTE && error != 0) {
			printk(KERN_WARNING "%s: "
				"Error %d from RCLMS_ROLL_FORWARD_RECEIVE",
				__FUNCTION__, error);
		}
	}
	kfree(handle_list);
	/*
	 * Roll the transitions forward on this node, as well.  This
	 * uses the client code to process the transitions; when
	 * everything is done, we'll revert to the master code, which
	 * is blocked right now.
	 */
	(void)rclms_roll_forward(this_node,
				 &status,
				 trans_buf,
				 mytransl.size);
	/*
	 * Unblock any clients that need it.  We have to do this after the
	 * roll forward to avoid potential races.
	 */
	clms_unblock_clients(data_head);
	/*
	 * Now go through our transition list and free any dummies that
	 * we added above.
	 */
	trans = miss_start;
	while (trans != NULL) {
		ntrans = trans->next;
		if (trans->trans == CLMS_STATE_DUMMY_INTENT ||
		    trans->trans == CLMS_STATE_DUMMY)
			kfree(trans);
		trans = ntrans;
	}
	/*
	 * Free the buffer we used.
	 */
	kfree(trans_buf);
}

/*
 * clms_process_key_servers()
 *	Find and unhang any hung key servers.
 *
 * Description:
 *	If key service failover was ongoing when the master died, there
 *	may be key servers that haven't received their RCLMS_REASSIGN_SERVICES
 *	message even though other nodes have received the nodedown message
 *	indicating the new key servers.  Find these servers and send them
 *	the appropriate RCLMS_REASSIGN_SERVICES messages.
 *
 * Calling/Exit State:
 *	None
 *
 * Parameters:
 *	data_head	Pointer to the top of the data from all nodes.
 *
 * Returns:
 *	None.
 */
static void
clms_process_key_servers(
	struct query_data_st *data_head)
{
	int i, j, num_servers, error;
	int **service_flags, *hung_types, *flaglist = NULL;
	struct query_data_st *cur;
	clusternode_t *hung_servers, *nodelist;

	SSI_ASSERT(data_head != NULL);
	/*
	 * Now we can process the list of key servers returned by the query.
	 * We need to notice if any key server doesn't know that he's such,
	 * and so inform him.  We do this by running the list of key servers
	 * sent us during the cluster query.  If nodes indicate a server
	 * for a key service and that key service is not ready, and _that_
	 * node indicates _another_ node (one that may or may not be down,
	 * but that has a status of ready), then he must be sent the
	 * RCLMS_REASSIGN_SERVICES message to inform him that he's the
	 * new server for the key service.
	 */
	hung_servers = kzmalloc_nofail(clms_num_key_svcs
				      * sizeof(clusternode_t));
	hung_types = kzmalloc_nofail(clms_num_key_svcs * sizeof(int));
	nodelist = kzmalloc_nofail(clms_num_key_svcs * sizeof(clusternode_t));
	service_flags = kzmalloc_nofail(clms_num_key_svcs * sizeof(int *));
	for (i = 0; i < clms_num_key_svcs; i++) {
		service_flags[i] =
			kzmalloc_nofail(clms_num_key_svcs * sizeof(int));
	}
	for (i = 0; i < clms_num_key_svcs; i++)
		hung_servers[i] = CLUSTERNODE_INVAL;
	/*
	 * Find any servers that might be hung.  This finds all the key
	 * servers and makes an entry for each in the hung_servers and
	 * hung_types arrays.
	 */
	cur = data_head;
	while (cur != NULL) {
		for (i = 0; i < clms_num_key_svcs; i++) {
			if (cur->statuses[i] == CLUSTER_SERVER_NOT_READY) {
				hung_servers[i] = cur->servers[i];
				hung_types[i] = cur->types[i];
			}
		}
		cur = cur->next;
	}
	/*
	 * Check the servers found above to see if any point to some node
	 * other than themselves.  If a key server node thinks that some other
	 * node is the key server, then it can't have received the reassign,
	 * and the service is hung.  The way we do this here is scan for
	 * each server, and remove it from the list of hung servers if it
	 * _does_ point to itself for that service.
	 */
	for (i = 0; i < clms_num_key_svcs; i++) {
		cur = data_head;
		while (cur != NULL) {
			if (cur->node == hung_servers[i] &&
			    cur->servers[i] == hung_servers[i]) {
				hung_servers[i] = CLUSTERNODE_INVAL;
				break;
			}
			cur = cur->next;
		}
	}

	/*
	 * The list build above is sparse.  Compress it into another list,
	 * and set up the list of flags for each reassign message.  Each
	 * per-node flag array is initialized (by kmem_zalloc(), above)
	 * to zero, which means to take no action.  If the server is
	 * believed to be primary, set the flag to 1, else set it to 2
	 * to indicate that it's a floater server.  Note that the flaglist
	 * arrays are indexed by service, therefore i, not anything else.
	 */
	num_servers = 0;
	for (i = 0; i < clms_num_key_svcs; i++) {
		if (hung_servers[i] != CLUSTERNODE_INVAL) {
			for (j = 0; j < num_servers; j++)
				if (nodelist[j] == hung_servers[i])
					break;
			if (j == num_servers) {
				nodelist[num_servers] = hung_servers[i];
				flaglist = service_flags[num_servers];
				num_servers++;
			}
			if (hung_types[i] == CLUSTER_PRIMARY_SERVER)
				flaglist[i] = 1;
			else
				flaglist[i] = 2;
		}
	}
	/*
	 * Send an RCLMS_REASSIGN_SERVICES message to each key server, to
	 * make it do key service failover.
	 */
	for (i = 0; i < num_servers; i++) {
		flaglist = service_flags[i];
		if (nodelist[i] == this_node) {
			struct clms_reassign_info_st *reassign_info;

			reassign_info = kmalloc_nofail(
				sizeof(struct clms_reassign_info_st));
			reassign_info->from_node = this_node;
			reassign_info->down_node = CLUSTERNODE_INVAL;
			reassign_info->nlist =
				     clms_get_nsc_nodelist(CLMS_NODE_HALF_UP |
							   CLMS_NODE_UP |
							   CLMS_NODE_HALF_DOWN);
			memcpy(reassign_info->flags,
			       flaglist,
			       clms_num_key_svcs * sizeof(int));
			smp_mb(); /* barrier ensures args visible to handler */
			do {
				error = spawn_daemon_thread(
						"reassign_services_thread",
						clms_reassign_services_thread,
						(void *)reassign_info);
				if (error != 0)
					idelay(HZ/10);
			} while(error != 0);
		}
		else {
			nsc_nodelist_t *nlist;

			nlist = clms_get_nsc_nodelist(CLMS_NODE_HALF_UP |
						      CLMS_NODE_UP |
						      CLMS_NODE_HALF_DOWN);
			(void)RCLMS_REASSIGN_SERVICES_MSG(nodelist[i],
							  CLUSTERNODE_INVAL,
							  nlist,
							  flaglist,
							  clms_num_key_svcs);
			NSC_NODELIST_FREE(nlist);
		}
	}
	for (i = 0; i < clms_num_key_svcs; i++)
		kfree(service_flags[i]);
	kfree(service_flags);
	kfree(nodelist);
	kfree(hung_types);
	kfree(hung_servers);
}

/*
 * clms_take_over_master
 *	Perform CLMS failover as the new master.
 *
 * Description:
 *	We're tapped to be the new CLMS master.  Block all transitions as
 *	the master, reinitialize ourselves appropriately, then build a
 *	list of all potentially outstanding transitions in the cluster
 *	and roll them forward, if necessary.  Also detect hung key services
 *	and un-hang them.  When we're done, allow nodedowns, and do nodedowns
 *	for the old master and any other nodes we might have lost.  Only then
 *	allow nodeups and clean up.
 *
 * Calling/Exit State:
 *	None
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
static void
clms_take_over_master(void)
{
	clusternode_t old_master;
	struct clms_nodelist *contact_list;
	int i, error, status, num_nodes;
	struct query_private_st *private;
	cli_handle_t **handle_list, **handle_list_ptr;
	struct query_data_st *cur;
	clusternode_t *ucleanup_list, *nd_list;
	struct clms_transition_data *cdp, *tmp;

	printk(KERN_WARNING
	       "Taking over master from node %u.\n",
	       clms_master_node);
	CLMSLOG(CLMSLOG_FAILSTART, clms_master_node, 0, 0, 0, 0, 0, 0);
	/*
	 * Lock out all API-driven transitions until we're ready for them.
	 */
	LOCK_COND_LOCK(&clms.clms_transition_lock);
	clms.clms_transition_flag |= (CLMS_TRANS_BLOCK_TRANS |
				      CLMS_TRANS_FAILOVER);
	UNLOCK_COND_LOCK(&clms.clms_transition_lock);

	/*
	 * We're about to become the master.  Clear the flag that indicates
	 * that all key services have been assigned, so that we don't do
	 * nodeups. All key services _are_ assigned, but we'll indicate that
	 * when we're ready.
	 */
	clms_all_svcs_assigned = 0;

	/*
	 * Set the "CTV failover" flag to block state transitions from
	 * updating the CTV until we've rolled any missing transitions
	 * forward.  This also prevents nodedowns and nodeups from being
	 * sent to clients while failover is going on, so they don't need
	 * a separate way to block them.  This DOES NOT block ICS nodedown;
	 * it's done before the nodedown thread sleeps.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	clms.clms_ctv_failover = 1;
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * Make us the master so that new nodes will find us and so we
	 * start hearing about nodedowns.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	old_master = clms_master_node;
	clms_master_node = this_node;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	/*
	 * Initialize this node as the master.  For non-Regroup kernels, this
	 * also starts the keepalive daemon, which will (eventually) time out
	 * any nodes that are down but for which we have not gotten a nodedown
	 * from the now-dead master.
	 */
	clms_master_init(1);

	/*
	 * Query each known node and wait for a response.  If any nodes
	 * were (or go) down, Regroup will let us know and we'll do an
	 * ICS nodedown, so we won't wait forever.  First get the list of
	 * nodes that Regroup knows are up; this is the list of nodes we
	 * will contact.
	 */
	contact_list = clms_get_up_nodelist();
	private = clms_query_cluster(old_master, contact_list);
	/* clms_free_nodelist(contact_list); */ /* Done in clms_query_cluster() */
	/*
	 * Create the transition list and roll any missing transitions
	 * forward.
	 */
	clms_process_transitions(private, old_master);
	/*
	 * The CTV is stable and we can handle nodedowns now, so clear the
	 * "CTV failover" flag.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	clms.clms_ctv_failover = 0;
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	BROADCAST_CONDITION(&clms.clms_ctv_cond);
	nd_list = kzmalloc_nofail((NSC_MAX_NODE_VALUE + 1)
				  * sizeof(clusternode_t));
	/*
	 * Do a nodedown for the former master node.  All clients now know
	 * that we're the new master, as a result of the query.
	 */
	clms_nodedown(old_master);
	nd_list[old_master] = 1;
	/*
	 * Nodes that were coming up just as the failover occured will
	 * panic if they have not yet reached the COMINGUP API state.
	 * We need to flush stale entries left by these coming-up nodes.
	 */
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		if (clms.clms_node_info[i].current_state ==
				CLMS_NODE_INFORM_COMING_UP)
			clms_nodedown(i);
	}
	/*
	 * Process the list of key servers and un-hang any hung servers.
	 */
	clms_process_key_servers(private->data_head);
	/*
	 * Before we can allow nodes to join the cluster, we have to wait for
	 * all nodedown processing to complete on all nodes; we do this because
	 * there may be outstanding nodedown processing initiated by the old
	 * master, and one or more of those nodes may be trying to rejoin the
	 * cluster.  We can't let that happen until all nodedown processing is
	 * finished.  This RPC doesn't return from any node until the node has
	 * completed its nodedown processing.
	 */
	num_nodes = 0;
	handle_list_ptr = handle_list =
			(cli_handle_t **)kmalloc_nofail(NSC_MAX_NODE_VALUE
							* sizeof(cli_handle_t *));
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		if (i == this_node || i == old_master)
			continue;
		if (!clms_isnodeavail(i))
			continue;
		*handle_list_ptr = NULL;
		error = RCLMS_WAITFOR_NODEDOWNS_SEND(i,
						     handle_list_ptr);
		if (icscli_async_sent(handle_list_ptr)) {
			num_nodes++;
			handle_list_ptr++;
		} else if (error != -EREMOTE)
			printk(KERN_WARNING
			       "%s: Error %d from RCLMS_WAITFOR_NODEDOWNS_SEND"
			       " to node %d", __FUNCTION__,  error, i);
	}
	handle_list_ptr = handle_list;
	for (i = 0; i < num_nodes; i++) {
		error = RCLMS_WAITFOR_NODEDOWNS_RECEIVE(*handle_list_ptr++,
							&status);
		if (error == 0)
			error = status;
		if (error != -EREMOTE && error != 0)
			printk(KERN_WARNING
			       "clms_take_over_master:  Error %d from RCLMS_ROLL_FORWARD_RECEIVE",
			       error);
	}
	kfree(handle_list);
	/*
	 * This macro waits for the clms_transition_nd field to go to zero;
	 * this field is incremented for each nodedown and decremented when
	 * the nodedown completes.  The second parameter is a flag to set in
	 * the clms_transition_flag field; since it's zero here, this is a
	 * no-op.
	 */
	CLMS_TRANSITION_BLOCK(B_TRUE, 0);
	/*
	 * If there were any nodedowns outstanding when we failed over, we
	 * need to finish processing them.  Any that have an API state of
	 * CLMS_STATE_KCLEANUP need to go to CLMS_STATE_UCLEANUP; any others
	 * were already finished by the client code.  Remember this list and
	 * process it after we unblock state transitions.  Don't remember any
	 * nodes for which we did nodedowns above; this includes the old
	 * master.
	 */
	ucleanup_list = kmalloc_nofail(NSC_MAX_NODE_VALUE
				       * sizeof(clusternode_t));
	num_nodes = 0;
	LOCK_COND_LOCK(&clms.clms_node_lock);
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		if (nd_list[i] != 1 &&
		    clms.clms_node_info[i].api_state.state ==
							   CLMS_STATE_KCLEANUP)
			ucleanup_list[num_nodes++] = i;
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	CLMSLOG(CLMSLOG_FAILFIN, 0, 0, 0, 0, 0, 0, 0);
	/*
	 * Now we can handle node joins, so indicate that all services have
	 * been assigned (this lets the node join daemon actually do stuff)
	 * and wake up the node join daemon.  This may change as a result
	 * of nodedown, though.
	 */
	LOCK_COND_LOCK(&clms.clms_node_join_queue.cq_lock);
	clms_all_svcs_assigned = 1;
	UNLOCK_COND_LOCK(&clms.clms_node_join_queue.cq_lock);
	SIGNAL_CONDITION(&clms.clms_node_join_queue.cq_cond);
	/*
	 * Allow announcements that nodes are up; we can handle it now.
	 */
	LOCK_COND_LOCK(&clms.clms_transition_lock);
	clms.clms_transition_flag &= ~(CLMS_TRANS_BLOCK_TRANS |
				       CLMS_TRANS_FAILOVER);
	BROADCAST_CONDITION(&clms.clms_transition_cond);
	UNLOCK_COND_LOCK(&clms.clms_transition_lock);
	/*
	 * Now do the KCLEANUP->UCLEANUP transitions we saved away earlier.
	 */
	for (i = 0; i < num_nodes; i++) {
		int why;

		error = clms_api_validate_new_state(ucleanup_list[i],
						    CLMS_STATE_UCLEANUP,
						    &why);
		SSI_ASSERT(error == 0);
		clms_api_newstate(ucleanup_list[i], CLMS_STATE_UCLEANUP, why);
	}
	/*
	 * Process any nodes that tried to come in while we were doing
	 * failover.  We don't need to lock here because we've cleared
	 * the CLMS_TRANS_FAILOVER flag so no new nodes will enter the
	 * list; we're the only clients of the list.
	 */
	cdp = clms.clms_failover_nodeup_queue.cq_first;
	while (cdp != NULL) {
		tmp = cdp->next;
		clms_node_trans_ind(cdp->node,
				    CLMS_NODE_HALF_UP,
				    cdp->private_data);
		clms_rmvq_trans_l(&clms.clms_failover_nodeup_queue, cdp);
		clms_free_trans_data(cdp);
		cdp = tmp;
	}
	/*
	 * Clean up after ourselves.
	 */
	kfree(ucleanup_list);
	kfree(nd_list);
	/*
	 * Now free all the data we accumulated from the query.
	 */
	cur = private->data_head;
	while (cur != NULL) {
		private->data_head = cur->next;
		if (cur->trans_buf != NULL)
			kfree(cur->trans_buf);
		if (cur->save_buf != NULL)
			kfree(cur->save_buf);
		kfree(cur);
		cur = private->data_head;
	}
	kfree(private);
}
