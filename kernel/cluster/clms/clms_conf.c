/*
 * 	CLMS subsystem configuration file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * NOTE: This file is the clms subsystem configuration file. Up to
 * 	 CLMS_MAX_SUBSYSTEMS (see clms.h) subsystems can be added.
 * 	 In order to add a new clms service, this file has to be edited
 *       to properly register the service.
 *
 *	 The registered routines (nodeup and nodedown) will be called when
 *	 the appropriate cluster transition occurs.
 *
 *	 (i) All these routines have to return a value of 0.
 *
 *	 (ii) They have to issue a callback to clms once they have completed
 *	      the processing. The callback functions are as follows:
 *
 *		Function		Callback Fucntion
 *
 *		nodeup 			clms_nodeup_callback()
 *		nodedown 		clms_nodedown_callback()
 *
 *	 	All the callback functions are called with
 *		(void *clms_handle, int service, clusternode_t node).
 *		Incidentally these three parameters are passed to the function
 *		being called by CLMS.
 *
 *
 *		Example:
 *		The nodedown() function of a service may decide to spawn a
 *		thread to handle the "Node Down" and return to the caller.
 *		In that case the nodedown() function has to return a 0.
 *		The spawned thread will issue the callback to CLMS.
 */

#include <cluster/clms.h>

int clms_num_subsys = 0;
struct sinit	*clms_subsystems[CLMS_MAX_SUBSYSTEMS];


int
register_clms_subsys(
	char	*s_name,
	int	pri,
	int	(*subsys_nodeup)(void *, clms_subsys_t,
			      clusternode_t, clusternode_t, void *),
	int	(*subsys_nodedown)(void *, clms_subsys_t,
				clusternode_t, clusternode_t, void *),
	void	(*subsys_init_prejoin)(void),
	void	(*subsys_init_postjoin)(void),
	void	(*subsys_init_postroot)(void))
{
	struct sinit *subsys;

	SSI_ASSERT(clms_num_subsys < CLMS_MAX_SUBSYSTEMS);

	subsys = (struct sinit *)kmalloc_nofail(sizeof(struct sinit));
	subsys->service = clms_num_subsys++;
	subsys->s_name = s_name;
	subsys->pri = pri;
	subsys->subsys_nodeup = subsys_nodeup;
	subsys->subsys_nodedown = subsys_nodedown;
	subsys->subsys_init_prejoin = subsys_init_prejoin;
	subsys->subsys_init_postjoin = subsys_init_postjoin;
	subsys->subsys_init_postroot = subsys_init_postroot;
	clms_subsystems[subsys->service] = subsys;

	clms_svc_register(subsys);

	return 0;
}

void
clms_subsystems_init_prejoin(void)
{
	int i;

	for (i = 0; i < clms_num_subsys; i++) {
		if (clms_subsystems[i]->subsys_init_prejoin)
			clms_subsystems[i]->subsys_init_prejoin();
	}
}

void
clms_subsystems_init_postjoin(void)
{
	int i;

	for (i = 0; i < clms_num_subsys; i++) {
		if (clms_subsystems[i]->subsys_init_postjoin)
			clms_subsystems[i]->subsys_init_postjoin();
	}
}

void
clms_subsystems_init_postroot(void)
{
	int i;

	for (i = 0; i < clms_num_subsys; i++) {
		if (clms_subsystems[i]->subsys_init_postroot)
			clms_subsystems[i]->subsys_init_postroot();
	}
}

/*
 * CLMS Tunables
 */
/* Whether or not nodedown runs in Fixed-Priority scheduling class. */
#define CLMS_NODEDOWN_FP        1

/*
 * How long in seconds to wait for a secondary root that should take over to
 * stop probing us.
 */
#define CLMS_FORCE_ROOT_DLY     300

/*
 * How long in seconds to wait for nodedown to complete before panicking
 * the node.
 */
#define CLMS_NDTO_PANIC_SECS    300

/*
 * From net/nsc_clms/nsc_clms.cf/Space.c
 */
unsigned int clms_run_nodedown_at_fp = CLMS_NODEDOWN_FP;
unsigned int clms_force_root_dly = CLMS_FORCE_ROOT_DLY;
unsigned int clms_nodedown_timeout = CLMS_NDTO_PANIC_SECS;

/*
 * From clms_boot.c
 */
int clms_service_wait_timeout = 60; /* How long to wait for services */
int clms_service_timeout_use_2nd = 0; /* Use a secondary if we timed out */
int clms_service_timeout_use_master = 1; /* Use the master likewise */

