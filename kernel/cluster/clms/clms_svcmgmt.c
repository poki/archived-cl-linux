/*
 *	CLMS key services management code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the CLMS Management code. It provides CLMS
 * initialization and some common subroutines.
 */

#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/timer.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/async.h>
#include <cluster/nodelist.h>
#include <cluster/icsgen.h>

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>

static void clms_handle_nodedown_timeout(unsigned long);
static int clms_waitfor_callbacks(struct clms_transition_data *, int, int);
static int clms_check_callbacks(struct clms_transition_data *, int, int);

int clms_num_key_svcs = 0;

/*
 * The table of CLMS key services.  This structure contains the failover
 * callback function pointer, as well as a "critical service" flag that
 * indicates to CLMS that the cluster cannot function without a server for
 * the key service. This also contains a field for the key server node, for
 * quick reference (so we don't have to search the big table for that
 * information). The name registered is the same used in the boot file.
 */
struct key_service_info {
	char		*name;
	clusternode_t	server;
	int		crit;
	void		(*fail)(nsc_nodelist_t *);
	int		(*pull_data)(char *, int, int *);
	void		(*failover_data)(clusternode_t, char *, int);
	void		(*server_ready)(clusternode_t);
	void		(*init_prejoin)(void);
	void		(*init_postjoin)(void);
	void		(*init_postroot)(void);
} key_service_info[CLMS_MAX_KEY_SERVICES];

#define	CLMS_CALL_SERVER_READY(_svc, _node) \
	(key_service_info[(_svc)].server_ready != NULL \
	 ? key_service_info[(_svc)].server_ready(_node) \
	 : (void)0)

static struct clms_assignee_list {
	clusternode_t			node;
	struct clms_assignee_list	*next;
} *clms_assignees = NULL;

static struct clms_assignee_list *clms_waitforq = NULL;

/*
 * This flag indicates whether all key services have been assigned on the
 * master, and is only meaningful (or used at all) there.  It is zero
 * at boot, and is set to one when clms_assign_key_services() is called
 * to assign all key services.  Zero indicates that we must queue any
 * CLUSTER_SVRINFO_REQUESTs, since they arrived before the master node
 * had discovered all the key servers.
 */
int clms_all_svcs_assigned = 0;

#define isspace(c) (((c) == ' ') || ((c) == '\t') || ((c) == '\n'))

int
register_clms_key_service(
	char	*name,
	int	*service_num,
	int	crit_flag,
	void	(*fail)(nsc_nodelist_t *),
	int	(*pull_data)(char *, int, int *),
	void	(*failover_data)(clusternode_t, char *, int),
	void	(*server_ready)(clusternode_t),
	void	(*init_prejoin)(void),
	void	(*init_postjoin)(void),
	void	(*init_postroot)(void))
{
	int service;

	SSI_ASSERT(clms_num_key_svcs < CLMS_MAX_KEY_SERVICES);

	*service_num = service = clms_num_key_svcs++;
	key_service_info[service].name = name;
	key_service_info[service].crit = crit_flag;
	key_service_info[service].fail = fail;
	key_service_info[service].pull_data = pull_data;
	key_service_info[service].failover_data = failover_data;
	key_service_info[service].server_ready = server_ready;
	key_service_info[service].init_prejoin = init_prejoin;
	key_service_info[service].init_postjoin = init_postjoin;
	key_service_info[service].init_postroot = init_postroot;

	return 0;
}

void
clms_key_svcs_init_prejoin(void)
{
	int i;

	for (i = 0; i < clms_num_key_svcs; i++) {
		if (key_service_info[i].init_prejoin)
			key_service_info[i].init_prejoin();
	}
}

void
clms_key_svcs_init_postjoin(void)
{
	int i;

	for (i = 0; i < clms_num_key_svcs; i++) {
		if (key_service_info[i].init_postjoin)
			key_service_info[i].init_postjoin();
	}
}

void
clms_key_svcs_init_postroot(void)
{
	int i;

	for (i = 0; i < clms_num_key_svcs; i++) {
		if (key_service_info[i].init_postroot)
			key_service_info[i].init_postroot();
	}
}

/*
 * Get the next token from the supplied string.
 * Returns pointer to the next token (adds null termination).
 * Resets user-supplied pointer to point to remainder of string.
 */
char *
clms_next_token(
	char	**strpp,	/* [IN/OUT]: points to unparsed portion */
	char	delim)		/* String delimiter */
{
	char	*tok;
	register char *p = *strpp;

	if (p == NULL)
		return NULL;

	while (isspace(*p))
		p++;
	tok = p;		/* beginning of token */

	/*
	 * Look for string delimiter or end of string
	 * If delimiter found,
	 *	- terminate service name
	 *	- reset pointer to unparsed portion
	 * If end of string,
	 *	- no need to terminate
	 *	- reset pointer to NULL (indicates string completely parsed)
	 */
	while (*p != '\0' && *p != delim)
		p++;
	if (*p == '\0') {
		*strpp = NULL;
	} else if (*p == delim) {
		*p = '\0';
		*strpp = p + 1;
	}

	return tok;
}


/*
 * clms_master_setup_key_services()
 *	Set up a masters's key services.
 *
 * Description:
 *	Just set the key services in the database.
 *
 * Arguments:
 *	None.
 */
void
clms_master_setup_key_services(void)
{
	register int j;

	SSI_ASSERT(clms_master_node == this_node);
#ifdef DEBUG
	printk(KERN_DEBUG "Initializing key services for master...\n");
#endif

	/* If nothing specified, default to primary=all */
	if (cluster_svcinfo == NULL && cluster_svcsecondary == NULL)
		cluster_svcinfo = "all";

	if (cluster_svcinfo) {
		if (strcmp(cluster_svcinfo, "all") == 0) {
			for (j = 0; j < clms_num_key_svcs; j++) {
				clms_vset_cluster_svr(j,
						      this_node,
						      CLUSTER_PRIMARY_SERVER,
						      CLUSTER_SERVER_NOT_READY);
#ifdef DEBUG
				printk(KERN_DEBUG
				       "	%s as primary\n",
				       key_service_info[j].name);
#endif
			}
		}
		else {
			register char *svc = NULL;
			char *nextsvc;

			if (strcmp(cluster_svcinfo, "none") == 0)
				nextsvc = NULL;
			else
				nextsvc = cluster_svcinfo;

			while ((svc = clms_next_token(&nextsvc, ','))) {
				register int service;

				service = -1;
				for (j = 0; j < clms_num_key_svcs; j++) {
					if (!strcmp(svc,
						    key_service_info[j].name)) {
						service = j;
						break;
					}
				}
				if (service == -1) {
					printk(KERN_WARNING
					       "Invalid key service \"%s\" in bootfile\n",
					       svc);
					continue;
				}
				clms_vset_cluster_svr(
					service, this_node,
					CLUSTER_PRIMARY_SERVER,
					CLUSTER_SERVER_NOT_READY);
#ifdef DEBUG
				printk(KERN_DEBUG
				       "	%s as primary\n",
				       key_service_info[service].name);
#endif
			}
		}
	}

	if (cluster_svcsecondary) {
		if (strcmp(cluster_svcsecondary, "all") == 0) {
			for (j = 0; j < clms_num_key_svcs; j++) {
				if (clms_is_cluster_svr_set(j, this_node))
					continue;
				clms_vset_cluster_svr(
					j, this_node,
					CLUSTER_SECONDARY_SERVER,
					CLUSTER_SERVER_NOT_READY);
#ifdef DEBUG
				printk(KERN_DEBUG
				       "	%s as secondary\n",
				       key_service_info[j].name);
#endif
			}
		} else {
			register char *svc = NULL;
			char *nextsvc;

			if (strcmp(cluster_svcsecondary, "none") == 0)
				nextsvc = NULL;
			else
				nextsvc = cluster_svcsecondary;

			while ((svc = clms_next_token(&nextsvc, ','))) {
				register int service;

				service = -1;
				for (j = 0; j < clms_num_key_svcs; j++) {
					if (!strcmp(svc,
						    key_service_info[j].name)) {
						service = j;
						break;
					}
				}
				if (service == -1) {
					printk(KERN_WARNING
					       "Invalid key service \"%s\" in bootfile\n",
					       svc);
					continue;
				}
				if (clms_is_cluster_svr_set(j, this_node))
					continue;
				clms_vset_cluster_svr(
					service, this_node,
					CLUSTER_SECONDARY_SERVER,
					CLUSTER_SERVER_NOT_READY);
#ifdef DEBUG
				printk(KERN_DEBUG
				       "	%s as secondary\n",
				       key_service_info[service].name);
#endif
			}
		}
	}
}

/*
 * clms_client_setup_key_services()
 *	Set up a client's key services.
 *
 * Description:
 *	Offer any key services we may provide to the master, and wait for
 *	the reply.
 *
 * Arguments:
 *	None.
 */
void
clms_client_setup_key_services(void)
{
	register int i, j;
	int error;
	int svcs[CLMS_MAX_KEY_SERVICES];
	register int have_service;

	SSI_ASSERT(clms_master_node != this_node);
	have_service = 0;
#ifdef DEBUG
	printk(KERN_DEBUG "clms_client_setup_key_services: Initializing key services for client...\n");
#endif
	/* If there are no key services, simply return. */
	if (clms_num_key_svcs == 0)
		return;

	/*
	 * Create a list of key services we provide, and send this to the
	 * CLMS master.
	 */

	for (i = 0; i < clms_num_key_svcs; i++)
		svcs[i] = CLUSTER_NOT_A_SERVER;		/* initialize */

	/*
	 * If we're in the list of possible masters and
	 * nothing specified, default to primary=all.
	 */
	if (clms_is_secondary(this_node) &&
	    (cluster_svcinfo == NULL && cluster_svcsecondary == NULL))
		cluster_svcinfo = "all";

	if (cluster_svcinfo) {
		if (strcmp(cluster_svcinfo, "all") == 0) {
			for (j = 0; j < clms_num_key_svcs; j++)
				svcs[j] = CLUSTER_PRIMARY_SERVER;
			have_service = 1;
		} else {
			register char *svc;
			char *nextsvc;

			if (strcmp(cluster_svcinfo, "none") == 0)
				nextsvc = NULL;
			else
				nextsvc = cluster_svcinfo;

			while ((svc = clms_next_token(&nextsvc, ','))) {
				register int service;

				service = -1;
				for (j = 0; j < clms_num_key_svcs; j++) {
					if (!strcmp(svc,
						    key_service_info[j].name)) {
						service = j;
						break;
					}
				}
				if (service == -1) {
					printk(KERN_WARNING
					       "Invalid key service \"%s\" in bootfile\n",
					       svc);
					continue;
				}
				svcs[service] = CLUSTER_PRIMARY_SERVER;
				have_service = 1;
			}
		}
	}

	if (cluster_svcsecondary) {
		if (strcmp(cluster_svcsecondary, "all") == 0) {
			for (j = 0; j < clms_num_key_svcs; j++) {
				if (svcs[j] == 0)
					svcs[j] = CLUSTER_SECONDARY_SERVER;
			}
			have_service = 1;
		} else {
			register char *svc;
			char *nextsvc;

			if (strcmp(cluster_svcsecondary, "none") == 0)
				nextsvc = NULL;
			else
				nextsvc = cluster_svcsecondary;

			while ((svc = clms_next_token(&nextsvc, ','))) {
				register int service;

				service = -1;
				for (j = 0; j < clms_num_key_svcs; j++) {
					if (!strcmp(svc,
						    key_service_info[j].name)) {
						service = j;
						break;
					}
				}
				if (service == -1) {
					printk(KERN_WARNING
					       "Invalid key service \"%s\" in bootfile\n",
					       svc);
					continue;
				}
				if (svcs[service] == 0)
					svcs[service] = CLUSTER_SECONDARY_SERVER;
				have_service = 1;
			}
		}
	}

	if (have_service) {
#ifdef DEBUG
		printk(KERN_DEBUG "clms_client_setup_key_services: Offering:\n");
#endif /* DEBUG */
		for (i = 0; i < clms_num_key_svcs; i++) {
			if (svcs[i] != CLUSTER_NOT_A_SERVER) {
#ifdef DEBUG
				printk(KERN_DEBUG
				       "	%s as %s\n",
				       key_service_info[i].name,
				       (svcs[i] == CLUSTER_PRIMARY_SERVER) ?
						   "primary" : "secondary");
#endif
				clms_vset_cluster_svr(i,
						      this_node,
						      CLUSTER_SECONDARY_SERVER,
						      CLUSTER_SERVER_NOT_READY);
			}
		}
		error = RCLMS_OFFER_SERVICES_MSG(clms_master_node,
						 this_node,
						 svcs,
						 clms_num_key_svcs);
		if (error) {
			panic("clms_assign_key_servers: "
			      "rclms_offer_services failed with error %d\n",
			      error);
		}
#ifdef DEBUG
		printk(KERN_DEBUG "clms_client_setup_key_services: Sent offer_services_msg, waiting for assign_service_event\n");
#endif /* DEBUG */
		WAIT_EVENT(&clms_assign_service_event);
	}
}

int
rclms_offer_services(
	clusternode_t	node,
	clusternode_t	from_node,
	int		*svcs,
	int		num_svcs)
{
	register int i, done;
	cluster_server_t *clsp, *savep = NULL;
	int *new_svcs;

#ifdef DEBUG
	printk(KERN_DEBUG "Node %u offering:\n", from_node);
	for (i = 0; i < clms_num_key_svcs; i++) {
		if (svcs[i] != CLUSTER_NOT_A_SERVER) {
			printk(KERN_DEBUG "	%s as %s\n",
			       key_service_info[i].name,
			       svcs[i] == CLUSTER_PRIMARY_SERVER ? "primary" : "secondary");
		}
	}
#endif
	LOCK_LOCK(&clms_key_service_lock);

	if (clms_all_svcs_assigned == 0) {
		struct clms_assignee_list *new;
		new = kmalloc_nofail(sizeof(struct clms_assignee_list));
		new->node = from_node;
		new->next = clms_assignees;
		clms_assignees = new;
	}

	new_svcs = kmalloc_nofail(sizeof(int) * clms_num_key_svcs);
	done = 1;
	for (i=0; i < clms_num_key_svcs; i++) {
		new_svcs[i] = CLUSTER_NOT_A_SERVER;
		if (svcs[i] != CLUSTER_NOT_A_SERVER) {
			clms_vset_cluster_svr(i,
					      from_node,
					      CLUSTER_SECONDARY_SERVER,
					      CLUSTER_SERVER_NOT_READY);
			new_svcs[i] = CLUSTER_SECONDARY_SERVER;
		}
		if (svcs[i] == CLUSTER_PRIMARY_SERVER)
			done = 0;
	}

	if (done)
		goto unlock;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		if (svcs[i] == CLUSTER_PRIMARY_SERVER) {
			clsp = clms.clms_cluster_server[i];
			while (clsp) {
				if (clsp->cls_node == from_node)
					savep = clsp;
				/*
				 * If there's a primary or floater server, we
				 * can't be it.  There can be multiple
				 * secondary servers.
				 */
				if (clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
				    clsp->cls_type == CLUSTER_FLOATER_SERVER)
					break;
				clsp = clsp->cls_next;
			}
			/*
			 * If no primary was found and we're still getting
			 * servers to form a cluster, let this guy be the
			 * primary.
			 */
			if (clsp == NULL && clms_all_svcs_assigned == 0) {
				savep->cls_type = CLUSTER_PRIMARY_SERVER;
				new_svcs[i] = CLUSTER_PRIMARY_SERVER;
				key_service_info[i].server = from_node;
			}
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
unlock:	;
	UNLOCK_LOCK(&clms_key_service_lock);
	if (clms_all_svcs_assigned != 0) {
		int error;

		/* Make sure we can talk to the node. */
		error = clms_waitfor_nodeicsup(from_node);
                if (error & (CLMS_NODE_GOING_DOWN | CLMS_NODE_DOWN)) {
#ifdef DEBUG
                        printk(KERN_DEBUG "rclms_offer_services:  Node %u went down before we could assign him key services!\n", from_node);
#endif
                        return(0);
                        /*NOTREACHED*/
                }
		error = RCLMS_ASSIGN_SERVICES_MSG(from_node,
						  clms_all_svcs_assigned,
						  new_svcs,
						  clms_num_key_svcs);
		if (error != 0)
                        panic("rclms_offer_services:  Error %d from RCLMS_ASSIGN_SERVICES to node %u!\n", error, from_node);

	}
	kfree(new_svcs);
	return(0);
}

int
rclms_assign_services(
	clusternode_t	node,
	int		all_svcs_assigned,
	int		*svcs,
	int		num_svcs)
{
	int i;

#ifdef DEBUG
	printk(KERN_DEBUG "We were assigned:\n");
	for (i = 0; i < clms_num_key_svcs; i++) {
		if (svcs[i] != CLUSTER_NOT_A_SERVER) {
			printk(KERN_DEBUG "	%s as %s\n",
			       key_service_info[i].name,
			       svcs[i] == CLUSTER_PRIMARY_SERVER ? "primary" : "secondary");
		}
	}
#endif
	for (i = 0; i < clms_num_key_svcs; i++) {
		if (svcs[i]) {
			clms_vset_cluster_svr(i,
					      this_node,
					      svcs[i],
					      CLUSTER_SERVER_NOT_READY);
		}
	}
#ifdef LINUX_SSI_EVENT
	SIGNAL_EVENT(&clms_assign_service_event);
#else
	BROADCAST_EVENT(&clms_assign_service_event);
#endif
	clms_all_svcs_assigned = all_svcs_assigned;
	BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);
	return(0);
}

void
clms_assign_key_services(void)
{
	int i, error, *svcs;
	struct clms_assignee_list *assignee, *last;
	cluster_server_t *clsp;

	LOCK_LOCK(&clms_key_service_lock);
	SSI_ASSERT(clms_all_svcs_assigned == 0);
	LOCK_COND_LOCK(&clms.clms_node_join_queue.cq_lock);
	clms_all_svcs_assigned = 1;
	UNLOCK_COND_LOCK(&clms.clms_node_join_queue.cq_lock);
	SIGNAL_CONDITION(&clms.clms_node_join_queue.cq_cond);
	if (clms_assignees == NULL && clms_waitforq == NULL) {
		UNLOCK_LOCK(&clms_key_service_lock);
		return;
	}

	svcs = kmalloc_nofail(sizeof(int) * clms_num_key_svcs);

	assignee = clms_assignees;
	while (assignee != NULL) {
		last = assignee->next;
		LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		for (i=0; i < clms_num_key_svcs; i++) {
			svcs[i] = CLUSTER_NOT_A_SERVER;
			clsp = clms.clms_cluster_server[i];
			while (clsp) {
				if (clsp->cls_node == assignee->node) {
					svcs[i] = clsp->cls_type;
					break;
				}
				clsp = clsp->cls_next;
			}
		}
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
#ifdef DEBUG
		printk(KERN_DEBUG "Assigning to node %u:\n", assignee->node);
		for (i = 0; i < clms_num_key_svcs; i++) {
			if (svcs[i] != CLUSTER_NOT_A_SERVER) {
				printk(KERN_DEBUG "	%s as %s\n",
				       key_service_info[i].name,
				       svcs[i] == CLUSTER_PRIMARY_SERVER ? "primary" : "secondary");
			}
		}
#endif
		error = RCLMS_ASSIGN_SERVICES_MSG(assignee->node,
						  clms_all_svcs_assigned,
						  svcs,
						  clms_num_key_svcs);
		if (error != 0 && error != -EREMOTE)
			printk(KERN_WARNING "clms_assign_key_services:  rclms_assign_services failed with error %d\n", error);
		kfree(assignee);
		assignee = last;
	}
	clms_assignees = NULL;
	assignee = clms_waitforq;
	while (assignee != NULL) {
		last = assignee->next;
		clms_answer_cluster_svrinfo_request(assignee->node);
		kfree(assignee);
		assignee = last;
	}
	clms_waitforq = NULL;
	UNLOCK_LOCK(&clms_key_service_lock);
	kfree(svcs);
}

int
clms_queue_cluster_svrinfo_request(
	clusternode_t	node)
{
	struct clms_assignee_list *new;

	LOCK_LOCK(&clms_key_service_lock);
	if (clms_all_svcs_assigned == 1) {
		UNLOCK_LOCK(&clms_key_service_lock);
		return(clms_answer_cluster_svrinfo_request(node));
	}
	new = kmalloc_nofail(sizeof(struct clms_assignee_list));
	new->node = node;
	new->next = clms_waitforq;
	clms_waitforq = new;
	UNLOCK_LOCK(&clms_key_service_lock);
	return(0);
}

/*
 * clms_reassign_key_services
 *	Reassign key services served by a down node.
 *
 * Description:
 *	Only used on the master.  If the passed node was a server for any
 *	key services, find secondaries for those key services and assign the
 *	key services to those nodes.
 *
 * Arguments:
 *	down_node	The node that went down, if this call is from
 *			clms_master_nodedown_thread() to reassign key services
 *			when a node has gone down.  CLUSTERNODE_INVAL if this
 *			call is from rclms_send_nodeup_info() to assign
 *			unassigned key services to a new node.
 *	new_node	The node that just came up, if this call is from
 *			rclms_send_nodeup_info() to assign unassigned
 *			key services to a new node.  CLUSTERNODE_INVAL
 *			otherwise.
 *	servers		The list of nodes to which each key service is
 *			assigned entries may be CLUSTERNODE_INVAL if there is
 *			no server for that key service.  This may carry
 *			redundant information for nodes whose status didn't
 *			change.
 */
void
clms_reassign_key_services(
	clusternode_t	down_node,
	clusternode_t	new_node,
	clusternode_t	*servers)
{
	register int i, j, no_secondary = 0, nodes = 0;
	int error;
	int need_secondary[CLMS_MAX_KEY_SERVICES];
	clusternode_t *nodelist = NULL;
	int **flags = NULL, *flaglist;
	register cluster_server_t *clsp, *savep = NULL;

	error = 0;
	/*
	 * Scan the key_service_info list to see if the down node was a
	 * primary or we have no server for a key service.  This is much
	 * quicker than delving into the key service database, for nodes
	 * that weren't primaries.
	 */
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		if (new_node == CLUSTERNODE_INVAL)
			servers[i] = key_service_info[i].server;
		else
			servers[i] = CLUSTERNODE_INVAL;
		if (key_service_info[i].server == down_node) {
			/*
			 * The node was a primary, or we had no server (if
			 * down_node is CLUSTERNODE_INVAL).  Remember that
			 * we need a secondary for this key service, and that
			 * we got a hit on our scan.
			 */
			need_secondary[i] = 1;
			servers[i] = CLUSTERNODE_INVAL;
			error = 1;
#ifdef DEBUG
			printk(KERN_DEBUG
			       "clms_reassign_key_services:  Need new server for %s.\n",
			       key_service_info[i].name);
#endif
			CLMSLOG(CLMSLOG_NEED2ND, i, down_node, new_node,
						 0, 0, 0, 0);
		} else
			need_secondary[i] = 0;
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	if (error == 0)
		return;
	if (new_node == CLUSTERNODE_INVAL) {
		no_secondary = nodes = 0;
		flags = kmalloc_nofail(clms_num_key_svcs * sizeof(int *));
		for (i = 0; i < clms_num_key_svcs; i++) {
			flags[i] = kzmalloc_nofail(clms_num_key_svcs
						   * sizeof(int));
		}
		nodelist = kmalloc_nofail(clms_num_key_svcs
					  * sizeof(clusternode_t));
	}
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		savep = NULL;
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (down_node != CLUSTERNODE_INVAL &&
			    clsp->cls_node == down_node) {
				/*
				 * We found the node.  If it was the CLMS
				 * master set up in a floater, forget that.
				 * Else, even if it's not the primary, set
				 * CLUSTER_NOT_A_SERVER so that we never try
				 * to pick a down node as a potential
				 * secondary.  (We don't need to communicate
				 * this to clients, since they will notice
				 * the situation themselves.
				 */
				if (clsp->cls_type == CLUSTER_FLOATER_SERVER) {
					clsp->cls_type = CLUSTER_FLOATER_ENTRY;
					clsp->cls_node = CLUSTERNODE_INVAL;
				}
				else
					clsp->cls_type = CLUSTER_NOT_A_SERVER;
#ifdef DEBUG
				printk(KERN_DEBUG
				       "clms_reassign_key_services:  Clearing key service %s for node %u.\n",
				       key_service_info[i].name,
				       clsp->cls_node);
#endif
				CLMSLOG(CLMSLOG_CLEARSVC, i, clsp->cls_node,
							  0, 0, 0, 0, 0);
			}
			if (down_node != CLUSTERNODE_INVAL &&
			    clsp->cls_node != down_node) {
				/*
				 * Not the down node; if it's a secondary,
				 * remember it in case we need one for this
				 * key service.  This always picks the
				 * secondary on the lowest-numbered node.
				 */
				if (clsp->cls_type == CLUSTER_SECONDARY_SERVER &&
				    clsp->cls_status == CLUSTER_SECONDARY_UP &&
				    (savep == NULL ||
				     clsp->cls_node < savep->cls_node) &&
				    clms_isnodeavail(clsp->cls_node)) {
					savep = clsp;
					CLMSLOG(CLMSLOG_SAVE2ND,
						i,
						clsp->cls_node,
						clsp->cls_type,
						clsp->cls_status,
						0, 0, 0);
				}
			}
			if (new_node != CLUSTERNODE_INVAL &&
			    clsp->cls_node == new_node &&
			    need_secondary[i] == 1) {
				/*
				 * If the new node is a secondary, what?
				 * Choose it?  Remember it for below?
				 */
				SSI_ASSERT(clsp->cls_type == CLUSTER_SECONDARY_SERVER);
				servers[i] = new_node;
				clsp->cls_type = CLUSTER_PRIMARY_SERVER;
				clsp->cls_status = CLUSTER_SERVER_NOT_READY;
				key_service_info[i].server = clsp->cls_node;
			}
			/*
			 * Communicate all servers; this is redundant, but
			 * lets us indicate that a key service has lost its
			 * server.
			 */
			if (new_node == CLUSTERNODE_INVAL &&
			    (clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			     clsp->cls_type == CLUSTER_FLOATER_SERVER))
				servers[i] = clsp->cls_node;
			clsp = clsp->cls_next;
		}
		if (new_node == CLUSTERNODE_INVAL && need_secondary[i] == 1) {
			/*
			 * We need a secondary for this key service.
			 */
			if (savep == NULL) {	/* Didn't find one */
				no_secondary = 1;
				need_secondary[i] = 2;
			}
			else {
				/*
				 * We found a secondary above.  Remember the
				 * node for the caller, and mark it as the
				 * primary.
				 */
				servers[i] = savep->cls_node;
				savep->cls_type = CLUSTER_PRIMARY_SERVER;
				savep->cls_status = CLUSTER_SERVER_NOT_READY;
				key_service_info[i].server = savep->cls_node;
				/*
				 * If we already picked the node that is the
				 * new primary for this key service as the new
				 * primary for another key service, just mark
				 * the service entry appropriately.  Else make
				 * a new entry for this node in our nodelist
				 * array, and remember that we reassigned the
				 * key service to that node.  We use a two-
				 * dimensional array here to be able to avoid
				 * more than one RPC per reassigned-to node
				 * below.
				 */
				for (j = 0; j < nodes; j++)
					if (nodelist[j] == savep->cls_node)
						break;
				if (j == nodes) {
					nodelist[nodes] = savep->cls_node;
					nodes++;
				}
				flaglist = flags[j];
				flaglist[i] = 1;
			}
		}
	}
	if (new_node == CLUSTERNODE_INVAL && no_secondary != 0) {
		/*
		 * We couldn't find a secondary for some key services.
		 * Remember that in a convenient place and complain.
		 * Fall back to the CLMS master if we can.
		 */
		for (i = 0; i < clms_num_key_svcs; i++)
			if (need_secondary[i] == 2) {
				if (key_service_info[i].crit != 0) {
					/*
					 * NSC_XXX We currently can't handle
					 * losing a "critical" key service with
					 * no node to take it over.  Just
					 * panic for now.  In the future,
					 * this code will be useful.
					 *
					no_secondary |= 2;
					key_service_info[i].server = CLUSTERNODE_INVAL;
					continue;
					 */
					panic("Lost critical service %s with no secondary node!",
					      key_service_info[i].name);
				}
				no_secondary |= 4;
				need_secondary[i] = 3;
				for (j = 0; j < nodes; j++)
					if (nodelist[j] == this_node)
						break;
				if (j == nodes) {
					nodelist[nodes] = savep->cls_node;
					nodes++;
				}
				flaglist = flags[j];
				flaglist[i] = 2;
				savep = clms.clms_cluster_floater[i];
				savep->cls_node = this_node;
				savep->cls_type = CLUSTER_FLOATER_SERVER;
				key_service_info[i].server = this_node;
				servers[i] = this_node;
			}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	/*
	 * If we were assigning to a new node, tell all nodes what we've done.
	 */
	if (new_node != CLUSTERNODE_INVAL) {
		for (i = 0; i < clms_num_key_svcs; i++)
			if (servers[i] != CLUSTERNODE_INVAL)
				clms_send_cluster_svrinfo(
					new_node,
					i,
					CLUSTER_PRIMARY_SERVER,
					CLUSTER_SERVER_NOT_READY,
					CLMS_NODE_HALF_UP|CLMS_NODE_UP
							 |CLMS_NODE_COMING_UP);

		return;
	}
	if (no_secondary != 0) {
		/*
		 * Complain about the above events.
		 */
		if (no_secondary & 2) {
			printk(KERN_INFO "WARNING: Lost key server for:");
			for (i = 0; i < clms_num_key_svcs; i++) {
				if (need_secondary[i] == 2)
					printk(KERN_INFO
					       " %s",
					       key_service_info[i].name);
			}
			printk(KERN_INFO "\n");
		}
		if (no_secondary & 4) {
			printk(KERN_INFO "WARNING: Using CLMS master as key server for:");
			for (i = 0; i < clms_num_key_svcs; i++) {
				if (need_secondary[i] == 3)
					printk(KERN_INFO
						" %s",
						key_service_info[i].name);
			}
			printk(KERN_INFO "\n");
		}
	}
	/*
	 * Inform each of the nodes that are being reassigned as primaries
	 * that such is the case.
	 */
	for (i=0; i < nodes; i++) {
		flaglist = flags[i];
		if (nodelist[i] == this_node) {
			struct clms_reassign_info_st *reassign_info;

			reassign_info = kmalloc_nofail(
				sizeof(struct clms_reassign_info_st));
			reassign_info->from_node = this_node;
			reassign_info->down_node = down_node;
			reassign_info->nlist =
				    clms_get_nsc_nodelist(CLMS_NODE_HALF_UP |
							  CLMS_NODE_UP |
							  CLMS_NODE_HALF_DOWN);
			if (reassign_info->nlist == NULL)
				panic("clms_reassign_key_services:  No memory!\n");
			memcpy(reassign_info->flags,
			       flaglist,
			       clms_num_key_svcs * sizeof(int));
			smp_mb(); /* barrier ensures args visible to handler */
			do {
				error = spawn_daemon_thread(
						"reassign_services_thread",
						clms_reassign_services_thread,
						(void *)reassign_info);
				if (error != 0)
					idelay(HZ/10);
			} while(error != 0);
		}
		else {
			nsc_nodelist_t *nlist;

			nlist = clms_get_nsc_nodelist(CLMS_NODE_HALF_UP |
						      CLMS_NODE_UP |
						      CLMS_NODE_HALF_DOWN);
			if (nlist == NULL)
				panic("clms_reassign_key_services:  No memory!\n");
			error = -EAGAIN;
			while (error == -EAGAIN) {
				error = RCLMS_REASSIGN_SERVICES_MSG(nodelist[i],
								    down_node,
								    nlist,
								    flaglist,
								    clms_num_key_svcs);
				idelay(HZ/10);
			}
			/*
			 * If we got an error on the RPC, just post a nodedown
			 * indication and go on, unless it was -EREMOTE, which
			 * just indicates that the node is already down.
			 */
			if (error != 0 && error != -EREMOTE)
				clms_nodedown_ind(nodelist[i]);
			NSC_NODELIST_FREE(nlist);
		}
	}
	kfree(nodelist);
	for (i = 0; i < clms_num_key_svcs; i++)
		kfree(flags[i]);
	kfree(flags);
	return;
}

struct fail_thread_st {
	int             service;
	nsc_nodelist_t  *nlist;
};

static void
clms_key_svc_failover_thread(
	void *fail_threadp)
{
	struct fail_thread_st *fail_thread = fail_threadp;

	nsc_daemonize();

	INCR_MEMPRIO();			/* avoid blocking for memory */
	key_service_info[fail_thread->service].fail(fail_thread->nlist);
	kfree(fail_thread);
	exit_daemon_thread();
}

/*
 * clms_key_service_failover
 *	Call key service failover routines.
 *
 * Description:
 *	This routine is called by the rclms_reassign_key_services RPC.  If
 *	the key service that is to be failed over has a failover entry point,
 *	we spawn a thread for that key service, passing that thread the
 *	relevant information, including its own copy of the list of nodes
 *	that are up or half up, as passed to us from the CLMS master.
 *
 * Arguments:
 *	service		The key service to be failed over.
 *	down_node	The node that went down to cause this; this is the
 *			former key server for the key service.
 *	nlist		The list of up or halfup nodes passed to us from the
 *			CLMS master.
 */
void
clms_key_service_failover(
	int		service,
	clusternode_t	down_node,
	nsc_nodelist_t	*nlist)
{
	int error;
	nsc_nodelist_t *mynlist;
	struct fail_thread_st *fail_threadp;

	if (key_service_info[service].fail != NULL) {
		mynlist = NSC_NODELIST_COPY(nlist);
		if (!mynlist)
			panic("rclms_reassign_services: No Memory\n");
		if (down_node != CLUSTERNODE_INVAL)
			NSC_NODELIST_CLR1(mynlist, down_node);
		fail_threadp = kmalloc_nofail(sizeof(struct fail_thread_st));
		fail_threadp->service = service;
		fail_threadp->nlist = mynlist;
		smp_mb(); /* barrier ensures args visible to handler */
		do {
			error = spawn_daemon_thread(
					"key svc failover thread",
					clms_key_svc_failover_thread,
					(void *)fail_threadp);
		} while(error != 0);
	}
}

/*
 * clms_reassign_services_thread
 *	Local call to rclms_reassign_services().
 *
 * Description:
 *	This routine is run as a thread on the CLMS master to call
 *	rclms_reassign_services().  It is passed a clms_reassign_info_st
 *	which contains parameters for that routine.  Note that this
 *	routine need not free the nodelist within the clms_reassign_info_st,
 *	since that field is freed by rclms_reassign_services() itself.
 *
 * Arguments:
 *	argp		A pointer to a clms_reassign_info_st structure.
 */
void
clms_reassign_services_thread(
	void *argp)
{
	struct clms_reassign_info_st	*reassign_info = argp;

	nsc_daemonize();

	INCR_MEMPRIO();			/* avoid blocking for memory */
	(void)rclms_reassign_services(reassign_info->from_node,
				      reassign_info->down_node,
				      reassign_info->nlist,
				      reassign_info->flags,
				      clms_num_key_svcs);
	kfree(reassign_info);
	exit_daemon_thread();
}

/*
 * rclms_reassign_services
 *	Handle the RCLMS_REASSIGN_SERVICES message.
 *
 * Description:
 *	This routine serves the RCLMS_REASSIGN_SERVICES message.  It sets
 *	any key services newly assigned to this node as primary and not ready;
 *	if we're getting a floater, it sets that appropriately, as well.
 *	It then calls clms_key_service_failover() to spawn a thread to call
 *	the key service failover entry point.
 *
 * Arguments:
 *	to_node		This node.
 *	down_node	The node that went down to cause this; this is the
 *			former key server for the key service.
 *	nlist		The list of up or halfup nodes passed to us from the
 *			CLMS master.
 *	flags		An array indexed by key service indicating which
 *			key services we are receiving:
 *				0	Ignore the entry.
 *				1	We're getting the key service as a
 *					regular primary.
 *				2	We're getting the key service as a
 *					floater server.
 */
int
rclms_reassign_services(
	clusternode_t	from_node,
	clusternode_t	down_node,
	nsc_nodelist_t	*nlist,
	int		*flags,
	int		num_flags)
{
	int i;

#ifdef NSC_INHERIT_USER_NICE_XXX
	(void) set_daemon_prio(0, SCHED_NORMAL);
#endif
	for (i = 0; i < clms_num_key_svcs; i++) {
		SSI_ASSERT(flags[i] == 0 || flags[i] == 1 || flags[i] == 2);
		if (flags[i] != 0) {
#ifdef DEBUG
			printk(KERN_DEBUG
			       "rclms_reassign_services:  Got primary for %s.\n",
			       key_service_info[i].name);
#endif
			if (flags[i] == 1)
				clms_vset_cluster_svr(i,
						      this_node,
						      CLUSTER_PRIMARY_SERVER,
						      CLUSTER_SERVER_NOT_READY);
			else {	/* flags[i] == 2, so we use the floater */
				clms_vset_cluster_svr(i,
						      this_node,
						      CLUSTER_FLOATER_SERVER,
						      CLUSTER_SERVER_NOT_READY);
			}
			clms_key_service_failover(i, down_node, nlist);
		}
	}
	NSC_NODELIST_FREE(nlist);
	return(0);
}

/*
 * clms_get_key_servers()
 *	Get the list of key servers and their status for CLMS failover.
 *
 * Description:
 *	This gets the list of key servers and the status of each as known
 *	by this node, for CLMS failover.  Note that we can ignore the floater
 *	entries here, since when we are called the CLMS master is dead, so
 *	we'll have to select a new key server anyway.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	clusternode_t *servers	The list of servers.
 *	int *statuses		The status of each server.
 */
void
clms_get_key_servers(
	clusternode_t *servers,
	int *statuses,
	int *types)
{
	register int	i;
	cluster_server_t *clsp;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		servers[i] = CLUSTERNODE_INVAL;
		statuses[i] = -1;
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_PRIMARY_SERVER) {
				servers[i] = clsp->cls_node;
				statuses[i] = clsp->cls_status;
				types[i] = clsp->cls_type;
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
}

/*
 * clms_inform_key_nodes()
 *	Inform the key nodes that a new node is coming up.
 *
 * Description:
 *	NSC defines a few key services essential to operate a cluster.
 *	Examples are CLMS, ROOT FS, SYSTEM V IPC etc.  These key services
 *	are distributed, and the nodes providing these key services must
 *	be informed of all the nodes that are coming up.  This is so that
 *	they can allow connections from these nodes for limited purposes.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	clusternode_t new_node		the node that is coming up.
 */
int
clms_inform_key_nodes(
	clusternode_t new_node)
{
	icsinfo_t	new_icsinfo;
	int		error;
	int		flags;
	register int	i, j, num_servers = 0;
	clusternode_t	*servers;
	cluster_server_t *clsp;

	servers = kmalloc_nofail(sizeof(clusternode_t)
				 * (NSC_MAX_NODE_VALUE+1));
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if ((clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			     clsp->cls_type == CLUSTER_FLOATER_SERVER ||
			     clsp->cls_type == CLUSTER_SECONDARY_SERVER) &&
			    clsp->cls_node != this_node &&
			    clsp->cls_node != new_node) {
				for (j = 0; j < num_servers; j++)
					if (servers[j] == clsp->cls_node)
						break;
				if (j == num_servers)
					servers[num_servers++] =
							clsp->cls_node;
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	ics_geticsinfo(new_node, &new_icsinfo);
	for (i = 0; i < num_servers; i++) {
		if (new_node == servers[i])
			continue;	/* He knows he's up. */
		LOCK_COND_LOCK(&clms.clms_node_lock);
		flags = clms.clms_node_info[servers[i]].flags;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		if (flags & CLMS_NODE_INFORM_NOT_READY)
			continue;
		CLMSLOG(CLMSLOG_INFORMING,
			servers[i],
			flags,
			new_node,
			0, 0, 0, 0);
		error = RCLMS_INFORM_KEY_NODES_MSG(servers[i],
						   new_node,
						   &new_icsinfo);
		if (error != 0 && error != -EREMOTE) {
			printk(KERN_WARNING "clms_inform_key_nodes: RPC "
			       "RCLMS_INFORM_KEY_NODES() failed"
			       " error = %d\n", error);
		}
	}
	kfree(servers);
	return 0;
}

/*
 * clms_ready_key_nodes()
 *	Inform the key servers that a new node is ready for communication.
 *
 * Description:
 *	This routine tells the key servers informed by clms_inform_key_nodes()
 *	that a coming-up node is ready for RPCs.  This is called on the
 *	CLMS master when the coming-up node calls RCLMS_WANT_TO_JOIN().
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	clusternode_t new_node		the node that is coming up.
 */
void
clms_ready_key_nodes(
	clusternode_t new_node)
{
	int		error;
	int		flags;
	register int	i, j, num_servers = 0;
	clusternode_t	*servers;
	cluster_server_t *clsp;

	servers = kzmalloc_nofail(sizeof(clusternode_t)
				  * (NSC_MAX_NODE_VALUE+1));
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if ((clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			     clsp->cls_type == CLUSTER_FLOATER_SERVER ||
			     clsp->cls_type == CLUSTER_SECONDARY_SERVER) &&
			    clsp->cls_node != this_node &&
			    clsp->cls_node != new_node) {
				for (j = 0; j < num_servers; j++)
					if (servers[j] == clsp->cls_node)
						break;
				if (j == num_servers)
					servers[num_servers++] =
							clsp->cls_node;
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i = 0; i < num_servers; i++) {
		if (new_node == servers[i])
			continue;	/* He knows he's up. */
		LOCK_COND_LOCK(&clms.clms_node_lock);
		flags = clms.clms_node_info[servers[i]].flags;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		if (flags & CLMS_NODE_INFORM_NOT_READY)
			continue;
		CLMSLOG(CLMSLOG_READYING,
			new_node,
			servers[i],
			flags,
			0, 0, 0, 0);
		error = RCLMS_READY_KEY_NODES_MSG(servers[i],
						  new_node);
		if (error != 0 && error != -EREMOTE) {
			printk(KERN_WARNING "clms_ready_key_nodes: RPC "
			       "RCLMS_READY_KEY_NODES() failed"
			       " error = %d\n", error);
		}
	}
	kfree(servers);
}

/*
 * clms_set_key_secondary_ready()
 *	Inform the clms that a key service has completed initialization and is
 *	ready for use as a secondary.
 *
 * Description:
 *	This function is called from a key service to inform CLMS that the
 *	key service has completed enough initialization to be ready take over
 *	the service from a failed server.
 *
 *	If the running node is not a secondary server for the key service, the
 *	call is ignored.
 *
 *	If the running node is the CLMS master, inform all nodes that are
 *	waiting for information.  If the running node is not the CLMS master,
 *	the CLMS master node is informed.  The CLMS master, in turn, will
 *	inform all the other nodes in the cluster.
 *
 * Arguments:
 *	node	The node for which to mark secondaries up.
 *
 * Return value:
 *	None.
 */
void
clms_set_key_secondary_ready(
	int service)
{
	int type;
	int error;

	/*
	 * Look up the key service on this node.  If it's a secondary, mark it
	 * up and tell other nodes, else ignore it.
	 */
	type = clms_get_cluster_svr_type(service, this_node);
	CLMSLOG(CLMSLOG_SET2ND, service, type, 0, 0, 0, 0, 0);
	if (type == CLUSTER_SECONDARY_SERVER) {
		/*
		 * Mark the key service as up and inform the master (if we
		 * aren't him) or all the nodes in the cluster (if we are).
		 */
		clms_set_cluster_svr_status(service,
					    this_node,
					    CLUSTER_SECONDARY_UP);
		if (this_node == clms_master_node) {
			clms_send_cluster_svrinfo(this_node,
				  	  	 service,
				  	  	 type,
				  	  	 CLUSTER_SECONDARY_UP,
				  	  	 CLMS_NODE_HALF_UP|CLMS_NODE_UP
							|CLMS_NODE_COMING_UP);
		}
		else {
			error = RCLMS_NEW_CLUSTER_SVRINFO_MSG(clms_master_node,
						 	      this_node,
						 	      service,
						 	      type,
						 	      CLUSTER_SECONDARY_UP);
			if (error) {
				printk(KERN_WARNING "clms_send_cluster_svrinfo: "
				       " ICS MSG failed for node %u"
				       " error %d \n",
				       clms_master_node, error);
			}
		}
	}
}

/*
 * clms_key_service_nodedown()
 *	Handle nodedown for key services.
 *
 * Description:
 *	Called on clients.  If the passed node is a primary or secondary, mark
 *	it as NOT_A_SERVER.  For each reassigned key service, if the "reassign"
 *	flag is set, mark the entry for the new server as the primary, and
 *	mark the status as "not ready."  If we raced with a reassign or with
 *	RCLMS_NEW_CLUSTER_SVRINFO_MSG, we'll either win, get the lock, find
 *	the secondary/floater and fill it in, or we'll lose the race to the
 *	lock, clms_vset_cluster_svr() will get it and will fill in the entry,
 *	which we will ignore since there's no work to do.
 *
 * Arguments:
 *	node		The node that went down.
 *	servers		The node to which each key service is assigned.
 *			Entries may be CLUSTERNODE_INVAL if there is no server
 *			for a key service.  Entries may be unchanged relative
 *			to the information already in the database if that
 *			server didn't die.
 *	reassign	Flag indicating whether to actually reassign any
 *			key services; if nodedown messages arrive out of order,
 *			this flag may be zero.
 */
void
clms_key_service_nodedown(
	clusternode_t	down_node,
	clusternode_t	*servers,
	int		reassign)
{
	register int i;
	register cluster_server_t *clsp;
	int found[CLMS_MAX_KEY_SERVICES];

	memset(found, 0, sizeof(found));
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		if (reassign != 0)
			key_service_info[i].server = servers[i];
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			/*
			 * If we found the down node, clear the entry.
			 */
			if (clsp->cls_node == down_node) {
				if (clsp->cls_type == CLUSTER_FLOATER_SERVER) {
					clsp->cls_type = CLUSTER_FLOATER_ENTRY;
					clsp->cls_node = CLUSTERNODE_INVAL;
					clsp->cls_status = CLUSTER_SERVER_NOT_READY;
				}
				else
					clsp->cls_type = CLUSTER_NOT_A_SERVER;
			}
			/*
			 * If we found an entry for a server, and there _is_
			 * a new server (not CLUSTERNODE_INVAL), and it was a
			 * secondary, and we're reassigning, do the reassign.
			 */
			if (clsp->cls_node == servers[i] &&
			    servers[i] != CLUSTERNODE_INVAL &&
			    clsp->cls_type == CLUSTER_SECONDARY_SERVER &&
			    reassign != 0) {
				clsp->cls_type = CLUSTER_PRIMARY_SERVER;
				clsp->cls_status = CLUSTER_SERVER_NOT_READY;
			}
			/*
			 * Remember that we found a server for this key
			 * service.
			 */
			if (clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			    clsp->cls_type == CLUSTER_FLOATER_SERVER)
				found[i] = 1;
			clsp = clsp->cls_next;
		}
	}
	/*
	 * Nothing more to do if we aren't reassigning anything.
	 */
	if (reassign == 0) {
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		return;
	}
	/*
	 * If there is a server for the key service but we didn't find it above,
	 * then it must be a new server and it must be the CLMS master taking
	 * over for a dead server.  Set the floater entry accordingly, unless
	 * it has already been set up for that server.  If the down_node was
	 * a floater server, the entry was reverted to a floater_entry above;
	 * if it's not a floater_entry, it was set by clms_vset_cluster_svr(),
	 * so we only set it if it's a floater_entry.
	 */
	for (i = 0; i < clms_num_key_svcs; i++) {
		if (servers[i] != CLUSTERNODE_INVAL && found[i] == 0) {
			clsp = clms.clms_cluster_floater[i];
			if (clsp->cls_type == CLUSTER_FLOATER_ENTRY) {
				clsp->cls_type = CLUSTER_FLOATER_SERVER;
				clsp->cls_node = servers[i];
				clsp->cls_status = CLUSTER_SERVER_NOT_READY;
			}
		}
	}
	/*
	 * Make sure that anyone waiting for a server wakes up.
	 */
	BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
}

/*
 * clms_set_cluster_server()
 *	Store information about a key service provided by a node.
 *
 * Description:
 *	This is a routine that is used to maintain the database that
 *	is kept of what CLMS key services are provided on which nodes.
 *
 *	This database is maintained in-memory on both CLMS master and
 *	client nodes. The routine is used internally within CLMS.
 *
 *	This routine is used to indicate when a node is a primary
 *	server, when it is a secondary server, and when a node is
 *	no longer providing a key service.
 *
 * Arguments:
 *	svc		the CLMS key service being provided
 *	node		the node that is/was providing the key service
 *	type		one of CLUSTER_PRIMARY_SERVER
 *				(the primary provider of the key service)
 *			CLUSTER_SECONDARY_SERVER
 *				(a secondary provider of the key service)
 *			CLUSTER_NOT_A_SERVER
 *				(is no longer providing the key service)
 *	flag		status of server  either CLUSTER_SERVER_NOT_READY or
 *			CLUSTER_SERVER_READY
 *
 */
int
clms_set_cluster_server(
	int		svc,
	clusternode_t	node,
	int		type,
	int		status)
{
	cluster_server_t	*cls;

	cls = (cluster_server_t *)kzmalloc_nofail(sizeof(cluster_server_t));
	cls->cls_svc = svc;
	cls->cls_node = node;
	cls->cls_type = type;
	cls->cls_status = status;
	cls->cls_next = NULL;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	clms_ins_cluster_server_l(cls);
	/*
	 * Process the cluster server info and do any updates necessary.
	 */
	if (type == CLUSTER_PRIMARY_SERVER)
		key_service_info[svc].server = node;
	if (type == CLUSTER_FLOATER_ENTRY)
		clms.clms_cluster_floater[svc] = cls;
	if (status == CLUSTER_SERVER_READY)
		CLMS_CALL_SERVER_READY(svc, node);
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);

	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_cluster_stat.clms_num_cluster_servers++;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	return 0;
}

/*
 * clms_ins_cluster_server_l()
 *	Insert a cluster server entry in the clms structure.
 *
 * Description:
 *	This function inserts a cluster server entry in the linked list
 *	servers.
 *
 * Calling/Exit State:
 *	The caller has the responsibility for all locking.
 *
 * Parameters:
 *	cluster_server_t	*cls - pointer to cluster server data
 */
int
clms_ins_cluster_server_l(cluster_server_t *cls)
{
	cluster_server_t *tmp;

	tmp  = clms.clms_cluster_server[cls->cls_svc];
	if (!tmp) {
		clms.clms_cluster_server[cls->cls_svc] = cls;
	} else {
		while (tmp->cls_next) {
			tmp = tmp->cls_next;
		}
		tmp->cls_next = cls;
	}

	return 0;
}


/*
 * clms_vset_cluster_svr()
 *	Set/Update the cluster server entry in the local data base.
 *
 * Description:
 *	This function update the status and type of a cluster server entry
 *	in the local data base. If one does not exist, it creates the entry.
 *
 * Parameters:
 *	int	 	service		- the key service
 *	clusternode_t 	server_node	- the server providing the key service
 *	int 		svr_type	- the type of server (primary/secondary)
 *	int 		svr_status	- the status of server (ready or not)
 *
 */
int
clms_vset_cluster_svr(int service,
		      clusternode_t server_node,
		      int svr_type,
		      int svr_status)
{
	cluster_server_t *cls, *clsp;
	int dofree;
	dofree = 0;
	cls = NULL;
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	/*
	 * Find the secondary entry for the node, or the floater entry or
	 * floater server if we're making the node a floater server.
	 */
	clsp = clms.clms_cluster_server[service];
	while (clsp) {
		if (clsp->cls_node == server_node ||
		    (clsp->cls_type == CLUSTER_FLOATER_SERVER &&
		     svr_type == CLUSTER_FLOATER_SERVER) ||
		    (clsp->cls_type == CLUSTER_FLOATER_ENTRY &&
		     svr_type == CLUSTER_FLOATER_SERVER))
			break;
		clsp = clsp->cls_next;
	}
	if (clsp == NULL) {
		/*
		 * If there wasn't one, unlock and allocate one, then relock
		 * and see if one appeared.
		 */
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		cls = (cluster_server_t *)kzmalloc_nofail(
			sizeof(cluster_server_t));
		LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		clsp = clms.clms_cluster_server[service];
		while (clsp) {
			if (clsp->cls_node == server_node)
				break;
			if (svr_type == CLUSTER_FLOATER_SERVER &&
			    clsp->cls_type == CLUSTER_FLOATER_ENTRY)
				break;
			clsp = clsp->cls_next;
		}
	}
	/*
	 * If we found an entry for the node, or if we found the floater
	 * entry, update it.  Else insert the new entry.
	 */
	if (clsp) {
		dofree = 1;
		if (clsp->cls_type == CLUSTER_FLOATER_ENTRY) {
			clsp->cls_node = server_node;
			if (svr_type == CLUSTER_FLOATER_SERVER)
				key_service_info[service].server = server_node;
		}
		/*
		 * We might haved raced with clms_set_key_secondary_ready(),
		 * so don't set the status if it's already CLUSTER_SECONDARY_UP
		 * for the secondary node in question.
		 */
		if (clsp->cls_type != CLUSTER_SECONDARY_SERVER ||
		    clsp->cls_status != CLUSTER_SECONDARY_UP ||
		    svr_status != CLUSTER_SERVER_NOT_READY) {
			clsp->cls_type = svr_type;
			if (svr_status == CLUSTER_SERVER_READY &&
			    clsp->cls_status != CLUSTER_SERVER_READY)
				CLMS_CALL_SERVER_READY(service, server_node);
			clsp->cls_status = svr_status;
		}
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	}
	else {
		cls->cls_svc = service;
		cls->cls_node = server_node;
		cls->cls_type = svr_type;
		cls->cls_status = svr_status;
		cls->cls_next = NULL;
		clms_ins_cluster_server_l(cls);
		if (svr_type == CLUSTER_PRIMARY_SERVER ||
		    svr_type == CLUSTER_FLOATER_SERVER)
			key_service_info[service].server = server_node;
		if (svr_type == CLUSTER_FLOATER_ENTRY)
			clms.clms_cluster_floater[service] = cls;
		if (svr_status == CLUSTER_SERVER_READY)
			CLMS_CALL_SERVER_READY(service, server_node);
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		LOCK_COND_LOCK(&clms.clms_node_lock);
		clms.clms_cluster_stat.clms_num_cluster_servers++;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
	}
	BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);
	if (dofree && cls != NULL)
		kfree(cls);
	return(0);
}

/*
 * clms_set_cluster_svr_status()
 *	Set the status of the cluster server.
 *
 * Description:
 *	Sets the status of the key server/service pair. If key service is
 *	not specified, status is applied to all the key services provided
 *	by the node.
 *
 *	All processes waiting for the condition are signaled.
 *
 *	Status specifies ready or not.
 *
 * Parameters:
 *	int		service		key service
 *	clusternode_t	node		server node number
 *	int		status		status
 */
int
clms_set_cluster_svr_status(int service, clusternode_t node, int status)
{
	cluster_server_t *clsp;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	clsp = clms.clms_cluster_server[service];
	while (clsp) {
		if (clsp->cls_node == node) {
			if (status == CLUSTER_SERVER_READY &&
			    clsp->cls_status != CLUSTER_SERVER_READY)
				CLMS_CALL_SERVER_READY(service, node);
			clsp->cls_status = status;
			break;
		}
		clsp = clsp->cls_next;
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);

	return(0);
}

/*
 * clms_set_cluster_svr_type()
 *	Set the type of the cluster server.
 *
 * Description:
 *	Sets the type of the server for key service. If key service is
 *	not specified, status is applied to all the key services provided
 *	by the node.
 *
 *	All processes waiting for the condition are signaled.
 *
 *	Type specifies Primary/Secondary/Not providing the service
 *
 * Parameters:
 *	int		service		service
 *	clusternode_t	node		server node number
 *	int		type		type of server
 *
 * Currently unused and commented out.
 *
static int
clms_set_cluster_svr_type(int service, clusternode_t node, int type)
{
	cluster_server_t *clsp;
	int i;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	clsp = clms.clms_cluster_server[service];;
	while (clsp) {
		if (clsp->cls_node == node) {
			clsp->cls_type = type;
			break;
		}
		clsp = clsp->cls_next;
	}
	if (type == CLUSTER_PRIMARY_SERVER ||
	    type == CLUSTER_FLOATER_SERVER)
		key_service_info[service].server = node;
	BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	return(0);
}
 */

/*
 * clms_handle_cluster_svr_timeout()
 *      Processing routine for key service wait timeout.
 *
 * Description:
 *      This routine gets called from the async daemon if the timeout
 *	goes off while the master is waiting for key services to announce
 *	themselves.  It checks the database and if there aren't primaries
 *	for all key services needed for boot, complains to the console.
 *	If the configurable parameter clms_service_timeout_use_2nd is set
 *	and secondaries are available for the key services lacking primaries,
 *	those are used.  If there are several secondaries for a key service,
 *	it uses the one with the lowest node number.  Otherwise it just
 *	returns.
 */
void
clms_handle_cluster_svr_timeout(void *none)
{
	int service, found_noprimary, nosecond, found_second, used_master;
	int present[CLMS_MAX_KEY_SERVICES];
	cluster_server_t *secondary[CLMS_MAX_KEY_SERVICES];
	cluster_server_t *clsp, *savep;

	found_noprimary = nosecond = found_second = 0;
	/*
	 * Lock out RCLMS_OFFER_SERVICE.
	 */
	LOCK_LOCK(&clms_key_service_lock);
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (service = 0; service < clms_num_key_svcs; service++) {
		clsp = clms.clms_cluster_server[service];
		present[service] = 0;
		secondary[service] = NULL;
		while (clsp) {
			if (clsp->cls_type == CLUSTER_PRIMARY_SERVER)
				present[service] = 2;
			else if (clsp->cls_type == CLUSTER_SECONDARY_SERVER &&
				 present[service] == 0) {
				if (secondary[service] == NULL ||
				    secondary[service]->cls_node < clsp->cls_node)
					secondary[service] = clsp;
				present[service] = 1;
				found_second = 1;
			}
			clsp = clsp->cls_next;
		}
		if (present[service] < 2)
			found_noprimary = 1;
		if (present[service] == 0)
			nosecond = 1;
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	/*
	 * If all key services had a primary, we went off after or during
	 * a clms_set_cluster_svr_info() call, which will have woken
	 * clms_waitfor_key_service_info(), so we don't have to.
	 */
	if (found_noprimary == 0) {
		UNLOCK_LOCK(&clms_key_service_lock);
		return;
	}
	if (nosecond == 0 && clms_service_timeout_use_2nd != 0) {
		/*
		 * We couldn't find primaries for all key services, but we did
		 * find secondaries for all key services, so we want to use
		 * those secondaries.  Mark the erstwhile secondaries as
		 * primaries and wake up clms_waitfor_key_service_info().
		 */
		LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		for (service = 0; service < clms_num_key_svcs; service++) {
			if (present[service] == 1) {
				clsp = secondary[service];
				clsp->cls_type = CLUSTER_PRIMARY_SERVER;
				key_service_info[service].server = clsp->cls_node;
			}
		}
		BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		UNLOCK_LOCK(&clms_key_service_lock);
		printk(KERN_INFO
		       "WARNING: CLMS timed out waiting for services:");
		for (service = 0; service < clms_num_key_svcs; service++)
			if (present[service] == 1 || present[service] == 0)
				printk(KERN_INFO " %s", key_service_info[service].name);
		printk(KERN_INFO "\n");
		printk(KERN_WARNING "Using secondary for those services.\n");
		return;
	}
	/*
	 * We couldn't find either primaries or secondaries for all
	 * key services.  Use those secondaries we found, and use the
	 * master (this_node) for the rest.  Note that we use the
	 * floater entry here, since we're falling back to the
	 * CLMS master.
	 */
	if (found_second) {
		LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		for (service = 0;
		     service < clms_num_key_svcs; service++) {
			if (present[service] == 1) {
				clsp = secondary[service];
				clsp->cls_type = CLUSTER_PRIMARY_SERVER;
				key_service_info[service].server = clsp->cls_node;
			}
		}
		BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	}
	used_master = 0;
	for (service = 0; service < clms_num_key_svcs; service++) {
		if (present[service] == 0 &&
		    key_service_info[service].crit == 0) {
			if ((used_master & 1) == 0)
				used_master |= 1;
			LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
			savep = clms.clms_cluster_floater[service];
			savep->cls_node = this_node;
			savep->cls_type = CLUSTER_FLOATER_SERVER;
			key_service_info[service].server = this_node;
			BROADCAST_CONDITION(&clms.clms_cluster_svr_cond);
			UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		}
		if (present[service] == 0 &&
		    key_service_info[service].crit != 0 &&
		    (used_master & 2) == 0)
			used_master |= 2;
	}
	UNLOCK_LOCK(&clms_key_service_lock);
	printk(KERN_INFO
	       "WARNING: CLMS timed out waiting for services:");
	for (service = 0; service < clms_num_key_svcs; service++)
		if (present[service] == 1 ||
		    present[service] == 0)
			printk(KERN_INFO
			       " %s",
			       key_service_info[service].name);
	printk(KERN_INFO "\n");
	if (found_second) {
		printk(KERN_INFO "WARNING: Using secondary for:");
		for (service = 0;
		     service < clms_num_key_svcs; service++)
			if (present[service] == 1)
				printk(KERN_INFO
				       " %s",
				       key_service_info[service].name);
		printk(KERN_INFO "\n");
	}
	if (used_master & 1) {
		printk(KERN_INFO "WARNING: Using the master for:");
		for (service = 0; service < clms_num_key_svcs; service++)
			if (present[service] == 0 &&
		    	key_service_info[service].crit == 0)
				printk(KERN_INFO
				       " %s",
				       key_service_info[service].name);
		printk(KERN_INFO "\n");
	}
	if (used_master & 2) {
		printk(KERN_INFO "WARNING: No server for:");
		for (service = 0; service < clms_num_key_svcs; service++)
			if (present[service] == 0 &&
			    key_service_info[service].crit != 0)
				printk(KERN_INFO
				       " %s",
				       key_service_info[service].name);
		printk(KERN_INFO "\n");
	}
	return;
}

void
clms_waitfor_key_service_info(void)
{
	int	service;

#ifdef DEBUG
	printk(KERN_DEBUG "\nWaiting for server for all key services\n");
#endif
	for (service = 0; service < clms_num_key_svcs; service++)
		clms_waitfor_cluster_svr_info(service);
}

/*
 * clms_get_cluster_svr_type()
 *	Get the type of the cluster server, given the key service and the
 *	node.
 *
 * Description:
 *	Gets the type of the server for a key service/node combination.
 *	If the key service is not found for the node, CLUSTER_NOT_A_SERVER
 *	is returned.
 *
 * Parameters:
 *	int		service		key service to find
 *	clusternode_t	node		node number of server
 */
int
clms_get_cluster_svr_type(int service, clusternode_t node)
{
	cluster_server_t *clsp;
	int type;

	type = CLUSTER_NOT_A_SERVER;
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	clsp = clms.clms_cluster_server[service];;
	while (clsp) {
		if (clsp->cls_node == node) {
			type = clsp->cls_type;
			break;
		}
		clsp = clsp->cls_next;
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	return(type);
}

/*
 * clms_process_cluster_svrinfo()
 *	Validate and store cluster server information (from a new node).
 *
 * Description:
 *	This function is primarily used to validate the cluster server
 * 	information from a new node joining the cluster. If the information
 * 	is valid, it is stored in the local cluster servers data base.
 * 	Otherwise, an error is returned.
 *
 *	Each key service will already in be the database; this should
 *	assert same.  The cls_type should match (another assertion)
 *	and the status may differ (the server may have become
 *	ready).
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	node		- the node that is offering the key services
 * 	svclist 	- list of key services provided by the node.
 *	svr_typelist 	- the type of server w.r.t each key service
 *				(primary/secondary/none)
 *	svr_statuslist 	- the status of server w.r.t each key service
 *				(ready/not ready)
 *	num_svcs	 - Number of Services (i.e. number of entries in each
 *				list)
 *
 * NSC_XXX:
 *	The server information must be passed in a structure.
 *
 */
int
clms_process_cluster_svrinfo(clusternode_t node, int *svclist,
			     int *svr_typelist, int *svr_statuslist,
			     int num_svcs)
{

	int	i, forwards;
	int	forward_info[CLMS_MAX_KEY_SERVICES];
	int	node_state;
	cluster_server_t *clsp;

	memset(forward_info, 0, sizeof(forward_info));
	forwards = 0;
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i = 0; i < num_svcs; i++) {
		clsp = clms.clms_cluster_server[svclist[i]];
		while (clsp) {
			if (clsp->cls_node == node)
				break;
		}
		SSI_ASSERT(clsp != NULL);
		SSI_ASSERT(svr_typelist[i] == clsp->cls_type);
		if (svr_statuslist[i] != clsp->cls_status) {
			forward_info[forwards++] = i;
			clsp->cls_status = svr_statuslist[i];
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	/*
	 * Inform everyone else, if necessary.
	 */
	node_state = CLMS_NODE_HALF_UP | CLMS_NODE_UP | CLMS_NODE_COMING_UP;
	for (i = 0; i < forwards; i++) {
		clms_send_cluster_svrinfo(node,
					  svclist[forward_info[i]],
		  	  		  svr_typelist[forward_info[i]],
					  svr_statuslist[forward_info[i]],
					  node_state);
	}
	return(0);
}

/*
 * clms_waitfor_cluster_svr_info()
 *	Wait for a cluster key service information (who is providing it).
 *
 * Description:
 *	This function waits for information about a key service to become
 *	available. However, it does not wait for the key service to become
 *	ready for use.
 *
 * Parameters:
 *	int	service		key service for which to wait
 */
int
clms_waitfor_cluster_svr_info(int service)
{
	cluster_server_t	*clsp;

	SSI_ASSERT(service >= 0 && service <= CLMS_LAST_KEY_SERVICE);
	/*
	 * Wait for information about a key service.
	 */
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (; ;) {
		clsp = clms.clms_cluster_server[service];
		while (clsp &&
		       clsp->cls_type != CLUSTER_PRIMARY_SERVER &&
		       clsp->cls_type != CLUSTER_FLOATER_SERVER)
			clsp = clsp->cls_next;
		if (clsp || clms_all_svcs_assigned != 0)
			break;
		WAIT_CONDITION(	&clms.clms_cluster_svr_cond,
				&clms.clms_cluster_svr_lock);
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	return 0;
}


/*
 * clms_waitfor_key_service()
 *	Wait for a key service to be ready.
 *
 * Description:
 *	This function first waits for a key service to be known. That is
 *	it waits until somebody in the cluster is willing to offer the
 *	Service. It then waits for the key service to be ready. Note
 *	that the server node need not be fully up even when the key
 *	service it offers is ready.
 *
 * Parameters:
 *	int	service	- key service to wait for
 */
int
clms_waitfor_key_service(
	int service)
{
	cluster_server_t	*clsp;

	SSI_ASSERT(service >= 0 && service <= CLMS_LAST_KEY_SERVICE);
	/*
	 * Wait until the service is ready.
	 */
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (; ;) {
		clsp = clms.clms_cluster_server[service];
		while (clsp) {
			if ((clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			     clsp->cls_type == CLUSTER_FLOATER_SERVER) &&
	  		    clsp->cls_status == CLUSTER_SERVER_READY)
				break;
			clsp = clsp->cls_next;
		}
		if (clsp)
			break;
		WAIT_CONDITION(&clms.clms_cluster_svr_cond,
			       &clms.clms_cluster_svr_lock);
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	return 0;
}

/*
 * clms_key_service_node_lock()
 *	Lock the key service node lock for a key service.
 *
 * Description:
 *	Locks the key service node lock on behalf of a key service.
 *
 *	This is currently implemented using the clms_key_service_node_lock;
 *	the parameter is ignored.
 *
 * Parameters:
 *	int	service		key service to lock
 */
void
clms_key_service_node_lock(
	int service)
{
	LOCK_SPIN_LOCK(&clms.clms_key_service_node_lock);
}

/*
 * clms_key_service_node_unlock()
 *	Unlock the key service node lock for a key service.
 *
 * Description:
 *	Unlocks the key service node lock on behalf of  a key service.
 *
 *	This is currently implemented using the clms_key_service_node_lock;
 *	the parameter is ignored.
 *
 * Parameters:
 *	int	service		key service to unlock
 */
void
clms_key_service_node_unlock(
	int service)
{
	UNLOCK_SPIN_LOCK(&clms.clms_key_service_node_lock);
}

/*
 * Routines to deal with CLMS Services like ICS, VPROC, CFS, SSIDEV etc.
 */
/*
 * clms_get_svcband()
 *	Return the subsystem band structure for the given priority; allocate
 *	it if needed.
 *
 * Description:
 *	This function gets/allocates a band structures for a given priority.
 *	The clms subsystems specify nodeup(), nodedown() routines to be
 *	called when the event occurs. The subsystems also specify priorities.
 *	All subsystems of the same priority will be called simultaneously.
 *	Services of a particular priority will be called only after the
 *	subsystems of higher priorities have completed their processing. 0 is
 *	the highest priority.
 *
 * Parameters:
 *	pri		The subsystem priority.
 */

struct clms_sband *
clms_get_svcband(int pri)
{

	struct clms_sband **svcbpp;
	struct clms_sband *newp;

	svcbpp = &(clms.clms_svc.cs_first);

	/* A semaphore is held in clms_svc_register to protect this. */
	while (*svcbpp != NULL) {
		if ((*svcbpp)->sb_pri == pri)
			goto out;
		if ((*svcbpp)->sb_pri > pri)
			break;
		svcbpp = &(*svcbpp)->sb_next;
	}
	/* Band not found: allocate a new one. */
	newp = kzmalloc(sizeof(*newp), GFP_KERNEL);
	if (newp == NULL)
		panic("No memory for CLMS Services\n");
	newp->sb_pri = pri;
	newp->sb_next = *svcbpp;
	*svcbpp = newp;
 	clms.clms_svc.cs_nbands++;
out:
	return *svcbpp;
}

/*
 * clms_alloc_cluster_svrinfo()
 *	Allocate space for storing all the information about cluster key
 *	services.
 *
 * Description:
 *	This function allocates space for storing storing all the information
 *	about key services. The information include node number,
 *	key service number, type of server, and status of key server/service.
 *
 *	The information can be requested for a particular node or for
 *	the entire cluster.
 *
 *	Key services are those services which are critical for the
 *	successful operation of the cluster. Examples are ROOT FS, CLMS,
 *	INIT etc.
 *
 *	This function has been designed to facilitate the reuse of space.
 *
 *	NOTE:
 *	The parameters may have to be a structure.
 *
 * Parameters:
 *	clusternode_t *node 	- pointer to node (null for entire cluster)
 *	int	      **svclist - pointer to array of key services
 *	clusternode_t **svrlist - pointer to array of corresponding key servers
 *	int    **svr_typelist  	- pointer to array of corresponding types
 *	int    **svr_statuslist - pointer to array of corresponding statuses
 *	int    *num_entries	- pointer to size of the above arrays
 *
 *  Returns:
 *	On success returns 0. On failure the corresponding error code
 *	is returned.
 */
int
clms_alloc_cluster_svrinfo(clusternode_t *node,
			   int    **svclist,
			   clusternode_t **svrlist,
			   int    **svr_typelist,
			   int    **svr_statuslist,
			   int    *num_entries)
{
	cluster_server_t	*clsp;
	int			svc_count;
	int			i;

	/*
	 * Determine the number of entries in the cluster key service database.
 	 */
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	svc_count = 0;
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_NOT_A_SERVER) {
				clsp = clsp->cls_next;
				continue;
			}
			if (node) {
				if (clsp->cls_node == *node) {
					svc_count++;
					break;
				}
			} else {
				svc_count++;
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);

	/*
	 * Make sure we have room for the floaters.
	 */
	svc_count += clms_num_key_svcs;

	/*
	 * Allocate space (if necessary) for storing all the information.
 	 */
	if (*num_entries < svc_count) {
		if (*svclist) {
			kfree((caddr_t)*svclist);
		}
		if (*svrlist) {
			kfree((caddr_t)*svrlist);
		}
		if (*svr_typelist) {
			kfree((caddr_t)*svr_typelist);
		}
		if (*svr_statuslist) {
			kfree((caddr_t)*svr_statuslist);
		}

		*num_entries = max(16, svc_count);

		*svclist = (int *)
			   kzmalloc_nofail((*num_entries)*sizeof(int));
		*svrlist = (clusternode_t *)
			kzmalloc_nofail((*num_entries)*sizeof(clusternode_t));
		*svr_typelist = (int *)
			kzmalloc_nofail((*num_entries)*sizeof(int));
		*svr_statuslist = (int *)
			kzmalloc_nofail((*num_entries)*sizeof(int));
	}

	return 0;
}

/*
 * clms_get_cluster_svrinfo()
 *	Get all the information about cluster key services.
 *
 * Description:
 *	This function gets all the information about key services.
 *	The information include node number, key service number,
 *	type of server, and status of key server/service.
 *
 *	The information can be requested for a particular node or for
 *	the entire cluster.
 *
 *	Key services are those services which are critical for the
 *	successful operation of the cluster. Examples are ROOT FS, CLMS,
 *	INIT etc.
 *
 *	The space is preallocated.
 *
 *	NOTE:
 *	The parameters may have to be a structure.
 *
 * Parameters:
 *	clusternode_t *node 	- pointer to node (null for entire cluster)
 *	int    *svclist  	- pointer to key services
 *	clusternode_t *svrlist 	- pointer to corresponding key servers
 *	int    *svr_typelist  	- pointer to corresponding types
 *	int    *svr_statuslist  - pointer to corresponding statuses
 *	int    num_entries	- size of the above arrays
 *
 * Returns:
 *	On success returns the number of entries in the list.
 *	On failure returns a -1.
 */
int
clms_get_cluster_svrinfo(clusternode_t	*node,
			 int *svclist,
			 clusternode_t *svrlist,
			 int *svr_typelist,
			 int *svr_statuslist,
			 int num_entries)
{
	cluster_server_t	*clsp;
	int			svc_count;
	int			i;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	svc_count = 0;
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_NOT_A_SERVER) {
				clsp = clsp->cls_next;
				continue;
			}
			if (node) {
				if (clsp->cls_node == *node) {
					svclist[svc_count] = i;
					if (svrlist) {
						svrlist[svc_count]
							= clsp->cls_node;
					}
					svr_typelist[svc_count]=clsp->cls_type;
					svr_statuslist[svc_count]
							 = clsp->cls_status;
					svc_count++;
					if (svc_count == num_entries) {
						UNLOCK_COND_LOCK(
						  &clms.clms_cluster_svr_lock);
						return svc_count;
					}
					break;
				}
			} else {
				svclist[svc_count] = i;
				svrlist[svc_count] = clsp->cls_node;
				svr_typelist[svc_count]=clsp->cls_type;
				svr_statuslist[svc_count] = clsp->cls_status;
				svc_count++;
				if (svc_count == num_entries) {
					UNLOCK_COND_LOCK(
						&clms.clms_cluster_svr_lock);
					return svc_count;
				}
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);

	return svc_count;
}

/*
 * clms_is_cluster_svr_set()
 *	Determine if a key server/service pair is set in the cluster
 *	key service database.
 *
 * Description:
 *	This function returns a 1 if the given key server/service pair
 *	is found in the cluster key service database. It returns a 0
 *	otherwise.
 *
 * NOTE:
 *	A third parameter may have to be added to indicate the type of
 *	key server.
 *
 * Parameters:
 *	int		svc	- key service number
 *	clusternode_t	node	- node number
 *
 * Returns:
 *	1 if found, 0 otherwise.
 */
int
clms_is_cluster_svr_set(int svc, clusternode_t	node)
{
	cluster_server_t	*clsp;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	clsp = clms.clms_cluster_server[svc];
	while (clsp) {
		if (clsp->cls_node == node)
			break;
		clsp = clsp->cls_next;
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	if (clsp)
		return(1);
	return(0);
}

/*
 * clms_call_svcfunc()
 *	Call a procedure of a registered clms subsystem.
 *
 * Description:
 *	Components of NSC register with clms. The registered functions
 *	include nodeup() and node_down() procedures.  These procedures
 *	will be called by the clms at the appropriate time. CLMS calls
 *	these procedures according to the priority specified. The functions
 *	at the same priority levels may be called simultaneously.
 *	The CLMS, then, waits for the functions to complete execution.
 *
 *	Subsystems with band priority -1 have no dependencies on other
 *	subsystems and therefore are executed in parallel with all other
 *	priority band processing.
 *
 *	This function calls the registered procedure specified in the
 *	parameter. The procedure called can be one of nodeup() and nodedown().
 *	The called function is also marked in the clms_transition_data
 *	structure. The CLMS will wait for the called procedure to complete
 *	processing. The called procedure will call back the clms.
 *
 * Calling/Exit State:
 *	Static function.
 *
 * Parameters:
 *	struct clms_transition_date	*cdp	- pointer to clms transition
 *						  structure
 *	int				flag	- flag indicates what type
 *						  of procedure to call
 *						  (nodeup(), nodedown())
 *	struct clms_svc_data		*svcinfo - The structure giving
 *						   all the information about
 *						   the registered subsystem.
 *
 * Returns:
 *	0 on success and an error code on failure.
 */
static int
clms_call_svcfunc(struct clms_transition_data *cdp,
		  int			       flag,
		  struct clms_svc_data        *svcinfo)
{
	clusternode_t	node;
	clusternode_t	surrogate_node;
	int 		error;
	char 		*what;
	int		(*function)(void *, clms_subsys_t, clusternode_t, clusternode_t, void *);
	clms_subsys_t	service;
	int		*callbacks;

	error = 0;
	service = svcinfo->s_sinfo->service;
	node = cdp->node;
	surrogate_node = cdp->surrogate_node;
	if (svcinfo->s_pri == -1)
		callbacks = cdp->transition_callbacks2;
	else
		callbacks = cdp->transition_callbacks;

	switch (flag) {
		case CLMS_CALL_NODEUP:
			what = " nodeup function ";
			function = svcinfo->s_sinfo->subsys_nodeup;
			if (function) {
				callbacks[service] =
					CLMS_CALLED_FUNCTION;
				error = (*function)((void *)cdp,
						    service,
						    node,
					            surrogate_node,
						    (void *)NULL);
				if (error) {
					callbacks[service] =
						CLMS_FUNCTION_CALLED_BACK;
				}
			}
			break;

		case CLMS_CALL_NODEDOWN:
			what = " nodedown function ";
			function = svcinfo->s_sinfo->subsys_nodedown;
			if (function) {
				callbacks[service] =
					CLMS_CALLED_FUNCTION;
				error = (*function)((void *)cdp,
						    service,
						    node,
					            surrogate_node,
						    (void *)NULL);
			}
			break;

		default:
			what="";
			printk(KERN_WARNING "clms_call_svcfunc(): "
			       " Unknown flag\n");
			break;
	}

	if (error) {
		printk(KERN_NOTICE "clms_call_svcfunc: %s failed error = %d\n",
		       what, error);
	}

	return error;
}


/*
 * clms_call_all_svcs()
 *	Call all the registered subsystems to inform an event (like "Node Up").
 *
 * Description:
 *	This function is used to call the procedures of all registered
 *	subsystems in the event of a "Node Up" or "Node Down". It goes
 *	through all the registered subsystems and calls their procedures
 *	according to their priorities. `
 *
 *	It calls all the subsystems of the same priority simultaneously and
 *	then waits for them to complete the processing. The subsystems call
 *	back clms after they have completed the processing.
 *
 *	Subsystems with band priority -1 are independent of all other
 *	subsystems.  The registered procedures of these subsystems are
 *	therefore executed in parallel with other priority bands.
 *	CLMS does not wait for the subsystems in band -1 to complete
 *	until all other priority bands have completed their processing.
 *
 * Parameters:
 *	struct clms_transition_data *cdp - clms transition data structure
 *	int			    flag - indicates the type of procedure
 *					   to be called.
 *
 */
int
clms_call_all_svcs(struct clms_transition_data *cdp, int flag)
{
	int error;
	int pri;
	struct clms_sband *csband;
	struct clms_svc_data *svcinfo;
	struct timer_list clms_nodedown_timer;
	struct timer_list clms_nodedown_timer2;

	init_timer(&clms_nodedown_timer);
	init_timer(&clms_nodedown_timer2);
	/*
 	 * Take the first priority band.
 	 */
	csband = clms.clms_svc.cs_first;
	pri = -2;

	while (csband) {
		SSI_ASSERT(csband->sb_pri > pri);
		pri = csband->sb_pri;
		/*
		 * Call the subsystems of a particular priority.
		 */
		/*
		 * Band -1 is for subsystems with no dependencies.
		 * They are the first to be called back and the last to
		 * be waited on.
		 */
		if (pri == -1) {
			if (flag == CLMS_CALL_NODEDOWN) {
				clms_nodedown_timer2.expires = jiffies +
						clms_nodedown_timeout * HZ;
				clms_nodedown_timer2.function =
						clms_handle_nodedown_timeout;
				clms_nodedown_timer2.data = (unsigned long)cdp;
				add_timer(&clms_nodedown_timer2);
			}
		}
		else {
			if (flag == CLMS_CALL_NODEDOWN) {
				clms_nodedown_timer.expires = jiffies +
						clms_nodedown_timeout * HZ;
				clms_nodedown_timer.function =
						clms_handle_nodedown_timeout;
				clms_nodedown_timer.data = (unsigned long)cdp;
				add_timer(&clms_nodedown_timer);
			}
		}
		svcinfo = csband->sb_first;
		while (svcinfo) {
#ifdef DEBUG
			if (flag == CLMS_CALL_NODEDOWN)
			{
				printk(KERN_NOTICE
				       "!clms_call_all_svcs (CALL %u %s): %ld\n",
				       cdp->node,
				       svcinfo->s_sinfo->s_name,
				       TICKS());
			}
#endif
			error = clms_call_svcfunc(cdp, flag, svcinfo);
			if (error) {
				return error;   /* correct? NSC_XXX */
			}
			svcinfo = svcinfo->s_next;
		}
		/*
		 * Wait for the subsystems of the particular priority to
		 * call back saying that they have completed the processing.
		 */
		/*
		 * Don't wait for band -1. Since these subsystems have no
		 * dependencies on other subsystems, they can run in parallel
		 * with other priority bands. We'll do a waitfor after all
		 * the other priority bands have finished processing.
		 */
		if (pri != -1) {
			clms_waitfor_callbacks(cdp, flag, 0);
			if (flag == CLMS_CALL_NODEDOWN) {
				del_timer_sync(&clms_nodedown_timer);
			}
#ifdef DEBUG
			if (flag == CLMS_CALL_NODEDOWN)
			{
				printk(KERN_NOTICE
				       "!clms_waitfor_callbacks (DONE %u): %ld\n",
				       cdp->node,
				       TICKS());
			}
#endif
		}

		csband = csband->sb_next;
	}
	/* Wait for band -1 subsystems to finish processing. */
	clms_waitfor_callbacks(cdp, flag, 1);
	if ( (flag == CLMS_CALL_NODEDOWN) &&
	     timer_pending(&clms_nodedown_timer2))
	{
		del_timer_sync(&clms_nodedown_timer2);
	}
#ifdef DEBUG
	if (flag == CLMS_CALL_NODEDOWN)
	{
		printk(KERN_NOTICE
		       "!clms_waitfor_callbacks (DONE %u): %ld\n",
		       cdp->node,
		       TICKS());
	}
#endif

	return 0;
}

/*
 * clms_handle_nodedown_timeout()
 *	Timeout routine for nodedowns.
 *
 * Description:
 *	This routine gets called if the timeout goes off while we're waiting
 *	for a subsystem to complete nodedown.  We panic if the nodedown
 *	routine doesn't callback after clms_nodedown_time seconds.
 */
static void
clms_handle_nodedown_timeout(unsigned long dummy)
{
	/*
	 * I assert: if this timer goes off, you are in trouble. Checking
	 * if you are in last-second race isn't worthwhile and it means
	 * that I have to make the cdp->lock block interrupts.
	 */
	panic("Timed out waiting for nodedown to complete!");
}

/*
 * clms_check_callbacks()
 *	Check if all the subsystems have called back.
 *
 * Description:
 *	This function if all clms subsystems have called back saying that
 *	they have completed the processing. If so, it retuns a 1. Otherwise
 *	it returns a 0.
 *
 * Calling/Exit State:
 *	Static function.
 *
 * Parameters:
 *	struct clms_transition_data *cdp - pointer to clms transition data
 *					   structure.
 *	int			    flag - flag indicating what type of
 *					   procedure to check.
 *	int			    nodep - flag indicating if checking
 *					    band -1 (nodep) callbacks.
 *
 * Returns:
 *	1 if all subsystems have called back. 0 otherwise.
 */
static int
clms_check_callbacks(struct clms_transition_data *cdp, int flag, int nodep)
{
	int i;
	int all_called_back;
	int *callbacks;

	switch (flag) {
		case CLMS_CALL_NODEUP:
		case CLMS_CALL_NODEDOWN:
			all_called_back = 1;
			if (nodep)
				callbacks = cdp->transition_callbacks2;
			else
				callbacks = cdp->transition_callbacks;
			for (i=0; i < clms_num_subsys; i++) {
				if (callbacks[i] !=
						CLMS_FUNCTION_CALLED_BACK)
					all_called_back = 0;
			}
			break;

		default:
			all_called_back = 0;
			printk(KERN_WARNING "clms_check_callbacks: "
			       " Unknown flag\n");
			break;
	}

	return all_called_back;

}

/*
 * clms_waitfor_callbacks()
 *	Waits for all the subsystems to call back saying that they have
 *	completed processing the event (either "Node Up" or "Node Down").
 *
 * Description:
 *	This function waits for all subsystems to call back. The flag indicates
 *	the type of procedure. An entry is marked in the clms transition
 *	data structure before the function is called. The entry is deleted
 *	after the procedure has called back. This function checks whether
 *	all procedures have called back.
 *
 * Parameters:
 *	struct clms_transition_data *cdp - pointer to clms transition data
 *					   structure.
 *	int			    flag - flag indicating what type of
 *					   procedure.
 *	int			    nodep - flag indicating if waiting
 *					    for band -1 (nodep) callbacks.
 */
static int
clms_waitfor_callbacks(struct clms_transition_data *cdp, int flag, int nodep)
{

	LOCK_COND_LOCK(&cdp->lock);
	while (!clms_check_callbacks(cdp, flag, nodep))
		WAIT_CONDITION(&cdp->cond, &cdp->lock);
	UNLOCK_COND_LOCK(&cdp->lock);

	return 0;
}

/* semaphore to protect critical section in clms_svc_register(). */
DECLARE_MUTEX(clms_svc_register_sem);

/*
 * clms_svc_register()
 *	Register a subsystems with CLMS.
 *
 * Description:
 *	This function is called by the clms subsystems to register with
 *	CLMS. Each subsystems registers a set of functions
 *	(nodeup(), nodedown()) with the clms. One or more of the functions
 *	may be NULL. The sinit structure is used to register.  A priority
 *	is also specified for each subsystem. The order in which a subsystem
 *	is called depends on its priority. The priorities of subsystems are
 *	stored in a global table.
 *
 * Parameters:
 *	struct sinit *sinit - pointer to structure describing the subsystem
 */
void
clms_svc_register(struct sinit *sinit)
{
	struct clms_sband *svcbp;
	struct clms_svc_data *csdatap;

	csdatap = (struct clms_svc_data *)
		kzmalloc_nofail(sizeof(struct clms_svc_data));
	csdatap->s_next = NULL;
	csdatap->s_sinfo = sinit;
	csdatap->s_service = sinit->service;
	csdatap->s_pri = sinit->pri;

	down(&clms_svc_register_sem);
	svcbp = clms_get_svcband(sinit->pri);
	SSI_ASSERT(svcbp);

	if (svcbp->sb_first == NULL) {
		svcbp->sb_first = svcbp->sb_last = csdatap;
	} else {
		svcbp->sb_last->s_next = csdatap;
		svcbp->sb_last = csdatap;
	}
	up(&clms_svc_register_sem);

	return;

}

/*
 * clms_get_key_server_node()
 *	Get the server node for a key service.
 *
 * Description:
 *	Find the key service entry in the database, and get the server node.
 *	If there is no server node listed, return CLUSTERNODE_INVAL.  If
 *	the running node is the primary server, this routine is guaranteed
 *	to return this_node, since the entry has already been installed as
 *	a result of the RCLMS_OFFER_SERVICE RPC issued to the master, earlier.
 *
 *	If the "wait" flag is set, this routine will wait for the server
 *	information to arrive (a la clms_waitfor_cluster_svr_info()).  This
 *	has ordering constraints, since this routine may be called before
 *	the node has issued the RCLMS_WANT_TO_JOIN RPC, and therefore before
 *	it has received the RCLMS_JOIN information.  It may be called this
 *	early so that key services wishing to initialize as servers may do
 *	so.  Under this circumstance, the "wait" flag must never be set, to
 *	avoid deadlock.
 *
 *	This routine may be called after the node has joined the cluster with
 *	the "wait" flag set to wait for the server node to make itself known.
 *
 *	NSC_XXX - If the wait version of the routine is never used, this
 *		  capability should be removed.
 *
 * Parameters:
 *	service		The key service for which to find the server.
 *	wait		Flag indicating whether to wait for the server to
 *			appear.
 */
clusternode_t
clms_get_key_server_node(
	int service,
	int wait)
{
	cluster_server_t *clsp;
	clusternode_t node;

	SSI_ASSERT(service >= 0 && service <= CLMS_LAST_KEY_SERVICE);
	node = CLUSTERNODE_INVAL;
	clsp = NULL;
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	while (clsp == NULL) {
		clsp = clms.clms_cluster_server[service];
		while (clsp &&
		       ((clsp->cls_type != CLUSTER_PRIMARY_SERVER &&
		        clsp->cls_type != CLUSTER_FLOATER_SERVER) ||
			clsp->cls_node == CLUSTERNODE_INVAL))
			clsp = clsp->cls_next;
		if (clsp == NULL && wait == 0)
			break;
		if (clsp == NULL) {
			WAIT_CONDITION(&clms.clms_cluster_svr_cond,
				       &clms.clms_cluster_svr_lock);
		}
	}
	if (clsp != NULL)
		node = clsp->cls_node;
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	return(node);
}

/*
 * clms_get_key_secondary_node()
 *	Get the first secondary node for a key service.
 *
 * Description:
 *	Find the key service entry in the database, and get the first
 *	secondary. If there is no secondary listed, return CLUSTERNODE_INVAL.
 *
 *	If the "wait" flag is set, this routine will wait for the secondary
 *	server information to arrive (a la clms_waitfor_cluster_svr_info()).
 *
 *	NSC_XXX - If the wait version of the routine is never used, this
 *		  capability should be removed.
 *
 * Parameters:
 *	service		The key service for which to find a secondary.
 *	wait		Flag indicating whether to wait for a secondary to
 *			appear.
 */
clusternode_t
clms_get_key_secondary_node(
	int service,
	int wait)
{
	cluster_server_t *clsp, *savep;
	clusternode_t node;

	SSI_ASSERT(service >= 0 && service <= CLMS_LAST_KEY_SERVICE);
	node = CLUSTERNODE_INVAL;
	clsp = savep = NULL;
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	while (savep == NULL) {
		clsp = clms.clms_cluster_server[service];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_SECONDARY_SERVER) {
				if (savep == NULL)
					savep = clsp;
				else
					if (clsp->cls_node < savep->cls_node)
						savep = clsp;
			}
			clsp = clsp->cls_next;
		}
		if (savep == NULL && wait == 0)
			break;
		if (savep == NULL) {
			WAIT_CONDITION(&clms.clms_cluster_svr_cond,
				       &clms.clms_cluster_svr_lock);
		}
	}
	if (savep != NULL)
		node = savep->cls_node;
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	return(node);
}

/*
 * clms_set_key_service_ready()
 *	Inform the clms that a key service has completed initialization and is
 *	ready for use.
 *
 * Description:
 *	This function is called by a key server to inform CLMS that the
 *	key service has completed its initialization and is ready to be used
 *	by other nodes in the cluster.
 *
 * 	If the running node is not the primary server, the call is ignored.
 *
 *	If the running node is the CLMS master, inform all nodes that are
 *	waiting for information.  If the running node is not the CLMS master,
 *	the CLMS master node is informed.  The CLMS master, in turn, will
 *	inform all the other nodes in the cluster.
 *
 * Parameters:
 *	None.
 */
void
clms_set_key_service_ready(
	int service)
{
	int type;
	int error;

	/*
	 * Look up the key service on this node.
	 * If it's the primary, mark it ready and tell other nodes.
	 * If it's not the primary, ignore it.
	 */
	type = clms_get_cluster_svr_type(service, this_node);
	if (type == CLUSTER_PRIMARY_SERVER || type == CLUSTER_FLOATER_SERVER) {
		/*
		 * Mark the key service as ready and inform the master (if
		 * we aren't him) or all the nodes in the cluster (if we are).
		 */
		clms_set_cluster_svr_status(service,
					    this_node,
					    CLUSTER_SERVER_READY);

		if (this_node == clms_master_node) {
			clms_send_cluster_svrinfo(this_node,
				  	  	 service,
				  	  	 type,
				  	  	 CLUSTER_SERVER_READY,
				  	  	 CLMS_NODE_UP|CLMS_NODE_HALF_UP
							|CLMS_NODE_COMING_UP);
		}
		else {
			error = RCLMS_NEW_CLUSTER_SVRINFO_MSG(clms_master_node,
						 	      this_node,
						 	      service,
						 	      type,
						 	      CLUSTER_SERVER_READY);
			if (error) {
				printk(KERN_WARNING "clms_send_cluster_svrinfo: "
				       " ICS MSG failed for node %u"
				       " error %d \n",
				       clms_master_node, error);
			}
		}
	}
}

/*
 * rclms_new_cluster_svrinfo()
 *	Receives information about a server in the cluster providing a
 *	key service.
 *
 * Description:
 *	This routine is called whenever a server in the cluster providing
 *	a key service (e.g. root fs) has status information about the
 *	key service.
 *
 *	The local data base is updated and anybody waiting for this
 *	key service is waken up.
 *
 *	If this node is the clms master, the information is also sent to
 *	all the other nodes in the cluster.
 *
 * Calling/Exit State:
 *	Called from ics server proc to handle an ICS message.
 *
 * Parameters:
 *	clusternode_t	node		- this node
 *	clusternode_t	server_node	- the node number of the key server
 *	int		service 	- the cluster key service provided
 *	int		svr_type	- the server type
 *					  (primary/secondary/none)
 *	int		svr_status	- the status of key server/service
 *					  (ready or not)
 *
 * Returns
 *	0.
 */
int
rclms_new_cluster_svrinfo(clusternode_t node,
			  clusternode_t server_node,
			  int service,
			  int svr_type,
			  int svr_status)
{

	int node_state;

	/*
	 * Store the information about the key server/service in the local
 	 * data base. Make sure to only update if necessary.
	 */
	clms_vset_cluster_svr(service, server_node, svr_type, svr_status);

	/*
	 * If we are the clms master node, send the information to all
	 * the other nodes in the cluster.
	 */
	if (this_node == clms_master_node) {
		node_state = CLMS_NODE_UP | CLMS_NODE_HALF_UP
					  | CLMS_NODE_COMING_UP;
		clms_send_cluster_svrinfo(server_node, service,
				  	  svr_type, svr_status, node_state);
	}

	return 0;
}

/*
 * clms_send_cluster_svrinfo()
 *	Send information about a cluster key service/server to a set of nodes.
 *
 * Description:
 *	This function is called to send out information about a cluster
 *	key service (e.g. root file system) to all the nodes in the cluster.
 *	The selection of nodes to be informed is performed based on the
 *	state flag (node_flag parameter).
 *
 *	Any "Node Up" processing s prevented for the duration of
 *	this routine.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	clusternode_t node	- node number of the server
 *	int    service		- key service
 *	int    type		- type of server (primary/secondary etc.)
 *	int    status		- status of server (ready/not ready)
 *	int    node_flag	- state of the node. All the nodes in the
 *			          cluster whose state matches the above state
 *				  are sent the information.
 *
 * NSC_XXX:
 *	node_flag needs to be validated for NODE_DOWN/NODE_NEVER_UP states.
 */
int
clms_send_cluster_svrinfo(clusternode_t node,
			  int    service,
			  int    type,
			  int    status,
			  int    node_flag)
{
	int state;
	int i;
	int error;
	cluster_server_t	*clsp;

	clms_freeze_nodeup(); /* Freeze nodeups */

	for (i=1; i <= NSC_MAX_NODE_VALUE; i++) {
		if ( (i == this_node) || (i == node) )
			continue;
		state = clms_get_node_status(i);
		if (state & node_flag) {
			error = RCLMS_NEW_CLUSTER_SVRINFO_MSG(i,
						  node, service,
						  type, status);
			if (error) {
				printk(KERN_WARNING "clms_send_cluster_svrinfo: "
				       " ICS MSG failed for node %d"
				       " error %d \n",
				       i, error);
			}
		}
	}
	/* Does making this ready end the cold boot process? */
	if (clms_master_cold_boot) {
		LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		/* Do all key services have a ready server? */
		for (i = 0; i < clms_num_key_svcs; i++) {
			clsp = clms.clms_cluster_server[i];
			while (clsp != NULL &&
			       !((clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
				  clsp->cls_type == CLUSTER_FLOATER_SERVER) &&
				 clsp->cls_status == CLUSTER_SERVER_READY))
				clsp = clsp->cls_next;
			if (clsp == NULL)
				break;
		}
		UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
		/* Did we find all the key services to be ready? */
		if (i == clms_num_key_svcs)
			/* Yes: we're hot! */
			clms_master_cold_boot = FALSE;
	}
	clms_unfreeze_nodeup();

	return 0;
}

/*
 * clms_iskeynode()
 *	determine if a node is providing a key service.
 *
 * Description:
 *	This functions determines if a node provides any key service or not.
 *	It returns true if the node provides any such key service,
 *	false otherwise. The local database is searched to see if the node
 *	is a primary server for any key service.
 *
 *	NOTE:
 *	The criteria could change.
 *
 * Parameters:
 *	clusternode_t	node	Node number of the node.
 *
 * Returns:
 *	Returns 1 if the node is key node. Otherwise returns a 0.
 *
 */
int
clms_iskeynode(clusternode_t node)
{
	cluster_server_t	*clsp;
	int			i;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			    clsp->cls_type == CLUSTER_FLOATER_SERVER ||
			    clsp->cls_type == CLUSTER_SECONDARY_SERVER) {
				if (clsp->cls_node == node) {
					UNLOCK_COND_LOCK(
						&clms.clms_cluster_svr_lock);
					return 1;
				}
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);

	return 0;
}

/*
 * clms_issecondarynode()
 *	Determine if a node is providing or secondarying for a key service.
 *
 * Description:
 *	This functions determines if a node provides any key service, or
 *	if it is a secondary node for such a service.  It returns true if
 *	the node provides any such service, false otherwise.  The local
 *	database is searched to see if the node is a primary or secondary
 *	server for any key service.
 *
 * Parameters:
 *	clusternode_t	node	Node number of the node.
 *
 * Returns:
 *	Returns 1 if the node is a key or secondary node. Otherwise returns a 0.
 */
int
clms_issecondarynode(clusternode_t node)
{
	cluster_server_t	*clsp;
	int			i;

	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			    clsp->cls_type == CLUSTER_FLOATER_SERVER ||
			    clsp->cls_type == CLUSTER_SECONDARY_SERVER) {
				if (clsp->cls_node == node) {
					UNLOCK_COND_LOCK(
						&clms.clms_cluster_svr_lock);
					return 1;
				}
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	return(0);
}

/*
 * clms_alloc_keynodeinfo()
 *	Allocate space for storing a list of key nodes and their ics
 *	information.
 *
 * Description:
 *	This function allocates space for storing the key nodes in the
 *	cluster and their ics information. Key nodes are defined as those
 *	which provide cluster key services like Root File System,
 *	CLMS, INIT etc. The number of such nodes are obtained from
 *	the local database. The routine is designed to allow the
 *	arrays allocated to be reused.
 *
 * Parameters:
 *	clusternode_t	  **nodelist	- pointer to pointer to list of nodes
 *	icsinfo_t	  **icsinfolist	- pointer to pointer to icsinfo list
 *	int		  *num_nodes  	- pointer to size of the above arrays
 *
 *  Returns:
 *	On success returns 0. On failure the corresponding error code
 *	is returned.
 *	-EINVAL	- Invalid arguments
 *	-ENOSR	- Out of memory
 */
int
clms_alloc_key_nodeinfo(
	clusternode_t	**nodelist,
	icsinfo_t	**icsinfolist,
	int		*num_nodes)
{
	cluster_server_t	*clsp;
	int			node_count;
	int			i;


	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	node_count = 0;
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			    clsp->cls_type == CLUSTER_FLOATER_SERVER ||
			    clsp->cls_type == CLUSTER_SECONDARY_SERVER)
				node_count++;
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);

	if (node_count > *num_nodes) {
		if (*nodelist) {
			kfree((u_char *)*nodelist);
		}
		if (*icsinfolist) {
			kfree((u_char *)*icsinfolist);
		}
		*num_nodes = max(16, node_count);
		*nodelist = (clusternode_t *)kzmalloc(
					(*num_nodes)*sizeof(clusternode_t),
					GFP_KERNEL);
		if (*nodelist == NULL) {
			*num_nodes = 0;
			return -ENOSR;
		}
		*icsinfolist = (icsinfo_t *)
				kzmalloc((*num_nodes)*sizeof(icsinfo_t),
						GFP_KERNEL);
		if (*icsinfolist == NULL) {
			kfree((u_char *)*nodelist);
			*num_nodes = 0;
			return -ENOSR;
		}
	}

	return 0;
}

/*
 * clms_get_key_nodeinfo()
 *	Get the list of key nodes and their ics information.
 *
 * Description:
 *	This function gets the list of key nodes and their ics information
 *	and stores the information in the given array. A keynode is defined
 *	as a node providing a cluster key service.
 *
 * Parameters:
 *	clusternode_t *nodelist	- pointer to list of nodes
 *	icsinfo_t *icsinfolist	- pointer to icsinfo list
 *	int	  num_nodes  	- size of the above arrays
 *
 *  Returns:
 *	On success returns the number of nodes in the list.
 *	On failure returns a -1.
 */
int
clms_get_key_nodeinfo(
	clusternode_t	*nodelist,
	icsinfo_t	*icsi,
	int		num_nodes)
{
	cluster_server_t	*clsp;
	int			node_count;
	int			i, j;
	int			error;


	if (icsi == NULL && num_nodes > 0)
		return(-1);
	LOCK_COND_LOCK(&clms.clms_cluster_svr_lock);
	node_count = 0;
	for (i=0; i < clms_num_key_svcs; i++) {
		clsp = clms.clms_cluster_server[i];
		while (clsp) {
			if (clsp->cls_type == CLUSTER_PRIMARY_SERVER ||
			    clsp->cls_type == CLUSTER_FLOATER_SERVER ||
			    clsp->cls_type == CLUSTER_SECONDARY_SERVER) {
				if (node_count == num_nodes) {
					UNLOCK_COND_LOCK(
						  &clms.clms_cluster_svr_lock);
					return(-1);
				}
				for (j = 0; j < node_count; j++) {
					if (nodelist[j] == clsp->cls_node)
						break;
				}
				if (j == node_count) {
					nodelist[node_count] = clsp->cls_node;
					error = ics_geticsinfo(
							   clsp->cls_node,
							   &(icsi[node_count]));
					SSI_ASSERT(error == 0);
					node_count++;
				}
			}
			clsp = clsp->cls_next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_cluster_svr_lock);

	return node_count;
}

/*
 * void
 * clms_callback()
 *	Register callbacks from clms subsystems so that clms knows
 *	that a subsystem has completed processing.
 *
 * Description:
 *	Each clms subsystem is called when an event like "Node Up" occurs.
 *	The subsystem calls CLMS back once it has completed processing.
 *	This subroutine is used to register the callback in the
 *	clms transition structure (a pointer to which is passed to the
 *	subsystem). Anybody waiting for this call back is waken up.
 *
 * Parameters:
 *	void		*clms_handle	- passed to the subsystems to be passed
 *					  back.  (actually a pointer to the
 *					  clms transition structure)
 * 	int		service		- clms subsystem
 *	clusternode_t	node		- node associated with the event for
 *					  which callback is issued.
 *	int	flag			- type of function calling back
 *					  (nodeup(), nodedown())
 */
void
clms_callback(void *clms_handle, int service, clusternode_t node, int flag)
{
	struct clms_transition_data *cdp;

	cdp = (struct clms_transition_data *)clms_handle;

	SSI_ASSERT(node == cdp->node);

	LOCK_COND_LOCK(&cdp->lock);
	switch (flag) {
		case CLMS_CALL_NODEUP:
		case CLMS_CALL_NODEDOWN:
			cdp->transition_callbacks[service] =
			cdp->transition_callbacks2[service] =
						CLMS_FUNCTION_CALLED_BACK;
			break;

		default:
			printk(KERN_WARNING "clms_callback: Unknown flag %d\n",
			       flag);
			break;
	}
	SIGNAL_CONDITION(&cdp->cond);
	UNLOCK_COND_LOCK(&cdp->lock);

	return;
}

/*
 * void
 * clms_nodeup_calllback()
 *	Issued by clms subsystems to inform clms that they have completed
 *	"Node Up" processing.
 *
 * Description:
 *	This function is called by the clms subsystem to inform clms that
 *	they have completed the "Node Up" processing. The actual work is
 *	done by clms_callback().
 *
 * Parameters:
 *	void		*clms_handle	- passed to the subsystem to be
 *					  passed back.
 *					  (actually a pointer to the
 *					  clms transition structure)
 * 	int		service		- clms subsystem
 *	clusternode_t	node		- node associated with the event for
 *					  which callback is issued.
 */
void
clms_nodeup_callback(void *clms_handle, int service, clusternode_t node)
{
	clms_callback(clms_handle, service, node, CLMS_CALL_NODEUP);
}

/*
 * void
 * clms_nodedown_calllback()
 *	Issued by clms subsystems to inform clms that they have completed
 *	"Node Down" processing.
 *
 * Description:
 *	This function is called by the clms subsystem to inform clms that
 *	they have completed the "Node Down" processing. The actual work is
 *	done by clms_callback().
 *
 * Parameters:
 *	void		*clms_handle	- passed to the subsystem to be
 *					  passed back.
 *					  (actually a pointer to the
 *					  clms transition structure)
 * 	int		service		- clms subsystem
 *	clusternode_t	node		- node associated with the event for
 *					  which callback is issued.
 */
void
clms_nodedown_callback(void *clms_handle, int service, clusternode_t node)
{

	clms_callback(clms_handle, service, node, CLMS_CALL_NODEDOWN);
}

void
clms_key_svc_pull_data(
	int service,
	int buflen_hint,
	nsc_nodelist_t *nodelist)
{
	data_t *buffer;
	int buffer_len, data_len, rval, error;
	clusternode_t node;
	nsc_nlcookie_t cookie;

	data_len = buffer_len = buflen_hint;
	buffer = kmalloc_nofail(buffer_len);
	NSC_NLCOOKIE_INIT(&cookie);
	while ((node = NSC_NODELIST_GET_NEXT(&cookie, nodelist))
							!= CLUSTERNODE_INVAL) {
		do {
			if (node == this_node &&
			    key_service_info[service].pull_data != NULL) {
				error = key_service_info[service].pull_data(buffer,
									    buffer_len,
									    &data_len);
			}
			else {
				error = RCLMS_KEY_SVC_PULL_DATA(node,
								&rval,
								service,
								&buffer,
								&buffer_len,
								&data_len);
				if (error == 0)
					error = rval;
			}
			if (error == -E2BIG) {
				kfree(buffer);
				SSI_ASSERT(data_len > buffer_len);
				buffer_len = data_len;
				buffer = kmalloc_nofail(buffer_len);
			}
		} while (error == -E2BIG);
		if (error == 0 && key_service_info[service].failover_data != NULL)
			key_service_info[service].failover_data(node,
								buffer,
								data_len);
	}
	kfree(buffer);
}

int
rclms_key_svc_pull_data(
	clusternode_t node,
	int *rval,
	int service,
	data_t **buffer,
	int *buffer_len,
	int *data_len)
{
	*buffer = kmalloc_nofail(*buffer_len);
	*data_len = 0;
	if (key_service_info[service].pull_data != NULL) {
		*rval = key_service_info[service].pull_data((char *)*buffer,
							    *buffer_len,
							    data_len);
	}

	return 0;
}

#if defined(DEBUG) || defined(DEBUG_TOOLS)
void
clms_print_svc_bands(void)
{
	struct clms_sband *cb;
	struct clms_svc_data *csd;

	printk(KERN_DEBUG "\n\n");
	printk(KERN_DEBUG "Clms: Number of Subsystem Bands = %d\n",clms.clms_svc.cs_nbands);

	cb = clms.clms_svc.cs_first;
	while (cb) {
		printk(KERN_DEBUG "Clms: Priority of Band = %d \n", cb->sb_pri);
		csd = cb->sb_first;
		while (csd) {
			printk(KERN_DEBUG "Clms: Subsystem is %s \n", csd->s_sinfo->s_name);
			csd = csd->s_next;
		}
		printk(KERN_DEBUG "\n\n");
		cb = cb->sb_next;
	}
}

void
print_clms_key_service(int service)
{
	cluster_server_t *clsp;
	static char *types[5] = { "not a server", "primary", "secondary",
				  "floater entry", "floater server" };
	static char *stats[3] = { "not ready", "ready", "up" };

	printk(KERN_DEBUG "Service %s, info node %u, crit %d\n",
		     key_service_info[service].name,
		     key_service_info[service].server,
		     key_service_info[service].crit);
	clsp = clms.clms_cluster_server[service];
	if (clsp == NULL)
		printk(KERN_DEBUG "No server info\n");
	while (clsp) {
		printk(KERN_DEBUG "cluster_server_t @ 0x%p\n", clsp);
		printk(KERN_DEBUG "\tnext @ 0x%p, svc %d, node %u\n",
			     clsp->cls_next,
			     clsp->cls_svc,
			     clsp->cls_node);
		printk(KERN_DEBUG "\ttype %d (%s), status %d (%s)\n",
			     clsp->cls_type, types[clsp->cls_type],
			     clsp->cls_status, stats[clsp->cls_status]);
		clsp = clsp->cls_next;
	}
	printk(KERN_DEBUG "\n");
}

void
print_clms_key_svcs(void)
{
	int i;

	printk(KERN_DEBUG "Key services:\n");
	for (i=0; i < clms_num_key_svcs; i++)
		print_clms_key_service(i);
}

#endif /* DEBUG */
