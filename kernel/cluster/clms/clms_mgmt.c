/*
 * 	CLMS management code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the CLMS Management code. It provides CLMS
 * initialization and some common subroutines.
 */

#include <linux/delay.h>
#include <linux/reboot.h>
#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/nodelist.h>
#include <cluster/icsgen.h>

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>


void clms_nodeup_ind(clusternode_t, icsinfo_t *);
void clms_nodedown_ind(clusternode_t);

void clms_node_trans_ind(clusternode_t, int, icsinfo_t *);

void clms_nodeup(clusternode_t);
void clms_node_halfup(clusternode_t, icsinfo_t *);
void clms_node_halfdown(clusternode_t);

void clms_nodedown(clusternode_t);

int clms_nodedebug(clusternode_t);

void clms_nodedown_l(clusternode_t);

#if defined(DEBUG) || defined(DEBUG_TOOLS)
static void clms_do_nodedebug_one(clusternode_t);
#endif /* DEBUG || DEBUG_TOOLS */
static int clms_check_client_callbacks(struct clms_transition_data *, int);

extern clusternode_t clms_master_node;	/* Node Number of CLMS Coordinator */
extern struct clms clms;		/* Structure for the CLMS */

struct clms_handle_registry_st {
	struct clms_handle_registry_st	*next;
	struct clms_handle_registry_st	*prev;
	u_long				handle;
};
typedef struct clms_handle_registry_st clms_handle_registry_t;

static clms_handle_registry_t *clms_handle_registry = NULL;
__cacheline_aligned_in_smp SPIN_LOCK_T clms_handle_registry_lock;

/*
 * clms_nodeup_ind()
 *	Function specified to receive "Node Up" indications from ICS.
 *
 * Description:
 *	This function is specified as the function to be called back
 *	by ICS when it detects a node coming up.
 *
 * Parameters:
 *	clusternode_t node	- the node that is coming up.
 */
void
clms_nodeup_ind(
	clusternode_t node,
	icsinfo_t *icsinfo)
{
	struct clms_transition_data *cdp;

	CLMSLOG(CLMSLOG_NUIND, node, 0, 0, 0, 0, 0, 0);
	if (clms_master_node == this_node) {
		/*
		 * If failover has blocked us, sleep, queue this nodeup for
		 * later processing and return.
		 */
		cdp = clms_alloc_trans_data();
		cdp->node = node;
		cdp->private_data = kmalloc_nofail(sizeof(icsinfo_t));
		LOCK_COND_LOCK(&clms.clms_transition_lock);
		if (clms.clms_transition_flag & CLMS_TRANS_FAILOVER) {
			/*
			 * Put this on the queue without locking; since we're
			 * holding the transition lock and CLMS_TRANS_FAILOVER
			 * is set, we know that no one else will be looking at
			 * this queue.
			 */
			clms_putq_trans_l(&clms.clms_failover_nodeup_queue,
					  cdp);
			UNLOCK_COND_LOCK(&clms.clms_transition_lock);
			return;
		}
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);
		clms_free_trans_data(cdp);
		clms_node_trans_ind(node, CLMS_NODE_HALF_UP, icsinfo);
	}
}

/*
 * clms_nodedown_ind()
 *	Function specified to receive "Node Down" indications from ICS.
 *
 * Description:
 *	This function is specified as the function to be called back
 *	by ICS when it detects a node going down.
 *
 * Parameters:
 *	clusternode_t node	- the node that is going down or has gone down.
 */
void
clms_nodedown_ind(clusternode_t node)
{
	icsinfo_t cicsinfo;

	INCR_MEMPRIO();
	CLMSLOG(CLMSLOG_NDIND, node, clms_master_node, 0, 0, 0, 0, 0);
	if (node == clms_master_node) {
		SSI_ASSERT(node != this_node);
		clms_attempt_failover(clms_master_node);
	}
	if (clms_master_node == this_node) {
		clms_node_trans_ind(node, CLMS_NODE_DOWN, &cicsinfo);
	}
	DECR_MEMPRIO();
}

/*
 * clms_node_trans_ind()
 *	Function to receive "Node Transition" indications (Up/Down) from ICS.
 *
 * Description:
 *	This function is called when a "Node Up" or "Node Down" indication
 *	s received from ICS.
 *
 *	The clms_node_halfup() or clms_nodedown() is called depending on the
 *	situation. Routing the transitions through one function, especially
 *	ics notifications, makes serializations simple, if need be.
 *
 * Parameters:
 *	clusternode_t node	- the node that is undergoing a transition.
 *	int	      flag	- flag indicating the type of transition
 *				  (up/down)
 */
void
clms_node_trans_ind(
	clusternode_t node,
	int flag,
	icsinfo_t *icsinfo)
{
	if (this_node != clms_master_node) {
		/*
		 * Currently this "Node Up Indication" is not
		 * expected in a clms client node.
		 */
		return;
	}

	if (flag == CLMS_NODE_HALF_UP) {
		clms_node_halfup(node, icsinfo);
	}
	else {
		if (flag == CLMS_NODE_DOWN)
			clms_nodedown(node);
		else
			panic("clms_node_trans_ind: "
			      "unknown flag\n");
	}
}


/*
 * CLMS Internal Subroutines.
 */

/*
 * Routines to deal with Cluster Services and Servers (CLMS, ROOT FILE SYSTEM,
 * CMS, SYSTEM V IPC, INIT etc.). Note that currently all services are
 * provided by a single node. However, in the future these may be distributed.
 */
/*
 * clms_set_op_inprogress()
 *	Set the operation in progress flag for a node transition operation.
 *
 * Description:
 *	This function is used to gain entry into a critical section of
 *	code that handles a node transition. The examples of node transitions
 *	are "Node Up" and "Node Down". Sometimes it is necessary to serialize
 *	the processing.
 *
 *	The clms_ntrans_flag, clms_ntrans_cond, and the clms_ntrans_lock
 *	fields in the clms structure are used for serialization of all
 *	node transitions
 *
 * Parameters:
 *	int	op	- operation to be set
 */
void
clms_set_op_inprogress(int op)
{

	LOCK_COND_LOCK(&clms.clms_ntrans_lock);
	while (clms.clms_ntrans_flag & op) {
		WAIT_CONDITION(&clms.clms_ntrans_cond, &clms.clms_ntrans_lock);
	}
	clms.clms_ntrans_flag |= op;
	UNLOCK_COND_LOCK(&clms.clms_ntrans_lock);
}

/*
 * clms_clear_op_inprogress()
 *	Reset the operation in progress flag after a node transition processing
 *	is complete.
 *
 * Description:
 *	This function is used to reset the flag indicating that a node
 *	transition operation has been completed. This routine is the
 *	opposite of clms_set_op_inprogress().  All processes that
 *	waiting for the condition are signaled.
 *
 * Parameters:
 *	int	op	- operation that must be cleared.
 */
void
clms_clear_op_inprogress(int op)
{
	LOCK_COND_LOCK(&clms.clms_ntrans_lock);
	clms.clms_ntrans_flag &= ~op;
	BROADCAST_CONDITION(&clms.clms_ntrans_cond);
	UNLOCK_COND_LOCK(&clms.clms_ntrans_lock);
}

/*
 * clms_shutdown_freeze()
 *	Freeze the cluster in preparation for shutdown.
 *
 * Description:
 *	Tells the CLMS master to stop allowing nodeups, as we're about
 *	to shut down the cluster.  Handles the CLMS master failing over
 *	in the middle of the operation.
 */
void
clms_shutdown_freeze()
{
	int rval, error;

	do {
		if (this_node == clms_master_node)
			error = rclms_shutdown_freeze(clms_master_node,
						      &rval,
						      this_node);
		else {
			error = RCLMS_SHUTDOWN_FREEZE(clms_master_node,
						      &rval,
						      this_node);
			(void)rclms_shutdown_freeze(this_node,
						    &rval,
						    this_node);
		}
		if (error == -EREMOTE)
			idelay(10);
	} while (error == -EREMOTE);
}

/*
 * clms_waitfor_nodeup()
 *	Wait for a node to become fully up.
 *
 * Description:
 *	This function waits for a node to be fully up.
 *
 * Parameters:
 *	clusternode_t	node	- node to wait for
 *	flag			- Flag indicating whether to get out if the node
 *				  goes down.
 *
 * Return:
 *	The status of the node.
 */
int
clms_waitfor_nodeup(
	clusternode_t node,
	boolean_t flag)
{
	int status;

	SSI_ASSERT(node > 0 && node <= NSC_MAX_NODE_VALUE);
	/*
	 * wait until the node is up.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	status = clms_get_node_status(node);
	while (!(status & (CLMS_NODE_UP |
			   CLMS_NODE_HALF_UP |
			   CLMS_NODE_HALF_DOWN))) {
		if (flag && (status & (CLMS_NODE_GOING_DOWN | CLMS_NODE_DOWN)))
			break;
		WAIT_CONDITION(	&clms.clms_node_cond, &clms.clms_node_lock);
		status = clms_get_node_status(node);
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	return(status);
}

/*
 * clms_waitfor_nodeicsup()
 *      Wait for CLMS to be able to talk to a node.
 *
 * Description:
 *      This function waits for a node to be in CLMS_NODE_ICS_UP state so
 *      that we're sure than CLMS can talk to him; this blocks the
 *      RCLMS_ASSIGN_SERVICES_MSG() call, in particular, until the
 *      clms_master_nodeup_daemon has had a chance to run.  Always gives
 *      up if the node goes down on us.
 *
 * Parameters:
 *      clusternode_t   node    - node to wait for
 *
 * Return:
 *      The status of the node.
 */
int
clms_waitfor_nodeicsup(
        clusternode_t node)
{
        int status;

        SSI_ASSERT(node > 0 && node <= NSC_MAX_NODE_VALUE);
        /*
         * wait until the node is up.
         */
        LOCK_COND_LOCK(&clms.clms_node_lock);
        status = clms_get_node_status(node);
        while (!(status & CLMS_NODE_ICS_UP)) {
                if (status & (CLMS_NODE_GOING_DOWN | CLMS_NODE_DOWN))
                        break;
                WAIT_CONDITION( &clms.clms_node_cond, &clms.clms_node_lock);
                status = clms_get_node_status(node);
        }
        UNLOCK_COND_LOCK(&clms.clms_node_lock);

        return(status);
}

/*
 * clms_waitfor_nodedown()
 *	Wait for a node to become fully down.
 *
 * Description:
 *	This function waits for a node to be fully down.
 *
 * Parameters:
 *	clusternode_t	node	- node to wait for
 *
 * Return:
 *	None
 */
void
clms_waitfor_nodedown(
	clusternode_t node)
{
	int status;

	SSI_ASSERT(node > 0 && node <= NSC_MAX_NODE_VALUE);
	/*
	 * wait until the node is down.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	status = clms_get_node_status(node);
	if (!(status & CLMS_NODE_GOING_DOWN))
		goto out;
	while (!(status & CLMS_NODE_DOWN)) {
		WAIT_CONDITION(	&clms.clms_node_cond, &clms.clms_node_lock);
		status = clms_get_node_status(node);
	}
out:	;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return;
}

/*
 * clms_transition_qinit()
 *	Initialize the clms transition queue.
 *
 * Description:
 *	This function initializes the transition queue. This queue is
 *	used to queue all the node transition events. The synchronization
 *	and the lock are initialized.
 *
 * Parameters:
 *	struct clms_transition_queue *cq  	- pointer to queue
 *	int			     flag 	- flag to set
 */
int
clms_transition_qinit(struct clms_transition_queue *cq, int flag)
{

	cq->cq_first = NULL;
	cq->cq_last = NULL;
	cq->cq_flag = flag;
	INIT_CONDITION(&cq->cq_cond);
	INIT_COND_LOCK(&cq->cq_lock);

	return 0;
}

/*
 * clms_alloc_transition_data()
 *	Allocate the clms transition data structure.
 *
 * Description:
 *	This structure allocates the clms transition data
 *	structure.
 *
 * Parameters:
 *	None.
 *
 * Returns
 *	A pointer to struct clms_transition_data. Null on failure.
 *
 */
struct clms_transition_data *
clms_alloc_trans_data()
{
	struct clms_transition_data *cdp;

	cdp = (struct clms_transition_data *)
		   kzmalloc_nofail(sizeof(struct clms_transition_data));

	cdp->node = CLUSTERNODE_INVAL;
	cdp->next = NULL;
	cdp->prev = NULL;
	cdp->private_data = NULL;
	INIT_CONDITION(&cdp->cond);
	INIT_COND_LOCK(&cdp->lock);

	return cdp;
}

/*
 * clms_init_trans_data()
 *	Initialize a clms transition data structure.
 *
 * Description:
 *	This function initializes a given clms transition data structure.
 *	The node for which this structure is initialized and the flag to
 *	initialize with are passed as parameters. Initialize the
 *	transition_callbacks array.
 *
 * Parameters:
 *	clusternode_t	node	- the node number for which this structure is
 *				  allocated.
 *
 * Returns
 *	None.
 *
 */
void
clms_init_trans_data(struct clms_transition_data *cdp, clusternode_t node)
{
	int i;

	if (cdp) {
		cdp->node = node;
		cdp->next = NULL;
		cdp->prev = NULL;
		for (i=0; i < clms_num_key_svcs; i++) {
			cdp->transition_callbacks[i] = 0;
			cdp->transition_callbacks2[i] = 0;
		}
	}

	return;
}

/*
 * clms_free_trans_data()
 *	Free a clms transition data structure.
 *
 * Description:
 *	Frees a struct clms_transition_data.
 *
 * Parameters:
 *	struct clms_transition_data *cdp - pointer to structure to be freed.
 *
 */
void
clms_free_trans_data(struct clms_transition_data *cdp)
{
	if (cdp) {
		cdp->prev = cdp->next = NULL;

		if (cdp->cnlist) {
			clms_free_nodelist(cdp->cnlist);
			cdp->cnlist = NULL;
		}
		if (cdp->ready_cnlist) {
			clms_free_nodelist(cdp->ready_cnlist);
			cdp->ready_cnlist = NULL;
		}
		if (cdp->called_cnlist) {
			clms_free_nodelist(cdp->called_cnlist);
			cdp->called_cnlist = NULL;
		}
		if (cdp->private_data) {
			kfree(cdp->private_data);
			cdp->private_data = NULL;
		}

		kfree((caddr_t)cdp);
	}
}


/*
 * clms_getq_trans_l()
 *	Get the first clms transition message from a clms transition queue.
 *
 * Description:
 *	Retrieves the first  clms transition message
 *	(struct clms_transition_data) from clms transition queue
 *	(struct clms_transition_queue) and returns a pointer to the message.
 *
 * Calling/Exit State:
 *	Locking is the responsibility of the caller.
 *
 * Parameters:
 *	struct clms_transition_queue *cqp	- pointer to clms transition
 *						  queue.
 *
 * Returns
 *	A pointer to the struct clms_transition_data. NULL if no
 *	message is queued.
 */
struct clms_transition_data *
clms_getq_trans_l(struct clms_transition_queue *cqp)
{
	struct clms_transition_data *cdp;

	if ((cdp = cqp->cq_first) != NULL) {
		if (!(cqp->cq_first = cdp->next))
			cqp->cq_last = NULL;
		else
			cqp->cq_first->prev = NULL;
	}

	return cdp;
}



/*
 * clms_rmvq_trans_l()
 *	Remove a specified clms transition message from the clms transition
 *	queue.
 *
 * Description:
 *	Removes  the specified  clms transition message
 *	(struct clms_transition_data) from the given clms transition queue
 *	(struct clms_transition_queue).
 *
 * Calling/Exit State:
 *	Locking is the responsibility of the caller.
 *
 * Parameters:
 *	struct clms_transition_queue *cqp	- pointer to clms transition
 *						  queue.
 *	struct clms_transition_data  *cdp	- pointer to clms transition
 *						  data.
 *
 * NOTE:
 *	It is assumed that the message is valid and is part of the queue.
 *	No checking is done.
 */
int
clms_rmvq_trans_l(struct clms_transition_queue *cqp,
		 struct clms_transition_data *cdp)
{
	/*
         * Remove the transition data from the list.
         */
        if (cdp->prev)
                cdp->prev->next = cdp->next;
        else
                cqp->cq_first = cdp->next;

        if (cdp->next)
                cdp->next->prev = cdp->prev;
        else
                cqp->cq_last = cdp->prev;

	rmb();
        cdp->next = NULL;
        cdp->prev = NULL;

	return 0;
}

/*
 * clms_putq_trans()
 *	Append the clms transition message to the end of the clms transition
 *	queue.
 *
 * Description:
 *	This function appends the given clms transition message to the end
 *	of the clms transition queue.
 *
 * Calling/Exit State:
 *	This function acquires the queue lock before appending the message
 *	and releases it after appending the message.
 *
 * Parameters:
 *	struct clms_transition_queue *cqp	- pointer to clms transition
 *						  queue.
 *	struct clms_transition_data  *cdp	- pointer to clms transition
 *						  data
 */
int
clms_putq_trans(struct clms_transition_queue *cqp,
		 struct clms_transition_data *cdp)
{
	LOCK_COND_LOCK(&cqp->cq_lock);
	clms_putq_trans_l(cqp, cdp);
	SIGNAL_CONDITION(&cqp->cq_cond);
	UNLOCK_COND_LOCK(&cqp->cq_lock);

	return 0;
}


/*
 * clms_putq_trans_l()
 *	Append the given clms transition message to the given transition
 *	queue.
 *
 * Description:
 *	This function does the actual work of appending the clms transition
 *	message to the given clms transition queue. This is called by
 *	clms_putq_trans().
 *
 * Calling/Exit State:
 *	No locks are held. Locking is the responsibility of the caller.
 *
 * Parameters:
 *	struct clms_transition_queue *cqp	- pointer to clms transition
 *						  queue.
 *	struct clms_transition_data  *cdp	- pointer to clms transition
 *						  data
 */
int
clms_putq_trans_l(struct clms_transition_queue *cqp,
		 struct clms_transition_data *cdp)
{
	struct clms_transition_data *tmp;

	if (cqp->cq_first == NULL) {
		cdp->next = NULL;
		cdp->prev = NULL;
		cqp->cq_first = cdp;
		cqp->cq_last = cdp;
	} else {
		tmp = cqp->cq_last;
		cqp->cq_last = cdp;
		SSI_ASSERT(tmp);
		cdp->next = NULL;
		cdp->prev = tmp;
		tmp->next = cdp;
	}

	return 0;
}


/*
 * clms_putbq_trans()
 *	Place the clms transition message at the head of the given
 *	clms transition queue.
 *
 * Description:
 *	This function places the given clms transition message at the head
 *	of the clms transition queue.
 *
 * Calling/Exit State:
 *	This function acquires the queue lock before placing the message
 *	and releases it after placing the message.
 *
 * Parameters:
 *	struct clms_transition_queue *cqp	- pointer to clms transition
 *						  queue.
 *	struct clms_transition_data  *cdp	- pointer to clms transition
 *						  data
 */
int
clms_putbq_trans(struct clms_transition_queue *cqp,
		 struct clms_transition_data *cdp)
{
	LOCK_COND_LOCK(&cqp->cq_lock);
	clms_putbq_trans_l(cqp, cdp);
	SIGNAL_CONDITION(&cqp->cq_cond);
	UNLOCK_COND_LOCK(&cqp->cq_lock);

	return 0;
}

/*
 * clms_putbq_trans_l()
 *	Place the clms transition message at the head of the given
 *	clms transition queue.
 *
 * Description:
 *	This function does the actual work of placing the given clms
 *	transition message at the head of the clms transition queue.
 *
 * Calling/Exit State:
 *	No locks are held. It is the responsibility of the caller to acquire
 *	the queue locks.
 *
 * Parameters:
 *	struct clms_transition_queue *cqp	- pointer to clms transition
 *						  queue.
 *	struct clms_transition_data  *cdp	- pointer to clms transition
 *						  data
 */
int
clms_putbq_trans_l(struct clms_transition_queue *cqp,
		 struct clms_transition_data *cdp)
{
	struct clms_transition_data *tmp;

	tmp = cqp->cq_first;
	if (tmp == NULL) {
		cdp->next = NULL;
		cdp->prev = NULL;
		cqp->cq_first = cdp;
		cqp->cq_last = cdp;
	} else {
		cdp->next = tmp;
		cdp->prev = NULL;
		tmp->prev = cdp;
		cqp->cq_first = cdp;
	}

	return 0;
}

/*
 * clms_alloc_nodelist()
 *	Allocate a structure to store a list of nodes.
 *
 * Description:
 *	This function allocates a struct clms_nodelist. This structure is
 *	used to store a list of nodes and manipulate the list very
 *	inteligently.
 *
 * Parameters:
 *	int	num_nodes	Number of nodes to store
 *
 * Returns:
 *	A pointer to a struct clms_nodelist on success and NULL on
 *	failure.
 */
struct clms_nodelist *
clms_alloc_nodelist(
	int num_nodes,
	int status)
{
	struct clms_nodelist *cnlist;
	clusternode_t	     *nodelist;

	cnlist = (struct clms_nodelist *)
		 kzmalloc(sizeof(struct clms_nodelist), GFP_KERNEL);
	if (cnlist) {
		nodelist = (clusternode_t *)kzmalloc(
				num_nodes*sizeof(clusternode_t), GFP_KERNEL);
		if (nodelist) {
			cnlist->next = NULL;
			cnlist->prev = NULL;
			cnlist->nodelist = nodelist;
			cnlist->status = status;
			cnlist->n_rptr = nodelist;
			cnlist->n_wptr = nodelist;
			cnlist->n_lim = &nodelist[num_nodes];
			cnlist->num_nodes = num_nodes;
		} else {
			kfree((caddr_t)cnlist);
			cnlist = NULL;
		}
	}
	return(cnlist);
}

/*
 * clms_free_nodelist()
 *	Free a clms nodelist structure.
 *
 * Description:
 *	This function frees a clms nodelist structure. The structure must
 *	have been allocated using clms_alloc_nodelist().
 *
 * Parameters:
 *	struct clms_nodelist *cnlist	- ptr to node list
 */
void
clms_free_nodelist(struct clms_nodelist *cnlist)
{
	/* int num_nodes; */

	if (cnlist) {
		/* num_nodes = cnlist->num_nodes; */

		if (cnlist->nodelist)
			kfree((caddr_t)cnlist->nodelist);

		kfree((caddr_t)cnlist);
	}

	return;
}

/*
 * clms_alloc_client_tr_data()
 *	Allocates a clms_cli_tr_data_t.
 *
 * Description:
 *	This function allocates a struct clms_cli_tr_data and partially
 *	initializes it.
 *
 * Parameters:
 *	trans		The transition we're using the structure for.
 *	ctv		The CTV of the transition.
 *
 * Returns:
 *	A pointer to the struct clms_cli_nu_data upon success.
 *	NULL on failure.
 */
clms_cli_tr_data_t *
clms_alloc_client_tr_data(
	int trans,
	clusternode_t node,
	int why,
	clms_ctv_t ctv)
{
	clms_cli_tr_data_t *datap;

	datap = kzmalloc_nofail(sizeof(clms_cli_tr_data_t));
	datap->node = node;
	datap->trans = trans;
	datap->why = why;
	datap->ctv = ctv;
	return(datap);
}

/*
 * clms_get_trans_queue_l()
 *	Gets the first struct clms_cli_tr_data from clms_client_trans_queue.
 *
 * Description:
 *	This function gets the first element (clms_cli_tr_data_t) in
 *	the passed queue, if it is next in the CTV progression (two
 *	elements the same as at present, the third one greater).
 *	If there is no entry satisfying the CTV progression, the
 *	routine returns NULL.
 *
 * Calling/Exit State:
 *	No locks are held. It is the responsibility of the caller to
 *	hold the queue locks.
 *
 * Parameters:
 *	struct clms_client_trans_queue *qp - pointer to queue
 *
 * Returns
 *	A pointer to the element (clms_cli_tr_data_t) satisfying the
 *	condition, else NULL.
 */
clms_cli_tr_data_t *
clms_get_trans_queue_l(struct clms_client_trans_queue *qp)
{
	clms_cli_tr_data_t *datap;

	datap = qp->first;
	if (datap == NULL)
		return(NULL);
	if (CLMS_CTV_TRANS_IS_NEXT(&(datap->ctv), &(clms.clms_ctv))) {
		qp->first = datap->next;
		if (qp->first == NULL)
			qp->last = NULL;
		else
			qp->first->prev = NULL;
		qp->size--;
		SSI_ASSERT(qp->size >= 0);
	}
	else
		datap = NULL;
	return(datap);
}

/*
 * clms_del_trans_queue_l()
 *	Removes a struct clms_cli_tr_data from clms_client_trans_queue.
 *
 * Description:
 *	This function removes the element (clms_cli_tr_data_t) that matches
 *	the given CTV from the given clms_client_trans_queue structure,
 *	and returns a pointer to that entry.  If there is no entry
 *	that matches the given CTV, the routine returns NULL.
 *
 * Calling/Exit State:
 *	No locks are held. It is the responsibility of the caller to
 *	hold the queue locks.
 *
 * Parameters:
 *	struct clms_client_trans_queue *qp - pointer to queue
 *
 * Returns
 *	A pointer to the element (clms_cli_tr_data_t) that matches.  If no
 *	element is matches, or if the queue is empty, NULL.
 */
clms_cli_tr_data_t *
clms_del_trans_queue_l(
	struct clms_client_trans_queue *qp,
	clms_ctv_t ctv)
{
	clms_cli_tr_data_t *datap;

	datap = qp->first;
	while (datap != NULL) {
		if (datap->ctv.transid == ctv.transid)
			break;
		datap = datap->next;
	}
	if (datap != NULL) {
		if (qp->first == datap)
			qp->first = datap->next;
		else
			datap->prev->next = datap->next;
		if (qp->last == datap)
			qp->last = datap->prev;
		else
			datap->next->prev = datap->prev;
		qp->size--;
		SSI_ASSERT(qp->size >= 0);
	}
	return(datap);
}

/*
 * clms_put_trans_queue_l()
 *	Insert the given clms_cli_tr_data_t into the given
 *	clms_client_trans_queue.
 *
 * Description:
 *	This function inserts the element (clms_cli_tr_data_t)
 *	into the given clms_client_trans_queue structure, sorted by the CTV.
 *
 * Calling/Exit State:
 *	No locks are held. It is the responsibility of the caller to
 *	hold the queue locks.
 *
 * Parameters:
 *	struct clms_client_trans_queue *qp 	- pointer to queue
 *	clms_cli_tr_data_t *datap		- pointer to data
 *
 * Returns:
 *	0	The entry wasn't on the list.
 *	1	The entry was already on the list and was dropped.
 */
int
clms_put_trans_queue_l(struct clms_client_trans_queue *qp,
		       clms_cli_tr_data_t *datap)
{
	clms_cli_tr_data_t *tmp;

	if (qp->first == NULL) {
		SSI_ASSERT(qp->size == 0);
		datap->next = NULL;
		datap->prev = NULL;
		qp->first = datap;
		qp->last = datap;
		qp->size = 1;
		return(0);
	}
	tmp = qp->first;
	while (tmp != NULL) {
		/*
		 * If the transition is already here, just get out, we're
		 * done.  This won't happen under normal circumstances, but
		 * it might happen during failover, so we need to handle it.
		 */
		if (datap->ctv.transid == tmp->ctv.transid)
			return(1);
		if (datap->ctv.transid < tmp->ctv.transid)
			break;
		tmp = tmp->next;
	}
	if (tmp == NULL) {	/* Append to the list. */
		tmp = qp->last;
		SSI_ASSERT(tmp);
		datap->next = NULL;
		datap->prev = tmp;
		tmp->next = datap;
		qp->last = datap;
		qp->size++;
		SSI_ASSERT(qp->size <= CLMS_TRANS_HIST_SZ);
		return(0);
	}
	datap->next = tmp;
	datap->prev = tmp->prev;
	if (tmp->prev == NULL) {
		SSI_ASSERT(qp->first == tmp);
		qp->first = datap;
	}
	else
		tmp->prev->next = datap;
	tmp->prev = datap;
	qp->size++;
	SSI_ASSERT(qp->size <= CLMS_TRANS_HIST_SZ);
	return(0);
}

/*
 * clms_copy_trans_queue_l()
 *	Copy the contents of the passed clms_client_trans_queue into a passed
 *	buffer.
 *
 * Description:
 *	This function copies all entries in the passed clms_client_trans_queue
 *	into the passed buffer.  The buffer _must_ be large enough to contain
 *	the entire queue.
 *
 * Calling/Exit State:
 *	No locks are held. It is the responsibility of the caller to
 *	hold the queue locks.
 *
 * Parameters:
 *	struct clms_client_trans_queue *qp	Pointer to queue.
 *	clms_cli_tr_data_t *buffer		Buffer to receive the entries.
 *
 * Returns
 *	The passed buffer contains the contents of the queue.
 */
void
clms_copy_trans_queue_l(
	struct clms_client_trans_queue *qp,
	clms_cli_tr_data_t *buffer)
{
	int i;
	clms_cli_tr_data_t *datap;

	for (i = 0, datap = qp->first; datap != NULL; i++, datap = datap->next)
		buffer[i] = *datap;
}

/*
 * clms_isnode_inlist()
 *	Determine if a node is in a list.
 *
 * Description:
 *	This function is used to determine if a node is present in a
 *	given list of ndoes.
 **
 * Parameters:
 *	clusternode_t	*nodelist	- pointer to pointer to list of nodes
 *	int		size		- size of the above array
 *	clusternode_t	node		- the node in question
 *
 * Returns:
 *	Retuns a 1 if the node is present in the list, 0 otherwise.
 */
int
clms_isnode_inlist(clusternode_t *nodelist, int size,  clusternode_t node)
{
	int i;

	if (!nodelist)
		return 0;

	for (i=0; i < size; i++)
		if (nodelist[i] == node)
			return 1;

	return 0;
}

/*
 * clms_rmv_partial_nodeup()
 *	Remove a node from the partial nodeup queue.
 *
 * Description:
 *	This function removes a node from the partial nodeup queue.
 *	This is used during "Node Down" processing.
 *
 * Parameters:
 *	clusternode_t	node
 *
 */
int
clms_rmv_partial_nodeup(clusternode_t node)
{
	struct clms_transition_data *tmp;

	LOCK_COND_LOCK(&clms.clms_partial_nodeup_queue.cq_lock);
	tmp = clms.clms_partial_nodeup_queue.cq_first;
	while (tmp) {
		if (tmp->node == node) {
			clms_rmvq_trans_l(&clms.clms_partial_nodeup_queue,tmp);
			break;
		}
		tmp = tmp->next;
	}
	UNLOCK_COND_LOCK(&clms.clms_partial_nodeup_queue.cq_lock);

	return 0;
}

/*
 * clms_process_partial_nodeup()
 *	Process the partial nodeup queue.
 *
 * Description:
 *	Checks the partial nodeup queue. This is done at the end of "Node Down"
 *	processing. If the node is found in the partial queue, it is moved into
 *	the "Node Up Queue" and the "CLMS Node Up" daemon is waken up.
 *
 *	This function is applicable to CLMS Master only.
 *
 * Calling/Exit State:
 *
 *
 * Parameters:
 *	None.
 *
 */
int
clms_process_partial_nodeup(clusternode_t node)
{
	struct clms_transition_data *tmp;
	int error;

	LOCK_COND_LOCK(&clms.clms_partial_nodeup_queue.cq_lock);
	tmp = clms.clms_partial_nodeup_queue.cq_first;
	while (tmp) {
		if (tmp->node == node) {
			icsinfo_t *icsinfo;

			clms_rmvq_trans_l(&clms.clms_partial_nodeup_queue,tmp);
			UNLOCK_COND_LOCK(
				&clms.clms_partial_nodeup_queue.cq_lock);
			/*
			 * Set the icsinfo for the node, so we can communicate
			 * with him later.  If there's an error, we must have
			 * lost him, just toss the transition data, do an
			 * ics_nodedown() to make sure ICS state is consistent
			 * and ignore the nodeup.
			 */
			icsinfo = tmp->private_data;
			if ((error = ics_seticsinfo(node, icsinfo)) != 0) {
				clms_set_node_status(tmp->node, CLMS_NODE_DOWN);
				/* kfree(icsinfo); */
				clms_free_trans_data(tmp);
				ics_nodedown(node);
				return(0);
			}
			kfree(icsinfo);
			tmp->private_data = NULL;
			LOCK_COND_LOCK(&clms.clms_node_lock);
			clms_set_node_status_l(tmp->node, CLMS_NODE_COMING_UP);
			clms.clms_node_info[tmp->node].flags |=
						   (CLMS_NODE_INFORM_NOT_DONE |
						    CLMS_NODE_INFORM_NOT_READY);
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			LOCK_COND_LOCK(&clms.clms_nodeup_queue.cq_lock);
			clms_putq_trans_l(&clms.clms_nodeup_queue, tmp);
			SIGNAL_CONDITION(&clms.clms_nodeup_queue.cq_cond);
			UNLOCK_COND_LOCK(&clms.clms_nodeup_queue.cq_lock);
			return 0;
		} else {
			tmp = tmp->next;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_partial_nodeup_queue.cq_lock);

	return 0;
}

/*
 * clms_waitfor_client_callbacks()
 *	Waits for a set of nodes to call back saying they have
 *	completed the processing.
 *
 * Description:
 *	This function is used to wait for a set of nodes to call back.
 *
 *	For example, the CLMS Coordinator calls all the nodes in the
 *	cluster informing them of a "Node Down" event. It them waits
 *	for all of them to call back. It uses this function.
 *
 *	The nodes are marked in a list. They are deleted from the
 *	list once they have called back. They are also deleted from the
 *	list in the event that they have gone down.
 *
 *	This function will return once all the nodes have been accounted
 *	for.
 *
 * Parameters:
 *	cdp	The transition data for the transition being waited for.
 *	flag	Flag indicating which nodelist to use.  If 0, use
 *		called_cnlist, else use ready_cnlist.
 *
 * Returns:
 *	None.
 */
void
clms_waitfor_client_callbacks(
	struct clms_transition_data *cdp,
	int flag)
{
	LOCK_COND_LOCK(&clms.clms_node_lock);
	while (clms_check_client_callbacks(cdp, flag) == 0) {
		WAIT_CONDITION(&clms.clms_node_cond, &clms.clms_node_lock);
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
}

/*
 * clms_check_client_callbacks()
 *	Check if a set of nodes have called back informing that they
 *	have completed the processing.
 *
 * Description:
 *	This function checks if a set of nodes have called back.
 *	The set of nodes are contained in a list. If a node has
 *	called back or has gone down it is deleted from the list.
 *
 *	It returns a 1 if all the nodes have been accounted for.
 *	Otherwise it returns a 0.
 *
 * Calling/Exit State:
 *	struct clms_transition_data	*cdp - pointer to the clms transition
 *					       data structure.
 *
 * Parameters:
 *	cdp	The transition data for the transition being checked.
 *	flag	Flag indicating which nodelist to use.  If 0, use
 *		called_cnlist, else use ready_cnlist.
 *
 * Returns:
 *	1 if all nodes have called back (or gone down) and 0 otherwise.
 */
static int
clms_check_client_callbacks(
	struct clms_transition_data *cdp,
	int flag)
{
	int	all_called_back;
	clusternode_t	*tmp, *start, *end;

	/*
	 * First scan both lists for nodes that have gone down and clear
	 * those entries.  The ready_cnlist isn't guaranteed to be present.
	 */
	if (cdp->ready_cnlist != NULL) {
		for (tmp = cdp->ready_cnlist->n_rptr;
		     tmp != NULL && tmp != cdp->ready_cnlist->n_wptr; tmp++) {
			if (*tmp == CLUSTERNODE_INVAL)
				continue;
			/*
			 * If a node has gone down, pretend that it has
			 * called back.
			 */
			if (clms_isnodedown(*tmp)) {
				*tmp = CLUSTERNODE_INVAL;
				continue;
			}
		}
	}
	for (tmp = cdp->called_cnlist->n_rptr;
				tmp != cdp->called_cnlist->n_wptr ; tmp++) {
		if (*tmp == CLUSTERNODE_INVAL)
			continue;
		if (clms_isnodedown(*tmp)) {
			*tmp = CLUSTERNODE_INVAL;
			continue;
		}
	}
	all_called_back = 1;
	if (flag) {
		SSI_ASSERT(cdp->ready_cnlist != NULL);
		start = cdp->ready_cnlist->n_rptr;
		end = cdp->ready_cnlist->n_wptr;
	}
	else {
		SSI_ASSERT(cdp->called_cnlist != NULL);
		start = cdp->called_cnlist->n_rptr;
		end = cdp->called_cnlist->n_wptr;
	}
	for (tmp = start; tmp != end; tmp++) {
		if (*tmp == CLUSTERNODE_INVAL)
			continue;
		all_called_back = 0;
		break;
	}
	return(all_called_back);
}


/*
 * CLMS Kernel Interface (CKI).
 */

/*
 * clms_nodeup()
 *	Set this node to fully up state.
 *
 * Description:
 *	This sets a client node to fully up state.  This happens when all
 *	user-lvel initialization is done and the node is ready for business.
 *	Until this point, the node is basically up with respect to the
 *	cluster, but the node isn't yet ready for users and all rexecs/
 *	rforks/migrates except from noded/init are failed with -EAGAIN.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	clusternode_t	node	- the node coming up.
 */
void
clms_nodeup(clusternode_t node)
{
	clms_set_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_node_info[node].prev_state = clms.clms_node_info[node].current_state;
	clms.clms_node_info[node].current_state = CLMS_NODE_UP;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	clms_clear_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
}

/*
 * clms_node_halfdown()
 *	Set this node to "half down" state.
 *
 * Description:
 *	This sets a client node to "half down" state.  This happens when a
 *	node is in the process of shutting down, is still part of the cluster,
 *	but must allow no new rexecs/rforks/migrates except (possibly) from
 *	noded/init.  The node will probably leave the cluster shortly.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	clusternode_t	node	- the node shutting down.
 */
void
clms_node_halfdown(clusternode_t node)
{
	clms_set_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (clms.clms_node_info[node].current_state != CLMS_NODE_HALF_DOWN) {
		clms.clms_node_info[node].prev_state =
					clms.clms_node_info[node].current_state;
		clms.clms_node_info[node].current_state = CLMS_NODE_HALF_DOWN;
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	clms_clear_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
}

/*
 * clms_node_halfup()
 *	Bring the node to "half up" state.
 *
 * Description:
 *	This function is called when an indication from ICS is received
 *	about a possible node joining the cluster. This function sets the
 *	"clms node transition in progress" flag so that no other node
 *	transition can occur until we have made enough processing.
 *
 *	The appropriate function is called depending on if this is a
 *	clms master or client.
 *
 * Calling/Exit State:
 *	The "clms node transition in progress" flag is set so that
 *	 no other "Node Up" or "Node Down" can occur for the duration of
 *	this function. It is cleared at the end.
 *
 * Parameters:
 *	clusternode_t node	- the node that is coming up.
 *
 */
void
clms_node_halfup(
	clusternode_t node,
	icsinfo_t *icsinfo)
{
	clms_set_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
	if (this_node == clms_master_node)
		clms_master_nodeup(node, icsinfo);
	else
		clms_client_nodeup(node);
	clms_clear_op_inprogress(CLMS_NTRANS_STATE_CHANGE);

	return;
}

#if defined(DEBUG) || defined(DEBUG_TOOLS)
/*
 * clms_nodedebug()
 *	Exported routine provided for the ssisys_node_down system call
 *	to put a node in debug mode. Has to be called from thread context.
 *
 * Description:
 *	If the node is invalid, say so.
 *	If a specific node is specified and it's up call
 *		if it's this node,
 *			enter the debugger now
 *		else
 *			call clms_do_nodedebug_one()
 *		return
 *
 *	Else (node == 0)
 *		Get upnode list
 *		For each upnode,
 *			skip this node
 *			make sure it's still up
 *			call clms_do_nodedebug_one()
 *		free upnode list
 *		enter debugger for this node
 *	return
 *
 * Parameters:
 *	clusternode_t node	- the node to be taken to debug.
 *
 */
int
clms_nodedebug(clusternode_t node)
{
	int state;
	clusternode_t ni;
	struct clms_nodelist	*cnlist;

	if ((node < 0) || (node > NSC_MAX_NODE_VALUE))
		return -EINVAL;  /* invalid node number */

	if (node != CLUSTERNODE_THIS) {
		if (node != this_node) {
			state = clms_get_node_status(node);
			if (!(state & CLMS_NODE_UP))
				return -EBUSY;
			clms_do_nodedebug_one(node);
		}
		return 0;
	}

	/*
	 * Special case Node == 0, do ALL up nodes
	 */
	cnlist = clms_get_up_nodelist();
	if (!cnlist)
		return -EAGAIN;
	while ((ni = clms_get_next_node(cnlist)) != CLUSTERNODE_INVAL) {
		if (ni == this_node)
			continue;
		state = clms_get_node_status(ni);
		if (!(state & (CLMS_NODE_UP)) )	/* no longer up, skip */
			continue;
		clms_do_nodedebug_one(ni);
	}
	clms_free_nodelist(cnlist);

	/*
	 * Finally, do this node
	 */

	return 0;

}


/*
 * clms_do_nodedebug_one()
 *	Local routine provided for the ssisys_node_down system call
 *	to put a node in debug mode. Has to be called from thread context.
 *
 * Description:
 *	If the specified node is the clms master, warn.
 *
 * Parameters:
 *	clusternode_t node	- the node to be taken to debug.
 *
 */
static void
clms_do_nodedebug_one(clusternode_t node)
{

	SSI_ASSERT(node != this_node);

	if (node == clms_master_node)
		printk(KERN_WARNING "CLMS master going to debug.\n");

	RCLMS_YOU_ARE_DEBUG_MSG(node);

}

#endif /* DEBUG || DEBUG_TOOLS */

/*
 * clms_do_nodedown()
 *	Exported routine provided for the ssisys_node_down system call
 *	to down a node. Has to be called from thread context.
 *
 * Description:
 *	The CLMS master node cannot be brought dowm. If the node is
 *	the local node, the routine simply panics. Otherwise, if the
 *	node is up, we shoot the node via ics_shoot_node().
 *	This causes the node to panic. The node down is then
 *	detected by the Regroup Module as in any other nodedown
 *	situation.
 *
 * Parameters:
 *	clusternode_t node	- the node to be taken down.
 *	int flag		- CLMS_NODEDOWN_DOWN or CLMS_NODEDOWN_HALT
 *
 */
int
clms_do_nodedown(clusternode_t node, int flag)
{
	int		state;
	int		error = 0;

	SSI_ASSERT(flag == CLMS_NODEDOWN_DOWN || flag == CLMS_NODEDOWN_HALT);

	/* node number valid? */
	if ((node < 0) || (node > NSC_MAX_NODE_VALUE))
		return (-EINVAL);	/* No. */

	/* translate special case */
	if (node == CLUSTERNODE_THIS)
		node = this_node;

	/* Checks and warnings. */

	/* Is target clms master node? (We assume it's up.) */
	if (node == clms_master_node) {
		/* Yes. Are we that node? */
		if (this_node != clms_master_node) {
			/* No, just warn and shoot */
			printk(KERN_WARNING "Shooting the CLMS master node!\n");
		} else {
			/* Yes, warn and go down */
			if (flag == CLMS_NODEDOWN_HALT) {
				printk(KERN_WARNING
				       "Halting CLMS master node.\n");
			} else {
				printk(KERN_WARNING
				       "Rebooting CLMS master node.\n");
			}
		}

	} else {
		/* Is it us?  (We must be up.)*/
		if (node == this_node) {
			/* Yes, warn and go down */
			if (flag == CLMS_NODEDOWN_HALT) {
				printk(KERN_WARNING
				       "Halting due to nodedown.\n");
			} else {
				printk(KERN_WARNING
				       "Rebooting due to nodedown.\n");
			}
		} else {
			/* No, some other node */
			state = clms_get_node_status(node);
			/* Is it at least partially up? */
			if (!(state & (CLMS_NODE_COMING_UP
				   | CLMS_NODE_UP
				   | CLMS_NODE_HALF_UP))) {
				/* No. Sorry */
				error = -EAGAIN;
			} else {
				/* Yes. */
				if ((flag == CLMS_NODEDOWN_HALT)
				    && !(state & CLMS_NODE_UP))
					/* Can't halt if not fully up */
			    		error = -EBUSY;
			}
		}
	}

	/* If no error found, ready to shoot something. */
	if (!error) {
		/* Are we shooting ourselves? */
		if (node == this_node) {
			/* Yes. */
			if (flag == CLMS_NODEDOWN_HALT)
				machine_halt();
			else
				machine_restart(NULL);
			SSI_ASSERT(FALSE);
			/* NOT REACHED */
		} else {
			/* No. Some other node */
			ics_shoot_node(node);
		}
	}

	return error;
}

/*
 * clms_nodedown()
 *	Start a "Node Down" processing.
 *
 * Description:
 *	Set the "Node Transition in progress flag" and call the
 *	clms_nodedown_l() to do the real work.
 *
 * Calling/Exit State:
 *	The "clms node transition in progress" flag is set so that
 *	 no other "Node Up" or "Node Down" can occur for the duration of
 *	this function. It is cleared at the end.
 *
 * Parameters:
 *	clusternode_t node	- the node that has gone down.
 *
 */
void
clms_nodedown(clusternode_t node)
{
#ifdef DEBUG
	printk(KERN_NOTICE "!clms_nodedown (START): %ld\n", TICKS());
#endif

	clms_set_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
	clms_nodedown_l(node);
	clms_clear_op_inprogress(CLMS_NTRANS_STATE_CHANGE);

#ifdef DEBUG
	printk(KERN_NOTICE "!clms_nodedown (END): %ld\n", TICKS());
#endif

	return;
}

/*
 * clms_nodedown_l()
 *	Start a "Node Down" processing for a node.
 *
 * Description:
 *	This function is called when an indication is received
 *	that a node may have gone down.  This function sets the
 *	"clms node transition in progress" flag so that no other node
 *	transition can occur until we have made enough processing.
 *
 *	The appropriate function is called depending on if this is a
 *	clms master or client.
 *
 * Calling/Exit State:
 *	All locks are caller's responsibility. No locks are held by
 *	this function.
 *
 * Parameters:
 *	clusternode_t	node	- the node that may have gone down
 */
void
clms_nodedown_l(clusternode_t node)
{
	if (node == clms_master_node) {
		printk(KERN_WARNING "clms_master_nodedown: CLMS Master "
		       " node cannot be taken down\n");
	} else {
		if (this_node == clms_master_node)
			clms_master_nodedown(node);
		else
			clms_client_nodedown(node);
 	}
}

/*
 * clms_isnodedown()
 *	Determine if a node is down.
 *
 * Description:
 * 	This function returns a 1 if the node is never up, going down,
 *	or down.  It returns a 0 otherwise.
 *
 *	Note that a return value of 1 from this function only means that
 *	the node is (i) fully down or (ii) never up or (iii) going down.
 *
 *	A return value of 0 from this function only means  that the node is
 *	not in any of the 3 states mentioned above. It does not mean that
 *	the node is fully up as there are other intermediate states.
 *	Use clms_isnodeup() to determine if a node is fully up.
 *
 * Parameters:
 *	clusternode_t	node	- the node in question
 *
 * Returns:
 *	1 if the node is down.
 *	0 otherwise.
 */
int
clms_isnodedown(clusternode_t	node)
{
	return (!!(clms_get_node_status_nolock(node)
		   & (CLMS_NODE_DOWN|CLMS_NODE_GOING_DOWN|CLMS_NODE_NEVER_UP)));
}

/*
 * clms_isnodeup()
 *	Determine if a node is fully up.
 *
 * Description:
 * 	This function returns a 1 if the node is fully up.
 *	It returns a 0 otherwise.
 *
 *	A return value of 0 from this function only means that the node is
 *	not fully up. It does not mean that the node is down as the node
 *	may be coming up or "half" up.
 *	Use clms_isnodedown() to determine if a node is down.
 *
 * Parameters:
 *	clusternode_t	node	- the node in question
 *
 * Returns:
 *	1 if the node is fully up.
 *	0 otherwise.
 */
int
clms_isnodeup(clusternode_t node)
{
	return (!!(clms_get_node_status_nolock(node) & CLMS_NODE_UP));
}

/*
 * clms_ishalfup()
 *	Determine if a node is "half" up.
 *
 * Description:
 * 	This function returns a 1 if the node is "half" up (that is,
 *	functionally up as far as the kernel is concerned, but not yet
 *	ready for users).  It returns a 0 otherwise.
 *
 *	A return value of 0 from this function only means  that the node is
 *	not "half" up.  The node may be coming up or fully up.
 *	Use clms_isnodedown() to determine if a node is down.
 *
 * Parameters:
 *	clusternode_t	node	- the node in question
 *
 * Returns:
 *	1 if the node is "half" up.
 *	0 otherwise.
 */
int
clms_ishalfup(clusternode_t node)
{
	return (!!(clms_get_node_status_nolock(node) & CLMS_NODE_HALF_UP));
}

/*
 * clms_ishalfdown()
 *	Determine if a node is "half" down.
 *
 * Description:
 * 	This function returns a 1 if the node is "half" down (that is,
 *	functionally up as far as the kernel is concerned, but not
 *	to be used by users).  It returns a 0 otherwise.
 *
 *	A return value of 0 from this function only means  that the node is
 *	not "half" down.
 *
 * Parameters:
 *	clusternode_t	node	- the node in question
 *
 * Returns:
 *	1 if the node is "half" down.
 *	0 otherwise.
 */
int
clms_ishalfdown(clusternode_t node)
{
	return (!!(clms_get_node_status_nolock(node) & CLMS_NODE_HALF_DOWN));
}

/*
 * clms_isnodeavail()
 *	Determine if a node is in the cluster.
 *
 * Description:
 * 	This function returns a 1 if the node is in the cluster, that
 *	is, if it has a state of CLMS_NODE_HALF_UP, CLMS_NODE_UP, or
 *	CLMS_NODE_HALF_DOWN.  It returns a 0 otherwise.
 *
 *	A return value of 0 from this function only means that the node is
 *	part of the cluster. It does not mean that the node is either fully
 *	up (the node may be half up or half down) or down (the node may be
 *	coming up).
 *
 *	Use clms_isnodedown() to determine if a node is down, or
 *	clms_isnodeup() to determine if a node is fully up.
 *
 * Parameters:
 *	clusternode_t	node	- the node in question
 *
 * Returns:
 *	1 if the node is in the cluster.
 *	0 otherwise.
 */
int
clms_isnodeavail(clusternode_t node)
{
	return (!!(clms_get_node_status(node)
		   & (CLMS_NODE_UP|CLMS_NODE_HALF_UP|CLMS_NODE_HALF_DOWN)));
}

/*
 * clms_isnodeavailrdev()
 *	Determine if a node is in the cluster far enough for the remote
 *	driver access to work properly.
 *
 * Description:
 * 	This function returns a 1 if the node is in the cluster, that
 *	is, if it has a state of CLMS_NODE_HALF_UP, CLMS_NODE_UP,
 *	CLMS_NODE_HALF_DOWN, or CLMS_NODE_ICS_UP.  It returns a 0 otherwise.
 *
 *	A return value of 0 from this function only means that the node is
 *	part of the cluster. It does not mean that the node is either fully
 *	up (the node may be half up or half down) or down (the node may be
 *	coming up).
 *
 *	Use clms_isnodedown() to determine if a node is down, or
 *	clms_isnodeup() to determine if a node is fully up.
 *
 * Parameters:
 *	clusternode_t	node	- the node in question
 *
 * Returns:
 *	1 if the node is in the cluster.
 *	0 otherwise.
 */
int
clms_isnodeavailrdev(clusternode_t node)
{
	return (!!(clms_get_node_status(node) & (CLMS_NODE_ICS_UP|CLMS_NODE_UP|
						 CLMS_NODE_HALF_UP|
						 CLMS_NODE_HALF_DOWN)));
}

/*
 * clms_set_node_status()
 *	Set the current state of a node.
 *
 * Description:
 *	This function is called to set the current state of a node.
 *	This function gets the appropriate lock and calls the
 *	clms_set_node_status_l() function.
 *
 * Calling/Exit State:
 *	clms.clms_node_lock is acquired and released by this function.
 *
 * Parameters:
 *	clusternode_t	node	- node number
 *	int		status	- current state of the node
 *
 * Returns:
 *	The previous state of the node.
 */
int
clms_set_node_status(clusternode_t node, int status)
{
	int prev_state;

	LOCK_COND_LOCK(&clms.clms_node_lock);
	prev_state = clms_set_node_status_l(node, status);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return(prev_state);
}

/*
 * clms_set_node_status_l()
 *	Set the current state of the node.
 *
 * Description:
 *	This function does the actual work of setting the current state of the
 *	given node. If an invalid state change is requested, a -EINVAL is
 *	returned. As a side effect, the previous state also is set here.
 *	If an unknown state is given as the argument, this function panics.
 *
 *	All contexts waiting for a state change are woken up.
 *
 * Calling/Exit State:
 *	The caller has to hold the appropriate locks.
 *
 * Parameters:
 *	clusternode_t	node	- node number
 *	int		state	- current state desired

 * Returns:
 *	The previous state of the node.
 */
int
clms_set_node_status_l(clusternode_t node, int state)
{
	int current_state;

	SSI_ASSERT_LOCKED_COND_LOCK(&clms.clms_node_lock);
	current_state = clms.clms_node_info[node].current_state;

	switch (state) {
		case CLMS_NODE_HALF_UP:
			if (!(current_state & CLMS_NODE_COMING_UP)) {
				panic("clms_set_node_status_l: "
				      " Invalid State %x\n",
				      current_state);
				return -EINVAL;
			}
			break;

		/*
		 * If we're going to COMING_UP from INFORM_COMING_UP,
		 * forget INFORM_COMING_UP so we keep the previous state as
		 * DOWN or NEVER_UP.
		 */
		case CLMS_NODE_COMING_UP:
			if (current_state & CLMS_NODE_INFORM_COMING_UP)
				current_state =
					   clms.clms_node_info[node].prev_state;
			break;

		/*
		 * If we're going to INFORM_COMING_UP from REGROUP_KNOWN,
		 * forget REGROUP_KNOWN so we keep the previous state as
		 * DOWN or NEVER_UP.  Cheating, yes, but lets us keep things
		 * straight if the node goes down.
		 */
		case CLMS_NODE_INFORM_COMING_UP:
			if (current_state & CLMS_NODE_REGROUP_KNOWN)
				current_state =
					   clms.clms_node_info[node].prev_state;
			break;

		case CLMS_NODE_PARTIAL_COMING_UP:
		case CLMS_NODE_REGROUP_KNOWN:
		case CLMS_NODE_GOING_DOWN:
		case CLMS_NODE_DOWN:
		case CLMS_NODE_NEVER_UP:
		case CLMS_NODE_UP:
			break;

		default:
			panic("clms_set_node_status: Unknown "
			      " State %d \n", state);
			return -EINVAL;
	}

	clms.clms_node_info[node].prev_state = current_state;
	clms.clms_node_info[node].current_state = state;
	BROADCAST_CONDITION(&clms.clms_node_cond);

	return(current_state);
}

/*
 * clms_maintain_statistics()
 *	Maintain cluster statistics based on the node state.
 *
 * Description:
 *	This function maintains various cluster statistics based on the
 *	new state of a node.
 *
 * Calling/Exit State:
 *	clms.clms_node_lock held.
 *
 * Parameters:
 *	clusternode_t	node	Node changing state.
 *	int		state	New state of the node.

 * Returns:
 *	None.
 */
void
clms_maintain_statistics(
	clusternode_t node,
	int state,
	int prev_state)
{
	SSI_ASSERT(node >= 1 && node <= NSC_MAX_NODE_VALUE);
	SSI_ASSERT_LOCKED_COND_LOCK(&clms.clms_node_lock);
	CLMSLOG(CLMSLOG_STATS, node, state, prev_state, 0, 0, 0, 0);
	switch (state) {
		case CLMS_STATE_UP:
			clms.clms_cluster_stat.clms_num_nodes_up++;
			break;

		case CLMS_STATE_COMINGUP:
			clms.clms_node_info[node].api_state.up_transitions++;
			clms.clms_cluster_stat.clms_num_nodes++;
			clms.clms_cluster_stat.clms_num_up_transitions++;
			break;

		case CLMS_STATE_KCLEANUP:
			if (prev_state == CLMS_STATE_UP ||
			    prev_state == CLMS_STATE_SHUTDOWN ||
			    prev_state == CLMS_STATE_GOINGDOWN)
				clms.clms_cluster_stat.clms_num_nodes_up--;
			clms.clms_node_info[node].api_state.down_transitions++;
			clms.clms_cluster_stat.clms_num_down_transitions++;
			break;

		case CLMS_STATE_DOWN:
			clms.clms_cluster_stat.clms_num_nodes--;
			break;
	}
}

/*
 * Returns the current state of the node.
 */
int
clms_get_node_status_nolock(clusternode_t node)
{
	if ( node < 1 || node > NSC_MAX_NODE_VALUE )
		return CLMS_NODE_NEVER_UP;
	return clms.clms_node_info[node].current_state;
}

int
clms_get_node_status(clusternode_t node)
{
	int state, locked;

	if ( node < 1 || node > NSC_MAX_NODE_VALUE )
		return CLMS_NODE_NEVER_UP;

	if (!(locked = LOCK_COND_LOCKOWNED(&clms.clms_node_lock)))
		LOCK_COND_LOCK(&clms.clms_node_lock);

	state = clms.clms_node_info[node].current_state;
	if (!locked)
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return state;
}

/*
 * Returns the previous state of the node. Some CLMS routines may use this
 * subroutine.
 */
/* Called with clms_node_lock locked */
int
clms_get_node_prev_status(clusternode_t node)
{
	SSI_ASSERT_LOCKED_COND_LOCK(&clms.clms_node_lock);
	return clms.clms_node_info[node].prev_state;
}

/*
 * Return the number of nodes in the cluster.
 */
int
clms_get_cluster_num_nodes()
{
	return(clms.clms_cluster_stat.clms_num_nodes);
}

/*
 * Return the number of nodes in the cluster.
 */
int
clms_get_cluster_num_nodes_up(void)
{
	return(clms.clms_cluster_stat.clms_num_nodes_up);
}

/*
 * Returns the number of times a particular node booted or came up.
 */
int
clms_get_node_num_uptrans(clusternode_t node)
{
	return clms.clms_node_info[node].api_state.up_transitions;
}

/*
 * Returns the number of times "Node Up" event occurred in this cluster.
 */
int
clms_get_cluster_num_uptrans(void)
{
	return (clms.clms_cluster_stat.clms_num_up_transitions);
}


/*
 * Returns the number of times a particular node went down.
 */
int
clms_get_node_num_downtrans(clusternode_t node)
{

	return(clms.clms_node_info[node].api_state.down_transitions);

}

/*
 * Returns the number of "Node Down" events in the cluster.
 */
int
clms_get_cluster_num_downtrans(void)
{
	return (clms.clms_cluster_stat.clms_num_down_transitions);
}

/*
 * Saves the the number of cluster-wide "Node Down" events as passed to
 * clms_client_nodedown_thread() by the CLMS master, only if the passed
 * value is greater than the saved value.  Returns 1 if the value was
 * saved, zero otherwise.
 */
int
clms_test_and_save_cluster_downtrans(
	int down_trans)
{
	int ret_val;

	ret_val = 0;
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (down_trans > clms.clms_cluster_stat.clms_save_down_transitions) {
		clms.clms_cluster_stat.clms_save_down_transitions = down_trans;
		ret_val = 1;
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return(ret_val);
}

/*
 * clms_get_up_nodelist()
 *	Get the list of nodes that are upin struct clms_nodelist.
 *
 * Description:
 * 	This function allocates a struct clms_nodelist. It then collects
 *	all the nodes that are up and stores them in the list. It returns
 *	the list.  It returns a NULL if no memory is available.
 *
 * 	If during the execution of this function more nodes come up
 *	than there is space for, the number of nodes in the list will be
 *	less that the actual number of nodes that are up. It is caller's
 *	responsibility to deal with the situation.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	A pointer to the struct clms_nodelist on success. NULL on failure.
 *
 * NSC_XXX:
 *	Some kind of indication has to be given to the caller that the
 *	number of nodes in the list is less than the actual number of
 *	nodes in the cluster, if indeed that is the case.
 */
struct clms_nodelist *
clms_get_up_nodelist()
{
	struct clms_nodelist *cnlist;
	int	num_nodes;
	int	i;
	int 	node_count;

	num_nodes = clms.clms_cluster_stat.clms_num_nodes;

	num_nodes = max(NSC_MAX_NODE_VALUE, num_nodes+16);

	cnlist = clms_alloc_nodelist(num_nodes, CLMS_NODE_UP|CLMS_NODE_HALF_UP|CLMS_NODE_HALF_DOWN);

	if (cnlist) {
		LOCK_COND_LOCK(&clms.clms_node_lock);
		node_count = 0;
		for (i=1; i <= NSC_MAX_NODE_VALUE; i++) {
			if (clms_isnodeavail(i)) {
				cnlist->nodelist[node_count] = i;
				cnlist->n_wptr++;
				node_count++;
			}
			if (node_count == num_nodes)
				break;
		}
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
	}

	return cnlist;
}

/*
 * clms_get_next_node()
 *	Get the next node from the list of nodes (struct clms_nodelist).
 *
 * Description:
 * 	This function returns the next node from the struct clms_nodelist.
 *	The internal pointers are adjusted. At end of list, it returns
 *	CLUSTERNODE_INVAL. This function is primarily used by the CLMS.
 *	Other NSC components use the struct nsc_nodelist.
 *
 * Parameters:
 *	struct clms_nodelist *cnlist	- list of nodes
 *
 * Returns:
 *	node number if a node is found. CLUSTERNODE_INVAL at end of list.
 */
clusternode_t
clms_get_next_node(struct clms_nodelist *cnlist)
{
	clusternode_t node;

	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (cnlist->n_rptr != cnlist->n_wptr) {
		node = *(cnlist->n_rptr);
		(cnlist->n_rptr)++;
	} else {
		node = CLUSTERNODE_INVAL;
	}

	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return node;
}

/*
 * clms_set_next_node()
 *	Set the given node at the end of the list.
 *
 * Description:
 * 	This function sets the given node at the end of the given list
 *	(struct clms_nodelist).
 *
 *	The internal pointers are adjusted. It returns -EINVAL if there is
 *	no space in the list.
 *
 *	This function is primarily used by the CLMS.
 *	Other NSC components use the struct nsc_nodelist.
 *
 * Parameters:
 *	struct clms_nodelist *cnlist	- list of nodes
 *	clusternode_t	     node	- node to add
 *
 * Returns:
 *	0 on success and -EINVAL on failure.
 */
int
clms_set_node(struct clms_nodelist *cnlist, clusternode_t node)
{
	if (cnlist->n_wptr == cnlist->n_lim)
		return -EINVAL;

	LOCK_COND_LOCK(&clms.clms_node_lock);
	*cnlist->n_wptr = node;
	cnlist->n_wptr++;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	return 0;
}

/*
 * clms_clear_node()
 *	Clear the given node from the given list.
 *
 * Description:
 * 	This function clears the given node from the given list
 *	(struct clms_nodelist).
 *
 *	The internal pointers are adjusted.
 *
 *	This function is primarily used by the CLMS.
 *	Other NSC components use the struct nsc_nodelist.
 *
 * Parameters:
 *	struct clms_nodelist *cnlist	- list of nodes
 *	clusternode_t	     node	- node to clear
 *
 * Returns:
 *	0 on success and -EINVAL on failure.
 */
int
clms_clear_node(struct clms_nodelist *cnlist, clusternode_t node)
{
	clusternode_t 	*tmp, *tmp1;
	int ret;

	LOCK_COND_LOCK(&clms.clms_node_lock);
	for (tmp = cnlist->n_rptr; tmp != cnlist->n_wptr ; tmp++) {
		if (*tmp == node)
			break;
	}
	if (tmp == cnlist->n_wptr) {
		ret = -EINVAL;
		goto out;
	}
	tmp1 = tmp;
	while (++tmp1 != cnlist->n_wptr) {
		*tmp = *tmp1;
		tmp = tmp1;
	}
	cnlist->n_wptr = tmp;
	ret = 0;
out:	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	return(ret);
}


/*
 * clms_get_nsc_nodelist_fc()
 *	Copy the list of nodes from a struct clms_nodelist to struct
 *	nsc_nodelist.
 *
 * Description:
 *	This function copies the list of nodes from a given
 *	struct clms_nodelist to a nsc_nodelist.  This is
 *	is required because other NSC components use struct
 *	nsc_nodelist (a bitmask array).  The list of nodes
 *	passed to surrogate_nodeup() and surrogate_nodedown()
 *	routines is of type struct nsc_nodelist.
 *
 * Parameters:
 *	struct clms_nodelist	*cnist	- source node list.
 */
nsc_nodelist_t *
clms_get_nsc_nodelist_fc(struct clms_nodelist *cnlist)
{
	nsc_nodelist_t *tnlist;
	clusternode_t *tmp;

	tnlist = NSC_NODELIST_ALLOC();
	SSI_ASSERT(tnlist);

	if (cnlist) {
		for (tmp = cnlist->n_rptr; tmp != cnlist->n_wptr; tmp++) {
			if (clms_isnodeavail(*tmp))
				NSC_NODELIST_SET1(tnlist, *tmp);
		}
	}

	return tnlist;
}

/*
 * clms_get_nsc_nodelist()
 *	Get a list of nodes in the cluster, which are on a specific state.
 *
 * Description:
 *	This function returns a list of nodes matching the state specified
 *	by the flag parameter.
 *
 * Parameters:
 *	int			flag	- state of the nodes in the list (I)
 */
nsc_nodelist_t *
clms_get_nsc_nodelist(int flag)
{
	nsc_nodelist_t *tnlist;
	int i, state;

	tnlist = NSC_NODELIST_ALLOC();
	SSI_ASSERT(tnlist);

	LOCK_COND_LOCK(&clms.clms_node_lock);
	for (i=1; i <= NSC_MAX_NODE_VALUE; i++) {
		state = clms.clms_node_info[i].current_state;
		if (state & flag) {
			NSC_NODELIST_SET1(tnlist, i);
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	return tnlist;
}

/*
 * clms_get_clms_nodelist()
 *
 * Description:
 *	This function returns a list of the clms nodes, i.e. master and
 *	all secondaries, matching the state specified by the flag parameter.
 *
 * Parameters:
 *	int			flag	- state of the nodes in the list (I)
 */
nsc_nodelist_t *
clms_get_clms_nodelist(int flag)
{
	nsc_nodelist_t *tnlist;
	clms_secondary_list_t *ent;
	int state;

	tnlist = NSC_NODELIST_ALLOC();
	SSI_ASSERT(tnlist);

	LOCK_COND_LOCK(&clms.clms_node_lock);
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next) {
		state = clms.clms_node_info[ent->node].current_state;
		if (state & flag)
			NSC_NODELIST_SET1(tnlist, ent->node);
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);

	return tnlist;
}

/*
 * rclms_cold_boot()
 *	Server routine for RCLMS_COLD_BOOT RPC.
 *
 * Description:
 *	This is the server routine for the RCLMS_COLD_BOOT RPC that
 *	is sent from CLMS client nodes to the CLMS master node.
 *
 *	The CLMS CLIENT sends this rpc to find out if this is the
 *	cluster is being newly formed; the decision is made by
 *	determing if all the CLMS cluster global data is known.
 *
 * Calling/Exit State:
 *	RPC Server routine executed by the ICS Server Proc.
 *
 * Parameters:
 *	node		this node.
 *
 *	rval  		return value
 *
 *	cold_boot	Pointer to flag; non-zero => cold boot.
 *
 * Return value:
 *	Always 0.
 *
 */

int
rclms_cold_boot(
	clusternode_t	node,
	int		*rval,
	int		*cold_boot)
{
	SSI_ASSERT(this_node == clms_master_node);

	*rval = 0;
	*cold_boot = clms_master_cold_boot;

	return 0;
}

/*
 * clms_init_handle_registry()
 *	Initialize the CLMS handle registry support.
 *
 * Description:
 *	This routine just initializes the CLMS handle registry.
 *
 * Calling/exit state:
 *	None.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
void
clms_init_handle_registry(void)
{
	INIT_SPIN_LOCK(&clms_handle_registry_lock);
	clms_handle_registry = NULL;
}

/*
 * clms_register_handle()
 *	Register a CLMS handle that will be used when clients call back.
 *
 * Description:
 *	This routine records the passed CLMS handle in a linked list that
 *	is searched when a node sends a callback message.
 *
 * Calling/exit state:
 *	None.
 *
 * Parameters:
 *	u_long handle	The handle to register.
 *
 * Returns:
 *	None.
 */
void
clms_register_handle(u_long handle)
{
	clms_handle_registry_t *reg;

	SSI_ASSERT(clms_find_handle(handle) == 0);
	reg = kmalloc_nofail(sizeof(clms_handle_registry_t));
	reg->handle = handle;
	LOCK_SPIN_LOCK(&clms_handle_registry_lock);
	reg->next = clms_handle_registry;
	reg->prev = NULL;
	if (clms_handle_registry != NULL)
		clms_handle_registry->prev = reg;
	clms_handle_registry = reg;
	UNLOCK_SPIN_LOCK(&clms_handle_registry_lock);
}

/*
 * clms_deregister_handle()
 *	Deregister a CLMS handle.
 *
 * Description:
 *	This routine removes the passed CLMS handle from the CLMS handle
 *	registry.
 *
 * Calling/exit state:
 *	None.
 *
 * Parameters:
 *	u_long handle	The handle to register.
 *
 * Returns:
 *	None.
 */
void
clms_deregister_handle(u_long handle)
{
	clms_handle_registry_t *reg;

	LOCK_SPIN_LOCK(&clms_handle_registry_lock);
	reg = clms_handle_registry;
	while (reg != NULL) {
		if (reg->handle == handle)
			break;
		reg = reg->next;
	}
	SSI_ASSERT(reg != NULL);
	if (reg->prev != NULL)
		reg->prev->next = reg->next;
	if (reg->next != NULL)
		reg->next->prev = reg->prev;
	if (reg == clms_handle_registry)
		clms_handle_registry = reg->next;
	UNLOCK_SPIN_LOCK(&clms_handle_registry_lock);
	kfree(reg);
}

/*
 * clms_find_handle()
 *	Find a CLMS handle in the handle registry.
 *
 * Description:
 *	This routine finds the passed CLMS handle in the CLMS handle
 *	registry.
 *
 * Calling/exit state:
 *	None.
 *
 * Parameters:
 *	u_long handle	The handle to register.
 *
 * Returns:
 *	0	The handle was not found.
 *	1	The handle was found.
 */
int
clms_find_handle(u_long handle)
{
	clms_handle_registry_t *reg;

	LOCK_SPIN_LOCK(&clms_handle_registry_lock);
	reg = clms_handle_registry;
	while (reg != NULL) {
		if (reg->handle == handle)
			break;
		reg = reg->next;
	}
	UNLOCK_SPIN_LOCK(&clms_handle_registry_lock);
	return (reg != NULL);
}

/*
 * clms_going_away()
 *	Tell everyone this node is going away.
 *
 * Description:
 *	When this node is being rebooted or halted, we need to let other
 *	nodes know that that's happening so they will mark the transition
 *	properly.  This routine does that, using an RPC if this is an
 *	orderly shutdown (that is, if the current state is GOINGDOWN) or
 *	a message if this is a fast shutdown (that is, if the current state
 *	is anything else).
 *
 * Calling/exit state:
 *	None.
 *
 * Parameters:
 *	None.
 *
 * Returns:
 *	None.
 */
void
clms_going_away(void)
{
	int i;

	if (clms_master_node == this_node) {
		(void)rclms_going_away(this_node, &i, this_node);
		return;
	}
	if (clms_api_get_state(this_node) == CLMS_STATE_GOINGDOWN) {
		(void)RCLMS_GOING_AWAY(clms_master_node, &i, this_node);
	}
	else {
		(void)RCLMS_GOING_AWAY_FAST_MSG(clms_master_node, this_node);
		idelay(HZ);
	}
}

#ifdef SSI_XXX
/* Like the other trylock I don't know why a process would want
 * to do this and get EAGAIN just because some else is also doing this function
 * at the same time.  Originally we thought the trylock would be used in
 * the compare case.  Don't think this function would be useful anyway.
 *
 * Nobody currently uses this function.
 */
/*
 * clms_snap_trylock_ctv()
 *	Try to acquire the exclusive lock and return the CTV.
 *
 * Description:
 *	This function tries for the CTV exclusive lock, if succeeds, snaps
 *	the value of the CTV into the external CTV representation, and
 *	returns with the exclusive lock held. If it cant get the lock, it
 *	will return error and set ctv to NULL.
 *
 * Parameters:
 *	ctv_t *ctv	Area to receive the CTV.
 *
 * Returns:
 *	Succeeds: returns 0, with the RW exclusive lock is held.
 *	Fails: returns -EAGAIN, and ctv set to NULL.
 */
int
clms_snap_trylock_ctv(
	ctv_t *ctv)
{
	/* Try for the exclusive lock. */
	if (TRY_LOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock)) {
		clms_snap_unlocked_ctv(ctv);
		return 0;
	}

	return -EAGAIN;
}
#endif

/*
 * clms_snap_locked_ctv()
 *	Get the exclusive lock and return the CTV.
 *
 * Description:
 *	This function acquires the CTV exclusive lock, snaps the value of
 *	the CTV into the external CTV representation, and returns with the
 *	exclusive lock held.
 *
 * Parameters:
 *	ctv_t *ctv	Area to receive the CTV.
 *
 * Returns:
 *	The RW exclusive lock is held.
 */
void
clms_snap_locked_ctv(
	ctv_t *ctv)
{
	/* Get the exclusive lock. */
	LOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock);
	clms_snap_unlocked_ctv(ctv);
}

/*
 * clms_snap_unlocked_ctv()
 *	Return the CTV without locking the RW exclusive lock.
 *
 * Description:
 *	This function snaps the value of the CTV and returns without the
 *	exclusive lock held.
 *
 * Parameters:
 *	ctv_t *ctv	Area to receive the CTV.
 *
 * Returns:
 *	None.
 */
void
clms_snap_unlocked_ctv(
	ctv_t *ctv)
{
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	ctv->ups = clms.clms_ctv.ups;
	ctv->downs = clms.clms_ctv.downs;
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
}

/*
 * clms_unlock_ctv()
 *	Release the CTV exclusive lock.
 *
 * Description:
 *	Does that.
 *
 * Parameters:
 *	None.
 */
void
clms_unlock_ctv(void)
{
	UNLOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock);
}

#ifdef SSI_XXX
/* Unfortunately, this function needs the exclusive lock because the
 * clms code itself needs the shared.  The original intention was for
 * a trylock which would only fail if clms was doing a transition.  Because
 * we need the exclusive lock here, you might fail just because another
 * client is doing the same thing.
 *
 * Nobody currently uses this function.
 */
/*
 * clms_compare_trylock_ctv()
 *	Try the exclusive lock and compare the CTV with the passed vector.
 *
 * Description:
 *	This function trys to acquires the CTV exclusive lock, if it succeeds
 *	it then compares the passed
 *	vector to the current value of the CTV, and returns the result of the
 *	comparison (equal/not equal) with the exclusive lock held.
 *
 * Parameters:
 *	int flag		Flag indicating which component is to be
 *				compared.
 *					CLMS_CMP_CTV_ANY	any
 *					CLMS_CMP_CTV_UPS	ups
 *					CLMS_CMP_CTV_DOWNS	downs
 *	ctv_t ctv	The CTV to be compared.
 *
 * Returns:
 *	0	The values were not equal.
 *	1	The values were equal.
 *	The RW exclusive lock is held.
 */
int
clms_compare_trylock_ctv(
	int flag,
	ctv_t ctv)
{
	int ret = 0;

	/* Try for the exclusive lock. */
	if (TRY_LOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock)) {
		ret = clms_compare_unlocked_ctv(flag, ctv);
		if (!ret)
			UNLOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock);
	}
	return(ret);
}
#endif

/*
 * clms_compare_locked_ctv()
 *	Get the exclusive lock and compare the CTV with the passed vector.
 *
 * Description:
 *	This function acquires the CTV exclusive lock, compares the passed
 *	vector to the current value of the CTV, and returns the result of the
 *	comparison (equal/not equal) with the exclusive lock held.
 *
 * Parameters:
 *	int flag		Flag indicating which component is to be
 *				compared.
 *					CLMS_CMP_CTV_ANY	any
 *					CLMS_CMP_CTV_UPS	ups
 *					CLMS_CMP_CTV_DOWNS	downs
 *	ctv_t ctv	The CTV to be compared.
 *
 * Returns:
 *	0	The values were not equal.
 *	1	The values were equal.
 *	The RW exclusive lock is held.
 */
int
clms_compare_locked_ctv(
	int flag,
	ctv_t ctv)
{
	int ret;

	/* Get the exclusive lock. */
	LOCK_EXCL_RW_LOCK(&clms.clms_ctv_rw_lock);
	ret = clms_compare_unlocked_ctv(flag, ctv);
	return(ret);
}

/*
 * clms_compare_unlocked_ctv()
 *	Compare the CTV with the passed vector.
 *
 * Description:
 *	This function compares the passed vector to the current value of
 *	the CTV and returns the result of the comparison (equal/not equal)
 *	without the exclusive lock held.
 *
 * Parameters:
 *	int flag		Flag indicating which component is to be
 *				compared.
 *					CLMS_CMP_CTV_ANY	any
 *					CLMS_CMP_CTV_UPS	ups
 *					CLMS_CMP_CTV_DOWNS	downs
 *	ctv_t ctv	The CTV to be compared.
 *
 * Returns:
 *	0	The values were not equal.
 *	1	The values were equal.
 */
int
clms_compare_unlocked_ctv(
	int flag,
	ctv_t ctv)
{
	int ret;

	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	switch(flag) {
		case CLMS_CMP_CTV_ANY:
			if (ctv.ups == clms.clms_ctv.ups ||
			    ctv.downs == clms.clms_ctv.downs)
				ret = 1;
			else
				ret = 0;
			break;
		case CLMS_CMP_CTV_UPS:
			if (ctv.ups == clms.clms_ctv.ups)
				ret = 1;
			else
				ret = 0;
			break;
		case CLMS_CMP_CTV_DOWNS:
			if (ctv.downs == clms.clms_ctv.downs)
				ret = 1;
			else
				ret = 0;
			break;
		default:
			panic("clms_compare_unlocked_ctv:  Bad flag (%d)!", flag);
			break;
	}
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	return(ret);
}
