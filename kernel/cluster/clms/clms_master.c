/*
 * 	CLMS master support code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains code that runs on the NSC CLMS master node.
 *
 * The CLMS Master accepts connections from other nodes and
 * gives them cluster information.
 */

#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/timer.h>
#ifdef  CONFIG_VPROC
#include <linux/nd.h>
#endif
#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>
#include <cluster/clms.h>
#include <cluster/clms/clms_private.h>
#include <cluster/nodelist.h>
#include <cluster/async.h>
#include <cluster/icsgen.h>
#include <cluster/node_monitor.h>
#include <cluster/procfs.h>
#ifdef CONFIG_CFS
#include <cluster/ssi/cfs/cfs_mount.h>
#endif

#include <cluster/gen/ics_clms_macros_gen.h>
#include <cluster/gen/ics_clms_protos_gen.h>


static void clms_master_nodeup_daemon(void *);
static void clms_master_node_join_daemon(void *);
static void clms_master_nodedown_daemon(void *);
static void clms_master_nodedown_thread(void *);
static void clms_master_handle_resource_timeout(unsigned long);

/*
 * This global is to contain the difference between our lbolt and the lbolt
 * of the first booted root node.  This is used to maintain consistency of
 * the times() system call across failovers, and is assigned to the
 * p_ticks_delta field of user processes spawned by the kernel, which includes
 * init, among other things.
 *
 * This doesn't really belong in CLMS, but it's by far the most convenient
 * place to maintain and communicate it, so here it is.
 */
clock_t clms_ticks_delta = 0;

static void clms_master_node_join(clusternode_t);

void
clms_initialize_node_info(clusternode_t node)
{

	clms.clms_node_info[node].online_cpus =  num_online_cpus();
	clms.clms_node_info[node].total_cpus = num_possible_cpus();
	clms.clms_node_info[node].cpu_power = loops_per_jiffy;
	clms.clms_node_info[node].total_memory = num_physpages;
}

/*
 * clms_master_init()
 *	Initialize the CLMS coordinator.
 *
 * Description:
 *	The clms coordinator is prepared to handle cluster requests.
 *	The daemons for handling nodeup and nodedown events are spawned.
 *	The relevant locks and conditions are initialized.
 *
 * Calling/Exit State:
 *	This initialization routine is called from clms_init().
 *
 * Parameters:
 *	reinit_flag	If set, this is a CLMS secondary becoming the master.
 */
void
clms_master_init(
	int	reinit_flag)
{
	int error;

	/*
	 * Startup daemon for handling "Node Up Events".
	 */
	error = spawn_daemon_proc("cm nodeup dmn",
			  	  clms_master_nodeup_daemon,
			  	  NULL);
	if (error != 0) {
		panic("clms_master_init: spawn_daemon_proc: error %d", error);
	}

	/*
	 * Startup daemon for handling "Node Join Requests".
	 */
	error = spawn_daemon_proc("cm node join",
			  	  clms_master_node_join_daemon,
			  	  NULL);
	if (error != 0) {
		panic("clms_master_init: spawn_daemon_proc: error %d", error);
	}

	/*
	 * Startup daemon for handling "Node Down Events".
	 */
	error = spawn_daemon_proc("cm nodedwn dmn",
			  	  clms_master_nodedown_daemon,
			  	  (void *)NULL);
	if (error != 0) {
		panic("clms_master_init: spawn_daemon_proc: error %d\n", error);
	}

	if (reinit_flag == 0) {
		/*
		 * Set up any key services we may provide.
		 */
		clms_master_setup_key_services();
	}

	/* Initialize Node Monitoring on Master. */
	nm_init();

	/*
	 * set the num of cpus, total memory and cpu speed in clms_node_info
	 * structure for this node
	 */
	clms_initialize_node_info(this_node);

	return;
}

/*
 * clms_master_waitfor_resource_info()
 *   Wait until information about all the key services are available.
 *
 * Description:
 *	Waits until information about clms key services (like root
 *	file system) are available. Note that it does not wait for the
 *	key service to become available for use.
 *
 * Parameters:
 *	None.
 */
int
clms_master_waitfor_resource_info(void)
{
	struct timer_list clms_master_handle_resource_timer;

	if (clms_service_wait_timeout > 0) {
		init_timer(&clms_master_handle_resource_timer);
		clms_master_handle_resource_timer.function =
			clms_master_handle_resource_timeout;
		clms_master_handle_resource_timer.expires =
			jiffies + clms_service_wait_timeout * HZ;
		add_timer(&clms_master_handle_resource_timer);
	}

	clms_waitfor_key_service_info();
	del_timer_sync(&clms_master_handle_resource_timer);

	/*
	 * Got all key services, assign the services to the key nodes that
	 * offered them.
	 */
	clms_assign_key_services();
	return 0;
}

/*
 * clms_master_handle_resource_timeout()
 *	Timeout routine for key service wait.
 *
 * Description:
 *	This routine gets called if the timeout goes off while the master
 *	is waiting for key services to announce themselves.  It uses the
 *	async daemon to actually process the timeout.
 */
static void
clms_master_handle_resource_timeout(unsigned long dummy)
{
#ifndef NSC_ASYNC_ARGS_ZERO_COPY
	int arg = 0;
#endif
	int error;

#ifdef NSC_ASYNC_ARGS_ZERO_COPY
	error = nsc_async_queue(nsc_generic_async_queue,
				clms_handle_cluster_svr_timeout,
				NULL, 0, NSC_ASYNC_DUPCHECK);
#else
	error = nsc_async_queue(nsc_generic_async_queue,
				clms_handle_cluster_svr_timeout,
				&arg,
				sizeof(int),
				NSC_ASYNC_DUPCHECK);
#endif
	if (error)
		printk(KERN_WARNING "clms_master_handle_resource_timeout: "
		       "unable to allocate async event; error=%d", error);
}

/*
 * clms_master_join_cluster()
 *	Contact the clms master to join the cluster.
 *
 * Description:
 *	As this is the clms master node, this is a no-op function.
 *
 * Parameters:
 *	None.
 *
 * Returns
 *	0.
 */
int
clms_master_join_cluster(void)
{
	return 0;
}

/*
 * clms_master_startup_node()
 *	Do the necessary things to become fully up.
 *
 * Description:
 *	This function is called from the end of (unix) main. For clms
 *	client nodes the equivalent function declares the node as fully up.
 *	In the clms master, however, this is no op.
 *
 * Parameters:
 *	None.
 */

int
clms_master_startup_node(void)
{
	return 0;
}

/*
 * CLMS Master "Node Up" Processing routines.
 */
/*
 * rclms_cluster_svrinfo_request()
 *      A request from a clms client node for key services information.
 *
 * Description:
 *	A clms client requests for information about clms key services.
 *	This function is the server routine which processes the message.
 *
 * Parameters:
 *	clusternode_t node		- this node
 *	clusternode_t from_node		- the node number of the clms client
 *
 * Returns
 *	0.
 */
/*
 * The list of key service/server pairs in the cluster and their type and
 * statuses. .
 */
struct cluster_serviceinfo_req {
	int		*svclist;
	clusternode_t	*svrlist;
	int		*svr_typelist;
	int		*svr_statuslist;
	int		num_entries;
};

/*
 * Information about clms key services and the nodes providing the
 * key services.  This is sent to every node that is joining.
 */
static struct cluster_serviceinfo_req cluster_svcinfo_req
						= {NULL,NULL,NULL,NULL,0};

int
rclms_cluster_svrinfo_request(clusternode_t node, clusternode_t from_node)
{
	return(clms_queue_cluster_svrinfo_request(from_node));
}

int
clms_answer_cluster_svrinfo_request(
	clusternode_t	from_node)
{
	int	rval;
	int	error;
	int	svc_count;

	/*
	 * Send the key services information to the node.
	 */
	clms_set_op_inprogress(CLMS_NTRANS_STATE_CHANGE);

	/*
	 * Gather the key servers in the cluster currently.
	 */
	clms_alloc_cluster_svrinfo(NULL,
				   &cluster_svcinfo_req.svclist,
				   &cluster_svcinfo_req.svrlist,
				   &cluster_svcinfo_req.svr_typelist,
				   &cluster_svcinfo_req.svr_statuslist,
				   &cluster_svcinfo_req.num_entries);

	svc_count = clms_get_cluster_svrinfo(NULL,
					     cluster_svcinfo_req.svclist,
					     cluster_svcinfo_req.svrlist,
					     cluster_svcinfo_req.svr_typelist,
					     cluster_svcinfo_req.svr_statuslist,
					     cluster_svcinfo_req.num_entries);

	error = RCLMS_CLUSTER_SVRINFO(from_node,
				      &rval,
				      cluster_svcinfo_req.svclist,
				      svc_count,
				      cluster_svcinfo_req.svrlist,
				      svc_count,
				      cluster_svcinfo_req.svr_typelist,
				      svc_count,
				      cluster_svcinfo_req.svr_statuslist,
				      svc_count);

	if ((rval != 0) || (error != 0)) {
		printk(KERN_WARNING "rclms_cluster_svrinfo: "
		       "RCLMS_CLUSTER_SVRINFO: "
		       "error %d sending info to node %u",
				  error, from_node);
	}

	clms_clear_op_inprogress(CLMS_NTRANS_STATE_CHANGE);

	return 0;
}

/*
 * rclms_want_to_join()
 *	A clms client node wants to join the cluster.
 *
 * Description:
 *	The clms client node issues an rpc to join the cluster. This is
 *	the server procedure on the clms master node that handles the
 *	request.
 *
 *	The clms_nodeup() routine is called to start the "Node Up" processing
 *	of this client node.
 *
 * Calling/Exit State:
 *	This is the server procedure for an rpc and is called from a
 *	server proc.
 *
 * Parameters:
 *	clusternode_t	node	- this node
 *	int	*rval		- pointer to return value
 *	clusternode_t	**secondaries
 *				- pointer to an array to receive the list
 *				  of CLMS secondary nodes.
 *	int	*num_secondaries - pointer to a field to receive the length
 *				   of that array.
 *	icsinfo_t *secondary_icsinfo - pointer to an array to receive the
 *				       icsinfo for each node in the list of
 *				       CLMS secondaries.
 *	int	*num_secondary_ics - pointer to a field to receive the length
 *				     of that array.
 *	clusternode_t	**key_servers
 *				- pointer to an array to receive the list of
 *				  key server nodes.
 *	int	*num_key_servers - pointer to a field to receive the length
 *				   of that array.
 *	icsinfo_t **key_icsinfo	- pointer to an array to receive the icsinfo
 *				  each node in the list of key servers.
 *	int	*num_key_ics	- pointer to a field to receive the length
 *				  of that array.
 *	clusternode_t	client_node
 *				- the node that wants to join the cluster
 *
 * Returns:
 *	0.
 */

int
rclms_want_to_join(
	clusternode_t	node,
	int		*rval,
	clock_t 	*ticks,
	clusternode_t	**key_servers,
	int		*num_key_servers,
	icsinfo_t	**key_icsinfo,
	int		*num_key_ics,
	clusternode_t	client_node,
	int		cpus,
	int		on_cpus,
	unsigned long	cpupwr,
	unsigned long	total_mem)
{
	int i, node_count;
	clusternode_t *servers;
	icsinfo_t *icsinfo;

	*rval = 0;

	i = clms_iskeynode(client_node);
	CLMSLOG(CLMSLOG_WANTOJOIN, client_node,
		i ? ' ' : 'n',
		i ? ' ' : '\'',
		i ? ' ' : 't',
		i ? ' ' : ' ',
		0, 0);
	ics_nodeready(client_node);
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_node_info[client_node].flags |= CLMS_NODE_ICS_READY_DONE;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	clms_ready_key_nodes(client_node);
	if (this_node != clms_master_node)
		*rval = -EINVAL;
	else
		clms_master_node_join(client_node);
	/*
	 * Gather the list of nodes that are offering or secondarying for
	 * key services.
	 */
	servers = NULL;
	icsinfo = NULL;
	i = 0;
	do {
		clms_alloc_key_nodeinfo(&servers,
				        &icsinfo,
				        &i);
		node_count = clms_get_key_nodeinfo(servers,
						   icsinfo,
						   i);
	} while (node_count == -1);
	SSI_ASSERT(*key_servers == NULL);
	SSI_ASSERT(*num_key_servers == NSC_MAX_NODE_VALUE + 1);
	*key_servers = kmalloc_nofail(sizeof(clusternode_t)
				      * (NSC_MAX_NODE_VALUE + 1));
	SSI_ASSERT(*key_icsinfo == NULL);
	SSI_ASSERT(*num_key_ics == NSC_MAX_NODE_VALUE + 1);
	*key_icsinfo = kmalloc_nofail(sizeof(icsinfo_t)
				      * (NSC_MAX_NODE_VALUE + 1));
	memcpy(*key_servers, servers, node_count * sizeof(clusternode_t));
	memcpy(*key_icsinfo, icsinfo, node_count * sizeof(icsinfo_t));
	kfree(servers);
	kfree(icsinfo);
	*num_key_ics = *num_key_servers = node_count;

	clms_freeze_nodeup();
	/*
	 * If this is a key server node, he has missed the opportunity to get
	 * RCLMS_INFORM_KEY_NODES messages for any nodes that are already
	 * extant; send those now.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_node_info[client_node].flags &= ~CLMS_NODE_INFORM_NOT_READY;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	if (clms_iskeynode(client_node)) {
		int state, flags;
		icsinfo_t icsinfo, cicsinfo;

		for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
			if (i == client_node || i == this_node ||
			    clms_isnode_inlist(*key_servers, node_count, i))
				continue;
			LOCK_COND_LOCK(&clms.clms_node_lock);
			state = clms_get_node_status(i);
			flags = clms.clms_node_info[i].flags;
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			if ((state & (CLMS_NODE_UP |
				      CLMS_NODE_HALF_UP |
				      CLMS_NODE_COMING_UP)) &&
			    ((flags & CLMS_NODE_INFORM_NOT_DONE) == 0)) {
				ics_geticsinfo(i, &icsinfo);
				ics_geticsinfo((int)client_node, &cicsinfo);
				CLMSLOG(CLMSLOG_WTJINFORM,
					client_node,
					i,
					state,
					flags,
					0, 0, 0);
				(void)RCLMS_INFORM_KEY_NODES_MSG(client_node,
								 i,
								 &icsinfo);
				(void)RCLMS_READY_KEY_NODES_MSG(client_node,
								i);
			}
		}
	}
	clms_unfreeze_nodeup();
	clms.clms_node_info[client_node].total_cpus = cpus;
	clms.clms_node_info[client_node].online_cpus = on_cpus;
	clms.clms_node_info[client_node].cpu_power = cpupwr;
	clms.clms_node_info[client_node].total_memory = total_mem;
#ifdef CONFIG_LDLVL
	initialize_loadinfo(client_node, cpus, cpupwr);
#endif
	/*
	 * Give the joining node the _real_ lbolt, as known by the (possibly
	 * long-defunct) first root node.  Our lbolt will lag behind his,
	 * simply because we started later, so we apply the adjustment we
	 * computed when _we_ originally joined the cluster.  If we happen
	 * to be that first root node, of course, the delta will be zero.
	 */
	*ticks = TICKS() + clms_ticks_delta;
	return 0;
}


/*
 * clms_master_nodeup()
 *	Handle nodeup notifications on CLMS MASTER NODE (e.g. from ICS).
 *
 * Description:
 *	This routine is called on the clms master node when a node
 *	comes up.
 *	It is checked to see if the state of the node is correct.
 *	A clms transition data structure is allocated. Depending on the
 *	current state of the node, the information (pointer to the clms
 *	transition data structure) is queued on the appropriate queue and
 *	the appropriate daemon is waken up to do the rest of the processing.
 *
 * Calling/Exit State:
 *	This routine is called with the "CLMS NODEUP IN PROGRESS" flag set.
 *	Hence, at any given time, only one instance of this routine will
 *	be running.
 *
 * Parameters:
 *	node	node that wants to join the cluster.
 *
 * See Also
 *	clms_nodeup().
 */
void
clms_master_nodeup(
	clusternode_t 	node,
	icsinfo_t	*icsinfo)
{
	int				state, prev_state;
	int				error;
	struct clms_transition_data	*cdp;

	CLMSLOG(CLMSLOG_MASNUIND, node, 0, 0, 0, 0, 0, 0);
	/*
	 * If the cluster is being shut down, don't honor the nodeup.
	 * Toss the transition data, do an ics_nodedown() so ICS has
	 * consistent state, and ignore the nodeup.
	 */
	LOCK_COND_LOCK(&clms.clms_transition_lock);
	if (clms.clms_transition_flag & CLMS_TRANS_SHUTDOWN) {
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);
		CLMSLOG(CLMSLOG_SDTOSS, node, 0, 0, 0, 0, 0, 0);
		return;
	}
	UNLOCK_COND_LOCK(&clms.clms_transition_lock);
	/*
	 * Allocate and fill in a node transition
	 * structure with information  about the
	 * node that is coming up.
	 */
	cdp = clms_alloc_trans_data();
	cdp->node = node;
	cdp->private_data = kmalloc_nofail(sizeof(icsinfo_t));

	/*
	 * Check the state of the node.  It should only be NEVER_UP,
	 * GOING_DOWN or DOWN.  ICS_UP is subsumed in COMING_UP, so we
	 * don't need to check that explicitly.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	prev_state = clms_get_node_prev_status(node);
	state = clms_get_node_status(node);
	if (state & (CLMS_NODE_PARTIAL_COMING_UP | CLMS_NODE_COMING_UP |
		     CLMS_NODE_UP | CLMS_NODE_HALF_UP |
		     CLMS_NODE_HALF_DOWN | CLMS_NODE_INFORM_COMING_UP)) {
		panic("clms_master_nodeup:"
		      " Invalid State %d for node %u \n",
		      state,
		      node);
		/*NOTREACHED*/
	}
	if (state & CLMS_NODE_GOING_DOWN) {
		/*
		 * This node went down. The processing of
		 * "Node Down" event has not yet been
		 * completed. Wait until then.
		 */
		if (state & CLMS_NODE_PARTIAL_COMING_UP) {
			panic("clms_master_nodeup:"
			      " Invalid State %d for node %u \n",
			      state,
			      node);
			/*NOTREACHED*/
		}
		memcpy(cdp->private_data, icsinfo, sizeof(icsinfo_t));
		clms.clms_node_info[node].current_state |=
						CLMS_NODE_PARTIAL_COMING_UP;
		LOCK_COND_LOCK(&clms.clms_partial_nodeup_queue.cq_lock);
		clms_putq_trans_l(&clms.clms_partial_nodeup_queue, cdp);
		UNLOCK_COND_LOCK(&clms.clms_partial_nodeup_queue.cq_lock);
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		return;
	}
	/*
	 * Now set the node state, so that if the node fails, the right thing
	 * will happen.  If the seticsinfo fails, we undo this carefully.  Note
	 * that we haven't done clms_inform_key_nodes() for this node, nor is
	 * it ready to receive such messages.
	 */
	clms_set_node_status_l(node, CLMS_NODE_COMING_UP);
	clms.clms_node_info[node].flags |= CLMS_NODE_INFORM_NOT_READY;
	clms.clms_node_info[node].flags |= CLMS_NODE_INFORM_NOT_DONE;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	/*
	 * Set the icsinfo for the node, so we can communicate with him
	 * later.  If there's an error, we must have lost him, just toss
	 * the transition data, do an ics_nodedown() to make sure ICS
	 * has consistent state and ignore the nodeup.
	 */
	if ((error = ics_seticsinfo(node, icsinfo)) != 0) {
		LOCK_COND_LOCK(&clms.clms_node_lock);
		clms_set_node_status_l(node, prev_state);
		clms_set_node_status_l(node, state);
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		/* kfree(cdp->private_data); */
		clms_free_trans_data(cdp);
		ics_nodedown(node);
		CLMSLOG(CLMSLOG_MASNUFAIL, node, 0, 0, 0, 0, 0, 0);
		return;
	}
	/*
	 * At this point, the node could only have been NEVER_UP or DOWN.
	 */
	SSI_ASSERT((state & CLMS_NODE_NEVER_UP) || (state & CLMS_NODE_DOWN));
	/*
	 * Queue the transition FIFO and wake up the master nodeup daemon.
	 */
	LOCK_COND_LOCK(&clms.clms_nodeup_queue.cq_lock);
	clms_putq_trans_l(&(clms.clms_nodeup_queue), cdp);
	UNLOCK_COND_LOCK(&clms.clms_nodeup_queue.cq_lock);
	SIGNAL_CONDITION(&clms.clms_nodeup_queue.cq_cond);
	return;
}


/*
 * clms_master_nodeup_daemon()
 *	Daemon at the CLMS Coordinator responsible for handling "Node Up"
 *	of client nodes.
 *
 * Description:
 *	It retrieves the information the queue. It then checks to
 *	see if the node is still alive. If so, it calls the ics_nodeup()
 *	routine with the node number as the parameter. Once the ics_nodeup()
 *	returns, it is assured that all connections have been established
 *	to the node in question.
 *
 *	The client node will request to join the cluster at a later time.
 *	The clms_master_node_join_daemon() handles the joining process.
 *
 * Calling/Exit State:
 *	Daemon Process.
 *
 * Parameters:
 *	None.
 *
 *
 */
static void
clms_master_nodeup_daemon(void	*argp)
{
	struct clms_transition_data	*cdp;
	clusternode_t			new_node;
	int				error;
	int				state;

	nsc_daemonize();

	/*
	 * Loop forever...
	 */
	for (;;) {
		allow_reschedule();

		LOCK_COND_LOCK(&(clms.clms_nodeup_queue.cq_lock));
		while((cdp = clms_getq_trans_l(&clms.clms_nodeup_queue))
								== NULL) {
			WAIT_CONDITION(&clms.clms_nodeup_queue.cq_cond,
					&clms.clms_nodeup_queue.cq_lock);

		}
		UNLOCK_COND_LOCK(&clms.clms_nodeup_queue.cq_lock);

		/*
		 * If failover has blocked us, sleep.
		 */
		LOCK_COND_LOCK(&clms.clms_transition_lock);
		while (clms.clms_transition_flag & CLMS_TRANS_FAILOVER) {
			WAIT_CONDITION(&clms.clms_transition_cond,
				       &clms.clms_transition_lock);
		}
		UNLOCK_COND_LOCK(&clms.clms_transition_lock);

		new_node = cdp->node;
		clms_set_nodeup_inprogress();

		/*
		 * verify that the new node is still alive.
		 */
		state = clms_get_node_status(new_node);
		if (!(state & CLMS_NODE_COMING_UP)) {
			printk(KERN_WARNING "clms_master_nodeup_daemon: "
			       "new node %u status is %d\n",
			       new_node, state); 
			clms_free_trans_data(cdp);
			clms_clear_nodeup_inprogress();
			continue;
		}

		CLMSLOG(CLMSLOG_MASNODEUP,
			new_node,
			state,
			clms.clms_node_info[new_node].flags,
			0, 0, 0, 0);

		/*
		 * Tell ICS that this node is coming up. This is so that
		 * Surrogate origin data can be pulled and given to the
	 	 * new node.
		 *
		 * ics_nodeup() can return an error.
		 * If so, stop node up processing if
		 */
		if ((error = ics_nodeup(new_node)) != 0)
		{
			printk(KERN_WARNING "clms_master_nodeup_daemon: "
			       "ics_nodeup() for new node %u failed with error %d\n",
			       new_node, error);
			clms_nodedown(new_node);
			clms_free_trans_data(cdp);
			clms_clear_nodeup_inprogress();
			continue;
		}

		LOCK_COND_LOCK(&clms.clms_node_lock);
		clms.clms_node_info[new_node].flags |=
						     CLMS_NODE_ICS_NODEUP_DONE;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);

		/* Start monitoring this node. */
		nm_add_node(new_node);

		/*
		 * Tell all key servers that a new node is coming up.
		 */
		clms_inform_key_nodes(new_node);

		/*
	 	 * free the clms transition data structure.
		 */
		clms_free_trans_data(cdp);

		/*
		 * This is for coordination between this daemon and
		 * clms_master_node_join_daemon.  The clms_master_node_join
		 * daemon will proceed only after ics_nodeup() has been done
		 * on the node that is coming up.  Also clear the
		 * CLMS_NODE_INFORM_NOT_DONE flag so that we know to send new
		 * key server nodes inform messages for this node.
		 */
		LOCK_COND_LOCK(&clms.clms_node_lock);
		clms.clms_node_info[new_node].current_state |= CLMS_NODE_ICS_UP;
		clms.clms_node_info[new_node].flags &=
						     ~CLMS_NODE_INFORM_NOT_DONE;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		BROADCAST_CONDITION(&clms.clms_node_cond);

		/*
		 * Ready to process the next node.
		 */
		clms_clear_nodeup_inprogress();

		CLMSLOG(CLMSLOG_MASNUFIN,
			new_node,
			clms.clms_node_info[new_node].current_state,
			clms.clms_node_info[new_node].flags,
			0, 0, 0, 0);
	}
	/* NOT REACHED */
}

/*
 * clms_master_node_join()
 *	Handle request for joining the cluster.
 *
 * Description:
 *	This routine is called on the clms master node when a node
 *	issues a request to join the cluster.
 *	It is checked to see if the state of the node is correct.
 *	A clms transition data structure is allocated. Depending on the
 *	current state of the node, the information (pointer to the clms
 *	transition data structure) is queued on the appropriate queue and
 *	the appropriate daemon is waken up to do the rest of the processing.
 *
 * Calling/Exit State:
 *	This routine is called with the "CLMS NODEUP IN PROGRESS" flag set.
 *	Hence, at any given time, only one instance of this routine will
 *	be running.
 *
 * Parameters:
 *	node	node that wants to join the cluster.
 *
 */
void
clms_master_node_join(
	clusternode_t 	node)
{
	int				 state;
	struct clms_transition_data	*cdp;


	/*
	 * Allocate and fill in a node transition
	 * structure with information  about the
	 * node that is coming up.
	 */
	cdp = clms_alloc_trans_data();
	cdp->node = node;

	state = clms_get_node_status(node);
	if ( !(state & CLMS_NODE_COMING_UP) ) {
			panic("clms_master_nodeup:"
			      " Invalid State %x for node %u\n",
			      state, node);
			return;
	}
	/*
	 * queue it FIFO and wakeup "CLMS Master Node Join Daemon".
	 */
	LOCK_COND_LOCK(&clms.clms_node_join_queue.cq_lock);
	clms_putq_trans_l(&(clms.clms_node_join_queue),cdp);
	UNLOCK_COND_LOCK(&clms.clms_node_join_queue.cq_lock);
	SIGNAL_CONDITION(&clms.clms_node_join_queue.cq_cond);

	return;
}


/*
 * The following 2 data structures are used only by the daemon at the clms
 * master that is handling "Node Up" events and only one node can come
 * up at a time. Other nodes have to wait until it is their turn.
 *
 * We have to pass in structures to rclms_join (instead of individual
 * elements). NSC_XXX.
 */

struct cluster_nodeinfo {
	clusternode_t	*nodelist;
	icsinfo_t	*icsinfolist;
	int		num_nodes;
};


/*
 * The list of key service/server pairs in the cluster and their type and
 * statuses. NSC_XXX.
 */
struct cluster_serviceinfo {
	int		*svclist;
	clusternode_t	*svrlist;
	int		*svr_typelist;
	int		*svr_statuslist;
	int		num_entries;
};

/*
 * Information about key services and the nodes providing the key services.
 * This is sent to all the nodes coming up as the result of rclms_join.
 */
static struct cluster_serviceinfo cluster_svc_info;

/*
 * clms_master_node_join_daemon()
 *	Daemon at the CLMS Coordinator responsible for handling join requests
 *	from nodes in the cluster.
 *
 * Description:
 *	This daemon process is responsible for initiating the cluster joining
 *	ceremony. It retrieves the information the queue. It then checks to
 *	see if the node is still alive.
 *
 *	The nodes providing key services like root fs, init, cms
 *	etc. are informed that this node is "coming up". This will make
 *	them accept connection requests from the new node.
 *
 *	The nodeup() routines of all the clms services are called.
 *
 *	If the node is still alive, the rpc RCLMS_JOIN is issued.
 *	This rpc gives information about key service nodes in the cluster
 *	and the key services provided by them.  It gets back the information
 *	about the key services provided by the new node (if any). The key
 *	service information is processed by the clms.
 *
 *	If we are successful so far, this routine returns to process the
 *	next node that has made a wise decision to join this cluster.
 *
 *	At any given time, only one node is processed by this function.
 *
 * See Also:
 *	Comments at the end of this routine.
 *
 * Calling/Exit State:
 *	Daemon Process.
 *
 * Parameters:
 *	None.
 *
 *
 */
static void
clms_master_node_join_daemon(void *argp)
{
	struct clms_transition_data	*cdp;
	clusternode_t			new_node;
	int				svc_count;
	int				error;
	int				rval;
	int				state;
	clusternode_t			*servers = NULL;
	int				num_servers;

	nsc_daemonize();

	/*
	 * Initialize data structures used to send data to the joining
	 * node. Note that only one node is allowed to join at a time.
	 */
	cluster_svc_info.svclist = NULL;
	cluster_svc_info.svrlist = NULL;
	cluster_svc_info.svr_typelist = NULL;
	cluster_svc_info.svr_statuslist = NULL;
	cluster_svc_info.num_entries = 0;


	/*
	 * Loop forever...
	 */
	for (;;) {
		allow_reschedule();

		LOCK_COND_LOCK(&(clms.clms_node_join_queue.cq_lock));
		/*
		 * While there's nothing on the list, or all global data
		 * has not yet arrived, or all key services have not yet
		 * arrived, sleep.
		 */
		while((cdp = clms_getq_trans_l(&clms.clms_node_join_queue))
								== NULL ||
		      clms_all_svcs_assigned == 0) {
			if (cdp != NULL)
				clms_putq_trans_l(&(clms.clms_node_join_queue),
						  cdp);
			WAIT_CONDITION(&clms.clms_node_join_queue.cq_cond,
					&clms.clms_node_join_queue.cq_lock);

		}
		UNLOCK_COND_LOCK(&clms.clms_node_join_queue.cq_lock);
		new_node = cdp->node;

		CLMSLOG(CLMSLOG_NODEJOIN, new_node,
			clms.clms_node_info[new_node].current_state,
			0, 0, 0, 0, 0);

#ifdef CONFIG_CFS
		/*
		 * Don't allow nodes to join until the root has been
		 * remounted.
		 */

		while (!cfs_root_is_ready())
			nidelay(HZ);
#endif 

		/*
		 * If there are key services with no server, and the new node
		 * is a potential secondary for those services, we need to
		 * reassign those key services to him.  Check whether this is
		 * the case, and do so if necessary.  Do this before nodeups
		 * are disabled, since we may block on the nodeup flag in
		 * clms_reassign_key_services().
		 */
		if (servers)
			kfree(servers);
		servers = kmalloc_nofail(sizeof(clusternode_t) * clms_num_key_svcs);
		num_servers = clms_num_key_svcs;
		clms_reassign_key_services(CLUSTERNODE_INVAL, new_node,
					   servers);

		/*
		 * We're about to get the shared locks; set a flag so that
		 * nodedown will notice this, if necessary.  Both of these
		 * locks are released either in rclms_send_nodeup_info()
		 * after the node has fully joined the cluster, or in
		 * clms_master_nodedown() if the node dies before having
		 * fully joined.
		 */
		LOCK_COND_LOCK(&clms.clms_node_lock);
		if (clms_get_node_status(new_node) & CLMS_NODE_COMING_UP) {
			clms.clms_node_info[new_node].flags |=
							CLMS_NODE_MASTER_RWLOCK;
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			/*
			 * We're about to bring the node up.  Block users of
			 * the node/cluster transition API from seeing the
			 * transition until all nodes have been informed.
			 */
			LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
			CLMSLOG(CLMSLOG_API_LOCK, 0,
				__FILE__, __LINE__, current->pid, 0, 0, 0);
			/*
			 * Get the Cluster Transition Vector lock to hold off
			 * clients of the CTV until nodeup is complete.
			 */
			LOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
			LOCK_COND_LOCK(&clms.clms_node_lock);
		}

		/*
		 * Wait until ics_nodeup() has been completed on this node.
		 */
		while (!(clms.clms_node_info[new_node].current_state &
						CLMS_NODE_ICS_UP) &&
			(clms.clms_node_info[new_node].current_state &
						CLMS_NODE_COMING_UP) ) {
			WAIT_CONDITION(&clms.clms_node_cond,
					&clms.clms_node_lock);
		}
	 	UNLOCK_COND_LOCK(&clms.clms_node_lock);

		clms_set_nodeup_inprogress();

		/*
		 * verify that the new node is still alive.
		 */
		state = clms_get_node_status(new_node);
		if (!(state & CLMS_NODE_COMING_UP)) {
			printk(KERN_WARNING "clms_master_nodeup_daemon: "
			       "new node %u status is %d\n",
			       new_node, state);
			clms_nodedown(new_node);
			clms_free_trans_data(cdp);
			clms_clear_nodeup_inprogress();
			continue;
		}

		/*
		 * Call the nodeup routine of the Clms Subsystems.
		 */
		error = clms_call_all_svcs(cdp,CLMS_CALL_NODEUP);
		if (error) {
			printk(KERN_WARNING "clms_master_nodeup_daemon: "
			       " nodeup call failed. Error is %d"
			       " node = %u \n", error, new_node);
			clms_nodedown(new_node);
			clms_free_trans_data(cdp);
			clms_clear_nodeup_inprogress();
			continue;
		}

		/*
	 	 * Check the new node is still alive.
		 */
		state = clms_get_node_status(new_node);
		if (!(state & CLMS_NODE_COMING_UP)) {
			printk(KERN_WARNING "clms_master_nodeup_daemon: "
			       "new node %u status is %d\n",
			       new_node, state); 
			clms_free_trans_data(cdp);
			clms_clear_nodeup_inprogress();
			continue;
		}

		/*
		 * Gather the key server nodes in the cluster currently.
		 */
		clms_alloc_cluster_svrinfo(NULL,
					  &cluster_svc_info.svclist,
					  &cluster_svc_info.svrlist,
					  &cluster_svc_info.svr_typelist,
					  &cluster_svc_info.svr_statuslist,
					  &cluster_svc_info.num_entries);

		svc_count = clms_get_cluster_svrinfo(NULL,
					  	cluster_svc_info.svclist,
					  	cluster_svc_info.svrlist,
					  	cluster_svc_info.svr_typelist,
					  	cluster_svc_info.svr_statuslist,
					  	cluster_svc_info.num_entries);


		/*
		 * As the CLMS and reply services have been established,
		 * we can do RPCs to the client node.
		 *
 		 * Send the RPC that tells the client to join the cluster.
		 * Fetch its ICS information which services it provides,
		 * and send it information about which nodes in the cluster
		 * are up, and the services provided by the cluster.
		 * NSC_XXX: Make the following function call more modular!!!
		 */
		error = RCLMS_JOIN(new_node,
			 	   &rval,
				   cluster_svc_info.svclist,
				   svc_count,
				   cluster_svc_info.svrlist,
				   svc_count,
				   cluster_svc_info.svr_typelist,
				   svc_count,
				   cluster_svc_info.svr_statuslist,
				   svc_count,
				   servers,
				   num_servers);

		if ((rval != 0) || (error != 0)) {
			printk(KERN_WARNING
			       "clms_master_nodeup_daemon: RCLMS_JOIN: "
			       "error %d joining node %u\n",
			       error,
			       new_node);
			clms_nodedown(new_node);
			clms_free_trans_data(cdp);
			clms_clear_nodeup_inprogress();
			continue;
		}

		clms_clear_nodeup_inprogress();

		/*
		 * The rest of the "Node Up" processing for this node will
		 * be completed when the new node informs the clms master
		 * that it is fully up. Go back to process another node.
		 */
		/*
		 * The concept of "Nodes Providing Key Services are treated
		 * Special" makes this necessary.
 		 *
		 * An ordinary node coming up:
		 * 	All key service nodes are informed of this new node.
		 *	The new node is informed of all the nodes and
		 *	  and services available in the cluster.
		 *	If any key service node comes up, this new node will be
		 *	  informed, even if it is not fully up.
		 *	When this node becomes fully up, it will be given
		 *	  another snap shot of the cluster.
		 *
		 * A key service node coming up:
		 *	All key service nodes will be informed.
		 *	The cluster state reported to this node will include
		 *	  all nodes that are coming up also.
		 *	All nodes in the cluster (fully up and half up)
		 *	  will be informed when this key service node becomes
		 *	  fully up.
		 *
		 * By establishing a hierarchy in Key Services, deadlocks
		 * can be avoided. This algorithm also assumes that clms
		 * master knows the key service nodes.  However, currently
		 * only 2 key service nodes exist in the cluster
		 * (viz. CLMS and ROOT(and all other) and hence CLMS need not
		 * know who the root is until the root node joins.
		 * If the clms master does not know beforehand who the key
		 * service nodes are and what services they are providing, one
		 * more rpc will be needed in the case of key service nodes.
		 */

		/*
	 	 * free the clms transition data structure.
		 */
		clms_free_trans_data(cdp);
	}
	/* NOT REACHED */
}

/*
 * clms_master_newnode_inform()
 * 	Inform all the nodes in the cluster about a new node.
 *
 * Description:
 *	This routine informs all the nodes in the cluster about a new node
 *	that just joined the cluster.
 *
 *	If the new node joining the cluster is not a key service node, we
 *	inform only those nodes that are half or fully up.  If the new node
 *	joining the cluster is a key server node, we inform coming up nodes
 *	as well, since they may be waiting for this key server node to come
 *	up.
 *
 *	Note that by the time this routine is called, the new node is
 *	half up.
 *
 * Calling/Exit State:
 *	None.
 *
 * Parameters:
 *	newnode		The node that just came up.
 *	why		Why the transition happened.
 *	ctv		The CTV at the time the node became "half up."
 */
void
clms_master_newnode_inform(
	clusternode_t newnode,
	int why,
	clms_ctv_t ctv)
{
	static int svcs[CLMS_MAX_KEY_SERVICES];
	static int svr_types[CLMS_MAX_KEY_SERVICES];
	static int svr_statuses[CLMS_MAX_KEY_SERVICES];
	static cli_handle_t **handle_list = NULL;
	static clusternode_t *nodelist = NULL;
	cli_handle_t **handle_list_ptr;
	icsinfo_t		icsinfo;
	int	i, nodes, rval;
	int	status;
	int	error;
	int	num_svcs;
	int	total_cpus;
	int	online_cpus;
	unsigned long cpupwr;
	unsigned long total_mem;

	SSI_ASSERT(clms_get_node_status(newnode) & (CLMS_NODE_UP |
						CLMS_NODE_HALF_UP));
	if (handle_list == NULL)
		handle_list = (cli_handle_t **)kmalloc_nofail(
			NSC_MAX_NODE_VALUE * sizeof(cli_handle_t *));
	if (nodelist == NULL)
		nodelist = (clusternode_t *)kmalloc_nofail(
			NSC_MAX_NODE_VALUE * sizeof(clusternode_t));
	ics_geticsinfo(newnode, &icsinfo);
	num_svcs = clms_get_cluster_svrinfo(&newnode,
					    svcs,
					    NULL,
					    svr_types,
				 	    svr_statuses,
					    clms_num_key_svcs);

	/*
 	 * Go through all the nodes that are up, excluding the client
 	 * node that just came up and the CLMS master node (i.e. this
 	 * node; the client node and the CLMS master node are already
 	 * fully briefed) and tell them about the new node.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	total_cpus = clms.clms_node_info[newnode].total_cpus;
	online_cpus = clms.clms_node_info[newnode].online_cpus;
	cpupwr = clms.clms_node_info[newnode].cpu_power;
	total_mem = clms.clms_node_info[newnode].total_memory;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	nodes = 0;
	handle_list_ptr = handle_list;
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		if (i == clms_master_node || i == newnode)
			continue;
		status = clms_get_node_status(i);
		if (!(status & (CLMS_NODE_UP |
				CLMS_NODE_HALF_UP |
				CLMS_NODE_HALF_DOWN)))
			continue;
		CLMSLOG(CLMSLOG_SENDNEWNODE, newnode, i, 0, 0, 0, 0, 0);
		/*
		 * Send node number and cluster information about the
		 * new client node to the other nodes.
	 	 */
		*handle_list_ptr = NULL;
		error = RCLMS_NEWNODE_INFO_SEND(i,
						handle_list_ptr,
						this_node,
						why,
						ctv,
						newnode,
						&icsinfo,
						svcs,
						num_svcs,
						svr_types,
						num_svcs,
						svr_statuses,
						num_svcs,
						total_cpus,
						online_cpus,
						cpupwr,
						total_mem);
		if (icscli_async_sent(handle_list_ptr)) {
			nodelist[nodes++] = i;
			handle_list_ptr++;
		} else if (error != -EREMOTE)
			printk(KERN_WARNING
			       "%s: RCLMS_NEWNODE_INFO_SEND error %d "
			       "sending to node %d", __FUNCTION__, error, i);
	}
	handle_list_ptr = handle_list;
	for (i = 0; i < nodes; i++) {
		error = RCLMS_NEWNODE_INFO_RECEIVE(*handle_list_ptr++,
						   &rval,
						   this_node,
						   why,
						   ctv,
						   newnode,
						   &icsinfo,
						   svcs,
						   num_svcs,
						   svr_types,
						   num_svcs,
						   svr_statuses,
						   num_svcs,
						   total_cpus,
						   online_cpus,
						   cpupwr,
						   total_mem);
		if (error == 0)
			error = rval;
		if (error == -EREMOTE)
			nodelist[i] = CLUSTERNODE_INVAL;
		if (error != -EREMOTE && error != 0)
			printk(KERN_WARNING
			       "clms_master_newnode_inform:  RCLMS_NEWNODE_INFO error %d sending to node %u",
			       error,
			       nodelist[i]);
	}
	for (i = 0; i < nodes; i++) {
		if (nodelist[i] == CLUSTERNODE_INVAL)
			continue;
		error = RCLMS_TRANSITION_UNBLOCK_MSG(nodelist[i],
						     this_node,
						     ctv);
		if (error != -EREMOTE && error != 0)
			panic("clms_master_newnode_inform:  Error %d from RCLMS_TRANSITION_UNBLOCK_MSG to node %u\n",
			      error,
			      nodelist[i]);
	}
}

/*
 * rclms_send_nodeup_info()
 *	Server routine for RCLMS_SEND_NODEUP_INFO RPC.
 *
 * Description:
 *	This is the server routine for the RCLMS_SEND_NODEUP_INFO RPC that
 *	is sent from CLMS client nodes to the CLMS master node.
 *
 *	The CLMS CLIENT sends this rpc to indicate that it is FULLY UP.
 *	It gets back a snap shot of the cluster. A process is spawned
 *	to inform all the other nodes in the cluster about the new node.
 *	If the new node is also a node providing a key service, all
 *	nodes in the "coming up" state are also informed of the new node.
 *	If the new node is not a key service node, only the nodes in the
 *	"fully up" state are informed about the new node. Note that the key
 *	service nodes are informed of all the nodes that are "coming up"
 *	much earlier by the clms_master_nodeup_daemon().
 *
 *	Only one node is processed by this routine at any given time.
 *	This routine cannot be executed in parallel.
 *
 * Calling/Exit State:
 *	RPC Server routine executed by the ICS Server Proc.
 *
 * Parameters:
 *	node		this node.
 *
 *	rval  		return value
 *
 *	client_node	the client node that sent the RPC.
 *	ctv		pointer to a field to receive the current CTV
 *	downlist	pointer to an array of nodes that are marked down.
 *	nodelist	pointer to an array of nodes that are marked as fully
 *			up (returned to the client).
 *	icsinfolist	pointer to an array of the ics infos of nodes
 *			that are in the cluster and up (returned to the
 *			client).  Not present if this is is LCU.
 *	node_count	pointer to the number of meaningful entries in the
 *			above two arrays.
 *	halflist	pointer to an array of nodes that are marked as half
 *			up (returned to the client).
 *	halficsinfolist	pointer to an array of the ics infos of nodes
 *			that are in the cluster and up (returned to the
 *			client).  Not present if this is is LCU.
 *	half_count	pointer to the number of meaningful entries in the
 *			above two arrays.
 *	apistate	pointer to an array to receive the API state for all
 *			nodes or potential nodes in the cluster.
 *	hist		pointer to an array to receive the contents of the
 *			in-memory API transition history.
 *	trans_ents	pointer to the number of meaningful entries in the
 *			above array.
 *	stats		pointer to a field to receive the current cluster
 *			statistics.
 *
 * Return value:
 *	Always 0.
 */
int
rclms_send_nodeup_info(
	clusternode_t	node,
	int		*rval,
	clusternode_t	client_node,
	clms_ctv_t	*ctv,
	clusternode_t	**downlist,
	int		*down_count,
	clusternode_t	**nodelist,
	int		*node_count,
	icsinfo_t	**icsinfolist,
	int		*ics_count,
	clusternode_t	**halflist,
	int		*half_count,
	icsinfo_t	**halficsinfolist,
	int		*half_ics_count,
	clms_api_state_t **apistate,
	int		*api_count,
	clms_transition_t **hist,
	int		*trans_ents,
	clms_cluster_stat_t *stats,
	int		**total_cpus,
	int		*tot_len,
	int		**online_cpus,
	int		*on_len,
	unsigned long	**cpupwr,
	int		*pwr_len,
	unsigned long	**total_mem,
	int		*mem_len)
{
	int i, ups, downs, halfs;
	clms_api_state_t *apibuf;

	SSI_ASSERT(this_node == clms_master_node);
	SSI_ASSERT_LOCKED_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	SSI_ASSERT_LOCKED_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
	CLMSLOG(CLMSLOG_SENDNODEUP, client_node, 0, 0, 0, 0, 0, 0);
	/*
	 * Allocate all the arrays to be passed back to the client.
	 */
	SSI_ASSERT(*downlist == NULL);
	SSI_ASSERT(*down_count == NSC_MAX_NODE_VALUE + 1);
	*downlist = kmalloc_nofail((NSC_MAX_NODE_VALUE + 1)
				   * sizeof(clusternode_t));
	SSI_ASSERT(*nodelist == NULL);
	SSI_ASSERT(*node_count == NSC_MAX_NODE_VALUE + 1);
	*nodelist = kmalloc_nofail((NSC_MAX_NODE_VALUE + 1)
				   * sizeof(clusternode_t));
	SSI_ASSERT(*icsinfolist == NULL);
	SSI_ASSERT(*ics_count == NSC_MAX_NODE_VALUE + 1);
	*icsinfolist = kmalloc_nofail((NSC_MAX_NODE_VALUE + 1)
				      * sizeof(icsinfo_t));
	SSI_ASSERT(*halflist == NULL);
	SSI_ASSERT(*half_count == NSC_MAX_NODE_VALUE + 1);
	*halflist = kmalloc_nofail((NSC_MAX_NODE_VALUE + 1)
				   * sizeof(clusternode_t));
	SSI_ASSERT(*halficsinfolist == NULL);
	SSI_ASSERT(*half_ics_count == NSC_MAX_NODE_VALUE + 1);
	*halficsinfolist = kmalloc_nofail((NSC_MAX_NODE_VALUE + 1)
					  * sizeof(icsinfo_t));
	SSI_ASSERT(*apistate == NULL);
	SSI_ASSERT(*api_count == NSC_MAX_NODE_VALUE);
	apibuf = (clms_api_state_t *)kmalloc_nofail(NSC_MAX_NODE_VALUE
						    * sizeof(clms_api_state_t));
	SSI_ASSERT(*hist == NULL);
	SSI_ASSERT(*trans_ents == CLMS_TRANS_HIST_SZ);
	*hist = (clms_transition_t *)kmalloc_nofail(
		CLMS_TRANS_HIST_SZ * sizeof(clms_transition_t));
	SSI_ASSERT(*total_cpus == NULL);
	SSI_ASSERT(*tot_len == NSC_MAX_NODE_VALUE);
	*total_cpus = kzmalloc_nofail(NSC_MAX_NODE_VALUE * sizeof(**total_cpus));
	SSI_ASSERT(*online_cpus == NULL);
	SSI_ASSERT(*on_len == NSC_MAX_NODE_VALUE);
	*online_cpus = kzmalloc_nofail(NSC_MAX_NODE_VALUE * sizeof(**online_cpus));
	SSI_ASSERT(*cpupwr == NULL);
	SSI_ASSERT(*pwr_len == NSC_MAX_NODE_VALUE);
	*cpupwr = kzmalloc_nofail(NSC_MAX_NODE_VALUE * sizeof(**cpupwr));
	SSI_ASSERT(*total_mem == NULL);
	SSI_ASSERT(*mem_len == NSC_MAX_NODE_VALUE);
	*total_mem = kzmalloc_nofail(NSC_MAX_NODE_VALUE * sizeof(**total_mem));
	*tot_len = *on_len = *pwr_len = *mem_len = NSC_MAX_NODE_VALUE;

	/*
	 * We can't proceed while nodedowns are outstanding, while
	 * other instances of rclms_send_nodeup_info() are running, or
	 * while API-driven transitions are running.
	 */
	CLMS_TRANSITION_BLOCK(B_TRUE,
			      CLMS_TRANS_BLOCK_TRANS |
						CLMS_TRANS_BLOCK_NODEUP_INFO);
	/*
	 * Only one node is processed at a time.
	 */
	clms_set_nodeup_inprogress();

	/*
	 * Keep the cluster state stable while we take a snapshot of it.
	 */
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	/*
	 * If we're doing failover, we have to wait for it to complete before
	 * we can continue.
	 */
	while (clms.clms_ctv_failover) {
		WAIT_CONDITION(&clms.clms_ctv_cond, &clms.clms_ctv_lock);
	}
	LOCK_COND_LOCK(&clms.clms_node_lock);
	/*
	 * Gather the list of nodes that have been in the cluster, but are
	 * now down (these must be propagated to new nodes so that the
	 * information won't be lost on CLMS failover), the list of nodes
	 * that are fully up, and the list of nodes that are half up.
	 */
	downs = ups = halfs = 0;
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		if (clms.clms_node_info[i].current_state == CLMS_NODE_DOWN)
			(*downlist)[downs++] = i;
		if (clms.clms_node_info[i].current_state & CLMS_NODE_UP) {
			(*nodelist)[ups] = i;
			(void)ics_geticsinfo(i, &(*icsinfolist)[ups]);
			ups++;
		}
		if (clms.clms_node_info[i].current_state & CLMS_NODE_HALF_UP) {
			(*halflist)[halfs] = i;
			(void)ics_geticsinfo(i, &(*halficsinfolist)[halfs]);
			halfs++;
		}
	}
	SSI_ASSERT(downs <= NSC_MAX_NODE_VALUE &&
	       ups <= NSC_MAX_NODE_VALUE &&
	       halfs <= NSC_MAX_NODE_VALUE);
	*down_count = downs;
	*node_count = ups;
	*ics_count = ups;
	*half_count = halfs;
	*half_ics_count = halfs;
	*stats = clms.clms_cluster_stat;
	/*
	 * Get the API states and pass them to the new node.
	 */
	*apistate = apibuf;
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++)
		clms_api_get_full_state_l(i, apibuf++);
	/*
	 * Pass clms_node_info cpus info, etc to the new node
	 */
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		(*total_cpus)[i-1] = clms.clms_node_info[i].online_cpus;
		(*online_cpus)[i-1] = clms.clms_node_info[i].online_cpus;
		(*cpupwr)[i-1] = clms.clms_node_info[i].cpu_power;
		(*total_mem)[i-1] = clms.clms_node_info[i].total_memory;

	}
	/*
	 * Get the transition history and pass it to the new node.
	 */
	clms_api_get_hist(*hist, trans_ents, 0);
	/*
	 * If the status for this node is CLMS_NODE_GOING_DOWN, we're
	 * racing with nodedown.  Don't set the node status or queue anything
	 * for noded, just fill the various buffers and return; don't release
	 * the shared CTV lock since nodedown will do that for us.  If the
	 * status of the node is already CLMS_NODE_HALF_UP, failover ran in
	 * the middle of his nodeup and it got rolled forward; we're done,
	 * we just clean up and return.
	 */
	if ((clms_get_node_status(client_node) &
			     (CLMS_NODE_HALF_UP | CLMS_NODE_GOING_DOWN)) == 0) {
		++clms.clms_ctv.ups;
		CLMS_NEW_TRANSID(&(clms.clms_ctv));
		*ctv = clms.clms_ctv;
		clms_set_node_status_l(client_node, CLMS_NODE_HALF_UP);
		SSI_ASSERT(clms.clms_node_info[client_node].flags &
						      CLMS_NODE_MASTER_RWLOCK);
		clms.clms_node_info[client_node].flags &= ~CLMS_NODE_MASTER_RWLOCK;
		clms_api_transition_l(client_node,
				      CLMS_STATE_COMINGUP,
				      CLMS_TRANS_WHY_SYSTEM,
				      *ctv);
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		BROADCAST_CONDITION(&clms.clms_node_cond);
		/*
		 * Inform all nodes in the cluster about this node.
		 */
		clms_master_newnode_inform(client_node,
					   CLMS_TRANS_WHY_SYSTEM,
					   *ctv);
		/*
		 * Now allow the users to see the transition and signal them
		 * that a transition happened.
		 */
		UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		clms_sigcluster_all();
	}
	else {
		*ctv = clms.clms_ctv;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		/*
		 * Allow users to continue; there was no transition.
		 */
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
	}
	/*
	 * Let other instances of this routine and API state transitions
	 * proceed.
	 */
	CLMS_TRANSITION_UNBLOCK();
	/*
	 * Let another node come up.
	 */
	clms_clear_nodeup_inprogress();
	*rval = 0;
	return(0);
}

/*
 * CLMS Master "Node Down" processing routines.
 */
/*
 * rclms_inform_nodedown()
 *	Server routine for RCLMS_INFORM_NODEDOWN RPC.
 *
 * Description:
 *	This is the server routine for the RCLMS_INFORM_NODEDOWN RPC that
 *	is sent from a CLMS client node to the CLMS master node. This
 *	is to inform the clms master (the client thinks) that a node
 *	has gone down.
 *
 * Parameters:
 *	node		this node
 *	rval_p		return value; this routine must set it to zero
 *	down_node	the node (the client thinks) that went down
 *
 * Return value:
 *	Must be zero.
 */
int
rclms_inform_nodedown(
	clusternode_t	node,
	int		*rval,
	clusternode_t	down_node)
{

	SSI_ASSERT(this_node == clms_master_node);

	clms_nodedown(down_node);

	*rval = 0;

	return 0;

}

/*
 * clms_master_nodedown()
 *	Process a "Node Down" event.
 *
 * Description:
 *	This routine is called on the clms master node whenever a node
 *	goes down.
 *	The "Node Down"  processing is always started from the clms master.
 *	The clms master informs all the other nodes in the cluster
 *	after doing some groundwork.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node	node that has gone down
 */
void
clms_master_nodedown(clusternode_t node)
{
	int				state;
	struct clms_transition_data	*cdp;
	int				prev_state;

	/*
	 * Block users from seeing the transition until we're finished.
	 * This is unlocked by the nodedown thread for this node after
	 * we've finished the nodedown on all nodes.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_LOCK, 0, __FILE__, __LINE__, current->pid,
		0, 0, 0);
	LOCK_COND_LOCK(&clms.clms_node_lock);
	state = clms_get_node_status(node);
	CLMSLOG(CLMSLOG_MASNDIND, node, state, 0, 0, 0, 0, 0);

	if ( (state & CLMS_NODE_NEVER_UP) || (state & CLMS_NODE_DOWN) ) {
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		printk(KERN_WARNING
		       "clms_master_nodedown: Node %u is already down.\n",
		       node);
		return;
	}
	/*
	 * While the "Node Down" processing for this node is being done,
	 * the node could have come up and gone down. The following code
 	 * handles this case. If a node comes up while the "Node Down"
	 * processing is going on, it will be queued to the
 	 * partial queue. If it goes down when it is in the partial queue,
	 * it must be removed from the partial queue and the ics_nodedown()
	 * has to be called.
	 */
	if (state & CLMS_NODE_GOING_DOWN) {
		if (state & CLMS_NODE_PARTIAL_COMING_UP) {
			clms_rmv_partial_nodeup(node);
			clms.clms_node_info[node].current_state &=
						~CLMS_NODE_PARTIAL_COMING_UP;
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			ics_nodedown(node);
		} else {
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			printk(KERN_WARNING "clms_master_nodedown: "
			       "Node %u is already down.\n", node);

		}
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		return;
	}
	SSI_ASSERT (state & (CLMS_NODE_COMING_UP | CLMS_NODE_UP |
			 CLMS_NODE_HALF_UP | CLMS_NODE_HALF_DOWN |
			 CLMS_NODE_REGROUP_KNOWN));

#ifdef DEBUG
	printk(KERN_NOTICE "!clms_master_nodedown (START): %ld\n", TICKS());
#endif

	/*
 	 * Allocate and fill in a node transition
	 * structure with information  about the
	 * node that has gone down.
 	  */
	/*
	 * Block rclms_send_nodeup_info(), rclms_api_newstate() and
	 * anyone else that can't run while nodedowns are outstanding.
	 */
	CLMS_NODEDOWN_LOCK();
	/*
	 * Set the node status.  If the node had been NEVER_UP, then
	 * we must have died during COMING_UP, and the API doesn't
	 * need to know this.  We need to track this later in
	 * nodedown, as well, so we manipulate the state to keep
	 * NEVER_UP as the previous state.
	 */
	prev_state = clms_get_node_prev_status(node);
	if (prev_state == CLMS_NODE_NEVER_UP)
		clms_set_node_status_l(node, CLMS_NODE_NEVER_UP);
	clms_set_node_status_l(node, CLMS_NODE_GOING_DOWN);
	/*
	 * If we were coming up, we may have had the shared API and
	 * CTV locks held in that context.  If so, release them.
	 */
	if (clms.clms_node_info[node].flags & CLMS_NODE_MASTER_RWLOCK) {
		clms.clms_node_info[node].flags &= ~CLMS_NODE_MASTER_RWLOCK;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
	}
	else
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
	if ((prev_state & (CLMS_NODE_GOING_DOWN | CLMS_NODE_DOWN)) == 0)
		printk(KERN_WARNING "Node %u has gone down!!!\n",node);
	cdp = clms_alloc_trans_data();
	cdp->node = node;
	/*
	 * Get a list of nodes in the cluster which are UP and
	 * store it in cdp. This list will be given to the
	 * surrogate nodes/functions. Some nodes may go down after
	 * we get the list, but that is life.
	 */
	cdp->cnlist = clms_get_up_nodelist();
	if (!cdp->cnlist)
		panic("clms_master_nodedown: No Memory\n");
	cdp->ready_cnlist = clms_alloc_nodelist(cdp->cnlist->num_nodes, CLMS_NODE_UP);
	cdp->called_cnlist = clms_alloc_nodelist(cdp->cnlist->num_nodes, CLMS_NODE_UP);
	/*
	 * Queue it FIFO and wakeup "CLMS Master Nodedown Daemon".
	 */
	LOCK_COND_LOCK(&clms.clms_nodedown_queue.cq_lock);
	clms_putq_trans_l(&clms.clms_nodedown_queue, cdp);
	SIGNAL_CONDITION(&clms.clms_nodedown_queue.cq_cond);
	UNLOCK_COND_LOCK(&clms.clms_nodedown_queue.cq_lock);

#ifdef DEBUG
	printk(KERN_NOTICE "!clms_master_nodedown (END): %ld\n", TICKS());
#endif
}


/*
 * clms_master_nodedown_daemon()
 *	Handle "Node Down" events at the clms master.
 *
 * Description:
 * 	This process is created at init time in the clms master.
 *	The "Node Down" events are queued for this daemon. It will
 *	process the events by spawning a thread for each event.
 *
 * Calling/Exit State:
 *	Daemon.
 *
 * Parameters:
 *	None.
 *
 */
static void
clms_master_nodedown_daemon(void *argp)
{
	struct clms_transition_data	*cdp;
	int				error;
	clusternode_t			*servers;

	nsc_daemonize();

	INCR_MEMPRIO();		/* avoid blocking for memory */
	for (;;) {
		allow_reschedule();

		LOCK_COND_LOCK(&clms.clms_nodedown_queue.cq_lock);
		while((cdp = clms_getq_trans_l(&clms.clms_nodedown_queue))
								== NULL) {
			WAIT_CONDITION(&clms.clms_nodedown_queue.cq_cond,
					&clms.clms_nodedown_queue.cq_lock);

		}
		UNLOCK_COND_LOCK(&clms.clms_nodedown_queue.cq_lock);
		SSI_ASSERT(cdp != NULL);

#ifdef DEBUG
		printk(KERN_NOTICE "!clms_master_nodedown_daemon (START): %ld\n", TICKS());
#endif

		CLMSLOG(CLMSLOG_MASNDSTART, cdp->node, 0, 0, 0, 0, 0, 0);


		servers = kmalloc_nofail(sizeof(clusternode_t)
					 * clms_num_key_svcs);
		/*
		 * If the node that failed has any key services, and there are
		 * secondaries available for those key services, contact each
		 * of those nodes to inform them that they are now the primary
		 * for the affected services, and get the new key server for
		 * each reassigned key service.  This must be done before
		 * calling ics_nodedown, so that clms_get_key_nodeinfo()
		 * either gets the key services _and_ the icsinfo, or neither.
		 */
		clms_reassign_key_services(cdp->node, CLUSTERNODE_INVAL,
					   servers);

		/*
		 * Clear any key service entries for the down node.
		 */
		clms_key_service_nodedown(cdp->node, servers, 0);

		/*
		 * Call ICS (transport) "Node Down Handler". The transport
		 * must complete "Node Down" processing before anything else
		 * can be done. RDEV (remote device) nodedown handling must
		 * be done early as well in case swap devices depend upon it.
		 */
		ics_nodedown(cdp->node);
		/* NSC_XXX: not yet implemented.
		 rdev_nodedown(cdp->node);
		 */

		cdp->private_data = servers;
		smp_mb(); /* barrier ensures args visible to handler */
		do {
			error = spawn_daemon_thread("cm nd thr",
					   	    clms_master_nodedown_thread,
					    	    (void *)cdp);
			if (error != 0)
				idelay(HZ/10);
		} while(error != 0);

#ifdef DEBUG
		printk(KERN_NOTICE "!clms_master_nodedown_daemon (END): %ld\n", TICKS());
#endif
	}

}

/*
 * clms_master_nodedown_thread()
 *	The thread which processes "Node Down" for a node.
 *
 * Description:
 *	The ics_nodedown() is called to do the transport "Node Down".
 *	Then, the surrogate origin Node Down routines of all the
 *	services registered are called. Then, the other nodes (clms clients)
 *	in the cluster (that are up) are informed of the "Node Down".
 *	Once the clients complete the processing of the "Node Down",
 *	the "Node Down" handlers of the services in this node are called.
 *	Once all the services have completed processing, the Node Is Marked
 *	fully down. In case the node has booted before the "Node Down"
 *	processing has been completed, it is removed from the partial
 *	queue and placed in the nodeup queue and the nodeup_daemon is
 *	signaled.
 *
 * Parameters:
 *	void	*argp	- the pointer to the clms transition data structure
 */
void
clms_master_nodedown_thread(
		void *argp)
{
	struct clms_transition_data	*cdp;
	struct clms_nodelist		*cnlist;
	clusternode_t			p_node;		/* partitioned node */
	clusternode_t			surrogate_node;
	clusternode_t 			ni;
	clusternode_t			*servers;
	int				down_trans;
	int 				error;
	int				state;
	int				wasntup;
	int				why;
	nsc_nlcookie_t			cookie;
	clms_ctv_t			ctv;
	nsc_nodelist_t			*tlist;


	nsc_daemonize();

#ifdef DEBUG
	printk(KERN_NOTICE "!clms_master_nodedown_thread (START): %ld\n", TICKS());
#endif

	INCR_MEMPRIO();		/* avoid blocking for memory */
	cdp = (struct clms_transition_data *)argp;
	p_node = cdp->node;
	cnlist = cdp->cnlist;
	servers = cdp->private_data;
	/*
	 * Get the nsc_nodelist to track nodes that have responded to the
	 * nodedown, so we can wait to send them the RCLMS_TRANSITION_UNBLOCK()
	 * message when everyone is ready for it.
	 */
	tlist = clms_get_nsc_nodelist_fc(cnlist);
	NSC_NODELIST_CLRALL(tlist);
	cdp->private_data = tlist;
	/*
	 * Tell the client whether the node died while it was coming up; if
	 * so, we don't tell the API about this.  We distinguish between
	 * whether it had been up before or if this was the first time.
	 */
	wasntup = 0;
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (clms_get_node_prev_status(p_node) == CLMS_NODE_NEVER_UP)
		wasntup = 1;
	else
		if (clms_get_node_prev_status(p_node) == CLMS_NODE_DOWN)
			wasntup = 2;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	/*
	 * Get the Cluster Transition Vector lock; if we're doing failover,
	 * sleep until we're finished, then bump the nodedown component.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	while (clms.clms_ctv_failover) {
		WAIT_CONDITION(&clms.clms_ctv_cond, &clms.clms_ctv_lock);
	}
	++clms.clms_ctv.downs;
	CLMS_NEW_TRANSID(&(clms.clms_ctv));
	ctv = clms.clms_ctv;
	/*
	 * The node might have been doing a clean shutdown; if so, it told
	 * us about it, or tried to.  Check the flag.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (clms.clms_node_info[p_node].flags & CLMS_NODE_GOING_AWAY)
		why = CLMS_TRANS_WHY_API;
	else
		why = CLMS_TRANS_WHY_UNEXPECTED;
	/*
	 * Hide the transition from the API unless the node had been at
	 * least half up.
	 */
	if (wasntup == 0)
		clms_api_transition_l(p_node,
				      CLMS_STATE_KCLEANUP,
				      why,
				      ctv);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
	BROADCAST_CONDITION(&clms.clms_node_cond);

#ifdef CONFIG_VPROC
	/*
	 * Wait for surrogate key service to finish failing over.
	 */
	clms_waitfor_key_service(surrogate_key_service);
	cdp->surrogate_node = surrogate_node = surrogate_origin_node;
	(void)vproc_origin_create(p_node, VPROC_NODEDOWN);
#else
	cdp->surrogate_node = surrogate_node = this_node;
#endif /* CONFIG_VPROC */

	/*
	 * Get the number of down transitions for the nodedown message.
	 */
	down_trans = clms_get_cluster_num_downtrans();
	/*
	 * Register the handle we'll use for callbacks.
	 */
	clms_register_handle((u_long)cdp);
	/*
 	 * Send a message to all other nodes in the cluster that a node has
 	 * gone down.
 	 */
	for (ni = 1; ni <= NSC_MAX_NODE_VALUE; ni++) {
		if (ni == this_node)
			continue;

		state = clms_get_node_status(ni);
		if (!(state & (CLMS_NODE_UP |
			       CLMS_NODE_HALF_UP |
			       CLMS_NODE_HALF_DOWN)))
			continue;

		if (clms_set_node(cdp->ready_cnlist, ni) ||
		    clms_set_node(cdp->called_cnlist, ni)) {
			panic("clms_master_nodedown_daemon: clms_set_node error\n");
		}

		CLMSLOG(CLMSLOG_NDSENT, ni, p_node, surrogate_node, down_trans,
			ctv.ups, ctv.downs, 0);
		ICS_SETPRIO_UP(ICS_MAX_PRIO)
		error = RCLMS_NODEDOWN_MSG(ni,
					   (u_long)cdp,
					   this_node,
					   p_node,
					   surrogate_node,
					   down_trans,
					   wasntup,
					   why,
					   ctv,
					   servers,
					   clms_num_key_svcs);
		ICS_RESTORE_PRIO()
		if (error != 0) {
			clms_clear_node(cdp->ready_cnlist, ni);
			clms_clear_node(cdp->called_cnlist, ni);
			if (error != -EREMOTE)
				printk(KERN_WARNING
				       "clms_master_nodedown_thread:  RCLMS_NODEDOWN: error %d sending info to node %u\n",
				       error,
				       ni);
		}
	}
	/*
	 * Wait for all the client nodes to tell us they've started
	 * nodedown and are ready to be unblocked, or to go down.
	 */
	clms_waitfor_client_callbacks(cdp, 1);
	/*
 	 * Tell all nodes that told us they were ready that they can unblock
 	 * the node/cluster status API calls.
 	 */
	cookie = CLUSTERNODE_INVAL;
	while ((ni = NSC_NODELIST_GET_NEXT(&cookie, tlist))
							 != CLUSTERNODE_INVAL) {
		if (!clms_isnodeavail(ni))
			continue;
		CLMSLOG(CLMSLOG_NDFINSENT, p_node, ni, 0, 0, 0, 0, 0);
		error = RCLMS_TRANSITION_UNBLOCK_MSG(ni, this_node, ctv);
		if (error != -EREMOTE && error != 0) {
			printk(KERN_WARNING
			       "clms_master_nodedown_daemon:  RCLMS_TRANSITION_UNBLOCK_MSG: error %d sending unblock to node %u\n",
			       error,
			       ni);
		}
	}
	/*
	 * Allow users to see the transition we just finished.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
		0, 0, 0);
	/*
	 * Signal processes that a transition happened, unless the node
	 * wasn't all the way up.
	 */

	if (wasntup == 0)
		clms_sigcluster_all();

	/*
	 * Wait for all the client nodes to tell us they've finished
	 * nodedown, or to go down.
	 */
	clms_waitfor_client_callbacks(cdp, 0);
#ifdef CONFIG_VPROC
	/* Tell vproc client nodedown is complete. */
	vproc_origin_inform_nodedown_done(cdp->node);
#endif
	/*
 	 * Call the Node Down Handlers for all CLMS subsystems,
 	 * This function returns only after all subsystems have
	 * completed cleaning up.
 	 */
	clms_call_all_svcs(cdp, CLMS_CALL_NODEDOWN);
	/*
	 * Unlock the shared Cluster Transition Vector lock to let critical
	 * services proceed, now that nodedown is complete on this node.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_ctv_rw_lock);
	/*
	 * Wait for all servers for all services to complete.  This should
	 * avoid the possibility of having outstanding messages when a new
	 * incarnation of the node appears.
	 */
	icssvr_nodedown_wait(p_node);
	/*
	 * Allow rclms_send_nodeup_info() to proceed.
	 */
	CLMS_NODEDOWN_UNLOCK();
	/*
	 * Deregister the handle we used for callbacks.
	 */
	clms_deregister_handle((u_long)cdp);
	/*
	 * free the clms transition data.
	 */
	cdp->private_data = NULL;
	kfree(servers);
	/* clms_free_nodelist(cnlist); */ /* cdp->cnlist */
	/* clms_free_nodelist(cdp->called_cnlist); */
	clms_free_trans_data(cdp);
	NSC_NODELIST_FREE(tlist);
	CLMSLOG(CLMSLOG_MASNDFIN, p_node, 0, 0, 0, 0, 0, 0);
	/*
	 * If the node died while it was coming up (it was still in
	 * CLMS_NODE_COMING_UP CLMS internal state) then we drop back to
	 * where we were (NEVER_UP or DOWN) without ever notifying the
	 * API.
	 */
	if (wasntup != 0) {
		clms_set_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
		/*
		 * If the node has already started coming up again, move
		 * it from the partial-nodeup queue to to the nodeup queue
		 * so that the new nodeup can be processed.
		 */
		LOCK_COND_LOCK(&clms.clms_node_lock);
		state = clms_get_node_status(p_node);
		if (state & CLMS_NODE_PARTIAL_COMING_UP) {
			clms.clms_node_info[p_node].current_state &=
						~CLMS_NODE_PARTIAL_COMING_UP;
		}
		clms.clms_node_info[p_node].flags = 0;
		if (wasntup == 1)
			clms_set_node_status_l(p_node, CLMS_NODE_NEVER_UP);
		else
			clms_set_node_status_l(p_node, CLMS_NODE_DOWN);
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		if (state & CLMS_NODE_PARTIAL_COMING_UP)
			clms_process_partial_nodeup(p_node);
		clms_clear_op_inprogress(CLMS_NTRANS_STATE_CHANGE);
	}
	else {
		int why;

		/*
		 * Use the API to get us to UCLEANUP state.  Internally
		 * we're still GOING_DOWN.  The API will get us to DOWN
		 * later.
		 */
		error = clms_api_validate_new_state(p_node,
						    CLMS_STATE_UCLEANUP,
						    &why);
		SSI_ASSERT(error == 0);
		clms_api_newstate(p_node, CLMS_STATE_UCLEANUP, why);
	}
#ifdef DEBUG
	printk(KERN_NOTICE "!clms_master_nodedown_thread (END): %ld\n", TICKS());
#endif
	exit_daemon_thread();
}

/*
 * rclms_client_nodedown_callback()
 *	Server routine for the ICS messages sent by the clms clients
 *	to indicate that a particular operation has been completed
 *	(call back).
 *
 * Description:
 *	This is the server function for the Message/RPC sent by the clms
 *	clients after they have completed an operation. Typically the
 *	CLMS Master sends a message (e.g. "Node Down") to the clients
 *	and waits for the clients to complete the work. The clients
 *	call the master back after they have completed the operation.
 *
 *	The nodes that hae been called are marked in a list. The nodes
 *	are deleted form the list as the callback arrives.
 *
 * Calling/exit State:
 *	Called by a server proc when the corresponding  RPC/Message arrives.
 *
 * Parameters:
 *	clusternode_t 	node		this node
 * 	long		clms_handle	handle passed on to the clients to be
 *					passed back
 *	clusternode_t	client_node	the node number of the calling node
 *
 * Returns
 *	0.
 */
int
rclms_client_nodedown_callback(
	clusternode_t 	node,
	u_long clms_handle,
	clusternode_t	client_node)
{
	struct clms_transition_data *cdp;
	clusternode_t		    *tmp;

	/*
	 * If the handle isn't in the registry, we didn't send the nodedown.
	 * Drop it.
	 */
	if (clms_find_handle(clms_handle) == 0)
		return(0);
	cdp = (struct clms_transition_data *)clms_handle;

	LOCK_COND_LOCK(&clms.clms_node_lock);
	for (tmp = cdp->called_cnlist->n_rptr;
				tmp != cdp->called_cnlist->n_wptr ; tmp++) {
		if (*tmp == client_node) {
			*tmp = CLUSTERNODE_INVAL;
			break;
		}
	}
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	BROADCAST_CONDITION(&clms.clms_node_cond);

	return 0;
}

/*
 * rclms_nodedown_started()
 *	Server routine for the ICS messages sent by the clms clients to
 *	indicate that a nodeup or nodedown has been started, and they are
 *	ready for the RCLMS_TRANSITION_UNBLOCK message.
 *
 * Description:
 *	After a CLMS client has started a nodedown, but before it has done
 *	any processing (having just locked the API and updated the CTV),
 *	it sends the master an RCLMS_NODEDOWN_STARTED() message, so that
 *	the master knows that it's ready for an RCLMS_TRANSITION_UNBLOCK()
 *	message.  When all nodes have sent these messages, the CLMS master
 *	sends the unblock message, unblocks the API, and waits for the
 *	nodedown to complete.
 *
 * Calling/exit State:
 *	Called by a server proc when the corresponding  RPC/Message arrives.
 *
 * Parameters:
 *	clusternode_t 	node		this node
 * 	long		clms_handle	handle passed on to the clients to be
 *					passed back
 *	clusternode_t	client_node	the node number of the calling node
 *
 * Returns
 *	0.
 */
int
rclms_nodedown_started(
	clusternode_t 	node,
	u_long clms_handle,
	clusternode_t	client_node)
{
	struct clms_transition_data *cdp;
	clusternode_t		    *tmp;
	nsc_nodelist_t              *nlist;

	/*
	 * If the handle isn't in the registry, we didn't send the nodedown.
	 * Drop it.
	 */
	if (clms_find_handle(clms_handle) == 0)
		return(0);
	cdp = (struct clms_transition_data *)clms_handle;

	LOCK_COND_LOCK(&clms.clms_node_lock);
	for (tmp = cdp->ready_cnlist->n_rptr;
				tmp != cdp->ready_cnlist->n_wptr ; tmp++) {
		if (*tmp == client_node) {
			*tmp = CLUSTERNODE_INVAL;
			break;
		}
	}
	nlist = cdp->private_data;
	if (nlist != NULL)
		NSC_NODELIST_SET1(nlist, client_node);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	BROADCAST_CONDITION(&clms.clms_node_cond);

	return 0;
}

/*
 * rclms_api_newstate()
 *	A CLMS client is informing the master that it's changing state.
 *
 * Description:
 *	A node is making an API-driven transition.  Some states are
 *	set via the API from a process, or are of interest only to the
 *	API.  This routine processes such transitions, and forwards them
 *	to all other nodes in the cluster.
 *
 *	When a node initializes, it starts in "half up" state, where
 *	all rexecs/rforks/migrates are failed back to the requester
 *	with -EAGAIN, unless the requester ignores "half up" state, as
 *	in the case of noded/init.  When the node goes from "half up"
 *	to fully up state (as a result of a user-process-initiated API
 *	transition performed when user-level initialization is complete),
 *	it sends an RCLMS_API_NEW_STATE message to the CLMS master.  In
 *	addition, certain other API states are initiated by user processes.
 *	This routine handles them all by first sending an "intent"
 *	async RPC to all nodes (including the initiating node) to inform
 *	them that the node in question is changing state.  It then sends
 *	a "commit" message to actually perform the transition.  If the
 *	CLMS master dies during this process and no nodes have received
 *	the "commit," all nodes will discard the recorded "intent" and
 *	the client will retry the operation.  If any node has received a
 *	"commit," the CLMS failover code will send commits to all other
 *	nodes; in this case, the client must not retry the operation,
 *	even if the RPC returns -EREMOTE.
 *
 *	Note that this routine does _not_ hold the CTV shared lock; if
 *	the kernel CTV interface is extended to support "other" transitions,
 *	this must change.
 *
 * Calling/Exit State:
 *      This is the server procedure for an rpc and is called from a
 *      server proc.
 *
 * Parameters:
 *	clusternode_t	to_node		This node.
 *	int		*rval		Return value of the RPC.
 *	clusternode_t	from_node	The node that generated the RPC.
 *	clusternode_t	trans_node	The node that is transitioning.
 *					The same as from_node unless new_state
 *					is DOWN.
 *	int		new_state	The state the node is going to.
 *	int		why		The reason for the state change.
 *
 * Returns:
 *	0.
 */
int
rclms_api_newstate(
	clusternode_t to_node,
	int *rval,
	clusternode_t from_node,
	clusternode_t trans_node,
	int new_state,
	int why)
{
	int i, nodes, status, error;
	clusternode_t *nodelist;
	cli_handle_t **handle_list, **handle_list_ptr;
	clms_ctv_t ctv;

	SSI_ASSERT(
	    (from_node == trans_node && new_state != CLMS_STATE_DOWN) ||
	    (from_node != trans_node && new_state == CLMS_STATE_UCLEANUP) ||
	    (from_node != trans_node && new_state == CLMS_STATE_DOWN));
	CLMSLOG(CLMSLOG_NEWSTATE, from_node, trans_node, new_state, why,
		0, 0, 0);
	/*
	 * Block users of the node/cluster transition API from seeing the
	 * transition until all nodes have been informed.
	 */
	LOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_LOCK, 0, __FILE__, __LINE__, current->pid, 0, 0, 0);
	/*
	 * We can't proceed while rclms_send_nodeup_info() is running
	 * or while other API-driven transitions are running.
	 */
	CLMS_TRANSITION_BLOCK(B_FALSE,
			      CLMS_TRANS_BLOCK_TRANS |
						CLMS_TRANS_BLOCK_NODEUP_INFO);
	LOCK_COND_LOCK(&clms.clms_ctv_lock);
	while (clms.clms_ctv_failover) {
		WAIT_CONDITION(&clms.clms_ctv_cond, &clms.clms_ctv_lock);
	}
	/*
	 * If the node went down before we committed to this transition,
	 * the transition has been invalidated; return -EINVAL.
	 */
	LOCK_COND_LOCK(&clms.clms_node_lock);
	if (new_state != CLMS_STATE_UCLEANUP &&
	    clms_api_get_state_l(trans_node) == CLMS_STATE_KCLEANUP) {
		*rval = -EINVAL;
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		UNLOCK_COND_LOCK(&clms.clms_ctv_lock);
		CLMS_TRANSITION_UNBLOCK();
		UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
		CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
			0, 0, 0);
		return(0);
	}
	++clms.clms_ctv.other;
	CLMS_NEW_TRANSID(&(clms.clms_ctv));
	ctv = clms.clms_ctv;
	clms_api_transition_l(trans_node, new_state, why, ctv);
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	UNLOCK_COND_LOCK(&clms.clms_ctv_lock);

	BROADCAST_CONDITION(&clms.clms_node_cond);

	nodelist = kmalloc_nofail(sizeof(clusternode_t) * NSC_MAX_NODE_VALUE);
	clms_set_nodeup_inprogress();	/* Block other calls and nodeups. */
	/*
	 * Send the intent to the requesting node first; if we die during
	 * this process, this lets the requesting node know whether to retry
	 * the state change.  If we _are_ the requesting node, there's
	 * nothing to do here, since if we die, we die.
	 */
	if (from_node != this_node) {
		error = RCLMS_NEW_STATE_INTENT(from_node,
					       rval,
					       this_node,
					       trans_node,
					       new_state,
					       why,
					       ctv);
		if (error != -EREMOTE && error != 0)
			panic("rclms_api_newstate:  Error %d from RCLMS_NEW_STATE_INTENT to node %u\n",
			       error,
			       from_node);
	}
	/*
	 * Tell all nodes that are up about the transition.  Don't worry
	 * about telling the CLMS master, that's us, or the node that initiated
	 * the transition, we just told him.
	 */
	nodes = 0;
	handle_list = (cli_handle_t **)kmalloc_nofail(NSC_MAX_NODE_VALUE
						      * sizeof(cli_handle_t *));
	handle_list_ptr = handle_list;
	for (i = 1; i <= NSC_MAX_NODE_VALUE; i++) {
		if (i == this_node || i == from_node)
			continue;
		if (!clms_isnodeavail(i))
			continue;
		*handle_list_ptr = NULL;
		error = RCLMS_NEW_STATE_INTENT_SEND(i,
						    handle_list_ptr,
						    this_node,
						    trans_node,
						    new_state,
						    why,
						    ctv);
		if (icscli_async_sent(handle_list_ptr)) {
			nodelist[nodes++] = i;
			handle_list_ptr++;
		} else if (error != -EREMOTE)
			panic("%s: Error %d from RCLMS_NEW_STATE_INTENT_SEND"
			      " to node %d\n",
			      __FUNCTION__, error, i);
	}
	handle_list_ptr = handle_list;
	for (i = 0; i < nodes; i++) {
		error = RCLMS_NEW_STATE_INTENT_RECEIVE(*handle_list_ptr++,
						       rval,
						       this_node,
						       trans_node,
						       new_state,
						       why,
						       ctv);
		if (error == -EREMOTE)
			nodelist[i] = CLUSTERNODE_INVAL;
		if (error == 0)
			error = *rval;
		if (error != -EREMOTE && error != 0)
			panic("rclms_api_newstate:  Error %d from RCLMS_NEW_STATE_INTENT_RECEIVE to node %u\n",
			      error,
			      nodelist[i]);
	}
	kfree(handle_list);
	/*
	 * Send the commit to the originating node, as well.
	 */
	if (from_node != this_node && clms_isnodeavail(from_node)) {
		error = RCLMS_NEW_STATE_COMMIT_MSG(from_node, this_node, ctv);
		if (error != -EREMOTE && error != 0)
			printk(KERN_WARNING
			       "rclms_api_newstate:  Error %d from RCLMS_NEW_STATE_COMMIT_MSG to node %u\n",
			       error,
			       from_node);
	}
	for (i = 0; i < nodes; i++) {
		if (nodelist[i] == CLUSTERNODE_INVAL ||
		    !clms_isnodeavail(nodelist[i]))
			continue;
		error = RCLMS_NEW_STATE_COMMIT_MSG(nodelist[i], this_node, ctv);
		if (error != -EREMOTE && error != 0)
			printk(KERN_WARNING
			       "rclms_api_newstate:  Error %d from RCLMS_NEW_STATE_COMMIT_MSG to node %u\n",
			       error,
			       nodelist[i]);
	}
	clms_clear_nodeup_inprogress();	/* Unblock other calls and nodeups. */
	kfree(nodelist);
	switch (new_state) {
		case CLMS_STATE_UP:
			/*
			 * Only set the node as up if it's now half up; it
			 * might have gone down or still be fully up
			 * internally.
			 */
			LOCK_COND_LOCK(&clms.clms_node_lock);
			status = clms_get_node_status(trans_node);
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			if (status == CLMS_NODE_HALF_UP ||
			    status == CLMS_NODE_HALF_DOWN)
				clms_nodeup(trans_node);
			break;
		case CLMS_STATE_SHUTDOWN:
		case CLMS_STATE_GOINGDOWN:
			clms_node_halfdown(trans_node);
#ifdef CONFIG_LDLVL
			if (new_state == CLMS_STATE_SHUTDOWN && 
							trans_node == this_node)
				move_eligible_processes_off();
#endif
			break;
		case CLMS_STATE_DOWN:
			/*
			 * In case the node has already started coming up,
			 * move it from the partial nodeup queue to the
			 * nodeup queue so that "Node Up" can be processed.
			 */
			LOCK_COND_LOCK(&clms.clms_node_lock);
			status = clms_get_node_status(trans_node);
			if (status & CLMS_NODE_PARTIAL_COMING_UP) {
				clms.clms_node_info[trans_node].current_state &=
						   ~CLMS_NODE_PARTIAL_COMING_UP;
			}
			clms.clms_node_info[trans_node].flags = 0;
			/*
			 * Mark the node DOWN internally.
			 */
			clms_set_node_status_l(trans_node, CLMS_NODE_DOWN);
			UNLOCK_COND_LOCK(&clms.clms_node_lock);
			if (status & CLMS_NODE_PARTIAL_COMING_UP)
				clms_process_partial_nodeup(trans_node);
			break;
		/*
		 * Note:  No default block, since it's unnecessary here.
		 */
	}
	/*
	 * Allow rclms_send_nodeup_info() and other instances of
	 * rclms_api_newstate() to proceed.
	 */
	CLMS_TRANSITION_UNBLOCK();
	/*
	 * Now allow users to see the transition.
	 */
	UNLOCK_SHR_RW_LOCK(&clms.clms_api_rw_lock);
	CLMSLOG(CLMSLOG_API_UNLOCK, 0, __FILE__, __LINE__, current->pid,
		0, 0, 0);
	/*
	 * Send a signal to all processes that need to find out
	 * about cluster topology changes.
	 */
	clms_sigcluster_all();
	*rval = 0;
	return(0);
}

/*
 * rclms_shutdown_freeze()
 *	We're being informed that the cluster is being shut down.
 *
 * Description:
 *	We're shutting down, so block all nodeups on the CLMS master.
 *	The cluster will process nodeups already in progress as well as
 *	nodedowns.  We have to communicate this to clients, so we remember
 *	it if we failover CLMS in the middle.  This routine is overloaded
 *	to handle this, as well.
 *
 * Calling/Exit State:
 *      This is the server procedure for an rpc and is called from a
 *      server proc.
 *
 * Parameters:
 *	clusternode_t	node		This node.
 *	int		*rval		Return value of the RPC.
 *	clusternode_t	from_node	The node that sent us the RPC.
 *
 * Returns:
 *	0.
 */
int
rclms_shutdown_freeze(
	clusternode_t node,
	int *rval,
	clusternode_t from_node)
{
	int i, nodes, status, error;
	clusternode_t nodelist[NSC_MAX_NODE_VALUE];
	clms_secondary_list_t *ent;
	cli_handle_t **handle_list, **handle_list_ptr;

	CLMSLOG(CLMSLOG_SHUTDOWN, from_node, 0, 0, 0, 0, 0, 0);

	/*
	 * We can't proceed while nodedowns are outstanding, while
	 * rclms_send_nodeup_info() is running, or while API-driven
	 * transitions are running. or if other instances of
	 * rclms_shutdown_freeze() are running.  This also blocks
	 * new nodeups.
	 */
	CLMS_TRANSITION_BLOCK2(B_TRUE,
			       CLMS_TRANS_BLOCK_TRANS |
						CLMS_TRANS_BLOCK_NODEUP_INFO,
			       CLMS_TRANS_SHUTDOWN);
	*rval = 0;
	/*
	 * If we're not the CLMS master, we're done.
	 */
	if (this_node != clms_master_node)
		return(0);
	/*
	 * Tell all CLMS secondaries about this; we need to do this so
	 * CLMS failover doesn't have to reconstruct it.  Don't worry
	 * about telling the node that told us or the CLMS master, they
	 * already know.
	 */
	nodes = 0;
	handle_list = (cli_handle_t **)kmalloc_nofail(NSC_MAX_NODE_VALUE
						      * sizeof(cli_handle_t *));
	handle_list_ptr = handle_list;
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next) {
		if (ent->node == this_node || ent->node == from_node)
			continue;
		status = clms_get_node_status(ent->node);
		if (!(status & (CLMS_NODE_UP |
				CLMS_NODE_HALF_UP |
				CLMS_NODE_COMING_UP)))
			continue;
		*handle_list_ptr = NULL;
		error = RCLMS_SHUTDOWN_FREEZE_SEND(ent->node,
						   handle_list_ptr,
						   this_node);
		if (icscli_async_sent(handle_list_ptr)) {
			nodelist[nodes++] = ent->node;
			handle_list_ptr++;
		} else if (error != -EREMOTE)
			panic("%s: Error %d from RCLMS_SHUTDOWN_FREEZE_SEND"
			      " to node %u\n", __FUNCTION__, error, ent->node);
	}
	handle_list_ptr = handle_list;
	for (i = 0; i < nodes; i++) {
		error = RCLMS_SHUTDOWN_FREEZE_RECEIVE(*handle_list_ptr++,
						      rval,
						      this_node);
		if (error == 0)
			error = *rval;
		if (error != -EREMOTE && error != 0)
			panic("rclms_shutdown_freeze:  Error %d from RCLMS_SHUTDOWN_FREEZE_RECEIVE to node %u\n",
			      error,
			      nodelist[i]);
	}
	kfree(handle_list);
	*rval = 0;
	return(0);
}

/*
 * clms_sigcluster_all()
 *	This function sends a SIGCLUSTER to all local processes.
 *
 *	Not anymore. It now sends a signal to any process that
 *	opens /proc/cluster/events and sets the proper fcntls to
 *	have signals sent to it.
 *
 * Description:
 *	The signal is posted to all interested processes to indicate a
 *	node transition (i.e. up, down, etc..) has occurred.  Processes
 *	can choose to catch this signal if they want to be informed of node
 *	transitions.  See cluster_events_register_signal() in libcluster.
 *
 * Parameters:
 *	None.
 */
void
clms_sigcluster_all(void)
{
	kill_fasync(&proc_cluster_events_fasync_list, SIGIO, POLL_IN);
}

/*
 * rclms_going_away()
 *	We're being informed that a node is about to reboot or halt.
 *
 * Description:
 *	A node is being shut down, so he's telling us of that fact so when
 *	the regroup notification arrives, we'll know why.
 *
 * Calling/Exit State:
 *      This is the server procedure for an rpc and is called from a
 *      server proc.
 *
 * Parameters:
 *	clusternode_t	node		This node.
 *	int		*rval		Return value of the RPC.
 *	clusternode_t	from_node	The node that sent us the RPC.
 *
 * Returns:
 *	0.
 */
int
rclms_going_away(
	clusternode_t node,
	int *rval,
	clusternode_t from_node)
{
	int i, nodes, status, error;
	clusternode_t nodelist[NSC_MAX_NODE_VALUE];
	clms_secondary_list_t *ent;
	cli_handle_t **handle_list, **handle_list_ptr;

	CLMSLOG(CLMSLOG_GOINGAWAY, from_node, 0, 0, 0, 0, 0, 0);
	*rval = 0;
	LOCK_COND_LOCK(&clms.clms_node_lock);
	clms.clms_node_info[from_node].flags |= CLMS_NODE_GOING_AWAY;
	UNLOCK_COND_LOCK(&clms.clms_node_lock);
	/*
	 * If we're not the CLMS master, we're done.
	 */
	if (this_node != clms_master_node)
		return(0);
	/*
	 * Tell all CLMS secondaries about this; we need to do this so
	 * CLMS failover doesn't have to reconstruct it.  Don't worry
	 * about telling the node that told us or the CLMS master, they
	 * already know.
	 */
	nodes = 0;
	handle_list = (cli_handle_t **)kmalloc_nofail(NSC_MAX_NODE_VALUE
						      * sizeof(cli_handle_t *));
	handle_list_ptr = handle_list;
	for (ent = clms_secondary_list_head; ent != NULL; ent = ent->next) {
		if (ent->node == this_node || ent->node == from_node)
			continue;
		LOCK_COND_LOCK(&clms.clms_node_lock);
		status = clms_get_node_status(ent->node);
		UNLOCK_COND_LOCK(&clms.clms_node_lock);
		if (!(status & (CLMS_NODE_UP |
				CLMS_NODE_HALF_UP |
				CLMS_NODE_COMING_UP |
				CLMS_NODE_HALF_DOWN)))
			continue;
		*handle_list_ptr = NULL;
		error = RCLMS_GOING_AWAY_SEND(ent->node,
					      handle_list_ptr,
					      from_node);
		if (icscli_async_sent(handle_list_ptr)) {
			nodelist[nodes++] = ent->node;
			handle_list_ptr++;
		} else if (error != -EREMOTE)
			panic("%s: Error %d from RCLMS_GOING_AWAY_SEND"
			      " to node %u\n", __FUNCTION__, error, ent->node);
	}
	handle_list_ptr = handle_list;
	for (i = 0; i < nodes; i++) {
		error = RCLMS_GOING_AWAY_RECEIVE(*handle_list_ptr++,
						 rval,
						 from_node);
		if (error == 0)
			error = *rval;
		if (error != -EREMOTE && error != 0)
			panic("rclms_going_away:  Error %d from RCLMS_GOING_AWAY_RECEIVE to node %u\n",
			      error,
			      nodelist[i]);
	}
	kfree(handle_list);
	*rval = 0;
	return(0);
}

/*
 * rclms_going_away_fast()
 *	We're being informed that a node is about to reboot or halt.
 *
 * Description:
 *	This is the message version of rclms_going_away().  It just calls
 *	that routine to do the work.
 *
 * Calling/Exit State:
 *      This is the server procedure for an rpc and is called from a
 *      server proc.
 *
 * Parameters:
 *	clusternode_t	node		This node.
 *	clusternode_t	from_node	The node that sent us the RPC.
 *
 * Returns:
 *	0.
 */
int
rclms_going_away_fast(
	clusternode_t node,
	clusternode_t from_node)
{
	int rval;

	(void)rclms_going_away(node, &rval, from_node);
	return(0);
}
