/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
*/
 
/*++
"This software program is available to you under a choice of one of two 
licenses.  You may choose to be licensed under either the GNU General Public 
License (GPL) Version 2, June 1991, available at 
http://www.fsf.org/copyleft/gpl.html, or the Intel BSD + Patent License, 
the text of which follows:

"Recipient" has requested a license and Intel Corporation ("Intel") is willing 
to grant a license for the software entitled InfiniBand(tm) System Software 
(the "Software") being provided by Intel Corporation.

The following definitions apply to this License: 

"Licensed Patents" means patent claims licensable by Intel Corporation which 
are necessarily infringed by the use or sale of the Software alone or when 
combined with the operating system referred to below.

"Recipient" means the party to whom Intel delivers this Software.
"Licensee" means Recipient and those third parties that receive a license to 
any operating system available under the GNU Public License version 2.0 or 
later.

Copyright (c) 2006 Intel Corporation. All rights reserved. 

The license is provided to Recipient and Recipient's Licensees under the 
following terms.  

Redistribution and use in source and binary forms of the Software, with or 
without modification, are permitted provided that the following conditions are 
met: 
Redistributions of source code of the Software may retain the above copyright 
notice, this list of conditions and the following disclaimer. 

Redistributions in binary form of the Software may reproduce the above 
copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution. 

Neither the name of Intel Corporation nor the names of its contributors shall 
be used to endorse or promote products derived from this Software without 
specific prior written permission.

Intel hereby grants Recipient and Licensees a non-exclusive, worldwide, 
royalty-free patent license under Licensed Patents to make, use, sell, offer 
to sell, import and otherwise transfer the Software, if any, in source code 
and object code form. This license shall include changes to the Software that 
are error corrections or other minor changes to the Software that do not add 
functionality or features when the Software is incorporated in any version of 
a operating system that has been distributed under the GNU General Public 
License 2.0 or later.  This patent license shall apply to the combination of 
the Software and any operating system licensed under the GNU Public License 
version 2.0 or later if, at the time Intel provides the Software to Recipient, 
such addition of the Software to the then publicly available versions of such 
operating system available under the GNU Public License version 2.0 or later 
(whether in gold, beta or alpha form) causes such combination to be covered by 
the Licensed Patents. The patent license shall not apply to any other 
combinations which include the Software. No hardware per se is licensed 
hereunder. 
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL INTEL OR ITS CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
--*/

/*
* NAME
*   Proc filesystem support
* COPYRIGHT
*   GNU General Public or Intel BSD + Patent License
* PURPOSE
*   Suport reads and writes (from or to) '/proc/sys/ics_ib/{iostatus,status}'.
*   Writes are used to alter ICS IB internal debug state, display debug info
*   and execute debug/unit_test routines. Reading '/proc/sys/ics_ib/status'
*   displays current ICS IB status information.
* AUTHOR
*   Stan C. Smith    stan.smith@intel.com
* HISTORY
*   $Id$
*/

#include <linux/module.h>

#ifdef	CONFIG_PROC_FS

#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/seq_file.h>
#include <linux/sockios.h>
#include <linux/net.h>
#include <net/sock.h>
#include <linux/in.h>
#include <linux/inetdevice.h>
#include <linux/netdevice.h>
#include <linux/ip.h>
#include <linux/sysctl.h>
#include <linux/fs.h>

#include <asm/atomic.h>
#include <asm/uaccess.h>

#include <cluster/ics/ics_private.h>

#define ICS_PROC_FS_NODE "cluster/ics_ib"

extern ib_nexus_t ib_nexus;	// ics_ib_config.c
extern struct proc_dir_entry *proc_cluster;

static struct proc_dir_entry *proc_ics_ib;
static struct proc_dir_entry *proc_ics_ib_stats;
static struct proc_dir_entry *proc_ics_ib_iostat;

/* 'get_info' data buffer */

static char *gi_data = NULL;
static int  gi_data_size = 4096;

static int stat_show(struct seq_file *m, void *p);
static int iostat_show(struct seq_file *m, void *p);

#ifdef CONFIG_SYSCTL_X

struct ctl_table_header *ics_ib_sysctl;	// set when sysctl is registered.

static int ib_gen_data(ctl_table *table, int write, struct file *filp,
					void *buffer, size_t *lenp, loff_t *);
#define ICS_IB_SYSCTL		300
#define ICS_IB_SYSCTL_STATUS     1
#define ICS_IB_SYSCTL_DEBUG      2
#define ICS_IB_SYSCTL_DEBUG_CMD  3
#define ICS_IB_SYSCTL_IOSTAT     4

static ctl_table ics_ib_ctl_table[] = {

	// NULL data buffer replaced in ib_status() call
        {ICS_IB_SYSCTL_STATUS, "stat", NULL, 2000, 0644, NULL,
         	&ib_gen_data, &sysctl_string },

        {ICS_IB_SYSCTL_DEBUG, "debug", &ics_ib_debug, sizeof (int),
         	0644, NULL, &proc_dointvec},

	// NULL data buffer replaced in ib_status() call
        {ICS_IB_SYSCTL_DEBUG_CMD, "dbgcmd", NULL, 2000, 0644, NULL,
         	&ib_gen_data, &sysctl_string },

	// NULL data buffer replaced in ib_status() call
        {ICS_IB_SYSCTL_IOSTAT, "iostat", NULL, 2000, 0644, NULL,
         	&ib_gen_data, &sysctl_string },

        { 0 }
};


static ctl_table ics_ib_top_ctl_table[] = {
        {ICS_IB_SYSCTL, "ics_ib", NULL, 0, 0555, ics_ib_ctl_table},
        { 0 }
};
#endif // CONFIG_SYSCTL

/*
 * Format interesting ICS IB statistics (/proc/sys/ics_ib/stats).
 */
static int
stat_show(struct seq_file *m, void *p)
{
        int rc;
	ib_nexus_t *nd = &ib_nexus;
	ib_conn_t *conn;
	struct list_head *List, *pos, *tmp;
	char *recv_mesg_processor =
#if KSVR_TASKLET
		"Ksvr_tasklet";
#elif KSVR
		"Ksvr_threads";
#else
		"device interrupt?";
#endif

	seq_printf(m, "ICS_IB(0.0.2) IB msg payload(%dK) Mesg processor(%s)\n",
				IBA_MAX_BUF_SIZE/1024,recv_mesg_processor);

	seq_printf(m, "   IB nid[%s] connections(cur %d total %u)\n",
                        nid_str(nd->my_hid.nid),
                        atomic_read(&nd->num_connected),
                        nd->conn_total);

        seq_printf(m, "   RX global descriptors: free %d gets %d "
			"in-use %d in-use-max %d \n",
				nd->rx_pool_free, nd->rx_pool_gets,
				nd->rx_pool_in_use, nd->rx_pool_in_use_max );

        if ( (rc=atomic_read(&nd->unclaimed_tx_resources)) > 0 )
                seq_printf(m,"   Reclaimable TX resources: %d\n",rc);

        seq_printf(m, "   TX descriptors:\n       "
                       "blocking (in-use %u in-use-max %u free %u empty %u)\n",
                                nd->txds_in_use, nd->txds_max_in_use,
                                nd->txds_free, nd->txds_empty);

        seq_printf(m, "       "
                "non-blocking (in-use %u in-use-max %u free %u empty %u)\n",
                                nd->nblk_txds_in_use, nd->nblk_txds_max_in_use,
                                nd->nblk_txds_free, nd->nblk_txds_empty);

        List = &nd->active_conn;
	list_for_each_safe(pos,tmp,List) {
		conn = list_entry(pos,ib_conn_t,conn_list);

                if ( BAD_MAGIC(conn) ) {
		        seq_printf(m,"BAD conn magic?\n");
		        continue;
		}
       		seq_printf(m,"node %-2d @ nid [%s]",
				conn->r_hid.node, nid_str(conn->r_hid.nid));
                seq_printf(m, " '%s' posted(Rx %u Tx %u)\n",
					IB_conn_state_str(conn),
					atomic_read(&conn->rx_posted),
					atomic_read(&conn->tx_posted));
	}
	return 0;
}

#ifdef CONFIG_SYSCTL_X
/*
 * Handle writes to '/proc/cluster/ics_ib/ctl'.
 *
 * INPUTS:
 *	file	struct file ptr.
 *	buffer	data from the user write to '/proc/cluster/ics_ib/ctl'
 *	count	number of bytes in 'buffer'
 *  data	== 0 ?
 *
 * RETURN VALUE:
 *	number of bytes written, zero or negative errno.
 */

typedef struct _doit {
	char	*cmd;
	int		cmd_len;
	void	(*func)(char *);
} doit_t;


// debug functions
//	'echo debug {on/off/all}' > /proc/cluster/ics_ib/ctl'
// cm.c
extern void ics_ib_set_debug(char *);
// XXX extern void ics_ib_find_copy_threshold(char *cmd);

static doit_t	doit[] =
{
	{"debug",       	5, ics_ib_set_debug},
// XXX	{"threshold",       	9, ics_ib_find_copy_threshold},
	{NULL,		        0, NULL}
};

static int
dbg_cmd( char *dcmd, int count, char *errmsg, int errmsg_len )
{
	char	*cp = dcmd;
	doit_t	*D;

	// scan doit cmd table for a debug command match
	for(D=doit; D->cmd; D++)
	{
		// command match something we recognize?
		if ( strncmp(cp,D->cmd,D->cmd_len) == 0 )
		{
			unchar	c;

			c = *(cp+D->cmd_len);
			if ( c == '\0' || c == ' ' ) {
				// pass cmd string minus cmd which matched.
				D->func( (cp+D->cmd_len) );
				return count;	// all done: # of chars in dcmd.
			}
		}
	}

	// did not match a command - tell somebody
	cp = errmsg;
	cp += sprintf(cp,"%s%s bad cmd '%s' usage: [ ",WHOAMI,dcmd);
	for(D=doit; D->cmd; D++) {
		cp += sprintf(cp,"%s ",D->cmd);
	}
	cp += sprintf(cp,"]");

	return -EPERM;
}
#endif // CONFIG_SYSCTL_X

static int stat_open(struct inode *inode, struct file *file)
{
        return single_open(file, stat_show, NULL);
}

static struct file_operations proc_stats_ops = {
        .owner   = THIS_MODULE,
        .open    = stat_open,
        .read    = seq_read,
        .llseek  = seq_lseek,
        .release = single_release,
};

static int iostat_open(struct inode *inode, struct file *file)
{
        return single_open(file, iostat_show, NULL);
}

static struct file_operations proc_iostat_ops = {
        .owner   = THIS_MODULE,
        .open    = iostat_open,
        .read    = seq_read,
        .llseek  = seq_lseek,
        .release = single_release,
};

/*
* NAME
*       create_ib_proc_fs -- Create the specified proc filesystem entries
* DESCRIPTION
*       Create the /proc/cluster/ics_ib filesystem nodes. Called after
*	/proc/cluster/ is created (see fs/proc/proc_cluster_init.c) only
*	when CONFIG_ICS_IB_VERBS is defined.
* PARAMETERS
*       none.
* RETURN VALUE
*       SUCCESS(0) or negative errno.
* SIDE EFFECTS
*       FS node created in /proc/cluster/ics_ib, a file buffer is allocated.
* SEE ALSO
*       destroy_ib_proc_fs
********/

int 
create_ib_proc_fs(void)
{
	char *basedir = ICS_PROC_FS_NODE;

	if (proc_ics_ib)
		return -EACCES; // already been here 

	if (!proc_cluster) {
		//printk("%s() **** /proc/cluster NOT avail **** \n",__func__);
		return -EPERM;
	}

	/*
	 * allocate the Proc FS data buffer for the get_info() call.
	 */
	if ((gi_data = (char *)vmalloc(gi_data_size)) == NULL) {
		printk("%s%s Unable to kmalloc get_info() buffer size %d?",
							WHOAMI, gi_data_size);
		return -ENOMEM;
	}

	proc_ics_ib = proc_mkdir(basedir, NULL);
	if (!proc_ics_ib) {
		printk("%s /proc/%s failed?\n",__func__,basedir);
		return -EPERM;
	}

	proc_ics_ib_stats = create_proc_entry("stats",
                               S_IFREG|S_IRUSR|S_IRGRP|S_IROTH,
                               proc_ics_ib);
        if (!proc_ics_ib_stats) {
		printk("%s /proc/%s/stats failed?\n",__func__,basedir);
		return -EPERM;
	}
	proc_ics_ib_stats->proc_fops = &proc_stats_ops;

	proc_ics_ib_iostat = create_proc_entry("iostats",
                               S_IFREG|S_IRUSR|S_IRGRP|S_IROTH,
                               proc_ics_ib);
        if (!proc_ics_ib_iostat) {
		printk("%s /proc/%s/iostats failed?\n",
			__func__,basedir);
		return -EPERM;
	}
	proc_ics_ib_iostat->proc_fops = &proc_iostat_ops;

	return SUCCESS;
}

/*
* NAME
*       destroy_ib_proc_fs -- destroy the specified proc filesystem entry
* DESCRIPTION
*       Destroy/unlink the /proc/cluster/ics_ib/xxx filesystem nodes.
* PARAMETERS
*	none.
* RETURN VALUE
*       none, void.
* SIDE EFFECTS
*       Proc filesystem node is unlinked, allocated memory buffers released.
* SEE ALSO
*       create_proc_fs
********/

void
destroy_ib_proc_fs(void)
{

	if (!proc_ics_ib)
		return; /* nothing to do */
	remove_proc_entry("iostat",proc_ics_ib);
	remove_proc_entry("stat",proc_ics_ib);
	remove_proc_entry(ICS_PROC_FS_NODE,proc_cluster);

	/* release Proc FS data buffer */
	if (gi_data) {
		vfree(gi_data);
		gi_data = NULL;
	}
}

static char *
scale64(u64 n, char *b)
{
	ulong	r1,r2;

	if ( n >= (1024*1024*1024ULL) ) {
		r1 = n / (1024*1024*1024ULL);
		if ( (r2 = n % (1024*1024*1024ULL)) == 0 )
			sprintf(b,"%luG",r1);
		else
			sprintf(b,"%lu.%02luG",r1,(r2/((1024*1024*1024)/100)));
	}
	else if ( n >= (1024*1024) ) {
		r1 = n / (1024*1024);
		if ( (r2 = n % (1024*1024)) == 0 )
			sprintf(b,"%luM",r1);
		else
			sprintf(b,"%lu.%02luM",r1,(r2/((1024*1024)/100)));
	}
	else if ( n >= (1024) ) {
		r1 = n / (1024);
		if ( (r2 = n % 1024) == 0 )
			sprintf(b,"%luK",r1);
		else
			sprintf(b,"%lu.%02luK",r1,(r2/(1024/100)));
	}
	else {
		sprintf(b,"%llu",n);
	}
	return b;
}


/*
 * Format interesting ICS IB io statistics.
 *
 * INPUTS:
 *  **bufPtr   where to return/store the address of the data
 * implicit inputs:
 *	gi_data	- get info data buffer address
 *
 *	gi_data_size - get info data buffer size, < PAGE_SIZE as read() has
 *			some failure points.
 * OUTPUTS:
 *  start of data in **bufPtr
 * RETURN VALUE:
 *  # of bytes available
 */
static void
gen_conn_iostats(ib_conn_t *conn, struct seq_file *m)
{
	char	s[120];

	if (BAD_MAGIC(conn)) {
		seq_printf(m,"BAD conn magic?\n");
		return;
	}

       	seq_printf(m,
	"node_%-2d %s posted(Rx %-3u min %-3u Tx %-3u max %-3u)\n",
				conn->r_hid.node,
				conn->r_hid.hostname,
				atomic_read(&conn->rx_posted),
				conn->min_rx_posted,
				atomic_read(&conn->tx_posted),
				conn->max_tx_posted);
	seq_printf(m, "    RDMA ops: src %d sink %d crc: errs %d good %d\n",
				conn->rdma_src_op,
				conn->rdma_sink_op,
				conn->rdma_crc_errs,
				conn->rdma_crc_ok);
	seq_printf(m, "    RDMA/msg: max ops %d, bytes: max %d min %d\n",
				conn->max_rdma_per_msg,
				conn->max_rdma_bytes,
				conn->min_rdma_bytes);

	seq_printf(m, " Bytes:\n"
			"    Rx: tot %-8s buf %-8s rdma.sink  %-8s\n"
			"    Tx: tot %-8s buf %-8s rdma.src   %-8s\n",
			scale64(conn->buf_rx_bytes+conn->rdma_sink_bytes,s),
			scale64(conn->buf_rx_bytes,(s+15)),
			scale64(conn->rdma_sink_bytes,(s+30)),
			scale64(conn->buf_tx_bytes+conn->rdma_src_bytes,(s+45)),
			scale64(conn->buf_tx_bytes,(s+60)),
			scale64(conn->rdma_src_bytes,(s+75)));
	seq_printf(m,
	" CQ: polls %d events %d ave/poll %d max/poll %d static max %d\n",
			conn->cq_polls,conn->cq_events,
			(conn->cq_polls > 0 ? conn->cq_events / conn->cq_polls
				: 0),
			conn->cq_max_per_poll, MAX_CQ_WC);
	seq_printf(m,
	" ICS inline: (msgs %d bytes %d) min %d max %d ave %d static max %d\n",
			conn->icsm_cnt,conn->icsm_inline_bytes,
			conn->icsm_inline_min,
			conn->icsm_inline_max,
			(conn->icsm_cnt > 0 ?
				conn->icsm_inline_bytes / conn->icsm_cnt
				: 0),
			ICS_MAX_INLINE_DATA_SIZE);
}

static int
iostat_show(struct seq_file *m, void *p)
{
	ib_nexus_t *nd = &ib_nexus;
	struct list_head *List, *pos, *tmp;
	ib_conn_t *conn;

        List = &nd->active_conn;

	list_for_each_safe(pos,tmp,List) {
		conn = list_entry(pos,ib_conn_t,conn_list);
		gen_conn_iostats(conn,m);
	}
	return 0;
}
#endif	// CONFIG_PROC_FS

