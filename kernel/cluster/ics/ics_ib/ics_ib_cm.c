/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
 */

/*++
"This software program is available to you under a choice of one of two 
licenses.  You may choose to be licensed under either the GNU General Public 
License (GPL) Version 2, June 1991, available at 
http://www.fsf.org/copyleft/gpl.html, or the Intel BSD + Patent License, 
the text of which follows:

"Recipient" has requested a license and Intel Corporation ("Intel") is willing 
to grant a license for the software entitled InfiniBand(tm) System Software 
(the "Software") being provided by Intel Corporation.

The following definitions apply to this License: 

"Licensed Patents" means patent claims licensable by Intel Corporation which 
are necessarily infringed by the use or sale of the Software alone or when 
combined with the operating system referred to below.

"Recipient" means the party to whom Intel delivers this Software.
"Licensee" means Recipient and those third parties that receive a license to 
any operating system available under the GNU Public License version 2.0 or 
later.

Copyright (c) 2006 Intel Corporation. All rights reserved. 

The license is provided to Recipient and Recipient's Licensees under the 
following terms.  

Redistribution and use in source and binary forms of the Software, with or 
without modification, are permitted provided that the following conditions are 
met: 
Redistributions of source code of the Software may retain the above copyright 
notice, this list of conditions and the following disclaimer. 

Redistributions in binary form of the Software may reproduce the above 
copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution. 

Neither the name of Intel Corporation nor the names of its contributors shall 
be used to endorse or promote products derived from this Software without 
specific prior written permission.

Intel hereby grants Recipient and Licensees a non-exclusive, worldwide, 
royalty-free patent license under Licensed Patents to make, use, sell, offer 
to sell, import and otherwise transfer the Software, if any, in source code 
and object code form. This license shall include changes to the Software that 
are error corrections or other minor changes to the Software that do not add 
functionality or features when the Software is incorporated in any version of 
a operating system that has been distributed under the GNU General Public 
License 2.0 or later.  This patent license shall apply to the combination of 
the Software and any operating system licensed under the GNU Public License 
version 2.0 or later if, at the time Intel provides the Software to Recipient, 
such addition of the Software to the then publicly available versions of such 
operating system available under the GNU Public License version 2.0 or later 
(whether in gold, beta or alpha form) causes such combination to be covered by 
the Licensed Patents. The patent license shall not apply to any other 
combinations which include the Software. No hardware per se is licensed 
hereunder. 
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL INTEL OR ITS CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
--*/


/*
* NAME
*       Infiniband Connection Management
* COPYRIGHT
*       GNU General Public(GPL) or Intel BSD + Patent License
* PURPOSE
*	Routines which interface to the openIB.org Infiniband Access layer
*	to create and destroy Infiniband (IB) connections.
* AUTHOR
*       Stan C. Smith    stan.smith@intel.com
* DESCRIPTION
*
* ICS nodes post a persistent IB listen on service ports for ICS service
* requests and CLMS probes.
*
* -- Server Side Connection Establishment --
*
* Since IB registered transmit buffers come from a globally accessible pool,
* each server connection delays adding it's transmit resources to the global
* pool until the final stages of the connection handshake; reception of the RTU
* message. Upon arrival of the RTU message, the server will allocate transmit
* buffers, IB register said buffers and atomically add the transmit buffers
* to the global tx free pool.
*
* Given the transmit pool is global, a connection can not uniquely locate or
* isolate it's transmit donated resources during connection shutdown. Therefore
* a connection's transmit buffers remain in the global pool until the ibnal
* module is unloaded. The design expectation is that connections will become
* established and be long lived, not rapidly closing and reopening. During
* connection shutdown, the connection's donated transmit resources are recorded
* so the next connection creation can skip the step of donating TX resources
* and reuse those donated by a previous connection.
*
* A connection request can be rejected during the REQ message processing (IB
* resource allocation failure for instance) or during RTU processing (unable to
* allocate transmit buffers) in the RTU callback. Once a connection receives or
* sends a REJECT, the connection is destroyed with resources released.
*
* -- Client Side Connection Establishment --
*
* -- Connection Shutdown --
*
* The active connection struct (ib_conn_t) state is transitioned to the
* state of DISCONNECTING. At this juncture, the queue-pair is destroyed.
* Each IB posted receive buffer generates a Completion Queue work-completed
* (WC) event. If the WC event status is not success as in the case of shutdown,
* then the posted recv count for this connection is decremented as the recv
* buffer (kib_rx_t) will not be reposted. When the posted RX count and posted
* transmit count both drop to zero, then the remaining IB resources are released
* The IB connection is now canceled and destroyed.
*
* The expected server connection rate during cluster node startup will be
* high in that large numbers of nodes will be requesting an server connection;
* (probe & ICS connections).  Therefore ICS servers can expect initally high
* connection rates which soon drop to zero once all nodes are up. Therefore
* the connection rate can be viewed as transient resource demand with resources
* dynamically assigned.
*******/

#include <linux/ctype.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/errno.h>

#include <cluster/nsc.h>
#include <cluster/ics/ics_private.h>

/*
 * externals & forward references
 */
static void pool_tx_resources(ib_conn_t *kconn);
static ib_conn_t *alloc_new_connection(void);
static int alloc_conn_resources(ib_conn_t *kconn, int rx_post);
static int ib_post_rx_buffers(ib_conn_t *kconn);

extern void ics_probe_clms_daemon(void*);
extern void ics_accept_connection(void*);


/*
 * return an asciz string which represents the connection state.
 */

char *
IB_conn_state_str(ib_conn_t *conn)
{
	int		cs;
	static char	*cs_names[IBA_STATE_MAX]= 
					{ "IBA_DISCONNECTED",
					  "IBA_CONNECTING",
					  "IBA_CONNECTED",
					  "IBA_DISCONNECTING",
					  "IBA_CLOSING",
					  "IBA_REJECT",
					  "IBA_REJECTED",
					  "IBA_ERROR" };
			// cs_names MUST match IBA_ states in ics_ib_private.h

	cs = IB_GET_CONN_STATE(conn);
	if ( cs >= 0 && cs < IBA_STATE_MAX )
		return cs_names[cs];
	
	printk("%s%s bad state(%d)?\n",WHOAMI,cs);

	return("BAD conn state?");
}


/*
* DESCRIPTION -- generate a printable IP address string
*       Generate an IP address string from a nid (Network ID). A nid is a
*	network byte-order binary representation of an IPv4 network address.
* PARAMETERS
*    nid
*       [in] a binary (net byte-order) IPv4 address(32-bit)
*    buf
*       [out] IP address string in dot notation is written to this address.
* RETURN VALUE
*    Address of input argument 'buf'.
* NOTES
*       IP address is output in network byte-order as an asciz string suitable
*	for use in printk().
*/

char *
nid_2_str(u32 nid, char *buf)
{
	unchar	*p = (unchar *)&nid;	// net-order

	sprintf(buf,"%d.%d.%d.%d",p[0],p[1],p[2],p[3]);
	return buf;
}

char *
nid_str(u32 nid)
{
	static char buf[16];

	unchar	*p = (unchar *)&nid;	// net-order
	sprintf(buf,"%d.%d.%d.%d",p[0],p[1],p[2],p[3]);
	return buf;
}

char *
nid_2_str_p(u32 *addr_ho, char *buf)
{
	unchar	*p = (unchar*)addr_ho;	// net-order

	sprintf(buf,"%d.%d.%d.%d",p[0],p[1],p[2],p[3]);
	return buf;
}

char *
ib_event_str(enum ib_event_type event)
{
	static char *ib_events[IB_EVENT_CLIENT_REREGISTER+1] = {
        		"IB_EVENT_CQ_ERR",
        		"IB_EVENT_QP_FATAL",
        		"IB_EVENT_QP_REQ_ERR",
        		"IB_EVENT_QP_ACCESS_ERR",
        		"IB_EVENT_COMM_EST",
        		"IB_EVENT_SQ_DRAINED",
        		"IB_EVENT_PATH_MIG",
        		"IB_EVENT_PATH_MIG_ERR",
        		"IB_EVENT_DEVICE_FATAL",
        		"IB_EVENT_PORT_ACTIVE",
        		"IB_EVENT_PORT_ERR",
        		"IB_EVENT_LID_CHANGE",
        		"IB_EVENT_PKEY_CHANGE",
        		"IB_EVENT_SM_CHANGE",
        		"IB_EVENT_SRQ_ERR",
        		"IB_EVENT_SRQ_LIMIT_REACHED",
        		"IB_EVENT_QP_LAST_WQE_REACHED",
        		"IB_EVENT_CLIENT_REREGISTER"};
	static char bad[40];

	if (event >= IB_EVENT_CQ_ERR && event <= IB_EVENT_CLIENT_REREGISTER)
		return ib_events[event];
	
	sprintf(bad,"%s bad IB event code (%d)?\n",__func__,event);

	return bad;
}

/*
* DESCRIPTION -- CQ Async event callback
*	An Infiniband asynchronous event has occured on a CQ. The event is
*	not associated with a completion occurs on the CQ.
* PARAMETERS
*    *ib_event	IB asychronous event 
*    *data	pointer to event specific data
* RETURN VALUE
*	VOID
* SIDE EFFECTS
* SEE ALSO
*/

static void
_async_ib_event_cb(struct ib_event *evp, void *data)
{
	dprintk((DBG_CONN+DBG_SHUTDOWN+DBG_SEND+DBG_RECV),
		("%s %s\n",__func__,ib_event_str(evp->event)));
}

/*
* DESCRIPTION -- send a connection REJECT message
*	Construct and transmit a connection REJECT message containing the
*	reject status code accompanied by an textual error string.
* PARAMETERS
*    * cm_id
*	[in] RDMA CM (Connection Manager) ID
*    *err_msg
*	[in] Error string
* RETURN VALUE
*	VOID
*/
static void
cm_reject(struct rdma_cm_id *cm_id, char *err_msg)
{
	int	rc, len;

	// CM reject due to local problems.
	len = strlen((char *)err_msg) + 1; // + <null>
	rc = rdma_reject(cm_id, (void*)err_msg, (u8)len);
	if (rc) {
		printk( "%s%s ERR: rdma_rej() failure(%d)\n",WHOAMI,rc);
	}
}


/*
* DESCRIPTION -- complete the passive connection.
*	Server side callback routine which fires after the arrival of an
*	RTU message (Ready_To_Use) from the client/connect side. Add this
*	connection's transmit resources to the global TX pool and set the
*	connection state to ESTABLISHED; ready for data xfer.
* PARAMETERS
*    *cm_id
*	[in] CM (Connection Manager) ID struct
*    *event
*	[in] CM event ptr.
* RETURN VALUE
*	VOID
* SIDE EFFECTS
*	upon return from this function the IB connection is established and
*	ready for data transfer.
*/

static void
finish_passive_connect(ib_conn_t *kconn, struct rdma_cm_id *cm_id,
			struct rdma_cm_event *event)
{
	int rc;
	ulong flags;

	/*
	 * has the connection state changed since receiving the REQ and sending
	 * a REPLY? If so, then reject. We should be in IBA_CONNECTING which
	 * implies waiting for the RTU.
	 */
	if (IB_GET_CONN_STATE(kconn) != IBA_CONNECTING) {
		if ( IB_GET_CONN_STATE(kconn) != IBA_REJECTED ) {
			CWARN("Bad connection state(%d), REJECTed\n",
						IB_GET_CONN_STATE(kconn));
			// reject() the request due to local problems.
			cm_reject(cm_id,"Bad connection state?");
			write_lock_irqsave(&kconn->conn_lock,flags);
			IB_SET_CONN_STATE(kconn,IBA_REJECT);
			write_unlock_irqrestore(&kconn->conn_lock,flags);
			ib_async_destroy_conn(kconn);
			return;
		}
	}

	/*
	 * merge this connections TX resources into the global TX pool.
	 */
	pool_tx_resources(kconn);

	/*
	 * connection to client has been established - record the fact.
	 */
	IB_SET_CONN_STATE(kconn,IBA_CONNECTED);

	kconn->r_nid = kconn->r_hid.nid; // so connection can be located by nid
	atomic_inc(&kconn->ib_nexus->num_connected);
	kconn->ib_nexus->conn_total++;

	dprintk(DBG_CONN_V,("%s connected to '%s' @ [%s]\n", ME,
			kconn->r_hid.hostname, nid_str(kconn->r_hid.nid)));

	/*
	 * create a thread to service the connection.
	 */
	switch (kconn->svc_port) {

	  case ICS_SVC_PORT:
		dprintk(DBG_CONN_V,
			("%s spawn ics_accept_connection(%p) port %d\n",
						__func__,kconn,ICS_SVC_PORT));
		rc = spawn_daemon_thread("ics_accept_connection",
					ics_accept_connection,
					(void *)kconn);
		break;

#if 0 // when UNACK migrates from IPoIB (sock) to IB UDatagrams.
	  case ICS_UNACK_SVR_PORT:
		/*
		* Set up the ICS unacknowledged services.
		*/
		rc = spawn_daemon_thread("ics_llunack_daemon",
						ics_llunack_daemon,
						(void*)kconn);

		break;
#endif

	  case ICS_PROBE_CLMS_PORT:
		/* indicate this connection uses the direct receive from the
		 * received-message_queue, no ICS upcall.
		 */
		dprintk(DBG_CONN_V,
		("%s spawn ics_probe_clms_daemon(%p) port %d\n",
					__func__,kconn,ICS_PROBE_CLMS_PORT));
		rc = spawn_daemon_thread("ics_probe_clms_daemon",
					ics_probe_clms_daemon,
					(void *)kconn);
		break;

	  default:
		CWARN("Unknown service port(%d) no processor? destroying.\n",
							kconn->svc_port);
		ib_async_destroy_conn( kconn );
		rc = -EPERM;
	}
	if (rc) CWARN("Bad spawn...\n");
}


/*
 * Inform ICS this node has lost communication with the specified node.
 */

static void dead_node(void *nodenum)
{
	ics_nodedown_notification((clusternode_t)nodenum);
}

/*
* DESCRIPTION -- Connection disconnect request
*	The Remote side has sent a connection disconnect request (DREQ).
*	The rdma access layer has called back to handle the event. Verify
*	the connection state and mark this connection for asynchronous
*	destruction as this is a non-blocking context.
* PARAMETERS
*    * cm_dreq_rec
*	[in] Connection Manager disconnect request record pointer
* RETURN VALUE
*	VOID
* SIDE EFFECTS
*	Connection is marked for asynchronous destruction.
*/

static void
rdma_cm_disconnect(struct rdma_cm_id *cm_id, struct rdma_cm_event *event)
{
	int		prior_state;
	ib_conn_t	*ibc = (ib_conn_t*)cm_id->context;
	long		flags;

	if (BAD_MAGIC(ibc)) {
		printk("%s BAD MAGIC conn %p\n",__func__,ibc);
	}

	/*
	 * transition state to reflect our remote peer has requested connection
	 * termination, proceed with the close.
	 */
	write_lock_irqsave(&ibc->conn_lock,flags);
	if ((prior_state=IB_GET_CONN_STATE(ibc)) != IBA_DISCONNECTING) {
		IB_SET_CONN_STATE(ibc,IBA_CLOSING);
	}
	write_unlock_irqrestore(&ibc->conn_lock,flags);

	dprintk(DBG_SHUTDOWN,("%s lost connection to [%s].%d status %d\n",
		__func__,nid_str(ibc->r_hid.nid),ibc->svc_port,event->status));

#if 0
	/*
	 * node down notification only if established connection & ICS service
	 * port, otherwise the connection is never removed as ICS doesn't
	 * consider a node up due to a probe connection.
	 */
	if (prior_state == IBA_CONNECTED && ibc->r_hid.node == ICS_SVC_PORT) {
       		Dprintk(DBG_CONN,("%s conn(%p) queued node(%d) down\n",
				__func__,ibc,ibc->r_hid.node));

		INIT_WORK(&ibc->work,dead_node,(void*)ibc->r_hid.node);
     		queue_work(ibc->ib_nexus->wq, &ibc->work);
	}
	else
#endif
		ib_async_destroy_conn(ibc);
}

/*
* DESCRIPTION -- node has received an rdma CM connection request.
*	A posted IB listen() has received a connection request, the IB Access
*	layer calls this routine to handle the connection request.
*	Connection resources are allocated, Infiniband CQ, QP, receive buffers
*	are allocated, IB memory registered and posted, transmit resources are
*	allocated. Finally a CM REPLY is sent back to the client (connection
*	requestor). The connection now waits for the arrival of a CM RTU (Ready
*	To Use) message. RTU message arrival completes the 3-way connection
*	handshake.
* PARAMTERS
*	*cm_id	CM id tag for this specific connection, not a listen cm_id.
*	*event	rdma CM (Connecton Manager) event
* RETURN VALUE
*	VOID
*/

static void
rdma_passive_connect(struct rdma_cm_id *cm_id, struct rdma_cm_event *event)
{
	ib_listen_t		*ld;
	struct rdma_conn_param	conn_param;
	int			rc;
	ib_conn_t		*kconn;
	ib_host_id_t		*hid;

	ld = (ib_listen_t*)cm_id->context;	// IB listen() data

	// same DMA device? Implies the PD (Protection Domain is valid).
	SSI_ASSERT(cm_id->device->dma_device == ib_nexus.device->dma_device);

	if ((kconn = alloc_new_connection()) == NULL) {
		CWARN("unable to alloc conn memory, REJECT.\n");
		/* reject this connection */
		cm_reject(cm_id,"unable to alloc conn memory?");
		return;
	}
	kconn->svc_port = ld->port;	// set our service port
#if 0
	if (ld->port == ICS_PROBE_CLMS_PORT) {
		kconn->rbcnt = 2;
printk("%s PROBE conn, reset RX bufcnt %d\n",__func__,kconn->rbcnt);
	}
#endif
	/*
	 * save interesting connection request data in our conn struct
	 */
	kconn->cm_id = cm_id; // save current CM id for disconnect.
	cm_id->context = (void*)kconn; // set conn ptr for connection context

	IB_SET_CONN_STATE(kconn,IBA_CONNECTING);

	/* record the clients ID data in the CM request private data.
	 * Expectation of private data format is ib_host_id_t:
	 *   4 bytes of client's IPv4 address in network byte-order (nid),
	 *   1 byte of hostname string length including the <null> byte.
	 *   followed by the ASCII string of hostname.
	 */
	SSI_ASSERT(event->private_data_len > 0);

	hid = (ib_host_id_t*)event->private_data;
	rc = HOST_ID_BASE_SIZE + hid->hostname_len; 
	memcpy((void*)&kconn->r_hid, event->private_data, rc);

	dprintk(DBG_CONN,("%s%s conn REQ from client @ [%s]\n",
			WHOAMI,nid_str(kconn->r_hid.nid)));
	/*
	 * allocate IB resources, unable to post RX buffers yet, do not pool
	 * allocated TX resources until the RTU arrives.
	 */
	if (alloc_conn_resources(kconn,FALSE)) {
		CWARN("conn RESOURCE alloc? REJECT\n");
		/* reject this connection */
		cm_reject(kconn->cm_id,"conn RESOURCE alloc failure?");
		IB_SET_CONN_STATE(kconn,IBA_REJECT);
		// async due to the context is a rdma_cm system thread which
		// is not PC to block; use our own work queue.
		ib_async_destroy_conn(kconn);
		return;
	}

	/*
	 * Post Receive buffers
	 */
	rc = ib_post_rx_buffers(kconn);
	if (rc) {
		char *msg;

		switch(rc) {
		  case -EINVAL:
			msg = "unable to allocate RX resources";
			break;
		  case -ENOMEM:
			// mem allocation failed? REJECT connection
			msg = "unable to alloc IB work-request memory?";
			break;
		  default:
			msg = "unable to post recv's?";
			break;
		}
		/* reject this connection */
		cm_reject(cm_id,msg);
		IB_SET_CONN_STATE(kconn,IBA_REJECT);
		ib_async_destroy_conn(kconn);
		return;
	}
	
	/*
	 * accept the connection request. Return to the connecting
	 * side - my HostID info as private data for debug purposes.
	 */
        memset(&conn_param, 0, sizeof conn_param);

	conn_param.private_data = (void*)&kconn->ib_nexus->my_hid;
	conn_param.private_data_len = (u8)(HOST_ID_BASE_SIZE +
					kconn->ib_nexus->my_hid.hostname_len);
	conn_param.responder_resources = MAX_PENDING_RDMA;
	conn_param.initiator_depth = MAX_PENDING_RDMA;
	conn_param.flow_control = 1;
	conn_param.retry_count = 2;
	conn_param.rnr_retry_count = 4;

        rc = rdma_accept(kconn->cm_id, &conn_param);
        if (rc) {
                CWARN("rdma_accept() failed(%d)\n",rc);
		IB_SET_CONN_STATE(kconn,IBA_REJECT);
		cm_reject(kconn->cm_id,"rdma_accept() failed");
		ib_async_destroy_conn(kconn);
		return;
        }
	// waiting for RTU - connection Established to arrive.
	dprintk(DBG_CONN,("%s%s Expecting RTU from [%s]\n",WHOAMI,
					nid_str(kconn->r_hid.nid)));
}


/*
* DESCRIPTION -- rejected connection
*	A client side connection request was rejected by the server side. The
*	arrival of a REJ message after this side has sent a connection request
*	REQ message causes this callback to fire.
* PARAMETERS
*    *cm_id	CM connection ID.
*    *event	CM event info
*    *context	pointer to relevant connection struct
* RETURN VALUE
*	void
* SIDE EFFECTS
*	Connection state is rejected and must be destroyed.
* SEE ALSO
*/

static void
rdma_cm_reject_handler( struct rdma_cm_id *cm_id, struct rdma_cm_event *event)
{
	ib_conn_t *kconn = (ib_conn_t*)cm_id->context;
	int ec;
	ulong flags;

	if (event->private_data_len > 0)
		printk("%s conn(%p) REJECTED '%s'\n", __func__,kconn,
						(char*)event->private_data);

	ec = event->status; //reject status code
	printk("%s REJ reason '%d'\n",__func__,ec);

	if (BAD_MAGIC(kconn)) {
		printk("%s%s BAD *conn(%p) MAGIC?\n",WHOAMI,kconn);
		return;
	}

	if (kconn->r_hid.nid) {
		CWARN("Rejected by peer '%s' @ [%s]\n",kconn->r_hid.hostname,
						nid_str(kconn->r_nid));
	}
	else if (kconn->r_nid) {
		CWARN("Rejected by peer @ [%s]\n",
						nid_str(kconn->r_nid));
	}
	write_lock_irqsave(&kconn->conn_lock,flags);
	IB_SET_CONN_STATE(kconn,IBA_REJECTED);
	write_unlock_irqrestore(&kconn->conn_lock,flags);

	if (waitqueue_active(&kconn->waitQ)) {
		dprintk(DBG_CONN,("%s%s wake_up() conn requestor\n",WHOAMI));
		wake_up_interruptible(&kconn->waitQ);
	}
	else
		ib_async_destroy_conn(kconn);
	
}

/*
* NAME
*	finish_active_connect -- Complete client-side connection handshake
* DESCRIPTION
*	Connection request (CR) was previously sent, the server replied with
*	a REP (reply) message to grant our connection hence the IB access layer
*	fires this callback. Post receive buffers, send the remote side an
*	RTU (READY_TO_USE) message which completes the connection handshake.
*	Merge/add this connection's TX resources to the global TX free pool.
* PARAMETERS
*    *kconn
*	[in] IB connection pointer.
*    *cm_id
*	[in] CM ID for this connection
*    *event
*	[in] CM event data to handle the REPLY
* RETURN VALUE
*	void - errors returned in conn->error
* NOTES
*	Connection can be cancelled/rejected if failure to post RX buffers.
********/

static void
finish_active_connect(ib_conn_t            *kconn,
                      struct rdma_cm_id    *cm_id,
                      struct rdma_cm_event *event)
{
	/*
	 * is the connection in the normal/expected state? If not, reject
	 */
	if ( IB_GET_CONN_STATE(kconn) != IBA_CONNECTING ) {
		if (IB_GET_CONN_STATE(kconn) != IBA_REJECTED) {
			CWARN("Bad connection state(%d), REJECT\n",
					IB_GET_CONN_STATE(kconn));
			// reject() the request due to local problems.
			cm_reject(cm_id,"Bad connection state?");
			kconn->error = -EPROTO;
			wake_up_interruptible(&kconn->waitQ);
			return;
		}
	}
	//printk("%s() private_data %p len %d\n",__func__,
	//		event->private_data,event->private_data_len);

	SSI_ASSERT(event->private_data && event->private_data_len > 0);

	if (event->private_data && event->private_data_len > 0) {
		ib_host_id_t *rhid=event->private_data;

		//printk("%s() private_data from remote nid [%s] hostname_len %d\n",__func__,
		//		nid_str(rhid->nid),rhid->hostname_len);
		if (rhid->hostname_len > MAX_HOSTNAME_SIZE) {
			rhid->hostname_len = MAX_HOSTNAME_SIZE;
		}
		memcpy((void*)&kconn->r_hid,
			event->private_data,
			rhid->hostname_len+HOST_ID_BASE_SIZE);
	}

	/*
	 * post recv buffers to my qp and then send RTU message to the server.
	 * Receive buffers and descriptors were previously allocated and
	 * IB registered prior to issuing the connect request.
	 */
	if (ib_post_rx_buffers(kconn)) {
		CWARN("Error posting rx buffers? REJECT\n");
		// reject() the request due to local problems.
		cm_reject(cm_id,"unable to post RX buffers");
		kconn->error = -ENOBUFS;
		wake_up_interruptible( &kconn->waitQ );
		return;
	}

	// merge this connections TX resources with the global pool
	pool_tx_resources(kconn);

	// connection is established & ready to use - record the fact.
	IB_SET_CONN_STATE(kconn,IBA_CONNECTED);

	atomic_inc(&kconn->ib_nexus->num_connected);
	kconn->ib_nexus->conn_total++;

	dprintk(DBG_CONN_V,("%s%s %d connections\n",WHOAMI,
			atomic_read(&kconn->ib_nexus->num_connected)));

	// wake up connect requestor with an established connection.
	kconn->error = SUCCESS;
	wake_up_interruptible(&kconn->waitQ);
}

/*
* DESCRIPTION
*	Dynamically allocate a new connection structure. Structure is linked
*	into the list of active connections with the state set as
*	IBA_DISCONNECTED. The nid argument is the local IP address. This local
*	IP address is only a tag, and used as a lookup key to find the IB port
*	GUID which to connect with.
* RETURN VALUE
*	Address of connection struct or NULL on error
* SIDE EFFECTS
*	Connection struct is linked into the ib_nexus.active_conn list.
********/


static ib_conn_t *
alloc_new_connection(void)
{
	ib_conn_t *_new;
	
	_new = (ib_conn_t*)kzmalloc(sizeof(ib_conn_t),GFP_KERNEL);
	if (!_new) {
                printk("%s%s Unable to kmalloc struct ib_conn_t\n",WHOAMI);
		return NULL;
	}
	SET_MAGIC(_new);
	IB_SET_CONN_STATE(_new,IBA_DISCONNECTED);
	_new->ib_nexus = &ib_nexus;	// global IB data

	init_waitqueue_head( &_new->waitQ );
	init_waitqueue_head( &_new->rmq_waitQ );

	INIT_LIST_HEAD(&_new->rmq);	// received data queue
	INIT_LIST_HEAD(&_new->txds);	// blocking tx descriptors
	INIT_LIST_HEAD(&_new->nb_txds);	// non-blocking tx descriptors

	rwlock_init(&_new->conn_lock);
	spin_lock_init(&_new->rmq_lock);

	/* add new connection struct to the global list of conn structures */
	write_lock( &ib_nexus.conn_lock );
	list_add_tail (&_new->conn_list, &ib_nexus.active_conn);
	write_unlock( &ib_nexus.conn_lock );

	/*
	 * Connections start life receiving data directly to the connection
	 * structure (rmq_xxx) proper. Received messages are queued to be
	 * consumed by ib_readmsg() until such a time that ics_connect() has
	 * been called, after this received messages are passed to ics_dgram();
	 * see ics_ib_io.c tag IB_WC_RECV.
	 */
	_new->rmq_receive = TRUE;

	_new->min_rdma_bytes = INT_MAX;
	_new->icsm_inline_min = INT_MAX;

	return _new;
}

/*
 * validate the specified connection is on the active connection list.
 */

static int
find_connection(ib_conn_t *ibc)
{
	ib_nexus_t *nd = &ib_nexus;
	struct list_head *pos, *tmp;

	write_lock(&nd->conn_lock);

	if (list_empty(&nd->active_conn)) {
		write_unlock(&nd->conn_lock);
		return(-ENOTCONN);
	}
	list_for_each_safe(pos, tmp, &nd->active_conn) {
		ib_conn_t *conn;

		conn = list_entry(pos, ib_conn_t, conn_list); 
		if (conn == ibc) {
			write_unlock(&nd->conn_lock);
			return 0;
		}
	}
	write_unlock(&nd->conn_lock);
	return(-ENXIO);
}

/*
 * DESCRIPTION
 *	Release all resources assoicated with this connection. Disconnect the
 *	connection if still connected, not in an error state. Destroy the IB
 *	Queue Pair which triggers the return of all posted receive buffers.
 *	Complain if the posted receive buffer count is not zero after 10
 *	seconds. IB unregister receive buffers and release RX buffer memory.
 *	Figure out if the TX resources are allocated and if they are merged into
 *	the global TX pool. Remove this connection from the active connection
 *	list and finally free the conn memory.
 * PARAMETERS
 *    *kconn
 *	[in] connection pointer
 * RETURN VALUE
 *	VOID
 */

void
ib_destroy_connection(ib_conn_t *ibc)
{
	ib_nexus_t	*ibn = &ib_nexus;
	int		prev_state;
	ulong		flags;
	int		rc;


	/*
	 * locate this connection struct on the active connection list to verify
	 * the need to destroy.
	 */
	if (find_connection(ibc))
		return;
	
	if (BAD_MAGIC(ibc)) {
		CWARN("BAD ibc(%p) MAGIC?\n",ibc);
		return;
	}

	/*
	 * save current connection state & advertise this connection to be on
	 * it's way to destruction (death row).
	 */
	write_lock_irqsave(&ibc->conn_lock,flags);
	prev_state = IB_GET_CONN_STATE(ibc);
	IB_SET_CONN_STATE(ibc,IBA_DISCONNECTING);
	write_unlock_irqrestore(&ibc->conn_lock,flags);

	if (prev_state == IBA_DISCONNECTING) {
		dprintk(DBG_DCONN,("%s%s conn %p already on death-row! exit.\n",
			WHOAMI,ibc));
		return;
	}
	dprintk(DBG_DCONN,("%s%s conn(%p)\n",WHOAMI,ibc));

	/* disconnect ALWAYS as this transitions QPs to the error state */
	(void) rdma_disconnect(ibc->cm_id);
	if (prev_state != IBA_CONNECTING)
		atomic_dec(&ibn->num_connected); // one less connection

	dprintk(DBG_DCONN,("%s%s ICS connected to %d hosts\n",WHOAMI,
			atomic_read(&ibn->num_connected)));

	/*
	 * remove from connection list.
	 */
	write_lock(&ibn->conn_lock);

	if (ibc->conn_list.next == LIST_POISON1 ||
	    ibc->conn_list.prev == LIST_POISON2) {
		printk("%s ibc(%p) not on active list? skip deletion\n",
			__func__,ibc);
		return;
	}
	else
		list_del(&ibc->conn_list);

	write_unlock(&ibn->conn_lock);

	// destroy the QP (Queue Pair)
	if (ibc->cm_id->qp) {
		rdma_destroy_qp(ibc->cm_id);
		dprintk(DBG_DCONN_V,("%s conn(%p,cm_id %p) QP destroy done.\n",
				__func__,ibc,ibc->cm_id));
	}

	/*
	 * wait for at least 10 seconds for the ibc->rx_posted count to drop
	 * to zero. 'rx_posted' is incremented for each posted receive buffer
	 * and decremented when processing a CQ recv entry(failed or not).
	 */
	dprintk(DBG_DCONN,
		("%s%s RX posted(%d)\n",WHOAMI,atomic_read(&ibc->rx_posted)));

	if (atomic_read(&ibc->rx_posted) > 0) {
		dprintk(DBG_DCONN,
			("%s%s waiting to drain (%d) posted rx buffers\n",
				WHOAMI, atomic_read(&ibc->rx_posted)));

		interruptible_sleep_on_timeout(&ibc->waitQ, (HZ*4));

		if ( (rc=atomic_read(&ibc->rx_posted)) > 0 )
			printk("%s%s (%d) posted rx buffers outstanding?\n",
								WHOAMI,rc);
	}

	// delete the CQ
	if (ibc->cq) {
		if ((rc=ib_destroy_cq(ibc->cq))) {
			printk("%s%s conn(%p) destroy CQ %d\n",WHOAMI,ibc,rc);
		}
		dprintk((DBG_DCONN),
			("%s%s conn(%p) destroy CQ done\n", WHOAMI,ibc));
		ibc->cq = NULL;
	}

	// release CM connection ID
	if (ibc->cm_id) {
		rdma_destroy_id(ibc->cm_id);
		ibc->cm_id = NULL;
	}

	// release receive buffers and rx descriptors
	if (ibc->recvq) {
		kib_rx_t *rxp = ibc->recvq;

		// release DMA resouces associated with RX buffers
		for (rc=0; rc < ibc->rbcnt; rc++,rxp++) {
			if (rxp->payload)
				dma_free_coherent(ibn->device->dma_device,
							IBA_MAX_BUF_SIZE,
							(void*)rxp->payload,
							rxp->dma_addr);
		}
		kfree(ibc->recvq);
		dprintk(DBG_DCONN, ("%s%s released %d RX buffers (%p)\n",
				WHOAMI,ibc->rbcnt,ibc->recvq));
		ibc->rbcnt = 0;
		ibc->recvq = NULL;
	}

	/* Connection transmit resources may be in one of three states as
	 * indicated by the tri-state value of conn->km.
	 *	1) buffers allocated, km > 1.
	 *	2) State of #1, and merged into the global TX pool (km == 1).
	 *	3) never allocated (km == 0).
	 *
	 * TX resources allocations are performed early in the connection
	 * sequence and not merged into the global TX pool until late in the
	 * connect sequence (after RTU is sent or after the RTU is received),
	 * Therefore if the Infiniband CM connection handshake was interrupted,
	 * the TX resources could be null, allocated but not yet merged or
	 * merged.
	 */
	dprintk(DBG_DCONN,("conn(%p) TX resources\n",ibc));
	if (ibc->km == TX_RESOURCES_MERGED) {
		// indicate there are TX resources to be reclaimed by a new
		// connection.
		atomic_inc(&ibn->unclaimed_tx_resources);
	}
	else if (ibc->km) {
		kib_tx_t *ktx;
		struct list_head *pos, *tmp;

		dprintk(DBG_DCONN,("%s%s release TX buffers\n", WHOAMI));
		/*
		 * release our allocated but not yet merged TX resources.
		 * blocking txds 1st.
		 */
		for (pos=ibc->txds.next; pos != &ibc->txds; pos = tmp ) {
			tmp = pos->next;
			ktx = list_entry(pos, struct _kib_txd, ktx_list); 
			list_del(&ktx->ktx_list);
			kmem_cache_free(ibn->txd_cache,(void*)ktx);
		}
		/* non-blocking txds */
		for (pos=ibc->nb_txds.next; pos != &ibc->nb_txds; pos=tmp) {
			tmp = pos->next;
			ktx = list_entry(pos, struct _kib_txd, ktx_list); 
			list_del(&ktx->ktx_list);
			kmem_cache_free(ibn->txd_cache,(void*)ktx);
		}
	}
	ibc->km = 0;

	dprintk(DBG_DCONN,("conn(%p) released memory\n",ibc));

	// release connection memory
	CLEAR_MAGIC(ibc);	// mark as no longer valid structure.
	kfree((void*)ibc);
	dprintk(DBG_DCONN,("%s Exit\n",__func__));
}


/*
* DESCRIPTION
*	This function is invoked by an linux work queue thread to destroy
*	a connection. During the CM (Connection Manager) handshake there are
*	times when an CM callback thread can not block as the thread is a CM
*	thread, which by blocking would prevent other CM calls from happening.
*	For instance, Destroy can invoke CM operations (disconnect) which would
*	need a CM thread; circular, no CM thread, blocked no forward progress.
*	Hence a work queue access thread is utilizaed to perform the connection
*	destroy operation.
* PARAMETERS
*    *workq_context
*	[in] conection struct pointer to destroy
* RETURN VALUE
*	VOID
* SIDE EFFECTS
*	Connection is shutdown and destroyed(resources released).
*/

static void
conan_the_destroyer( void *workq_context )
{
	ib_conn_t *kconn = (ib_conn_t*)workq_context;

	// connection to be nuked...
	if (BAD_MAGIC(kconn)) {
		printk("%s() invalid kconn %p? IB Connection already dead\n",
			__func__,kconn);
		return;
	}

	//dprintk(DBG_CONN,("%s%s conn %p on death-row\n",WHOAMI,kconn));
	ib_destroy_connection(kconn);
}


/*
* NAME
*	ib_async_destroy_conn -- queue an asynchronous connection destroy
* DESCRIPTION
*	The specified connection needs to be destroyed although the current
*	thread can not block or is a thread needed in the destruction process,
*	likely a CM thread. Hand the connection pointer to a work queue
*	thread to complete the destruction of the connection.
* PARAMETERS
*    *kconn
*	[in] conection struct pointer to destroy
* RETURN VALUE
*	VOID
* SIDE EFFECTS
*	Connection is queued to be shutdown and destroyed(resources released)
*	by a system work queue thread.
*/

void
ib_async_destroy_conn(ib_conn_t *kconn)
{
	INIT_WORK(&kconn->work, conan_the_destroyer, kconn);
	queue_work(kconn->ib_nexus->wq, &kconn->work);
	dprintk(DBG_CONN_V,("%s%s conn(%p) queued\n",WHOAMI,kconn));
}


/*
* DESCRIPTION
*	splice the temp connection transmit lists (blocking and non-block tx
*	descriptors) into the global transmit pool lists. Make sure the tmp
*	source lists in the connection struct are reinitialized as empty so
*	no one is confused.
*	If previous connections have left resources in the global TX pool,
*	the kconn->km will reflect this, hence no new resources are added
*	to the global pool. See management of 'ibd->unclaimed_tx_resources'.
* PARAMETERS
*    kconn
*	[in] Infiniband connection struct pointer
* RETURN VALUE
*	VOID
* SEE ALSO
*	cm_conn_RTU
*/

static void
pool_tx_resources(ib_conn_t *kconn)
{
	unsigned long	flags;
	ib_nexus_t	*ibd = kconn->ib_nexus;	// NAL data

	if (kconn->km == 0) {
		CWARN("no TX resouces to pool? num_connected %d\n",
					atomic_read(&ibd->num_connected));
		return;
	}

	if (kconn->km == TX_RESOURCES_MERGED) {
		dprintk(DBG_CONN,("%s%s TX resouces reclaimed from previous "
				"connection\n", WHOAMI));
		return;
	}

	// splice the tmp tx lists into the global transmit resource lists.
	// make sure the tmp source lists in the connection struct are
	// reinitialized as empty.

	spin_lock_irqsave (&ibd->idletxd_lock, flags);

	list_splice( &kconn->txds, &ibd->idletxds );
	INIT_LIST_HEAD(&kconn->txds);
	ibd->txds_free += IBA_SEND_WR;	// see alloc_tx_resources

	list_splice( &kconn->nb_txds, &ibd->nblk_idletxds );
	INIT_LIST_HEAD(&kconn->nb_txds);
	ibd->nblk_txds_free += MAX_NB_WR;

	kconn->km = TX_RESOURCES_MERGED;

	spin_unlock_irqrestore (&ibd->idletxd_lock, flags);
}


/*
* DESCRIPTION
*	Allocate transmit (TX) descriptors (kib_tx_t) and the IB registered
*	TX buffer memory used for sends. TX resources are merged from the
*	connection struct into the global TX pools once the connection is
*	established. Here the resources are allocated and recorded for later
*	migration into the global TX pools.
*	There are two types of global TX resources in separate lists hung off
*	ib_nexus data structure: blocking TX descriptors and non-blocking
*	descriptors. A descriptor contains an associated TX buffer.
*	The non-blocking pool is for those send operations which can not
*	tolerate blocking.
*	In order to speed connection setup times, when a connection is destroyed
*	it's TX resources remain in the global pool. When the next connection
*	is established, instead of allocating new TX resources, a check is made
*	to see if there are some reclaimable TX resources in the global pool
*	from a previous connection which had been destroyed. If so, then skip
*	the TX resource allocation for the new connection.
* PARAMETERS
*    kconn
*	[in] pointer to a connection struct.
* RETURN VALUE
*	SUCCESS, otherwise negative errno
* SIDE EFFECTS
*	Fill in various kconn->tx fields with allocated tx information.
*/

static int
alloc_tx_resources(ib_conn_t *kconn)
{
	ib_nexus_t *ibd=kconn->ib_nexus;
	kib_tx_t *ktx;
	int i;
	int odr = get_order(IBA_MAX_BUF_SIZE);
	struct list_head *lh;

	/*
	 * Reclaimable TX resources available?
	 */
	if (atomic_read(&ibd->unclaimed_tx_resources) > 0) {
		unsigned long flags;

		dprintk(DBG_CONN,("%s%s reclaiming TX resources\n",WHOAMI));
		spin_lock_irqsave(&ibd->idletxd_lock,flags);

		// claim unused TX resources.
		atomic_dec(&ibd->unclaimed_tx_resources);

		/*
		 * Identify we are reclaiming previously allocated/merged global
		 * TX resources by falsely claiming we have merged our own
		 * TX resources.
		 */
		kconn->km = TX_RESOURCES_MERGED;
		spin_unlock_irqrestore(&ibd->idletxd_lock, flags);

		return SUCCESS;
	}

	// construct & initialize the list of TX descriptors, blk & non-blk

	for (i=0; i < IBA_SEND_WR+MAX_NB_WR; i++) {
		ktx = kmem_cache_alloc(ibd->txd_cache,GFP_KERNEL);
		if (!ktx) {
			CWARN("Unable to kmem_cache_alloc() TX descriptor");
			return -ENOMEM;
		}
		memset((void*)ktx,0,sizeof(*ktx));
		ktx->payload = (char*)__get_free_pages(GFP_KERNEL,odr);
		if (!ktx->payload) {
			CWARN("get_free_pages(ord %d) for TX payload failed",
				odr);
			return -ENOMEM;
		}
		ktx->lkey = ibd->kmr->lkey;	// local memory IB access Key
		// ktx->dma_addr set when ktx acquired. 

		if (i < IBA_SEND_WR) {
			ktx->non_blk = FALSE;
			lh = &kconn->txds;
		}
		else { // identify this element at non-block'ing
			ktx->non_blk = TRUE;
			lh = &kconn->nb_txds;
		}
		list_add_tail(&ktx->ktx_list, lh);
	}
	dprintk(DBG_CONN_V,
		("%s%s %d TX desc configured\n",WHOAMI,IBA_SEND_WR+MAX_NB_WR));

	/* indicate TX resources are allocated.
	 * TX resources are recycled when the connection is destroyed and
	 * reclaimed when the next connection is created.
	 */
	kconn->km = MAX_NB_WR + IBA_SEND_WR;

	/*
	 * Tx resources are hung off the conn struct at this juncture.
	 * Merge the TX resources into the global TX pool later.
	 */
	return SUCCESS;
}


/*
* DESCRIPTION
*	Enable this connection to receive IB data. Allocate an IB Queue-pair &
*	IB completion queue (CQ), allocate and IB register receive buffer
*	memory. Depending on rx_post, possibly post the registered receive
*	buffers.
*	If previous connections have closed, leaving TX buffers allocated, then
*	this connection can reclaim the TX resources and skip the allocation
*	phase. Otherwise, new TX resouces are allocated and IB registered but
*	not yet merged into the global TX pool. TX resource merge occurs in the
*	final stages of connection establishment to allow for TX resource
*	destruction for those connections which did not become fully
*	established.
* PARAMETERS
*    *kconn
*	[in] active connection structure.
*    rx_post
*	[in] TRUE - IB post receive buffers now, FALSE - do NOT post RX buffers.
* RETURN VALUE
*    0 == SUCCESS, otherwise a negative errno value.
*    -ENOMEM - unable to create a CQ (Completion Queue) 
*    -EPERM - unable to IB register recv descriptors & buffer memory
*    -ENODEV - unable to allocate an IB Queue-pair
*    -EIO - unable to IB post recveive buffers
*    -ENOSPC - unable to allocate transmit resources
* SIDE EFFECTS
*	Kernel memory allocated and IB registered, IB QP & CQ created.
*	If an error occurs, all resources up to the point of error are released.
********/

static int
alloc_conn_resources(ib_conn_t *kconn, int rx_post)
{
	ib_nexus_t *ibn = kconn->ib_nexus;
	u32 bsize;
	int rc, i;
	kib_rx_t *rxp;
	struct ib_qp_init_attr qp_attr;

	/*
	 * Create a per connection CQ (Completion Queue) which will receive both
	 * send and receive work-request (wr) completion events.
	 */
	kconn->cq = ib_create_cq(kconn->cm_id->device,
				 ib_completion_cb, 	// CQ callback
				 _async_ib_event_cb,
				 (void*)kconn,		// context
				 IBA_CQ_DEPTH);
	if (IS_ERR(kconn->cq)) {
		rc = PTR_ERR(kconn->cq);
		CWARN("Can't allocate CQ 'rc %d'\n",rc);
		return -ENOMEM;
	}

	/* arm the CQ to request completion notification on the next completion
	 * queue entry.
	 */
	rc = ib_req_notify_cq(kconn->cq, IB_CQ_NEXT_COMP);
	if (rc) {
		CERROR("unable to re-arm CQ %d\n",rc);
		(void) ib_destroy_cq(kconn->cq);
		kconn->cq = NULL;
		return (-ENOMEM);
	}
	dprintk(DBG_CONN_V,("%s%s CQ created & Armed\n",WHOAMI));

	/* create a Queue-Pair for this connection */

        memset(&qp_attr, 0, sizeof qp_attr);
        qp_attr.event_handler = _async_ib_event_cb;
        qp_attr.qp_context = (void*)kconn;
	qp_attr.send_cq = kconn->cq;
	qp_attr.recv_cq = kconn->cq;
	qp_attr.srq = NULL;	// no shared recv queue
        qp_attr.cap.max_send_wr = IBA_SEND_WR+1;
        qp_attr.cap.max_recv_wr = IBA_RECV_WR+1;
        qp_attr.cap.max_send_sge = MAX_SGL_ENTRIES;
        qp_attr.cap.max_recv_sge = 1;
        qp_attr.sq_sig_type = IB_SIGNAL_REQ_WR;
        qp_attr.qp_type = IB_QPT_RC; // reliable connection

	rc = rdma_create_qp(kconn->cm_id,kconn->ib_nexus->pd, &qp_attr);
	if (rc) {
		CWARN("Can't create IB Queue-pair %d\n", rc);
		(void) ib_destroy_cq(kconn->cq);
		kconn->cq = NULL;
		return rc;
	}
	dprintk(DBG_CONN_V,("%s%s QP created\n", WHOAMI));

	/*
	 * create per connection receive buffers and RX descriptors; both which
	 * must live in IB registered memory. These RX descriptors contain
	 * the memory where ICS message payload is received.
	 */
	if (kconn->rbcnt == 0) {
		kconn->rbcnt = IBA_RECV_WR;// # of RX buffers allocated
	}
	bsize = sizeof(kib_rx_t) * kconn->rbcnt;

	kconn->recvq = kzmalloc(bsize,GFP_KERNEL);
	if (!kconn->recvq) {
               	CWARN("IB recv descriptor allocate failed(%d)\n", bsize);
		(void) ib_destroy_cq(kconn->cq);
		kconn->cq = NULL;
		kconn->rbcnt = 0;
               	return (-ENOMEM);
	}
	dprintk(DBG_CONN_V,("%s%s %d RX buffers alloc'ed %p\n",WHOAMI,
        					kconn->rbcnt,kconn->recvq));

	/* Initialize recv descriptors for this connection. */

	for (rxp=kconn->recvq,i=0; i < kconn->rbcnt; i++,rxp++) {
		rxp->conn = kconn;
		rxp->nob = 0;
		rxp->class = RXD_CLASS_CONN;	// per connection allocation
		rxp->payload = dma_alloc_coherent(
						ibn->device->dma_device,
						IBA_MAX_BUF_SIZE,
						&rxp->dma_addr,
						GFP_KERNEL);
		if (!rxp->payload) {
			CERROR("RX payload alloc failed(sz %d)\n",
					IBA_MAX_BUF_SIZE/1024);
			kfree((void*)kconn->recvq);
			kconn->recvq = NULL;
			kconn->rbcnt = 0;
			return (-ENOMEM);
		}
		// IB post rx buffer now or later?
		if (rx_post) {
			if (kib_post_recv_wr(rxp)) {
				// since we may have posted some, let higher
				// level reclaim resources via rx completion
				// callback after qp destroyed.
				return (-EIO);
			}
		}
	}

	/*
	 * Allocate transmit buffers along with blocking plus non-blocking
	 * transmit descriptors and Memory descriptor keys. Do not merge TX
	 * resources into global TX pool, later.
	 */
	if (alloc_tx_resources(kconn)) {
                CWARN("Unable to allocate TX resources\n");
		return (-ENOSPC);
	}
	return SUCCESS;
}


/*
* NAME
*	ib_post_rx_buffer --  Infinband post a list of receive buffers
* DESCRIPTION
*	Routine called during IB client connection setup to post all allocated
*	and IB registered buffers; a one time post of all rx buffers,
*	afterwards RX buffers are reposted after a receive operation.
* PARAMETERS
*    kconn
*       [in] IB connection struct pointer. Implicit kconn->rbcnt == # of
*		allocated RX descriptors.
* RETURN VALUE
*    SUCCESS, otherwise a negative errno.
*	-EINVAL - missing list of RX descriptors?
*	-ENOMEM - unable to alloc memory for work-request descriptors
*	-EIO - error during IB work-request posting.
*	buffers have been successfully posted.
*/

static int
ib_post_rx_buffers(ib_conn_t *kconn)
{
	int		i,rc;
	kib_rx_t	*rxd;
	int		wr_size;
	void		*wr_alloc;
	struct ib_sge	*dsp;
	struct ib_recv_wr *wr, *recv_failure;

	// point at 1st RX descriptor in a vector of RXDs (recv descriptors).

	if (!kconn->recvq) {
		CWARN("NULL conn->recvq, missing RX descriptors?\n");
		return (-EINVAL);
	}

	/*
	 * Allocate and then link all initialized receive descriptor
	 * work-request elements together in preparation for a single RX post
	 * operation. The rx descriptors were initialize themselves in
	 * alloc_conn_res().
	 */

	// allocate dynamic memory for IB work-requests & data segments

	wr_size = (kconn->rbcnt * sizeof(struct ib_recv_wr)) +
		(kconn->rbcnt * (sizeof(struct ib_sge) * MAX_SGL_ENTRIES));

	wr_alloc = kzmalloc(wr_size, GFP_KERNEL);
	if (!wr_alloc) {
		// mem allocation failed?
		CERROR("unable to alloc IB RX work-req memory.\n");
		return -ENOMEM;
	}
	dprintk(DBG_CONN_V,
		("%s%s alloc()'ed %d RX work-requests+DataSegDesc conn %p\n",
						WHOAMI,kconn->rbcnt,kconn));
	
	wr = (struct ib_recv_wr*)wr_alloc;	// start of recv work-requests
	dsp = (struct ib_sge*)(wr+kconn->rbcnt); // skip to data seg descriptors

	// build list of recv work-request from the pre-initialized RX
	// descriptors.

	for (rxd=kconn->recvq,i=0; i < kconn->rbcnt; i++,rxd++,dsp++,wr++) {
		rxd->nob = 0;	// no bytes received yet
		if (rxd->conn != kconn) {
			rxd->conn = kconn;
			CWARN("bad conn ptr, reset.\n");
		}

		wr->wr_id = (u64)(ulong)rxd;	// kib_rx_t* pointer
		wr->num_sge = 1;	// # of *ds elements for this wr
		wr->sg_list = dsp;	// this wr's local data segment

		// init ds[0] to point at the payload bay in the RX descriptor.
		// RX descriptors live in IBA registered memory as do
		// the RX descriptors.
		dsp->addr = (u64)(ulong)rxd->dma_addr; // ICS payload buf
		dsp->length = IBA_MAX_BUF_SIZE;
		dsp->lkey = kconn->ib_nexus->kmr->lkey;

		wr->next = (wr+1);		// point @ next wr
	}
	// backup one work-request descriptor and terminate the list.
	(wr-1)->next = NULL;

	// list of recv work-requests to be posted.
	rc = ib_post_recv(kconn->cm_id->qp,
			  (struct ib_recv_wr*)wr_alloc,
			  &recv_failure);

	// release dynamic recv work-request memory as it's contents are
	// copied into IBAL generated IB WQE (Work Queue Entries).
	kfree(wr_alloc);

        if(rc != SUCCESS) {
                CWARN("Unable to post recv Work-requests(%d) conn %p\n",
			rc,kconn);
                return rc;
        }
	atomic_set(&kconn->rx_posted,kconn->rbcnt);
	kconn->min_rx_posted = kconn->rbcnt;

	dprintk(DBG_CONN_V,("%s%s IB posted %d RX work-requests conn %p\n",
			WHOAMI,atomic_read(&kconn->rx_posted),kconn));

	return SUCCESS;
}

/**
* NAME
*	ksvr_grow_rx_pool -- Add new descriptors to the global RX free pool
* DESCRIPTION
*	Grow the RX global pool of receive descriptors and receive buffers by
*	the specified number of descriptors. One recv buffer per recv
*	descriptor.
* PARAMETERS
*    *ibn
*	[in] Infiniband Nexus data
*    elements
*	[in] Grow the global RX receive descriptor pool by this number of
*	     elements.
* RETURN VALUE
*	void
**/

void
ksvr_grow_rx_pool(ib_nexus_t *ibn, u32 elements)
{
	kib_rx_t *rxd;
	int i;
	ulong flags;
	struct list_head qh;

	// serialize growth

	if (!spin_trylock(&ibn->rx_pool_grow_lock))
		return;

	// allocate elements: RXD

	INIT_LIST_HEAD(&qh);	// tmp list of rx descriptor elements

	for (i=0; i < elements; i++) {
		if (!(rxd=kzmalloc(sizeof(kib_rx_t),GFP_KERNEL))) {
			CERROR("RXD allocate failed?\n");
			spin_unlock(&ibn->rx_pool_grow_lock);
			return;
		}
		rxd->class = RXD_CLASS_CLONE;	// ID where this rxd came from
		rxd->payload = dma_alloc_coherent(
						ibn->device->dma_device,
						IBA_MAX_BUF_SIZE,
						&rxd->dma_addr,
						GFP_KERNEL);
		if (!rxd->payload) {
			CERROR("RX payload alloc failed(sz %dK)\n",
						IBA_MAX_BUF_SIZE/1024);
			kfree((void*)rxd);
			spin_unlock(&ibn->rx_pool_grow_lock);
			return;
		}
		list_add_tail(&rxd->link, &qh); // build tmp list
	}

	// merge our new rxd list into the global rx pool list

	spin_lock_irqsave( &ibn->rx_pool_lock, flags );
	list_splice( &qh, &ibn->rx_pool );
	ibn->rx_pool_free += elements;
	spin_unlock_irqrestore( &ibn->rx_pool_lock, flags );

	spin_unlock(&ibn->rx_pool_grow_lock);

	dprintk(DBG_STARTUP,("%s%s Global RX pool increased by %d elements\n",
						WHOAMI,elements));
}

/*
 * Set local debug flags; see 'proc_fs.c'
 *
 * echo debug xyz > /proc/sys/ics_ib
 */

typedef struct {
	char	*name;
	u8	name_len;
	u8	add;
	u32 	flags;

} dbg_opts_t;

dbg_opts_t	dbg_opts[] =
{
	{"off",3,0,DBG_NONE},
	{"all",3,1,DBG_ALL},
	{"intr",4,1,DBG_INTR},
	{"conn",4,1,DBG_CONN},
	{"rdma",4,1,DBG_RDMA},
	{"sendv",5,1,DBG_SEND_V},	// verbose
	{"send",4,1,DBG_SEND},
	{"recv",4,1,DBG_RECV},
	{"recvv",5,1,DBG_RECV_V},
	{"shutdown",8,1,DBG_SHUTDOWN},
	{"stats",5,1,DBG_STATS},
	{"time_send",9,1,DBG_TIME_SEND},
	{"time_send_pages",15,1,DBG_TIME_SEND_PAGES},
	{"time_recv",9,1,DBG_TIME_RECV},
	{NULL}
};

void
ics_ib_set_debug(char *cmd)
{
	char		*cp;
	u32		new=0, base;
	int		add=1;	// OR/enable the new bit.
	dbg_opts_t	*opts;

	static char	*usage = "%s debug what? {+/-}{off/all/"
				"conn/rdma/send/sendv/recv/recvv/map/intr/"
				"shutdown/stats/time_send/time_send_pages}"
				" 0xBits}\n";

	// EOS?
	if ( !cmd || *cmd == '\0' ) {
		printk("%s debug level 0x%x\n",ME,ics_ib_debug);
		printk(usage,ME);
		return;
	}

	for (add=1,base=ics_ib_debug,cp=cmd; *cp; )
	{
		// skip possible leading whitespace
		for (; *cp && isspace(*cp); ++cp) ;

		// EOS? (End-Of-String)
		if ( *cp == '\0' ) continue;

		// OR in new bits or replace entire level?
		// '+' or '-' are optional, if not present - default to '+'
		if ( *cp == '+' ) {
			cp++;
			continue;	// recheck for EOF
		}
		else if ( *cp == '-' ) {
			add = -1;
			cp++;
			continue;	// recheck for EOF
		}

		// debug options

		for(opts=dbg_opts; opts->name; opts++) {
			if ( strncmp(cp,opts->name,opts->name_len) == 0 ) {
				new = opts->flags;
				// replace - do not OR in bits if zero.
				if ( opts->add == 0 ) add = 0;
				cp += opts->name_len;// advance to next token
				break;
			}
		}

		if ( opts->name ) {
			if ( add == 0 )
				base = new;
			else if ( add < 0 )
				base &= (~new);
			else
				base |= new;
			add = 1;	// default is to OR in the new bit(s)
			continue;
		}
		else if ( isdigit(*cp) ) {
			// numeric gets 1 shot replacement.
			new = (u32)simple_strtoul(cp,0,16);
			add = 0;	// replace old with new dbg bits.
			printk("%s dbg level [old 0x%x new 0x%x]\n",
							ME,ics_ib_debug,new);
			ics_ib_debug = new;
			return;
		}
		else {
printk("%s%s unknown cmd: cp '%s' len %d\n",WHOAMI,cp,strlen(cp));
			printk(usage,ME);
			return;
		}
	}
	printk("%s dbg level [old 0x%x new 0x%x]\n",ME,ics_ib_debug,base);
	ics_ib_debug = base;
}



#if 0

/*
 * find the trade off point where copying data is not as fast/timely as IB
 * registering data. An attempt to determine the correct payload size @ which
 * an RDMA should be used instead of copying the payload to a TX buffer.
 * Invoked as 'echo threshold >/proc/net/ibanal', see 'proc_fs.c'.
 */
#define	MAX_RANGE 64	// integral number of VM pages

void
ib_find_copy_threshold(char *cmd)
{
	char		*cp, *base_src, *base_dst, *base_map;
	int		j,k,rc,range_sz;
	u32		nob;
	ulong		pgo,emt,ect;
	struct iovec	iov;
	kib_mdkey_t	mdkey;
	u64		ts, map_et, copy_et;

	/*
	 * allocated 3 ranges of kernel pages:
	 * 	1st range is IB registered/unregistered.
	 *	2nd range is the copy source
	 *	3rd range is the copy destination.
	 */

	range_sz = (MAX_RANGE * PAGE_SIZE);
	j = range_sz * 3;	// total # of bytes in 3 ranges
	pgo = get_order((ulong)j);

	//printk("%s%s alloc %d bytes, order %lu pages\n",WHOAMI,j,pgo);

	base_map = (char*)__get_free_pages(__GFP_DMA,pgo);
	if ( !base_map )
	{
		printk("%s%s mem alloc failed(%d bytes) pages %ld?\n",
						WHOAMI,j,(j/PAGE_SIZE));
		return;
	}
	memset((void*)base_map,0,j);
	cp = base_map;
	base_src = cp + range_sz;
	base_dst = base_src + range_sz;

	//printk("%s%s range sz %d base_map %p base_src %p base_dst %p\n",
	//			WHOAMI, range_sz,base_map,base_src,base_dst);

	for(j=1; j <= MAX_RANGE; j++) {

		copy_et = map_et = 0;

		for(k=0; k < 3; k++) {
			iov.iov_base = (void*)base_map;
			iov.iov_len = nob = (j * PAGE_SIZE);

			ts = us_time_stamp();
			rc = kib_map_iov( nob,
	 				  1,
	 				  &iov,
					  0,
	 				  &mdkey,
	 				  (u64*)base_dst );
			if ( rc ) {
				printk("%s%s map range of %d pages failed?\n",
								WHOAMI,j);
				free_pages((ulong)base_map,pgo);
				return;
			}
			(void) kib_unmap( (void*)mdkey.h_mr );
		
			map_et += us_time_stamp() - ts;

			ts = us_time_stamp();
			memcpy((void*)base_dst, (void*)base_src, (j*PAGE_SIZE));
			copy_et += us_time_stamp() - ts;
		}
		emt = (ulong)map_et / 3UL;
		ect = (ulong)copy_et / 3UL;
		printk("pages(%d) map/unmap %lu us, copy %lu us\n",j,emt,ect);
	}
	free_pages((ulong)base_map,pgo);

} 
#endif

static void
rdma_cm_wakeup(struct rdma_cm_id *cm_id, struct rdma_cm_event *event)
{
	ib_conn_t *ibc = cm_id->context;
	ibc->error = event->status;
	wake_up_interruptible(&ibc->waitQ);
}

/*
 * Handle RDMA CM (Conection Manager) Events
 */

static int
CM_event(struct rdma_cm_id *cm_id, struct rdma_cm_event *event)
{
	ib_conn_t *ibc = cm_id->context;
	int rc;

	switch (event->event) {

	  case RDMA_CM_EVENT_ADDR_RESOLVED:
		dprintk(DBG_CONN_V,("%s RDMA_CM_EVENT_ADDR_RESOLVED\n",__func__));
		rc = rdma_resolve_route(ibc->cm_id,10000);
		if (rc) {
			event->status = rc;
			rdma_cm_wakeup(cm_id, event);
		}
		/* route resolution success will do wakeup on success */
		break;

    	  case RDMA_CM_EVENT_ROUTE_RESOLVED:
		dprintk(DBG_CONN_V,("%s RDMA_CM_EVENT_ROUTE_RESOLVED\n",__func__));
		rdma_cm_wakeup(cm_id, event);
		break;

    	  case RDMA_CM_EVENT_CONNECT_REQUEST:
		dprintk(DBG_CONN_V,("%s RDMA_CM_EVENT_CONNECT_REQUEST\n",__func__));
		rdma_passive_connect(cm_id,event);
		break;

    	  case RDMA_CM_EVENT_ESTABLISHED:
		dprintk(DBG_CONN_V,("%s RDMA_CM_EVENT_ESTABLISHED\n",__func__));
		ibc = cm_id->context;
		if (BAD_MAGIC(ibc)) {
			CERROR("RDMA_CM_EVENT_ESTABLISHED: Bad ibc MAGIC?\n");
			cm_reject(cm_id,"Invalid cm_id context?");
			break;
		}
		SSI_ASSERT(ibc->cm_id == cm_id);
		if (waitqueue_active(&ibc->waitQ))
			finish_active_connect(ibc,cm_id,event);
		else
			finish_passive_connect(ibc,cm_id,event);
		break;

    	  case RDMA_CM_EVENT_UNREACHABLE:
		printk("%s() UNREACHABLE - status %d\n",__func__,event->status);
		ibc = cm_id->context;
		if (BAD_MAGIC(ibc)) {
			CERROR("RDMA_CM_EVENT_UNREACHABLE: Bad ibc MAGIC?\n");
			break;
		}
		if (IB_GET_CONN_STATE(ibc) == IBA_CONNECTED) {
			IB_SET_CONN_STATE(ibc,IBA_ERROR);
        		Dprintk(DBG_CONN,("%s conn(%p) queued node(%d) down\n",
					__func__,ibc,ibc->r_hid.node));
        		INIT_WORK(&ibc->work,dead_node,(void*)ibc->r_hid.node);
        		queue_work(ibc->ib_nexus->wq, &ibc->work);
		}
		else
			rdma_cm_wakeup(cm_id, event);
		break;

    	  case RDMA_CM_EVENT_CONNECT_ERROR:
		printk("%s() CONNECT ERROR\n",__func__);
		rdma_cm_wakeup(cm_id, event);
		break;

    	  case RDMA_CM_EVENT_ADDR_ERROR:
    	  case RDMA_CM_EVENT_ROUTE_ERROR:
		dprintk(DBG_CONN,
			("%s: RDMA_ADDR/ROUTE event %d, error %d\n",__func__,
					event->event, event->status));
		rdma_cm_wakeup(cm_id, event);
		break;

    	  case RDMA_CM_EVENT_REJECTED:
		printk("%s() REJECTED\n",__func__);
		rdma_cm_reject_handler(cm_id, event);
		break;

    	  case RDMA_CM_EVENT_DISCONNECTED:
		dprintk(DBG_CONN_V,("%s() DISCONNECTED\n",__func__));
		rdma_cm_disconnect(cm_id,event);
		break;

    	  case RDMA_CM_EVENT_DEVICE_REMOVAL:
		printk("%s() Device Removal\n",__func__);
		break;

	  default:
		printk("%s RDMA CM event %d not handled?\n", __func__,
								event->event);
		break;
	}
	return 0;	// if non-zero return then cm_id is destroyed.
}


/*
* DESCRIPTION -- post a Connection Manager listen request
*	Asynchronously post an RDMA Connection Manager (CM) reliable
*	connection listen request.
* PARAMETERS
*    *nd
*	[in] pointer to IB NAL data
*    svc_port
*	[in] service port on which to listen
* RETURN VALUE
*	0 - SUCCESS, otherwise negative errno value.
*	-EIO - unable to post IB listen
* SIDE EFFECTS
*	Upon success, a persistent IB listen is in force until cancelled.
*	ib_listen_t(listen-handle & port) are set upon a successful posted
*	listen.
* NOTES
*	When a connection request from a Lustre client arrives, our callback
*	routine will launch a thread to process the connection handshake.
*/

int
ib_listen_for_connection(ib_listen_t *ld, int svc_port)
{
	int rc;
	struct sockaddr_in saddr;
	ib_nexus_t *ibn = &ib_nexus;

	ld->port = svc_port;	// port we are listening on
	ld->listen_id = rdma_create_id(CM_event,(void*)ld,RDMA_PS_TCP);
	if (IS_ERR(ld->listen_id)) {
		rc = PTR_ERR(ld->listen_id);
                printk("%s: ERR - failed to create CM id: %d?\n", __func__, rc);
		ld->port = 0;
                return -EPERM;
	}

	memset((void*)&saddr,0,sizeof(saddr));
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(svc_port);
	saddr.sin_addr.s_addr = ibn->my_hid.nid;	// network-byte-order

	rc = rdma_bind_addr(ld->listen_id,(struct sockaddr*)&saddr);
	if (rc) {
                printk("%s: ERR - failed to rdma_bind? port %d nid %s\n",
			 __func__,svc_port,nid_str(ibn->my_hid.nid));
		rdma_destroy_id(ld->listen_id);
		ld->port = 0;
                return -EADDRNOTAVAIL;
	}

        rc = rdma_listen(ld->listen_id,15);
        if (rc) {
                printk("%s: failure(%d) to post cm listen(port %d)\n",
				__func__,rc,svc_port);
		rdma_destroy_id(ld->listen_id);
		ld->port = 0;
		rc = -EACCES;
        }
	dprintk(DBG_CONN_V,("%s on port %d @ %s\n", __func__,
				svc_port,nid_str(saddr.sin_addr.s_addr)));
	return rc;
}


/*
* DESCRIPTION
*	Client side routine to connect to an ICS server at the specified
*	node & service port.
* INPUTS
*    *kconn	partially initialized IB connection struct.
*    nid	remote host IPv4 address we are attempting to connect with
*    port	Service port number on which a remote ICS server is listening.
* RETURN VALUE
*	0 - SUCCESS, otherwise negative errno value.
*	-EIO - unable to post IB listen
*/

static int
connect_to_server(ib_conn_t *kconn, u32 nid, u32 port)
{
	int			rc,retry;
	ib_nexus_t		*nd = kconn->ib_nexus;
	struct rdma_conn_param	cm_req;
	struct sockaddr_in	src,dst;

	SSI_ASSERT(kconn);

	kconn->r_nid = nid;
	kconn->svc_port = port;

	kconn->cm_id = rdma_create_id(CM_event, (void*)kconn, RDMA_PS_TCP);
	if (IS_ERR(kconn->cm_id)) {
		rc = PTR_ERR(kconn->cm_id);
                printk("%s: ERR - failed to create CM id: %d?\n",__func__,rc);
                return -EPERM;
	}
	memset((void*)&src,0,sizeof src);
	src.sin_family = AF_INET;
	//src.sin_port = 0;
	//src.sin_addr.s_addr = nd->my_hid.nid;	// network-byte-order

	memset((void*)&dst,0,sizeof dst);
	dst.sin_family = AF_INET;
	dst.sin_port = htons(kconn->svc_port);
	dst.sin_addr.s_addr = kconn->r_nid;	// network-byte-order

	retry = 15;
	do {
		kconn->error = NO_STATUS;
		rc = rdma_resolve_addr(kconn->cm_id,
                                       (struct sockaddr*)&src,
                                       (struct sockaddr*)&dst,
                                       1000); /* 1 seconds */
		if (rc) {
			if (rc == -ETIMEDOUT) {
				interruptible_sleep_on_timeout(&kconn->waitQ,
								(1*HZ));
				kconn->error = rc;
				continue;
			}
			dprintk(DBG_CONN,
				("%s rdma_resolve_addr() [%s].%d err %d\n",
				__func__, nid_str(dst.sin_addr.s_addr),
				dst.sin_port,rc));
			return rc;
		}
		wait_event_interruptible(kconn->waitQ,
					(kconn->error != NO_STATUS));
		if (kconn->error) {
			if (kconn->error == -ETIMEDOUT) {
				interruptible_sleep_on_timeout(&kconn->waitQ,
								(2*HZ));
				continue;
			}
			CWARN("Address resolution failure [%s].%d (err %d)\n",
				nid_str(dst.sin_addr.s_addr),dst.sin_port,
				kconn->error);
			return kconn->error;
		}
	} while (kconn->error != 0 && --retry > 0);

	if (kconn->error != 0 && retry <= 0) {
		CWARN("unable to resolve address to node [%s].%d (err %d)\n",
			nid_str(dst.sin_addr.s_addr),dst.sin_port,kconn->error);
		return -ETIMEDOUT;
	}
	dprintk(DBG_CONN_V,("%s%s() route resolved\n",WHOAMI));

	/*
	 * allocate IB resources, delay posting RX buffers until final phase
	 * of the connect handshake (when we know we have a connection, just
	 * before sending the RTU (ready-to-use) message).
	 */
	if ((rc=alloc_conn_resources(kconn,FALSE))) {
		printk("%s%s unable to alloc connection resources?\n",WHOAMI);
		return rc;
	}

	/* connect to remote end point - setup CM connect request */
	memset((void*)&cm_req, 0, sizeof(cm_req));

	/*
	 * Send the server my name. Setup IB conn request private data as
	 * 4 bytes of my local IP address(nid), followed by 1 byte of hostname
	 * string length, followed by 'n' bytes of hostname. Hostname length
	 * includes the null termination byte.
	 * Private data is for informational/debug purposes only.
	 */
	cm_req.private_data = (void*)&nd->my_hid;
	cm_req.private_data_len=(u8)(nd->my_hid.hostname_len+HOST_ID_BASE_SIZE);

	cm_req.responder_resources = MAX_PENDING_RDMA;
	cm_req.initiator_depth = MAX_PENDING_RDMA;
	cm_req.flow_control = 1;
	cm_req.retry_count = 2;
	cm_req.rnr_retry_count = 4;

	kconn->error = NO_STATUS;

	IB_SET_CONN_STATE(kconn,IBA_CONNECTING);

	/* fire off the asynchronous connection request. */

	dprintk(DBG_CONN,("%s%s posting IB connection REQuest to [%s].%d\n",
				WHOAMI,nid_str(kconn->r_nid),kconn->svc_port));

	rc = rdma_connect(kconn->cm_id,&cm_req);
	if (rc) {
		CWARN("IB rdma_connect request failed(%d)?\n", rc);
		return rc;
	}
	if (kconn->error == NO_STATUS) {
		/* wait for connection to become established. wait (10 seconds)
		 * for callback to wake up this thread.
		 */
		dprintk(DBG_CONN_V,("%s%s waiting on connect response\n",WHOAMI));
		rc = interruptible_sleep_on_timeout(&kconn->waitQ, (HZ*60));
		if (rc == 0) {
			CWARN("timeout IB rdma_connect to [%s] ?\n",
					nid_str(kconn->r_nid));
			kconn->error = -ETIMEDOUT;
		}
	}

	/* connection errors? */
	if (IB_GET_CONN_STATE(kconn) != IBA_CONNECTED) {
		CWARN("IB connect error(%d) host[%s] ?\n",
					kconn->error,
					nid_str(kconn->r_nid));
	}
	return (kconn->error > 0 ? (-1 * kconn->error) : kconn->error);
}

/*
 * DESCRIPTION
 *	Establish an IB connection to the specified nid (Network IDentifier)
 *	IPv4 address and port.
 * PARAMETERS
 *	nid	binary IPv4 address in net byte-order.  Normally the IP addrs
 *		is the one which is bound to the IPoIB interface 'ib0' on the
 *		remote node; not used in the TCP space here, just a bit pattern.
 *	port	port number. Not a TCP port although it can be the same #.
 *		point is the port is just a number only meaningful to IB.
 */
int
ib_connect(u32 nid, u32 port, ib_conn_t **conn)
{
	int		rc = -ENODEV;
	ib_nexus_t	*ibd = &ib_nexus;
	ib_conn_t	*kconn;
	ulong		Tstart;

	*conn = NULL;	/* nothing yet */
	if (!ibd->my_hid.nid) {
		// missing local nid?
		CERROR("local nid NOT set, unable to connect?\n");
		return -ENODEV;
	}
	if (port == 0) {
		CERROR("missing service port ?\n");
		return -ENODEV;
	}
	if (nid == 0) {
		printk("%s zero remote IPv4 addrss?\n",__func__);
		return -EINVAL;
	}
	dprintk(DBG_CONN,("%s%s to nid [%s] port %d\n", WHOAMI,
						nid_str(nid), port));
#if 0
	// already connected?
	if ( locate_connection(nd,nid) != NULL ) {
		dprintk(DBG_CONN,("%s%s Already connected to nid [%s]\n",
						WHOAMI,nid_str(nid)));
		return -EISCONN;
	}
#endif
	Tstart = jiffies;

	// Allocate a new connection description
	kconn = alloc_new_connection();
	if (kconn == NULL) {
		CERROR("Unable to alloc connection?\n");
		return (-ENOMEM);
	}
	/*
	 * connect to an ICS server. Record the target server's IP address and
	 * the service port for later use.
	 */
	rc = connect_to_server(kconn,nid,port);
	Tstart = jiffies - Tstart;
	if (rc) {
		if (port != ICS_PROBE_CLMS_PORT)
			CWARN("No connection to host [%s] port %d\n",
						nid_str(nid), port);
		ib_destroy_connection(kconn);
	}
	else {
		dprintk(DBG_CONN,("%s connected to [%s].port(%d)\n", ME,
						nid_str(nid), port));
		/* return IB connection struct adrs */
		*conn = kconn;
	}
	dprintk(DBG_CONN,("%s connect time %lu\n",__func__,Tstart));
	return rc;
}

