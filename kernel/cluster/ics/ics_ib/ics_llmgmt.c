/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
 */

/*
 * 	Low-level ICS management code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the code for managing the ICS low-level module. It
 * does all the initialization and deinitialization of low-level ICS code,
 * and does all the node-up/node-down notifications.
 */

//#define ICS_DEBUG 1

#include <linux/types.h>
#include <linux/net.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/tcp.h>
#include <linux/inet.h>
#include <linux/delay.h>
#include <linux/reboot.h>
#include <linux/sched.h>
#include <linux/random.h>
#include <linux/crc32.h>

#include <net/sock.h>
#include <asm/uaccess.h>

#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */
#include <cluster/clms.h>
#include <cluster/node_monitor.h>

/* referenced in ics_ib_cm.c */
void ics_probe_clms_daemon(void *);
void ics_accept_connection(void *);

static int ics_request_connection(clusternode_t, int);
static int ics_llnodeup_connect(clusternode_t);
static void ics_llunack_init(void);
static void ics_llunack_daemon(void *);
static int ics_connect(struct ib_conn *, clusternode_t, int);
static int ics_llwaitforallconnections(clusternode_t);
static void ics_llnodeinfo_init(clusternode_t);
static int ics_chan_listen(int port);

ics_llnodeinfo_t	*ics_llnodeinfo[NSC_MAX_NODE_VALUE + 1];
clihand_list_t		clihand_list[CLIHAND_ARRAYSZ];
int			ics_service_ports[ICS_MAX_LL_CHANNELS];
int			ics_client_ports[ICS_MAX_LL_CHANNELS];
char			ics_ifdevname[ICS_MAX_IFNAME_SIZE];
char			ics_ifname[ICS_MAX_IFNAME_SIZE];
char			ics_netaddr[ICS_IPADDRLEN];
char			ics_netmask[ICS_IPADDRLEN];
LOCK_T			ics_llconn_lock;
SPIN_LOCK_T		ics_llnodeinfo_lock;
icsservice_t		icsservice[ICS_MAX_CHANNELS];
svrhand_list_t 		svrhand_list[SVRHAND_ARRAYSZ];
COND_LOCK_T		ics_nodestatus_lock;
CONDITION_T		ics_nodestatus_cond;

extern ics_chan_t ics_probe_clms_chan;

extern int ics_lltransport_config_done;

/*
 * Nodedown daemon list.
 */
SPIN_LOCK_T	ics_llnodedown_list_lock;
EVENT_T		ics_llnodedown_event;

/*
 * Seconds to wait before retrying connect.  This also equals tcp
 * max segment life time.
 */
#define ICS_CONNECT_DELAY 120

static struct socket *uics_sock;	/* socket for use by ics_llunack_send */

/*
 * ics_llinit()
 *	Initialize low-level ICS.
 *
 * Description:
 *	The low-level NSC ICS subsystem is initialized here. Called from
 *	the high-level ICS initialization routine.
 *
 *	This routine must be invoked from thread context.
 *
 * Parameters:
 *	None.
 */
void
ics_llinit()
{
	int	chan;
	int	port;
	int	error;

	/* Initialize locks and events. */
	INIT_SPIN_LOCK(&ics_llnodeinfo_lock);
	INIT_SPIN_LOCK(&ics_llnodedown_list_lock);
	INIT_EVENT(&ics_llnodedown_event);
	INIT_LOCK(&ics_llconn_lock);

	/*
	 * Perform the socket setup necessary for ICS communications.
	 * This function also establishes the current node's IP address and
	 * performs other setup.
	 */
	ics_lltransport_config();

	/*
	 * Assign TCP port numbers for each NSC Service known.
	 * Make sure port numbers are unique globally. Check relevant RFC.
	 */
	for (chan=0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		port = ICS_SVC_PORT + chan;
		if (port >= PPP_PORT)
			port++;
		ics_service_ports[chan] = port;
		port = ICS_CLI_PORT + chan;
		ics_client_ports[chan] = port;
	}

	/*
	 * Perform setup for the array with the IP addresses of all other
	 * nodes. Initial entries in the array are the current node and the
	 * cluster master node.
	 */
	error = ics_seticsinfo(this_node, (icsinfo_t *)ics_netaddr);
	if (error != 0) {
		panic("ics_llinit: ics_seticsinfo: error %d", error);
	}

	/*
	 * Set up the unacknowledged ICS service.
	 */
	ics_llunack_init();
}


/*
 * ics_llprobe_clms()
 *	Establish a connection for a given ICS comm channel to a given node.
 *
 * Description:
 *	A TCP connection is established for a given communication channel
 *	to the given remote node.
 *
 *	On each connection, TCP_NODELAY is set. This disables the Nagle
 *	algorithm (refer to RFC 793/1122). Generally TCP collects packets
 *	until the packet is big enough to send. This is to avoid sily
 *	window syndrome, which can occur when the memory goes very low.
 *	However, in the case of NSC, each packet the upper layer sends
 *	is a datagram and it has to be sent to the other side immediately
 *	regardless of its size.  For example, a request to kill a remote
 *	process is ~200 bytes in size. It has to be sent to the other
 *	side immediately.
 *
 * Parameters:
 *	remote_node	Node number of the node to contact.  (Not used in
 *			the ics_tcp implementation.)
 *	remote_addr	Address of the node to contact as an icsinfo_t.  This
 *			is actually an ASCII IP address here.
 *	master_node	Receives the node number of the actual master node,
 *			if such is sent by the contacted node.
 *	master_addr	Receives the IP address (as an icsinfo_t) of the
 *			master node.
 *
 * Return Value:
 *	0 == success, else error.
 */
int
ics_llprobe_clms(clusternode_t remote_node,
                 icsinfo_t     remote_addr,
                 clusternode_t *master_node,
                 icsinfo_t     *master_addr,
                 int           im_a_secondary,
                 int           im_a_member)
{
	int			rc;
	int			status = CLMS_PROBE_ERR_NOANSWER;
	u32			ipaddr;
	ib_conn_t		*ibc;
	kib_rx_t		*rxd;
	kib_tx_t		*txd;
	ics_clmsprobe_req_t	*reqp;
	ics_clmsprobe_rep_t	*repp;

	if (!ics_lltransport_config_done) {
		return status;
	}

	ipaddr = in_aton((char *)&remote_addr); // cvt ascii -> net order u32

#ifdef ICS_DEBUG
	printk("%s Probing node %d @ %s\n",__func__,
				remote_node,(char *)&remote_addr);
#endif
	/*
	 * Connect to the node. If this fails, return an error.
	 */
	rc = ib_connect(ipaddr, ICS_PROBE_CLMS_PORT, &ibc);
	if (rc) {
#ifdef ICS_DEBUG
		printk("%s unable to connect with node %d error %d\n",
						__func__,remote_node,rc);
#endif
		goto out;
	}
#ifdef ICS_DEBUG
	printk("%s IB connected to node %d @ %s\n",__func__,
				remote_node,(char *)&remote_addr);
#endif

	/* acquire a TX (transmit) descriptor & buffer */
        if ( (txd=get_txd(ibc,TRUE)) == NULL ) {
                CERROR ("ERR: Unable to allocate free pool TX desc?\n");
                goto out;
        }

	/* init RPC message */
	reqp = (ics_clmsprobe_req_t*)txd->payload;
	memset((void*)reqp,0,sizeof(*reqp));
	reqp->my_node = this_node;
	//memset(&reqp->my_icsinfo, 0, sizeof(reqp->my_icsinfo));
	ics_llnode_to_icsinfo(&reqp->my_icsinfo, this_node);
	reqp->imasecondary = im_a_secondary;
	reqp->imamember = im_a_member;

#ifdef ICS_DEBUG_V
	printk("%s IB send nob %d to [%s]\n",
			__func__,sizeof(*reqp),nid_str(ibc->r_hid.nid));
#endif
	/* post send & wait for send completion */
	rc = ib_sendmsg(txd, sizeof(*reqp));
	put_idle_txd(txd);
	if (rc) {
                CWARN("IBA send failed(%d) to [%s]\n", rc,
						nid_str(ibc->r_hid.nid));
		goto out;
	}

	/*
	 * Get the node number and IP address of the master from the target.
         */
	dprintk(DBG_CONN_V,
		("%s ib_readmsg() node # & IP addr of master\n",__func__));

	if (ib_readmsg(ibc, &rxd, 20)) {
		printk("%s: receive of master info failed, error(%d); possibly "
			"node went down\n",__func__, rxd->status);
		goto out;
	}

	if ( rxd->nob != sizeof(*repp) ) {
		printk("%s: bad length: request master info msg, "
			"wanted(%d) received(%d)?\n",
					__func__, sizeof(*repp), rxd->nob);
		goto out;
	}

	repp = (ics_clmsprobe_rep_t*)rxd->payload;

#ifdef ICS_DEBUG
	printk("%s master_node @ %d\n",__func__,repp->cp_master);
#endif
	/*
	 * Copy the node number.
	 */
	*master_node = repp->cp_master;
	memcpy(master_addr, &repp->cp_icsinfo, sizeof(*master_addr));
	status = repp->error;
out:
	if (ibc) ib_destroy_connection(ibc);

	return status;
}


/*
 * ics_llaccept_notifications()
 *	Inform low-level ICS code that nodeup/nodedown notifications are
 *	allowed.
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the ICS
 *	low-level code that it can call ics_nodeup_notification() and
 *	ics_nodedown_notification(). Prior to calling
 *	ics_accept_notifications(), the nodeup and nodedown notifications
 *	may not be done.
 *
 *	The low-level ICS code may finish up some initialization code
 *	at this time (i.e. the initialization code that allows for
 *	information about nodes coming up and going down).
 *
 *	This routine must be called from thread context.
 */
void
ics_llaccept_notifications(void)
{
	int		error;
	ib_nexus_t	*ibd = &ib_nexus;

	/*
 	 * Start listening for ICS service connection requests.
	 */
	error = ics_chan_listen(ICS_SVC_PORT);
	if (error)
		panic("%s: ics_chan_listen() error %d", __func__,error);

	/*
 	 * Start listening for CLMS probe requests.
	 */
        error = ib_listen_for_connection(&ibd->ics_probe_listen,
					 ICS_PROBE_CLMS_PORT);
	if (error)
		panic("%s: listen() error(%d) on ICS probe port(%d)\n",
					__func__,error,ICS_PROBE_CLMS_PORT);
}

/*
 * ics_llnodeinfo_init()
 *	Allocate a ics_llnodeinfo_t structure if there isn't one there already.
 *
 * Description:
 *	This is an internal routine used by the low-level ICS code to
 *	allocate a ics_llnodeinfo_t structure for a new node (if one has not
 *	already been allocated.
 *
 *	Note that it is probably unsafe to EVER get rid of a ics_llinfo_t
 *	structure once it has been allocated.
 *
 * Parameters:
 *	node		the node that is coming up
 */
static void
ics_llnodeinfo_init(
	clusternode_t	 node)
{
	ics_llnodeinfo_t *nodeinfo;

	/* Allocate the overall structure if necessary. */
	if (ics_llnodeinfo[node] == NULL) {
		nodeinfo = (ics_llnodeinfo_t *)
			   kmalloc_nofail(sizeof(ics_llnodeinfo_t));
		SSI_ASSERT(nodeinfo != NULL);
		memset(nodeinfo, 0, sizeof(ics_llnodeinfo_t));
		INIT_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		INIT_EVENT(&nodeinfo->icsni_event);
		INIT_EVENT(&nodeinfo->icsni_chan_event);
		LOCK_SPIN_LOCK(&ics_llnodeinfo_lock);
		if (ics_llnodeinfo[node] == NULL)  {
			ics_llnodeinfo[node] = nodeinfo;
			UNLOCK_SPIN_LOCK(&ics_llnodeinfo_lock);
		} else {
			UNLOCK_SPIN_LOCK(&ics_llnodeinfo_lock);
			DEINIT_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			DEINIT_EVENT(&nodeinfo->icsni_event);
			DEINIT_EVENT(&nodeinfo->icsni_chan_event);
			kfree(nodeinfo);
		}
	}
}

/*
 * ics_llseticsinfo()
 *	Store the ICS communication information about a node
 *
 * Description:
 *	This routine is called to store the information ICS needs to
 *	make a connection with another node in its local database. This
 *	routine is called by the CLMS when the CLMS (either master or
 *	client) receives information about a node.
 *
 *	This routine is also be used internally to store the ICS information
 *	about this node, the CLMS master node, and about nodes for which
 *	nodeup notifications have been generated.
 *
 *	Until this call is done for a node, ics_nodeup() cannot be called for
 *	a node, nor can CLMS communicate with this node.
 *
 *	For the socket-based implementation, the ICS information consists
 *	of the IP address of the node.
 *
 * Parameters:
 *	node		the node about which information is being stored
 *	icsinfo_p	the actual information being stored; treated as
 *			a character array
 */
int
ics_llseticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p)
{
	ics_llnodeinfo_t *nodeinfo;

	SSI_ASSERT(sizeof(icsinfo_t) >= ICS_IPADDRLEN);

	/* Allocate the overall structure if necessary. */
	ics_llnodeinfo_init(node);
	nodeinfo = ics_llnodeinfo[node];

	/* Make sure there isn't a string there already. */
	if (nodeinfo->icsni_node_addr != NULL)
		return(-EINVAL);

	/* Allocate space for the string. */
	nodeinfo->icsni_node_addr = kmalloc_nofail(ICS_IPADDRLEN);

	/* If the node went down, bail out. */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (nodeinfo->icsni_flag & ICSNI_GOING_DOWN) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		kfree(nodeinfo->icsni_node_addr);
		return(-EAGAIN);
	}
	nodeinfo->icsni_flag = ICSNI_COMING_UP;

	/* Copy the string (which is an IP address) into the allocated space. */
	memcpy(nodeinfo->icsni_node_addr, (void *)icsinfo_p, ICS_IPADDRLEN);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	return(0);
}

/*
 * ics_llgeticsinfo()
 *	Fetch the ICS communication information about a node
 *
 * Description:
 *	This routine is called to fetch the information ICS needs to
 *	make a connection with another node from its local database. This
 *	routine is called by the CLMS when the CLMS (either master or
 *	client) needs to send the ICS information to another node.
 *
 *	For the socket-based implementation, the ICS information consists
 *	of the IP address of the node.
 *
 * Parameters:
 *	node		the node about which information is being fetched
 *	icsinfo_p	the actual information that was stored; treated as
 *			a character array
 */
int
ics_llgeticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p)
{
	ics_llnodeinfo_t *nodeinfo;

	if (node == CLUSTERNODE_INVAL || node > NSC_MAX_NODE_VALUE)
		return -EINVAL;

	nodeinfo = ics_llnodeinfo[node];

	SSI_ASSERT(sizeof(icsinfo_t) >= ICS_IPADDRLEN);

	/* Make sure there is a string there already. */
	if (nodeinfo == NULL || nodeinfo->icsni_node_addr == NULL)
		return(-EINVAL);

	/* Copy the string to the output parameter. */
	memset((void *)icsinfo_p, 0, sizeof(icsinfo_t));
	memcpy((void *)icsinfo_p, nodeinfo->icsni_node_addr, ICS_IPADDRLEN);

	return(0);
}

/*
 * ics_llnodeup()
 *	Perform any initialization necessary for low-level ICS code
 *	when a new node joins the cluster.
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the ICS
 *	low-level code that it should allow communication by NSC services to
 *	a specific remote node.
 *
 *	Note that the CLMS service is allowed to communicate with other
 *	nodes even if ics_llnodeup() has not been called.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	rnode		node that just joined the system
 */
int
ics_llnodeup(clusternode_t rnode)
{
	int		error = 0;
	int		heskey, imkey;
	ics_llnodeinfo_t        *nodeinfo;

	/*
	 * If this is us, then there is nothing to do. If we have a key
	 * service and the rnode does not, initiate connections. If both
	 * have key services or both don't, then the lower numbered node
	 * should initiate the connections.  If we're the CLMS master, we
	 * never initiate connections.
	 */
	if (this_node != clms_master_node && rnode != this_node) {
		heskey = clms_iskeynode(rnode);
		imkey = clms_iskeynode(this_node);
		if (rnode == clms_master_node ||
		    ((imkey && !heskey) ||
		     (this_node < rnode &&
		      ((imkey && heskey) || (!imkey && !heskey))))) {
			error = ics_llnodeup_connect(rnode);
		}
	}
	/*
	 * Wait for all connections with the node to be
	 * complete before proceeding.
	 */
	if (rnode != this_node && error == 0) {
		error = ics_llwaitforallconnections(rnode);
	}

	if (!error) {
		nodeinfo = ics_llnodeinfo[rnode];
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (nodeinfo->icsni_flag == ICSNI_COMING_UP)
			nodeinfo->icsni_flag = ICSNI_NODE_UP;
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	}
	return(error);
}

/*
 * ics_llnodedown()
 *	Perform all work necessary to clean up communications with a down
 *	node and do not permit further communication with the node.
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the ICS
 *	ICS low-level code that it should stop communication with another node,
 *	and cause all outstanding communications with the node to fail.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_llnodedown(
	clusternode_t	node)
{
	ics_llnodedown_async_start(node);
	ics_llnodedown_async_end(node);
}

/*
 * ics_llnodedown_async_start()
 *	Perform all work necessary to clean up communications with a down
 *	node and do not permit further communication with the node.
 *	This is the asynchronous version, in which the nodedown processing
 *	is performed in two steps.  This start routine sets the low-level
 *	ICS state of the down node to "going down" and does nodedown
 *	processing.  Later in the end routine, we set the low-level ICS
 *	state to fully "down".
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the
 *	ICS low-level code that it should stop communication with another node,
 *	and cause all outstanding communications with the node to fail.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_llnodedown_async_start(clusternode_t node)
{
	ics_ib_conn_data_t	*conn;
	int			chan;
	ics_llnodeinfo_t	*nodeinfo = ics_llnodeinfo[node];
	boolean_t		dowait;

	/*
	 * It is possible (but unlikely) that the nodeinfo structure
	 * isn't set up yet. If so, we are done.
	 */
	if (nodeinfo == NULL)
		return;

	/*
	 * If the node is already down, we have no work to do. This
	 * can happen if the node going down is the former CLMS master,
	 * which we marked down previously in rclms_query_cluster.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (nodeinfo->icsni_flag & ICSNI_NODE_DOWN) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return;
	}

	/*
	 * Mark the node as in the special state of going down, and wait for
	 * any long-term pending operations to complete (long-term pending
	 * operations increment the nodeinfo reference count and pay attention
	 * to the special state in icsni_flag; short-term pending operations
	 * simply lock the connection lock, and they are taken care of later
	 * when we shut down individual connections).
	 */
	SSI_ASSERT((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) == 0);
	nodeinfo->icsni_flag = (ICSNI_GOING_DOWN | ICSNI_NODE_DOWN);
	dowait = (nodeinfo->icsni_ref_cnt != 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowait)
		WAIT_EVENT(&nodeinfo->icsni_event);
	SSI_ASSERT(nodeinfo->icsni_ref_cnt == 0);

	/*
	 * Send a disconnect request and then close all the comm channels for
	 * the node that just went down.
	 */
	for (chan = 0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		if (chan == ics_probe_clms_chan)
			continue;

		/*
		 * Find the connection. If the connection hasn't been
		 * established yet, then go to the next connection. Zero out
		 * the connection pointer to make sure we never use it
		 * again
		 */
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (nodeinfo->icsni_conn == NULL ||
		    nodeinfo->icsni_conn[chan] == NULL) {
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			continue;
		}
		conn = nodeinfo->icsni_conn[chan];
 		nodeinfo->icsni_conn[chan] = NULL;
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

		/*
		 * Send the disconnect request and shutdown/destroy
		 */
#if MUX_ICS_CHANNELS
		if ( chan == 0 )
#endif
		ib_destroy_connection(conn->conn_ib);

		/*
		 * Wakeup the throttled processes (clients) and
	 	 * wait for everyone to stop using the connection.
		 */
		while (READ_ATOMIC_INT(&conn->conn_ref_cnt) != 0)
			nidelay(HZ/20);
		conn->conn_ib = NULL;
		rand_deinitialize_ics(conn->conn_snd_rand_statep);
		conn->conn_snd_rand_statep = NULL;
		rand_deinitialize_ics(conn->conn_rcv_rand_statep);
		conn->conn_rcv_rand_statep = NULL;
		DEINIT_LOCK(&conn->conn_lock);
		kfree(conn);
		icssvr_llnodedown(node, chan);
	}

	/*
	 * Ensure that ics_seticsinfo() is called again before we allow
	 * communication with this node again.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (nodeinfo->icsni_node_addr != NULL) {
		kfree(nodeinfo->icsni_node_addr);
		nodeinfo->icsni_node_addr = NULL;
	}
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	/*
	 * Now that all the connections are torn down, call the low-level
	 * client code to clean up.
	 */
	icscli_llnodedown(node);
}

/*
 * ics_llnodedown_async_end()
 * 	Set the low-level ICS state of the down node to fully "down".
 * 	This is called after ics_llnodedown_async_start().
 *
 * Description:
 *	This routine is called by ICS high-level code to complete the
 *	low-level ICS nodedown processing by setting the low-level ICS
 *	state of the down node to fully "down".
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_llnodedown_async_end(clusternode_t node)
{
	ics_llnodeinfo_t		*nodeinfo = ics_llnodeinfo[node];

	if (nodeinfo == NULL)
		return;
	/*
	 * Turn off the special "going down" flag. This means that we
	 * can start accepting connections for this node once more.
	 *
	 * Note that we don't try to free the nodeinfo structure. Trying
	 * to do this opens up some difficult timing windows with respect
	 * to node shutdown and restart, and is not worth doing, since any
	 * node that went down will probably come back anyway, and there
	 * isn't that much memory involved.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	nodeinfo->icsni_flag = ICSNI_NODE_DOWN;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
}

/*
 * ics_llunack_open()
 *	Create an endpoint for the ICS unacknowledged service.
 *
 * Description:
 *	This function creates a UDP endpoint for the specified UDP port.
 *
 * Parameters:
 *	int port		UDP port for this endpoint
 *				in host byte order
 *
 * Returns:
 *	sock representing the endpoint
 */
static struct socket *
ics_llunack_open(int port)
{
	int error;
	char *ipaddr;
	struct socket *sock;
	struct sockaddr_in sin;

	ipaddr = ics_llnodeinfo[this_node]->icsni_node_addr;
	SSI_ASSERT(ipaddr != NULL);

	error = sock_create(PF_INET, SOCK_DGRAM, IPPROTO_UDP, &sock);
	if (error < 0)
		panic("%s: sock_create failed, error %d!\n", __func__, error);

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = in_aton(ipaddr);
	sin.sin_port = htons(port);

	error = sock->ops->bind(sock, (struct sockaddr *)&sin, sizeof(sin));
	if (error < 0)
		panic("%s: bind error %d", __func__, error);
	return sock;
}

/*
 * ics_llunack_daemon()
 *	Daemon to process unacknowledged ICS requests.
 *
 * Description:
 *	This daemon waits for datagrams on a UDP port.  When one is
 *	received it is validated and processed.
 * Note:
 *	At present, the only supported request is ICS_SHOOT_NODE.
 */
void
ics_llunack_daemon( void *argp )
{
	int len;
	struct socket *sock;
	uics_reqheader *hdr;
	icsinfo_t icsinfo;
	struct msghdr *msg;
	struct iovec iov;
	ics_shoot_node_t *req;
	void *base_addr;

        nsc_daemonize();
        (void)set_daemon_prio(MAX_RT_PRIO-3, SCHED_FIFO);

	/*
	 * Establish a UDP socket to send and receive unacknowledged
	 * requests.
	 */
	sock = ics_llunack_open(ICS_UNACK_SVR_PORT);
	SSI_ASSERT(sock);

	msg = (struct msghdr *)kmalloc_nofail(sizeof(struct msghdr));
	SSI_ASSERT(msg != NULL);
	msg->msg_name = NULL;
	msg->msg_namelen = 0;
	msg->msg_iov = &iov;
	msg->msg_iovlen	= 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = MSG_WAITALL;
	hdr = base_addr = kmalloc_nofail(sizeof(uics_reqheader) +
						ICS_UNACK_MAX_DGRAM_SIZE);
	SSI_ASSERT(hdr != NULL);

	for (;;) {
		(void)allow_reschedule();

		msg->msg_iov->iov_base = base_addr;
		msg->msg_iov->iov_len = (__kernel_size_t)
			(sizeof(uics_reqheader) + ICS_UNACK_MAX_DGRAM_SIZE);

		/*
		 * Receive a datagram.
		 */
		len = sock_recvmsg(sock,
				   msg,
				   sizeof(uics_reqheader) +
				   ICS_UNACK_MAX_DGRAM_SIZE,
				   MSG_WAITALL);
		if (len < 0) {
			printk(KERN_WARNING
			       "%s: sock_recvmsg error %d", __func__, -len);
			continue;
		}

		if (len < sizeof(uics_reqheader)) {
			printk(KERN_WARNING
			       "%s: received short message (%d)",__func__,len);
			continue;
		}
		//printk("%s recv msg len %d\n",__func__,len);

		ics_geticsinfo(hdr->uicsh_sender, &icsinfo);

		/*
		 * If we're not part of a cluster (we're not the CLMS master,
		 * nor have we found one), let the guy with the higher version
		 * win.  We panic.
		 */
		if (hdr->uicsh_version.uicsh_ver > ics_message_version &&
		    clms_master_node == CLUSTERNODE_INVAL)
			panic("Shot by newer kernel.");
		if (hdr->uicsh_version.uicsh_ver != ics_message_version) {
			printk(KERN_WARNING
			       "%s: ignoring mesg from %s ver %d expected %d",
			       __func__, (char *) icsinfo.icsinfo_string,
			       hdr->uicsh_version.uicsh_ver,
			       ics_message_version);
			continue;
		}

		/* Process "shoot_node" packet. */
		if (hdr->uicsh_type & ICS_UNACK_SHOOT_NODE) {
			if (!clms_is_secondary_by_addr(&icsinfo)) {
				printk(KERN_WARNING
				      "%s: received request from unauthorized "
					"node (0x%lx)", __func__,
						(unsigned long) &icsinfo);
				continue;
			}

			SSI_ASSERT(hdr + 1 != NULL);
			req = (ics_shoot_node_t *)(hdr + 1);

			printk(KERN_NOTICE "Shot by %s (his ICS version is %x "
				"and he %s a cluster).",
			       icsinfo.icsinfo_string,
			       req->my_ics_version,
			       (req->i_am_cluster ?  "has" : "doesn't have"));
			machine_restart(NULL);
			/*NOTREACHED*/
		}
		/* Process "imalive" packet. */
		else if (hdr->uicsh_type & ICS_UNACK_IMALIVE) {
			nm_received_imalive(hdr->uicsh_sender);
#ifdef CONFIG_LDLVL
			/* Process loadinfo packet. */
			if (hdr->uicsh_type & ICS_UNACK_LOAD) {
				load_array_t *loadptr;
				int	size;

				size = (len - sizeof(uics_reqheader));
				loadptr = (load_array_t *)kmalloc_nofail(size);
				memcpy(loadptr, (char *)(hdr + 1), size);
				loadinfo_received(loadptr);
				kfree(loadptr);
			}
#endif /* CONFIG_LDLVL */
		}
		else
			panic("%s: bad message type %d", __func__,
			      hdr->uicsh_type);
			/*NOTREACHED*/
	}
}

/*
 * ics_llunack_send()
 *	Send an unacknowledged ICS message.
 *
 * Description:
 *	This function sends a datagram to ICS in the specified node.
 *
 * Parameters:
 *	clusternode_t node	node to send request to
 *	u_int type		type of request
 *	void *message		buffer containing request message
 *	size_t lenght		length of message
 *
 * Returns:
 *	Unix errno or 0
 */
int
ics_llunack_send(
	clusternode_t node,
	u_int type,
	void *message,
	size_t length)
{
	uics_reqheader 	*hdr;
	int 		len;
	struct msghdr 	*msg;
	struct iovec 	iov;
	struct sockaddr_in *sin;
	mm_segment_t	oldfs = get_fs();


	SSI_ASSERT(uics_sock);
	if (ics_llnodeinfo[node] == NULL ||
	    ics_llnodeinfo[node]->icsni_node_addr == NULL)
		return -EREMOTE;

	sin = (struct sockaddr_in *)
			kmalloc_nofail(sizeof(struct sockaddr_in));
	if (sin == NULL) {
		return -ENOMEM;
	}
	sin->sin_family = AF_INET;
	sin->sin_port = htons(ICS_UNACK_SVR_PORT);
	sin->sin_addr.s_addr = in_aton(ics_llnodeinfo[node]->icsni_node_addr);

	hdr = kmalloc(sizeof(uics_reqheader) + length, GFP_KERNEL);
	if (hdr == NULL) {
		kfree(sin);
		return -ENOMEM;
	}
	hdr->uicsh_version.uicsh_ver_type = 15;
	hdr->uicsh_version.uicsh_ver = ics_message_version;
	hdr->uicsh_type = type;
	hdr->uicsh_procnum = 0;
	hdr->uicsh_service = 0;
	hdr->uicsh_transid = 0;
	hdr->uicsh_chan = 0;
	hdr->uicsh_sender = this_node;
	memcpy((char *)(hdr + 1), message, length);

	msg = (struct msghdr *)kmalloc(sizeof(struct msghdr), GFP_KERNEL);
	if (msg == NULL) {
		kfree(sin);
		kfree(hdr);
		return -ENOMEM;
	}
	msg->msg_name = sin;
	msg->msg_namelen = sizeof(struct sockaddr_in);
	msg->msg_iov = &iov;
	msg->msg_iovlen	= 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = MSG_WAITALL;
	msg->msg_iov->iov_base = (char *)hdr;
	msg->msg_iov->iov_len = (__kernel_size_t)sizeof(uics_reqheader) +
								length;
	SSI_ASSERT(uics_sock->sk != NULL);
	set_fs(KERNEL_DS);
	len = sock_sendmsg(uics_sock, msg, sizeof(uics_reqheader) + length);
	set_fs(oldfs);

	kfree(sin);
	kfree(hdr);
	kfree(msg);
#ifdef ICS_DEBUG_V
	printk("%s sock_sendmsg(type %d nob %d) to [%s]\n",
		__func__,type,len,nid_str(sin->sin_addr.s_addr));
#endif
	return(len < 0 ? len : 0);
}

/*
 * ics_llunack_init()
 *	Initialize the unacknowledged ICS service.
 *
 * Description:
 *	This function creates an endpoint to be used to send unacknowledged
 *	ICS requests (of which only ICS_SHOOT_NODE exists at present) and
 *	starts a daemon to service them.
 */
static void
ics_llunack_init(void)
{
	int error;

	/*
	 * Create the endpoint for sending unacknowledged requests.
	 */
        uics_sock = ics_llunack_open(ICS_UNACK_CLI_PORT);
        if (uics_sock == NULL)
                panic("%s: Unable to assign port",__func__);

	/*
	 * Set up the ICS unacknowledged services.
	 */
	error = spawn_daemon_thread("ics_llunack_daemon",
				    ics_llunack_daemon,
				    NULL);
	if (error != 0)
		panic("%s: ics_llunack_daemon create failed, error=%d",
							__func__, error);
}

/*
 * ics_llisicsport()
 *	Check if the given port is a port assigned for an ics service.
 *
 * Description:
 *	This function checks to see if a TCP port is reserved for ICS use.
 *	If so, it returns a 1 otherwise it returns a 0.
 *	This function assumes that the port is in Network Byte Order.
 *	It converts it to Host Byte Order before comparing with the stored
 *	value.
 *
 * Parameters:
 *	int port	port number
 *
 * Returns:
 *	1 if the port is an ICS port, 0 otherwise.
 */
int
ics_llisicsport(
	unsigned short	port)
{
	int chan;

	if (port == 0)
		return 0;

	port = ntohs(port);

	for (chan=0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		if (port == ics_service_ports[chan] ||
		    port == ics_client_ports[chan])
			return 1;
	}

	return 0;
}

/*
 * ics_llisicsunackport()
 *	Check if the given port is a port assigned for an unacknowledged
 *	ICS service (including Regroup).
 *
 * Description:
 *	This function checks to see if a UDP port is reserved for ICS
 *	or Regroup use.  If so, it returns a 1; otherwise it returns a 0.
 *	This function assumes that the port is in Network Byte Order.
 *	It converts it to Host Byte Order before comparing with the stored
 *	value.
 *
 * Parameters:
 *	int port	port number
 *
 * Returns:
 *	1 if the port is an ICS port, 0 otherwise.
 */
int
ics_llisicsunackport(
	unsigned short	port)
{
	unsigned short hport = ntohs(port);

	return (hport == ICS_UNACK_SVR_PORT || hport == ICS_UNACK_CLI_PORT);
}

/*
 * ics_llissvcenabled()
 *	Check to see if a particular service is enabled.
 *
 * Parameters:
 *	int svc			service number
 *	clusternode_t node	node number
 *
 * Returns:
 *	1 if the svc is enabled, 0 otherwise.
 */
int
ics_llissvcenabled(
	int svc,
	clusternode_t node)
{
	ics_llnodeinfo_t *nodeinfo = NULL;
	int chan = ICS_CHAN(svc);

	if (node >= 1 || node <= NSC_MAX_NODE_VALUE)
		nodeinfo = ics_llnodeinfo[node];
#ifdef ICS_DEBUG
	else
		SSI_ASSERT(node == CLUSTERNODE_INVAL);
#endif

	if (nodeinfo != NULL && nodeinfo->icsni_conn != NULL &&
	    nodeinfo->icsni_conn[chan] != NULL)
		return 1;

	return 0;
}

/*
 * ics_probe_clms_daemon()
 *	Listen for CLMS probes on the port ICS_PROBE_CLMS_PORT.
 *
 * Description:
 *	This function is called from spawn_daemon_thread function.
 *	Listens for connection requests from nodes that are probing
 *	to find the CLMS master.
 *
 * Parameters:
 *	port_argp		port to listen on.
 *
 * Return Values:
 *	None.
 */
void
ics_probe_clms_daemon(void *argp)
{
	ib_conn_t		*ibc = (ib_conn_t*)argp;
	int			error;
	clusternode_t		master;
	ics_clmsprobe_req_t	*reqp;
	ics_clmsprobe_rep_t	*repp;
	kib_rx_t		*rxd;
	kib_tx_t		*txd;

	nsc_daemonize();

	/*
	 * get the client request buffer
	 */
	if (ib_readmsg(ibc, &rxd, 0)) {
		printk(KERN_WARNING "%s: receive of request info failed, "
			"error(%d); possibly node went down\n",
							__func__, rxd->status);
		goto out;
	}

	if (rxd->nob != sizeof(*reqp)) {
		printk(KERN_WARNING "%s: bad length for request info msg, "
			"wanted(%d) received(%d)?\n",
					__func__, sizeof(*reqp), rxd->nob);
		goto out;
	}
	reqp = (ics_clmsprobe_req_t*)rxd->payload;

#ifdef ICS_DEBUG
	printk("%s: received probe request\n",__func__);
#endif
	master = CLUSTERNODE_INVAL;
	error = clms_get_probe_info(reqp->imasecondary,
				    reqp->imamember,
				    reqp->my_node,
				    &master);

	/* acquire a TX (transmit) descriptor, buffer */
        txd = get_txd(ibc,TRUE);
        if (!txd) {
                CERROR ("ERR: Unable to allocate TX desc from free pool?\n");
                goto out;
        }
	repp = (ics_clmsprobe_rep_t*)txd->payload;	/* point at reply */

	/* set reply message data */
	repp->cp_master = master;
	memset(&repp->cp_icsinfo, 0, sizeof(repp->cp_icsinfo));
	if (master != CLUSTERNODE_INVAL)
		ics_llnode_to_icsinfo(&repp->cp_icsinfo, master);
	repp->error = error;

#ifdef ICS_DEBUG
	printk("%s reply to [%s] master @ node %d\n",
		__func__,nid_str(ibc->r_hid.nid),repp->cp_master);
#endif
	/* post & wait for send completion */
	error = ib_sendmsg(txd, sizeof(*repp));
	put_idle_txd(txd);
	if (error) {
		printk("%s: IB reply send failed, error(%d)",__func__,error);
        }

	/* repost ics channel receive buffer */
	if (kib_post_recv_wr(rxd)) {
		CWARN("Unable to re-post RX buffer?");
	}
	/*
	 * let the remote node client close the connection in order to ensure
	 * the client has time to read & process our reply.
	 */
	exit_daemon_thread();

	/* NOT REACHED */
out:
	ib_destroy_connection(ibc);
	exit_daemon_thread();

	/* NOT REACHED */
}

/*
 * ics_chan_listen()
 *	Listen for connection requests on port ICS_SVC_PORT (900).
 *	Accept the connection requests and set up the connection.
 *
 * Description:
 *	This function is called from spawn_daemon_thread function.
 *	Listens for connection requests for a given communication channel
 *	and sets up the NSC ICS Session for the given node/channel.
 *
 *	The idea here is that channel connections are requested by
 *	lower numbered nodes and listened for by higher numbered
 *	nodes. (The exception is the CLMS channel, where the CLMS
 *	master node is always the listener for the CLMS channel and
 *	the REPLY channel).
 *
 *	Once a connection request comes in, it is accepted using tcp_accept().
 *	The new connection is set up which will be used by the channel.
 *	The original communication endpoint is used to listen for more
 *	connections.
 *
 *	On each connection, TCP_NODELAY is set. This disables the Nagle
 *	algorithm (refer to RFC 793/1122). Generally TCP collects packets
 *	until the packet is big enough to send. This is to avoid silly
 *	window syndrome, which can occur when the memory goes very low.
 *	However, in the case of NSC, each packet the upper layer sends
 *	is a datagram and it has to be sent to the other side immediately
 *	regardless of its size.  For example, a request to kill a remote
 *	process is ~200 bytes in size. It has to be sent to the other
 *	side immediately. This is done by setting the socket option
 *	TCP_NODELAY.
 *
 * Parameters:
 *	port_argp		port number to listen on.
 *
 * Return Values:
 *	None.
 */
static int
ics_chan_listen(int port)
{
	char		*ipaddr;
	int		error;

	ipaddr = ics_llnodeinfo[this_node]->icsni_node_addr;
	SSI_ASSERT(ipaddr != NULL);

	dprintk(DBG_CONN,("%s port %d @ %s\n",__func__,port,ipaddr));

	/* post an Infiniband listen(). listen() is persistent until
	 * cancelled.
	 */
	error = ib_listen_for_connection(&ib_nexus.ics_svc_listen, port);
	if (error)
		printk("%s: listen() error(%d) on ICS service port(%d)\n",
						__func__,error,port);
	/*
	 * when a connection is established, an ics_accept_connection()
	 * thread will be created. see ics_ib_cm.c
	 */
	return error;
}

static int
ics_chan_connect(ics_llnodeinfo_t *nodeinfo,
                 clusternode_t    rnode,
                 int              chan,
                 icsinfo_t        *icsinfo,
                 ib_conn_t        *ibc)
{
	int goingdown = 0;

	/*
	 * If the node isn't in the special going-down state, then up
	 * the reference count so that the node can't go down and
	 * connections can't be torn down until this routine spots it
	 * and exits.  If it _is_ in the going-down state, set the
	 * flag; we'll send the response to the client and exit.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
		goingdown = 1;
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return goingdown;
	}
	/* Forget that we may have gone down before. */
	nodeinfo->icsni_flag = ICSNI_NODE_UP;
	nodeinfo->icsni_ref_cnt++;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	/*
	 * The connection has been established from the listener's
	 * perspective.  Set up all the correct data structures.
	 */
	if (ics_connect(ibc, rnode, chan) != 0) {
		printk("%s err %d ics_connect(rnode %d chan %d)\n",__func__,
			goingdown,rnode,chan);
		return goingdown;
	}

	/*
	 * If this is the CLMS master and this is the CLMS channel
	 * then inform CLMS that the new node has established a
	 * connection with us.
	 */
	if (chan == ics_clms_chan && this_node == clms_master_node)
		ics_nodeup_notification(rnode, icsinfo);

#if ICS_DEBUG
	if (goingdown) printk("%s exit goingdown %d\n",__func__,goingdown);
#endif
	return goingdown;
}

static int
chk_node_chan_state(int state, clusternode_t rnode, int chan)
{
	if (this_node == clms_master_node && chan == ics_clms_chan) {
		if (ics_nodestatus[rnode]
		    & (ICS_NODE_COMING_UP | ICS_NODE_UP | ICS_NODE_READY))
				state = 1;      /* The node already exists. */
		else {
			int status;

			status = clms_get_node_status(rnode);
			if (!(status & (CLMS_NODE_COMING_UP|
				CLMS_NODE_DOWN|
				CLMS_NODE_NEVER_UP)))
					state = 2; /* The node needs to wait. */
		}
	}
	return state;
}

/*
 * ics_accept_connection()
 *	Accept an ICS connection request from another node.
 *
 * Description:
 *	A connection request has been received from another node, and
 *	this routine is spawned as a daemon to actually accept and
 *	establish a connection.
 *
 *	The idea here is that service connections are requested by
 *	lower numbered nodes and listened for by higher numbered
 *	nodes. (The exception is the CLMS service, where the CLMS
 *	master node is always the listener for the CLMS service and
 *	the REPLY service).
 *
 *	This function receives the node number and channel from the client and
 *	then actually establishes the connection.
 *
 * Parameters:
 *	argp	pointer to an IB connection descriptor.
 */
void
ics_accept_connection(void *argp)
{
	ib_conn_t	*ibc = argp;
	int		state;
	int		chan;
	clusternode_t	rnode;
	int		goingdown = 0;
	boolean_t	dowakeup;
	ics_llnodeinfo_t *nodeinfo;
	icsinfo_t	icsinfo;

	int		rc;
	kib_rx_t	*rxd;
	kib_tx_t        *txd;

	nsc_daemonize();

	memset((void*)&icsinfo, 0, sizeof(icsinfo));

	/*
	 * Generate the (asciz decimal dot notation) IPv4 address of the
	 * connecting node. This information is stored by ICS.
	 */
	strcpy((char*)&icsinfo.icsinfo_string, nid_str(ibc->r_hid.nid));

#ifdef ICS_DEBUG
	printk(KERN_DEBUG "%s: IB connection from [%s].%d\n",
			__func__,icsinfo.icsinfo_string,ibc->svc_port);
#endif
	/*
	 * Now read the node number and channel number from the client.
         */
	if (ib_readmsg(ibc, &rxd, 0)) {
		/* receive errors? */
		printk("%s: recv of node number failed, error 0x%x;"
		       " possibly node went down\n", __func__, rxd->status);
		goto err;
	}

	/* Copy the node number */
	memcpy(&rnode, rxd->payload, sizeof(clusternode_t));
	memcpy(&chan, (rxd->payload + sizeof(clusternode_t)), sizeof(int));

	if (rnode <= 0 || rnode > NSC_MAX_NODE_VALUE ||
	    chan < 0 || chan >= ICS_MAX_CHANNELS) {
		printk(KERN_WARNING "%s: Invalid node/channel received %u/%d",
		       					__func__, rnode, chan);
		goto err;
	}

#ifdef ICS_DEBUG
	printk("%s remote node %d chan %d\n",__func__,rnode,chan);
#endif

	/* repost ics channel receive buffer */
	if (kib_post_recv_wr(rxd)) {
		CWARN("Unable to re-post RX buffer?");
	}

	/*
	 * By default the node may join.
	 */
	state = 0;
	state = chk_node_chan_state(state,rnode,chan);

	/* Allocate the overall nodeinfo structure if necessary. */
	ics_llnodeinfo_init(rnode);
	nodeinfo = ics_llnodeinfo[rnode];
	SSI_ASSERT(nodeinfo != NULL);

	/* verify the ics channel connection state */
	if (state == 0)
		goingdown = ics_chan_connect(nodeinfo,rnode,chan,&icsinfo,ibc);

	/*
	 * Allocate and initialize for the status response.
	 */

	/* acquire a TX (transmit) descriptor, send buffer */
        if ((txd=get_txd(ibc,TRUE)) == NULL) {
                CERROR ("ERR: Unable to allocate TX desc from free pool\n");
                goto err;
        }
	/* set message contents - 4-bytes of node 'state' */
	memcpy((void*)txd->payload, &state, sizeof(int));

#ifdef ICS_DEBUG_V
	printk("%s() sending 4 bytes of node chan state 0x%x\n",__func__,state);
#endif
	/* post & wait for send completion */
	rc = ib_sendmsg(txd, sizeof(int));
	put_idle_txd(txd);
	if (rc) {
                CWARN("IBA send to [%s] failed %d\n",
					nid_str(ibc->r_hid.nid),rc);
		goto err;
        }

	/* handle the current state */
	if (state == 1) {
		if (chan == ics_clms_chan)
			printk(KERN_WARNING "%s: Node %d has already joined.\n",
						 __func__, (int) rnode);
		goto err;
	}
	if (state == 2) {
#ifdef ICS_DEBUG
		if (chan == ics_clms_chan) {
			printk(KERN_WARNING "%s: Node %d joining before "
				"nodedown complete.\n", __func__, (int) rnode);
		}
#endif
		goto err;
	}

	/*
	 * If we set the goingdown flag above, just exit this thread here.
	 */
	if (goingdown)
		goto err;

#if MUX_ICS_CHANNELS
	/*
	 * ICS chan 0 forces the creation of a 'real' transport connection.
	 * All other ICS channels are mux'ed onto chan 0's transport connection.
	 * Build/fake all other ICS channels as if they actually connected.
	 */
	if (state == 0) {
#ifdef ICS_DEBUG
		printk("%s MUXing ICS channels [0...%d] to node %d cur_chan %d\n",
				__func__,(ICS_MAX_CHANNELS-1),(int)rnode,chan);
#endif
		for(rc=0; rc < ICS_MAX_CHANNELS; rc++) {
			if ( rc == chan || rc == ics_probe_clms_chan)
				continue;
			if (chk_node_chan_state(state,rnode,rc) != 0) {
				printk("%s BAD state %d node %d chan %d\n",
						__func__,state,rnode,rc);
				goto err;
			}
			if (ics_chan_connect(nodeinfo,rnode,rc,&icsinfo,ibc)) {
				printk("%s BAD chan MUX node %d chan %d\n",
							__func__,rnode,rc);
				goto err;
			}
			/* remove reference from ics_chan_connect(). Keep
			 * reference count accurate all times in case of
			 * error.
			 */
			LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			nodeinfo->icsni_ref_cnt--;
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		}
	}
#endif
	/*
	 * Decrement the reference count now that we are done.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	nodeinfo->icsni_ref_cnt--;
	dowakeup = (nodeinfo->icsni_ref_cnt == 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowakeup)
		SIGNAL_EVENT(&nodeinfo->icsni_event);

	/* thread no longer needed, IB connection persists on Success */
#if ICS_DEBUG && MUX_ICS_CHANNELS
	printk("%s Done MUXing ICS channels [0...%d] to node %d\n",
				__func__,(ICS_MAX_CHANNELS-1),(int)rnode);
#endif
out:
	exit_daemon_thread();
	/* NOTREACHED */
err:
	/* connection no longer needed in the error condition. */
	ib_destroy_connection(ibc);
	goto out;
}

/*
 * ics_llnodeup_connect()
 *	Routine called to make connections when ICS is informed a new node
 *	has joined up (and this is the low-numbered node of the pair).
 *
 * Description:
 *	TCP connections are established for all services to a given
 *	node.
 *
 *	Note that this routine is called only if the node coming up has
 *	a higher node number than the current node. This is done so that
 *	connections are requested in one direction only.
 *
 * Parameters:
 *	rnode	remote node number.
 *
 * Return Value:
 *	0 - success
 *	-EAGAIN - the contacted node was still doing nodedown for us.
 */
static int
ics_llnodeup_connect(clusternode_t rnode)
{
	int		chan;
	int		error = 0;
	ics_llnodeinfo_t *nodeinfo = ics_llnodeinfo[rnode];

	/*
	 * If the node is still going down, wait for it to go completely
	 * down before we process the nodeup.
	 */
	clms_waitfor_nodedown(rnode);
	for (chan=0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		if (chan == ics_probe_clms_chan)
			continue;
		if (nodeinfo->icsni_conn == NULL ||
 		    nodeinfo->icsni_conn[chan] == NULL) {
#if ICS_DEBUG
			printk("%s: connect to node %u chan %d\n", __func__, rnode, chan);
#endif
			error = ics_request_connection(rnode, chan);
			if (error != 0)
				break;
		}
	}
	return(error);
}

/*
 * ics_request_connection()
 *	Establish a connection for a given ICS comm channel to a given node.
 *
 * Description:
 *	A TCP connection is established for a given communication channel
 *	to the given remote node.
 *
 *	On each connection, TCP_NODELAY is set. This disables the Nagle
 *	algorithm (refer to RFC 793/1122). Generally TCP collects packets
 *	until the packet is big enough to send. This is to avoid sily
 *	window syndrome, which can occur when the memory goes very low.
 *	However, in the case of NSC, each packet the upper layer sends
 *	is a datagram and it has to be sent to the other side immediately
 *	regardless of its size.  For example, a request to kill a remote
 *	process is ~200 bytes in size. It has to be sent to the other
 *	side immediately.
 *
 * Parameters:
 *	rnode	  remote node number.
 *	chan	  communication channel number.
 */
static int
ics_request_connection(clusternode_t rnode, int	chan)
{
	int			rc, port, error;
	char			*ipaddr;
	boolean_t		immed_return;
	boolean_t		dowakeup;
	ics_llnodeinfo_t	*nodeinfo = ics_llnodeinfo[rnode];
	char			*ptr;
	int			len;

	u32			ipa;
	ib_conn_t		*ibc;
	kib_rx_t		*rxd;
	kib_tx_t		*txd;

	SSI_ASSERT(ics_llnodeinfo[rnode] != NULL);

	port = ICS_SVC_PORT;
	/*
	 * If the node isn't in the special going-down state, then up
	 * the reference count so that the node can't go down and connections
	 * can't be torn down until this routine spots that it happened and
	 * returns an error to the caller.
	 */

	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return(-EAGAIN);
	}
	nodeinfo->icsni_ref_cnt++;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

#if MUX_ICS_CHANNELS
	/*
	 * All ICS channels are MUX'ed onto a single IB connection. channel(0) 
	 * forces the transport connection creation, all other channels fake an
	 * ICS connection and use chan 0's transport connection.
	 */
	if (chan > 0) {
		ibc = NULL; /* for ics_connect() */
		goto mux_ics_channels;
	}
#endif
	ipaddr = nodeinfo->icsni_node_addr;
	SSI_ASSERT(ipaddr != NULL);

	ipa = in_aton(ipaddr); // cvt ascii -> net order u32

tryagain:

	LOCK_LOCK(&ics_llconn_lock);

	/*
	 * Connect to the node. If this fails due to the fact that the node
	 * is going down, make sure that we decrement the reference count
	 * appropriately.
	 */
	if ((rc=ib_connect(ipa, port, &ibc))) {
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
			nodeinfo->icsni_ref_cnt--;
			dowakeup = (nodeinfo->icsni_ref_cnt == 0);
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if (dowakeup)
				SIGNAL_EVENT(&nodeinfo->icsni_event);
			rc = -EAGAIN;
			immed_return = TRUE;
		} else {
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			printk(KERN_WARNING
				"%s : connect to [%s] error %d\n", __func__,
								ipaddr, rc);
			immed_return = FALSE;
		}
		UNLOCK_LOCK(&ics_llconn_lock);
		if (immed_return)
			return(rc);
		idelay(ICS_CONNECT_DELAY * HZ);
		goto tryagain;
	}
	UNLOCK_LOCK(&ics_llconn_lock);

	/*
	 * Send the node number across. This may not be needed
	 * in the future, if we avoid race conditions between
	 * connection establishment and the clms master announcement.
	 * NSC_XXX.
	 *
	 * Note that we need to decrement the reference count if the
	 * send fails due to the node going down.
	 */

	/* acquire a TX (transmit) descriptor & buffer */
        if ( (txd=get_txd(ibc,TRUE)) == NULL ) {
                CERROR ("ERR: Unable to allocate free pool TX desc?\n");
		SSI_ASSERT( txd );
        }
	ptr = txd->payload;

	/* send 'node_num:chan' */
	len = (__kernel_size_t)(sizeof(clusternode_t)+sizeof(int));
	memcpy(ptr, &this_node, sizeof(clusternode_t));
	memcpy(ptr+sizeof(clusternode_t), &chan, sizeof(int));

	rc = ib_sendmsg(txd, len);
	put_idle_txd(txd);
	if (rc) {
                CWARN("IB send err(%d) to [%s]\n",rc,nid_str(ibc->r_hid.nid));
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
			nodeinfo->icsni_ref_cnt--;
			dowakeup = (nodeinfo->icsni_ref_cnt == 0);
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if (dowakeup)
				SIGNAL_EVENT(&nodeinfo->icsni_event);
			ib_destroy_connection(ibc);
			return(-EAGAIN);
		}
		else {
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		}
		printk("%s: Sending node number and channel "
			"number failed, error %d",__func__,rc);
		ib_destroy_connection(ibc);
		idelay(ICS_CONNECT_DELAY * HZ);
		goto tryagain;
	}

	/*
	 * Get the response from the master.  This tells us if we can
	 * continue.  It's a four-byte (int) value, either zero (success),
	 * one (the node number already exists) or two (he's still doing
	 * nodedown for the node).
         */

	if ( (rc=ib_readmsg(ibc,&rxd,8)) ) {
		printk(KERN_WARNING "%s: recv of status failed, error %d\n",
			__func__, rc);
		ib_destroy_connection(ibc);
		idelay(ICS_CONNECT_DELAY * HZ);
		goto tryagain;
	}
	if (rxd->nob != sizeof(int)) {
		CWARN("recieved %d bytes, expected %d?\n",rxd->nob,sizeof(int));
	}
	memcpy(&error, rxd->payload, sizeof(int));

	if (error == 1) {	/* Our node number is already taken. */
		ib_destroy_connection(ibc);
		idelay(10*HZ);	/* Delay for a while to let things settle. */
		panic("Node number %d already taken!\n", (int) this_node);
	}
	if (error == 2) {	/* He's still in nodedown for this node. */
#if ICS_DEBUG
		printk(KERN_WARNING "%s:  We're joining before our nodedown "
			"was completed?\n",__func__);
#endif
		ib_destroy_connection(ibc);
		/*
		 * Drop the ref count we put on earlier.
		 */
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		nodeinfo->icsni_ref_cnt--;
		dowakeup = (nodeinfo->icsni_ref_cnt == 0);
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (dowakeup)
			SIGNAL_EVENT(&nodeinfo->icsni_event);
		return(-EAGAIN);
	}

	/*
	 * Connection established from initiator's side. Set up the
	 * correct data structures.
	 */
#if MUX_ICS_CHANNELS
mux_ics_channels:
#endif
	(void) ics_connect(ibc, rnode, chan);

	/*
	 * Decrement the reference count now that we are done.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	nodeinfo->icsni_ref_cnt--;
	dowakeup = (nodeinfo->icsni_ref_cnt == 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowakeup)
		SIGNAL_EVENT(&nodeinfo->icsni_event);

	return 0;
}


/*
 * ics_connect()
 *	Set up the data structures for a particular node/channel connection.
 *
 * Description:
 *	This routine is called by either ics_accept_connection() or
 *	ics_request_connection() once a connection has been established.
 *
 *	It sets up the data structures necessary for a connection and
 *	performs other configuration as necessary.
 *
 * 	Once the connection has been set up, the socket's data_ready
 * 	callback routine is replaced by the NSC ICS specific callback
 * 	"ics_dgram". This is so that NSC datagrams can be reconstructed
 * 	efficiently. Note that the ICS server daemons do not call the
 * 	conventional sock_recvmsg() to receive incoming ICS datagrams.
 *
 * Parameters:
 *	conn		pointer to IB connection struct
 *	rnode		node number of the remote node.
 *	channel		communication channel number.
 *
 * Return Values:
 *	0 on success and an error number on failure.
 *
 */
static int
ics_connect(ib_conn_t *ib_conn, clusternode_t rnode, int chan)
{
	ics_ib_conn_data_t	*conn;
	ics_ib_conn_data_t	**connp;
	int			error=0,len;
	ics_llnodeinfo_t	*nodeinfo = ics_llnodeinfo[rnode];
	char			cname[30];

#if X_ICS_DEBUG
	printk("%s ics chan %d %s to node %d\n",
		__func__,chan,(ib_conn ? "Connect":"MUX"),rnode);
#endif
#if MUX_ICS_CHANNELS
	if (ib_conn == NULL) {
		/* ICS chan 0 creates a transport connection, all other ICS
		 * channels use the same transport connection. Get ICS channel
		 * zero's transport connection pointer.
		 */
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
 		conn = nodeinfo->icsni_conn[0];
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		SSI_ASSERT(conn);
		ib_conn = conn->conn_ib;
	}
#endif
	/* append ics channel # to remote host's name for debug */
	if (chan == 0) {
#if MUX_ICS_CHANNELS
		len = sprintf(cname,".chan[0..%d].port(%d)",
				(ICS_LL_NUM_CHANNELS-1), ib_conn->svc_port);
#else
		len = sprintf(cname,".chan(0).port(%d)",ib_conn->svc_port);
#endif
		if ((ib_conn->r_hid.hostname_len+(u8)len) < MAX_HOSTNAME_SIZE) {
			strcat(ib_conn->r_hid.hostname,cname);
			ib_conn->r_hid.hostname_len += (u8)len;
		}
	}
	conn = kzmalloc_nofail(sizeof(ics_ib_conn_data_t));
	SSI_ASSERT(conn != NULL);

	conn->conn_node = rnode;
	conn->conn_chan = chan;
	conn->conn_ib = ib_conn;

	ib_conn->rmq_receive = FALSE;	/* enable ics_dgram() recv's */
	ib_conn->ics_data = conn;	/* back link to ics data, chan, node */

	conn->conn_snd_rand_statep = rand_initialize_ics(GFP_KERNEL);
	conn->conn_rcv_rand_statep = rand_initialize_ics(GFP_KERNEL);
	INIT_ATOMIC_INT(&conn->conn_ref_cnt, 0); /* count of number of */
					         /* processes throttled */
	INIT_LOCK(&conn->conn_lock);

	/*
	 * Allocate space for the connection structures if necessary.
	 */
	SSI_ASSERT(nodeinfo != NULL);
	if (nodeinfo->icsni_conn == NULL) {
		connp = kzmalloc_nofail(sizeof(connp) * ICS_LL_NUM_CHANNELS);
		SSI_ASSERT(connp != NULL);
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (nodeinfo->icsni_conn == NULL)  {
			nodeinfo->icsni_conn = connp;
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		} else {
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			kfree(connp);
		}
	}

	/*
	 * Set up the connection, and mark the node as coming up.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
 	nodeinfo->icsni_conn[chan] = conn;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	BROADCAST_EVENT(&nodeinfo->icsni_chan_event);

	if (error) printk("%s exit %d\n",__func__,error);

	return error;
}

/*
 * ics_llwaitforallconnections()
 *	Wait for connections to be established for ALL services to a node.
 *
 * Description:
 *	This function waits for connections to be established
 *	for all services to a given node.  This is not very
 *	efficient, if there are problems in establishing connections.
 *	Also, this function will have to change when node down conditions
 *	are handled (NSC_XXX).
 *
 * Parameters:
 *	rnode		remote node number.
 *
 * Return value:
 *	Zero if all connections successfully established, -EAGAIN if the
 *	node went down before connections could be established.
 */
static int
ics_llwaitforallconnections(
	clusternode_t	rnode)
{
	int		chan;
	boolean_t	allchans;
	boolean_t	dowakeup;
	ics_llnodeinfo_t *nodeinfo = ics_llnodeinfo[rnode];

	/*
	 * If the node isn't in the special going-down state, then up
	 * the reference count so that the node can't go down and connections
	 * can't be torn down until this routine spots that it happened and
	 * returns an error to the caller.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return(-EAGAIN);
	}
	nodeinfo->icsni_ref_cnt++;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	/*
	 * Go through all the connections looking for them all to be
	 * set up. If we find one that is not set up, we check if the
	 * node is going down. If it is not going down, we then wait
	 * a while and then retry the loop from the top.
	 */
	do {
		allchans = TRUE;
		for (chan = 0; chan < ICS_LL_NUM_CHANNELS; chan++) {
			if (chan == ics_probe_clms_chan)
				continue;
			if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
				LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
				nodeinfo->icsni_ref_cnt--;
				dowakeup = (nodeinfo->icsni_ref_cnt == 0);
				UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
				if (dowakeup)
					SIGNAL_EVENT(&nodeinfo->icsni_event);
				return(-EAGAIN);
			}
 			if (nodeinfo->icsni_conn == NULL ||
			    nodeinfo->icsni_conn[chan] == NULL) {
				allchans = FALSE;
				idelay(HZ/100);
				break;
			}
		}
	} while (!allchans);

	/*
	 * Decrement the reference count now that we are done.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	nodeinfo->icsni_ref_cnt--;
	dowakeup = (nodeinfo->icsni_ref_cnt == 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowakeup)
		SIGNAL_EVENT(&nodeinfo->icsni_event);

	return(0);
}

int
ics_llshoot_node(clusternode_t	remote_node)
{
	ics_shoot_node_t shoot_node;
	int error;

	shoot_node.my_ics_version = ics_message_version;
	shoot_node.i_am_cluster = (clms_master_node != CLUSTERNODE_INVAL);
	do {
		error = ics_llunack_send(remote_node, ICS_UNACK_SHOOT_NODE,
					 &shoot_node, sizeof(shoot_node));
		if (error == -ENOMEM) {
			idelay(HZ/2);
		}
	} while (error == -ENOMEM);
	if (error != 0)
		printk(KERN_WARNING
			"%s: error %d in ics_llunack_send", __func__, error);
	return(B_TRUE);
}

int
ics_llget_nodenumsize(void)
{
	return ICS_NODENUMSIZE;
}

int
ics_llget_addrlen(void)
{
	return ICS_IPADDRLEN;
}

void
ics_llnode_to_icsinfo(icsinfo_t *infop, clusternode_t node)
{
	(void) ics_llgeticsinfo(node, infop);
}


/*
 * RDMA operation timeout handler. Fires when the remote side has not sent an
 * RDMA status message within the allocated time. Used to prevent the sending
 * side from hanging. This timeout handler will set an error status and wake
 * the waiting sender; see ics_llsend().
 */
void
ib_ool_rdma_timeout(ulong data)
{
        kib_tx_t *txd = (kib_tx_t*)data;

	printk("%s txd %p conn %p force sender wakeup.\n",
				__func__,txd,txd->conn);

	/*
         * The RDMA.read side did not send an RDMA status message within the
	 * timeout period? Assume remote node failure and cancel this
	 * icscli_send() or icssvr_llreply() RDMA wait. Set the Infiniband
	 * work-complete (WC) error & wake the waiting send/reply thread.
	 */
        txd->status = IB_WC_RESP_TIMEOUT_ERR;
	*(int*)(txd->private) = -ETIME;
	wake_up(&txd->conn->waitQ);
}

/*
 * send an ics message from the specified msg iovec. Routine called from
 * icscli_sendmsg() or icssvr_llsendmsg().
 *
 * An IB message is message is formatted and transmitted. Input iov data
 * segments are copied into the IB message and sent. If the iov data segment
 * size will not fit in the remaining IB message, then an RDMA descriptor is
 * sent in the IB message; the receiver decode_ool_data_t routine will recognize
 * the RDMA descriptor and RDMA.read (pull) the data.
 *
 * IB message format:
 *  ICS header : ICS inline data : IB_msg_info : [IB_msg_ool_desc : data]...
 *
 * The ICS header and inline data are formatted the same as ics_tcp.
 *
 * The IB_msg_info blk contains the number of IB_msg_ool_descriptors contained
 * in this message, the # of RDMA operations required to completely receive this
 * message and the address of the sending transmit descriptor (txd). The txd
 * address is returned to the waiting sender in an RDMA.read status message in
 * order for the waiting sender (see our caller) to be awakened on an error or
 * when the required number of RDMA.read operations have been completed by the
 * receiver.
 *
 * Each iov data segment has a corrosponding IB_msg_ool descriptor in the IB
 * message proper. The descriptor contains the iov data length (bytes).
 * If the data length is positive, then the iov data is copied into the IB
 * message following the 4 bytes of iov data length.
 * If the iov data will not fit in the remaining space in the IB message, an
 * RDMA.read op by the receiver is required to pull the data. The iov data
 * length is encoded as a negative length, with the following 12 bytes
 * constititing RDMA.read info for the receiver (see ib_ool_desc_t).
 *
 * Inputs:
 *  *connp	ics ib data - not the IB connection itself (see connp->conn_ib)
 *  *msgp	message/iov to transmit.
 *  size	total ics message bytes - does not include IB OOL descriptor
 *		bytes.
 *  ib_rmem	[out] vector of IB registered memory region (DMA) descriptors
 *		which represent iov data segments which would not fit in the
 *		IB msg, hence will be RDMA.read by a remote node. This DMA
 *		segment descriptor is used to IB/DMA unmap after the RDMA
 *		operation has completed.
 *  rdma_cnt_p	address of expected RDMA operation count.
 *  txd_p	Transmit Descriptor pointer to wait on RDMA operation
 *		completion. Only set in the case of when RDMA operations
 *		are required (return vale > 0).
 * Return Value:
 *	>=0 implies success, > 0 value is the number of expected RDMA.read
 *		operations for OOL data segments; expect RDMA.read status msg.
 *	else (-)errno.
 * Notes:
 *	size == number-of-ics-data-bytes to send, actual IB message transmited
 *	size can be larger due to IB ool in-msg data with optional RDMA
 *	descriptors.
 */

int
ics_llsendmsg( ics_ib_conn_data_t *connp,
               struct msghdr      *msgp,
               int                size,
               ib_rdma_seg_t	  *ib_rmem,
               int                *rdma_cnt_p,
               struct _kib_txd    **txd_p )
{
	ib_conn_t	*ibc = connp->conn_ib;
	kib_tx_t	*txd;
	int		rc,nob,rdma_cnt;
	int		niov;
	int		cpy=FALSE;
	ulong		laddr;
	ib_ool_info_t	*ib_ool_info;
	struct iovec	*iov;
	ib_ool_desc_t	*iod;
	icshdr_t	*ihdr_p;
	char		_err[16];

	if (BAD_MAGIC(ibc)) {
		printk("%s() BAD Magic on conn %p\n",__func__,ibc);
		return -EIO;
	}
	if (IB_GET_CONN_STATE(ibc) != IBA_CONNECTED) {
		printk("%s() !connected? conn %p\n",__func__,ibc);
		return -ENOTCONN;
	}

	iov = msgp->msg_iov; /* ICS header & inline data in iov[0] */
	niov = msgp->msg_iovlen;
	ihdr_p = (icshdr_t*)iov[0].iov_base;

	/* will ics message + IB info+ool descriptors fit in this IB message
	 * payload or will RDMA(s) be required ?
	 * 'sizeof int' is all thats used from an ib_ool_desc_t when the data
	 * is copied into the IB message.
	 */
	rc = sizeof(ib_ool_info_t) + (niov * sizeof(int)) + size;
	if (rc < IBA_MAX_BUF_SIZE)
		cpy = TRUE;

	dprintk(DBG_ICS,("%s nob %d, %s data, inline sz %d\n",__func__,size,
			(cpy ?"Copy":"RDMA"),ihdr_p->ihdr_inline_len));

	if (ihdr_p->ihdr_inline_len > ICS_MAX_INLINE_DATA_SIZE) {
		CWARN("inline len %d >  max_inline %d?\n",
			ihdr_p->ihdr_inline_len,ICS_MAX_INLINE_DATA_SIZE);
		return -E2BIG;
	}

	/* ICS message stats */
	if (ihdr_p->ihdr_inline_len > ibc->icsm_inline_max)
		ibc->icsm_inline_max = ihdr_p->ihdr_inline_len;

	if (ihdr_p->ihdr_inline_len < ibc->icsm_inline_min)
		ibc->icsm_inline_min = ihdr_p->ihdr_inline_len;
	ibc->icsm_cnt++;
	ibc->icsm_inline_bytes += ihdr_p->ihdr_inline_len;

	/* acquire a TX (transmit) descriptor & payload buffer */
        if ( (txd=get_txd(ibc,TRUE)) == NULL ) {
               	CERROR ("ERR: Unable to allocate free pool TX desc?\n");
		rc = -ENOMEM;
		goto errout;
        }
	*rdma_cnt_p = 0;	// no RDMA OOL segs yet.

	/* copy ICS header & inline data */
	memcpy((void*)txd->payload, iov->iov_base, iov->iov_len);

	ihdr_p = (icshdr_t*)txd->payload;

	/* update # of message bytes & payload space avail.
	 * iov[0] is the ics header + inline data. Include the IB ool info blk.
	 */
	nob = iov->iov_len + sizeof(ib_ool_info_t);

	/* point at IB ool info which follows inline data. The RDMA count
	 * address is returned in the RDMA status message and points to an
	 * 'int' within the llcli_handle. The int is a count of expected RMDA
	 * operations (RDMA.read of ool data). In our callers routine,
	 * if the RDMA count is > 0 then the thread will wait for the count to
	 * drop to zero or less-than 0 being error case. A single rdma_status
	 * message will be sent from the server containing the cumulative RDMA
	 * operation status & count. A status message represents 'n' successful
	 * RDMA.read operations. If 'n' from the status message does not match
	 * the ISC handle expected rdma count, then there have been errors.
	 * In the case the status message contains an error code (!=0) then the
	 * rdma count from the status message represents the number of RDMA ops
	 * which completed successfully, thereby allowing one to determine
	 * which RDMA.read operation failed.
	 */
	ib_ool_info = (ib_ool_info_t*) (txd->payload + iov->iov_len);
	ib_ool_info->txd = NULL;
	ib_ool_info->rdma_ool_desc_cnt = 0;
	ib_ool_info->ool_desc_cnt = msgp->msg_iovlen - 1; // skip inline data

	/* point at 1st IB ool descriptor, follows inline data */
	iod = (ib_ool_desc_t*) (ib_ool_info + 1);

	/* build IB ool descriptors - copy OOL data inline or RDMA.read setup */
        for (rdma_cnt=0,iov++; --niov > 0;  iov++) {
		int sz;

		sz = iov->iov_len + sizeof(int);

		/* will this iov data + ool desc(length only) fit in the IB
		 * message payload buffer?
		 */
		if (cpy) {
			iod->len = iov->iov_len;
			/* data will fit - copy into IB message */
                	memcpy(iod->ool_data, iov->iov_base, iov->iov_len);
                	nob += sz;
			/* advance over ib ool desc + data to next desc */
			iod = (ib_ool_desc_t*)(((u_char*)iod) + sz);
		}
		else if (((nob+sz+((niov-1)*sizeof(ib_ool_desc_t))) <= IBA_MAX_BUF_SIZE)) {
				iod->len = iov->iov_len;
				/* data will fit - copy into IB message */
                		memcpy(iod->ool_data, iov->iov_base,
								iov->iov_len);
                		nob += sz;
				/* advance over ib ool desc+data to next desc */
				iod = (ib_ool_desc_t*)(((u_char*)iod) + sz);
		}
		else {
			/*
			 * iov data will not fit in remaining free space of
			 * IB message. Setup an RDMA.read so the server decode
			 * rtn can RDMA.read/pull the OOL data.
			 */
	                if ((nob + sizeof(ib_ool_desc_t)) > IBA_MAX_BUF_SIZE) {
	                        printk("%s no IB msg space for RDMA OOL desc\n",
	                                			__func__);
	                        printk("%s   rdma_cnt %d iovs(done %d todo %d) "
					"Tsize %d nob %d cpy %d\n",
					__func__,rdma_cnt,
					(msgp->msg_iovlen-niov),niov,size,nob,
					cpy);
	                        rc = -E2BIG;
				goto errout;
	                }

	                /* setup so receiver can RDMA.read the OOL data */
			if (rdma_cnt > 1) {
	                	dprintk(DBG_RDMA,
				("%s RDMA.read setup (iov.%p sz %d) free %d\n",
				__func__,iov->iov_base,iov->iov_len,
				(IBA_MAX_BUF_SIZE-nob)));
			}

			iod->len = -1 * (int)iov->iov_len; /* < 0 sez RDMA op */
			iod->ool_magic = 0xcafebabe; // XXX

			/* setup can change src buffer address which is released
			 * by our caller, after the RDMA.reads are completed.
			 */
	                rc = ib_rdma_setup((u_char*)iov->iov_base, iov->iov_len,
					   DMA_TO_DEVICE,
					   &laddr,
					   ib_rmem);
	                if (rc) {
	                        CWARN("RDMA.read setup err %d\n", rc);
	                        rc = -EPERM;
				goto errout;
	                }
			/* set HCA address for use by remote side */
			iod->ool_rdma_addr = (u64) laddr;

			/* IB remote access key */
			iod->ool_rkey = ib_rmem->mr->rkey;

	                rdma_cnt++;	/* one more RDMA operation */
			if (rdma_cnt > MAX_RDMA_READ_OPS) {
				CWARN("too many RDMA.read ops, max %d\n",
							MAX_RDMA_READ_OPS);
	                        rc = -EPERM;
				goto errout;
			}
#if CRC
			iod->ool_rdma_crc = crc32(0,iov->iov_base,iov->iov_len);
#endif
			ibc->rdma_src_bytes += iov->iov_len; // track RDMA 
			ibc->rdma_src_op++; // track RDMA 
			if ( (int)iov->iov_len < ibc->min_rdma_bytes )
				ibc->min_rdma_bytes = (int)iov->iov_len;
			if ( (int)iov->iov_len > ibc->max_rdma_bytes )
				ibc->max_rdma_bytes = (int)iov->iov_len;

			ib_rmem++;	/* next IB rdma segment */
                	nob += sizeof(ib_ool_desc_t);
			iod++;		/* next IB ool desc */
		}
        }

	/* update count of expected RDMA.read ops. Keep two counts as by the
	 * time this routine returns to our caller, some of the RDMA.read ops
	 * could have occured thus (*rdma_cnt) could be zero. Return the
	 * expected RMDA op count.
	 */
	if (rdma_cnt > 0) {
		*rdma_cnt_p = rdma_cnt;
		ib_ool_info->txd = txd;
		ib_ool_info->rdma_ool_desc_cnt = rdma_cnt;

		/* During the send CQ entry processing, if 'txd->private' is
		 * non-null, then the successfully sent message requires RDMA
		 * read-op(s) from the receiver in order to get all the OOL
		 * data; OOL data would not fit in current IB message payload.
		 * IB send completion processing will set a timer on the RDMA
		 * transactions in order to escape from failed server RDMA ops
		 * so this thread does not wait forever on RDMA reads which may
		 * not occur. Record the expected rdma cnt adrs so the timeout
		 * routine can set the wait value (<0) and then wake the waiting
		 * sender for the error condition.
		 */
		txd->private = rdma_cnt_p;
		if (rdma_cnt > ibc->max_rdma_per_msg)
			ibc->max_rdma_per_msg = rdma_cnt;// monitor RDMAs/IB msg
	}

	/* post IB send request & wait for send completion */

	if ((rc = ib_sendmsg(txd,nob)) == SUCCESS) {
		if (rdma_cnt > 0) {
			/* return txd to higher-layer to wait for RDMA
			 * completion(s).
			 */
			*txd_p = txd;
		}
		else {
			/* successful IB send - no RDMA OOL data hence no
			 * reason to keep txd.
			 */
			put_idle_txd(txd);
		}
		dprintk(DBG_ICS,("%s ret %d\n",__func__,rdma_cnt));
		return rdma_cnt;
	}

	/* IB send error condition */

	if (rdma_cnt > 0) {
		/* cancel RDMA wait timeout */
		del_timer_sync(&txd->timeout_timer);
		txd->timeout_timer.expires = 0;
	}
	sprintf(_err,"errno(-%d)",abs(rc));
	printk("%s send(%d bytes %d rdma ops) err(%d) '%s' to [%s] chan %d\n",
		__func__,nob,rdma_cnt,rc,
		(rc > 0 ? ib_get_wc_status_str(rc):_err),
		nid_str(ibc->r_hid.nid),ihdr_p->ihdr_chan);
	rc = -EIO;
errout:
	put_idle_txd(txd);

	if ((ics_llnodeinfo[connp->conn_node]->icsni_flag &
		     (ICSNI_GOING_DOWN | ICSNI_NODE_DOWN)) == 0) {
			printk(KERN_WARNING
			       "%s: rnode %u chan %d, error -(%d)\n", __func__,
				connp->conn_node, ihdr_p->ihdr_chan, (-1*rc));
	}
	return rc;
}

