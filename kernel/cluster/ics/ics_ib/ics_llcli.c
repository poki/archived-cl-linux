/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
 */

/*
 * 	Low-level ICS client code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
//#define ICS_DEBUG 1

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/uio.h>
#include <asm/uaccess.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */
#include <cluster/log.h>

#ifdef NSC_ICSLOG
const char *icslog_llformat[] = {
	"%lx-llcsnd : id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-llssnd : id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-dgmsmal: n %ld c %ld len %ld icshdr_len %ld\n",
	"%lx-dgmhdrl: n %ld c %ld len %ld ihdr_len %ld id 0x%lx/0x%04lx\n",
	"%lx-dgminc : n %ld c %ld len %ld ihdr_len %ld id 0x%lx/0x%04lx\n",
	"%lx-dgmmult: n %ld c %ld len %ld ihdr_len %ld id 0x%lx/0x%04lx\n",
	"%lx-dgmupcl: n %ld c %ld id 0x%lx/0x%04lx sp 0x%lx dp 0x%lx\n",
	"%lx-llsnd  : n %ld c %ld s %s\n",
	"%lx-llseq  : id 0x%lx/0x%04lx n %ld c %ld m %ld f %ld\n",
	"%lx-llsdq  : id 0x%lx/0x%04lx n %ld c %ld m %ld l %ld\n",
	"%lx-llcsndu: id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-llspmsg: id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-llsndd : id 0x%lx/0x%04lx n %ld c %ld m %ld l %ld\n",
};

#endif /* NSC_ICSLOG */

#define ICS_CLI_COPY_OOLDATA

/*
 * icscli_llinit()
 *	Perform relevant initialization for the low-level client code.
 */
void
icscli_llinit()
{
#if defined (NSC_ICSLOG)
	int i;
	int j;

	for (i = 0; i < NUM_ENTRIES(icslog_llformat); i++) {
		j = ICSLOG_LLICSOFF + i;
		if (j >= ICSLOG_NUM_MSGS)
			break;
		icslog_format[j] = icslog_llformat[i];
	}
#endif /* NSC_ICSLOG */
}

/*
 * icscli_llhandle_init()
 *	Initialize the low-level ICS portion of a client handle
 *
 * Description:
 *	This routine is called by ICS high-level code to initialize the
 *	portion of the handle that is specific to ICS low-level code.
 *
 *	This routine can assume that, if the handle is being re-used,
 *	that icscli_llhandle_deinit() has been called.
 *
 *	This routine cannot assume that the low-level portion of the client
 *	handle has been initialized to all zeroes.
 *
 *	This routine may be called from interrupt context (if flag is FALSE).
 *
 * Parameters:
 *	handle		the handle to be initialized
 *	flag		if TRUE, this routine can sleep waiting for resources
 *
 * Return value:
 *	If sleep_flag is TRUE, this routine always returns 0 (success). If flag
 *	is FALSE, then this routine returns 0 (success) if it managed to
 *	initialize the handle without sleeping, and -EAGAIN if it would
 *	have had to go to sleep.
 */
int
icscli_llhandle_init(cli_handle_t *handle, boolean_t sleep_flag)
{
	struct iovec	*iov;
	struct msghdr	*msg;

	(void)sleep_flag;
	msg = &handle->ch_msghdr;
	iov = handle->ch_iovec;
	iov[0].iov_base = handle->ch_inline;
	iov[0].iov_len = sizeof(icshdr_t);
	msg->msg_name = NULL;
	msg->msg_namelen = 0;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = 0;
	msg->msg_iov = iov;
	msg->msg_iovlen = 1;
	handle->ch_msg_msghdr_p = msg;
	handle->ch_msg_inline_iov_p = iov;
	handle->ch_msg_inline_data_p = iov[0].iov_base + sizeof(icshdr_t);
	handle->ch_msg_ooldesc_p = handle->ch_ooldesc;
	handle->ch_msg_ooldesc_cnt = 0;
	handle->ch_rep_rxd = NULL;
	handle->ch_rep_inline_data_p = NULL;
	handle->ch_rep_ib_ool_info =NULL;
	handle->ch_rep_ib_od = NULL;
	handle->ch_rep_rdma_cnt = 0;
	handle->ch_msg_len = 0;
	handle->ch_llflag = 0;

	/*
	 * 2 references in case this turns out to be an RPC:
	 * if it is, it will be added to the icscli handle list
	 * and needs the extra reference to avoid problems with nodedown.
	 */
	handle->ch_ref_cnt = 2;

	/* Initialize the lock. */
	INIT_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
//printk("%s handle %p\n",__func__,handle);
	return(0);
}

/*
 * icscli_llencode_inline()
 *
 * Description:
 *	Put data into the in-line buffer as a byte stream.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */
void
icscli_llencode_inline(
	cli_handle_t	*handle,
	const void	*param,
	int		size)
{
	if (size <= 0)
		return;
#ifdef DEBUG_TOOLS
	SSI_ASSERT(handle->ch_msg_inline_data_p);
	if ((void *)handle->ch_msg_inline_data_p + size >
	    handle->ch_msg_inline_iov_p[0].iov_base +
	    sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE)
		panic("%s:inline data size exceeded\n", __func__);
#ifndef CONFIG_X86_4G
	if ((unsigned long)param < TASK_SIZE)
		panic("%s:user address %p not allowed\n", __func__, param);
#endif
#endif
	/* Stuff the argument into the in-line buffer. */
	bcopy(param, handle->ch_msg_inline_data_p, size);
	handle->ch_msg_inline_data_p += size;
        handle->ch_msg_inline_iov_p->iov_len += size;
}

static void
icscli_llhandle_realloc(cli_handle_t *handle, int check_ool)
{
	struct iovec	*curiov;
	struct iovec	*newiov;
	ooldesc_t	*curool;
	ooldesc_t	*newool;
	int curlen;
	int newlen;

	curlen = handle->ch_msg_msghdr_p->msg_iovlen;
	SSI_ASSERT(curlen >= ICS_DEF_OOL_SEGMENTS + 1);
	if ((curlen - (ICS_DEF_OOL_SEGMENTS + 1))
	    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
		newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
		curiov = handle->ch_msg_msghdr_p->msg_iov;
		newiov = kmalloc_nofail(newlen * sizeof(*newiov));
		memcpy(newiov, curiov, curlen * sizeof(*newiov));
		handle->ch_msg_msghdr_p->msg_iov = newiov;
		handle->ch_msg_inline_iov_p = newiov;
		if (curiov != handle->ch_iovec)
			kfree(curiov);
	}
	if (check_ool) {
		curlen = handle->ch_msg_ooldesc_cnt;
		if ((curlen - ICS_DEF_OOL_SEGMENTS)
		    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
			newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
			curool = handle->ch_msg_ooldesc_p;
			newool = kmalloc_nofail(newlen * sizeof(*newool));
			memcpy(newool, curool, curlen * sizeof(*newool));
			handle->ch_msg_ooldesc_p = newool;
			if (curool != handle->ch_ooldesc)
				kfree(curool);
		}
	}
}

/*
 * icscli_llencode_ool_data_t()
 *	Set up an OOL buffer with the specified data.
 *
 * Description:
 *	Set up an OOL buffer for an input parameter, with the buffer containing
 *	the specified data.  Called by ICS high-level code to perform argument
 *	marshalling for client/server interaction.
 *
 *	For the TCP implementation, if ICS_CLI_COPY_OOLDATA is not defined,
 *	in order to avoid extra data copies, an esballoc()-type mblk is set
 *	up with the OOL data, and chained to the existing mblk's.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data; data should not be de-allocated until
 *			either the callback routine is called or the message
 *			is sent
 *	len		length (in bytes) of the data
 *	callback	routine that is called after the data is no longer
 *			required; can be NULL; if used, it is typically used
 *			for data de-allocation; callback is called with
 *			two parameters: param (from above) as first parameter
 *			and callarg (below) as 2nd parameter
 *	callarg		2nd parameter to callback routine (if callback routine
 *			is called)
 *
 * NSC_XXX: Earlier implementations required clumsy 2-level indirection for
 * calling back the freeing routine. This can and needs to be simplified.
 *
 */
int
icscli_llencode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*param,
	long		len,
	void		(*callback)(
				u_char		*param,
				long		callarg),
	long		callarg)
{
	ooldesc_t	*ool_p;
	struct iovec	*iov;
	int		i;

	/* No data? */
	if (len <= 0) {
		/*
		 * There's nothing to encode, so we're "finished" with the
		 * buffer.
		 */
		if (callback != NULL)
			callback(param, callarg);
		return(0);
	}
#ifdef DEBUG_TOOLS
#ifndef CONFIG_X86_4G
	if ((unsigned long)param < TASK_SIZE)
		panic("%s:user address %p not allowed\n", __func__, param);
#endif
#endif

	/* Make sure there is room */
	if (handle->ch_msg_msghdr_p->msg_iovlen >= ICS_DEF_OOL_SEGMENTS + 1) {
		icscli_llhandle_realloc(handle, (callback != NULL));
	}

	/* Just point the iovec at the client data. */
	handle->ch_ref_cnt++;
	i = (handle->ch_msg_msghdr_p->msg_iovlen)++;
	iov = &handle->ch_msg_msghdr_p->msg_iov[i];
	iov->iov_base = param;
	iov->iov_len = len;
	/*
	 * If appropriate, set up the buffer freeing routine to be called.
	 */
	if (callback != NULL) {
		i = (handle->ch_msg_ooldesc_cnt)++;
		ool_p = &handle->ch_msg_ooldesc_p[i];
		ool_p->ool_cli_handle = handle;
		ool_p->ool_callback_data_t = callback;
		ool_p->ool_callparam = param;
		ool_p->ool_callarg = callarg;
	}
	return(0);
}

/*
 * icscli_llencoderesp_ool_data_t()
#ifdef SNET
 *	Prepare for extraction of an OOL output parameter into the specified
 *	area
 *
 * Description:
 *	Prepare for extraction of an OOL output parameter for the specified
 *	handle into the user-provided buffer area. Called by ICS high-level
 *	code to perform argument marshalling for client/server interaction.
 *
 *	This routine must be called prior to icscli_llsend(). Note that
 *	icscli_lldecode_ool_data_t() must also be called for each OOL
 *	output parameter.
 *
 *	This routine must be called from thread context.
 *
 *	Note that for the TCP implementation there is nothing to do done.
 *	For implementations where the OOL output parameter is pushed across
 *	using DMA, then some setup may be required here (e.g. setting up
 *	some pointers in the in-line buffer).
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data; must not be NULL
 *	len		length (in bytes) of the data
#endif
 */
int
icscli_llencoderesp_ool_data_t(
	cli_handle_t	*handle,
	u_char		*param,
	long		len)
{
	/* Do Nothing. */
	return(0);
}

int
icscli_llencode_ool_struct_msghdr(
	cli_handle_t *handle,
	struct msghdr *msg,
	int rwflag)
{
	__typeof__ (msg->msg_iovlen) i;

	/*
	 * Encode the iovecs in msghdr; the free routine frees the entire
	 * msghdr.
	 */
	icscli_llencode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));
	icscli_llencode_ool_data_t(handle, (u_char *)msg->msg_iov,
				   msg->msg_iovlen * sizeof(msg->msg_iov[0]),
				   freemsghdr_callback, (long)msg);
#ifdef ICS_DEBUG
	printk("%s: msg->msg_iovlen : %d\n",
		__func__, (int)msg->msg_iovlen);
#endif /* ICS_DEBUG */

	/*
	 * Encode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef ICS_DEBUG
		printk("%s: msg->msg_iov[%d].iov_len : %d\n",
		       __func__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* ICS_DEBUG */
		icscli_llencode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   msg->msg_iov[i].iov_len,
					   NULL, (long)NULL);
	}

	return (0);
}

int
icscli_lldecode_ool_struct_msghdr(
	cli_handle_t *handle,
	struct msghdr **mpp,
	int rwflag)
{
	struct msghdr *msg = *mpp;
	__typeof__ (msg->msg_iovlen) i;
	int alloclen;

	if (msg == NULL)
		*mpp = msg = kzmalloc_nofail(sizeof(*msg));

	/*
	 * Decode the number of iovecs in msghdr.
	 */
	icscli_lldecode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));
	alloclen = msg->msg_iovlen * sizeof(msg->msg_iov[0]);
	msg->msg_iov = kzmalloc_nofail(alloclen);
#ifdef ICS_DEBUG
	printk("%s: msg_iovlen: %d\n", __func__, (int)msg->msg_iovlen);
#endif /* ICS_DEBUG */
	icscli_lldecode_ool_data_t(handle, (u_char *)msg->msg_iov, alloclen);

	/*
	 * Decode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef ICS_DEBUG
		printk("%s: msg_iov[%d].iov_len: %d\n",
		       __func__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* ICS_DEBUG */
		alloclen = msg->msg_iov[i].iov_len;
		msg->msg_iov[i].iov_base = kzmalloc_nofail(alloclen);
		icscli_lldecode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   alloclen);
	}

	return (0);
}

/*
 * icscli_llsend()
 *	Send a message from the client to the server
 *
 * Description:
 *	This routine is called by the ICS high-level code to actually send
 *	the message from the client to the server.
 *
 *	After obtaining a handle and performing the appropriate argument
 *	marshalling, icscli_llsend() is used to send the message from the
 *	client to the server.
 *
 *	The callback routine is called (by the underlying transport) once
 *	the message has been sent (either successfully or not) to the server,
 *	and (for message/response interactions) the response has been
 *	received. Once the callback routine has been called (or during the
 *	callback routine), output parameters may be decoded (i.e. calls to
 *	icscli_lldecode_*() can be performed) and the handle can be
 *	released.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the client handle associated with the message
 *
 * Returns:
 *      -EREMOTE
 *      0
 */
int
icscli_llsend(cli_handle_t *handle)
{
	struct iovec	*iov;
	icshdr_t	*ihdr_p;
	int		i;
	int		error = 0;
	boolean_t	immediate_callback;
	ics_llnodeinfo_t *nodeinfo;
	int             ret_status = 0; /* avoid race w sendup_reply
					 * freeing handle
					 */
	ics_ib_conn_data_t *conn = NULL;
	kib_tx_t	*txd=NULL;
	int		expected_rdma_ops;
	ib_rdma_seg_t	ib_rmem[MAX_RDMA_READ_OPS];
	ib_rdma_seg_t	*rm;
	ulong		Tstart;

	/* Set up fields in the message header */
	iov = handle->ch_msg_msghdr_p->msg_iov;
	ihdr_p = (icshdr_t *)iov[0].iov_base;
	ihdr_p->ihdr_marker = ICSHDR_MARKER;
	ihdr_p->ihdr_inline_len = (void *)handle->ch_msg_inline_data_p -
					     iov[0].iov_base - sizeof(icshdr_t);
	ihdr_p->ihdr_len = 0;
	for (i = 0; i < handle->ch_msg_msghdr_p->msg_iovlen; i++)
		ihdr_p->ihdr_len += iov[i].iov_len;
	ihdr_p->ihdr_version = 1;
	ihdr_p->ihdr_service = handle->ch_service;
	ihdr_p->ihdr_flag = 0;
	if (handle->ch_flag & CLI_HANDLE_NO_REPLY) {
		ihdr_p->ihdr_flag = ICS_NO_REPLY;
		/* Drop extra hold put on handle in icscli_llhandle_init()
		   just in case this was an RPC. */
		SSI_ASSERT(handle->ch_ref_cnt > 0);
		handle->ch_ref_cnt--;
	}

	ihdr_p->ihdr_procnum = handle->ch_procnum;
	ihdr_p->ihdr_transid = handle->ch_transid;
	ihdr_p->ihdr_orignode = this_node;
	ihdr_p->ihdr_chan = handle->ch_chan;
#ifdef NSC_ICSLOG
	ihdr_p->ihdr_uniqueid = handle->ch_uniqueid;
#endif

	/* Log the message. */
	ICSLOG(1, ICSLOG_CLILLSEND, ihdr_p->ihdr_transid,
				    ihdr_p->ihdr_uniqueid,
				    handle->ch_node,
				    ihdr_p->ihdr_service,
				    ihdr_p->ihdr_chan,
				    ihdr_p->ihdr_procnum);

	/*
	 * Find a connection to the node, and send the message if the
	 * connection exists.
	 * NOTE: The connection must be established prior to this call.
	 *
	 * NSC_XXX - The race avoidance here isn't well done due to shortfalls
	 * 	     in ICS nodedown; it should be reexamined when ICS
	 *	     nodedown is fixed.
	 *
	 * NOTE: If there has been an error detected for this handle,
	 * force the error path to be followed.
	 */
	if (handle->ch_status != 0)
		nodeinfo = NULL;
	else
		nodeinfo = ics_llnodeinfo[handle->ch_node];
	if (nodeinfo != NULL) {
		LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		while (nodeinfo->icsni_conn == NULL ||
		       (nodeinfo->icsni_conn[handle->ch_chan] == NULL &&
			(nodeinfo->icsni_flag & ICSNI_NODE_UP))) {
			UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			while (nodeinfo->icsni_conn == NULL ||
			       (nodeinfo->icsni_conn[handle->ch_chan] == NULL &&
				(nodeinfo->icsni_flag & ICSNI_NODE_UP))) {
				WAIT_EVENT(&nodeinfo->icsni_chan_event);
			}
			LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		}
		conn = nodeinfo->icsni_conn[handle->ch_chan];
		if (conn != NULL)
			INCR_ATOMIC_INT(&conn->conn_ref_cnt);
		UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (conn != NULL) {
			LOCK_LOCK(&conn->conn_lock);
			Tstart = jiffies;
			error = ics_llsendmsg(conn,
					      handle->ch_msg_msghdr_p,
					      ihdr_p->ihdr_len,
					      ib_rmem,
					      &expected_rdma_ops,
					      &txd);
			UNLOCK_LOCK(&conn->conn_lock);
			DECR_ATOMIC_INT(&conn->conn_ref_cnt);
		}
	}

	if (error > 0) {
		/* Successful IB mesg send - wait for RDMA OOL data segments to
		 * be pulled by the receiver. A kernel timer is set by llsendmsg
		 * to escape the following wait should the receiver not be able
		 * to perform the RDMA operations and report back with an RDMA
		 * status message.
		 */
		if (error > 1)
			dprintk(DBG_RDMA,("%s hdl %p expecting %d RDMA.reads\n",
						__func__,handle,error));

		/* wait for RMDA.read status message */
		wait_event(conn->conn_ib->waitQ,(expected_rdma_ops <= 0));

		if (error > 1)
			dprintk(DBG_RDMA,
				("%s hdl %p, %d RDMA.reads done jif %ld\n",
				__func__,handle,error,(jiffies-Tstart)));

        	put_idle_txd(txd);      /* return TX descriptor to free pool */

		/* IB deregister/unmap memory used in RDMA.read op */
		for(i=0,rm=ib_rmem; i < error; i++,rm++) {
			struct _ib_nexus *ibn=conn->conn_ib->ib_nexus;

			if (!rm->baddr) {
				dma_unmap_single(ibn->device->dma_device,
                                                 pci_unmap_addr(rm,mapping),
                                                 pci_unmap_len(rm,mlen),
                                                 DMA_TO_DEVICE);
			}
			else {
                                dma_unmap_page(ibn->device->dma_device,
                                               pci_unmap_addr(rm,mapping),
                                               pci_unmap_len(rm,mlen),
                                               DMA_TO_DEVICE);
			}
		}

		/* Errors? The success path is expected_rdma_ops drops to
		 * zero, else it contains a timeout handler error or RDMA
		 * status message error.
		 */
		error = expected_rdma_ops;
	}

	/*
	 * Do the freebuf callback for OOL data.
	 */
#ifdef ICS_DEBUG
        printk("%s: freeing OOL data, %d desc\n",
					__func__,handle->ch_msg_ooldesc_cnt);
#endif
	for (i = 0; i < handle->ch_msg_ooldesc_cnt; i++) {
		register ooldesc_t *ool_p;

		ool_p = &handle->ch_msg_ooldesc_p[i];

		/* now release the memory */
		(ool_p->ool_callback_data_t)(ool_p->ool_callparam,
					     ool_p->ool_callarg);
	}
	/*
	 * The ch_ref_cnt count was incremented for every OOL segment encoded in
	 * icscli_llencode_ool_data_t(). Now decrement the count for each
	 * encoding.
	 */
	if (handle->ch_msg_msghdr_p->msg_iovlen > 1) {
		LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		handle->ch_ref_cnt -= handle->ch_msg_msghdr_p->msg_iovlen - 1;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	}

	/*
	 * If the message was not sent, then clean up after ourselves. The
	 * callback must be performed either here or later in the one
	 * of the free routines called from freemsg().
	 */
	if (nodeinfo == NULL || conn == NULL || error < 0) {
#ifdef ICS_DEBUG
		printk("%s: Message not sent! err %d\n",__func__,error);
#endif /* ICS_DEBUG */
		LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		/* RPC? */
		if ((handle->ch_flag & CLI_HANDLE_NO_REPLY) == 0) {
			/* Yes: handshake with icscli_llnodedown(). */
			if ((handle->ch_llflag & CLI_HANDLE_REPLY_DONE) == 0) {
				handle->ch_llflag |= CLI_HANDLE_REPLY_DONE;
				handle->ch_status = -EREMOTE;
				SSI_ASSERT(handle->ch_ref_cnt > 0);
				handle->ch_ref_cnt--;
			}
		}
		SSI_ASSERT(handle->ch_ref_cnt > 0);
		handle->ch_ref_cnt--;
		immediate_callback = (handle->ch_ref_cnt == 0);
		ret_status = handle->ch_status;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		if (immediate_callback) {
#ifdef ICS_DEBUG
			printk("%s: call icscli_sendup_reply() @ line %d\n",
				__func__,__LINE__);
#endif
			icscli_sendup_reply(handle);
		}
#ifdef ICS_DEBUG
		if (ret_status) printk("%s() Exit %d\n",__func__,ret_status);
#endif
		return(ret_status);
	}

	/*
	 * See if we can perform an immediate callback, and do so if we
	 * can. This can happen if there was no OOL data or the OOL
	 * data was sent and freed very fast and if any reply was
	 * received very fast.
	 */
	LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	SSI_ASSERT(handle->ch_ref_cnt > 0);
	handle->ch_ref_cnt--;
	immediate_callback = (handle->ch_ref_cnt == 0);
	UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	if (immediate_callback) {
#ifdef ICS_DEBUG
		if (ret_status)
			printk("%s: icscli_sendup_reply() @ line %d\n",
				__func__,__LINE__);
#endif /* ICS_DEBUG */
		icscli_sendup_reply(handle);
	}
	return(ret_status);
}

/*
 * icscli_llsendup_reply()
 *	Set up some stuff and perform the callback specified by icscli_llsend().
 *
 * Description:
 *	This is a routine that is called by the network bottom half handler
 *	when a complete ICS datagram has arrived from the server node with the
 *	reply to a call to icscli_llsend(). This routine is responsible for
 *	performing some preliminary setup, and then possibly calling the
 *	high-level ICS callback routine.
 *
 *	Note that for the TCP implementation, the reply may arrive before
 *	the message send has completed (because of TCP retry characteristics).
 *	Because the message send may re-reference the OOL data, we don't
 *	want to do the high-level ICS callback until both the reply has arrived
 *	and the send has completed.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	skb		pointer to sk_buff that contains the entire reply
 */
void
icscli_llsendup_reply(struct _kib_rxd *rxd)
{
	cli_handle_t	*handle;
	boolean_t	do_callup;
	icshdr_t	*icshdr;
	struct ib_conn  *conn;

	if ( !rxd ) {
		printk("%s rxd <NULL>?\n",__func__);
		return;
	}
	icshdr = LL_ICSHDR(rxd);
	conn = rxd->conn;

	/* Figure out the client handle that corresponds to the reply. */
	handle = icscli_find_transid_handle(icshdr->ihdr_transid);

	/* Store the received IB msg descript for later RX repost */
	handle->ch_rep_rxd = rxd;

	/* Set up pointers for the in-line buffer. */
	handle->ch_rep_inline_data_p = (u_char*)(icshdr+1);

	ICSLOG(1, ICSLOG_CLILLSENDUP, icshdr->ihdr_transid,
				      icshdr->ihdr_uniqueid,
				      handle->ch_node,
				      icshdr->ihdr_service,
				      icshdr->ihdr_chan,
				      icshdr->ihdr_procnum);

	/* Set up pointers to the message data descritpors (in/out) */
	handle->ch_rep_ib_ool_info =  (ib_ool_info_t*) 
			((u_char*)(icshdr+1) + icshdr->ihdr_inline_len);
	handle->ch_rep_ib_od = (ib_ool_desc_t*)(handle->ch_rep_ib_ool_info+1);

	/*
	 * Most likely we do the high-level ICS callup now. But it is possible
	 * that OOL data has not been freed yet, and this will perform the
	 * callup if we can't.
	 */
	LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	SSI_ASSERT((handle->ch_llflag & CLI_HANDLE_REPLY_DONE) == 0);
	handle->ch_llflag |= CLI_HANDLE_REPLY_DONE;
	SSI_ASSERT(handle->ch_ref_cnt > 0);
	handle->ch_ref_cnt--;
	do_callup = (handle->ch_ref_cnt == 0);
	UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	if (do_callup)
		icscli_sendup_reply(handle);
}

/*
 * icscli_lldecode_inline()
 *
 * Description:
 *	Pull data from the in-line buffer as a byte stream.
 *	If 'param' is <null>, then advance data pointer by 'size' & exit.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */

void
icscli_lldecode_inline(
	cli_handle_t	*handle,
	void		*param,
	int		size)
{
	if (size <= 0) return;

#ifdef DEBUG_TOOLS
#ifndef CONFIG_X86_4G
	if (param != NULL && (unsigned long)param < TASK_SIZE)
		panic("%s:user address %p not allowed\n", __func__, param);
#endif
	SSI_ASSERT(handle->ch_rep_inline_data_p);
#endif
	if (param) memcpy(param, (void*)handle->ch_rep_inline_data_p, size);
	handle->ch_rep_inline_data_p += size;
}


/*
 * icscli_lldecode_ool_data_t()
 *	Extract an OOL output parameter into the specified area
 *
 * Description:
 *	Extract an OOL output parameter for the specified handle into
 *	the user-provided buffer area. Called by ICS high-level code
 *	to perform argument marshalling for client/server interaction.
 *
 *	This routine must be called after icscli_llsend().
 *
#ifdef SNET
 *	Note that icscli_llencoderesp_ool_data_t() must also be called for
 *	each output parameter prior to the call to icscli_llsend().
#endif
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to array of characters; must not be NULL
 *	nelem		number of elements in the array
 */

int
icscli_lldecode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*param,
	long		len)
{
	ib_ool_desc_t	*iod = handle->ch_rep_ib_od;
        int             rc, rlen, EOM=0;
        ib_rdma_seg_t	rseg;
        ulong           laddr;
	struct device	*dev;

	SSI_ASSERT(iod);
	SSI_ASSERT(handle->ch_rep_ib_ool_info);
	SSI_ASSERT(handle->ch_rep_rxd);
	SSI_ASSERT(handle->ch_rep_rxd->conn);

	/* any reason to proceed? */
	if (len <= 0) return 0;

	SSI_ASSERT(handle->ch_rep_ib_ool_info->ool_desc_cnt > 0);

#ifdef DEBUG_TOOLS
#ifndef CONFIG_X86_4G
	if ((unsigned long)param < TASK_SIZE)
		panic("%s:user address %p not allowed\n", __func__, param);
#endif
#endif
	SSI_ASSERT(iod->len != 0);

	/* process the current IB ool descriptor */
	if ( iod->len > 0 ) {
		/* OOL data is contained within the IB message, copyout */
		if ( iod->len != len ) {
			printk("%s reply in-msg OOL data sz %d != "
				"decode len %ld\n",__func__,iod->len,len);
		}
		if (param) memcpy((void*)param, iod->ool_data, iod->len);
		/* advance to next ib ool data descriptor */
		handle->ch_rep_ib_od = (ib_ool_desc_t*)
					(((u_char*)iod)+sizeof(int)+iod->len);
	}
	else {
		/* OOL data is NOT present in the IB message as the sender
		 * determined it would not fit. OOL data needs to be RDMA.read
		 * from the sender.
		 */
		SSI_ASSERT(param);
		if (iod->ool_magic != 0xcafebabe) { // XXX debug
			printk("%s bad magic(%x) ch_rep_rdma_cnt %d line %d\n",
				__func__, iod->ool_magic,
				handle->ch_rep_rdma_cnt,__LINE__);
		}

		rlen = -1 * iod->len;	// 'real' RDMA data length in bytes
		if (rlen != len) {
			printk("%s OOL RDMA data sz %d != request len %ld\n",
						__func__,rlen,len);
			return -EINVAL;
		}
		/*
		 * one more RDMA ool to process
		 */
		handle->ch_rep_rdma_cnt++;

		rc = ib_rdma_setup(param, len, DMA_FROM_DEVICE, &laddr, &rseg);
		if (rc) {
			// error
			printk("%s RDMA.read(0x%p sz %ld) setup err %d\n",
							__func__, param,len,rc);
			return -ENOMEM;
		}
		/*
		 * RDMA.read OOL data to local buffer. If this is the last RDMA
		 * OOL in the current ICS message, then send an RDMA status back
		 * to the ICS message sender which completes the send op.
		 */
		dprintk(DBG_RDMA,
			("%s ch_rep_rdma_cnt %d rdma_ool_desc_cnt %d\n",
			__func__, handle->ch_rep_rdma_cnt,
			handle->ch_rep_ib_ool_info->rdma_ool_desc_cnt));
		/*
		 * finished all required RDMA.reads? if so, send RDMA.read
		 * status message with last RDMA.read request.
		 */
#if 1
		if (handle->ch_rep_rdma_cnt == handle->ch_rep_ib_ool_info->rdma_ool_desc_cnt) {
			EOM = handle->ch_rep_rdma_cnt;
		}
#else
		EOM = 1; /* force RMDA status reply per RDMA.read */
#endif

		rc = ib_rdma_read(handle->ch_rep_rxd->conn, param, len, &laddr,
					rseg.mr->lkey, iod, 
					handle->ch_rep_ib_ool_info->txd, EOM);

		/* advance to next IB OOL descriptor */
		handle->ch_rep_ib_od = iod + 1;

		/* undo what if/anything rdma read setup did w.r.t. mapping */
		dev = handle->ch_rep_rxd->conn->ib_nexus->device->dma_device;
		if (!rseg.baddr) {
			dma_unmap_single(dev,
                                         pci_unmap_addr(&rseg,mapping),
                                         pci_unmap_len(&rseg,mlen),
                                         DMA_FROM_DEVICE);
		}
		else {
			dma_unmap_page(dev,
                                       pci_unmap_addr(&rseg,mapping),
                                       pci_unmap_len(&rseg,mlen),
                                       DMA_FROM_DEVICE);
		}

		/* RDMA.read OK? */
		if (rc) {
			printk("%s Err - RDMA.read(0x%p sz %ld) rc %d\n",
							__func__, param,len,rc);
			return -ENOMEM;
		}
	}
	return 0;
}

/*
 * icscli_llhandle_deinit()
 *	Perform any deinitialization of the handle necessary for low-level ICS.
 *
 * Description:
 *	This routine is called by ICS high level code when a client handle is
 *	about to be released.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle to be deinitialized
 *
 * Return value:
 */
void
icscli_llhandle_deinit(
	cli_handle_t	*handle)
{
	/* Deinitialize any handle locks. */
	DEINIT_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);

	/*
	 * Free outgoing msghdr and iovecs.
	 * We only free the inline data. It is the responsibility
	 * of the caller to free the OOL data.
	 */
	if (handle->ch_msg_msghdr_p->msg_iov != handle->ch_iovec)
		kfree(handle->ch_msg_msghdr_p->msg_iov);
	if (handle->ch_msg_ooldesc_p != handle->ch_ooldesc)
		kfree(handle->ch_msg_ooldesc_p);

	/* Free incoming IB rx descritpr. */
	if (handle->ch_rep_rxd) {
		ics_llfreemsg(handle->ch_rep_rxd);
		handle->ch_rep_rxd = NULL;
	}
}

/*
 * icscli_llnodedown()
 *	Perform the low-level client portion of nodedown logic.
 *
 * Description:
 *	This routine is called by the ics_llnodedown() routine after all the
 *	connections to the down node have been torn down.
 *
 *	This routine looks at all the currently outstanding RPCs to the node
 *	that is down and performs the callback on each of them.
 *
 *	This routine  must be called from thread context.
 *
 * Parameters:
 *	node		the node that is being declared down
 */
void
icscli_llnodedown(
	clusternode_t	node)
{
	cli_handle_t	*handle;
	clihand_list_t	*chl_p = &clihand_list[CLIHAND_HASH(node)];
	boolean_t	immediate_callback;

	LOCK_SPIN_LOCK(&chl_p->chlist_lock);
	/*
	 * Go through all the handles on the list.
	 */
	for (handle = chl_p->chlist_first;
			handle != NULL;
			handle = handle->ch_next) {

		/* Eliminate handles that don't refer to the node going down. */
		if (handle->ch_node != node)
			continue;

		/*
		 * If a callback has been done, then skip the handle.
		 * Otherwise, mark the callback as done, and see if it
		 * can be performed immediately, or whether the call
		 * to freemsg() will perform it.
		 */
		LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		if ((handle->ch_llflag & CLI_HANDLE_REPLY_DONE) != 0) {
			UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
			continue;
		}
		handle->ch_llflag |= CLI_HANDLE_REPLY_DONE;
		SSI_ASSERT(handle->ch_ref_cnt > 0);
		handle->ch_ref_cnt--;
		immediate_callback = (handle->ch_ref_cnt == 0);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		handle->ch_status = -EREMOTE;
		if (immediate_callback)
			icscli_sendup_reply(handle);
	}
	UNLOCK_SPIN_LOCK(&chl_p->chlist_lock);
}

#if defined(ICS_DEBUG) || defined(DEBUG_TOOLS)
/*
 * Print an ICS handle
 */
void
print_icscli_llhandle(cli_llhandle_t *generic)
{
	ib_cli_llhandle_t *cli = &generic->ibcli_llhandle;

	printk(KERN_DEBUG
	       "\n_ch_msg_msghdr_p 0x%p _ch_msg_inline_iov_p 0x%p\n",
			cli->_ch_msg_msghdr_p,
			cli->_ch_msg_inline_iov_p);

	printk(KERN_DEBUG "\n_ch_msg_inline_data_p 0x%p \n",
			cli->_ch_msg_inline_data_p);

	printk(KERN_DEBUG "\n_ch_msg_ooldesc_p 0x%p _ch_msg_ooldesc_cnt %d\n",
	       cli->_ch_msg_ooldesc_p, cli->_ch_msg_ooldesc_cnt);

	printk(KERN_DEBUG "\n_ch_rep_inline_data_p 0x%p \n",
			 cli->_ch_rep_inline_data_p);

	printk(KERN_DEBUG "\n_ch_rep_rdma_cnt %d\n",
			 cli->_ch_rep_rdma_cnt);

	printk(KERN_DEBUG "\n Reference Count %d\n", cli->_ch_ref_cnt);

	printk(KERN_DEBUG "\n_ch_llflag %d _ch_lllock 0x%p \n",
			  cli->_ch_llflag, &(cli->_ch_lllock));

	return;

}

#endif /* ICS_DEBUG || DEBUG_TOOLS */
