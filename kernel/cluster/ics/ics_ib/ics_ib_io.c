/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
 */

/*++
"This software program is available to you under a choice of one of two 
licenses.  You may choose to be licensed under either the GNU General Public 
License (GPL) Version 2, June 1991, available at 
http://www.fsf.org/copyleft/gpl.html, or the Intel BSD + Patent License, 
the text of which follows:

"Recipient" has requested a license and Intel Corporation ("Intel") is willing 
to grant a license for the software entitled InfiniBand(tm) System Software 
(the "Software") being provided by Intel Corporation.

The following definitions apply to this License: 

"Licensed Patents" means patent claims licensable by Intel Corporation which 
are necessarily infringed by the use or sale of the Software alone or when 
combined with the operating system referred to below.

"Recipient" means the party to whom Intel delivers this Software.
"Licensee" means Recipient and those third parties that receive a license to 
any operating system available under the GNU Public License version 2.0 or 
later.

Copyright (c) 2006 Intel Corporation. All rights reserved. 

The license is provided to Recipient and Recipient's Licensees under the 
following terms.  

Redistribution and use in source and binary forms of the Software, with or 
without modification, are permitted provided that the following conditions are 
met: 
Redistributions of source code of the Software may retain the above copyright 
notice, this list of conditions and the following disclaimer. 

Redistributions in binary form of the Software may reproduce the above 
copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution. 

Neither the name of Intel Corporation nor the names of its contributors shall 
be used to endorse or promote products derived from this Software without 
specific prior written permission.

Intel hereby grants Recipient and Licensees a non-exclusive, worldwide, 
royalty-free patent license under Licensed Patents to make, use, sell, offer 
to sell, import and otherwise transfer the Software, if any, in source code 
and object code form. This license shall include changes to the Software that 
are error corrections or other minor changes to the Software that do not add 
functionality or features when the Software is incorporated in any version of 
a operating system that has been distributed under the GNU General Public 
License 2.0 or later.  This patent license shall apply to the combination of 
the Software and any operating system licensed under the GNU Public License 
version 2.0 or later if, at the time Intel provides the Software to Recipient, 
such addition of the Software to the then publicly available versions of such 
operating system available under the GNU Public License version 2.0 or later 
(whether in gold, beta or alpha form) causes such combination to be covered by 
the Licensed Patents. The patent license shall not apply to any other 
combinations which include the Software. No hardware per se is licensed 
hereunder. 
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL INTEL OR ITS CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
--*/


/*
* NAME
*       Low-level ICS packet I/O code
* COPYRIGHT
*       GNU General Public or Intel BSD + Patent License
* PURPOSE
*
*	This file contains the service procedures for sending & receiving NSC
*	ICS datagrams over the openIB.org Infiniband verbs layer.
*	The rdma_xxx layer provides CM (connection managment) while the IB
*	verbs layer provides data movement services.
*
* AUTHOR
*       Stan C. Smith    stan.smith@intel.com [Aug 2006]
* DESCRIPTION
*
*	Before any ICS data exchange can take place a reliable Infiniband (IB)
*	connection must first be established (see Infiniband Connection
*	Management); no RDMA over IB Unreliable Datagrams.
*	When a connection is created, connection local receive buffers are
*	allocated posted to the connection QP (Queue pair). Transmit resources,
*	on a per connection basis, are added to the global transmit free pool
*	once a connection has become established. This transmit resource
*	management approach allows all connections to share transmit resources
*	thus reducing the chance of a transmit operations blocking for TX
*	resources without having to allocate max TX resources for each
*	connection; see /proc/cluster/ics_ib/{stats|iostats}.
*
* -- ICS Data Transmission
*
*	An ICS message send (ics_llsendmsg) operation with the total message
*	data size <= the RDMA threshold (IBA_MAX_MESG_SIZE), is accomplished by
*	copying message data (inline & OOL) into the IB message payload buffer
*	and sent. RDMA setup, xfer and status message out weight the cost of
*	copying ICS message data, up to approximately 12 KB; see ics_ib_procfs.c
*	buffer copy routines for timing information.
*
*	Mechanics of a data copy send:
*
*	A transmit descriptor buffer is retrieved from the global transmit
*	free pool on the sending node. ICS message data is copied into the
*	TX buffer (txd->payload) and the IB send work-request posted to the
*	Infiniband 'established' connection's QP (Queue-Pair).
*
*	When the completion event of the IB send operation is posted to the
*	connection CQ (Completion Queue), the waiting IB sender (ics_llsendmesg)
*	is wakened to complete the send operation.
*
*	Mechanics of an RDMA data send:
*
*	When the ICS message size or cumulative OOL data size is > the RDMA
*	threshold (TX descriptor payload size), an Infiniband RDMA read
*	operation is performed by the node receiving the ICS message.
*	The local OOL memory is identified in the ICS message RDMA OOL
*	descriptor contained in the ICS message proper.
*
*	When the ICS message destination node receives the ICS message, standard
*	recv OOL data processing recognizes the RDMA ool data descriptior and
*	initiates an RDMA.read to retrieve the OOL data. When the RDMA read
*	operation completes, the data sink node (ICS message receiver) sends
*	an RDMA.read status message back to the data source node which completes
*	the original ICS message send operation. There may be numerous RDMA
*	OOL data segments per ICS message sent. The ICS message sender waits
*	until all RDMA.read transactions have completed successfully before
*	completing the original ICS send operation successful.
*	The sender sets an RDMA send timer to cover remote node failure
*	resulting in incomplete RDMA transaction(s). Should the send timer
*	expire, the entire ICS message send is declared failed.
*	Successful reception of the last expected RDMA.read status message
*	clears the send timer.
*	One can now see why copying message data can be faster than RDMA.read
*	transactions.
*	Future work - reduce multiple RDMA.read status messages to a single
*	cumulative RDMA.status message.
*
* -- ICS Data Reception --
*
*	Infiniband message reception requires a pre-posted buffer, otherwise
*	RNR (Receiver-Not_Ready) errors occur at the sender.
*	Per the ICS/IB send discussion, the ICS message header, inline data
*	and OOL descriptors arrive in the RX descriptor payload buffer.
*	Standard ICS message receive OOL decode routines can issue RDMA.read
*	operations to retrieve OOL data.
*
*	All ICS receive message processing is done in a kernel thread
*	context; 'ksvr_thread() or ksvr_tasklet()'. Therefore the Infiniband CQ
*	(Completion Queue) processing for a receive operation just entails
*	queuing the received ICS message on the ksvr_thread's receive
*	queue. The ksvr thread calls ics_dgram() to process received
*	messages.
*/

#if KSVR && KSVR_TASKLET
#error KSVR & KSVR_TASKLET both defined
#endif
#if KSVR==0 && KSVR_TASKLET==0
#warning Neither KSVR or KSVR_TASKLET are defined!
#error ISC recv message processing @ device IRQ does NOT work!
#endif


#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/delay.h>
#include <linux/signal.h>
#include <linux/crc32.h>
#include <linux/highmem.h>

#include <linux/net.h>
//#include <linux/random.h>
#include <net/sock.h>

#include <cluster/nsc.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>        /* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>  /* Must be after <ics_private.h> */

/* External definitions & Forward references */
extern clusternode_t clms_master_node;
extern void ics_recvmsg(struct sock*, struct sk_buff *);
extern void ksvr_grow_rx_pool(ib_nexus_t *nd, u32 elements);
void ksvr_queue_work(struct _kib_rxd *rxd);

/*
 * Infiniband Work REQUEST type names.
 */
static char *wr_type_text[] = {
	"IB_WR_RDMA_WRITE",
	"IB_WR_RDMA_WRITE_WITH_IMM",
        "IB_WR_SEND",
        "IB_WR_SEND_WITH_IMM",
        "IB_WR_RDMA_READ",
        "IB_WR_ATOMIC_CMP_AND_SWP",
        "IB_WR_ATOMIC_FETCH_AND_ADD"
};

static const char *
ib_get_wc_type_text(enum ib_wc_opcode op)
{
	static char *wc_opcode_text[] = {
        	"IB_WC_SEND",
        	"IB_WC_RDMA_WRITE",
        	"IB_WC_RDMA_READ",
        	"IB_WC_COMP_SWP",
        	"IB_WC_FETCH_ADD",
        	"IB_WC_BIND_MW" };
	static char buf[40];

	if (op >= IB_WC_SEND && op <= IB_WC_BIND_MW)
		return wc_opcode_text[op];
	if (op & IB_WC_RECV)
		return "IB_WC_RECV";
	if (op == IB_WC_RECV_RDMA_WITH_IMM)
		return "IB_WC_RECV_RDMA_WITH_IMM";
	sprintf(buf,"bad wc_opcode %d(0x%x)",op,op);
	return buf;
}


const char *
ib_get_wc_status_str(enum ib_wc_status s)
{
	static char *wc_status_text[] = {
        	"IB_WC_SUCCESS",
        	"IB_WC_LOC_LEN_ERR",
        	"IB_WC_LOC_QP_OP_ERR",
        	"IB_WC_LOC_EEC_OP_ERR",
        	"IB_WC_LOC_PROT_ERR",
        	"IB_WC_WR_FLUSH_ERR",
        	"IB_WC_MW_BIND_ERR",
        	"IB_WC_BAD_RESP_ERR",
        	"IB_WC_LOC_ACCESS_ERR",
        	"IB_WC_REM_INV_REQ_ERR",
        	"IB_WC_REM_ACCESS_ERR",
        	"IB_WC_REM_OP_ERR",
        	"IB_WC_RETRY_EXC_ERR",
        	"IB_WC_RNR_RETRY_EXC_ERR",
        	"IB_WC_LOC_RDD_VIOL_ERR",
        	"IB_WC_REM_INV_RD_REQ_ERR",
        	"IB_WC_REM_ABORT_ERR",
        	"IB_WC_INV_EECN_ERR",
        	"IB_WC_INV_EEC_STATE_ERR",
        	"IB_WC_FATAL_ERR",
        	"IB_WC_RESP_TIMEOUT_ERR",
        	"IB_WC_GENERAL_ERR"
	};
       if (s == NO_STATUS)
               return "NO_STATUS";

	return wc_status_text[s];
}

void
gimmeabreak(void)
{
}

/*
 * Return the number of micro seconds that have passed since the system
 * was booted
 */
u64
us_time_stamp( void )
{
    u64			tstamp;
    struct timeval	tval;

    do_gettimeofday( &tval );
    tstamp = ((u64)tval.tv_sec * USEC_PER_SEC) + (u64)tval.tv_usec;

    return( tstamp );
}


/*
* NAME
*	ib_post_recv_wr -- Post an Infiniband receive work-request
* DESCRIPTION
*	Post a receive work-request to the connection queue-pair work queue.
*	Posting is an synchronous operation, a CQ (Completion Queue) event
*	will be posted after a message has arrived.
* PARAMETERS
*    *krx
*	[in] receive descriptor
* RETURN VALUE
*	0 == successfully posted to the connection work-queue
*	Otherwise, IB status code, failure to post work-request
*	IB_INVALID_PARAMETER - null rx desc buffer ptr.
********/

int
kib_post_recv_wr(kib_rx_t *rxd)
{
	int			rc;
	ib_conn_t		*kconn = rxd->conn;
	struct ib_sge		sge[MAX_SGL_ENTRIES];
	struct ib_recv_wr	wr,*err;

	/* if cloned RX descriptor, return to free pool */
	if (rxd->class == RXD_CLASS_CLONE) {
		ulong flags;
		ib_nexus_t *ibx = rxd->conn->ib_nexus;

		spin_lock_irqsave(&ibx->rx_pool_lock, flags);
		list_add_tail(&rxd->link, &ibx->rx_pool);
		ibx->rx_pool_free++;
		ibx->rx_pool_in_use--;
		spin_unlock_irqrestore(&ibx->rx_pool_lock, flags);
		return SUCCESS;
	}

	rxd->nob = 0;		// reset recv'd byte count
	sge->length = IBA_MAX_BUF_SIZE;
	sge->lkey = kconn->ib_nexus->kmr->lkey;
	sge->addr = (u64)((ulong)rxd->dma_addr);

	// init the Infiniband receive work-request
	wr.next = NULL;
	wr.wr_id = (u64)((ulong)rxd);
	wr.sg_list = sge;
	wr.num_sge = 1;

	// post the receive request
	rc = ib_post_recv(kconn->cm_id->qp, &wr, &err);

	if(unlikely(rc != SUCCESS)) {
        	CERROR("Can't post recv wr, IB err %d\n", rc);
		return rc;
	}
	atomic_inc(&kconn->rx_posted);

	return SUCCESS;
}



/*
* DESCRIPTION
*	The IB send work-request is initialized. IB work-request send options
*	are set to signalled, a CQ (Completion-Queue) entry is inserted into
*	the completion queue for this connection's QP when the work-request has
*	been completed. A single send work-request is assumed. No immediate
*	data, the address of the TX descriptor is the send work-request ID.
*	Determine the total number of bytes this send work-request will
*	transmit and return this value.
* PARAMETERS
*    *ktx
*	[in] transmit descriptor
*    *sq
*	[in] IB send work-request
*    num_ds
*	[in] number of local descriptors in 'ds' vector
*    *ds
*	[in] local descriptor
*    wr_type
*	[in] work-request type (IB_WR_SEND, WR_RDMA_READ, WR_RDMA_WRITE)	
* RETURN VALUE
*	The total number of bytes which this send work-request will transmit.
* NOTES
*	Work-request send options are fixed here.
*/

static u32
send_wr_setup(	kib_tx_t	  *ktx,
		struct ib_send_wr *sq,
		u32	          num_sge, 
	 	struct ib_sge	  *sgl,
		enum ib_wr_opcode wr_type )
{
	int	i;
	u32	nob;

	sq->next = NULL;
	sq->wr_id = (u64)(ulong)ktx;
	sq->opcode = wr_type;
	ktx->wrt = wr_type;
	sq->send_flags = IB_SEND_SIGNALED;
	sq->num_sge = num_sge;
	sq->sg_list = sgl;
	sq->imm_data = 0;

	// determine the number of bytes to send
	if (num_sge == 1) {
		return sgl->length;
	}

	for (nob=0,i=0; i < num_sge; i++) {
		nob += sgl[i].length;
	}
	return nob;
}

#if 0 // Possible Future
/*
* NAME
*	locate_connection -- map a destination nid to it's connection structure.
* DESCRIPTION
*	Given a NID (Network ID, IPv4 address in network byte-order), return
*	a pointer to the connection struct which represents an IB connection
*	to the specified NID. If a connection does not exist for the given NID,
*	fail by returning NULL.
* PARAMETERS
*    *nd
*	[in] NAL data pointer
*    *nid
*	[in] Network ID
* RETURN VALUE
*	pointer to a valid connection structure or NULL.
* NOTES
*	IB nexus data is locked to read; lock_irqsave when modifying the
*	active connection list.
*/


ib_conn_t *
locate_connection(ib_nexus_t *nd, u32 nid)
{
	ib_conn_t		*kconn;
	struct list_head	*tmp;

	read_lock(&nd->conn_lock);

	if ( ! list_empty (&nd->active_conn) ) {
		list_for_each (tmp, &nd->active_conn)
		{
			kconn = list_entry (tmp, ib_conn_t, conn_list);
			if ( kconn->r_nid == nid ) {
				read_unlock(&nd->conn_lock);
				if ( BAD_MAGIC(kconn) ) {
					CERROR("BAD Connection struct MAGIC\n");
					kconn = NULL;
				}
				return kconn;
			}
		}
	}
	read_unlock(&nd->conn_lock);
	return NULL;
}
#endif // Future

/*
* DESCRIPTION -- return transmit descriptor back to free pool
*	Place the idle transmit descriptor back in the global free pool.
* PARAMETERS
*    *ktx
*	[in] Transmit discriptor pointer
* RETURN VALUE
*	VOID
* SIDE EFFECTS
*	global tx pool locked while tx placed on free pool list.
* SEE ALSO
*	get_txd
*/

void
put_idle_txd(kib_tx_t *ktx)
{
	ib_nexus_t *ibnx = ktx->conn->ib_nexus;
        unsigned long flags;

	if ( !ktx )
		return;

	// release DMA mapping
	if (ktx->dma_addr) {
		dma_unmap_single(ibnx->device->dma_device,
                                 pci_unmap_addr(ktx,mapping),
                                 pci_unmap_len(ktx,mlen),
                                 DMA_TO_DEVICE);
		ktx->dma_addr = 0;
	}

	ktx->private = NULL;

        spin_lock_irqsave (&ibnx->idletxd_lock, flags);

        if (ktx->non_blk) {
                /* reserved for non-blocking tx */
                list_add_tail (&ktx->ktx_list, &ibnx->nblk_idletxds);
		ibnx->nblk_txds_in_use--;
		ibnx->nblk_txds_free++;
                spin_unlock_irqrestore (&ibnx->idletxd_lock, flags);
                return;
        }

        list_add_tail (&ktx->ktx_list, &ibnx->idletxds);

	ibnx->txds_in_use--;
	ibnx->txds_free++;

 	/* wakeup a waiter... */
        if (waitqueue_active(&ibnx->idletxd_waitq) || ibnx->txds_free >= 1) {
                /* local sender waiting for tx desc */
                wake_up(&ibnx->idletxd_waitq);
        }
        spin_unlock_irqrestore (&ibnx->idletxd_lock, flags);
}


/*
* NAME
*	get_txd -- retrieve a transmit descriptor from the free pool
* DESCRIPTION
*	Acquire a blocking or non-blocking TX descriptor from the global TX
*	fee pool. Upon success, the TX descriptor is initialized with a pointer
*	to the connection struct assoicated with the specified NID (Network ID,
*	IPv4 address).
*	Blocking implies the caller can tolerate waiting/sleeping until a
*	TX descriptor is available (send of a portals PUT or GET message).
*	Non-blocking refers to the case of sending a portals ACK or REPLY
*	message where blocking is intolerable.
* PARAMETERS
*    *conn
*	[in] IB connection struct pointer
*    may_block
*	[in] TRUE indicates the caller can block waiting for tx resource.
*	     FALSE indicates the caller can NOT block.
* RETURN VALUE
*	a TX descriptor pointer or NULL
* SIDE EFFECTS
*	TX descriptor itself is initialized with a pointer to the connection
*	struct associated with the input nid. If may_block == TRUE, then the
*	calling thread may block waiting for TX resouces if the blocking free
*	pool is empty.
* NOTES
*	A non-blocking request will 1st attempt to pull from the blocking pool.
*	If the blocking pool is empty, then an attempt to pull from the
*	non-blocking pool. Should the non-blocking pool be empty, complain
*	loudly and return NULL. Higher layers will also complain.
* SEE ALSO
*	put_idle_txd
********/

kib_tx_t *
get_txd(ib_conn_t *conn, int may_block)
{
	ib_nexus_t	*nd = &ib_nexus;
        kib_tx_t	*ltx = NULL;
        unsigned long	flags;

	for (;;) {
		// acquire tx descriptor list lock
		spin_lock_irqsave(&nd->idletxd_lock, flags);

		/* blocking descriptor free ? */

		if ( !list_empty(&nd->idletxds) ) {
			ltx = list_entry(nd->idletxds.next, kib_tx_t, ktx_list);

			if ( ++nd->txds_in_use > nd->txds_max_in_use )
				nd->txds_max_in_use = nd->txds_in_use;

			if ( --nd->txds_free == 0 )
				nd->txds_empty++;
			break;
		}

		/*
		 * non-blocking transmit?
		 */

		if (!may_block) {
			if (list_empty(&nd->nblk_idletxds)) {
				CERROR("intr tx desc pool exhausted\n");
				break;
			}
			ltx = list_entry(nd->nblk_idletxds.next,
						kib_tx_t, ktx_list);

			if ( ++nd->nblk_txds_in_use > nd->nblk_txds_max_in_use )
				nd->nblk_txds_max_in_use = nd->nblk_txds_in_use;

			if ( --nd->nblk_txds_free == 0 )
				nd->nblk_txds_empty++;
			break;
		}

		/* block waiting for idle txd, see put_idle_txd for wakeup */

		spin_unlock_irqrestore(&nd->idletxd_lock, flags);

 		dprintk(DBG_SEND,("blocking for tx desc\n"));

		wait_event(nd->idletxd_waitq, !list_empty(&nd->idletxds));
	}

	if (ltx) {
		// remove from free list, point at nid's connection
		list_del (&ltx->ktx_list);
		ltx->conn = conn;
	}
	spin_unlock_irqrestore (&nd->idletxd_lock, flags);

	/*
	 * setup DMA mappings for the TX payload buffer
	 */
	ltx->dma_addr = dma_map_single(nd->device->dma_device,
                                       (void*)ltx->payload,
                                       (size_t)IBA_MAX_BUF_SIZE,
                                       DMA_TO_DEVICE);
	if (dma_mapping_error(ltx->dma_addr)) {
		CERROR("tx desc dma map error %p\n",ltx->payload);
	}
        pci_unmap_addr_set(ltx,mapping,ltx->dma_addr);
	pci_unmap_len_set(ltx,mlen,IBA_MAX_BUF_SIZE);

	return (ltx);
}

/*
* DESCRIPTION -- clone an RX descriptor from the global.
*	Remove an RX descriptor from the global free pool and copy the input
*	RXD contents to the rxd from the global pool. This routine is
*	designed to be called from the CQ processing loop at tasklet context.
*	A particular connection's posted receive buffer count has expired,
*	so the contents of a 'real' RXD are copied/cloned to a global RXD as
*	a safety net so no packets are dropped. The source RXD which is
*	constructed from IB registered memory is then reposted while the cloned
*	RXD is queued for processing by a kserver thread. All this to keep IB
*	receive buffers posted while continuing to process Portals messages.
*	Bottom line: it is permissible to slow down, while stopping is not.
* PARAMETERS
*    *nd
*	[in] this NAL's data pointer
* RETURN VALUE
*	void
* SIDE EFFECTS
*	An RX descriptor is removed from the global RX free pool.
* NOTES
*	RX descriptors from the global pool are NOT suitable to be posted for
*	an IB receive operation as the memory which the RXD is constructed
*	from is not IB registered. The global RX pool is only an escape hatch
*	used to keep IB receive buffers posted while allowing Portals message
*	processing to continue.
********/


kib_rx_t *
clone_rxd(ib_nexus_t *nd, kib_rx_t *src_rxd)
{
	ulong		flags;
	kib_rx_t	*nrxd;

	spin_lock_irqsave(&nd->rx_pool_lock, flags);

	if ( list_empty(&nd->rx_pool) ) {
		spin_unlock_irqrestore( &nd->rx_pool_lock, flags );
		CWARN("Global RX free pool empty?\n");
		return NULL;
	}

	// remove an RX descriptor from the pool

	nrxd = list_entry(nd->rx_pool.next,kib_rx_t, link);

	list_del(&nrxd->link);

	nd->rx_pool_free--;
	if (++nd->rx_pool_in_use > nd->rx_pool_in_use_max)
		nd->rx_pool_in_use_max = nd->rx_pool_in_use;
	nd->rx_pool_gets++;

	spin_unlock_irqrestore(&nd->rx_pool_lock, flags);

	SSI_ASSERT(nrxd->class == RXD_CLASS_CLONE);

	/*
	 * set interesting RX descriptor fields.
	 */
	nrxd->conn = src_rxd->conn;
	nrxd->nob = src_rxd->nob;
	nrxd->ib_wc_type = src_rxd->ib_wc_type;
	nrxd->status = src_rxd->status;

	/*
	 * copy received payload 
	 */
	memcpy((void*)nrxd->payload, (void*)src_rxd->payload, src_rxd->nob);

	return nrxd;
}


#if KSVR_TASKLET

void ksvr_tasklet(unsigned long unused)
{
	ib_nexus_t	*nd = &ib_nexus;
       	kib_rx_t	*krx;
        ulong           flags;


        while (TRUE) {
		krx = NULL;
       		spin_lock_irqsave(&nd->ksvr_lock, flags);

		if (! list_empty(&nd->ksvr_workQ)) {
			krx = list_entry(nd->ksvr_workQ.next, kib_rx_t,link);
			list_del (&krx->link);
			nd->ksvr_workQ_cnt--;
		}
		else {
			nd->ksvr_workQ_cnt = 0;
		}
		spin_unlock_irqrestore(&nd->ksvr_lock, flags);

		if (krx == NULL) return;

		/* repost recv buffer on error */
		if (krx->status) {
			kib_post_recv_wr(krx);
			continue;
		}
		ics_dgram(krx);	// process the received ICS message.
	}
}

DECLARE_TASKLET(Ksvr_tasklet,ksvr_tasklet,0);

#endif


#if KSVR || KSVR_TASKLET

/*
* NAME
*	ksvr_queue_work -- queue a work element for the ksvr thread to process
* DESCRIPTION
*	Queue a work element on the ksvr thread(s) work queue. The work element
*	normally is embedded in a larger parent structure; typically a recv
*	descriptor; all received packet processing is done in a process/thread
*	context.
* PARAMETERS
*    *rxd
*	[in] IB receive message descriptor
* RETURN VALUE
*	void
*/

void
ksvr_queue_work(struct _kib_rxd *rxd)
{
	ib_nexus_t *ibd = &ib_nexus;

	spin_lock(&ibd->ksvr_lock);

	list_add_tail(&rxd->link, &ibd->ksvr_workQ);

	if ( ++ibd->ksvr_workQ_cnt > ibd->ksvr_workQ_max )
		ibd->ksvr_workQ_max = ibd->ksvr_workQ_cnt;
#if KSVR_TASKLET
	if (ibd->ksvr_workQ_cnt <= 1)
		tasklet_schedule(&Ksvr_tasklet);
#else
	if (waitqueue_active(&ibd->ksvr_waitQ))
		wake_up_interruptible(&ibd->ksvr_waitQ);
#endif
	spin_unlock(&ibd->ksvr_lock);
}
#endif


#if KSVR

/****f* ksvr_processor
* NAME
*	ksvr_thread -- A kernel server work queue processing thread
* DESCRIPTION
*	Interrupt level packet receive routines enqueue the received packet on
*	the ksvr work queue and wake a ksvr thread to process the received
*	packet.
* SYNOPSIS
*	void
*	ksvr_thread (void *arg) 
* PARAMETERS
*    *arg
*	[in] not used.
* RETURN VALUE
*	VOID
* NOTES
*	Device interupts are blocked when checking if the ksvr work queue is
*	empty and when dequeuing an element.
*	Multiple ksvr threads can be servicing the single ksvr work queue.
*	Typically, a ksvr thread per physical processor.
********/

int
ksvr_thread(void *arg)
{
	ib_nexus_t	*nd = &ib_nexus;
       	kib_rx_t	*krx;
        ulong           flags;
        int             rc;
        int             counter = 0;
        int             did_something;
	mm_segment_t	oldfs;

        nsc_daemonize();
	(void)set_daemon_prio(MAX_RT_PRIO-3, SCHED_FIFO);
        
	oldfs = get_fs();

	dprintk(DBG_RECV,("%s%s on cpu %d\n",WHOAMI,smp_processor_id()));

        atomic_inc (&nd->ksvr_nthreads);

        while (nd->ksvr_run) {
                did_something = FALSE;

                if (nd->ksvr_workQ_cnt > 0) {
			/*
			 * above test is a 'hint', check for real with
			 * interrupts disabled. If work queue is not empty,
			 * remove from the Queue head, interrupt enqueues at
			 * the tail.
			 */

			krx = NULL;
        		spin_lock_irqsave(&nd->ksvr_lock, flags);

			if (! list_empty(&nd->ksvr_workQ)) {
				krx = list_entry(nd->ksvr_workQ.next,
							kib_rx_t,link);
				list_del (&krx->link);
				nd->ksvr_workQ_cnt--;
			}
			else
				nd->ksvr_workQ_cnt = 0;

                        spin_unlock_irqrestore(&nd->ksvr_lock, flags);

			/*
			 * this can occur as the test ksvr_workQ_cnt > 0 is
			 * lightweight (a hint) and SMP unprotected. The idea
			 * to skip interrupt disable/enable to discover an
			 * empty work queue.
			 */
			if ( krx == NULL ) continue;

			if (krx->status) {
				kib_post_recv_wr(krx);
				continue;
				//async_destroy_conn( krx->conn );
			}
			set_fs(KERNEL_DS);
			ics_dgram(krx);
			set_fs(oldfs);
                        did_something = TRUE;
                }

		/*
		 * populate the Global RX buffer pool. Per connection RX
		 * resources (rxd & recv buffers) are allocated when a
		 * connection is constructed. The Golbal RX pool is a saftey
		 * net when per connection RX rxd/buffers are exhausted.
		 * A connection suffering from RX resource exhaustion will
		 * pull RX buffers from this pool and return them when the
		 * transitent RX demand has subsided. Infiniband CQ handling
		 * (WC recv) will pull from the the global pool when the
		 * number of connection RX buffers has passed below a LOW
		 * water mark.
		 */

		/*
		 * Time for an RX pool growth spurt? Is the Global RX pool
		 * count below the Global RX pool replenish threshold?
		 * clms master node gets larger RX free pool to handle remote
		 * node probes.
		 */
		if (nd->rx_pool_free <= CONN_RX_BUF_LO_WATER) {
			ksvr_grow_rx_pool(nd,(this_node == clms_master_node
						? (RX_GPOOL_ELEM_CNT*3)
						: RX_GPOOL_ELEM_CNT));
		}

                /* nothing to do or hogging CPU */

                if (!did_something || counter++ >= 100) {
                        counter = 0;
                        if (!did_something) {
                                rc = wait_event_interruptible( nd->ksvr_waitQ,
						(nd->ksvr_run == 0) ||
						(nd->ksvr_workQ_cnt > 0) );
                        }
			else if (test_tsk_thread_flag(current,TIF_NEED_RESCHED))
                                schedule();
                }
        }
	atomic_dec(&nd->ksvr_nthreads);

        return SUCCESS;
}

/* flag threads to terminate, wake them and wait for them to die */

void
ksvr_thread_stop(void)
{
	int	loops = 5;

	if ( ib_nexus.ksvr_run == 0 ) {
		printk("%s%s ksvr thread !running?\n",WHOAMI);
		return;
	}
	ib_nexus.ksvr_run = 0;

	wake_up_all(&ib_nexus.ksvr_waitQ);

	while (loops-- > 0 && atomic_read(&ib_nexus.ksvr_nthreads) != 0) {
		dprintk(DBG_SHUTDOWN,("waiting for %d threads to terminate\n",
				atomic_read (&ib_nexus.ksvr_nthreads)));
		set_current_state (TASK_INTERRUPTIBLE);
		schedule_timeout (HZ);
	}
}
#endif	// KSVR


static void
wake_tx_waiter(ib_conn_t *conn)
{
        //if ( waitqueue_active(&conn->waitQ) ) {
                wake_up(&conn->waitQ);
        //}
}

static void
queue_receive_data(ib_conn_t *conn, kib_rx_t *rxd)
{
        ulong           flags;

	//dprintk(DBG_CONN_V,("%s ib_conn %p nob %d\n",__func__,conn,rxd->nob));
        spin_lock_irqsave(&conn->rmq_lock,flags);

        list_add_tail(&rxd->link, &conn->rmq);

        if ( ++conn->rmq_cnt > conn->rmq_max )
                conn->rmq_max = conn->rmq_cnt;

	if (waitqueue_active(&conn->rmq_waitQ))
                wake_up(&conn->rmq_waitQ);

        spin_unlock_irqrestore(&conn->rmq_lock, flags);
}


/*
* DESCRIPTION -- process a single IB work complete entry
*	An Infiniband work-complete element has been placed in the completion
*	queue associated with this connection's queue-pair. Here the
*	work-complete descriptor is decoded and processed for events like
*	packet reception, packet send completed. RDMA read/write completions
*	are not signalled but are posted with a 2nd work-requesting of a
*	buffered send. The buffered send is signalled and will generate a CQ
*	entry which gets control here.
* PARAMETERS
*    *cq
*	[in] IB completion queue event descriptor (aka work complete element).
* RETURN VALUE
*	VOID
* NOTES
*	Called in device interrupt/tasklet context. Received packets are queued
*	to the ksvr work queue and will be processed in a thread context by 
*	a ksvr thread.
**/
void
process_cq_event(struct ib_wc *WC)
{
	int			ibs;
	kib_tx_t		*ktx;
	kib_rx_t		*krx;
	ib_conn_t		*conn;
	ib_nexus_t		*nd;
	ib_rdma_status_t	*rs;

	if (unlikely(WC == NULL)) {
		printk("%s%s CQ NULL?\n",WHOAMI);
		return;
	}

	if (unlikely(WC->status != IB_WC_SUCCESS)) {
		if (WC->status == IB_WC_WR_FLUSH_ERR) {
			/* fix an openIB bug - opcode is trashed */
			WC->opcode = IB_WC_RECV;
		}
		else {
			printk("%s op %s status '%s'\n", __func__,
					ib_get_wc_type_text(WC->opcode),
					ib_get_wc_status_str(WC->status));
		}
	}

#if 0 // XXX verify need

	// IBAL/Mellanox-driver RDMA errors are reported as WC_SEND, not
	// RDMA_xxx? fix this.

	if (WC->status != IB_WC_SUCCESS && WC->opcode == IB_WC_SEND) {
		ktx = (kib_tx_t *) (ulong)WC->wr_id;

		if ( ktx->wrt == WR_RDMA_READ )
			WC->wc_type = IB_WC_RDMA_READ;
		else if ( ktx->wrt == WR_RDMA_WRITE )
			WC->wc_type = IB_WC_RDMA_WRITE;
	}
#endif

	/*
	 * dispatch based on Work Complete type
	 */

        switch (WC->opcode) {
#if 0
	  case IB_WC_RDMA_WRITE:
		/*
		 * Should not get here as CQ event signal has been disabled
		 * for RDMA writes.
		 * Local side has issued an RDMA.write to fulfill a received
		 * portals GET with a payload > RDMA_threshold.
		 */
		ktx = (kib_tx_t *) (ulong)WC->wr_id;

		if ( WC->status != IB_WC_SUCCESS ) {
			CWARN("WC_RDMA_WRITE txd %p err '%s'\n",
					ktx, ib_get_wc_status_str(WC->status));
			ksvr_queue_work( &ktx->wqe );
		}
		else
			CWARN("WC_RDMA_WRITE txd %p why?\n",ktx);

		break;
#endif
	  case IB_WC_RDMA_READ:
		/*
		 * Local side has issued an RDMA.read to fulfill a received
		 * ISC message with a payload OOL data > RDMA_threshold.
		 */
		ktx = (kib_tx_t *) (ulong)WC->wr_id;
		Xdprintk(DBG_RDMA,("%s WC_RDMA_READ txd %p nob %d\n",
					__func__,ktx,ktx->nob));
		conn = ktx->conn;
		if (BAD_MAGIC(conn)) {
			CERROR("BAD RDMA_READ context MAGIC\n");
			return;
		}
		atomic_dec(&conn->tx_posted);
		if (ktx->status == NO_STATUS)
			ktx->status = WC->status;	// return send status

		if (unlikely(WC->status != IB_WC_SUCCESS)) {
			printk("%s WC_RDMA_READ txd %p err '%s'\n",__func__,
					ktx, ib_get_wc_status_str(WC->status));

			if (IB_GET_CONN_STATE(conn) == IBA_CONNECTED) {
				IB_SET_CONN_STATE(conn,IBA_ERROR);
			}
			conn->error = ktx->status;
			wake_tx_waiter(conn);
			return;
		}

		/* wakeup RDMA.read waiter */
		conn->error = ktx->status;
		wake_tx_waiter(conn);
		return;

	  case IB_WC_SEND:
		/*
		 * handle send CQ event handler. Handle buffered sends, some
		 * which come after an RDMA read/write operation. The send op
		 * after RDMA will fail if the RDMA failed.
		 */
		ktx = (kib_tx_t *) (ulong)WC->wr_id;
		SSI_ASSERT(ktx);

		conn = ktx->conn;
		if (BAD_MAGIC(conn)) {
			CERROR("BAD SEND_WR context MAGIC\n");
			SSI_ASSERT(!BAD_MAGIC(conn));
			return;
		}
		atomic_dec(&conn->tx_posted);
		if (ktx->status == NO_STATUS)
			ktx->status = WC->status;	// return send status
#if 0
		// bug in Mellanox driver, WC->byte_len not set on SUCCESS send.

		if (ktx->nob != WC->byte_len) {
			printk("%s%s IBA send expected[%d] actual[%d] bytes\n", 
				WHOAMI, ktx->nob, WC->byte_len);
			CWARN("IBA send expected[%d] actual[%d] byte\n",
				ktx->nob, WC->byte_len);
		}
#endif
		/*
		 * Infiniband send errors? finalize in thread context.
		 */
		if (unlikely(WC->status != IB_WC_SUCCESS)) {
			/*
			 * RDMA work-requests are followed by a buffered send
			 * work-request. If the RDMA has an error, then the
			 * buffered send is flushed. Since both IB work-requests
			 * point at the same TXD, do not finalize the TXD
			 * twice.
			 */
			if (WC->status == IB_WC_WR_FLUSH_ERR)
				return;
			CWARN("TX CQ err %s conn %p\n",
					ib_get_wc_status_str(WC->status),
					ktx->conn);

			if (IB_GET_CONN_STATE(conn) == IBA_CONNECTED) {
				IB_SET_CONN_STATE(conn,IBA_ERROR);
			}
			conn->error = ktx->status;
			wake_tx_waiter(conn);
			return;
		}

		/* Does this IB message require RDMA.read(s) from the receiver
		 * in order to receive all the OOL data segments?
		 */
		if (ktx->private) {
			/*
			 * start timeout timer on RDMA transaction. Timer is
			 * deleted when a RDMA status (immediate data flagged)
			 * message is received which references this TX
			 * descriptor.
			 */
			extern void ib_ool_rdma_timeout(ulong);
			init_timer(&ktx->timeout_timer);
			ktx->timeout_timer.function = ib_ool_rdma_timeout;
			ktx->timeout_timer.data = (ulong)ktx;
			ktx->timeout_timer.expires = jiffies+(RDMA_TIMEOUT*HZ);
			add_timer(&ktx->timeout_timer);
		}

		wake_tx_waiter(conn); /* release from ib_sendmsg() wait */

		return;

	  case IB_WC_RECV:

		krx = (kib_rx_t *) (ulong)WC->wr_id;
		SSI_ASSERT(krx);
		conn = krx->conn;	// IB connection received on.

		if (BAD_MAGIC(conn)) {
			CERROR("BAD RECV WR context MAGIC, corrupt wr_id?\n");
			return;
		}

		// 1 less recv buffer posted

		atomic_dec(&conn->rx_posted); // 1 less RX buf posted

		{/* /proc stats accounting */
			int rxp;
			rxp = atomic_read(&conn->rx_posted);
			if ( rxp < (int)conn->min_rx_posted) {
				conn->min_rx_posted = (u32)rxp;
			}
		}

		krx->ib_wc_type = WC->opcode;
		nd = conn->ib_nexus;

		// any IB ERROR conditions?

		krx->status = WC->status;	// record the status

		if (unlikely(WC->status != IB_WC_SUCCESS)) {
			krx->nob = 0;

			// actual IB error or connection shutdown drain?
			if ( WC->status != IB_WC_WR_FLUSH_ERR ) {
				CWARN("RX CQ err %s conn %p rx_posted %d\n",
					ib_get_wc_status_str(WC->status),
					krx->conn,
					atomic_read(&conn->rx_posted));

				IB_SET_CONN_STATE(conn,IBA_ERROR);
				conn->error = krx->status;
				wake_tx_waiter(conn);
				return;
			}

			// shutting down this connection, waiting for posted
			// receives to drain?
			if (atomic_read(&conn->rx_posted) <= 0) {
				dprintk( DBG_INTR, ("%s%s rx_posted %d\n",
					WHOAMI,atomic_read(&conn->rx_posted)));

				// if all posted rx buffers are back, then
				// wake waiter.
				if (waitqueue_active(&conn->waitQ)) {
					dprintk(DBG_INTR,
					("%s%s wakeup rx_posted == 0 waiter.\n",
								WHOAMI));
					wake_up_interruptible(&conn->waitQ);
				}
			}
			return;
		}

		// No IB Receive errors

		/*
		 * Immediate data present? If so, then its presence flags the
		 * existance of RDMA read status message. Extract out the
		 * RDMA.read status indicator and the address of the sending
		 * TX descriptor. Wake the waiting send/reply thread.
		 */
		if ( WC->wc_flags & IB_WC_WITH_IMM ) {
			int *expected_rdma_ops;

			rs = (ib_rdma_status_t*)krx->payload;
			if ( rs->magic != RDMA_STATUS_MAGIC ) {
				printk("%s%s ERR BAD rdma read status magic "
					"0x%x\n", WHOAMI,rs->magic);
				return;
			} 
			ktx = rs->txd;
			expected_rdma_ops = ktx->private;
#if 0
			printk("%s RDMA status: waiting txd %p RDMA.read "
				"status %d RDMA op cnt %d expected op cnt %d\n",
				__func__,ktx,rs->status,rs->rdma_op_cnt,
				*expected_rdma_ops);
#endif
			if (rs->status) {
				/* Set <0 error code so waiter will wakeup */
				*expected_rdma_ops = -rs->status;
			}
			else {
				// 1 rmda status msg with total RDMA.read cnt.
#if 0
				if (rs->rdma_op_cnt != 1) {
					printk("%s rdma_op_cnt(%d) != 1 ?\n",
						__func__,rs->rdma_op_cnt);
				}
#endif
				*expected_rdma_ops -= rs->rdma_op_cnt;
			}

			if (*expected_rdma_ops <= 0) {
				// cancel RDMA transaction timeout timer.
				del_timer_sync( &ktx->timeout_timer );
				ktx->timeout_timer.expires = 0;
				wake_up(&conn->waitQ);
			}

			// repost RX descriptor
			if ( (ibs = kib_post_recv_wr(krx)) ) {
				CWARN("rxd post failure '%d'\n",ibs);
			}
			return;
		}

		krx->nob = WC->byte_len;
		conn->buf_rx_bytes += WC->byte_len;

		/*
		 * check RX buffer/desc resource consumption. IB 'always' wants
		 * at least 1 posted RX buffer.
		 */

		if ((conn->rbcnt == IBA_RECV_WR) &&
		    (atomic_read(&conn->rx_posted) < CONN_RX_BUF_LO_WATER) &&
		    (IB_GET_CONN_STATE(conn) == IBA_CONNECTED) )
		{
			kib_rx_t *nrxd;

			/*
			 * grab an RX descriptor from the global pool and copy
			 * the received contents to the cloned rxd. Repost the
			 * src rxd, queue cloned rxd for ksvr processing.
			 */
			nrxd = clone_rxd(nd,krx);
			printk("%s conn %p below RX low-water(%d), clone %p\n",
				__func__,conn,CONN_RX_BUF_LO_WATER,nrxd);
			if (nrxd) {
				// repost the original RX descriptor
				if ((ibs = kib_post_recv_wr(krx))) {
					CWARN("rx post failure '%d'\n",ibs);
				}
				krx = nrxd;	// go process the clone
			}
			else
				printk("%s conn %p unable to clone rxd\n",
						__func__,conn);
		}

		if (conn->rmq_receive) {
			/*
			 * interrupt/tasklet context, enqueue RX descriptor to
			 * the connection direct recv Q and wake any RX waiters.
			 */
			queue_receive_data(conn,krx);
		}
		else {
			/* upcall to deliver the ICS message */
#if KSVR || KSVR_TASKLET
			// must match ics_ib_config.c ksvr...
			ksvr_queue_work(krx);
#else
			ics_dgram(krx);
#endif
		}
		return;

	  default:
		// NOT SUPPORTED
		CERROR("Unsupported IB wc type %d (0x%x)\n",
						WC->opcode, WC->opcode);
		break;
	}
}


/*
* NAME
*	ib_completion_cb -- Completion Queue event callback
* DESCRIPTION
*	This callback routine is invoked by the Access Layer when a completion
*	queue event has been inserted into the CQ for a specific IB connection.
*	Each connection has a single CQ for both TX and RX.
*	Process all events in the CQ before returning from the tasklet context
*	function.
* PARAMETERS
*    *cq
*	[in] pointer to Completion queue with completion(s) to process
* RETURN VALUE
*	VOID
* SIDE EFFECTS
*	CQ is emptied, CQ events are processed.
*/

void
ib_completion_cb(struct ib_cq *cq, void *cq_context)
{
	int		rc;
	ib_conn_t	*kconn;
	bool_t		do_rearm;
	struct ib_wc	*wc, work_complete[MAX_CQ_WC];

	kconn = (ib_conn_t*)cq_context;
	if ( BAD_MAGIC(kconn) ) {
		CERROR("BAD CQ context MAGIC\n");
		return;
	}

	// expected Completion Queue?
	if (cq != kconn->cq) {
		CERROR("Bad CQ(%p) wanted kconn->cq(%p)?\n",cq,kconn->cq);
		return;
	}
	do_rearm = TRUE;

recheck:
	do {
		/* loop on CQ completions */
		rc = ib_poll_cq(cq, MAX_CQ_WC, work_complete);
		if (unlikely(rc < 0)) {
			CERROR("ERR - ib_poll_cq [%d]\n",rc); 
        		(void)ib_req_notify_cq(cq, IB_CQ_NEXT_COMP);
                	return;
		}

		/*
		 * If there is nothing to process and the cq is rearmed, we're
		 * done.
		 */
		if (rc == 0 && do_rearm == FALSE) {
			Xdprintk((DBG_INTR_V),
				("%s%s CQ empty & re-armed\n",WHOAMI));
			return;
		}
		do_rearm = TRUE;

		/* process completions */
		for(wc=work_complete; wc < &work_complete[rc]; wc++) {
			process_cq_event(wc);
		}

		/* Completion Queue event stats */
		if (rc > kconn->cq_max_per_poll)
			kconn->cq_max_per_poll = rc;
		kconn->cq_events += rc;
		kconn->cq_polls++;

		/*
		 * if rc == MAX_CQ_WC, then CQ has been drained, otherwise
		 * unprocessed CQ entries remain, process more.
		 */
	} while (rc == MAX_CQ_WC);

	/* rearm the cq */

        if ((rc=ib_req_notify_cq(cq, IB_CQ_NEXT_COMP))) {
                printk("%s ERR - unable to re-arm CQ %d\n",__func__,rc);
                return;
        }

	/*
	 *  Pickup any completed events that were pending for this rearm
	 */
	do_rearm = FALSE;
	goto recheck;
}



static void
ib_readmsg_timeout(ulong data)
{
	ib_conn_t	*ibc = (ib_conn_t*)data;

	/* actually a timeout: set the Infiniband work-complete (WC) error
	 * & queue for lib finalize + IB mem release in a thread context.
	 */

	if (waitqueue_active(&ibc->rmq_waitQ)) {
		ibc->error = IB_WC_RESP_TIMEOUT_ERR;
		wake_up(&ibc->rmq_waitQ);
	}
}

/*
 * Read the next IB message from the connection queue of received messages.
 * Used immediately after an IB connection is started prio to switching over
 * to ics_dgram() IB message reception/processing.
 * Inputs:
 *	*conn	[in] IB connection descriptor
 *	**rrxd	[out] IB receive descriptor containing the IB message data
 *	wait_secs - [in] read request times out after this many seconds.
 *
 * Return Value:
 *	0 == SUCCESS, else IB error code.
 * Side Effects:
 *	*rrxd written with the address of the received IB message which has
 *	been dequeued from the connection struct. Once read, the caller
 *	is responsible to repost the RX descriptor for receive.
 */

int
ib_readmsg(ib_conn_t *conn, kib_rx_t **rrxd, u32 wait_secs)
{
	kib_rx_t *rxd;
	ulong	 flags;

	//dprintk(DBG_RECV_V,("%s wait_secs %d\n",__func__,wait_secs));

	/* check if client message has arrived. */
	if (conn->rmq_cnt <= 0) {
		/* wait for mesg */
		conn->error = 0;
		if (wait_secs) {
			/*
			 * start timeout timer on recv transaction. Timer is
			 * deleted when RX data arrives
			 */
			init_timer(&conn->timeout);
			conn->timeout.function = ib_readmsg_timeout;
			conn->timeout.data = (ulong)conn;
			conn->timeout.expires = jiffies + (wait_secs*HZ);
			add_timer(&conn->timeout);
		}
		wait_event(conn->rmq_waitQ,(conn->error||(conn->rmq_cnt != 0)));

		if (wait_secs) {
			if (conn->error == IB_WC_RESP_TIMEOUT_ERR) {
				/* read time-out */
				dprintk(DBG_RECV,("%s read time-out(secs %d)\n",
							__func__,wait_secs));
				return conn->error;
			}
			else {
				// cancel - remove IB read timeout timer.
				del_timer_sync(&conn->timeout);
				conn->timeout.expires = 0;
			}
		}
	}

	/*
	 * remove message from this connection's received message queue.
	 */
	spin_lock_irqsave (&conn->rmq_lock, flags);

	rxd = list_entry(conn->rmq.next,kib_rx_t,link);
	list_del (&rxd->link);
	conn->rmq_cnt--;

	spin_unlock_irqrestore(&conn->rmq_lock, flags);

	*rrxd = rxd;	/* return RX descriptor address */

	//dprintk(DBG_RECV_V, ("%s ib_conn %p rxd %p status %d read %d bytes\n",
	//			__func__,rxd->conn,rxd,rxd->status,rxd->nob));
	return rxd->status;
}


/*
 * Send an IB message over the specified IB connection. Wait for the Send
 * work-request completion notification (see process_cq @ WC_SEND) to wake
 * this thread.
 *
 * Inputs:
 *	*txd	IB TX descriptor which contains send data
 *	cnt	number of bytes to send.
 * Return Value:
 *	0 == SUCCESS
 *	otherwise negative errno code or positive IB Work-complete error code.
 * Notes:
 *  Caller is responsible for returning the TX descriptor back to the free pool.
 */

int
ib_sendmsg(kib_tx_t *txd, u32 cnt)
{
	ib_conn_t		*kconn;
	int			rc;
        struct ib_send_wr	sq, *err_wr;
        struct ib_sge		sgl[MAX_SGL_ENTRIES];

	/* IB connection to send on */
	kconn = txd->conn;

	/* connection state valid for xmit? */
	if (IB_GET_CONN_STATE(kconn) != IBA_CONNECTED) {
		printk("%s conn(%p) ! CONNECTED? '%s'\n",__func__,kconn,
					IB_conn_state_str(kconn));
		return -ENOTCONN;
	}

        if (in_interrupt()) {
                CWARN("Interrupt context?\n");
                return -EDEADLK;
        }

	/* init IB send data segment descriptor */
	memset((void*)&sq,0,sizeof(sq));

	sgl->addr = (u64)(ulong)txd->dma_addr;
	sgl->length = cnt;
	sgl->lkey = txd->lkey;

	/* setup send work-request */
	txd->nob = send_wr_setup(txd, &sq, 1, sgl, IB_WR_SEND);

	/*
	 * post IB send operation - upon success txd is returned to pool
	 * during CQ (Completion Queue processing).
	 */
	atomic_inc(&kconn->tx_posted);
	if ((rc=atomic_read(&kconn->tx_posted)) > kconn->max_tx_posted)
		kconn->max_tx_posted = (u32)rc;

	//dprintk(DBG_SEND_V,("%s xmit(%d) bytes txd.nob(%d)\n",
	//	__func__,cnt,txd->nob));	

        txd->status = NO_STATUS;
        rc = ib_post_send(kconn->cm_id->qp, &sq, &err_wr);

        if (rc != SUCCESS) {
                CWARN("IBA send[%s] failed(%d) to [%s]\n",
			wr_type_text[sq.opcode],rc,nid_str(kconn->r_hid.nid));
		return -rc;
        }

	/*
	 * wait for an IB send completion event which will wake this thread.
	 */
	wait_event(txd->conn->waitQ,(txd->status != NO_STATUS));

	if (txd->status == SUCCESS) {
		kconn->buf_tx_op++;
		kconn->buf_tx_bytes += txd->nob;
	}
	else {
		printk("%s err %d(0x%x) send %d bytes\n",
			__func__,txd->status,txd->status,txd->nob);	
	}
	return (int)txd->status;
}

/*
 * Setup DMA mapping in preparation for a remote node to issue an RDMA.read
 * operation which specifies this memory region.
 * All kernel physical memory is registered with an HCA. Get the DMA/bus addr
 * along with remote access protection key. 
 *
 * Args:
 *  *buf [in/out] kernel memory start address.
 *  nob [in] number-of-bytes to map, RDMA.read length
 *  dma_dir [in] TO/FROM DMA device.
 *  *rdma_adrs [out] DMA bus address used by the remote side as the RDMA address
 *		to read from.
 *  *rseg [in/out] RDMA segment descriptor used to release/unmap memory
 *		after the RDMA.read operation.
 *
 * Return Value:
 *  0 == SUCCESS, else errno code
 */			

int
ib_rdma_setup(u_char                  *buf,
              int                     nob,
              enum dma_data_direction dma_dir,
              ulong                   *rdma_adrs,
              ib_rdma_seg_t           *rseg)
{
	ib_nexus_t *ibn = &ib_nexus;
	dma_addr_t daddr;
	struct page *page;
	ulong offset;

	rseg->nob = (size_t)nob;
	rseg->baddr = NULL;
	rseg->mr = ibn->kmr; // kernel DMA memory region

	if ((ulong)buf < (ulong)VMALLOC_START) {
		/* common case, ! (HIGHMEM or USERHIGHMEM)
		 * get DMA address for the RDMA buffer
		 */
		daddr = dma_map_single(ibn->device->dma_device,
					(void*)buf,
					(size_t)nob,
					dma_dir);
	}
	else {
		/*
		 * handle highmem addresses which do not map correctly via
		 * dma_map_single().
		 */
		page = vmalloc_to_page((void*)buf);
		rseg->baddr = (void*)page; // flag page mapping
		if (!page) {
			CWARN("<null> *page?\n");
			return -ENOMEM;
		}
		offset = (ulong)buf & ~PAGE_MASK;
		daddr = dma_map_page(ibn->device->dma_device,
					page,
					offset,
					(size_t)nob,
					dma_dir);
	}

	pci_unmap_addr_set(rseg,mapping,daddr);
	pci_unmap_len_set(rseg,mlen,nob);

	// bus address delivered to remote node for RDMA.read
	*rdma_adrs = (ulong) daddr;

       	return (dma_mapping_error(daddr) ? -ENOMEM : SUCCESS);
}

/*
 * Post an Infiniband RDMA.read operation with an optional RDMA.read status
 * message send. If the RDMA.read fails the RDMA.read status message will not be
 * sent; RDMA.data_src side times out initial ics_llsendmsg().
 * Sending an RDMA status message (EOM != 0) will decrement the RDMA.read count
 * by the (rdma_op_cnt=EOM) field in the rdma_status message. Normally a single
 * status message is sent with the rdma_op_cnt field equal to the number of
 * expected rdma.read operations the ICS message sender is waiting on.
 * On the llsendmsg() side (rdma.data_src), once the RDMA.count has transitioned
 * to <= 0, the waiting ICS message sender is wakened. When RDMA.count is zero,
 * the ICS message sender knows all RDMA.reads have completed successfully,
 * thus completing the ICS message send (see ics_llsendmsg).
 *
 * Inputs:
 *  *conn	IB connection descriptor
 *  *buf	local buffer where RDMA.read data lands
 *  len		length of the RDMA.read
 *  *laddr	IB HCA DMA bus address representing the local buffer
 *  *od		IB OOL data descriptor which arrived in IB message; contains the
 *		RDMA.read address and rkey for the remote memory.
 *  rdma_cnt	Number of RDMA.reads this side has performed for the current
 *		IB message. IB message sender is expecting 'rdma_cnt' RDMA.reads
 *		to transpire in order to understand all the IB message data
 *		has been sent/transferred.
 *  *waiting_txd remote node's address of the sending TX descriptor. Carried
 *		 in the RDMA status message so remote side can locate and
 *		 wakeup the IB message sender to complet the IB send operation.
 *  EOM		End-Of-Message flag, 0 == do not post the RDMA status message.
 *		Otherwise, EOM represents the # of successful RDMA.read ops
 *		performed for the current ICS message.
 * Return Value:
 *  0 == SUCCESS, else IB error code.
 * Side Effects:
 *  Wait for the IB CQ (Completion Queue) processing routine to wake this
 *  thread after the RDMA.status message transmission or RDMA.read failure.
 * Notes:
 *  TX descriptor is acquired and release within this routine!
 */

int
ib_rdma_read(
	ib_conn_t      *conn,
        char           *buf,
        int            len,
        ulong          *laddr,
	u32		lkey,
        ib_ool_desc_t  *od,
        kib_tx_t       *waiting_txd,
        int            EOM )
{
	int			rc;
	kib_tx_t		*txd;
        struct ib_send_wr    	sq, sq1, *err_wr;
        struct ib_sge   	ds[MAX_SGL_ENTRIES], ds1[MAX_SGL_ENTRIES];
	ib_rdma_status_t	*rrs;

// 	dprintk(DBG_RDMA,("%s start nob %d EOM %d\n",__func__,len,EOM));

        if (in_interrupt()) {
                CWARN("Interrupt context?\n");
                return IB_WC_GENERAL_ERR;
        }

	/* acquire a TX descriptor & buffer */
	if ((txd=get_txd(conn,TRUE)) == NULL) {
		return IB_WC_LOC_PROT_ERR;
	}

	/*
	 * set IB data segment descriptor for local sink/destination of the
	 * RDMA.read operation.
	 */
	ds[0].addr = (u64) (*laddr);
	ds[0].length = len;
	ds[0].lkey = lkey; // local memory IB access Key

	// set remote RDMA.read information in IB work-request

	memset((void*)&sq,0,sizeof(sq));

	/*
	 * od.ool_rdma_addr is an IB HCA assigned address resultant from
	 * the remote memory registration call during the ics ool data argument
	 * marshalling operation (encode_ool). The remote ool data RDMA
	 * information arrived in the received ics message.
	 */
	sq.wr.rdma.remote_addr = od->ool_rdma_addr; // remote OOL data(HCA addr)
	sq.wr.rdma.rkey = od->ool_rkey;	// remote access key

	/*
	 * set RDMA.read work-request
	 */

	txd->nob = send_wr_setup(txd, &sq, 1, ds, IB_WR_RDMA_READ );

	/*
	 * Time to set a cumulative RMDA.read error status message?
	 */
	if (EOM) {
		/*
		 * Link RDMA.read send work-request to the 2nd IB work-request.
		 * Disable CQ event signalling for RDMA.read, CQ event will be
		 * enabled for the Immediate data send.
		 * Effect is a single CQ event for the entire SUCCESSFUL
		 * RDMA.read & RDMA status send. An RDMA error generates two
		 * CQ entries: one RDMA error with an IB_FLUSHED for the
		 * RDMA.status send.
		 */
		sq.next = &sq1;
		sq.send_flags &= ~IB_SEND_SIGNALED;

		/*
		 * send IB immediate data to the remote side to notify the
		 * remote side an RDMA.read has completed. Remote side is
		 * waiting for 'n' RDMA.reads to transpire before it's IB send
		 * is completed. see WC_RECV immediate data handling.
		 */

		// setup rdma read status {txd,status}.

		rrs = (ib_rdma_status_t*) txd->payload;
		rrs->txd = waiting_txd;
		rrs->magic = RDMA_STATUS_MAGIC;
		rrs->status = SUCCESS;
		rrs->rdma_op_cnt = EOM; // # of RDMA operations completed.

		ds1[0].addr = (u64)(ulong)txd->dma_addr;
		ds1[0].length = sizeof(ib_rdma_status_t);
		ds1[0].lkey = txd->lkey;

		memset((void*)&sq1,0,sizeof(sq1));

		txd->nob += send_wr_setup(txd,&sq1,1,ds1,IB_WR_SEND_WITH_IMM);

		/*
		 * flag immediate data and force all pending send operations
		 * (RDMA.read) to complete first, before processing this
		 * Immediate data send.
		 * Immediate data is just a flag; actual immediate data is not
		 * used. The immediate data flags the existance of an RDMA.read
		 * status block in the received message payload bay. The remote
		 * TX descriptor address which was sent with the original IB
		 * message that required the RDMA read is located in the
		 * RDMA.read status block. This TXD address is used to wake
		 * orginal IB message sender.
		 */
		sq1.imm_data = 0xcafebabe;
		sq1.send_flags |= IB_SEND_FENCE;
	}
	else
		sq.send_flags |= IB_SEND_FENCE;

	// post the RDMA.read & RDMA status send operations
	atomic_inc(&conn->tx_posted);
	if ((rc=atomic_read(&conn->tx_posted)) > conn->max_tx_posted)
		conn->max_tx_posted = (u32)rc;

	txd->status = NO_STATUS;
       	rc = ib_post_send(conn->cm_id->qp, &sq, &err_wr);
        if (rc) {
                CWARN("IBA send[%s] failed(%d) to [%s]\n",
                                wr_type_text[sq.opcode], rc,
                                nid_str(conn->r_hid.nid));
		put_idle_txd(txd);
		return rc;
        }

	/* wait for IB send completion event */
	wait_event(txd->conn->waitQ,(txd->status != NO_STATUS));

	if (txd->status == SUCCESS) {
		conn->rdma_sink_op++;           // count RDMA.read operations
		conn->rdma_sink_bytes += len;
	}
	rc = txd->status;
	put_idle_txd(txd);

	if (rc == 0) {
#if CRC
		u32 crc;

		crc = crc32(0,buf,len);
		if (crc != od->ool_rdma_crc) {
			conn->rdma_crc_errs++;
	//		printk("%s conn %p nob %d crc: local %x remote %x\n",
	//			__func__,conn,len,crc,od->ool_rdma_crc);
		}
		else
#endif
			conn->rdma_crc_ok++;
	}
	if (EOM > 1) {
		dprintk(DBG_RDMA,
			("%s finished nob %d @ %p EOM %d crc: err %d good %d\n",
			__func__,len,buf,EOM,conn->rdma_crc_errs,
			conn->rdma_crc_ok));
	}
	return rc;
}


int
send_rdma_read_status(ib_conn_t	*conn,
                      kib_tx_t	*waiting_txd,
                      int	status,
                      int	EOM)
{
	int			rc;
	kib_tx_t		*txd;
        struct ib_send_wr    	sq, *err_wr;
        struct ib_sge   	ds;
	ib_rdma_status_t	*rrs;

// 	dprintk(DBG_RDMA,("%s start nob %d EOM %d\n",__func__,len,EOM));

        if (in_interrupt()) {
                CWARN("Interrupt context?\n");
                return IB_WC_GENERAL_ERR;
        }

	/* acquire a TX descriptor & buffer */
	if ((txd=get_txd(conn,TRUE)) == NULL) {
		return IB_WC_LOC_PROT_ERR;
	}

	/*
	 * send IB immediate data to the remote side to notify the
	 * remote side an RDMA.read has completed. Remote side is
	 * waiting for 'n' RDMA.reads to transpire before it's IB send
	 * is completed. see WC_RECV immediate data handling.
	 */

	/* setup rdma read status */

	rrs = (ib_rdma_status_t*) txd->payload;
	rrs->txd = waiting_txd;
	rrs->magic = RDMA_STATUS_MAGIC;
	rrs->status = status;
	rrs->rdma_op_cnt = EOM; // # of RDMA operations completed.

	ds.addr = (u64)(ulong)txd->dma_addr;
	ds.length = sizeof(ib_rdma_status_t);
	ds.lkey = txd->lkey;

	memset((void*)&sq,0,sizeof(sq));

	txd->nob = send_wr_setup(txd,&sq,1,&ds,IB_WR_SEND_WITH_IMM);

	/*
	 * flag immediate data and force all pending send operations
	 * (RDMA.read) to complete first, before processing this
	 * Immediate data send.
	 * Immediate data is just a flag; actual immediate data is not
	 * used. The immediate data flags the existance of an RDMA.read
	 * status block in the received message payload bay. The remote
	 * TX descriptor address which was sent with the original IB
	 * message that required the RDMA read is located in the
	 * RDMA.read status block. This TXD address is used to wake
	 * orginal IB message sender.
	 */
	sq.imm_data = 0xcafebabe;

	// post the RDMA.read & RDMA status send operations
	atomic_inc(&conn->tx_posted);
	if ((rc=atomic_read(&conn->tx_posted)) > conn->max_tx_posted)
		conn->max_tx_posted = (u32)rc;

	txd->status = NO_STATUS;

       	rc = ib_post_send(conn->cm_id->qp, &sq, &err_wr);

        if (rc != SUCCESS) {
                CWARN("IBA send[%s] failed(%d) to [%s]\n",
                                wr_type_text[sq.opcode], rc,
                                nid_str(conn->r_hid.nid));
		put_idle_txd(txd);
		return rc;
        }

	/* wait for IB send completion event */
	wait_event(txd->conn->waitQ,(txd->status != NO_STATUS));

	rc = txd->status;
	put_idle_txd(txd);

	dprintk(DBG_RDMA,("%s finished EOM %d\n",__func__,EOM));

	return rc;
}
