/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
 */

/*
 * 	Low-level ICS server code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
//#define ICS_DEBUG 1

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/uio.h>
#include <asm/uaccess.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */

/* Forward references. */
static void icssvr_llprocess_msg(svr_handle_t *, struct _kib_rxd *);

/*
 * Data structures dealing with the list of incoming messages that have
 * arrived for which no server handles were available.
 */

typedef struct {
	SOFTIRQ_SPIN_LOCK_T	msglist_lock;
	struct list_head	msglist;
} icsmsg_arrival_t;

icsmsg_arrival_t icsmsg_arrival[ICS_MAX_CHANNELS];

#ifdef 	ICS_DEBUG
/*
 * Don't do throttling if this is set (for debugging purposes).
 */
static int	ics_nothrottle;
#endif		/* ICS_DEBUG */
/*
 * icssvr_llinit()
 *	Perform any relevant server initialization.
 */
void
icssvr_llinit()
{
	int		i;

	for (i = 0; i < ics_num_channels; i++) {
		INIT_SOFTIRQ_SPIN_LOCK(&icsmsg_arrival[i].msglist_lock);
		INIT_LIST_HEAD(&icsmsg_arrival[i].msglist);
	}
}

/*
 * icssvr_llhandle_init()
 *	Initialize a handle that has not been used
 *
 * Description:
 *	This routine is called by ICS high level code to initialize the
 *	low-level ICS portion of a handle.
 *
 *	This routine cannot assume that the low-level portion of the client
 *	handle has been initialized to all zeroes.
 *
 *	If flag is TRUE, then this routine must be called from thread
 *	context. If flag is FALSE, then this routine may be called from
 *	interrupt context.
 *
 * Parameters:
 *	handle		the handle to be initialized
 *	sleep_flag	this routine can sleep waiting for resources/memory
 *
 * Return value:
 *	If sleep_flag is TRUE, then this routine always returns 0 (success).
 *	If sleep_flag is FALSE, then this routine returns -EAGAIN if it
 *	could not allocate resources/memory without sleeping, and 0 (success)
 *	otherwise.
 */
int
icssvr_llhandle_init(
	svr_handle_t	*handle,
	boolean_t	sleep_flag)
{
	struct iovec	*iov;
	struct msghdr	*msg;

	(void)sleep_flag;

	msg = &handle->sh_msghdr;
	iov = handle->sh_iovec;
	iov[0].iov_base = handle->sh_inline;
	msg->msg_name = NULL;
	msg->msg_namelen = 0;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = 0;
	msg->msg_iov = iov;
	msg->msg_iovlen = 0;

	handle->sh_msg_rxd = NULL;
	handle->sh_msg_inline_data_p = NULL;
	handle->sh_msg_ib_ool_info = NULL;
	handle->sh_msg_ib_od = NULL;
	handle->sh_msg_rdma_cnt = 0;

	handle->sh_rep_msghdr_p = msg;
	handle->sh_rep_inline_iov_p = NULL;
	handle->sh_rep_inline_data_p = NULL;
	handle->sh_rep_ooldesc = handle->sh_ooldesc;
	handle->sh_rep_ooldesc_cnt = 0;
	handle->sh_ref_cnt = 0;

	/* Initialize the lock. */
	INIT_SPIN_LOCK(&handle->sh_lllock);

	return 0;
}

/*
 * icssvr_llhandle_init_for_recv()
 *	Prepare a handle so that a message can be received
 *
 * Description:
 *	This routine is called by ICS high level code to prepare a handle
 *	to receive a message from a client.
 *
 *	Note that icssvr_llhandle_init() has been called at least once
 *	for the handle, but that icssvr_llhandle_init_for_recv() can be called
 *	many times for the same handle.
 *
 *	If sleep_flag is TRUE, then this routine must be called from thread
 *	context. If sleep_flag is FALSE, then this routine may be called from
 *	interrupt context.
 *
 * Parameters:
 *	handle		the handle to be initialized
 *	sleep_flag	if TRUE this routine can sleep waiting for
 *			resources/memory
 *
 * Return value:
 *	If sleep_flag is TRUE, then this routine always returns 0 (success).
 *	If sleep_flag is FALSE, then this routine returns -EAGAIN if it
 *	could not allocate resources/memory without sleeping, and 0 (success)
 *	otherwise.
 *
 */

int
icssvr_llhandle_init_for_recv(
	svr_handle_t	*handle,
	boolean_t	sleep_flag)
{
	/*
	 * If there is an unfreed incoming message, then free it.
	 */
	if ( handle->sh_msg_rxd ) {
		printk("%s free rxd %p hdl %p\n",__func__,handle->sh_msg_rxd,handle);
		ics_llfreemsg(handle->sh_msg_rxd);
		handle->sh_msg_rxd = NULL;
	}

	/* Reset iov and ooldesc buffers. */
	if (handle->sh_rep_msghdr_p->msg_iov != handle->sh_iovec) {
		kfree(handle->sh_rep_msghdr_p->msg_iov);
		handle->sh_rep_msghdr_p->msg_iov = handle->sh_iovec;
	}
	handle->sh_rep_msghdr_p->msg_iovlen = 1;
	if (handle->sh_rep_ooldesc != handle->sh_ooldesc) {
		kfree(handle->sh_rep_ooldesc);
		handle->sh_rep_ooldesc = handle->sh_ooldesc;
	}
	handle->sh_rep_ooldesc_cnt = 0;

	handle->sh_rep_inline_iov_p = &handle->sh_rep_msghdr_p->msg_iov[0];
	handle->sh_rep_inline_data_p = handle->sh_rep_inline_iov_p->iov_base +
							sizeof(icshdr_t);
	handle->sh_rep_inline_iov_p->iov_len = sizeof(icshdr_t);

	/* Initialize the reference count */
	handle->sh_ref_cnt = 1;

	return(0);
}

/*
 * Low-level ICS queueing:
 *
 * In my drive to make sure that we didn't have to do any allocations
 * in low-level ICS, I lost sight of the fact that we have a single
 * channel queue at this point for all the nodes.
 */

/*
 * enqueue or process the newly arrived ICS datagram. The entire ICS datagram
 * is contained in the IB buffer, sans any RDMA OOL data.
 *
 * input:
 *	msgl_p	received message queue for the specific channel.
 *	rxd	IB receive descriptor (includes ics datagram in payload)
 *	chan	ICS channel datagram arrived on.
 *
 * implicit inputs:
 *	this channel's msgl_p->msglist_lock is locked.
 *	routine called in IB device interrupt context; NOT a thread context.
 *
 * side effects:
 *	ics datagram is either queued or processed.
 */

static void
icssvr_llenqueue(icsmsg_arrival_t *msgl_p,
		 struct _kib_rxd  *rxd,
		 int              chan)
{
#ifdef NSC_ICSLOG
	icshdr_t *icshdr = LL_ICSHDR(rxd);

	ICSLOG(16, ICSLOG_SVRLLEQ, icshdr->ihdr_transid,
	       icshdr->ihdr_uniqueid,
	       icshdr->ihdr_orignode, chan, 0, /* XXX */
	       list_empty(&msgl_p->msglist));
#endif
	SSI_ASSERT((int)(msgl_p - icsmsg_arrival) == chan);

	list_add_tail( &rxd->link, &msgl_p->msglist );
}

/*
 * seach received message queue, return a pointer to the 1st message which
 * matches the specified 'chan' number.
 * 
 * msgl_p is already channel based, return 0 if no queued messages,
 * else return 1 with a pointer to the 1st message on the list.
 */

static int
icssvr_lldequeue_peek(icsmsg_arrival_t *msgl_p,
		      struct _kib_rxd  **rbufp,
		      int              chan)
{
	kib_rx_t	*rxd;

	(void)chan;
	SSI_ASSERT((int)(msgl_p - icsmsg_arrival) == chan);

	*rbufp = NULL;

	/* If there are no more messages then we are done. */
	if (list_empty(&msgl_p->msglist))
		return 0;

	/* point at the 1st rxd on the received message list */
	rxd = list_entry(msgl_p->msglist.next,kib_rx_t,link);
	*rbufp = rxd;

#ifdef ICS_DEBUG
	{
		icshdr_t	*icshdr;
		/* get the ICS header. */
		icshdr = LL_ICSHDR(rxd);

		if ( icshdr->ihdr_chan != chan ) {
			printk("%s icshdr->ihdr_chan(%d) != requested "
				"chan(%d)\n",__func__,icshdr->ihdr_chan,chan);
		}
	}
#endif
	return 1;
}

/*
 * pull 1st message off the fifo queue.
 */

static void
icssvr_lldequeue(icsmsg_arrival_t *msgl_p,
		 kib_rx_t	  **rxd_p,
		 int		  chan)
{
	kib_rx_t *rxd;
	icshdr_t *icshdr;
#ifdef NSC_ICSLOG
	int	multi = 0;
	int	last = 0;
#endif

	SSI_ASSERT(((int)(msgl_p - icsmsg_arrival)) == chan);

	if (list_empty(&msgl_p->msglist)) {
		printk("%s chan %d - empty msg queue?\n",__func__,chan);
		*rxd_p = NULL;
		return;
	}
	/* remove 1st received IB-message from this channel's list */
	rxd = list_entry(msgl_p->msglist.next,kib_rx_t,link);
	list_del( &rxd->link);
	*rxd_p = rxd;
	icshdr = LL_ICSHDR(rxd);

	ICSLOG(16, ICSLOG_SVRLLDQ, icshdr->ihdr_transid,
			icshdr->ihdr_uniqueid,
			icshdr->ihdr_orignode,
			chan, multi, last);
}

void
icssvr_llnodedown(clusternode_t node, int chan)
{
	icsmsg_arrival_t *msgl_p = &icsmsg_arrival[chan];
	icsmsg_arrival_t msgl;
	kib_rx_t	 *rxd;
	icshdr_t	 *icshdr;

	if (chan >= ics_num_channels)
		return;

	/* Drop all messages on the queue from the down node. */
	LOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);

	ICSLOG(16, ICSLOG_SVRLLND, node, chan, (long)"start", 0, 0, 0);

	msgl = *msgl_p; // XXX? why copy? why not the 'real' queue?

	while (icssvr_lldequeue_peek(msgl_p, &rxd, chan)) {
		icssvr_lldequeue(msgl_p, &rxd, chan);
		icshdr = LL_ICSHDR(rxd);
		if (icshdr->ihdr_orignode == node) {
			ICSLOG(16, ICSLOG_SVRLLNDDROP,
			       icshdr->ihdr_transid,
			       icshdr->ihdr_uniqueid,
			       icshdr->ihdr_orignode, chan,
			       0 /* XXX */,
			       list_empty(msgl_p->msglist));
			ics_llfreemsg(rxd);
		} else
			icssvr_llenqueue(msgl_p, rxd, chan);
	}
	ICSLOG(16, ICSLOG_SVRLLND, node, chan, (long)"end", 0, 0, 0);
	UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
}


/*
 * icssvr_llsendup_msg()
 *	Deal with arriving message sent up from ics_dgram().
 *
 * Description:
 *	This is a routine that is called on a server node when a NSC message
 *	arrives from a client node. 
 *
 *	The routine is responsible for obtaining a free handle from the
 *	high-level ICS code, if such a handle is available. If the handle
 *	is available, the message is processed by calling
 *	icssvr_llprocess_msg(). If the handle is not available, then
 *	the message is queued on an internal queue, and it will be processed
 *	once handles are available. [Some sort of flow control may need to
 *	be done here also].
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	rxd	pointer to an Infiniband (IB) receive descriptor.
 */
void
icssvr_llsendup_msg(struct _kib_rxd *rxd)
{
	icshdr_t *icshdr = LL_ICSHDR(rxd);
	int chan = icshdr->ihdr_chan;
	int svc = icshdr->ihdr_service;
	icsmsg_arrival_t *msgl_p = &icsmsg_arrival[chan];
	svr_handle_t	*handle;

	/*
	 * This lock must be held across the call to icssvr_recv_find_handle()
	 * to prevent a window where the shwait_nohandles flag can get
	 * set and icssvr_llhandles_present() gets called before the message
	 * is placed on the queue.
	 */
	LOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);

	/*
	 * If there are messages queued up already, then we add this message
	 * to the queue. We do this indiscriminate queuing to process the
	 * messages in order. (Not strictly necessary from an
	 * ICS perspective, but it does prevent problems where new messages
	 * steal all the handles from queued messages.)
	 */
	if ( !list_empty(&msgl_p->msglist) ) {
		icssvr_llenqueue(msgl_p, rxd, chan);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
		SIGNAL_EVENT(&icssvr_nanny_event);
		return;
	}

	/*
	 * See if there are any handles. If there are, then we process the
	 * message. If there are not, then we start up a message queue.
	 * NSC_XXX: this transport doesn't (yet) support the flags arg.
	 */
	handle = icssvr_find_recv_handle(chan, svc, 0);
	if (handle != NULL) {
		icssvr_llprocess_msg(handle, rxd);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
	} else {
		icssvr_llenqueue(msgl_p, rxd, chan);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
		SIGNAL_EVENT(&icssvr_nanny_event);
	}
}

/*
 * icssvr_llhandles_present()
 *	Inform low-level server code that handles for a channel are available
 *
 * Description:
 *	This call is made by high-level ICS code when a server handle for
 *	a particular communications becomes available and the prior call to
 *	icssvr_find_recv_handle() failed.
 *
 *	This call merely informs low-level ICS code that handles are
 *	available - it is possible that another thread may actually use
 *	the handle immediately.
 *
 *	For the TCP implementation, we obtain a message from
 *	the queued list of waiting messages and obtain a handle by calling
 *	icssvr_find_recv_handle(). Once we have both, icssvr_llprocess_msg()
 *	is called to actually deal with the message.
 *
 *	This routine may be called from interrupt context
 *
 * Parameters:
 *	chan		communication channel number being used
 */
static void
__icssvr_llhandles_present(int chan)
{
	icsmsg_arrival_t *msgl_p = &icsmsg_arrival[chan];
	svr_handle_t	*handle;
	struct _kib_rxd *rxd;
	icshdr_t	*icshdr;

	/*
	 * As long as there are server handles present and messages on the
	 * queue, continue processing messages.
	 */
	for(;;) {
		LOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);

		/* Peek and find out if there is a message pending. */
		if (!icssvr_lldequeue_peek(msgl_p, &rxd, chan)) {
			UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
			break;
		}
		icshdr = LL_ICSHDR(rxd);

		/*
		 * Get a handle from the high-level ICS code. If there
		 * are none, then we are done.
		 */
		handle = icssvr_find_recv_handle(chan,
						 icshdr->ihdr_service,
						 0);
		if (handle == NULL) {
			UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
			break;
		}
		/* Pull the message off the queue. */
		icssvr_lldequeue(msgl_p, &rxd, chan);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);

		/*
		 * With the msglist lock unlocked, process the message.
		 */
		icssvr_llprocess_msg(handle, rxd);
	}
}

void
icssvr_llhandles_present(int chan)
{
	int c;

	/*
	 * There can be high priority messages that can use handles
	 * for this channel; try them all first.
	 */
	if (chan < ics_min_prio_chan || chan > ics_max_prio_chan) {
		for (c = ics_min_prio_chan ; c <= ics_max_prio_chan; c++)
			__icssvr_llhandles_present(c);
	}
	__icssvr_llhandles_present(chan);
}

/*
 * icssvr_llprocess_msg()
 *	Final preparations with an incoming message to the server.
 *
 * Description:
 *	This routine is an internal low-level ICS routine that is called
 *	when the combination of a server handle/NSC message pair is
 *	present.
 *
 *	It is called to perform any low-level ICS setup necessary for the
 *	handle, so that future decodes and encodes can be performed.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle	server handle used to process the message
 *	skb	sk_buff chain representing the incoming message
 */
static void
icssvr_llprocess_msg(svr_handle_t *handle, struct _kib_rxd *rxd)
{
	icshdr_t *icshdr = LL_ICSHDR(rxd);

	/* Save information from the message header. */
	SSI_ASSERT(ICS_CHAN(handle->sh_service) == ICS_CHAN(icshdr->ihdr_service));
	handle->sh_service = icshdr->ihdr_service;
	handle->sh_chan = icshdr->ihdr_chan;
	handle->sh_transid = icshdr->ihdr_transid;
#ifdef NSC_ICSLOG
	handle->sh_uniqueid = icshdr->ihdr_uniqueid;
#endif
	handle->sh_node = icshdr->ihdr_orignode;
	handle->sh_procnum = icshdr->ihdr_procnum;
	if ((icshdr->ihdr_flag & ICS_NO_REPLY) != 0)
		handle->sh_flag |= SVR_HANDLE_NO_REPLY;
	else
		handle->sh_flag &= ~SVR_HANDLE_NO_REPLY;
#if 0
printk("%s Enter: rxd %p from-node %d chan %d svc %d inline len %d\n",
		__func__,rxd,
		icshdr->ihdr_orignode, icshdr->ihdr_chan,icshdr->ihdr_service,
		icshdr->ihdr_inline_len);
#endif
	/* Save a pointer to the IB message as a whole. */
	handle->sh_msg_rxd = rxd;

	/* Set up the pointers to the in-line portion of the message. */
#if 0
	// access ics hdr + inline from the received IB message.
	handle->sh_msg_inline_data_p = (u_char*)(icshdr+1);
#else
	/* copy ics header + inline data into the handle inline buffer */
	handle->sh_msg_inline_data_p = handle->sh_inline + sizeof(icshdr_t);
	memcpy((void*)handle->sh_inline, (void*)(icshdr),
				icshdr->ihdr_inline_len + sizeof(icshdr_t));
#endif

	ICSLOG(1, ICSLOG_SVRLLPROCMSG, icshdr->ihdr_transid,
				       icshdr->ihdr_uniqueid,
				       icshdr->ihdr_orignode,
				       icshdr->ihdr_service,
				       icshdr->ihdr_chan,
				       icshdr->ihdr_procnum);

	/* setup the IB OOL info pointer */
	handle->sh_msg_ib_ool_info = (ib_ool_info_t*) ((u_char*)(icshdr+1)
						+ icshdr->ihdr_inline_len);

	/* Set up the pointers to the OOL data portion of the message. */
	handle->sh_msg_ib_od = (ib_ool_desc_t*)(handle->sh_msg_ib_ool_info+1);

	/* # of completed RDMA.read ool descriptors in current IB message */
	handle->sh_msg_rdma_cnt = 0;

	/* Inform the high-level ICS code the message has arrived. */
	icssvr_sendup_msg(handle);
}

/*
 * icssvr_lldecode_inline()
 *
 * Description:
 *	Pull data from the in-line buffer as a byte stream.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */
void
icssvr_lldecode_inline(
	svr_handle_t	*handle,
	void		*param,
	int		size)
{
	if (size <= 0)
		return;
	if (param) memcpy(param, (void*)handle->sh_msg_inline_data_p, size);
	handle->sh_msg_inline_data_p += size;
}

/*
 * icssvr_lldecode_ool_data_t()
 *	Extract an OOL input parameter into the specified area
 *
 * Description:
 *	Extract an OOL input parameter for the specified handle into
 *	the user-provided buffer area. Called by ICS high-level code
 *	to perform argument marshalling for client/server interaction.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to array of characters; must not be NULL
 *	nelem		number of elements in the array
 */
int
icssvr_lldecode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*param,
	long		len)
{
	ib_ool_desc_t	*iod = handle->sh_msg_ib_od;
        int             rc, rlen, EOM=0;
        ib_rdma_seg_t	rseg;
        ulong           laddr;
	struct device	*dev;

	SSI_ASSERT(iod);
	SSI_ASSERT(handle->sh_msg_ib_ool_info);

	/* any reason to proceed? */
	if (len <= 0) return 0;

	SSI_ASSERT(handle->sh_msg_ib_ool_info->ool_desc_cnt > 0);

	/* process the current IB ool descriptor */
	if (iod->len > 0) {
		/* OOL data is contained within the IB message, copyout */
		if ( iod->len != len ) {
			printk("%s in-msg OOL data sz %d != decode len %ld\n",
						__func__,iod->len,len);
		}
		if (param) memcpy((void*)param, iod->ool_data, len);
		/* advance to next ib ool data descriptor */
		handle->sh_msg_ib_od = (ib_ool_desc_t*)
					(((char*)iod) + sizeof(int) + len);
	}
	else {
		/* OOL data is NOT present in the IB message. OOL data needs
		 * to be RDMA.read from the sender.
		 */
		if (iod->ool_magic != 0xcafebabe) {
			printk("%s bad magic(%x) sh_msg_rdma_cnt %d\n",__func__,
				iod->ool_magic,handle->sh_msg_rdma_cnt);
		}
		SSI_ASSERT(param);
		rlen = (-1 * iod->len); // 'real' RDMA data length in bytes
		if (rlen != len) {
			printk("%s OOL RDMA data sz %d != request len %ld\n",
						__func__,rlen,len);
			return -EINVAL;
		}

		/*
		 * process another RDMA ool
		 */
		handle->sh_msg_rdma_cnt++;
		dprintk(DBG_RDMA,
			("%s sh_msg_rdma_cnt %d rdma_ool_desc_cnt %d\n",
				__func__,handle->sh_msg_rdma_cnt,
				handle->sh_msg_ib_ool_info->rdma_ool_desc_cnt));

		rc = ib_rdma_setup(param, len, DMA_FROM_DEVICE, &laddr, &rseg);
		if (rc) {
			// error
			printk("%s RDMA.read(0x%p sz %ld) setup err %d\n",
							__func__, param,len,rc);
			return -ENOMEM;
		}
		/*
		 * RDMA.read the OOL data - if this is the last RDMA OOL
		 * within the current ICS message, then send an RDMA status
		 * back to the ICS message sender, thus releasing the waiting
		 * sender and completing the ICS message send.
		 */
#if 1
		/*
		 * Completed all RDMA.reads for the current message?
		 * Send 1 RDMA status message indicating all RDMA.reads are
		 * completed.
		 */
		if (handle->sh_msg_rdma_cnt == handle->sh_msg_ib_ool_info->rdma_ool_desc_cnt) {
			EOM = handle->sh_msg_rdma_cnt; /* send RDMA status */
		}
#else
		EOM = 1; // force status reply for each RDMA.read
#endif
		rc = ib_rdma_read(handle->sh_msg_rxd->conn, param, len, &laddr,
					rseg.mr->lkey, iod,
					handle->sh_msg_ib_ool_info->txd, EOM);

		/* advance to next IB OOL descriptor */
		handle->sh_msg_ib_od = iod + 1;

		/* undo what if/anything rdma read setup did w.r.t. mapping */
		dev = handle->sh_msg_rxd->conn->ib_nexus->device->dma_device;
		if (!rseg.baddr) {
                	dma_unmap_single(dev,
                                         pci_unmap_addr(&rseg,mapping),
                                         pci_unmap_len(&rseg,mlen),
                                         DMA_FROM_DEVICE);
		}
		else {
                	dma_unmap_page(dev,
                                       pci_unmap_addr(&rseg,mapping),
                                       pci_unmap_len(&rseg,mlen),
                                       DMA_FROM_DEVICE);
		}

		/* RDMA.read OK? */
		if (rc) {
			printk("%s Err - RDMA.read(0x%p sz %ld) setup %d\n",
						__func__, param, len,rc);
			return -ENOMEM;
		}
		dprintk(DBG_RDMA,
			("%s RDMA.read OOL data sz %ld\n",__func__,len));
	}
	return 0;
}


int
icssvr_llencode_ool_struct_msghdr(
	svr_handle_t *handle,
	struct msghdr *msg,
	int rwflag)
{
	__typeof__ (msg->msg_iovlen) i;

	/*
	 * Encode the iovecs in msghdr; the free routine frees the entire
	 * msghdr.
	 */
	icssvr_llencode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));

	icssvr_llencode_ool_data_t(handle, (u_char *)msg->msg_iov,
				   msg->msg_iovlen * sizeof(msg->msg_iov[0]),
				   freemsghdr_callback, (long)msg);
#ifdef ICS_DEBUG
	printk("%s: msg->msg_iovlen : %d\n", __func__, (int)msg->msg_iovlen);
#endif /* ICS_DEBUG */

	/*
	 * Encode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef ICS_DEBUG
		printk("%s: msg->msg_iov[%d].iov_len : %d\n",
		       __func__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* ICS_DEBUG */
		icssvr_llencode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   msg->msg_iov[i].iov_len,
					   NULL, (long)NULL);
	}
	return (0);
}


int
icssvr_lldecode_ool_struct_msghdr(
	svr_handle_t *handle,
	struct msghdr **mpp,
	int rwflag)
{
	struct msghdr *msg = *mpp;
	__typeof__ (msg->msg_iovlen) i;
	int alloclen;

	if (msg == NULL)
		*mpp = msg = kzmalloc_nofail(sizeof(*msg));

	/*
	 * Decode the number of iovecs in msghdr.
	 */
	icssvr_lldecode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));
	alloclen = msg->msg_iovlen * sizeof(msg->msg_iov[0]);
	msg->msg_iov = kzmalloc_nofail(alloclen);
#ifdef ICS_DEBUG
	printk("%s: msg_iovlen: %d\n", __func__, (int)msg->msg_iovlen);
#endif /* ICS_DEBUG */

	icssvr_lldecode_ool_data_t(handle, (u_char *)msg->msg_iov, alloclen);

	/*
	 * Decode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef ICS_DEBUG
		printk("%s: msg_iov[%d].iov_len: %d\n",
		       __func__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* ICS_DEBUG */
		alloclen = msg->msg_iov[i].iov_len;
		msg->msg_iov[i].iov_base = kzmalloc_nofail(alloclen);

		icssvr_lldecode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   alloclen);
	}
	return (0);
}


/*
 * icssvr_lldecode_done()
 *	Give an indication that all icssvr_lldecode_*() routines are complete.
 *
 * Description:
 *	Called after the final icssvr_lldecode_*() routine has completed.
 *	This routine is called to give the low-level code a chance to free
 *	up resources associated with the incoming message (if there
 *	are resources to be freed).
 *
#ifdef SNET
 *	Note that it may not be possible to free the inline buffer pertaining
 *	to the incoming message - if this is a message that requires a reply
 *	and the icscli_llencoderesp_ool*() routines actually encode data
 *	into the buffer, then the inline buffer is required for the
 *	icssvr_lldecode_ool*() routines to function. For this case the
 *	low-level ICS implementation may wish to encode a special flag
 *	into the header to indicate whether the inline buffer can be freed
 *	or re-used.
#endif
 *
 *	This routine is optional - if it is not called, then the freeing
 *	of the resources (if any) will not be performed until the call to
 *	icssvr_llreply().
 *
 *	This routine may be called from interrupt context.
 *
 *	For the non-SNET implementation, the inline buffer (and any other
 *	data from the incoming message) can be freed.
 *
 * Parameters:
 *	handle		server handle associated with the incoming message
 */
void
icssvr_lldecode_done(svr_handle_t *handle)
{
	/* Free the message itself. */
	if (handle->sh_msg_rxd) {
		ics_llfreemsg(handle->sh_msg_rxd);
		handle->sh_msg_rxd = NULL;
	}
	else
		printk("%s handle(%p)->sh_msg_rxd <NULL>?\n",__func__,handle);
}

/*
 * icssvr_llrewind()
 *	Undo any icssvr_llencode*() calls that have been done so far.
 *
 * Description:
 *	Undo any calls to icssvr_llencode*() that have been done on this
 *	handle so far. This routine is typically called when the
 *	server encounters errors in calls to icssvr_encode*() routines
 *	and wishes to undo the work done so far and simply return an
 *	error return value in icssvr_reply().
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the handle being operated on
 */
void
icssvr_llrewind(
	svr_handle_t	*handle)
{
	ooldesc_t	*ool_p;
	int 		i;

	/*
	 * If there is any OOL data so far, then free it and make things
	 * look like no OOL data.
	 */

	/* Free up OOL segments that specified a callback. */
	for (i = 0; i < handle->sh_rep_ooldesc_cnt; i++) {
		ool_p = &handle->sh_rep_ooldesc[i];
		(ool_p->ool_callback_data_t)(ool_p->ool_callparam,
					     ool_p->ool_callarg);
	}
	handle->sh_rep_ooldesc_cnt = 0;
	handle->sh_ref_cnt = 1;
	/* No more OOL data. */
	handle->sh_rep_msghdr_p->msg_iovlen = 1;
	/*
	 * Make sure that in-line encodes from now on start at the beginning
	 * of the in-line buffer.
	 */
	handle->sh_rep_inline_data_p
		= handle->sh_rep_msghdr_p->msg_iov[0].iov_base
		  + sizeof(icshdr_t);
	/* Reset inline data length. */
	handle->sh_rep_msghdr_p->msg_iov[0].iov_len = sizeof(icshdr_t);
}
/*
 * icssvr_llencode_inline()
 *
 * Description:
 *	Put data into the in-line buffer as a byte stream.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */
void
icssvr_llencode_inline(
	svr_handle_t	*handle,
	const void	*param,
	int		size)
{
	SSI_ASSERT(size >= 0);
	if (size <= 0)
		return;

	SSI_ASSERT(param);
	SSI_ASSERT(handle->sh_rep_inline_data_p);

	if ((void *)handle->sh_rep_inline_data_p + size >
	    handle->sh_rep_inline_iov_p[0].iov_base +
	    sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE)
		panic("%s:inline data size exceeded\n", __func__);

	/* Stuff the argument into the in-line buffer. */
	bcopy(param, handle->sh_rep_inline_data_p, size);
	handle->sh_rep_inline_data_p += size;
        handle->sh_rep_inline_iov_p->iov_len += size;
}

static void
icssvr_llhandle_realloc(
	svr_handle_t	*handle,
	int check_ool)
{
	struct iovec	*curiov;
	struct iovec	*newiov;
	ooldesc_t	*curool;
	ooldesc_t	*newool;
	int curlen;
	int newlen;

	curlen = handle->sh_rep_msghdr_p->msg_iovlen;
	SSI_ASSERT(curlen >= ICS_DEF_OOL_SEGMENTS + 1);
	if ((curlen - (ICS_DEF_OOL_SEGMENTS + 1))
	    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
		newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
		curiov = handle->sh_rep_msghdr_p->msg_iov;
		newiov = kmalloc_nofail(newlen * sizeof(*newiov));
		memcpy(newiov, curiov, curlen * sizeof(*newiov));
		handle->sh_rep_msghdr_p->msg_iov = newiov;
		handle->sh_rep_inline_iov_p = newiov;
		if (curiov != handle->sh_iovec)
			kfree(curiov);
	}
	if (check_ool) {
		curlen = handle->sh_rep_ooldesc_cnt;
		if ((curlen - ICS_DEF_OOL_SEGMENTS)
		    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
			newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
			curool = handle->sh_rep_ooldesc;
			newool = kmalloc_nofail(newlen * sizeof(*newool));
			memcpy(newool, curool, curlen * sizeof(*newool));
			handle->sh_rep_ooldesc = newool;
			if (curool != handle->sh_ooldesc)
				kfree(curool);
		}
	}
}

/*
 * icssvr_llencode_ool_data_t()
 * 	Set up an OOL buffer with the specified data.
 *
 * Description:
 *	Set up an OOL buffer for an input parameter, with the buffer containing
 *	the specified data.  Called by ICS high-level code to perform argument
 *	marshalling for client/server interaction.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data; data should not be de-allocated until
 *			either the callback routine is called or the message
 *			is sent
 *	len		length (in bytes) of the data
 *	callback	routine that is called after the data is no longer
 *			required; can be NULL; if used, it is typically used
 *			for data de-allocation; callback is called with
 *			two parameters: param (from above) as first parameter
 *			and callarg (below) as 2nd parameter
 *	callarg		2nd parameter to callback routine (if callback routine
 *			is called)
 *
 * NSC_XXX: Earlier implemntations required clumsy 2-level indirection for
 * calling back the freeing routine. This can and needs to be simplified.
 */
int
icssvr_llencode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*param,
	long		len,
	void		(*callback)(
				u_char		*param,
				long		callarg),
	long		callarg)
{
	ooldesc_t	*ool_p;
	struct iovec	*iov;

	//printk("%s hdl %p sz %ld\n",__func__,handle,len);

	SSI_ASSERT((handle->sh_flag & SVR_HANDLE_NO_REPLY) == 0);

	/* Do nothing if there is no data. */
	if (len <= 0) {
		if (callback != NULL)
			callback(param, callarg);
		return(0);
	}
	SSI_ASSERT(param);

	/* Make sure there is room. */
	if (handle->sh_rep_msghdr_p->msg_iovlen >= ICS_DEF_OOL_SEGMENTS + 1)
		icssvr_llhandle_realloc(handle, (callback != NULL));

	/* Just point the iovec at the server data. */
	iov = &handle->sh_rep_msghdr_p->msg_iov[(handle->sh_rep_msghdr_p->msg_iovlen)++];
	iov->iov_base = param;
	iov->iov_len = len;

	/*
	 * If appropriate, set up the buffer freeing routine to be called.
	 */
	if (callback != NULL) {
		ool_p = &handle->sh_rep_ooldesc[(handle->sh_rep_ooldesc_cnt)++];
		ool_p->ool_svr_handle = handle;
		ool_p->ool_callback_data_t = callback;
		ool_p->ool_callparam = param;
		ool_p->ool_callarg = callarg;
		handle->sh_ref_cnt++;
	}
	return(0);
}

/*
 * icssvr_llreply()
 *	Reply to the client node.
 *
 * Description:
 *	This routine is called to reply to the client node. If the client
 *	sent a plain asynchronous message, then this routine tells the
 *	transport that all incoming arguments have been decoded and the
 *	high-level code is done with the handle. If the client sent the
 *	request side of a request/response message, then this routine
 *	is called to send the response back to the client.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle of the message
 */
void
icssvr_llreply(svr_handle_t *handle)
{
	struct iovec	*iov;
	icshdr_t	*ihdr_p;
	int		i,T;
	int		error = 0;
	boolean_t	immediate_reply;
	ics_llnodeinfo_t *nodeinfo;
	ics_ib_conn_data_t *conn = NULL;
	kib_tx_t	*txd;
	int		expected_rdma_ops;
	ib_rdma_seg_t	ib_rmem[MAX_RDMA_READ_OPS];
	ib_rdma_seg_t	*rm;
	ulong		Tstart;

	/* Free the message itself (if icssvr_lldecode_done() wasn't called). */
	if (handle->sh_msg_rxd) {
		ics_llfreemsg(handle->sh_msg_rxd);
		handle->sh_msg_rxd = NULL;
	}

	/*
	 * Send a reply only if one was actually expected.
	 */
	if ((handle->sh_flag & SVR_HANDLE_NO_REPLY) == 0) {

		/* Set up fields in the message header */
		iov = handle->sh_rep_msghdr_p->msg_iov;
		ihdr_p = (icshdr_t *)iov[0].iov_base;
		ihdr_p->ihdr_marker = ICSHDR_MARKER;
		ihdr_p->ihdr_inline_len = (void *)handle->sh_rep_inline_data_p -
						iov[0].iov_base - sizeof(icshdr_t);
		ihdr_p->ihdr_len = 0;
		for (i = 0; i < handle->sh_rep_msghdr_p->msg_iovlen; i++)
			ihdr_p->ihdr_len += iov[i].iov_len;
		ihdr_p->ihdr_version = 1;
		ihdr_p->ihdr_service = handle->sh_service;
		ihdr_p->ihdr_flag = 0;
		ihdr_p->ihdr_procnum = 0;
		ihdr_p->ihdr_transid = handle->sh_transid;
		ihdr_p->ihdr_orignode = this_node;
#ifdef NSC_ICSLOG
		ihdr_p->ihdr_uniqueid = handle->sh_uniqueid;
#endif
		/*
		 * Reply channel determined by whether original message
		 * was a priority message or not.
		 */
		if (handle->sh_chan >= ics_min_prio_chan &&
		    handle->sh_chan <= ics_max_prio_chan)
			ihdr_p->ihdr_chan = ics_reply_prio_chan;
		else
			ihdr_p->ihdr_chan = ics_reply_chan;

		/* Log the message. */
		ICSLOG(1, ICSLOG_SVRLLSEND, ihdr_p->ihdr_transid,
					    ihdr_p->ihdr_uniqueid,
					    handle->sh_node,
					    ihdr_p->ihdr_service,
					    ihdr_p->ihdr_chan,
					    ihdr_p->ihdr_procnum);

		/*
		 * Find the connection to the node. If the connection exists,
		 * then send the reply to it.
		 * NOTE: The connection must be established prior to this call.
		 */
		nodeinfo = ics_llnodeinfo[handle->sh_node];
		if (nodeinfo != NULL) {
			LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			SSI_ASSERT(nodeinfo->icsni_conn);
			conn = nodeinfo->icsni_conn[ihdr_p->ihdr_chan];
			if (conn != NULL)
				INCR_ATOMIC_INT(&conn->conn_ref_cnt);
			UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if (conn != NULL) {
				LOCK_LOCK(&conn->conn_lock);
				Tstart = jiffies;
				error = ics_llsendmsg(conn,
						      handle->sh_rep_msghdr_p,
						      ihdr_p->ihdr_len,
						      ib_rmem,
						      &expected_rdma_ops,
						      &txd);
				UNLOCK_LOCK(&conn->conn_lock);
				DECR_ATOMIC_INT(&conn->conn_ref_cnt);
			}
		}

		if (error > 0) {
			/*
			 * wait for an RDMA status message as the reply message
			 * required an RDMA.read by the client; OOL reply data
			 * would not fit in IB txd buffer.
			 */
			if (error > 1)
				dprintk(DBG_RDMA,
				     ("%s handle %p waiting for %d RDMA."
				     "reads+status\n",__func__,handle,error));

			/* wakened by -
			 *  rdma ops count drops to zero (success).
			 *  Errors - timeout handler
			 *           RDMA status message with an error
			 */
			wait_event(conn->conn_ib->waitQ,
						(expected_rdma_ops <= 0));
			Tstart = jiffies - Tstart;
			put_idle_txd(txd);

			/* IB deregister/unmap memory used in RDMA.read op */
			for(i=0,T=0,rm=ib_rmem; i < error; i++,rm++) {
				ib_nexus_t *ibn=conn->conn_ib->ib_nexus;

				if (!rm->baddr) {
					dma_unmap_single(
						    ibn->device->dma_device,
						    pci_unmap_addr(rm,mapping),
					 	    pci_unmap_len(rm,mlen),
					 	    DMA_TO_DEVICE);
				}
				else {
					dma_unmap_page(
						    ibn->device->dma_device,
						    pci_unmap_addr(rm,mapping),
					 	    pci_unmap_len(rm,mlen),
					 	    DMA_TO_DEVICE);
				}
				T += rm->nob;
			}

			if (error > 1)
				dprintk(DBG_RDMA,
					("%s hdl(%p) RDMA.reads complete(%d) "
					"bytes(%d) jifs(%ld)\n", __func__,
					handle,error,T,Tstart));

			/* Errors? The success path is expected_rdma_ops
			 * drops to zero, else an RDMA status message error
			 * code or timeout handler error code.
			 */
			error = expected_rdma_ops;
		}

		/*
		 * Free inline data by setting inline_data pointer to NULL.
		 */
		handle->sh_rep_inline_data_p = NULL;

		/*
		 * Do the freebuf callback for OOL data.
		 */
		for (i = 0; i < handle->sh_rep_ooldesc_cnt; i++) {
			register ooldesc_t *ool_p;

			ool_p = &handle->sh_rep_ooldesc[i];
			/* release/free memory */
			(ool_p->ool_callback_data_t)(ool_p->ool_callparam,
						     ool_p->ool_callarg);
			/*
			 * The sh_ref_cnt was incremented for each OOL segment
			 * that was encoded with a callback in
			 * icssvr_llencode_ool_data_t(). Now decrement the
			 * count for each encoding.
			 */
			handle->sh_ref_cnt--;
		}

		/*
		 * If we didn't manage to send the reply, clean up after
		 * ourselves.
		 */
		if (nodeinfo == NULL || conn == NULL || error < 0) {
#ifdef ICS_DEBUG
			printk(/*KERN_WARNING*/ "%s: Error! Reply not sent!\n",__func__);
#endif /* ICS_DEBUG */
			SSI_ASSERT(handle->sh_ref_cnt > 0);
			handle->sh_ref_cnt--;		/* No need to lock */
			if (handle->sh_ref_cnt == 0)
				icssvr_sendup_replydone(handle, -EREMOTE);
			goto out;
		}

		/*
		 * If either there was no OOL data (with callbacks) or the
		 * data with callbacks has already been freed, then
		 * inform the high-level ICS code that we are done.
		 */
		LOCK_SPIN_LOCK(&handle->sh_lllock);
		SSI_ASSERT(handle->sh_ref_cnt > 0);
		handle->sh_ref_cnt--;
		immediate_reply = (handle->sh_ref_cnt == 0);
		UNLOCK_SPIN_LOCK(&handle->sh_lllock);
		if (immediate_reply)
			icssvr_sendup_replydone(handle, 0);
	} else {
		/*
		 * No reply to be sent. Inform the high-level ICS code that
		 * we are done.
		 */
		icssvr_sendup_replydone(handle, 0);
	}
out:
	return;
}

/*
 * icssvr_llhandle_deinit()
 *	Deinitialize a server handle, since we have completed our operation.
 *
 * Description:
 *	This routine is called by ICS high-level code to deinitialize the
 *	low-level portion of a server handle. No subsequent icssvr_ll*()
 *	routines may be called with this handle.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle to be deinitialized
 */
void
icssvr_llhandle_deinit(
	svr_handle_t	*handle)
{
	/* Deinitialize any handle locks. */
	DEINIT_SPIN_LOCK(&handle->sh_lllock);

	/*
	 * Free outgoing msghdr and iovecs.
	 * We only free the inline data. It is the responsibility
	 * of the caller to free the OOL data.
	 */
	if (handle->sh_rep_msghdr_p->msg_iov != handle->sh_iovec)
		kfree(handle->sh_rep_msghdr_p->msg_iov);
	if (handle->sh_rep_ooldesc != handle->sh_ooldesc)
		kfree(handle->sh_rep_ooldesc);

	/* Free incoming IB message descriptor */
	if (handle->sh_msg_rxd) {
		ics_llfreemsg(handle->sh_msg_rxd);
		handle->sh_msg_rxd = NULL;
	}
}

/*
 * An ICS datagram has arrived on an Infinband connection.
 * This routine called from IB CQ (Completion Queue) event handler routine
 * in the IB HCA device interrupt context.
 *
 * Input:
 *	rxd - pointer to the IB received descriptor; payload is the actual
 *		message. This routine is ONLY called on a successful IB recv.
 */

void
ics_dgram(kib_rx_t *rxd)
{
	int chan;

        SSI_ASSERT(rxd);
#ifdef ICS_DEBUG_V
	printk("%s rxd %p payld %p nob %d posted %d\n",__func__,rxd,
		rxd->payload,rxd->nob,atomic_read(&rxd->conn->rx_posted));
#endif
        /*
         * Get a pointer to the private data for this stream.
         */
#if MUX_ICS_CHANNELS
	{
		/* ICS channel '0' is the only 'real' IB transport connection,
		 * all other ICS channels are MUX'ed onto the transport
		 * connection. The RX descriptor always points at the IB
		 * connection for ICS channel 0. Hence the ICS channel must be
		 * extracted from the ICS header.
		 */
		chan = ((icshdr_t *)rxd->payload)->ihdr_chan;
	}
#else
	{
		/* each ics channel is a separate IB connection, hence RX
		 * descriptors all point at the correct struct *ib_conn, so
		 * rxd->conn->ics_data is always correct to acquire the ICS
		 * channel #.
		 */
        	chan = rxd->conn->ics_data->conn_chan;
	}
#endif
	if (chan == ics_reply_chan || chan == ics_reply_prio_chan) {
		icscli_llsendup_reply(rxd);
	}
	else {
		icssvr_llsendup_msg(rxd);
	}
}


#if defined(ICS_DEBUG) || defined(DEBUG_TOOLS)
void
print_icssvr_llhandle(svr_llhandle_t *generic)
{
	ib_svr_llhandle_t *svr = &generic->ibsvr_llhandle;

	printk(KERN_DEBUG
	       "_sh_msg_inline_data_p 0x%p\n", svr->_sh_msg_inline_data_p);
	printk(KERN_DEBUG
	       "_sh_msg_ib_ool_info 0x%p\n", svr->_sh_msg_ib_ool_info);
	printk(KERN_DEBUG "_sh_rep_msghdr_p 0x%p\n",
	       svr->_sh_rep_msghdr_p);
	printk(KERN_DEBUG
	       "_sh_rep_inline_iov_p 0x%p\t_sh_rep_inline_data_p 0x%p\n",
	       svr->_sh_rep_inline_iov_p,
	       svr->_sh_rep_inline_data_p);
	printk(KERN_DEBUG "_sh_rep_ooldesc_cnt %d\t_sh_ref_cnt %d\n",
			svr->_sh_rep_ooldesc_cnt, svr->_sh_ref_cnt);
}

#endif /* ICS_DEBUG || DEBUG_TOOLS */
