/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:noexpandtab:shiftwidth=8:tabstop=8:
 */

/*
 * 	Low-level ICS transport code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the routine ics_lltransport_config() and its related
 * subroutines. This is an initialization routine that does all the
 * Infiniband initialization to allow ICS to use Infiniband verbs as its
 * transport.
 */
#include <linux/ctype.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/route.h>
#include <linux/inet.h>
#include <linux/netdevice.h>
#include <linux/inetdevice.h>
#include <linux/cpumask.h>
#include <asm/uaccess.h>
#include <net/route.h>
#include <linux/if.h>
#include <cluster/nsc.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>        /* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>  /* Must be after <ics_private.h> */
#include <cluster/clms.h>

static int ics_getifconfig(char *);
static char *ics_getinterfaces(void);
static int open_HCA(ib_nexus_t *);

int		ics_lltransport_config_done = 0;
u32		ics_ib_debug=DBG_SET;
ib_nexus_t	ib_nexus;

static struct ib_client ib_user;

/*
 * called by the Linux PCI device subsystem when an Infiniband HCA device is
 * avaiable/configured.
 */
static void ics_ib_add_dev(struct ib_device *dev)
{
	ib_nexus_t *ibd = &ib_nexus;

        if (!ibd->device) {
                ibd->device = dev;
                ib_set_client_data(dev, &ib_user, (void*)ibd);
		/* now that an ICS device is avail, allow ICS init to proceed */
		wake_up_interruptible_sync(&ibd->ksvr_waitQ);
	}
	else
		printk("%s() ib_nexus.device set(%p)?\n",__func__,ibd->device);
}

/*
 * called when an Infiniband device is removed from service.
 */
static void ics_ib_rm_dev(struct ib_device *dev)
{
	ib_nexus_t *ibd = &ib_nexus;

	printk("%s() dev %p ib_nexus.device %p\n",
			__func__,dev,ibd->device);
        if (ib_get_client_data(dev, &ib_user) != ibd) {
		printk("%s() BAD IB dev %p client data?\n",__func__,dev);
		return;
	}
	/*
	 * remove persistent ICS server listen()'s
	 */
	if (ibd->ics_svc_listen.listen_id)
		rdma_destroy_id(ibd->ics_svc_listen.listen_id);
	if (ibd->ics_probe_listen.listen_id)
		rdma_destroy_id(ibd->ics_probe_listen.listen_id);
	if (ibd->ics_unack_listen.listen_id)
		rdma_destroy_id(ibd->ics_unack_listen.listen_id);

	/* what else? ICS upper level issues? */
}

static struct ib_client ib_user = {
	.name   = "ics_ib",
	.add    = ics_ib_add_dev,
	.remove = ics_ib_rm_dev,
};


/*
 * ics_lltransport_config()
 *	Routine called at system boot time to configure the ICS transport.
 *
 * Description:
 *	This routine gets the ifconfig parameters specified in the
 *	kernel-boot command line.
 *	It registers with the Infiniband stack as a client
 *
 * NSC_XXX: Need to add some hook here to prevent ifconfig program from
 * 	    trying to configure the ICS device at the user level.
 *
 *
 * Calling/Exit State:
 *	Called very early in main, before root has been mounted.
 *
 *
 * Parameters:
 *	None.
 */
void
ics_lltransport_config(void)
{
	struct net_device	*dev;
	int			rc, ifconfig_ok = 1;
	char			*intf;
	mm_segment_t		oldfs;
	ib_nexus_t		*ibd = &ib_nexus;

	memset((void*)ibd, 0, sizeof(*ibd) );

	/* SSI_XXX: This is going away in a future release */
	if (ics_getifconfig(cluster_ifconfig)) {
		ifconfig_ok = 0;
	}

	/*
	 * pick the IPv4 address assigned to the IPoIB interface. Yes, this
	 * implies IB VERBS requires IPoIB due to the desire to not pertabate
	 * the cluster config user interfaces.
	 */
	while ((intf = ics_getinterfaces())) {
		/* Set this when IFCONFIG= not specified above */
		if (strlen(ics_netaddr) == 0) {
			struct ifreq ifr;
			struct sockaddr_in sin = {AF_INET};
			int error;
			__u32 addr;

			oldfs = get_fs();
			set_fs(KERNEL_DS);
			strncpy(ifr.ifr_name, intf, IFNAMSIZ);
			ifr.ifr_addr = *(struct sockaddr *)&sin;
			error = devinet_ioctl(SIOCGIFADDR, (void *)&ifr);
			if (error)
				printk(KERN_WARNING "No interface addr for %s?\n", intf);
			else {
				addr = ((struct sockaddr_in *)
						&ifr.ifr_addr)->sin_addr.s_addr;
				sprintf(ics_netaddr, "%d.%d.%d.%d",
								NIPQUAD(addr));
				ibd->my_hid.nid = addr; // net byte-order
				ibd->my_hid.hostname_len = (u8) sprintf(
					(char*)ibd->my_hid.hostname,"[%s]",
								ics_netaddr);
				ibd->my_hid.node = this_node;
			}
			set_fs(oldfs);
		}
		if ( (dev = dev_get_by_name(intf)) != NULL) {
			dev->priv_flags |= IFF_ICS;
			dev_put(dev);
		} else
			printk(KERN_WARNING "%s: Could't set IFF_ICS on %s.\n",
				__func__, intf);
	}

	/* SSI_XXX: This is going away in a future release */
	if (ifconfig_ok) {
		if ( (dev = dev_get_by_name(ics_ifname)) != NULL) {
			dev->priv_flags |= IFF_ICS;
			dev_put(dev);
		} else
			printk(KERN_WARNING "%s: Could't set IFF_ICS on %s.\n",
				__func__,ics_ifname);
	}

	/*
	 * init Infiniband global data (nexus).
	 */
	rwlock_init(&ibd->conn_lock);	
	spin_lock_init(&ibd->idletxd_lock);
	spin_lock_init(&ibd->rx_pool_lock);
	spin_lock_init(&ibd->rx_pool_grow_lock);

	INIT_LIST_HEAD(&ibd->active_conn);
	INIT_LIST_HEAD(&ibd->idletxds);
	INIT_LIST_HEAD(&ibd->nblk_idletxds);
	INIT_LIST_HEAD(&ibd->rx_pool);

	spin_lock_init(&ibd->ksvr_lock);
	INIT_LIST_HEAD(&ibd->ksvr_workQ);
	atomic_set(&ibd->ksvr_nthreads,0);
	init_waitqueue_head(&ibd->ksvr_waitQ);
	init_waitqueue_head(&ibd->idletxd_waitq);
	ibd->ksvr_workQ_cnt = 0;

	atomic_set( &ibd->unclaimed_tx_resources, 0 );

	ibd->wq = create_workqueue("ics_ib");
	if ( !ibd->wq ) {
		printk("%s ERR - unable to create a work queue?\n",__func__);
		return;
	}

	/*
	 * register ourselves as an ib client. The callbacks specified in
	 * ib_user occure when an Infiniband HCA (Host Channel Adapter)
	 * device is added or removed.
	 * Wait here until we have an IB device to use.
	 */

       	if ((rc=ib_register_client(&ib_user))) {
		printk("%s ERR - failed IB client register (err %d)?\n",
							__func__,rc);
               	destroy_workqueue(ibd->wq);
		return;
	}

	/* wait for an Infiniband HCA device to register - we borrow the
	 * ksvr_waitQ as it's not in use yet.
	 */
	rc = wait_event_interruptible_timeout( ibd->ksvr_waitQ,
					       (ibd->device != NULL),
					       (30*HZ) );
	if (!ibd->device) {
		printk("%s ERR - no IB device available? (rc %d)?\n",
							__func__,rc);
               	destroy_workqueue(ibd->wq);
		return;
	}

        /*
         * find the Infiniband (IB) HCA (Host Channel Adapters) on this
         * node. Use the first active HCA port on the 1st active HCA.
         */
        if (open_HCA(ibd)) {
                printk(KERN_WARNING "%s Can't find IBA hardware?\n",__func__);
               	destroy_workqueue(ibd->wq);
		return;
        }

	/* setup TX (Transmit) descriptor cache */
	ibd->txd_cache = kmem_cache_create("txd_cache",
					   sizeof(struct _kib_txd),
					   0, SLAB_HWCACHE_ALIGN,
					   NULL, NULL);

        /* create the proc filesystem entry /proc/cluster/ics_ib/ */
        (void) create_ib_proc_fs();

#if KSVR
	/*
	 * Spawn kernel Server thread(s): ICS receive message processing is the
	 * primary task.
	 */
	{
		int i,rc;
		extern void ksvr_thread(void*);
		extern void ksvr_thread_stop(void);
		static void close_HCA(ib_nexus_t*);

		dprintk(DBG_STARTUP,("%s spawn %d ksvr_thread(s)\n",
			__func__,num_present_cpus()));

		ibd->ksvr_run = 1;
		for (i=0; i < num_present_cpus(); i++) {
			rc = spawn_daemon_thread("ksvr_recv",ksvr_thread,NULL);
			if (rc) {
				CERROR ("spawn ksvr thread %d\n",rc);
				ksvr_thread_stop();
				close_HCA(ibd);
				destroy_ib_proc_fs();
               			destroy_workqueue(ibd->wq);
				return;
			}
		}
	}
#endif
	ics_lltransport_config_done = 1;
}

/*
 * ics_getinterfaces()
 *	Parse the interfaces string specified in cluster prep phase
 *
 * Description:
 *	This function parses the interfaces information and stores the
 *	information in gobal variables.
 *
 * 	format of ifconfig information given is
 *		<device>:<device>...
 *	For example:
 *		INTERFACES=bond0:eth0:eth1
 *		INTERFACES=eth0
 *
 * Return Values:
 *	Returns unprocessed device
 *      NULL    no more device to return
 */
static char *
ics_getinterfaces(void)
{
	static char intr[ICS_MAX_IFNAME_SIZE];
	char	*s;

	if (cluster_interfaces == NULL)
		return NULL;

	/*
	 * Get the interface device name.
	 */
	s = clms_next_token(&cluster_interfaces, ':');
	if (s == NULL)
		return NULL;

	strncpy(intr, s, ICS_MAX_IFNAME_SIZE);
	return intr;
}

/* SSI_XXX: This is going away in a future release */
/*
 * ics_getifconfig()
 *	Parse the ifconfig string specified in the kernel-boot command line.
 *
 * Description:
 *	This function parses the ifconfig information and stores the
 *	information in gobal variables.
 *
 * 	format of ifconfig information given is
 *		<device>:<ipaddr>:<netmask>
 *	For example:
 *		IFCONFIG=eth0:168.87.155.51:255.255.255.128
 *
 *	The interface name is stored in ics_ifname[] as
 *		"<device>"
 *	In our example:
 *		"eth0"
 *	The device is stored in ics_ifdevname as "<device>".
 *	The IP address is stored in ics_netaddr[] as a character string.
 *	The netmask is stored in ics_netmask[] as a character string.
 *
 * Parameters:
 *	ifp	- pointer to ifconfig information string.
 *
 * Return Values:
 *	Returns 0 on success and an error number on failure.
 */
static int
ics_getifconfig(
	char	*ifp)
{

	char	*s;

	/*
	 * Get the interface device name.
	 */
	s = clms_next_token(&ifp, ':');
	if (s == NULL) {
		return(-EINVAL);
	}
	strncpy(ics_ifdevname, s, ICS_MAX_IFNAME_SIZE);
	/*
	 * The devname is also the beginning of the interface name.
	 */
	strncpy(ics_ifname, ics_ifdevname, ICS_MAX_IFNAME_SIZE);
	/*
	 * Get the IP address.
	 */
	s = clms_next_token(&ifp, ':');
	if (s == NULL) {
		printk(KERN_WARNING "ics_getifconfig: Invalid ics_netaddr.\n");
		return(-EINVAL);
	}
	strncpy(ics_netaddr, s, ICS_IPADDRLEN);
	/*
	 * Get the netmask.
	 */
	s = clms_next_token(&ifp, ':');
	if (s == NULL) {
		printk(KERN_WARNING "ics_getifconfig: Invalid ics_netmask.\n");
		return(-EINVAL);
	}
	strncpy(ics_netmask, s, ICS_IPADDRLEN);
	return 0;
}



/*
* DESCRIPTION -- Initialize the Infinband Access Layer
*	Open an interface to the (Infiniband device Layer). Create a
*	protection domain based on the 1st found HCA (Host Channel Adapter,
*	often shortened to CA).
* PARAMETERS
*    *ibd
*	[in] pointer to IB data
* RETURN VALUE
*	0 - SUCCESS, otherwise negative errno value.
*	-ENOMEM - unable to allocate kernel memeory.
*/

static int open_HCA(ib_nexus_t *ibd)
{
	int rc;

	SSI_ASSERT(ibd->device);

	dprintk(DBG_STARTUP,("%s dev %s DMA_dev %s\n",__func__,
		ibd->device->name,ibd->device->dma_device->driver->name));
	/*
	 * based on the HCA, create a PD (Protection Domain) to be used for
	 * all connections.
	 */
	ibd->pd = ib_alloc_pd(ibd->device);
	if (IS_ERR(ibd->pd)) {
		rc = PTR_ERR(ibd->pd);
		printk("%s IB Protection Domain alloc failure? %d\n",
								__func__,rc);
		return (-EACCES);
	}

	/*
	 * set kernel DMA'able memory region info: lkey & rkey
	 */
	ibd->kmr = ib_get_dma_mr(ibd->pd, (IB_ACCESS_LOCAL_WRITE |
                                           IB_ACCESS_REMOTE_READ |
                                           IB_ACCESS_REMOTE_WRITE));
	if (IS_ERR(ibd->kmr)) { 
		rc = PTR_ERR(ibd->kmr);
		ibd->kmr = NULL;
		printk("%s unable to alloc kernel DMA memory region %d\n",
								__func__,rc);
		return (-rc);
	}
	dprintk(DBG_STARTUP,("%s PD %p KMR %p lkey %x\n",
			__func__,ibd->pd,ibd->kmr,ibd->kmr->lkey));

	return SUCCESS;
}


#if KSVR

/*
* DESCRIPTION
*	close and release all IB resources including the global transmit
*	pool as the tx buffers/desc are IB memory registered. 
* PARAMETERS
*    *ibd
*	[in] pointer to IB data
* RETURN VALUE
*	0 - SUCCESS, otherwise negative errno value.
*	-ENOMEM - unable to allocate kernel memeory.
* SIDE EFFECTS
*	IB handles for AL, CA(Channel adapter) and PD stored in ibd fields. 
*/

static void 
close_HCA(ib_nexus_t *ibn)
{
	int rc;
	struct list_head *pos, *tmp;
	struct _kib_rxd *rxd;

	if ( atomic_read(&ibn->num_connected) > 0 )
		printk("%s (%d) connections remain? ignored.\n", __func__,
					atomic_read(&ibn->num_connected));

	// no more TX resources to reclaim
	atomic_set( &ibn->unclaimed_tx_resources, 0 );

	// release TX buffers
	kmem_cache_destroy(ibn->txd_cache);

	// release global RX buffers
	for (pos=ibn->rx_pool.next; pos != &ibn->rx_pool; pos=tmp) {
		tmp = pos->next;
		rxd = list_entry(pos, struct _kib_rxd, link); 
		list_del(&rxd->link);
		dma_free_coherent( /* release payload buffer */
				ibn->device->dma_device,
				IBA_MAX_BUF_SIZE,
				(void*)rxd->payload,
				rxd->dma_addr);
		kfree((void*)rxd);
	}
	dprintk(DBG_SHUTDOWN, ("%s RX & TX resources released\n",__func__));

	/* release kernel memory region */
	if (ibn->kmr) {
		ib_dereg_mr(ibn->kmr);
		ibn->kmr = NULL;
	}

	/* destroy the PD (Protection Domain) */
	if ( (ibn->pd) && (rc=ib_dealloc_pd(ibn->pd)) )
	{
		ibn->pd = NULL;
		printk("%s IB protection domain destroy error %d\n",
							__func__,rc);
	}

	// release IB device
	ib_unregister_client(&ib_user);

	flush_workqueue(ibn->wq);
	destroy_workqueue(ibn->wq);
}
#endif // KSVR

