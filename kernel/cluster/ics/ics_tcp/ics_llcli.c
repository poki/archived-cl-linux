/*
 * 	Low-level ICS client code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Portions Copyright 2007 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/uio.h>
#include <asm/uaccess.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */
#include <cluster/log.h>

#ifdef NSC_ICSLOG
const char *icslog_llformat[] = {
	"%lx-llcsnd : id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-llssnd : id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-dgmsmal: n %ld c %ld len %ld icshdr_len %ld\n",
	"%lx-dgmhdrl: n %ld c %ld len %ld ihdr_len %ld id 0x%lx/0x%04lx\n",
	"%lx-dgminc : n %ld c %ld len %ld ihdr_len %ld id 0x%lx/0x%04lx\n",
	"%lx-dgmmult: n %ld c %ld len %ld ihdr_len %ld id 0x%lx/0x%04lx\n",
	"%lx-dgmupcl: n %ld c %ld id 0x%lx/0x%04lx sp 0x%lx dp 0x%lx\n",
	"%lx-llsnd  : n %ld c %ld s %s\n",
	"%lx-llseq  : id 0x%lx/0x%04lx n %ld c %ld m %ld f %ld\n",
	"%lx-llsdq  : id 0x%lx/0x%04lx n %ld c %ld m %ld l %ld\n",
	"%lx-llcsndu: id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-llspmsg: id 0x%lx/0x%04lx n %ld s/c/p 0x%lx/%ld/%ld\n",
	"%lx-llsndd : id 0x%lx/0x%04lx n %ld c %ld m %ld l %ld\n",
};

#endif /* NSC_ICSLOG */

#define ICS_CLI_COPY_OOLDATA

/*
 * icscli_llinit()
 *	Perform relevant initialization for the low-level client code.
 */
void
icscli_llinit()
{
#if defined (NSC_ICSLOG)
	int i;
	int j;

	for (i = 0; i < NUM_ENTRIES(icslog_llformat); i++) {
		j = ICSLOG_LLICSOFF + i;
		if (j >= ICSLOG_NUM_MSGS)
			break;
		icslog_format[j] = icslog_llformat[i];
	}
#endif /* NSC_ICSLOG */
}

/*
 * icscli_llhandle_init()
 *	Initialize the low-level ICS portion of a client handle
 *
 * Description:
 *	This routine is called by ICS high-level code to initialize the
 *	portion of the handle that is specific to ICS low-level code.
 *
 *	This routine can assume that, if the handle is being re-used,
 *	that icscli_llhandle_deinit() has been called.
 *
 *	This routine cannot assume that the low-level portion of the client
 *	handle has been initialized to all zeroes.
 *
 *	This routine may be called from interrupt context (if flag is FALSE).
 *
 * Parameters:
 *	handle		the handle to be initialized
 *	flag		if TRUE, this routine can sleep waiting for resources
 *
 * Return value:
 *	If flag is TRUE, this routine always returns 0 (success). If flag
 *	is FALSE, then this routine returns 0 (success) if it managed to
 *	initialize the handle without sleeping, and -EAGAIN if it would
 *	have had to go to sleep.
 */
int
icscli_llhandle_init(
	cli_handle_t	*handle,
	boolean_t	sleep_flag)
{
	struct iovec	*iov;
	struct msghdr	*msg;

	(void)sleep_flag;
	msg = &handle->ch_msghdr;
	iov = handle->ch_iovec;
	iov[0].iov_base = handle->ch_inline;
	iov[0].iov_len = sizeof(icshdr_t);
	msg->msg_name = NULL;
	msg->msg_namelen = 0;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
#ifdef ICS_TCP_SENDPAGE
	msg->msg_flags = MSG_NOSIGNAL;
#else
	msg->msg_flags = 0;
#endif
	msg->msg_iov = iov;
	msg->msg_iovlen = 1;
	handle->ch_msg_msghdr_p = msg;
#ifdef ICS_TCP_SENDPAGE
	handle->ch_msg_pagevec = NULL;
	handle->ch_msg_page_len = 0;
#endif
	handle->ch_msg_inline_iov_p = iov;
	handle->ch_msg_inline_data_p = iov[0].iov_base + sizeof(icshdr_t);
	handle->ch_rep_skb_p = NULL;
	handle->ch_msg_ooldesc_p = handle->ch_ooldesc;
	handle->ch_msg_ooldesc_cnt = 0;
	handle->ch_llflag = 0;
	/*
	 * 2 references in case this turns out to be an RPC:
	 * if it is, it will be added to the icscli handle list
	 * and needs the extra reference to avoid problems with nodedown.
	 */
	handle->ch_ref_cnt = 2;

	/* Initialize the lock. */
	INIT_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);

	return(0);
}

/*
 * icscli_llencode_inline()
 *
 * Description:
 *	Put data into the in-line buffer as a byte stream.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */
void
icscli_llencode_inline(
	cli_handle_t	*handle,
	const void	*param,
	int		size)
{
	if (size <= 0)
		return;
#ifdef DEBUG_TOOLS
	if ((void *)handle->ch_msg_inline_data_p + size >
	    handle->ch_msg_inline_iov_p[0].iov_base +
	    sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE)
		panic("%s:inline data size exceeded\n", __FUNCTION__);
	if (!segment_eq(get_fs(), KERNEL_DS))
		panic("%s:fs must be KERNEL_DS\n", __FUNCTION__);
#ifndef CONFIG_X86_4G
	if ((unsigned long)param < TASK_SIZE)
		panic("%s:user address %p not allowed\n", __FUNCTION__, param);
#endif
#endif
	/* Stuff the argument into the in-line buffer. */
	bcopy(param, handle->ch_msg_inline_data_p, size);
	handle->ch_msg_inline_data_p += size;
        handle->ch_msg_inline_iov_p->iov_len += size;
}

static void
icscli_llhandle_realloc(
	cli_handle_t	*handle,
	int check_ool)
{
	struct iovec	*curiov;
	struct iovec	*newiov;
	ooldesc_t	*curool;
	ooldesc_t	*newool;
	int curlen;
	int newlen;

	curlen = handle->ch_msg_msghdr_p->msg_iovlen;
	SSI_ASSERT(curlen >= ICS_DEF_OOL_SEGMENTS + 1);
	if ((curlen - (ICS_DEF_OOL_SEGMENTS + 1))
	    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
		newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
		curiov = handle->ch_msg_msghdr_p->msg_iov;
		newiov = kmalloc_nofail(newlen * sizeof(*newiov));
		memcpy(newiov, curiov, curlen * sizeof(*newiov));
		handle->ch_msg_msghdr_p->msg_iov = newiov;
		handle->ch_msg_inline_iov_p = newiov;
		if (curiov != handle->ch_iovec)
			kfree(curiov);
	}
	if (check_ool) {
		curlen = handle->ch_msg_ooldesc_cnt;
		if ((curlen - ICS_DEF_OOL_SEGMENTS)
		    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
			newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
			curool = handle->ch_msg_ooldesc_p;
			newool = kmalloc_nofail(newlen * sizeof(*newool));
			memcpy(newool, curool, curlen * sizeof(*newool));
			handle->ch_msg_ooldesc_p = newool;
			if (curool != handle->ch_ooldesc)
				kfree(curool);
		}
	}
}

/*
 * icscli_llencode_ool_data_t()
 *	Set up an OOL buffer with the specified data.
 *
 * Description:
 *	Set up an OOL buffer for an input parameter, with the buffer containing
 *	the specified data.  Called by ICS high-level code to perform argument
 *	marshalling for client/server interaction.
 *
 *	For the TCP implementation, if ICS_CLI_COPY_OOLDATA is not defined,
 *	in order to avoid extra data copies, an esballoc()-type mblk is set
 *	up with the OOL data, and chained to the existing mblk's.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data; data should not be de-allocated until
 *			either the callback routine is called or the message
 *			is sent
 *	len		length (in bytes) of the data
 *	callback	routine that is called after the data is no longer
 *			required; can be NULL; if used, it is typically used
 *			for data de-allocation; callback is called with
 *			two parameters: param (from above) as first parameter
 *			and callarg (below) as 2nd parameter
 *	callarg		2nd parameter to callback routine (if callback routine
 *			is called)
 *
 * NSC_XXX: Earlier implemntations required clumsy 2-level indirection for
 * calling back the freeing routine. This can and needs to be simplified.
 *
 */
int
icscli_llencode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*param,
	long		len,
	void		(*callback)(
				u_char		*param,
				long		callarg),
	long		callarg)
{
	ooldesc_t	*ool_p;
	struct iovec	*iov;
	int		i;

	/* No data? */
	if (len <= 0) {
		/*
		 * There's nothing to encode, so we're "finished" with the
		 * buffer.
		 */
		if (callback != NULL)
			callback(param, callarg);
		return(0);
	}
#ifdef DEBUG_TOOLS
	if (!segment_eq(get_fs(), KERNEL_DS))
		panic("%s:fs must be KERNEL_DS\n", __FUNCTION__);
#ifndef CONFIG_X86_4G
	if ((unsigned long)param < TASK_SIZE)
		panic("%s:user address %p not allowed\n", __FUNCTION__, param);
#endif
#endif

	/* Make sure there is room */
	if (handle->ch_msg_msghdr_p->msg_iovlen >= ICS_DEF_OOL_SEGMENTS + 1)
		icscli_llhandle_realloc(handle, (callback != NULL));

	/* Just point the iovec at the client data. */
	handle->ch_ref_cnt++;
	i = (handle->ch_msg_msghdr_p->msg_iovlen)++;
	iov = &handle->ch_msg_msghdr_p->msg_iov[i];
	iov->iov_base = param;
	iov->iov_len = len;
	/*
	 * If appropriate, set up the buffer freeing routine to be called.
	 */
	if (callback != NULL) {
		i = (handle->ch_msg_ooldesc_cnt)++;
		ool_p = &handle->ch_msg_ooldesc_p[i];
		ool_p->ool_cli_handle = handle;
		ool_p->ool_callback_data_t = callback;
		ool_p->ool_callparam = param;
		ool_p->ool_callarg = callarg;
	}

	return(0);
}

/*
 * icscli_llencoderesp_ool_data_t()
#ifdef SNET
 *	Prepare for extraction of an OOL output parameter into the specified
 *	area
 *
 * Description:
 *	Prepare for extraction of an OOL output parameter for the specified
 *	handle into the user-provided buffer area. Called by ICS high-level
 *	code to perform argument marshalling for client/server interaction.
 *
 *	This routine must be called prior to icscli_llsend(). Note that
 *	icscli_lldecode_ool_data_t() must also be called for each OOL
 *	output parameter.
 *
 *	This routine must be called from thread context.
 *
 *	Note that for the TCP implementation there is nothing to do done.
 *	For implementations where the OOL output parameter is pushed across
 *	using DMA, then some setup may be required here (e.g. setting up
 *	some pointers in the in-line buffer).
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data; must not be NULL
 *	len		length (in bytes) of the data
#endif
 */
int
icscli_llencoderesp_ool_data_t(
	cli_handle_t	*handle,
	u_char		*param,
	long		len)
{
	/* Do Nothing. */
	return(0);
}

int
icscli_llencode_ool_struct_msghdr(
	cli_handle_t *handle,
	struct msghdr *msg,
	int rwflag)
{
	__typeof__ (msg->msg_iovlen) i;

	/*
	 * Encode the iovecs in msghdr; the free routine frees the entire
	 * msghdr.
	 */
	icscli_llencode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));
	icscli_llencode_ool_data_t(handle, (u_char *)msg->msg_iov,
				   msg->msg_iovlen * sizeof(msg->msg_iov[0]),
				   freemsghdr_callback, (long)msg);
#ifdef DEBUG
	printk("%s: msg->msg_iovlen : %d\n",
		__FUNCTION__, (int)msg->msg_iovlen);
#endif /* DEBUG */

	/*
	 * Encode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef DEBUG
		printk("%s: msg->msg_iov[%d].iov_len : %d\n",
		       __FUNCTION__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* DEBUG */
		icscli_llencode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   msg->msg_iov[i].iov_len,
					   NULL, (long)NULL);
	}

	return (0);
}

int
icscli_lldecode_ool_struct_msghdr(
	cli_handle_t *handle,
	struct msghdr **mpp,
	int rwflag)
{
	struct msghdr *msg = *mpp;
	__typeof__ (msg->msg_iovlen) i;
	int alloclen;

	if (msg == NULL)
		*mpp = msg = kmalloc_nofail(sizeof(*msg));

	/*
	 * Decode the number of iovecs in msghdr.
	 */
	icscli_lldecode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));
	alloclen = msg->msg_iovlen * sizeof(msg->msg_iov[0]);
	msg->msg_iov = kmalloc_nofail(alloclen);
	icscli_lldecode_ool_data_t(handle, (u_char *)msg->msg_iov, alloclen);
#ifdef DEBUG
	printk("%s: msg_iovlen: %d\n", __FUNCTION__, (int)msg->msg_iovlen);
#endif /* DEBUG */

	/*
	 * Decode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef DEBUG
		printk("%s: msg_iov[%d].iov_len: %d\n",
		       __FUNCTION__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* DEBUG */
		alloclen = msg->msg_iov[i].iov_len;
		msg->msg_iov[i].iov_base = kmalloc_nofail(alloclen);
		icscli_lldecode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   alloclen);
	}

	return (0);
}

#ifdef ICS_OOL_STRUCT_PAGES
/*
 * icscli_llencode_ool_struct_page_p_p()
 *	Set up an OOL vector with the specified pages.
 *
 * Description:
 *	Set up an OOL vector for an input parameter, with the vector containing
 *	the specified pages.  Called by ICS high-level code to perform argument
 *	marshalling for client/server interaction.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to pagevec; pagevec should not be de-allocated until
 *			either the callback routine is called or the message
 *			is sent
 *	len		length (in bytes) of the data in the pagevec
 *	callback	routine that is called after the data is no longer
 *			required; can be NULL; if used, it is typically used
 *			for data de-allocation; callback is called with
 *			two parameters: param (from above) as first parameter
 *			and callarg (below) as 2nd parameter
 *	callarg		2nd parameter to callback routine (if callback routine
 *			is called)
 *
#ifdef ICS_TCP_SENDPAGE
 * SSI_XXX: No support for more than one OOL pagevec.
 * SSI_XXX: Must be final OOL encode as ICS low-level does sendpage last.
#endif
 *
 */
int
icscli_llencode_ool_struct_page_p_p(
	cli_handle_t	*handle,
	struct page	**pages,
	unsigned long	len,
	void		(*callback)(
				u_char		*param,
				long		callarg),
	long		callarg)
{
#ifdef ICS_TCP_SENDPAGE
	ooldesc_t	*ool_p;
	int i;

	/* No data? */
	if (len == 0) {
		/*
		 * There's nothing to encode, so we're "finished" with the
		 * buffer.
		 */
		if (callback != NULL)
			callback((u_char *)pages, callarg);
		return(0);
	}

#ifdef DEBUG_TOOLS
	if (!segment_eq(get_fs(), KERNEL_DS))
		panic("%s:fs must be KERNEL_DS\n", __FUNCTION__);
#endif

	handle->ch_msg_pagevec = pages;
	handle->ch_msg_page_len = (unsigned int) len;
	BUG_ON(len > UINT_MAX);

	/*
	 * If appropriate, set up the buffer freeing routine to be called.
	 */
	if (callback != NULL) {
		i = (handle->ch_msg_ooldesc_cnt)++;
		ool_p = &handle->ch_msg_ooldesc_p[i];
		ool_p->ool_cli_handle = handle;
		ool_p->ool_callback_data_t = callback;
		ool_p->ool_callparam = (u_char *) pages;
		ool_p->ool_callarg = callarg;
	}
#else
#error not supported
	BUG();
	/* Not reached */
	for (;;) ;
#endif /* !ICS_TCP_SENDPAGE */

	return(0);
}

int
icscli_lldecode_ool_struct_page_p_p(
	cli_handle_t	*handle,
	struct page	**pages,
	unsigned long	len)
{
	void *vaddr;
	unsigned int i, vlen, size, data_len, pgbase;

	if (len == 0)
		return 0;

#define CALC_VLEN(_len, _data_len, _pgbase, _vlen) do { \
	/* Bottom PAGE_CACHE_SHIFT bits reserved for base */ \
	_data_len = ((_len) >> PAGE_CACHE_SHIFT); \
	_pgbase = ((_len) & ~PAGE_CACHE_MASK); \
	_vlen = (_pgbase + _data_len + PAGE_CACHE_SIZE - 1) >> PAGE_CACHE_SHIFT; \
	BUG_ON(!_data_len); \
} while (0)
	CALC_VLEN(len, data_len, pgbase, vlen);
#undef CALC_VLEN

	/*
	 * Decode first page.
	 */
	size = pgbase ? PAGE_CACHE_SIZE - pgbase : PAGE_CACHE_SIZE;
	if (data_len < size) {
		size = data_len;
		BUG_ON(vlen != 1);
	} else
		data_len -= size;

	vaddr = kmap_atomic(pages[0], KM_USER0);
	icscli_lldecode_ool_data_t(handle, vaddr + pgbase, size);
	kunmap_atomic(vaddr, KM_USER0);

	/*
	 * Decode remaining pages.
	 */
	size = PAGE_CACHE_SIZE;
        for (i = 1; i < vlen; i++) {
		if (data_len < size) {
			size = data_len;
			BUG_ON(i + 1 != vlen);
		} else
			data_len -= size;

		vaddr = kmap_atomic(pages[i], KM_USER0);
                icscli_lldecode_ool_data_t(handle, vaddr, size);
		kunmap_atomic(vaddr, KM_USER0);
        }

	return(0);
}
#endif /* ICS_OOL_STRUCT_PAGES */

/*
 * icscli_llsend()
 *	Send a message from the client to the server
 *
 * Description:
 *	This routine is called by the ICS high-level code to actually send
 *	the message from the client to the server.
 *
 *	After obtaining a handle and performing the appropriate argument
 *	marshalling, icscli_llsend() is used to send the message from the
 *	client to the server.
 *
 *	The callback routine is called (by the underlying transport) once
 *	the message has been sent (either successfully or not) to the server,
 *	and (for message/response interactions) the response has been
 *	received. Once the callback routine has been called (or during the
 *	callback routine), output parameters may be decoded (i.e. calls to
 *	icscli_lldecode_*() can be performed) and the handle can be
 *	released.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the client handle associated with the message
 *
 * Returns:
 *      -EREMOTE
 *      0
 */
int
icscli_llsend(
	cli_handle_t	*handle)
{
	struct iovec	*iov;
	icshdr_t	*ihdr_p;
	int		i;
	int		error = 0;
	boolean_t	immediate_callback;
	ics_sock_conn_data_t *conn = NULL;
	ics_llnodeinfo_t *nodeinfo;
	int             ret_status = 0; /* avoid race w sendup_reply
					 * freeing handle
					 */

#ifdef DEBUG_TOOLS
	if (!segment_eq(get_fs(), KERNEL_DS))
		panic("%s:fs not KERNEL_DS\n", __FUNCTION__);
#endif

	/* Set up fields in the message header */
	iov = handle->ch_msg_msghdr_p->msg_iov;
	ihdr_p = (icshdr_t *)iov[0].iov_base;
	ihdr_p->ihdr_marker = ICSHDR_MARKER;
	ihdr_p->ihdr_inline_len = (void *)handle->ch_msg_inline_data_p -
					     iov[0].iov_base - sizeof(icshdr_t);
#ifdef ICS_TCP_SENDPAGE
	if (handle->ch_msg_pagevec != NULL)
		ihdr_p->ihdr_len = (handle->ch_msg_page_len >> PAGE_CACHE_SHIFT);
	else
#endif
	ihdr_p->ihdr_len = 0;
	for (i = 0; i < handle->ch_msg_msghdr_p->msg_iovlen; i++)
		ihdr_p->ihdr_len += iov[i].iov_len;
	ihdr_p->ihdr_version = 1;
	ihdr_p->ihdr_service = handle->ch_service;
	ihdr_p->ihdr_flag = 0;
	if (handle->ch_flag & CLI_HANDLE_NO_REPLY) {
		ihdr_p->ihdr_flag = ICS_NO_REPLY;
		/* Drop extra hold put on handle in icscli_llhandle_init()
		   just in case this was an RPC. */
		SSI_ASSERT(handle->ch_ref_cnt > 0);
		handle->ch_ref_cnt--;
	}

	ihdr_p->ihdr_procnum = handle->ch_procnum;
	ihdr_p->ihdr_transid = handle->ch_transid;
	ihdr_p->ihdr_orignode = this_node;
	ihdr_p->ihdr_chan = handle->ch_chan;
#ifdef NSC_ICSLOG
	ihdr_p->ihdr_uniqueid = handle->ch_uniqueid;
#elif defined(NSC_INHERIT_USER_NICE)
	ihdr_p->ihdr_user_nice = (int)handle->ch_user_nice;
#endif

	/* Log the message. */
	ICSLOG(1, ICSLOG_CLILLSEND, ihdr_p->ihdr_transid,
				    ihdr_p->ihdr_uniqueid,
				    handle->ch_node,
				    ihdr_p->ihdr_service,
				    ihdr_p->ihdr_chan,
				    ihdr_p->ihdr_procnum);

	/*
	 * Find a connection to the node, and send the message if the
	 * connection exists.
	 * NOTE: The connection must be established prior to this call.
	 *
	 * NSC_XXX - The race avoidance here isn't well done due to shortfalls
	 * 	     in ICS nodedown; it should be reexamined when ICS
	 *	     nodedown is fixed.
	 *
	 * NOTE: If there has been an error detected for this handle,
	 * force the error path to be followed.
	 */
	if (handle->ch_status != 0)
		nodeinfo = NULL;
	else
		nodeinfo = ics_llnodeinfo[handle->ch_node];
	if (nodeinfo != NULL) {
		LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		while (nodeinfo->icsni_conn == NULL ||
		       (nodeinfo->icsni_conn[handle->ch_chan] == NULL &&
			(nodeinfo->icsni_flag & ICSNI_NODE_UP))) {
			UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			while (nodeinfo->icsni_conn == NULL ||
			       (nodeinfo->icsni_conn[handle->ch_chan] == NULL &&
				(nodeinfo->icsni_flag & ICSNI_NODE_UP)))
#ifdef LINUX_SSI_EVENT
				if (!TRY_EVENT(&nodeinfo->icsni_chan_event))
					nidelay(HZ/10);
#else
				/* XXX: Using channel-wide event listner
				 * on specific channel.
				 */
				WAIT_EVENT(&nodeinfo->icsni_chan_event);
#endif
			LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		}
		conn = nodeinfo->icsni_conn[handle->ch_chan];
		if (conn != NULL)
			INCR_ATOMIC_INT(&conn->conn_ref_cnt);
		UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (conn != NULL) {
			LOCK_LOCK(&conn->conn_lock);
#ifdef ICS_TCP_SENDPAGE
			error = ics_llsendmsg(conn,
					      handle->ch_msg_msghdr_p,
					      ihdr_p->ihdr_len,
					      handle->ch_msg_pagevec,
					      handle->ch_msg_page_len);
#else
			error = ics_llsendmsg(conn,
					      handle->ch_msg_msghdr_p,
					      ihdr_p->ihdr_len);
#endif
			UNLOCK_LOCK(&conn->conn_lock);
			DECR_ATOMIC_INT(&conn->conn_ref_cnt);
		}
	}

	/*
	 * Do the freebuf callback for OOL data.
	 */
#ifdef DEBUG
        printk(KERN_DEBUG "icscli_llsend: freeing OOL data.\n");
#endif /* DEBUG */
	for (i = 0; i < handle->ch_msg_ooldesc_cnt; i++) {
		register ooldesc_t *ool_p;

		ool_p = &handle->ch_msg_ooldesc_p[i];
		(ool_p->ool_callback_data_t)(ool_p->ool_callparam,
					     ool_p->ool_callarg);
	}
	/*
	 * The ch_ref_cnt count was incremented for every OOL segment encoded in
	 * icscli_llencode_ool_data_t(). Now decrement the count for each encoding.
	 */
	if (handle->ch_msg_msghdr_p->msg_iovlen > 1) {
		LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		handle->ch_ref_cnt -= handle->ch_msg_msghdr_p->msg_iovlen - 1;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	}

	/*
	 * If the message was not sent, then clean up after ourselves. The
	 * callback must be performed either here or later in the one
	 * of the free routines called from freemsg().
	 */
	if (nodeinfo == NULL || conn == NULL || error < 0) {
#ifdef DEBUG
		printk(KERN_DEBUG "icscli_llsend: Error! Message not sent!\n");
#endif /* DEBUG */
		LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		/* RPC? */
		if ((handle->ch_flag & CLI_HANDLE_NO_REPLY) == 0) {
			/* Yes: handshake with icscli_llnodedown(). */
			if ((handle->ch_llflag & CLI_HANDLE_REPLY_DONE) == 0) {
				handle->ch_llflag |= CLI_HANDLE_REPLY_DONE;
				handle->ch_status = -EREMOTE;
				SSI_ASSERT(handle->ch_ref_cnt > 0);
				handle->ch_ref_cnt--;
			}
		}
		SSI_ASSERT(handle->ch_ref_cnt > 0);
		handle->ch_ref_cnt--;
		immediate_callback = (handle->ch_ref_cnt == 0);
		ret_status = handle->ch_status;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		if (immediate_callback) {
#ifdef DEBUG
			printk(KERN_DEBUG "icscli_llsend: calling icscli_sendup_reply()\n");
#endif /* DEBUG */
			icscli_sendup_reply(handle);
		}
		return(ret_status);
	}

	/*
	 * See if we can perform an immediate callback, and do so if we
	 * can. This can happen if there was no OOL data or the OOL
	 * data was sent and freed very fast and if any reply was
	 * received very fast.
	 */
	LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	SSI_ASSERT(handle->ch_ref_cnt > 0);
	handle->ch_ref_cnt--;
	immediate_callback = (handle->ch_ref_cnt == 0);
	UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	if (immediate_callback) {
#ifdef DEBUG
                        printk(KERN_DEBUG "icscli_llsend: calling icscli_sendup_reply()\n");
#endif /* DEBUG */
		icscli_sendup_reply(handle);
	}

	return(ret_status);
}

/*
 * icscli_llsendup_reply()
 *	Set up some stuff and perform the callback specified by icscli_llsend().
 *
 * Description:
 *	This is a routine that is called by the network bottom half handler
 *	when a complete ICS datagram has arrived from the server node with the
 *	reply to a call to icscli_llsend(). This routine is responsible for
 *	performing some preliminary setup, and then possibly calling the
 *	high-level ICS callback routine.
 *
 *	Note that for the TCP implementation, the reply may arrive before
 *	the message send has completed (because of TCP retry characteristics).
 *	Because the message send may re-reference the OOL data, we don't
 *	want to do the high-level ICS callback until both the reply has arrived
 *	and the send has completed.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	skb		pointer to sk_buff that contains the entire reply
 */
void
icscli_llsendup_reply(
	struct ics_reass_buf *rbuf)
{
	cli_handle_t	*handle;
	int		len_left;
	boolean_t	do_callup;
	icshdr_t	*icshdr = &rbuf->rb_icshdr;

	/* Figure out the client handle that corresponds to the reply. */
	handle = icscli_find_transid_handle(icshdr->ihdr_transid);

	/* Store the sk_buff pointer so it can be freed later. */
	handle->ch_rep_skb_p = rbuf->rb_hskbp;
	handle->ch_rep_tskb_p = rbuf->rb_tskbp;

	/* Set up pointers for the in-line buffer. */
	handle->ch_rep_inline_skb_p = rbuf->rb_rskbp;
	handle->ch_rep_inline_data_p = rbuf->rb_rdatap;

	ICSLOG(1, ICSLOG_CLILLSENDUP, icshdr->ihdr_transid,
				      icshdr->ihdr_uniqueid,
				      handle->ch_node,
				      icshdr->ihdr_service,
				      icshdr->ihdr_chan,
				      icshdr->ihdr_procnum);

	/*
	 * Skip over the in-line buffer without copying it anywhere.
	 * Note that if there is no in-line data or out-of-line data, then
	 * skb == NULL, and ics_skb_bcopy() doesn't like this. So we don't
	 * call ics_skb_bcopy() if there is no in-line data to skip over.
	 */
	if (icshdr->ihdr_inline_len > 0) {
		len_left = ics_skb_bcopy(&rbuf->rb_rskbp, &rbuf->rb_rdatap,
					 NULL, icshdr->ihdr_inline_len);
		SSI_ASSERT(len_left == 0);
	}

	/* Set up pointers for the out-of-line buffer. */
	handle->ch_rep_ool_skb_p = rbuf->rb_rskbp;
	handle->ch_rep_ool_data_p = rbuf->rb_rdatap;

	/*
	 * Most likely we do the high-level ICS callup now. But it is possible
	 * that OOL data has not been freed yet, and this will perform the
	 * callup if we can't.
	 */
	LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	SSI_ASSERT((handle->ch_llflag & CLI_HANDLE_REPLY_DONE) == 0);
	handle->ch_llflag |= CLI_HANDLE_REPLY_DONE;
	SSI_ASSERT(handle->ch_ref_cnt > 0);
	handle->ch_ref_cnt--;
	do_callup = (handle->ch_ref_cnt == 0);
	UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
	if (do_callup)
		icscli_sendup_reply(handle);
}

/*
 * icscli_lldecode_inline()
 *
 * Description:
 *	Pull data from the in-line buffer as a byte stream.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */
void
icscli_lldecode_inline(
	cli_handle_t	*handle,
	void		*param,
	int		size)
{
	int lenleft;

	if (size > 0) {
#ifdef DEBUG_TOOLS
		if (!segment_eq(get_fs(), KERNEL_DS))
			panic("%s:fs must be KERNEL_DS\n", __FUNCTION__);
#ifndef CONFIG_X86_4G
		if (param != NULL && (unsigned long)param < TASK_SIZE)
			panic("%s:user address %p not allowed\n",
			      __FUNCTION__, param);
#endif
#endif
		lenleft = ics_skb_bcopy(&handle->ch_rep_inline_skb_p,
				        &handle->ch_rep_inline_data_p,
				        param, size);
		SSI_ASSERT(lenleft == 0);
	}
}

/*
 * icscli_lldecode_ool_data_t()
 *	Extract an OOL output parameter into the specified area
 *
 * Description:
 *	Extract an OOL output parameter for the specified handle into
 *	the user-provided buffer area. Called by ICS high-level code
 *	to perform argument marshalling for client/server interaction.
 *
 *	This routine must be called after icscli_llsend().
 *
#ifdef SNET
 *	Note that icscli_llencoderesp_ool_data_t() must also be called for
 *	each output parameter prior to the call to icscli_llsend().
#endif
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to array of characters; must not be NULL
 *	nelem		number of elements in the array
 */
int
icscli_lldecode_ool_data_t(
	cli_handle_t	*handle,
	u_char		*param,
	long		len)
{
	int lenleft;

	if (len > 0) {
#ifdef DEBUG_TOOLS
		if (!segment_eq(get_fs(), KERNEL_DS))
			panic("%s:fs must be KERNEL_DS\n", __FUNCTION__);
#ifndef CONFIG_X86_4G
		if ((unsigned long)param < TASK_SIZE)
			panic("%s:user address %p not allowed\n",
			      __FUNCTION__, param);
#endif
#endif
		lenleft = ics_skb_bcopy(&handle->ch_rep_ool_skb_p,
				        &handle->ch_rep_ool_data_p,
				        param, len);
		SSI_ASSERT(lenleft == 0);
	}

	return(0);
}

EXPORT_SYMBOL(icscli_llencode_inline);
EXPORT_SYMBOL(icscli_llencode_ool_data_t);
EXPORT_SYMBOL(icscli_lldecode_inline);
EXPORT_SYMBOL(icscli_lldecode_ool_data_t);

/*
 * icscli_llhandle_deinit()
 *	Perform any deinitialization of the handle necessary for low-level ICS.
 *
 * Description:
 *	This routine is called by ICS high level code when a client handle is
 *	about to be released.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle to be deinitialized
 *
 * Return value:
 */
void
icscli_llhandle_deinit(
	cli_handle_t	*handle)
{
	/* Deinitialize any handle locks. */
	DEINIT_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);

	/*
	 * Free outgoing msghdr and iovecs.
	 * We only free the inline data. It is the responsibility
	 * of the caller to free the OOL data.
	 */
	if (handle->ch_msg_msghdr_p->msg_iov != handle->ch_iovec)
		kfree(handle->ch_msg_msghdr_p->msg_iov);
	if (handle->ch_msg_ooldesc_p != handle->ch_ooldesc)
		kfree(handle->ch_msg_ooldesc_p);

	/* Free incoming sk_buffs. */
	ics_llfreemsg(&handle->ch_rep_skb_p, handle->ch_rep_tskb_p);
}

/*
 * icscli_llnodedown()
 *	Perform the low-level client portion of nodedown logic.
 *
 * Description:
 *	This routine is called by the ics_llnodedown() routine after all the
 *	connections to the down node have been torn down.
 *
 *	This routine looks at all the currently outstanding RPCs to the node
 *	that is down and performs the callback on each of them.
 *
 *	This routine  must be called from thread context.
 *
 * Parameters:
 *	node		the node that is being declared down
 */
/* Lock ordering: chlist_lock, ch_lllock */
void
icscli_llnodedown(
	clusternode_t	node)
{
	cli_handle_t	*handle;
	clihand_list_t	*chl_p = &clihand_list[CLIHAND_HASH(node)];
	boolean_t	immediate_callback;

	LOCK_SPIN_LOCK(&chl_p->chlist_lock);

	/*
	 * Go through all the handles on the list.
	 */
	for (handle = chl_p->chlist_first;
			handle != NULL;
			handle = handle->ch_next) {

		/* Eliminate handles that don't refer to the node going down. */
		if (handle->ch_node != node)
			continue;

		/*
		 * If a callback has been done, then skip the handle.
		 * Otherwise, mark the callback as done, and see if it
		 * can be performed immediately, or whether the call
		 * to freemsg() will perform it.
		 */
		LOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		if ((handle->ch_llflag & CLI_HANDLE_REPLY_DONE) != 0) {
			UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
			continue;
		}
		handle->ch_llflag |= CLI_HANDLE_REPLY_DONE;
		SSI_ASSERT(handle->ch_ref_cnt > 0);
		handle->ch_ref_cnt--;
		immediate_callback = (handle->ch_ref_cnt == 0);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&handle->ch_lllock);
		handle->ch_status = -EREMOTE;
		if (immediate_callback)
			icscli_sendup_reply(handle);
	}

	UNLOCK_SPIN_LOCK(&chl_p->chlist_lock);
}

#if defined(DEBUG) || defined(DEBUG_TOOLS)
/*
 * Print an ICS handle
 */
void
print_icscli_llhandle(cli_llhandle_t *generic)
{
	tcp_cli_llhandle_t *cli = &generic->tcpcli_llhandle;

	printk(KERN_DEBUG
	       "\n_ch_msg_msghdr_p 0x%p _ch_msg_inline_iov_p 0x%p\n",
			cli->_ch_msg_msghdr_p,
			cli->_ch_msg_inline_iov_p);

	printk(KERN_DEBUG "\n_ch_msg_inline_data_p 0x%p \n",
			cli->_ch_msg_inline_data_p);

	printk(KERN_DEBUG "\n_ch_msg_ooldesc_p 0x%p _ch_msg_ooldesc_cnt %d\n",
	       cli->_ch_msg_ooldesc_p, cli->_ch_msg_ooldesc_cnt);

	printk(KERN_DEBUG "\n_ch_rep_skb_p 0x%p _ch_rep_tskb_p 0x%p\n",
			cli->_ch_rep_skb_p, cli->_ch_rep_tskb_p);

	printk(KERN_DEBUG "\n_ch_rep_inline_skb_p 0x%p  "
			 "_ch_rep_inline_data_p 0x%p \n",
			 cli->_ch_rep_inline_skb_p,
			 cli->_ch_rep_inline_data_p);

	printk(KERN_DEBUG "\n_ch_rep_ool_skb_p 0x%p  "
			 "_ch_rep_ool_data_p 0x%p \n",
			 cli->_ch_rep_ool_skb_p,
			 cli->_ch_rep_ool_data_p);

	printk(KERN_DEBUG "\n Reference Count %d\n", cli->_ch_ref_cnt);

	printk(KERN_DEBUG "\n_ch_llflag %d _ch_lllock 0x%p \n",
			  cli->_ch_llflag, &(cli->_ch_lllock));

	return;

}

#endif /* DEBUG || DEBUG_TOOLS */
