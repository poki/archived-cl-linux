/*
 * 	Low-level ICS packet receive code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the put and service procedures for receiving NSC ICS
 * datagrams over a socket-based transport.
 */

#include <linux/net.h>
#include <linux/random.h>
#include <net/sock.h>
#include <cluster/nsc.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */

extern void ics_recvmsg(struct sock*, struct sk_buff *);

/*
 * ics_dgram()
 *	Process NSC Datagrams
 *
 * Description:
 *	This procedure is called from tcp when data is ready on a socket,
 *	to process NSC datagrams.  It processes the sk_buffs that arrive
 *	from the transport and creates sk_buff messages that correspond
 *	to complete ICS messages/replies.
 *
 *      For clarification:
 *      rbuf->rb_len - total length of sk_buff chain on reassembly queue
 *      rbuf->rb_icshdr.ihdr_len - expected length of ics message
 *
 *
 * Parameters:
 *	sock	- pointer to socket
 *	len	- length of data
 */

static inline int
__ics_dgram_parse(struct ics_reass_buf *rbufp, int chan, clusternode_t node)
{
	int			len;
	struct sk_buff		*tskbp;
	u_char			*tdatap;
	unsigned int		tlen;

	/*
	 * If the datagram does not even include the entire
	 * header, go back and wait for it.
	 */
	if (rbufp->rb_len < sizeof(icshdr_t)) {
		ICSLOG(16, ICSLOG_DGRAMMSGSMALL, node,
		       chan, rbufp->rb_len,
		       sizeof(icshdr_t), 0, 0);
		return 0;
	}
	/*
	 * If we haven't copied the header before to get the
	 * total message length that we are expecting, do so now
	 */
	if (rbufp->rb_icshdr.ihdr_len == 0) {
		len = ics_skb_bcopy(&rbufp->rb_rskbp,
				    &rbufp->rb_rdatap,
				    (u_char *)&rbufp->rb_icshdr,
				    sizeof(rbufp->rb_icshdr));
		SSI_ASSERT(len == 0);
		SSI_ASSERT(rbufp->rb_icshdr.ihdr_marker == ICSHDR_MARKER);
		SSI_ASSERT(rbufp->rb_icshdr.ihdr_chan == chan);
		SSI_ASSERT(rbufp->rb_icshdr.ihdr_orignode == node);
		ICSLOG(16, ICSLOG_DGRAMMSGHDRL, node, chan,
		       rbufp->rb_len,
		       rbufp->rb_icshdr.ihdr_len,
		       rbufp->rb_icshdr.ihdr_transid,
		       rbufp->rb_icshdr.ihdr_uniqueid);
		if (rbufp->rb_icshdr.ihdr_version != 1)
			panic("%s:wrong version number %d on ICS"
			      " message\n",
			      __FUNCTION__, rbufp->rb_icshdr.ihdr_version);
	}
	/*
	 * If the length of the datagram was longer than the header
	 * but less than the expected length of the message, then
	 * wait for the rest of the message in the next datagram.
	 */
	if (rbufp->rb_len < rbufp->rb_icshdr.ihdr_len) {
		ICSLOG(16, ICSLOG_DGRAMMSGINC, node, chan,
		       rbufp->rb_len,
		       rbufp->rb_icshdr.ihdr_len,
		       rbufp->rb_icshdr.ihdr_transid,
		       rbufp->rb_icshdr.ihdr_uniqueid);
		return 0;
	}
	tskbp = NULL;
	tlen = 0;
	tdatap = rbufp->rb_tskbp->tail;
	/*
	 * If the length of the datagram was longer than the expected
	 * length of the message, then we break the datagram into two
	 * pieces. The part of the datagram that we are not going to
	 * deal with now (i.e. the part after the message we are dealing
	 * with currently) is put back on the queue. The service
	 * procedure for the stream will take care of it on the next
	 * go-around.
	 */
	if (rbufp->rb_len > rbufp->rb_icshdr.ihdr_len) {
		ICSLOG(16, ICSLOG_DGRAMMULTMSG, node, chan,
		       rbufp->rb_len,
		       rbufp->rb_icshdr.ihdr_len,
		       rbufp->rb_icshdr.ihdr_transid,
		       rbufp->rb_icshdr.ihdr_uniqueid);
		/*
		 * The split must occur in the last sk_buff on the
		 * list. Calculate the length of the last sk_buff needed
		 * to complete the current ICS message and save the
		 * pointer for the start of the next message. The
		 * skb_get() places an extra hold on the
		 * sk_buff.
		 */
		tskbp = skb_get(rbufp->rb_tskbp);
		tlen = rbufp->rb_len - rbufp->rb_icshdr.ihdr_len;
		tdatap -= tlen;
	}
	rbufp->rb_tdatap = tdatap;
	rbufp->rb_len = tlen;
	ICSLOG(16, ICSLOG_DGRAMUPCALL, node, chan,
	       rbufp->rb_icshdr.ihdr_transid,
	       rbufp->rb_icshdr.ihdr_uniqueid,
	       (long)rbufp->rb_hskbp, (long)rbufp->rb_hdatap);

	return 1;
}

int
ics_dgram_parse(struct ics_reass_buf *rbufp, int chan, clusternode_t node)
{
	return __ics_dgram_parse(rbufp, chan, node);
}

void
ics_dgram(
	struct sock *sockp,
	int len)
{
	ics_sock_conn_data_t	*connp;
	struct ics_reass_buf	*rbufp;
	struct sk_buff		*skbp;

	SSI_ASSERT(sockp);
	/*
	 * Get a pointer to the private data for this stream.
	 */
	connp = (ics_sock_conn_data_t *)sockp->sk_user_data;
	SSI_ASSERT(connp);
	/*
	 * Get a pointer to the reassembly buffer for this stream.
	 */
	rbufp = &(connp->conn_rbuf);
	/*
	 * Get the incoming datagrams from the sock structure; hang them off
	 * the reassembly queue. Most if the time there will be only one,
	 * but if tcp processes an out-of-order packet there can be more.
	 * I could cheat and grab the entire chain at once, but I don't think
	 * it is worth the effort since the case where there is more than one
	 * skb is so rare.
	 */
	while ((skbp = __skb_dequeue(&sockp->sk_receive_queue)) != NULL) {
		/*
		 * Hack to update correct TCP data structures, since
		 * tcp_recvmsg() is not being called.
		 */
		ics_recvmsg(sockp, skbp);
		SSI_ASSERT(atomic_read(&skbp->users) == 1);
		SSI_ASSERT(skbp->next == NULL);
		if (rbufp->rb_hskbp == NULL) {
			SSI_ASSERT(rbufp->rb_len == 0);
			SSI_ASSERT(rbufp->rb_icshdr.ihdr_len == 0);
			rbufp->rb_hskbp = skbp;
			rbufp->rb_hdatap = skbp->data;
			rbufp->rb_rskbp = skbp;
			rbufp->rb_rdatap = skbp->data;
			rbufp->rb_tskbp = skbp;
		} else {
			SSI_ASSERT(rbufp->rb_tskbp != NULL);
			SSI_ASSERT(rbufp->rb_tskbp->next == NULL);
			rbufp->rb_tskbp->next = skbp;
			wmb();
			rbufp->rb_tskbp = skbp;
		}
		rbufp->rb_len += skbp->len;
#ifdef DEBUG
		if (connp->conn_chan == ics_reply_chan ||
		    connp->conn_chan == ics_reply_prio_chan)
			printk(KERN_DEBUG "%s: pid %d rb_len %u\n",
				__FUNCTION__, current->pid, rbufp->rb_len);
#endif
		while (__ics_dgram_parse(rbufp, connp->conn_chan,
					 connp->conn_node)) {
			add_ics_randomness(connp->conn_rcv_rand_statep,
					   ((unsigned long)rbufp->rb_hdatap +
					   rbufp->rb_len) / sizeof(int));
			if (connp->conn_chan == ics_reply_chan ||
			    connp->conn_chan == ics_reply_prio_chan)
				icscli_llsendup_reply(rbufp);
			else {
				icssvr_llsendup_msg(rbufp);
				/*
				 * Note: If icssvr_llsendup_msg() queues
				 * the message, it will also queue all the
				 * messages in the tail sk_buff and advance
				 * rbufp accordingly. The code below will
				 * be redundant in this case.
				 */
			}
			/* Is there more data in the sk_buff? */
			rbufp->rb_icshdr.ihdr_len = 0;
			if (rbufp->rb_len == 0) {
				/* No: reassembly list is empty. */
				rbufp->rb_hskbp = NULL;
				break;
			} else {
				/* Yes: set up to process data. */
				rbufp->rb_hskbp =  rbufp->rb_tskbp;
				rbufp->rb_rskbp =  rbufp->rb_tskbp;
				rbufp->rb_hdatap = rbufp->rb_tdatap;
				rbufp->rb_rdatap = rbufp->rb_tdatap;
			}
		}
	}
}
