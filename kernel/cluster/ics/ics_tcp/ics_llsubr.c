/*
 * 	Low-level ICS sk_buff copy code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * Subroutines used by the TCP ICS client and server code
 */

#include <linux/skbuff.h>
#include <asm/uaccess.h>
#include <asm/hardirq.h>

#include <cluster/assert.h>
#include <cluster/ics/ics_private.h>

/*
 * ics_skb_bcopy()
 * 	Copy data from a series of sk_buffs to a user-specified buffer.
 *
 * Description:
 *	Copy data from a message represented by a series of sk_buffs into the
 *	user-specified buffer. This call is structured so that series of
 *	calls can be made to extract all the information in the buffer.
 *
 *	This routine is designed to be run alongside ics_skb_copyout() and
 *	ics_skb_extract_skb() in copying data out of a series of sk_buffs.
 *
 *	This routine may be called from interrupt context.
 *
 * Arguments:
 *	skb_p_p		address of a pointer to an sk_buff; on entry the pointer
 *			has the current sk_buff to start copying from; on exit
 *			the pointer has the sk_buff to be used for the next
 *			call to ics_skb_*() (i.e. on exit the pointer
 *			represents the sk_buff where the (last byte + 1) of data
 *			copied resides)
 *	buf_p_p		address of a pointer to data within an sk_buff's buffer;
 *			on entry the pointer points within the sk_buff's buffer
 *			to where data copying is to start from [note that this
 *			routine is able to deal with the case where
 *			(*buf_p_p == (*skb)->tail) (i.e. it can deal with
 *			with the case where the pointer points
 *			at the end of an sk_buff's buffer)]; on exit the
 *			pointer points to the next data pointer to be used
 *			for the next call to ics_skb_*() (i.e. on exit
 *			the pointer represents a pointer within the buffer of
 *			the sk_buff returned in skb_p where the (last byte + 1)
 *			of data copied resides)
 *	data		where data is to be copied to; if this is NULL, then
 *			this routine actually does no copying, but merely
 *			skips over the specified length
 *	len		length to be copied
 *
 * Returns:
 *	Number of bytes of 'len' that were not copied due to running out of
 *	buffer space in the sk_buff's buffer.
 */
int
ics_skb_bcopy(
	struct sk_buff	**skb_p_p,
	u_char		**buf_p_p,
	u_char		*data,
	long		len)
{
	struct sk_buff	*skb_p = *skb_p_p;
	u_char		*buf_p = *buf_p_p;
	long		copylen;

/*
 * KS_XXX: It's possible that we pass ics_skb_bcopy empty sk_buff chains.
 * For instance, when the server routine of an RPC fails, only the error
 * status is encoded and sent back to the client. The client, however,
 * will blindly try to decode all expected return values, before doing
 * any kind of error checking. Therefore, the assertion here is not
 * guaranteed to succeed in all cases. We circumvent error cases here by
 * returning 0, and hope that the client will perform proper error checking
 * later in the RPC code.
 *
 *	SSI_ASSERT(skb_p != NULL);
 *	SSI_ASSERT(buf_p != NULL);
 *	SSI_ASSERT(len > 0);
 */

	if (skb_p == NULL || buf_p == NULL || len <= 0)
		return 0;

	/*
	 * Loop through as many sk_buffs as necessary, copying data into the
	 * caller's buffer area. Note the assumption that the routine is
	 * always called with some data left in the current sk_buff.
	 */
	for (;;) {
		/*
		 * Copy as much as possible from current sk_buff. Note that the
		 * length to copy may end up being zero if we are currently
		 * pointing at the end of the data in the sk_buff.
		 */
		copylen = min(len, (long)(skb_p->tail - buf_p));
		if (data != NULL && copylen > 0) {
			memcpy(data, buf_p, copylen);
			data += copylen;
		}

		/*
		 * Increment data pointers. If necessary, switch to the
		 * next sk_buff in the chain.
		 */
		buf_p += copylen;
		len -= copylen;
		/*
		 * The len check is here because is here because I do not
		 * want to null out the skb_p pointer if we are done.
		 * This saves a check and fix in __ics_dgram() in the
		 * case where the icshdr_t is at the end of a skbuff.
		 */
		if (len == 0)
			break;
		if (buf_p == skb_p->tail) {
			skb_p = skb_p->next;
			if (skb_p == NULL)
				break;
			buf_p = skb_p->data;
		}
	}

	/* Return updated pointers to sk_buff and buffer */
	*skb_p_p = skb_p;
	*buf_p_p = buf_p;
	return(len);
}

