/*
 * 	Low-level ICS transport code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the routine ics_lltransport_config() and its related
 * subroutines. This is an initialization routine that does all the
 * stack manipulation to allow ICS to use TCP as its transport.
 */

#include <linux/ctype.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/route.h>
#include <linux/inet.h>
#include <linux/netdevice.h>
#include <linux/inetdevice.h>
#include <asm/uaccess.h>
#include <net/route.h>
#include <linux/if.h>
#include <cluster/nsc.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */
#include <cluster/clms.h>

static int ics_getifconfig(char *);
static char *ics_getinterfaces(void);

int ics_lltransport_config_done = 0;

/*
 * ics_lltransport_config()
 *	Routine called at system boot time to configure the ICS transport.
 *
 * Description:
 *	This routine gets the ifconfig parameters specified in the
 *	kernel-boot command line.
 *	It sets up the tcp/ip protocol stack.
 *
 *	It configures the network device and the local loop back driver.
 *
 *
 * NSC_XXX: Need to add some hook here to prevent ifconfig program from
 * 	    trying to configure the ICS device at the user level.
 *
 *
 * Calling/Exit State:
 *	Called very early in main, before root has been mounted.
 *
 *
 * Parameters:
 *	None.
 */
void
ics_lltransport_config(void)
{
	struct net_device *dev;
	int ifconfig_ok = 1;
	char *intf;
	mm_segment_t oldfs;

	/* SSI_XXX: This is going away in a future release */
	if (ics_getifconfig(cluster_ifconfig)) {
		ifconfig_ok = 0;
	}

	while ((intf = ics_getinterfaces())) {
		/* Set this when IFCONFIG= not specified above */
		if (strlen(ics_netaddr) == 0) {
			struct ifreq ifr;
			struct sockaddr_in sin = {AF_INET};
			int error;
			__u32 addr;

			oldfs = get_fs();
			set_fs(KERNEL_DS);
			strncpy(ifr.ifr_name, intf, IFNAMSIZ);
			ifr.ifr_addr = *(struct sockaddr *)&sin;
			error = devinet_ioctl(SIOCGIFADDR, (void *)&ifr);
			if (error)
				printk(KERN_WARNING "Can't get interface addr for %s\n", intf);
			else {
				addr = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr;
				sprintf(ics_netaddr, "%d.%d.%d.%d", NIPQUAD(addr));
			}
			set_fs(oldfs);
		}
		if ( (dev = dev_get_by_name(intf)) != NULL) {
			dev->priv_flags |= IFF_ICS;
			dev_put(dev);
		} else
			printk(KERN_WARNING "ics_lltransport_config: Could't set IFF_ICS on %s.\n",
				intf);
	}

	/* SSI_XXX: This is going away in a future release */
	if (ifconfig_ok) {
		if ( (dev = dev_get_by_name(ics_ifname)) != NULL) {
			dev->priv_flags |= IFF_ICS;
			dev_put(dev);
		} else
			printk(KERN_WARNING "ics_lltransport_config: Could't set IFF_ICS on %s.\n",
				ics_ifname);
	}

	ics_lltransport_config_done = 1;
	return;
}

/*
 * ics_getinterfaces()
 *	Parse the interfaces string specified in cluster prep phase
 *
 * Description:
 *	This function parses the interfaces information and stores the
 *	information in gobal variables.
 *
 * 	format of ifconfig information given is
 *		<device>:<device>...
 *	For example:
 *		INTERFACES=bond0:eth0:eth1
 *		INTERFACES=eth0
 *
 * Return Values:
 *	Returns unprocessed device
 *      NULL    no more device to return
 */
static char *
ics_getinterfaces(void)
{
	static char intr[ICS_MAX_IFNAME_SIZE];
	char	*s;

	if (cluster_interfaces == NULL)
		return NULL;

	/*
	 * Get the interface device name.
	 */
	s = clms_next_token(&cluster_interfaces, ':');
	if (s == NULL)
		return NULL;

	strncpy(intr, s, ICS_MAX_IFNAME_SIZE);
	return intr;
}

/* SSI_XXX: This is going away in a future release */
/*
 * ics_getifconfig()
 *	Parse the ifconfig string specified in the kernel-boot command line.
 *
 * Description:
 *	This function parses the ifconfig information and stores the
 *	information in gobal variables.
 *
 * 	format of ifconfig information given is
 *		<device>:<ipaddr>:<netmask>
 *	For example:
 *		IFCONFIG=eth0:168.87.155.51:255.255.255.128
 *
 *	The interface name is stored in ics_ifname[] as
 *		"<device>"
 *	In our example:
 *		"eth0"
 *	The device is stored in ics_ifdevname as "<device>".
 *	The IP address is stored in ics_netaddr[] as a character string.
 *	The netmask is stored in ics_netmask[] as a character string.
 *
 * Parameters:
 *	ifp	- pointer to ifconfig information string.
 *
 * Return Values:
 *	Returns 0 on success and an error number on failure.
 */
static int
ics_getifconfig(
	char	*ifp)
{

	char	*s;

	/*
	 * Get the interface device name.
	 */
	s = clms_next_token(&ifp, ':');
	if (s == NULL) {
		return(-EINVAL);
	}
	strncpy(ics_ifdevname, s, ICS_MAX_IFNAME_SIZE);
	/*
	 * The devname is also the beginning of the interface name.
	 */
	strncpy(ics_ifname, ics_ifdevname, ICS_MAX_IFNAME_SIZE);
	/*
	 * Get the IP address.
	 */
	s = clms_next_token(&ifp, ':');
	if (s == NULL) {
		printk(KERN_WARNING "ics_getifconfig: Invalid ics_netaddr.\n");
		return(-EINVAL);
	}
	strncpy(ics_netaddr, s, ICS_IPADDRLEN);
	/*
	 * Get the netmask.
	 */
	s = clms_next_token(&ifp, ':');
	if (s == NULL) {
		printk(KERN_WARNING "ics_getifconfig: Invalid ics_netmask.\n");
		return(-EINVAL);
	}
	strncpy(ics_netmask, s, ICS_IPADDRLEN);
	return 0;
}
