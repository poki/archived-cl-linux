/*
 * 	Low-level ICS server code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Portions Copyright 2007 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/uio.h>
#include <asm/uaccess.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */

/* Forward references. */
static void icssvr_llprocess_msg(svr_handle_t *, struct ics_reass_buf *);

/*
 * Data structures dealing with the list of incoming messages that have
 * arrived for which no server handles were available.
 */

typedef struct {
	SOFTIRQ_SPIN_LOCK_T	msglist_lock;
	struct sk_buff	*msglist_hskbp;
	u_char		*msglist_hdatap;
	struct sk_buff	*msglist_tskbp;
	u_char		*msglist_tdatap;
} icsmsg_arrival_t;

icsmsg_arrival_t icsmsg_arrival[ICS_MAX_CHANNELS];

#ifdef		DEBUG
/*
 * Don't do throttling if this is set (for debugging purposes).
 */
static int	ics_nothrottle;
#endif		/* DEBUG */
/*
 * icssvr_llinit()
 *	Perform any relevant server initialization.
 */
void
icssvr_llinit()
{
	int		i;

	for (i = 0; i < ics_num_channels; i++) {
		INIT_SOFTIRQ_SPIN_LOCK(&icsmsg_arrival[i].msglist_lock);
	}
}

/*
 * icssvr_llhandle_init()
 *	Initialize a handle that has not been used
 *
 * Description:
 *	This routine is called by ICS high level code to initialize the
 *	low-level ICS portion of a handle.
 *
 *	This routine cannot assume that the low-level portion of the client
 *	handle has been initialized to all zeroes.
 *
 *	If flag is TRUE, then this routine must be called from thread
 *	context. If flag is FALSE, then this routine may be called from
 *	interrupt context.
 *
 * Parameters:
 *	handle		the handle to be initialized
 *	sleep_flag	this routine can sleep waiting for resources/memory
 *
 * Return value:
 *	If sleep_flag is TRUE, then this routine always returns 0 (success).
 *	If sleep_flag is FALSE, then this routine returns -EAGAIN if it
 *	could not allocate resources/memory without sleeping, and 0 (success)
 *	otherwise.
 */
int
icssvr_llhandle_init(
	svr_handle_t	*handle,
	boolean_t	sleep_flag)
{
	struct iovec	*iov;
	struct msghdr	*msg;

	(void)sleep_flag;

	msg = &handle->sh_msghdr;
	iov = handle->sh_iovec;
	iov[0].iov_base = handle->sh_inline;
	msg->msg_name = NULL;
	msg->msg_namelen = 0;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = 0;
	msg->msg_iov = iov;
	msg->msg_iovlen = 0;
	handle->sh_rep_msghdr_p = msg;
#ifdef ICS_TCP_SENDPAGE
	handle->sh_rep_pagevec = NULL;
	handle->sh_rep_page_len = 0;
#endif
	handle->sh_rep_inline_iov_p = NULL;
	handle->sh_rep_inline_data_p = NULL;
	handle->sh_msg_skb_p = NULL;
	handle->sh_rep_ooldesc = handle->sh_ooldesc;
	handle->sh_rep_ooldesc_cnt = 0;

	/* Initialize the lock. */
	INIT_SPIN_LOCK(&handle->sh_lllock);

	return 0;
}

/*
 * icssvr_llhandle_init_for_recv()
 *	Prepare a handle so that a message can be received
 *
 * Description:
 *	This routine is called by ICS high level code to prepare a handle
 *	to receive a message from a client.
 *
 *	Note that icssvr_llhandle_init() has been called at least once
 *	for the handle, but that icssvr_llhandle_init_for_recv() can be called
 *	many times for the same handle.
 *
 *	If sleep_flag is TRUE, then this routine must be called from thread
 *	context. If sleep_flag is FALSE, then this routine may be called from
 *	interrupt context.
 *
 * Parameters:
 *	handle		the handle to be initialized
 *	sleep_flag	if TRUE this routine can sleep waiting for
 *			resources/memory
 *
 * Return value:
 *	If sleep_flag is TRUE, then this routine always returns 0 (success).
 *	If sleep_flag is FALSE, then this routine returns -EAGAIN if it
 *	could not allocate resources/memory without sleeping, and 0 (success)
 *	otherwise.
 *
 */

int
icssvr_llhandle_init_for_recv(
	svr_handle_t	*handle,
	boolean_t	sleep_flag)
{
	/*
	 * If there is an unfreed incoming message, then free it.
	 */
	ics_llfreemsg(&handle->sh_msg_skb_p, handle->sh_msg_tskb_p);

	/* Reset iov and ooldesc buffers. */
	if (handle->sh_rep_msghdr_p->msg_iov != handle->sh_iovec) {
		kfree(handle->sh_rep_msghdr_p->msg_iov);
		handle->sh_rep_msghdr_p->msg_iov = handle->sh_iovec;
	}
#ifdef ICS_TCP_SENDPAGE
	handle->sh_rep_pagevec = NULL;
	handle->sh_rep_page_len = 0;
#endif
	handle->sh_rep_msghdr_p->msg_iovlen = 1;
	if (handle->sh_rep_ooldesc != handle->sh_ooldesc) {
		kfree(handle->sh_rep_ooldesc);
		handle->sh_rep_ooldesc = handle->sh_ooldesc;
	}
	handle->sh_rep_ooldesc_cnt = 0;

	handle->sh_rep_inline_iov_p = &handle->sh_rep_msghdr_p->msg_iov[0];
	handle->sh_rep_inline_data_p =
		handle->sh_rep_inline_iov_p->iov_base + sizeof(icshdr_t);
	handle->sh_rep_inline_iov_p->iov_len = sizeof(icshdr_t);

	/* Initialize the reference count */
	handle->sh_ref_cnt = 1;

	return(0);
}

/*
 * Low-level ICS queueing:
 *
 * In my drive to make sure that we didn't have to do any allocations
 * in low-level ICS, I lost sight of the fact that we have a single
 * queue at this point for all the nodes. So, if the last sk_buff in
 * at the tail of the current queue contains the beginning of
 * an uncompleted message, I have to be *very* careful, because the
 * next message queued can come from another node.
 *
 * I "steal" the list, sk, and dev pointers so I don't have to allocate
 * control structures.
 */

static inline void
__icssvr_llenqueue(icsmsg_arrival_t *msgl_p,
		   struct ics_reass_buf *rbufp,
		   int chan)
{
	ICSLOG(16, ICSLOG_SVRLLEQ, rbufp->rb_icshdr.ihdr_transid,
	       rbufp->rb_icshdr.ihdr_uniqueid,
	       rbufp->rb_icshdr.ihdr_orignode, chan,
	       rbufp->rb_hskbp != rbufp->rb_tskbp,
	       msgl_p->msglist_hskbp == NULL);
	if (msgl_p->msglist_hskbp == NULL) {
		msgl_p->msglist_hskbp = rbufp->rb_hskbp;
		msgl_p->msglist_hdatap = rbufp->rb_hdatap;
	} else if (msgl_p->msglist_tskbp != rbufp->rb_hskbp) {
		SSI_ASSERT(msgl_p->msglist_tskbp->sk == NULL);
		msgl_p->msglist_tskbp->sk = (void *)rbufp->rb_hskbp;
		msgl_p->msglist_tskbp->dev = (void *)rbufp->rb_hdatap;
	}
	msgl_p->msglist_tskbp = rbufp->rb_tskbp;
	msgl_p->msglist_tdatap = rbufp->rb_tdatap;
	/* Does the message span more than one sk_buff? */
	if (rbufp->rb_hskbp != rbufp->rb_tskbp) {
		/* Yes: save tail info to prevent rescans. */
		SSI_ASSERT(rbufp->rb_hskbp->prev == NULL);
		SSI_ASSERT(rbufp->rb_hskbp->list == NULL);
		rbufp->rb_hskbp->prev = rbufp->rb_tskbp;
		rbufp->rb_hskbp->list = (void *)rbufp->rb_tdatap;
	}
}

static void
icssvr_llenqueue(icsmsg_arrival_t *msgl_p,
		 struct ics_reass_buf *rbufp,
		 int chan,
		 int dotail)
{
	__icssvr_llenqueue(msgl_p, rbufp, chan);
	/* Are the any other messages in the tail of the sk_buff? */
	if (dotail) {
		/* Yes: gotta get'em all */
		for (;;) {
			/* Is there more data in the sk_buff? */
			rbufp->rb_icshdr.ihdr_len = 0;
			if (rbufp->rb_len == 0) {
				/* No: reassembly list is empty. */
				rbufp->rb_hskbp = NULL;
				break;
			} else {
				/* Yes: set up to process data. */
				rbufp->rb_hskbp =  rbufp->rb_tskbp;
				rbufp->rb_rskbp =  rbufp->rb_tskbp;
				rbufp->rb_hdatap = rbufp->rb_tdatap;
				rbufp->rb_rdatap = rbufp->rb_tdatap;
			}
			if (!ics_dgram_parse(rbufp, chan,
					     rbufp->rb_icshdr.ihdr_orignode))
				break;
			__icssvr_llenqueue(msgl_p, rbufp, chan);
		}
	}
}

static int
icssvr_lldequeue_peek(icsmsg_arrival_t *msgl_p,
		      struct ics_reass_buf *rbufp,
		      int chan)
{
	struct sk_buff	*hskbp;
	struct sk_buff	*nskbp;
	u_char 		*hdatap;
	int		datalen;
	int		len;

	(void)chan;
	/* If there are no more messages then we are done. */
	if (msgl_p->msglist_hskbp == NULL)
		return 0;
	/*
	 * Get a temporary copy of the ICS header so we can figure
	 * out the service that is relevant.
	 *
	 * We have to be very careful here; if the message
	 * spans the end of the sk_buff, it is possible the next sk_buff
	 * for it isn't "next" in the sense of the queue and we have to
	 * skip it for now.
	 */
	hskbp = msgl_p->msglist_hskbp;
	hdatap = msgl_p->msglist_hdatap;
	datalen = hskbp->tail - hdatap;
	nskbp = (void *)hskbp->sk;
	/* Is there a break in the chain? */
	if (nskbp != NULL) {
		/* Yes: does the header cross the end of the buffer? */
		if (datalen < sizeof(rbufp->rb_icshdr)) {
			/* Yes: start at next buffer. */
			hskbp->sk = NULL;
			hdatap = (void *)hskbp->dev;
			hskbp = nskbp;
			nskbp = NULL;
			msgl_p->msglist_hskbp = hskbp;
			msgl_p->msglist_hdatap = hdatap;
		}
	}
	/* Read the ICS header. */
	rbufp->rb_rskbp = hskbp;
	rbufp->rb_rdatap = hdatap;
	len = ics_skb_bcopy(&rbufp->rb_rskbp, &rbufp->rb_rdatap,
			    (u_char *)&rbufp->rb_icshdr,
			    sizeof(rbufp->rb_icshdr));
	SSI_ASSERT(len == 0);
	/* Is there a break in the chain? */
	if (nskbp != NULL) {
		/* Yes: does the message cross the end of the buffer? */
		if (datalen < rbufp->rb_icshdr.ihdr_len) {
			/* Yes: start at next buffer. */
			hskbp->sk = NULL;
			hdatap = (void *)hskbp->dev;
			hskbp = nskbp;
			msgl_p->msglist_hskbp = hskbp;
			msgl_p->msglist_hdatap = hdatap;
			/* Reread the header. */
			rbufp->rb_rskbp = hskbp;
			rbufp->rb_rdatap = hdatap;
			len = ics_skb_bcopy(&rbufp->rb_rskbp,
					    &rbufp->rb_rdatap,
					    (u_char *)&rbufp->rb_icshdr,
					    sizeof(rbufp->rb_icshdr));
			SSI_ASSERT(len == 0);
		}
	}

	return 1;
}

static void
icssvr_lldequeue(icsmsg_arrival_t *msgl_p,
		 struct ics_reass_buf *rbufp,
		 int chan)
{
#ifdef NSC_ICSLOG
	int		multi = 0;
	int		last = 0;
#endif
	int		datalen;

	/* Recreate the rbuf for the message. */
	rbufp->rb_hskbp = msgl_p->msglist_hskbp;
	rbufp->rb_hdatap = msgl_p->msglist_hdatap;
	datalen = rbufp->rb_hskbp->tail - rbufp->rb_hdatap;
	/* Does this message cross the end of the buffer? */
	if (rbufp->rb_icshdr.ihdr_len > datalen) {
		/* Yes: used saved tail info. */
		rbufp->rb_tskbp = rbufp->rb_hskbp->prev;
		rbufp->rb_tdatap = (void *)rbufp->rb_hskbp->list;
		rbufp->rb_hskbp->prev = NULL;
		rbufp->rb_hskbp->list = NULL;
#ifdef NSC_ICSLOG
		multi = 1;
#endif
	} else {
		/* No: compute tail of message. */
		rbufp->rb_tskbp = rbufp->rb_hskbp;
		rbufp->rb_tdatap = rbufp->rb_hdatap + rbufp->rb_icshdr.ihdr_len;
	}
	/* Is the tail at the end of an sk_buff? */
	if (rbufp->rb_tdatap == rbufp->rb_tskbp->tail) {
		/* Yes: set next head to the next sk_buff. */
		msgl_p->msglist_hskbp = (void *)rbufp->rb_tskbp->sk;
		msgl_p->msglist_hdatap = (void *)rbufp->rb_tskbp->dev;
		rbufp->rb_tskbp->sk = NULL;
#ifdef NSC_ICSLOG
		if (msgl_p->msglist_hskbp == NULL)
			last = 1;
#endif
	} else {
		/* No: set next head to the tail of the message. */
		msgl_p->msglist_hskbp = rbufp->rb_tskbp;
		msgl_p->msglist_hdatap = rbufp->rb_tdatap;
		/* Are we at the tail of the messages we know about? */
		if (msgl_p->msglist_hdatap == msgl_p->msglist_tdatap) {
			/*
			 * Yes: we're "done". There is another
			 * message in the sk_buff, but we don't
			 * Have the information about it, yet.
			 */
			msgl_p->msglist_hskbp = NULL;
#ifdef NSC_ICSLOG
			last = 1;
#endif
		}
	}
	ICSLOG(16, ICSLOG_SVRLLDQ, rbufp->rb_icshdr.ihdr_transid,
	       rbufp->rb_icshdr.ihdr_uniqueid,
	       rbufp->rb_icshdr.ihdr_orignode,
	       chan, multi, last);
}

void
icssvr_llnodedown(clusternode_t node, int chan)
{
	icsmsg_arrival_t *msgl_p = &icsmsg_arrival[chan];
	icsmsg_arrival_t msgl;
	struct ics_reass_buf rbuf;

	if (chan >= ics_num_channels)
		return;

	/* Drop all messages on the queue from the down node. */
	LOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
	ICSLOG(16, ICSLOG_SVRLLND, node, chan, (long)"start", 0, 0, 0);
	msgl = *msgl_p;
	msgl_p->msglist_hskbp = NULL;
	while (icssvr_lldequeue_peek(&msgl, &rbuf, chan)) {
		icssvr_lldequeue(&msgl, &rbuf, chan);
		if (rbuf.rb_icshdr.ihdr_orignode == node) {
			ICSLOG(16, ICSLOG_SVRLLNDDROP,
			       rbuf.rb_icshdr.ihdr_transid,
			       rbuf.rb_icshdr.ihdr_uniqueid,
			       rbuf.rb_icshdr.ihdr_orignode, chan,
			       rbuf.rb_hskbp != rbuf.rb_tskbp,
			       msgl.msglist_hskbp == NULL);
			__ics_llfreemsg(rbuf.rb_hskbp, rbuf.rb_tskbp);
		} else
			icssvr_llenqueue(msgl_p, &rbuf, chan, 0);
	}
	ICSLOG(16, ICSLOG_SVRLLND, node, chan, (long)"end", 0, 0, 0);
	UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
}


/*
 * icssvr_llsendup_msg()
 *	Deal with arriving message sent up from TCP service procedure.
 *
 * Description:
 *	This is a routine that is called on a server node when a NSC message
 *	arrives from a client node. It is called by the TCP
 *	service procedure that handles incoming messages.
 *
 *	The routine is responsible for obtaining a free handle from the
 *	high-level ICS code, if such a handle is available. If the handle
 *	is available, the message is processed by calling
 *	icssvr_llprocess_msg(). If the handle is not available, then
 *	the message is queued on an internal queue, and it will be processed
 *	once handles are available. [Some sort of flow control may need to
 *	be done here also].
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	rbuf	pointer to reassembly buffer
 */
void
icssvr_llsendup_msg(
	struct ics_reass_buf *rbufp)
{
	int chan = rbufp->rb_icshdr.ihdr_chan;
	int svc = rbufp->rb_icshdr.ihdr_service;
	icsmsg_arrival_t *msgl_p = &icsmsg_arrival[chan];
	svr_handle_t	*handle;

	/*
	 * This lock must be held across the call to icssvr_recv_find_handle()
	 * to prevent a window where the shwait_nohandles flag can get
	 * set and icssvr_llhandles_present() gets called before the message
	 * is placed on the queue.
	 */
	LOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);

	/*
	 * If there are messages queued up already, then we add this message
	 * to the queue. We do this indiscriminate queuing to process the
	 * messages in order. (Not strictly necessary from an
	 * ICS perspective, but it does prevent problems where new messages
	 * steal all the handles from queued messages.)
	 */
	if (msgl_p->msglist_hskbp != NULL) {
		icssvr_llenqueue(msgl_p, rbufp, chan, 1);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
		SIGNAL_EVENT(&icssvr_nanny_event);
		return;
	}

	/*
	 * See if there are any handles. If there are, then we process the
	 * message. If there are not, then we start up a message queue.
	 * NSC_XXX: this transport doesn't (yet) support the flags arg.
	 */
	handle = icssvr_find_recv_handle(chan, svc, 0);
	if (handle != NULL) {
		icssvr_llprocess_msg(handle, rbufp);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
	} else {
		icssvr_llenqueue(msgl_p, rbufp, chan, 1);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
		SIGNAL_EVENT(&icssvr_nanny_event);
	}
}

/*
 * icssvr_llhandles_present()
 *	Inform low-level server code that handles for a channel are available
 *
 * Description:
 *	This call is made by high-level ICS code when a server handle for
 *	a particular communications becomes available and the prior call to
 *	icssvr_find_recv_handle() failed.
 *
 *	This call merely informs low-level ICS code that handles are
 *	available - it is possible that another thread may actually use
 *	the handle immediately.
 *
 *	For the TCP implementation, we obtain a message from
 *	the queued list of waiting messages and obtain a handle by calling
 *	icssvr_find_recv_handle(). Once we have both, icssvr_llprocess_msg()
 *	is called to actually deal with the message.
 *
 *	This routine may be called from interrupt context
 *
 * Parameters:
 *	chan		communication channel number being used
 */
static void
__icssvr_llhandles_present(
	int		chan)
{
	icsmsg_arrival_t *msgl_p = &icsmsg_arrival[chan];
	svr_handle_t	*handle;
	struct ics_reass_buf rbuf;

	/*
	 * As long as there are server handles present and messages on the
	 * queue, continue processing messages.
	 */
	for(;;) {
		LOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);

		/* Peek and find out if there is a message pending. */
		if (!icssvr_lldequeue_peek(msgl_p, &rbuf, chan)) {
			UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
			break;
		}
		/*
		 * Get a handle from the high-level ICS code. If there
		 * are none, then we are done.
		 */
		handle = icssvr_find_recv_handle(chan,
						 rbuf.rb_icshdr.ihdr_service,
						 0);
		if (handle == NULL) {
			UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);
			break;
		}
		/* Pull the message off the queue. */
		icssvr_lldequeue(msgl_p, &rbuf, chan);
		UNLOCK_SOFTIRQ_SPIN_LOCK(&msgl_p->msglist_lock);

		/*
		 * With the msglist lock unlocked, process the message.
		 */
		icssvr_llprocess_msg(handle, &rbuf);
	}
}

void
icssvr_llhandles_present(
	int		chan)
{
	int c;

	/*
	 * There can be high priority messages that can use handles
	 * for this channel; try them all first.
	 */
	if (chan < ics_min_prio_chan || chan > ics_max_prio_chan) {
		for (c = ics_min_prio_chan ; c <= ics_max_prio_chan; c++)
			__icssvr_llhandles_present(c);
	}
	__icssvr_llhandles_present(chan);
}

/*
 * icssvr_llprocess_msg()
 *	Final preparations with an incoming message to the server.
 *
 * Description:
 *	This routine is an internal low-level ICS routine that is called
 *	when the combination of a server handle/NSC message pair is
 *	present.
 *
 *	It is called to perform any low-level ICS setup necessary for the
 *	handle, so that future decodes and encodes can be performed.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle	server handle used to process the message
 *	skb	sk_buff chain representing the incoming message
 */
static void
icssvr_llprocess_msg(
	svr_handle_t	*handle,
	struct ics_reass_buf *rbuf)
{
	int		len;
	icshdr_t	*icshdr = &rbuf->rb_icshdr;

	/* Save a pointer to the message as a whole. */
	handle->sh_msg_skb_p = rbuf->rb_hskbp;
	handle->sh_msg_tskb_p = rbuf->rb_tskbp;

	/* Save information from the message header. */
	SSI_ASSERT(ICS_CHAN(handle->sh_service) == ICS_CHAN(icshdr->ihdr_service));
	handle->sh_service = icshdr->ihdr_service;
	handle->sh_chan = icshdr->ihdr_chan;
	handle->sh_transid = icshdr->ihdr_transid;
#ifdef NSC_ICSLOG
	handle->sh_uniqueid = icshdr->ihdr_uniqueid;
#elif defined(NSC_INHERIT_USER_NICE)
	handle->sh_user_nice = (long)icshdr->ihdr_user_nice; 
#endif
	handle->sh_node = icshdr->ihdr_orignode;
	handle->sh_procnum = icshdr->ihdr_procnum;
	if ((icshdr->ihdr_flag & ICS_NO_REPLY) != 0)
		handle->sh_flag |= SVR_HANDLE_NO_REPLY;
	else
		handle->sh_flag &= ~SVR_HANDLE_NO_REPLY;

	/* Set up the pointers to the in-line portion of the message. */
	handle->sh_msg_inline_skb_p = rbuf->rb_rskbp;
	handle->sh_msg_inline_data_p = rbuf->rb_rdatap;

	/* Skip over the in-line portion of the message, if extant. */
	if (icshdr->ihdr_inline_len > 0) {
		len = ics_skb_bcopy(&rbuf->rb_rskbp, &rbuf->rb_rdatap,
				    NULL, icshdr->ihdr_inline_len);
		SSI_ASSERT(len == 0);
	}

	ICSLOG(1, ICSLOG_SVRLLPROCMSG, icshdr->ihdr_transid,
				       icshdr->ihdr_uniqueid,
				       icshdr->ihdr_orignode,
				       icshdr->ihdr_service,
				       icshdr->ihdr_chan,
				       icshdr->ihdr_procnum);

	/* Set up the pointers to the OOL portion of the message. */
	handle->sh_msg_ool_skb_p = rbuf->rb_rskbp;
	handle->sh_msg_ool_data_p = rbuf->rb_rdatap;

	/* Inform the high-level ICS code the message has arrived. */
	icssvr_sendup_msg(handle);
}

/*
 * icssvr_lldecode_inline()
 *
 * Description:
 *	Pull data from the in-line buffer as a byte stream.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */
void
icssvr_lldecode_inline(
	svr_handle_t	*handle,
	void		*param,
	int		size)
{
	int lenleft;

	if (size > 0) {
		lenleft = ics_skb_bcopy(&handle->sh_msg_inline_skb_p,
				        &handle->sh_msg_inline_data_p,
				        param, size);
		SSI_ASSERT(lenleft == 0);
	}
}

/*
 * icssvr_lldecode_ool_data_t()
 *	Extract an OOL input parameter into the specified area
 *
 * Description:
 *	Extract an OOL input parameter for the specified handle into
 *	the user-provided buffer area. Called by ICS high-level code
 *	to perform argument marshalling for client/server interaction.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to array of characters; must not be NULL
 *	nelem		number of elements in the array
 */
int
icssvr_lldecode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*param,
	long		len)
{
	int lenleft;

	if (len > 0) {
		lenleft = ics_skb_bcopy(&handle->sh_msg_ool_skb_p,
					&handle->sh_msg_ool_data_p,
					param, len);
		SSI_ASSERT(lenleft == 0);
	}

	return(0);
}

int
icssvr_llencode_ool_struct_msghdr(
	svr_handle_t *handle,
	struct msghdr *msg,
	int rwflag)
{
	__typeof__ (msg->msg_iovlen) i;

	/*
	 * Encode the iovecs in msghdr; the free routine frees the entire
	 * msghdr.
	 */
	icssvr_llencode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));
	icssvr_llencode_ool_data_t(handle, (u_char *)msg->msg_iov,
				   msg->msg_iovlen * sizeof(msg->msg_iov[0]),
				   freemsghdr_callback, (long)msg);
#ifdef DEBUG
	printk("%s: msg->msg_iovlen : %d\n",
		__FUNCTION__, (int)msg->msg_iovlen);
#endif /* DEBUG */

	/*
	 * Encode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef DEBUG
		printk("%s: msg->msg_iov[%d].iov_len : %d\n",
		       __FUNCTION__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* DEBUG */
		icssvr_llencode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   msg->msg_iov[i].iov_len,
					   NULL, (long)NULL);
	}

	return (0);
}

int
icssvr_lldecode_ool_struct_msghdr(
	svr_handle_t *handle,
	struct msghdr **mpp,
	int rwflag)
{
	struct msghdr *msg = *mpp;
	__typeof__ (msg->msg_iovlen) i;
	int alloclen;

	if (msg == NULL)
		*mpp = msg = kmalloc_nofail(sizeof(*msg));

	/*
	 * Decode the number of iovecs in msghdr.
	 */
	icssvr_lldecode_inline(handle, &msg->msg_iovlen,
			       sizeof(msg->msg_iovlen));
	alloclen = msg->msg_iovlen * sizeof(msg->msg_iov[0]);
	msg->msg_iov = kmalloc_nofail(alloclen);
	icssvr_lldecode_ool_data_t(handle, (u_char *)msg->msg_iov, alloclen);
#ifdef DEBUG
	printk("%s: msg_iovlen: %d\n", __FUNCTION__, (int)msg->msg_iovlen);
#endif /* DEBUG */

	/*
	 * Decode individual iovecs.
	 */
	for (i = 0; i < msg->msg_iovlen; i++) {
#ifdef DEBUG
		printk("%s: msg_iov[%d].iov_len: %d\n",
		       __FUNCTION__, (int)i, (int)msg->msg_iov[i].iov_len);
#endif /* DEBUG */
		alloclen = msg->msg_iov[i].iov_len;
		msg->msg_iov[i].iov_base = kmalloc_nofail(alloclen);
		icssvr_lldecode_ool_data_t(handle,
					   msg->msg_iov[i].iov_base,
					   alloclen);
	}

	return (0);
}

#ifdef ICS_OOL_STRUCT_PAGES
/*
 * icssvr_llencode_ool_struct_page_p_p()
 *	Set up an OOL vector with the specified pages.
 *
 * Description:
 *	Set up an OOL vector for an input parameter, with the vector containing
 *	the specified pages.  Called by ICS high-level code to perform argument
 *	marshalling for client/server interaction.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to pagevec; pagevec should not be de-allocated until
 *			either the callback routine is called or the message
 *			is sent
 *	len		length (in bytes) of the data in the pagevec
 *	callback	routine that is called after the data is no longer
 *			required; can be NULL; if used, it is typically used
 *			for data de-allocation; callback is called with
 *			two parameters: param (from above) as first parameter
 *			and callarg (below) as 2nd parameter
 *	callarg		2nd parameter to callback routine (if callback routine
 *			is called)
 *
#ifdef ICS_TCP_SENDPAGE
 * SSI_XXX: No support for more than one OOL vector.
 * SSI_XXX: Must be final OOL encode as ICS low-level does sendpage last.
#endif
 *
 */
int
icssvr_llencode_ool_struct_page_p_p(
	svr_handle_t	*handle,
	struct page	**pages,
	unsigned long	len,
	void		(*callback)(
				u_char		*param,
				long		callarg),
	long		callarg)
{
#ifdef ICS_TCP_SENDPAGE
	ooldesc_t	*ool_p;

	/* No data? */
	if (len == 0) {
		/*
		 * There's nothing to encode, so we're "finished" with the
		 * buffer.
		 */
		if (callback != NULL)
			callback((u_char *)pages, callarg);
		return(0);
	}

#ifdef DEBUG_TOOLS
	if (!segment_eq(get_fs(), KERNEL_DS))
		panic("%s:fs must be KERNEL_DS\n", __FUNCTION__);
#endif

	handle->sh_rep_pagevec = pages;
	handle->sh_rep_page_len = (len & PAGE_MASK); /* server-side pgbase 0 */
	BUG_ON(len > UINT_MAX);

	/*
	 * If appropriate, set up the buffer freeing routine to be called.
	 */
	if (callback != NULL) {
		ool_p = &handle->sh_rep_ooldesc[(handle->sh_rep_ooldesc_cnt)++];
		ool_p->ool_svr_handle = handle;
		ool_p->ool_callback_data_t = callback;
		ool_p->ool_callparam = (u_char *)pages;
		ool_p->ool_callarg = callarg;
		handle->sh_ref_cnt++;
	}
#else
#error not supported
	BUG();
	/* Not reached */
	for (;;) ;
#endif /* !ICS_TCP_SENDPAGE */

	return(0);
}

int
icssvr_lldecode_ool_struct_page_p_p(
	svr_handle_t	*handle,
	struct page	***pagevec,
	unsigned long	len)
{
	struct page **pages;
	unsigned int vlen, size, data_len, i;
#ifndef CFS_PAGES_CACHE
	unsigned int array_size;
#else
	extern struct page ** cfs_pagevec_alloc(unsigned int);
	extern void cfs_pagevec_free(struct page **);
#endif

	if (len == 0)
		return 0;

#define CALC_VLEN(_len, _data_len, _vlen) do { \
	/* Bottom PAGE_SHIFT bits reserved for base */ \
	_data_len = ((unsigned)(_len) >> PAGE_SHIFT); \
	_vlen = (_data_len + PAGE_SIZE - 1) >> PAGE_SHIFT; \
	BUG_ON(!_data_len); \
} while (0)
	CALC_VLEN(len, data_len, vlen);
#undef CALC_VLEN

	/*
	 * Allocate the pagevec array.
	 */
#ifdef CFS_PAGES_CACHE
	*pagevec = pages = cfs_pagevec_alloc(vlen);
	BUG_ON(PageHighMem(*pages));

	/*
	 * Decode pages.
	 */
	size = PAGE_SIZE;
        for (i = 0; i < vlen; i++) {
		if (data_len < size) {
			size = data_len;
			BUG_ON(i + 1 != vlen);
		} else
			data_len -= size;

                icssvr_lldecode_ool_data_t(handle, page_address(pages[i]), size);
        }
#if 0
	while (data_len > 0) {
		if (data_len < size)
			size = data_len;

                icssvr_lldecode_ool_data_t(handle, page_address(*pages), size);
		pages++;
		data_len -= size;
        }
#endif
#else
	array_size = (vlen * sizeof(struct page *));

	BUG_ON(array_size > PAGE_SIZE);
        *pagevec = pages = kmalloc_nofail(array_size);

        for (i = 0; i < vlen; i++)
                pages[i] = alloc_page(GFP_KERNEL|__GFP_NOFAIL);

	/*
	 * Decode pages.
	 */
	size = PAGE_SIZE;
        for (i = 0; i < vlen; i++) {
		if (data_len < size) {
			size = data_len;
			BUG_ON(i + 1 != vlen);
		} else
			data_len -= size;

                icssvr_lldecode_ool_data_t(handle, page_address(pages[i]), size);
        }
#endif /* !CFS_PAGES_CACHE */

	return(0);
}
#endif /* ICS_OOL_STRUCT_PAGES */


/*
 * icssvr_lldecode_done()
 *	Give an indication that all icssvr_lldecode_*() routines are complete.
 *
 * Description:
 *	Called after the final icssvr_lldecode_*() routine has completed.
 *	This routine is called to give the low-level code a chance to free
 *	up resources associated with the incoming message (if there
 *	are resources to be freed).
 *
#ifdef SNET
 *	Note that it may not be possible to free the inline buffer pertaining
 *	to the incoming message - if this is a message that requires a reply
 *	and the icscli_llencoderesp_ool*() routines actually encode data
 *	into the buffer, then the inline buffer is required for the
 *	icssvr_lldecode_ool*() routines to function. For this case the
 *	low-level ICS implementation may wish to encode a special flag
 *	into the header to indicate whether the inline buffer can be freed
 *	or re-used.
#endif
 *
 *	This routine is optional - if it is not called, then the freeing
 *	of the resources (if any) will not be performed until the call to
 *	icssvr_llreply().
 *
 *	This routine may be called from interrupt context.
 *
 *	For the non-SNET implementation, the inline buffer (and any other
 *	data from the incoming message) can be freed.
 *
 * Parameters:
 *	handle		server handle associated with the incoming message
 */
void
icssvr_lldecode_done(
	svr_handle_t	*handle)
{
	SSI_ASSERT(handle->sh_msg_skb_p != NULL);
	/*
	 * If a OOL segment has been set up using icssvr_llencode_ool_data_t(),
	 * then we skip any freeing and will perform it in icssvr_llreply().
	 */
	SSI_ASSERT(handle->sh_rep_ooldesc_cnt==0 || handle->sh_rep_ooldesc_cnt==1);
	if (handle->sh_rep_ooldesc_cnt > 0)
		return;
	/* Free the message itself. */
	ics_llfreemsg(&handle->sh_msg_skb_p, handle->sh_msg_tskb_p);
}

/*
 * icssvr_llrewind()
 *	Undo any icssvr_llencode*() calls that have been done so far.
 *
 * Description:
 *	Undo any calls to icssvr_llencode*() that have been done on this
 *	handle so far. This routine is typically called when the
 *	server encounters errors in calls to icssvr_encode*() routines
 *	and wishes to undo the work done so far and simply return an
 *	error return value in icssvr_reply().
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the handle being operated on
 */
void
icssvr_llrewind(
	svr_handle_t	*handle)
{
	ooldesc_t	*ool_p;
	int 		i;

	/*
	 * If there is any OOL data so far, then free it and make things
	 * look like no OOL data.
	 */

	/* Free up OOL segments that specified a callback. */
	for (i = 0; i < handle->sh_rep_ooldesc_cnt; i++) {
		ool_p = &handle->sh_rep_ooldesc[i];
		(ool_p->ool_callback_data_t)(ool_p->ool_callparam,
					     ool_p->ool_callarg);
	}
	handle->sh_rep_ooldesc_cnt = 0;
	handle->sh_ref_cnt = 1;
	/* No more OOL data. */
	handle->sh_rep_msghdr_p->msg_iovlen = 1;
	/*
	 * Make sure that in-line encodes from now on start at the beginning
	 * of the in-line buffer.
	 */
	handle->sh_rep_inline_data_p
		= handle->sh_rep_msghdr_p->msg_iov[0].iov_base
		  + sizeof(icshdr_t);
	/* Reset inline data length. */
	handle->sh_rep_msghdr_p->msg_iov[0].iov_len = sizeof(icshdr_t);
}
/*
 * icssvr_llencode_inline()
 *
 * Description:
 *	Put data into the in-line buffer as a byte stream.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data buffer
 *	size		size in bytes
 */
void
icssvr_llencode_inline(
	svr_handle_t	*handle,
	const void	*param,
	int		size)
{
	SSI_ASSERT(size >= 0);
	if (size <= 0)
		return;
	if ((void *)handle->sh_rep_inline_data_p + size >
	    handle->sh_rep_inline_iov_p[0].iov_base +
	    sizeof(icshdr_t) + ICS_MAX_INLINE_DATA_SIZE)
		panic("%s:inline data size exceeded\n", __FUNCTION__);
	/* Stuff the argument into the in-line buffer. */
	bcopy(param, handle->sh_rep_inline_data_p, size);
	handle->sh_rep_inline_data_p += size;
        handle->sh_rep_inline_iov_p->iov_len += size;
}

static void
icssvr_llhandle_realloc(
	svr_handle_t	*handle,
	int check_ool)
{
	struct iovec	*curiov;
	struct iovec	*newiov;
	ooldesc_t	*curool;
	ooldesc_t	*newool;
	int curlen;
	int newlen;

	curlen = handle->sh_rep_msghdr_p->msg_iovlen;
	SSI_ASSERT(curlen >= ICS_DEF_OOL_SEGMENTS + 1);
	if ((curlen - (ICS_DEF_OOL_SEGMENTS + 1))
	    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
		newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
		curiov = handle->sh_rep_msghdr_p->msg_iov;
		newiov = kmalloc_nofail(newlen * sizeof(*newiov));
		memcpy(newiov, curiov, curlen * sizeof(*newiov));
		handle->sh_rep_msghdr_p->msg_iov = newiov;
		handle->sh_rep_inline_iov_p = newiov;
		if (curiov != handle->sh_iovec)
			kfree(curiov);
	}
	if (check_ool) {
		curlen = handle->sh_rep_ooldesc_cnt;
		if ((curlen - ICS_DEF_OOL_SEGMENTS)
		    % ICS_OOL_SEGMENTS_CHUNKS == 0) {
			newlen = curlen + ICS_OOL_SEGMENTS_CHUNKS;
			curool = handle->sh_rep_ooldesc;
			newool = kmalloc_nofail(newlen * sizeof(*newool));
			memcpy(newool, curool, curlen * sizeof(*newool));
			handle->sh_rep_ooldesc = newool;
			if (curool != handle->sh_ooldesc)
				kfree(curool);
		}
	}
}

/*
 * icssvr_llencode_ool_data_t()
 * 	Set up an OOL buffer with the specified data.
 *
 * Description:
 *	Set up an OOL buffer for an input parameter, with the buffer containing
 *	the specified data.  Called by ICS high-level code to perform argument
 *	marshalling for client/server interaction.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	handle		handle for this client/server interaction
 *	param		pointer to data; data should not be de-allocated until
 *			either the callback routine is called or the message
 *			is sent
 *	len		length (in bytes) of the data
 *	callback	routine that is called after the data is no longer
 *			required; can be NULL; if used, it is typically used
 *			for data de-allocation; callback is called with
 *			two parameters: param (from above) as first parameter
 *			and callarg (below) as 2nd parameter
 *	callarg		2nd parameter to callback routine (if callback routine
 *			is called)
 *
 * NSC_XXX: Earlier implemntations required clumsy 2-level indirection for
 * calling back the freeing routine. This can and needs to be simplified.
 */
int
icssvr_llencode_ool_data_t(
	svr_handle_t	*handle,
	u_char		*param,
	long		len,
	void		(*callback)(
				u_char		*param,
				long		callarg),
	long		callarg)
{
	ooldesc_t	*ool_p;
	struct iovec	*iov;

	SSI_ASSERT((handle->sh_flag & SVR_HANDLE_NO_REPLY) == 0);

	/* Do nothing if there is no data. */
	if (len == 0) {
		if (callback != NULL)
			callback(param, callarg);
		return(0);
	}

	/* Make sure there is room. */
	if (handle->sh_rep_msghdr_p->msg_iovlen >= ICS_DEF_OOL_SEGMENTS + 1)
		icssvr_llhandle_realloc(handle, (callback != NULL));

	/* Just point the iovec at the server data. */
	iov = &handle->sh_rep_msghdr_p->msg_iov[(handle->sh_rep_msghdr_p->msg_iovlen)++];
	iov->iov_base = param;
	iov->iov_len = len;

	/*
	 * If appropriate, set up the buffer freeing routine to be called.
	 */
	if (callback != NULL) {
		ool_p = &handle->sh_rep_ooldesc[(handle->sh_rep_ooldesc_cnt)++];
		ool_p->ool_svr_handle = handle;
		ool_p->ool_callback_data_t = callback;
		ool_p->ool_callparam = param;
		ool_p->ool_callarg = callarg;
		handle->sh_ref_cnt++;
	}


	return(0);
}

EXPORT_SYMBOL(icssvr_llencode_inline);
EXPORT_SYMBOL(icssvr_lldecode_inline);
EXPORT_SYMBOL(icssvr_lldecode_done);
EXPORT_SYMBOL(icssvr_llrewind);

/*
 * icssvr_llreply()
 *	Reply to the client node.
 *
 * Description:
 *	This routine is called to reply to the client node. If the client
 *	sent a plain asynchronous message, then this routine tells the
 *	transport that all incoming arguments have been decoded and the
 *	high-level code is done with the handle. If the client sent the
 *	request side of a request/response message, then this routine
 *	is called to send the response back to the client.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle of the message
 */
void
icssvr_llreply(
	svr_handle_t	*handle)
{
	struct iovec	*iov;
	icshdr_t	*ihdr_p;
	int		i;
	int		error = 0;
	boolean_t	immediate_reply;
	ics_sock_conn_data_t *conn = NULL;
	ics_llnodeinfo_t *nodeinfo;
	mm_segment_t	oldfs = get_fs();

	/* Free the message itself (if icssvr_lldecode_done() wasn't called). */
	ics_llfreemsg(&handle->sh_msg_skb_p, handle->sh_msg_tskb_p);

	/*
	 * Send a reply only if one was actually expected.
	 */
	if ((handle->sh_flag & SVR_HANDLE_NO_REPLY) == 0) {

		/* Set up fields in the message header */
		iov = handle->sh_rep_msghdr_p->msg_iov;
		ihdr_p = (icshdr_t *)iov[0].iov_base;
		ihdr_p->ihdr_marker = ICSHDR_MARKER;
		ihdr_p->ihdr_inline_len = (void *)handle->sh_rep_inline_data_p -
						iov[0].iov_base - sizeof(icshdr_t);
#ifdef ICS_TCP_SENDPAGE
		if (handle->sh_rep_pagevec != NULL)
			ihdr_p->ihdr_len = (handle->sh_rep_page_len >> PAGE_CACHE_SHIFT);
		else
#endif
		ihdr_p->ihdr_len = 0;
		for (i = 0; i < handle->sh_rep_msghdr_p->msg_iovlen; i++)
			ihdr_p->ihdr_len += iov[i].iov_len;
		ihdr_p->ihdr_version = 1;
		ihdr_p->ihdr_service = handle->sh_service;
		ihdr_p->ihdr_flag = 0;
		ihdr_p->ihdr_procnum = 0;
		ihdr_p->ihdr_transid = handle->sh_transid;
		ihdr_p->ihdr_orignode = this_node;
#ifdef NSC_ICSLOG
		ihdr_p->ihdr_uniqueid = handle->sh_uniqueid;
#endif

		/*
		 * Reply channel determined by whether original message
		 * was a priority message or not.
		 */
		if (handle->sh_chan >= ics_min_prio_chan &&
		    handle->sh_chan <= ics_max_prio_chan)
			ihdr_p->ihdr_chan = ics_reply_prio_chan;
		else
			ihdr_p->ihdr_chan = ics_reply_chan;

		/* Log the message. */
		ICSLOG(1, ICSLOG_SVRLLSEND, ihdr_p->ihdr_transid,
					    ihdr_p->ihdr_uniqueid,
					    handle->sh_node,
					    ihdr_p->ihdr_service,
					    ihdr_p->ihdr_chan,
					    ihdr_p->ihdr_procnum);

		/*
		 * Find the connection to the node. If the connection exists,
		 * then send the reply to it.
		 * NOTE: The connection must be established prior to this call.
		 */
		nodeinfo = ics_llnodeinfo[handle->sh_node];
		if (nodeinfo != NULL) {
			LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			SSI_ASSERT(nodeinfo->icsni_conn);
			conn = nodeinfo->icsni_conn[ihdr_p->ihdr_chan];
			if (conn != NULL)
				INCR_ATOMIC_INT(&conn->conn_ref_cnt);
			UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if (conn != NULL) {
				LOCK_LOCK(&conn->conn_lock);
				set_fs(KERNEL_DS);
#ifdef ICS_TCP_SENDPAGE
				error = ics_llsendmsg(conn,
						      handle->sh_rep_msghdr_p,
						      ihdr_p->ihdr_len,
						      handle->sh_rep_pagevec,
						      handle->sh_rep_page_len);
#else
				error = ics_llsendmsg(conn,
						      handle->sh_rep_msghdr_p,
						      ihdr_p->ihdr_len);
#endif
				set_fs(oldfs);
				UNLOCK_LOCK(&conn->conn_lock);
				DECR_ATOMIC_INT(&conn->conn_ref_cnt);
			}
		}

		/*
		 * Free inline data by setting inline_data pointer to NULL.
		 */
		handle->sh_rep_inline_data_p = NULL;

		/*
		 * Do the freebuf callback for OOL data.
		 */
		for (i = 0; i < handle->sh_rep_ooldesc_cnt; i++) {
			register ooldesc_t *ool_p;

			ool_p = &handle->sh_rep_ooldesc[i];
			(ool_p->ool_callback_data_t)(ool_p->ool_callparam,
						     ool_p->ool_callarg);
			/*
			 * The sh_ref_cnt was incremented for each OOL segment
			 * that was encoded with a callback in
			 * icssvr_llencode_ool_data_t(). Now decrement the
			 * count for each encoding.
			 */
			handle->sh_ref_cnt--;
		}

		/*
		 * If we didn't manage to send the reply, clean up after
		 * ourselves.
		 */
		if (nodeinfo == NULL || conn == NULL || error < 0) {
#ifdef DEBUG
			printk(KERN_WARNING "icssvr_llreply: Error! Reply not sent!\n");
#endif /* DEBUG */
			SSI_ASSERT(handle->sh_ref_cnt > 0);
			handle->sh_ref_cnt--;		/* No need to lock */
			if (handle->sh_ref_cnt == 0)
				icssvr_sendup_replydone(handle, -EREMOTE);
			goto out;
		}

		/*
		 * If either there was no OOL data (with callbacks) or the
		 * data with callbacks has already been freed, then
		 * inform the high-level ICS code that we are done.
		 */
		LOCK_SPIN_LOCK(&handle->sh_lllock);
		SSI_ASSERT(handle->sh_ref_cnt > 0);
		handle->sh_ref_cnt--;
		immediate_reply = (handle->sh_ref_cnt == 0);
		UNLOCK_SPIN_LOCK(&handle->sh_lllock);
		if (immediate_reply)
			icssvr_sendup_replydone(handle, 0);
	} else {
		/*
		 * No reply to be sent. Inform the high-level ICS code that
		 * we are done.
		 */
		icssvr_sendup_replydone(handle, 0);
	}

out:
	return;
}

/*
 * icssvr_llhandle_deinit()
 *	Deinitialize a server handle, since we have completed our operation.
 *
 * Description:
 *	This routine is called by ICS high-level code to deinitialize the
 *	low-level portion of a server handle. No subsequent icssvr_ll*()
 *	routines may be called with this handle.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle to be deinitialized
 */
void
icssvr_llhandle_deinit(
	svr_handle_t	*handle)
{
	/* Deinitialize any handle locks. */
	DEINIT_SPIN_LOCK(&handle->sh_lllock);

	/*
	 * Free outgoing msghdr and iovecs.
	 * We only free the inline data. It is the responsibility
	 * of the caller to free the OOL data.
	 */
	if (handle->sh_rep_msghdr_p->msg_iov != handle->sh_iovec)
		kfree(handle->sh_rep_msghdr_p->msg_iov);
	if (handle->sh_rep_ooldesc != handle->sh_ooldesc)
		kfree(handle->sh_rep_ooldesc);

	/* Free incoming sk_buffs. */
	ics_llfreemsg(&handle->sh_msg_skb_p, handle->sh_msg_tskb_p);
}

#if defined(DEBUG) || defined(DEBUG_TOOLS)
void
print_icssvr_llhandle(svr_llhandle_t *generic)
{
	tcp_svr_llhandle_t *svr = &generic->tcpsvr_llhandle;

	printk(KERN_DEBUG "_sh_msg_skb_p 0x%p _sh_msg_tskb_p 0x%p\n",
			svr->_sh_msg_skb_p, svr->_sh_msg_tskb_p);

	printk(KERN_DEBUG
	       "_sh_msg_inline_skb_p 0x%p\t_sh_msg_inline_data_p 0x%p\n",
	       svr->_sh_msg_inline_skb_p,
	       svr->_sh_msg_inline_data_p);
	printk(KERN_DEBUG
	       "_sh_msg_ool_skb_p 0x%p\t_sh_msg_ool_data_p 0x%p\n",
	       svr->_sh_msg_ool_skb_p,
	       svr->_sh_msg_ool_data_p);
	printk(KERN_DEBUG "_sh_rep_msghdr_p 0x%p\n",
	       svr->_sh_rep_msghdr_p);
	printk(KERN_DEBUG
	       "_sh_rep_inline_iov_p 0x%p\t_sh_rep_inline_data_p 0x%p\n",
	       svr->_sh_rep_inline_iov_p,
	       svr->_sh_rep_inline_data_p);
	printk(KERN_DEBUG "_sh_rep_ooldesc_cnt %d\t_sh_ref_cnt %d\n",
			svr->_sh_rep_ooldesc_cnt, svr->_sh_ref_cnt);
}

#endif /* DEBUG || DEBUG_TOOLS */
