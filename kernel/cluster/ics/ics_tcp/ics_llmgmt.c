/*
 * 	Low-level ICS management code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * This file contains the code for managing the ICS low-level module. It
 * does all the initialization and deinitialization of low-level ICS code,
 * and does all the node-up/node-down notifications.
 */

#include <linux/net.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/tcp.h>
#include <linux/inet.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/reboot.h>
#include <linux/sched.h>
#include <linux/random.h>
#include <net/sock.h>
#include <asm/uaccess.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */
#include <cluster/clms.h>
#include <cluster/node_monitor.h>

static void ics_chan_listen(void *);
static void ics_probe_clms_daemon(void *);
static void ics_accept_connection(void *);
static int ics_request_connection(clusternode_t, int);
static int ics_llnodeup_connect(clusternode_t);
static void ics_llunack_init(void);
static void ics_llunack_daemon(void *);
static int ics_connect(struct socket *, clusternode_t, int);
static int ics_llwaitforallconnections(clusternode_t);
static void ics_llnodeinfo_init(clusternode_t);
#ifdef ICS_TCP_SENDPAGE
static int __ics_llsendmsg(ics_sock_conn_data_t *, struct msghdr *, int);
#endif

ics_llnodeinfo_t	*ics_llnodeinfo[NSC_MAX_NODE_VALUE + 1];
clihand_list_t		clihand_list[CLIHAND_ARRAYSZ];
int			ics_service_ports[ICS_MAX_LL_CHANNELS];
int			ics_client_ports[ICS_MAX_LL_CHANNELS];
char			ics_ifdevname[ICS_MAX_IFNAME_SIZE];
char			ics_ifname[ICS_MAX_IFNAME_SIZE];
char			ics_netaddr[ICS_IPADDRLEN];
char			ics_netmask[ICS_IPADDRLEN];
LOCK_T			ics_llconn_lock;
SPIN_LOCK_T 		ics_llnodeinfo_lock __cacheline_aligned_in_smp;
icsservice_t		icsservice[ICS_MAX_CHANNELS];
svrhand_list_t 		svrhand_list[SVRHAND_ARRAYSZ];
COND_LOCK_T 		ics_nodestatus_lock __cacheline_aligned_in_smp;
CONDITION_T		ics_nodestatus_cond;

extern ics_chan_t ics_probe_clms_chan;

extern int ics_lltransport_config_done;

#ifdef NOTUSED
/*
 * Nodedown daemon list.
 */
SPIN_LOCK_T	ics_llnodedown_list_lock;
EVENT_T		ics_llnodedown_event;
#endif

/*
 * The number of times to retry sending a SYN packet during establishment
 * of a probe TCP connection. The default in the kernel is set to 5.
 */
int		probe_syn_retries= 1;

/*
 * Seconds to wait before retrying connect.  This also equals tcp
 * max segment life time.
 */
#define ICS_CONNECT_DELAY 120

static struct socket *uics_sock;	/* socket for use by ics_llunack_send */

/*
 * ics_llinit()
 *	Initialize low-level ICS.
 *
 * Description:
 *	The low-level NSC ICS subsystem is initialized here. Called from
 *	the high-level ICS initialization routine.
 *
 *	This routine must be invoked from thread context.
 *
 * Parameters:
 *	None.
 */
void
ics_llinit()
{
	int	chan;
	int	port;
	int	error;

	/* Initialize locks and events. */
	INIT_SPIN_LOCK(&ics_llnodeinfo_lock);
#ifdef NOTUSED
	INIT_SPIN_LOCK(&ics_llnodedown_list_lock);
	INIT_EVENT(&ics_llnodedown_event);
#endif
	INIT_LOCK(&ics_llconn_lock);

	/*
	 * Perform the socket setup necessary for ICS communications.
	 * This function also establishes the current node's IP address and
	 * performs other setup.
	 */
	ics_lltransport_config();

	/*
	 * Assign TCP port numbers for each NSC Service known.
	 * Make sure port numbers are unique globally. Check relevant RFC.
	 */
	for (chan=0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		port = ICS_SVC_PORT + chan;
		if (port >= PPP_PORT)
			port++;
		ics_service_ports[chan] = port;
		port = ICS_CLI_PORT + chan;
		ics_client_ports[chan] = port;
	}

	/*
	 * Perform setup for the array with the IP addresses of all other
	 * nodes. Initial entries in the array are the current node and the
	 * cluster master node.
	 */
	error = ics_seticsinfo(this_node, (icsinfo_t *)ics_netaddr);
	if (error != 0) {
		panic("ics_llinit: ics_seticsinfo: error %d", error);
	}

	/*
	 * Set up the unacknowledged ICS service.
	 */
	ics_llunack_init();
}

/*
 * ics_llprobe_clms()
 *	Establish a connection for a given ICS comm channel to a given node.
 *
 * Description:
 *	A TCP connection is established for a given communication channel
 *	to the given remote node.
 *
 *	On each connection, TCP_NODELAY is set. This disables the Nagle
 *	algorithm (refer to RFC 793/1122). Generally TCP collects packets
 *	until the packet is big enough to send. This is to avoid sily
 *	window syndrome, which can occur when the memory goes very low.
 *	However, in the case of NSC, each packet the upper layer sends
 *	is a datagram and it has to be sent to the other side immediately
 *	regardless of its size.  For example, a request to kill a remote
 *	process is ~200 bytes in size. It has to be sent to the other
 *	side immediately.
 *
 * Parameters:
 *	remote_node	Node number of the node to contact.  (Not used in
 *			the ics_tcp implementation.)
 *	remote_addr	Address of the node to contact as an icsinfo_t.  This
 *			is actually an IP address here.
 *	master_node	Receives the node number of the actual master node,
 *			if such is sent by the contacted node.
 *	master_addr	Receives the IP address (as an icsinfo_t) of the
 *			master node.
 */
int
ics_llprobe_clms(
	clusternode_t		remote_node,
	icsinfo_t		remote_addr,
	clusternode_t		*master_node,
	icsinfo_t		*master_addr,
	int			im_a_secondary,
	int			im_a_member)
{
	int			port;
	int			error;
	struct sockaddr_in	sin;
	struct msghdr		*msg = NULL;
	struct iovec		iov;
	int			len;
	ics_clmsprobe_req_t	*reqp;
	ics_clmsprobe_rep_t	*repp;
	struct socket		*sock;
	int			save_syn_retries;
	mm_segment_t		oldfs = get_fs();
	struct timeval		tv;
	extern int sysctl_tcp_syn_retries;	/* Default value for the
						   number of retries on an
						   initial TCP connect.  */

	if (!ics_lltransport_config_done) {
		return CLMS_PROBE_ERR_NOANSWER;
	}

	/*
	 * Save the TCP retry count for sending a SYN packet and replace it
	 * with our own (currently 1 retry).
	 */
	save_syn_retries = sysctl_tcp_syn_retries;
	sysctl_tcp_syn_retries = probe_syn_retries;

	port = ICS_PROBE_CLMS_PORT;

	error = sock_create_kern(PF_INET, SOCK_STREAM, IPPROTO_TCP, &sock);
	if (error < 0) {
		panic("ics_llprobe_clms:  sock_create failed, error %d!\n",
		      error);
	}

	set_fs(KERNEL_DS);
	error = 1;
	sock_setsockopt(sock,
			SOL_SOCKET,
			SO_REUSEADDR,
			(char *)&error,
			sizeof(int));
	set_fs(oldfs);

	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = in_aton((char *)&remote_addr);

#ifdef DEBUG
	printk(KERN_DEBUG "Probing node %d\n", (int) remote_node);
#endif
	/*
	 * Connect to the node. If this fails, return an error.
	 */
	error = sock->ops->connect(sock,
				   (struct sockaddr *)&sin,
				   sizeof(sin),
				   0);
	if (error < 0) {
#ifdef DEBUG
		printk(KERN_DEBUG "Connect to node %d got error %d\n",
			     (int) remote_node,
			     error);
#endif
		error = CLMS_PROBE_ERR_NOANSWER;
		goto out;
	}
	/*
	 * Disable Nagle algorithm; set a 10 second timeout fgor the receive.
	 */
	set_fs(KERNEL_DS);
	error = 1;
	sock->ops->setsockopt(sock, SOL_TCP, TCP_NODELAY, (char *)&error, sizeof(int));
	tv.tv_sec = 10;
	tv.tv_usec = 0;
	sock_setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (void *)&tv, sizeof(tv));
	set_fs(oldfs);

	msg = (struct msghdr *)kmalloc_nofail(sizeof(struct msghdr));
	SSI_ASSERT(msg != NULL);
	msg->msg_namelen = 0;
	msg->msg_iov = &iov;
	msg->msg_iovlen	= 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = 0;
	reqp = msg->msg_iov->iov_base = kmalloc_nofail(sizeof(*reqp));
	msg->msg_iov->iov_len = sizeof(*reqp);
	reqp->my_node = this_node;
	memset(&reqp->my_icsinfo, 0, sizeof(reqp->my_icsinfo));
	ics_llnode_to_icsinfo(&reqp->my_icsinfo, this_node);
	reqp->imasecondary = im_a_secondary;
	reqp->imamember = im_a_member;

	SSI_ASSERT(sock->sk != NULL);
	set_fs(KERNEL_DS);
	error = sock_sendmsg(sock, msg, sizeof(*reqp));
	set_fs(oldfs);
	kfree(msg->msg_iov->iov_base);
	if (error < 0) {
		printk(KERN_WARNING "ics_llprobe_clms:  Send failed!  Error %d.",
				error);
		error = CLMS_PROBE_ERR_NOANSWER;
		goto out;
	}
	/*
	 * Get the node number and IP address of the master from the target.
         */
	repp = msg->msg_iov->iov_base = kmalloc_nofail(sizeof(*repp));
	msg->msg_iov->iov_len = sizeof(*repp);
	msg->msg_flags = MSG_WAITALL;

	set_fs(KERNEL_DS);
	len = sock_recvmsg(sock,
			   msg,
			   sizeof(*repp),
			   MSG_WAITALL);
	set_fs(oldfs);
	if (len < 0) {
		printk(KERN_WARNING "ics_llprobe_clms: receive of master info failed, error = %d; possibly node went down", len);
		kfree(repp);
		error = CLMS_PROBE_ERR_NOANSWER;
		goto out;
	}

	/*
	 * Copy the node number.
	 */
	*master_node = repp->cp_master;
	memcpy(master_addr, &repp->cp_icsinfo, sizeof(*master_addr));
	error = repp->error;
	kfree(repp);
	/*
	 * Restore the real timeout for SYN response and return any error.
	 */
out:
	sysctl_tcp_syn_retries = save_syn_retries;
	if (msg != NULL)
		kfree(msg);
	sock->ops->shutdown(sock, SEND_SHUTDOWN);
	sock_release(sock);
	return(error);
}

/*
 * ics_llaccept_notifications()
 *	Inform low-level ICS code that nodeup/nodedown notifications are
 *	allowed.
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the ICS
 *	low-level code that it can call ics_nodeup_notification() and
 *	ics_nodedown_notification(). Prior to calling
 *	ics_accept_notifications(), the nodeup and nodedown notifications
 *	may not be done.
 *
 *	The low-level ICS code may finish up some initialization code
 *	at this time (i.e. the initialization code that allows for
 *	information about nodes coming up and going down).
 *
 *	This routine must be called from thread context.
 */
void
ics_llaccept_notifications(void)
{
	int		error;

	/*
 	 * Start listening for service connection requests.
	 */
	error = spawn_daemon_thread("ics_chan_listen",
				    ics_chan_listen,
				    (void *)ICS_SVC_PORT);
	if (error)
		panic("ics_llaccept_notifications: error spawning ics_chan_listen %d", error);

	/*
 	 * Start listening for CLMS probe requests.
	 */
	error = spawn_daemon_thread("ics_probe_clms_daemon",
				    ics_probe_clms_daemon,
				    (void *)ICS_PROBE_CLMS_PORT);
	if (error)
		panic("ics_llaccept_notifications: error spawning ics_probe_clms_daemon %d", error);
}

/*
 * ics_llnodeinfo_init()
 *	Allocate a ics_llnodeinfo_t structure if there isn't one there already.
 *
 * Description:
 *	This is an internal routine used by the low-level ICS code to
 *	allocate a ics_llnodeinfo_t structure for a new node (if one has not
 *	already been allocated.
 *
 *	Note that it is probably unsafe to EVER get rid of a ics_llinfo_t
 *	structure once it has been allocated.
 *
 * Parameters:
 *	node		the node that is coming up
 */
static void
ics_llnodeinfo_init(
	clusternode_t	 node)
{
	ics_llnodeinfo_t *nodeinfo;

	/* Allocate the overall structure if necessary. */
	if (ics_llnodeinfo[node] == NULL) {
		nodeinfo = kzmalloc_nofail(sizeof(*nodeinfo));
		SSI_ASSERT(nodeinfo != NULL);
		INIT_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		INIT_EVENT(&nodeinfo->icsni_event);
		INIT_EVENT(&nodeinfo->icsni_chan_event);
		LOCK_SPIN_LOCK(&ics_llnodeinfo_lock);
		if (ics_llnodeinfo[node] == NULL)  {
			ics_llnodeinfo[node] = nodeinfo;
			UNLOCK_SPIN_LOCK(&ics_llnodeinfo_lock);
		} else {
			UNLOCK_SPIN_LOCK(&ics_llnodeinfo_lock);
			DEINIT_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			DEINIT_EVENT(&nodeinfo->icsni_event);
			DEINIT_EVENT(&nodeinfo->icsni_chan_event);
			kfree(nodeinfo);
		}
	}
}

/*
 * ics_llseticsinfo()
 *	Store the ICS communication information about a node
 *
 * Description:
 *	This routine is called to store the information ICS needs to
 *	make a connection with another node in its local database. This
 *	routine is called by the CLMS when the CLMS (either master or
 *	client) receives information about a node.
 *
 *	This routine is also be used internally to store the ICS information
 *	about this node, the CLMS master node, and about nodes for which
 *	nodeup notifications have been generated.
 *
 *	Until this call is done for a node, ics_nodeup() cannot be called for
 *	a node, nor can CLMS communicate with this node.
 *
 *	For the socket-based implementation, the ICS information consists
 *	of the IP address of the node.
 *
 * Parameters:
 *	node		the node about which information is being stored
 *	icsinfo_p	the actual information being stored; treated as
 *			a character array
 */
int
ics_llseticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p)
{
	ics_llnodeinfo_t *nodeinfo;

	SSI_ASSERT(sizeof(icsinfo_t) >= ICS_IPADDRLEN);

	/* Allocate the overall structure if necessary. */
	ics_llnodeinfo_init(node);
	nodeinfo = ics_llnodeinfo[node];

	/* Make sure there isn't a string there already. */
	if (nodeinfo->icsni_node_addr != NULL)
		return(-EINVAL);

	/* Allocate space for the string. */
	nodeinfo->icsni_node_addr = kmalloc_nofail(ICS_IPADDRLEN);

	/* If the node went down, bail out. */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (nodeinfo->icsni_flag & ICSNI_GOING_DOWN) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		kfree(nodeinfo->icsni_node_addr);
		return(-EAGAIN);
	}
	nodeinfo->icsni_flag = ICSNI_COMING_UP;

	/* Copy the string (which is an IP address) into the allocated space. */
	memcpy(nodeinfo->icsni_node_addr, (void *)icsinfo_p, ICS_IPADDRLEN);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	return(0);
}

/*
 * ics_llgeticsinfo()
 *	Fetch the ICS communication information about a node
 *
 * Description:
 *	This routine is called to fetch the information ICS needs to
 *	make a connection with another node from its local database. This
 *	routine is called by the CLMS when the CLMS (either master or
 *	client) needs to send the ICS information to another node.
 *
 *	For the socket-based implementation, the ICS information consists
 *	of the IP address of the node.
 *
 * Parameters:
 *	node		the node about which information is being fetched
 *	icsinfo_p	the actual information that was stored; treated as
 *			a character array
 */
int
ics_llgeticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p)
{
	ics_llnodeinfo_t *nodeinfo;

	if (node == CLUSTERNODE_INVAL || node > NSC_MAX_NODE_VALUE)
		return -EINVAL;

	nodeinfo = ics_llnodeinfo[node];

	SSI_ASSERT(sizeof(icsinfo_t) >= ICS_IPADDRLEN);

	/* Make sure there is a string there already. */
	if (nodeinfo == NULL || nodeinfo->icsni_node_addr == NULL)
		return(-EINVAL);

	/* Copy the string to the output parameter. */
	memset((void *)icsinfo_p, 0, sizeof(icsinfo_t));
	memcpy((void *)icsinfo_p, nodeinfo->icsni_node_addr, ICS_IPADDRLEN);

	return(0);
}

/*
 * ics_llnodeup()
 *	Perform any initialization necessary for low-level ICS code
 *	when a new node joins the cluster.
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the ICS
 *	low-level code that it should allow communication by NSC services to
 *	a specific remote node.
 *
 *	Note that the CLMS service is allowed to communicate with other
 *	nodes even if ics_llnodeup() has not been called.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	rnode		node that just joined the system
 */
int
ics_llnodeup(
	clusternode_t	rnode)
{
	int		error = 0;
	int		heskey, imkey;
	ics_llnodeinfo_t        *nodeinfo;

	/*
	 * If this is us, then there is nothing to do. If we have a key
	 * service and the rnode does not, initiate connections. If both
	 * have key services or both don't, then the lower numbered node
	 * should initiate the connections.  If we're the CLMS master, we
	 * never initiate connections.
	 */
	if (this_node != clms_master_node && rnode != this_node) {
		heskey = clms_iskeynode(rnode);
		imkey = clms_iskeynode(this_node);
		if (rnode == clms_master_node ||
		    ((imkey && !heskey) ||
		     (this_node < rnode &&
		      ((imkey && heskey) || (!imkey && !heskey))))) {
			error = ics_llnodeup_connect(rnode);
		}
	}
	/*
	 * Wait for all connections with the node to be
	 * complete before proceeding.
	 */
	if (rnode != this_node && error == 0) {
		error = ics_llwaitforallconnections(rnode);
	}

	if (!error) {
		nodeinfo = ics_llnodeinfo[rnode];
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (nodeinfo->icsni_flag == ICSNI_COMING_UP)
			nodeinfo->icsni_flag = ICSNI_NODE_UP;
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	}
	return(error);
}

/*
 * ics_llnodedown()
 *	Perform all work necessary to clean up communications with a down
 *	node and do not permit further communication with the node.
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the ICS
 *	ICS low-level code that it should stop communication with another node,
 *	and cause all outstanding communications with the node to fail.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_llnodedown(
	clusternode_t	node)
{
	ics_llnodedown_async_start(node);
	ics_llnodedown_async_end(node);
}

/*
 * ics_llnodedown_async_start()
 *	Perform all work necessary to clean up communications with a down
 *	node and do not permit further communication with the node.
 *	This is the asynchronous version, in which the nodedown processing
 *	is performed in two steps.  This start routine sets the low-level
 *	ICS state of the down node to "going down" and does nodedown
 *	processing.  Later in the end routine, we set the low-level ICS
 *	state to fully "down".
 *
 * Description:
 *	This routine is called by ICS high-level code to inform the ICS
 *	ICS low-level code that it should stop communication with another node,
 *	and cause all outstanding communications with the node to fail.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_llnodedown_async_start(
	clusternode_t	node)
{
	ics_sock_conn_data_t 		*conn;
	int				chan;
	ics_llnodeinfo_t		*nodeinfo = ics_llnodeinfo[node];
	boolean_t			dowait;

	/*
	 * It is possible (but unlikely) that the nodeinfo structure
	 * isn't set up yet. If so, we are done.
	 */
	if (nodeinfo == NULL)
		return;

	/*
	 * If the node is already down, we have no work to do. This
	 * can happen if the node going down is the former CLMS master,
	 * which we marked down previously in rclms_query_cluster.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (nodeinfo->icsni_flag & ICSNI_NODE_DOWN) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return;
	}

	/*
	 * Mark the node as in the special state of going down, and wait for
	 * any long-term pending operations to complete (long-term pending
	 * operations increment the nodeinfo reference count and pay attention
	 * to the special state in icsni_flag; short-term pending operations
	 * simply lock the connection lock, and they are taken care of later
	 * when we shut down individual connections).
	 */
	SSI_ASSERT((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) == 0);
	nodeinfo->icsni_flag = (ICSNI_GOING_DOWN | ICSNI_NODE_DOWN);
	dowait = (nodeinfo->icsni_ref_cnt != 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowait)
		WAIT_EVENT(&nodeinfo->icsni_event);
	SSI_ASSERT(nodeinfo->icsni_ref_cnt == 0);

	/*
	 * Send a disconnect request and then close all the comm channels for
	 * the node that just went down.
	 */
	for (chan = 0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		if (chan == ics_probe_clms_chan)
			continue;

		/*
		 * Find the connection. If the connection hasn't been
		 * established yet, then go to the next connection. Zero out
		 * the connection pointer to make sure we never use it
		 * again
		 */
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (nodeinfo->icsni_conn == NULL ||
		    nodeinfo->icsni_conn[chan] == NULL) {
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			continue;
		}
		conn = nodeinfo->icsni_conn[chan];
 		nodeinfo->icsni_conn[chan] = NULL;
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

		/*
		 * Send the disconnect request.
		 */
		conn->conn_sock->ops->shutdown(conn->conn_sock, SEND_SHUTDOWN);

		/*
		 * Wakeup the throttled processes (clients) and
	 	 * wait for everyone to stop using the connection. XXX
		 */
		while (READ_ATOMIC_INT(&conn->conn_ref_cnt) != 0)
			nidelay(HZ/20);
		sock_release(conn->conn_sock);
		conn->conn_sock = NULL;
		rand_deinitialize_ics(conn->conn_snd_rand_statep);
		conn->conn_snd_rand_statep = NULL;
		rand_deinitialize_ics(conn->conn_rcv_rand_statep);
		conn->conn_rcv_rand_statep = NULL;
		DEINIT_LOCK(&conn->conn_lock);
		ics_llfreemsg(&conn->conn_rbuf.rb_hskbp,
			      conn->conn_rbuf.rb_tskbp);
		kfree(conn);
		icssvr_llnodedown(node, chan);
	}

	/*
	 * Ensure that ics_seticsinfo() is called again before we allow
	 * communication with this  node again.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (nodeinfo->icsni_node_addr != NULL) {
		kfree(nodeinfo->icsni_node_addr);
		nodeinfo->icsni_node_addr = NULL;
	}
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	/*
	 * Now that all the connections are torn down, call the low-level
	 * client code to clean up.
	 */
	icscli_llnodedown(node);
}

/*
 * ics_llnodedown_async_end()
 * 	Set the low-level ICS state of the down node to fully "down".
 * 	This is called after ics_llnodedown_async_start().
 *
 * Description:
 *	This routine is called by ICS high-level code to complete the
 *	low-level ICS nodedown processing by setting the low-level ICS
 *	state of the down node to fully "down".
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_llnodedown_async_end(clusternode_t node)
{
	ics_llnodeinfo_t		*nodeinfo = ics_llnodeinfo[node];

	if (nodeinfo == NULL)
		return;
	/*
	 * Turn off the special "going down" flag. This means that we
	 * can start accepting connections for this node once more.
	 *
	 * Note that we don't try to free the nodeinfo structure. Trying
	 * to do this opens up some difficult timing windows with respect
	 * to node shutdown and restart, and is not worth doing, since any
	 * node that went down will probably come back anyway, and there
	 * isn't that much memory involved.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	nodeinfo->icsni_flag = ICSNI_NODE_DOWN;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
}

/*
 * ics_llunack_open()
 *	Create an endpoint for the ICS unacknowledged service.
 *
 * Description:
 *	This function creates a UDP endpoint for the specified UDP port.
 *
 * Parameters:
 *	int port		UDP port for this endpoint
 *				in host byte order
 *
 * Returns:
 *	sock representing the endpoint
 */
static struct socket *
ics_llunack_open(
	int port)
{
	char *ipaddr;
	int error;
	struct socket *sock;
	struct sockaddr_in sin;

	ipaddr = ics_llnodeinfo[this_node]->icsni_node_addr;
	SSI_ASSERT(ipaddr != NULL);

	error = sock_create_kern(PF_INET, SOCK_DGRAM, IPPROTO_UDP, &sock);
	if (error < 0)
		panic("ics_llunack_open:  sock_create failed, error %d!\n",
		      error);

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = in_aton(ipaddr);
	sin.sin_port = htons(port);

	error = sock->ops->bind(sock, (struct sockaddr *)&sin, sizeof(sin));
	if (error < 0)
		panic("ics_llunack_open: bind error %d", error);
	return sock;
}

/*
 * ics_llunack_daemon()
 *	Daemon t process unacknowledged ICS requests.
 *
 * Description:
 *	This daemon waits for datagrams on a UDP port.  When one is
 *	received it is validated and processed.
 * Note:
 *	At present, the only supported request is ICS_SHOOT_NODE.
 */
static void
ics_llunack_daemon(
	void *argp)
{
	int len;
	struct socket *sock;
	uics_reqheader *hdr;
	icsinfo_t icsinfo;
	struct msghdr *msg;
	struct iovec iov;
	ics_shoot_node_t *req;
	void *base_addr;

	nsc_daemonize();
	(void)set_daemon_prio(MAX_RT_PRIO-3, SCHED_FIFO);

	/*
	 * Establish a UDP socket to send and receive unacknowledged
	 * requests.
	 */
	sock = ics_llunack_open(ICS_UNACK_SVR_PORT);
	SSI_ASSERT(sock);

	msg = (struct msghdr *)kmalloc_nofail(sizeof(struct msghdr));
	SSI_ASSERT(msg != NULL);
	msg->msg_name = NULL;
	msg->msg_namelen = 0;
	msg->msg_iov = &iov;
	msg->msg_iovlen	= 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = MSG_WAITALL;
	hdr = base_addr = kmalloc_nofail(sizeof(uics_reqheader) +
						ICS_UNACK_MAX_DGRAM_SIZE);
	SSI_ASSERT(hdr != NULL);

	for (;;) {
		allow_reschedule();

		msg->msg_iov->iov_base = base_addr;
		msg->msg_iov->iov_len = (__kernel_size_t)
			(sizeof(uics_reqheader) + ICS_UNACK_MAX_DGRAM_SIZE);

		/*
		 * Receive a datagram.
		 */
		len = sock_recvmsg(sock,
				   msg,
				   sizeof(uics_reqheader) +
				   ICS_UNACK_MAX_DGRAM_SIZE,
				   MSG_WAITALL);
		if (len < 0) {
			printk(KERN_WARNING
			       "ics_llunack_daemon: sock_recvmsg error %d",
			       -len);
			continue;
		}

		if (len < sizeof(uics_reqheader)) {
			printk(KERN_WARNING
			       "ics_llunack_daemon: received short message (%d)",
			       len);
			continue;
		}
		ics_geticsinfo(hdr->uicsh_sender, &icsinfo);

		/*
		 * If we're not part of a cluster (we're not the CLMS master,
		 * nor have we found one), let the guy with the higher version
		 * win.  We panic.
		 */
		if (hdr->uicsh_version.uicsh_ver > ics_message_version &&
		    clms_master_node == CLUSTERNODE_INVAL)
			panic("Shot by newer kernel.");
		if (hdr->uicsh_version.uicsh_ver != ics_message_version) {
			printk(KERN_WARNING
			       "ics_llunack_daemon: ignoring message from %s ver %d expected %d",
			       (char *) icsinfo.icsinfo_string,
			       hdr->uicsh_version.uicsh_ver,
			       ics_message_version);
			continue;
		}

		/* Process "shoot_node" packet. */
		if (hdr->uicsh_type & ICS_UNACK_SHOOT_NODE) {
			if (!clms_is_secondary_by_addr(&icsinfo)) {
				printk(KERN_WARNING
				      "ics_llunack_daemon: received request from unauthorized node (0x%lx)",
				      (unsigned long) &icsinfo);
				continue;
			}

			SSI_ASSERT(hdr + 1 != NULL);
			req = (ics_shoot_node_t *)(hdr + 1);

			printk(KERN_NOTICE
			       "Shot by %s (his ICS version is %x and he %s a cluster).",
			       icsinfo.icsinfo_string,
			       req->my_ics_version,
			       (req->i_am_cluster ?  "has" : "doesn't have"));
			machine_restart(NULL);
			/*NOTREACHED*/
		}
		/* Process "imalive" packet. */
		else if (hdr->uicsh_type & ICS_UNACK_IMALIVE) {
			nm_received_imalive(hdr->uicsh_sender);

#ifdef CONFIG_LDLVL
			/* Process loadinfo packet. */
			if (hdr->uicsh_type & ICS_UNACK_LOAD) {
				loadinfo_received((load_array_t *)(hdr + 1));
#ifdef SSI_SKIP
				load_array_t *loadptr;
				int	size;

				size = (len - sizeof(uics_reqheader));
				loadptr = (load_array_t *)kmalloc_nofail(size);
				memcpy(loadptr, (char *)(hdr + 1), size);
				loadinfo_received(loadptr);
				kfree(loadptr);
#endif
			}
#endif /* CONFIG_LDLVL */
		}
		else
			panic("ics_llunack_daemon: bad message type %d",
			      hdr->uicsh_type);
			/*NOTREACHED*/
	}
}

/*
 * ics_llunack_send()
 *	Send an unacknowledged ICS message.
 *
 * Description:
 *	This function sends a datagram to ICS in the specified node.
 *
 * Parameters:
 *	clusternode_t node	node to send request to
 *	u_int type		type of request
 *	void *message		buffer containing request message
 *	size_t lenght		length of message
 *
 * Returns:
 *	Unix errno or 0
 */
int
ics_llunack_send(
	clusternode_t node,
	u_int type,
	void *message,
	size_t length)
{
	uics_reqheader 	*hdr;
	int 		len;
	struct msghdr 	*msg;
	struct iovec 	iov;
	struct sockaddr_in *sin;
	mm_segment_t	oldfs = get_fs();


	SSI_ASSERT(uics_sock);
	if (ics_llnodeinfo[node] == NULL ||
	    ics_llnodeinfo[node]->icsni_node_addr == NULL)
		return -EREMOTE;

	sin = (struct sockaddr_in *)
			kmalloc(sizeof(struct sockaddr_in), GFP_KERNEL);
	if (sin == NULL)
		return -ENOMEM;

	sin->sin_family = AF_INET;
	sin->sin_port = htons(ICS_UNACK_SVR_PORT);
	sin->sin_addr.s_addr = in_aton(ics_llnodeinfo[node]->icsni_node_addr);

	hdr = kmalloc(sizeof(uics_reqheader) + length, GFP_KERNEL);
	if (hdr == NULL) {
		kfree(sin);
		return -ENOMEM;
	}
	hdr->uicsh_version.uicsh_ver_type = 15;
	hdr->uicsh_version.uicsh_ver = ics_message_version;
	hdr->uicsh_type = type;
	hdr->uicsh_procnum = 0;
	hdr->uicsh_service = 0;
	hdr->uicsh_transid = 0;
	hdr->uicsh_chan = 0;
	hdr->uicsh_sender = this_node;
	memcpy((char *)(hdr + 1), message, length);

	msg = (struct msghdr *)kmalloc(sizeof(struct msghdr), GFP_KERNEL);
	if (msg == NULL) {
		kfree(sin);
		kfree(hdr);
		return -ENOMEM;
	}
	msg->msg_name = sin;
	msg->msg_namelen = sizeof(struct sockaddr_in);
	msg->msg_iov = &iov;
	msg->msg_iovlen	= 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = MSG_WAITALL;
	msg->msg_iov->iov_base = (char *)hdr;
	msg->msg_iov->iov_len = (__kernel_size_t)sizeof(uics_reqheader) +
								length;
	SSI_ASSERT(uics_sock->sk != NULL);
	set_fs(KERNEL_DS);
	len = sock_sendmsg(uics_sock, msg, sizeof(uics_reqheader) + length);
	set_fs(oldfs);

	kfree(sin);
	kfree(hdr);
	kfree(msg);
	return(len < 0 ? len : 0);
}

/*
 * ics_llunack_init()
 *	Initialize the unacknowledged ICS service.
 *
 * Description:
 *	This function creates an endpoint to be used to send unacknowledged
 *	ICS requests (of which only ICS_SHOOT_NODE exists at present) and
 *	starts a daemon to service them.
 */
static void
ics_llunack_init(void)
{
	int error;

	/*
	 * Create the endpoint for sending unacknowledged requests.
	 */
	uics_sock = ics_llunack_open(ICS_UNACK_CLI_PORT);
	if (uics_sock == NULL)
		panic("ics_llunack_init: Unable to assign port");

	/*
	 * Set up the ICS unacknowledged services.
	 */
	error = spawn_daemon_thread("ics_llunack_daemon",
				    ics_llunack_daemon,
				    NULL);
	if (error != 0)
		panic("ics_llinit: ics_llunack_daemon create failed, error=%d",
		      error);
}

/*
 * ics_llisicsport()
 *	Check if the given port is a port assigned for an ics service.
 *
 * Description:
 *	This function checks to see if a TCP port is reserved for ICS use.
 *	If so, it returns a 1 otherwise it returns a 0.
 *	This function assumes that the port is in Network Byte Order.
 *	It converts it to Host Byte Order before comparing with the stored
 *	value.
 *
 * Parameters:
 *	int port	port number
 *
 * Returns:
 *	1 if the port is an ICS port, 0 otherwise.
 */
int
ics_llisicsport(
	unsigned short	port)
{
	int		chan;

	if (port == 0)
		return 0;

	port = ntohs(port);

	for (chan=0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		if (port == ics_service_ports[chan] ||
		    port == ics_client_ports[chan])
			return 1;
	}

	return 0;
}

/*
 * ics_llisicsunackport()
 *	Check if the given port is a port assigned for an unacknowledged
 *	ICS service (including Regroup).
 *
 * Description:
 *	This function checks to see if a UDP port is reserved for ICS
 *	or Regroup use.  If so, it returns a 1; otherwise it returns a 0.
 *	This function assumes that the port is in Network Byte Order.
 *	It converts it to Host Byte Order before comparing with the stored
 *	value.
 *
 * Parameters:
 *	int port	port number
 *
 * Returns:
 *	1 if the port is an ICS port, 0 otherwise.
 */
int
ics_llisicsunackport(
	unsigned short	port)
{
	unsigned short hport = ntohs(port);

	return (hport == ICS_UNACK_SVR_PORT || hport == ICS_UNACK_CLI_PORT);
}

/*
 * ics_llissvcenabled()
 *	Check to see if a particular service is enabled.
 *
 * Parameters:
 *	int svc			service number
 *	clusternode_t node	node number
 *
 * Returns:
 *	1 if the svc is enabled, 0 otherwise.
 */
int
ics_llissvcenabled(
	int svc,
	clusternode_t node)
{
	ics_llnodeinfo_t *nodeinfo = NULL;
	int chan = ICS_CHAN(svc);

	if (node >= 1 || node <= NSC_MAX_NODE_VALUE)
		nodeinfo = ics_llnodeinfo[node];
#ifdef DEBUG
	else
		SSI_ASSERT(node == CLUSTERNODE_INVAL);
#endif	/* #ifdef DEBUG */

	LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (nodeinfo != NULL && nodeinfo->icsni_conn != NULL &&
	    nodeinfo->icsni_conn[chan] != NULL) {
		UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return 1;
	}
	UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	return 0;
}

/*
 * ics_probe_clms_daemon()
 *	Listen for CLMS probes on the port ICS_PROBE_CLMS_PORT.
 *
 * Description:
 *	This function is called from spawn_daemon_thread function.
 *	Listens for connection requests from nodes that are probing
 *	to find the CLMS master.
 *
 * Parameters:
 *	port_argp		port to listen on.
 *
 * Return Values:
 *	None.
 */
static void
ics_probe_clms_daemon(
	void			*port_argp)
{
	long 			port;
	int			error;
	struct sockaddr_in	sin;
	struct msghdr		*msg;
	struct iovec		iov;
	clusternode_t		master;
	struct socket		*sock, *newsock;
	ics_clmsprobe_req_t	*reqp;
	ics_clmsprobe_rep_t	*repp;
	int 			len;
	mm_segment_t		oldfs = get_fs();
	char			*ipaddr;

	nsc_daemonize();

	ipaddr = ics_llnodeinfo[this_node]->icsni_node_addr;
	SSI_ASSERT(ipaddr != NULL);

	port = (long) port_argp;

	error = sock_create_kern(PF_INET, SOCK_STREAM, IPPROTO_TCP, &sock);
	if (error < 0)
		panic("ics_probe_clms_daemon:  sock_create failed, error %d!\n", error);
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = in_aton(ipaddr);
	sin.sin_port = htons(port);


	set_fs(KERNEL_DS);
	error = 1;
	sock_setsockopt(sock,
			SOL_SOCKET,
			SO_REUSEADDR,
			(char *)&error,
			sizeof(int));
	set_fs(oldfs);
	do {
		error = sock->ops->bind(sock,
					(struct sockaddr *)&sin,
					sizeof(sin));
		if (error < 0 && error != -EADDRINUSE)
			panic("ics_probe_clms_daemon:  bind error %d", error);
		if (error == -EADDRINUSE) {
#ifdef DEBUG
			printk(KERN_WARNING "ics_probe_clms_daemon: port %u already in use...retrying in 10 seconds\n", port);
#endif /* DEBUG */
			idelay(10*HZ);
		}
	} while (error < 0);

	for (;;) {
retry:
		error = sock->ops->listen(sock, NSC_MAX_NODE_VALUE);
		if (error < 0)
			panic("ics_probe_clms_daemon: listen error %d", error);

		newsock = sock_alloc();
		if (newsock == NULL)
			panic("ics_probe_clms_daemon: Can't allocate new socket!\n");
		newsock->type = sock->type;
		newsock->ops = sock->ops;
		error = sock->ops->accept(sock, newsock, 0);
		if (error < 0) {
			printk(KERN_WARNING "ics_probe_clms_daemon: accept error %d\n", error);
			sock_release(newsock);
			exit_daemon_thread();
		}

		/*
		 * Disable Nagle Algorithm
		 */
		set_fs(KERNEL_DS);
		error = 1;
		newsock->ops->setsockopt(newsock,
					 SOL_TCP,
					 TCP_NODELAY,
					 (char *)&error,
					 sizeof(int));
		set_fs(oldfs);
		/*
		 * Get the request buffer from the client.
		 */
		msg = (struct msghdr *)kmalloc_nofail(sizeof(struct msghdr));
		msg->msg_namelen = 0;
		msg->msg_iov = &iov;
		msg->msg_iovlen = 1;
		msg->msg_control = NULL;
		msg->msg_controllen = 0;
		msg->msg_flags = MSG_WAITALL;
		reqp = kmalloc_nofail(sizeof(*reqp));
		msg->msg_iov->iov_base = reqp;
		msg->msg_iov->iov_len = sizeof(*reqp);

		set_fs(KERNEL_DS);
		len = sock_recvmsg(newsock,
				   msg,
				   sizeof(*reqp),
				   MSG_WAITALL);
		set_fs(oldfs);
		if (len < 0) {
			printk(KERN_WARNING "ics_probe_clms_daemon: receive of request info failed, error = %d; possibly node went down\n", len);
			newsock->ops->shutdown(newsock, SEND_SHUTDOWN);
			sock_release(newsock);
			kfree(reqp);
			kfree(msg);
			goto retry;
		}
#ifdef DEBUG
		printk(KERN_DEBUG "ics_probe_clms_daemon: received probe request\n");
#endif /* DEBUG */
		master = CLUSTERNODE_INVAL;
		error = clms_get_probe_info(reqp->imasecondary,
					    reqp->imamember,
					    reqp->my_node,
					    &master);
		kfree(reqp);

		SSI_ASSERT(msg != NULL);
		msg->msg_flags = 0;
		repp = kmalloc_nofail(sizeof(*repp));
		msg->msg_iov->iov_base = repp;
		msg->msg_iov->iov_len = sizeof(*repp);

		repp->cp_master = master;
		memset(&repp->cp_icsinfo, 0, sizeof(repp->cp_icsinfo));
		if (master != CLUSTERNODE_INVAL)
			ics_llnode_to_icsinfo(&repp->cp_icsinfo, master);
		repp->error = error;
		SSI_ASSERT(newsock->sk != NULL);
		set_fs(KERNEL_DS);
		error = sock_sendmsg(newsock,
				     msg,
				     sizeof(*repp));
		set_fs(oldfs);
		if (error < 0)
			printk(KERN_WARNING "ics_probe_clms_daemon: Sending reply failed, error = %d", error);

		newsock->ops->shutdown(newsock, SEND_SHUTDOWN);
		sock_release(newsock);
		kfree(repp);
		kfree(msg);

		allow_reschedule();
	}
	/* NOT REACHED */
}

/*
 * Argument block for ics_accept_connection()
 */
struct _ics_rinfo {
	struct socket	*sock;		/* The communication endpoint */
	icsinfo_t 	icsinfo; 	/* The ip address of node */
};

/*
 * ics_chan_listen()
 *	Listen for connection requests on port ICS_SVC_PORT (900).
 *	Accept the connection requests and set up the connection.
 *
 * Description:
 *	This function is called from spawn_daemon_thread function.
 *	Listens for connection requests for a given communication channel
 *	and sets up the NSC ICS Session for the given node/channel.
 *
 *	The idea here is that channel connections are requested by
 *	lower numbered nodes and listened for by higher numbered
 *	nodes. (The exception is the CLMS channel, where the CLMS
 *	master node is always the listener for the CLMS channel and
 *	the REPLY channel).
 *
 *	Once a connection request comes in, it is accepted using tcp_accept().
 *	The new connection is set up which will be used by the channel.
 *	The original communication endpoint is used to listen for more
 *	connections.
 *
 *	On each connection, TCP_NODELAY is set. This disables the Nagle
 *	algorithm (refer to RFC 793/1122). Generally TCP collects packets
 *	until the packet is big enough to send. This is to avoid silly
 *	window syndrome, which can occur when the memory goes very low.
 *	However, in the case of NSC, each packet the upper layer sends
 *	is a datagram and it has to be sent to the other side immediately
 *	regardless of its size.  For example, a request to kill a remote
 *	process is ~200 bytes in size. It has to be sent to the other
 *	side immediately. This is done by setting the socket option
 *	TCP_NODELAY.
 *
 * Parameters:
 *	port_argp		port number to listen on.
 *
 * Return Values:
 *	None.
 */
static void
ics_chan_listen(
	void			*port_argp)
{
	char			*ipaddr;
	int			port;
	int			error;
	struct _ics_rinfo	*valptr = NULL;
	struct sockaddr_in	sin;
	struct socket		*sock, *newsock;
	mm_segment_t		oldfs = get_fs();

	nsc_daemonize();

	port = (int)((long)port_argp);	/* Odd cast to suppress warning. */

	ipaddr = ics_llnodeinfo[this_node]->icsni_node_addr;
	SSI_ASSERT(ipaddr != NULL);

	error = sock_create_kern(PF_INET, SOCK_STREAM, IPPROTO_TCP, &sock);
	if (error < 0)
		panic("ics_chan_listen: sock_create failed, error %d!\n", error);
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = in_aton(ipaddr);
	sin.sin_port = htons(port);

	set_fs(KERNEL_DS);
	error = 1;
	sock_setsockopt(sock,
			SOL_SOCKET,
			SO_REUSEADDR,
			(char *)&error,
			sizeof(int));
	set_fs(oldfs);
	do {
		error = sock->ops->bind(sock,
					(struct sockaddr *)&sin,
					sizeof(sin));
		if (error < 0 && error != -EADDRINUSE)
			panic("ics_chan_listen: bind error %d", error);
		if (error == -EADDRINUSE) {
			printk(KERN_WARNING "ics_chan_listen: port %d already in use...retrying in 10 seconds\n", port);
			idelay(10*HZ);
		}
	} while (error < 0);

	for (;;) {
retry:
		error = sock->ops->listen(sock, NSC_MAX_NODE_VALUE);
		if (error < 0)
			panic("ics_chan_listen: listen error %d", error);

		valptr = kzmalloc_nofail(sizeof(*valptr));
		SSI_ASSERT(valptr != NULL);

		newsock = sock_alloc();
		if (newsock == NULL)
			panic("ics_chan_listen:  Can't allocate new socket!");
		newsock->type = sock->type;
		newsock->ops = sock->ops;

		error = sock->ops->accept(sock, newsock, 0);
		if (error < 0) {
			printk(KERN_WARNING "ics_chan_listen: accept error %d\n", error);
			sock_release(newsock);
			kfree(valptr);
			goto retry;
		}

		/*
		 * Get the ip address of the connecting node.
		 * This information is stored by ICS.
		 */
		snprintf(valptr->icsinfo.icsinfo_string, ICS_IPADDRLEN,
			 "%u.%u.%u.%u", NIPQUAD(inet_sk(newsock->sk)->daddr));
#ifdef DEBUG
		printk(KERN_DEBUG "ics_chan_listen: connection request from %s\n", valptr->icsinfo.icsinfo_string);
#endif /* DEBUG */

		/*
		 * Disable Nagle Algorithm
		 */
		set_fs(KERNEL_DS);
		error = 1;
		newsock->ops->setsockopt(newsock,
					 SOL_TCP,
					 TCP_NODELAY,
					 (char *)&error,
					 sizeof(int));

		valptr->sock = newsock;
		smp_mb(); /* barrier ensures args visible to handler */
		error = spawn_daemon_thread("ics_accept_connection",
					    ics_accept_connection,
					    (void *)valptr);
		if (error)
			panic("ics_chan_listen: spawn_daemon_thread error %d", error);

		allow_reschedule();
	}
	/* NOT REACHED */
}

/*
 * ics_accept_connection()
 *	Accept a connection request from another node.
 *
 * Description:
 *	A connection request has been received from another node, and
 *	this routine is spawned as a daemon to actually accept and
 *	establish a connection.
 *
 *	The idea here is that service connections are requested by
 *	lower numbered nodes and listened for by higher numbered
 *	nodes. (The exception is the CLMS service, where the CLMS
 *	master node is always the listener for the CLMS service and
 *	the REPLY service).
 *
 *	This function receives the node number and channel from the client and
 *	then actually establishes the connection.
 *
 * Parameters:
 *	rinfo_argp	pointer to _ics_rinfo structure, with sock pointer
 *			and ip address of connecting node.
 */
static void
ics_accept_connection(
	void		*rinfo_argp)
{
#ifdef ICSNI_REF_CNT_RACE_FIX
	int		state = -1;
#else
	int		state;
#endif
	int		chan;
	clusternode_t	rnode;
	int		error;
	int		len;
	int		goingdown = 0;
	boolean_t	dowakeup;
#ifdef ICSNI_REF_CNT_RACE_FIX
	ics_llnodeinfo_t *nodeinfo = NULL;
#else
	ics_llnodeinfo_t *nodeinfo;
#endif
	icsinfo_t	*icsinfo;
	char		*ptr;
	struct iovec	iov;
	struct socket	*sock;
	struct msghdr	*msg;
	mm_segment_t	oldfs = get_fs();

	nsc_daemonize();

	sock = ((struct _ics_rinfo *)rinfo_argp)->sock;
	icsinfo = &((struct _ics_rinfo *)rinfo_argp)->icsinfo;

#ifdef DEBUG
	printk(KERN_DEBUG "ics_accept_connection: establishing connections with %s\n", icsinfo->icsinfo_string);
#endif /* DEBUG */
	msg = (struct msghdr *)kmalloc_nofail(sizeof(struct msghdr));
	SSI_ASSERT(msg != NULL);
	msg->msg_namelen = 0;
	msg->msg_iov = &iov;
	msg->msg_iovlen	= 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = MSG_WAITALL;
	ptr = msg->msg_iov->iov_base = kmalloc_nofail(sizeof(clusternode_t)
						      + sizeof(int));
	SSI_ASSERT(ptr != NULL);
	msg->msg_iov->iov_len = (__kernel_size_t)(sizeof(clusternode_t)+sizeof(int));

	/*
	 * Now get the node number and channel number from the client.
         */
	set_fs(KERNEL_DS);
	len = sock_recvmsg(sock,
			   msg,
			   sizeof(clusternode_t) + sizeof(int),
			   MSG_WAITALL);
	set_fs(oldfs);
	if (len < 0) {
		printk(KERN_WARNING
		       "%s: recv of node number failed, error = %d;"
		       " possibly node went down",
		       __FUNCTION__, -len);
		goto err;
	}

	/*
	 * Copy the node number.
	 */
	memcpy(&rnode, ptr, sizeof(clusternode_t));
	memcpy(&chan, ptr + sizeof(clusternode_t), sizeof(int));

	if (rnode <= 0 || rnode > NSC_MAX_NODE_VALUE ||
	    chan < 0 || chan >= ICS_MAX_CHANNELS) {
		printk(KERN_WARNING
		       "%s: Invalid node/channel received %u/%d",
		       __FUNCTION__, rnode, chan);
		goto err;
	}

	/*
	 * By default the node may join.
	 */
	state = 0;
	if (this_node == clms_master_node && chan == ics_clms_chan) {
		LOCK_COND_LOCK(&ics_nodestatus_lock);
		if (ics_nodestatus[rnode]
		    & (ICS_NODE_COMING_UP | ICS_NODE_UP | ICS_NODE_READY)) {
			UNLOCK_COND_LOCK(&ics_nodestatus_lock);
			state = 1;      /* The node already exists. */
		} else {
			int status;

			UNLOCK_COND_LOCK(&ics_nodestatus_lock);

			status = clms_get_node_status(rnode);
			if (!(status & (CLMS_NODE_COMING_UP | CLMS_NODE_DOWN |
					CLMS_NODE_NEVER_UP)))
				state = 2;      /* The node needs to wait. */
		}
	}

	/* Allocate the overall nodeinfo structure if necessary. */
	ics_llnodeinfo_init(rnode);
	nodeinfo = ics_llnodeinfo[rnode];
	SSI_ASSERT(nodeinfo != NULL);

	if (state == 0) {
		/*
		 * If the node isn't in the special going-down state, then up
		 * the reference count so that the node can't go down and
		 * connections can't be torn down until this routine spots it
		 * and exits.  If it _is_ in the going-down state, set the
		 * flag; we'll send the response to the client and exit.
		 */
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
			goingdown = 1;
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			goto reply;
		}
		/* Forget that we may have gone down before. */
		nodeinfo->icsni_flag = ICSNI_NODE_UP;
		nodeinfo->icsni_ref_cnt++;
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		/*
		 * The connection has been established from the listener's
		 * perspective.  Set up all the correct data structures.
		 */
		error = ics_connect(sock, rnode, chan);
		if (error != 0)
			goto reply;
		/*
		 * If this is the CLMS master and this is the CLMS channel
		 * then inform CLMS that the new node has established a
		 * connection with us.
		 */
		if (chan == ics_clms_chan && this_node == clms_master_node)
			ics_nodeup_notification(rnode, icsinfo);
	}
reply:
	/*
	 * Allocate and initialize the msghdr for the status response.
	 */

	msg->msg_iov->iov_base = ptr;
	msg->msg_iov->iov_len = (__kernel_size_t) sizeof(int);
	msg->msg_flags = 0;

	memcpy(ptr, &state, sizeof(int));

	SSI_ASSERT(sock->sk != NULL);
	set_fs(KERNEL_DS);
	len = sock_sendmsg(sock, msg, sizeof(int));
	set_fs(oldfs);
	if (len < 0) {
		printk(KERN_WARNING "ics_accept_connection:  Sending status response failed, %d.\n", len);
		goto err;
	}
	if (state == 1) {
		if (chan == ics_clms_chan)
			printk(KERN_WARNING "ics_accept_connection:"
				" Node %d has already joined.\n", (int) rnode);
		goto err;
	}
	if (state == 2) {
#ifdef DEBUG
		if (chan == ics_clms_chan) {
			printk(KERN_WARNING "ics_accept_connection:  Node %d joining before nodedown complete.\n", (int) rnode);
		}
#endif
		goto err;
	}

	/*
	 * If we set the goingdown flag above, just exit this thread here.
	 */
	if (goingdown)
		goto err;

#ifdef ICSNI_REF_CNT_RACE_FIX
done:
#endif
	/*
	 * Decrement the reference count now that we are done.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowakeup)
		SIGNAL_EVENT(&nodeinfo->icsni_event);
	goto out;

err:
	/*
	 * Thread no longer required...
	 */
	sock->ops->shutdown(sock, SEND_SHUTDOWN);
	sock_release(sock);
#ifdef ICSNI_REF_CNT_RACE_FIX
	if (state == 0 && !goingdown)
		goto done;
#endif
out:
	kfree(ptr);
	kfree(msg);
	kfree(rinfo_argp);
	exit_daemon_thread();
	/* NOTREACHED */
}

/*
 * ics_llnodeup_connect()
 *	Routine called to make connections when ICS is informed a new node
 *	has joined up (and this is the low-numbered node of the pair).
 *
 * Description:
 *	TCP connections are established for all services to a given
 *	node.
 *
 *	Note that this routine is called only if the node coming up has
 *	a higher node number than the current node. This is done so that
 *	connections are requested in one direction only.
 *
 * Parameters:
 *	rnode	remote node number.
 *
 * Return Value:
 *	0 - success
 *	-EAGAIN - the contacted node was still doing nodedown for us.
 */
static int
ics_llnodeup_connect(
	clusternode_t	rnode)
{
	int		chan;
	int		error = 0;
	ics_llnodeinfo_t *nodeinfo = ics_llnodeinfo[rnode];

	/*
	 * If the node is still going down, wait for it to go completely
	 * down before we process the nodeup.
	 */
	clms_waitfor_nodedown(rnode);
	for (chan=0; chan < ICS_LL_NUM_CHANNELS; chan++) {
		if (chan == ics_probe_clms_chan)
			continue;
		LOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (nodeinfo->icsni_conn == NULL ||
 		    nodeinfo->icsni_conn[chan] == NULL) {
			UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
#ifdef DEBUG
			printk(KERN_DEBUG "ics_llnodeup_connect: requesting connection on node %u, channel %d\n", rnode, chan);
#endif /* DEBUG */
			error = ics_request_connection(rnode, chan);
			if (error != 0)
				break;
		} else
			UNLOCK_SHR_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	}
	return(error);
}

/*
 * ics_request_connection()
 *	Establish a connection for a given ICS comm channel to a given node.
 *
 * Description:
 *	A TCP connection is established for a given communication channel
 *	to the given remote node.
 *
 *	On each connection, TCP_NODELAY is set. This disables the Nagle
 *	algorithm (refer to RFC 793/1122). Generally TCP collects packets
 *	until the packet is big enough to send. This is to avoid sily
 *	window syndrome, which can occur when the memory goes very low.
 *	However, in the case of NSC, each packet the upper layer sends
 *	is a datagram and it has to be sent to the other side immediately
 *	regardless of its size.  For example, a request to kill a remote
 *	process is ~200 bytes in size. It has to be sent to the other
 *	side immediately.
 *
 * Parameters:
 *	rnode	  remote node number.
 *	chan	  communication channel number.
 */
static int
ics_request_connection(
	clusternode_t		rnode,
	int			chan)
{
	int			port;
	char			*ipaddr;
	int			error;
	struct sockaddr_in	sin;
	boolean_t		immed_return;
	boolean_t		dowakeup;
	ics_llnodeinfo_t	*nodeinfo = ics_llnodeinfo[rnode];
	char			*ptr;
	int			len;
	struct socket		*sock;
	struct msghdr		*msg;
	struct iovec		iov;
	mm_segment_t		oldfs = get_fs();

	SSI_ASSERT(ics_llnodeinfo[rnode] != NULL);

	port = ICS_SVC_PORT;

	/*
	 * If the node isn't in the special going-down state, then up
	 * the reference count so that the node can't go down and connections
	 * can't be torn down until this routine spots that it happened and
	 * returns an error to the caller.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return(-EAGAIN);
	}
	nodeinfo->icsni_ref_cnt++;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	ipaddr = nodeinfo->icsni_node_addr;
	SSI_ASSERT(ipaddr != NULL);

	error = sock_create_kern(PF_INET, SOCK_STREAM, IPPROTO_TCP, &sock);
	if (error < 0) {
		panic("ics_request_connection:  sock_create failed, error %d!\n", error);
	}
	set_fs(KERNEL_DS);
	error = 1;
	sock_setsockopt(sock,
			SOL_SOCKET,
			SO_REUSEADDR,
			(char *)&error,
			sizeof(int));
	set_fs(oldfs);

tryagain:
	LOCK_LOCK(&ics_llconn_lock);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = in_aton(ipaddr);
	/*
	 * Connect to the node. If this fails due to the fact that the node
	 * is going down, make sure that we decrement the reference count
	 * appropriately.
	 */
	error = sock->ops->connect(sock,
				   (struct sockaddr *)&sin,
				   sizeof(sin),
				   0);
	if (error < 0) {
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
			dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if (dowakeup)
				SIGNAL_EVENT(&nodeinfo->icsni_event);
			error = -EAGAIN;
			immed_return = TRUE;
		} else {
			immed_return = FALSE;
#ifdef ICS_REQUEST_CONNECTION_TIMEOUT_MASTER
			/* We don't monitor master until received first imalive
			 * packet.  Master probably hung or rebooted.	-RT
			 */
			if (error == -ETIMEDOUT
			    && rnode == clms_master_node
			    && clms_get_node_status(rnode) < CLMS_NODE_ICS_UP)
				immed_return = TRUE;
#endif
#ifdef ICS_REQUEST_CONNECTION_ERESTARTSYS
			if (error == -ERESTARTSYS)
				immed_return = TRUE;
#endif
#if defined(ICS_REQUEST_CONNECTION_ERESTARTSYS) || defined(ICS_REQUEST_CONNECTION_TIMEOUT_MASTER)
			if (immed_return) {
				dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
				if (dowakeup)
					SIGNAL_EVENT(&nodeinfo->icsni_event);
				error = -EINVAL;
			}
#endif
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			printk(KERN_WARNING
				"ics_request_connection: "
				"connect failed, error = %d",
				error);
		}
		UNLOCK_LOCK(&ics_llconn_lock);
		if (immed_return)
			return(error);
		idelay(ICS_CONNECT_DELAY * HZ);
		goto tryagain;
	}
	UNLOCK_LOCK(&ics_llconn_lock);

	/*
	 * Disable Nagle algorithm.
	 */
	set_fs(KERNEL_DS);
	error = 1;
	sock->ops->setsockopt(sock,
			      SOL_TCP,
			      TCP_NODELAY,
			      (char *)&error,
			      sizeof(int));
	set_fs(oldfs);

	/*
	 * Send the node number across. This may not be needed
	 * in the future, if we avoid race conditions between
	 * connection establishment and the clms master announcement.
	 * NSC_XXX.
	 *
	 * Note that we need to decrement the reference count if the
	 * send fails due to the node going down.
	 */
	msg = (struct msghdr *)kmalloc_nofail(sizeof(struct msghdr));
	SSI_ASSERT(msg != NULL);
	msg->msg_namelen = 0;
	msg->msg_iov = &iov;
	msg->msg_iovlen	= 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = 0;
	ptr = msg->msg_iov->iov_base = kmalloc_nofail(sizeof(clusternode_t)
						      + sizeof(int));
	SSI_ASSERT(ptr != NULL);
	msg->msg_iov->iov_len = (__kernel_size_t)(sizeof(clusternode_t)+sizeof(int));
	memcpy(ptr, &this_node, sizeof(clusternode_t));
	memcpy(ptr+sizeof(clusternode_t), &chan, sizeof(int));

	SSI_ASSERT(sock->sk != NULL);
	set_fs(KERNEL_DS);
	error = sock_sendmsg(sock, msg, sizeof(clusternode_t)+sizeof(int));
	set_fs(oldfs);
	if (error < 0) {
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
			dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if (dowakeup)
				SIGNAL_EVENT(&nodeinfo->icsni_event);
			sock->ops->shutdown(sock, SEND_SHUTDOWN);
			sock_release(sock);
			kfree(ptr);
			kfree(msg);
			return(-EAGAIN);
		}
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		printk(KERN_WARNING "ics_request_connection: Sending node number and channel number failed, error = %d", error);
		sock->ops->shutdown(sock, SEND_SHUTDOWN);
		idelay(ICS_CONNECT_DELAY * HZ);
		kfree(ptr);
		kfree(msg);
		goto tryagain;
	}

	/*
	 * Get the response from the master.  This tells us if we can
	 * continue.  It's a four-byte (int) value, either zero (success),
	 * one (the node number already exists) or two (he's still doing
	 * nodedown for the node).
         */

	SSI_ASSERT(msg != NULL);
	msg->msg_flags = MSG_WAITALL;
	msg->msg_iov->iov_len = (__kernel_size_t)sizeof(int);

	set_fs(KERNEL_DS);
	len = sock_recvmsg(sock, msg, sizeof(int), MSG_WAITALL);
	set_fs(oldfs);
	if (len < 0) {
		printk(KERN_WARNING "ics_request_connection: recv of status failed, error = %d\n", len);
		sock->ops->shutdown(sock, SEND_SHUTDOWN);
		idelay(ICS_CONNECT_DELAY * HZ);
		kfree(ptr);
		kfree(msg);
		goto tryagain;
	}

	memcpy(&error, ptr, sizeof(int));
	if (error == 1) {	/* Our node number is already taken. */
		sock->ops->shutdown(sock, SEND_SHUTDOWN);
		idelay(10*HZ);	/* Delay for a while to let things settle. */
		panic("Node number %d already taken!\n", (int) this_node);
	}
	if (error == 2) {	/* He's still in nodedown for this node. */
#ifdef DEBUG
		printk(KERN_WARNING "ics_request_connection:  We're joining before our nodedown is complete.\n");
#endif
		sock->ops->shutdown(sock, SEND_SHUTDOWN);
		kfree(ptr);
		kfree(msg);
		sock_release(sock);
		/*
		 * Drop the ref count we put on earlier.
		 */
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (dowakeup)
			SIGNAL_EVENT(&nodeinfo->icsni_event);
		return(-EAGAIN);
	}

	kfree(ptr);
	kfree(msg);

	/*
	 * Connection established from initiator's side. Set up the
	 * correct data structures.
	 */
	ics_connect(sock, rnode, chan);

	/*
	 * Decrement the reference count now that we are done.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowakeup)
		SIGNAL_EVENT(&nodeinfo->icsni_event);
	return(0);
}

/*
 * ics_connect()
 *	Set up the data structures for a particular node/channel connection.
 *
 * Description:
 *	This routine is called by either ics_accept_connection() or
 *	ics_request_connection() once a connection has been established.
 *
 *	It sets up the data structures necessary for a connection and
 *	performs other configuration as necessary.
 *
 * 	Once the connection has been set up, the socket's data_ready
 * 	callback routine is replaced by the NSC ICS specific callback
 * 	"ics_dgram". This is so that NSC datagrams can be reconstructed
 * 	efficiently. Note that the ICS server daemons do not call the
 * 	conventional sock_recvmsg() to receive incoming ICS datagrams.
 *
 * Parameters:
 *	sock		pointer to socket
 *	rnode		node number of the remote node.
 *	channel		communication channel number.
 *
 * Return Values:
 *	0 on success and an error number on failure.
 *
 */
static int
ics_connect(
	struct socket	*sock,
	clusternode_t	rnode,
	int		chan)
{
	ics_sock_conn_data_t *conn;
	ics_sock_conn_data_t **connp;
	int		error;
	ics_llnodeinfo_t *nodeinfo = ics_llnodeinfo[rnode];
	extern		void ics_dgram(struct sock *, int);

	error = 0;
	conn = kzmalloc_nofail(sizeof(ics_sock_conn_data_t));
	SSI_ASSERT(conn != NULL);

	conn->conn_node = rnode;
	conn->conn_chan = chan;
	conn->conn_sock = sock;
	conn->conn_snd_rand_statep = rand_initialize_ics(GFP_KERNEL);
	conn->conn_rcv_rand_statep = rand_initialize_ics(GFP_KERNEL);
	INIT_ATOMIC_INT(&conn->conn_ref_cnt, 0); /* count of number of */
					         /* processes throttled */
	INIT_LOCK(&conn->conn_lock);
	/*
	 * Set up our "data ready" callback.
	 */
	sock->sk->sk_data_ready = &ics_dgram;
	sock->sk->sk_user_data = (void *)conn;

	/*
	 * Allocate space for the connection structures if necessary.
	 */
	SSI_ASSERT(nodeinfo != NULL);
	if (nodeinfo->icsni_conn == NULL) {
		connp = kzmalloc_nofail(sizeof(connp) * ICS_LL_NUM_CHANNELS);
		SSI_ASSERT(connp != NULL);
		LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		if (nodeinfo->icsni_conn == NULL)  {
			nodeinfo->icsni_conn = connp;
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		} else {
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			kfree(connp);
		}
	}

#ifdef CFS_ALLOW_NAGLE
	/*
	 * Enable Nagle algorithm.
	 */
	/* SSI: channel for CFS ? */
	if (chan == 10) { /* Yes */
		mm_segment_t oldfs = get_fs();
		int val = 0;

		set_fs(KERNEL_DS);
		sock->ops->setsockopt(sock,
				      SOL_TCP,
				      TCP_NODELAY,
				      (char *)&val,
				      sizeof(int));
		set_fs(oldfs);
	}
#endif

	/*
	 * Set up the connection, and mark the node as coming up.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
 	nodeinfo->icsni_conn[chan] = conn;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
#ifdef LINUX_SSI_EVENT
	SIGNAL_EVENT(&nodeinfo->icsni_chan_event);
#else
	BROADCAST_EVENT(&nodeinfo->icsni_chan_event);
#endif

	return error;
}

/*
 * ics_llwaitforallconnections()
 *	Wait for connections to be established for ALL services to a node.
 *
 * Description:
 *	This function waits for connections to be established
 *	for all services to a given node.  This is not very
 *	efficient, if there are problems in establishing connections.
 *	Also, this function will have to change when node down conditions
 *	are handled (NSC_XXX).
 *
 * Parameters:
 *	rnode		remote node number.
 *
 * Return value:
 *	Zero if all connections successfully established, -EAGAIN if the
 *	node went down before connections could be established.
 */
static int
ics_llwaitforallconnections(
	clusternode_t	rnode)
{
#ifdef ICS_LLWAITFORALLCONNECTIONS_LIVELOCK_FIX
	unsigned long	start_time = jiffies;
#else
	boolean_t	allchans;
#endif
	int		chan;
	boolean_t	dowakeup;
	ics_llnodeinfo_t *nodeinfo = ics_llnodeinfo[rnode];

	/*
	 * If the node isn't in the special going-down state, then up
	 * the reference count so that the node can't go down and connections
	 * can't be torn down until this routine spots that it happened and
	 * returns an error to the caller.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
		UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		return(-EAGAIN);
	}
	nodeinfo->icsni_ref_cnt++;
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);

	/*
	 * Go through all the connections looking for them all to be
	 * set up. If we find one that is not set up, we check if the
	 * node is going down. If it is not going down, we then wait
	 * a while and then retry the loop from the top.
	 */
	do {
#ifndef ICS_LLWAITFORALLCONNECTIONS_LIVELOCK_FIX
		allchans = TRUE;
#endif
		for (chan = 0; chan < ICS_LL_NUM_CHANNELS; chan++) {
			if (chan == ics_probe_clms_chan)
				continue;
#ifdef ICS_LLWAITFORALLCONNECTIONS_LIVELOCK_FIX
			LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
				dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
				UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
				if (dowakeup)
					SIGNAL_EVENT(&nodeinfo->icsni_event);
				return(-EAGAIN);
			}
 			if (nodeinfo->icsni_conn == NULL ||
			    nodeinfo->icsni_conn[chan] == NULL) {
				UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
				idelay(HZ/100);
				break;
			}
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
		}
		if (chan == ICS_LL_NUM_CHANNELS)
			break;
		/* CI_XXX: Livelock when ICS msg's to new CLMS master are
		 * rejected due to wrong transition ID.  Happens if CLMS master
		 * still failing over during clms_client_declare_master_up().
		 *
		 * CI_XXX: We don't monitor master until received first imalive packet.
		 * 	-Roger
		 */
		if (time_after(jiffies, start_time+(2*ICS_CONNECT_DELAY*HZ))) {
			LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
			UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
			if (dowakeup)
				SIGNAL_EVENT(&nodeinfo->icsni_event);
			return(-EAGAIN); /* do timeout nodedown CLMS master */
		}
	} while (1);
#else
			if ((nodeinfo->icsni_flag & ICSNI_GOING_DOWN) != 0) {
				LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
				dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
				UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
				if (dowakeup)
					SIGNAL_EVENT(&nodeinfo->icsni_event);
				return(-EAGAIN);
			}
 			if (nodeinfo->icsni_conn == NULL ||
			    nodeinfo->icsni_conn[chan] == NULL) {
				allchans = FALSE;
				idelay(HZ/100);
				break;
			}
		}
	} while (!allchans);
#endif /* !ICS_LLWAITFORALLCONNECTIONS_LIVELOCK_FIX */

	/*
	 * Decrement the reference count now that we are done.
	 */
	LOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	dowakeup = (--nodeinfo->icsni_ref_cnt == 0);
	UNLOCK_EXCL_RW_SPIN_LOCK(&nodeinfo->icsni_lock);
	if (dowakeup)
		SIGNAL_EVENT(&nodeinfo->icsni_event);

	return(0);
}

int
ics_llshoot_node(clusternode_t	remote_node)
{
	ics_shoot_node_t shoot_node;
	int error;

	shoot_node.my_ics_version = ics_message_version;
	shoot_node.i_am_cluster = (clms_master_node != CLUSTERNODE_INVAL);
	do {
		error = ics_llunack_send(remote_node, ICS_UNACK_SHOOT_NODE,
					 &shoot_node, sizeof(shoot_node));
		if (error == -ENOMEM) {
			idelay(HZ/2);
		}
	} while (error == -ENOMEM);
	if (error != 0)
		printk(KERN_WARNING
			"ics_llshoot_node: error %d in ics_llunack_send",
			error);
	return(B_TRUE);
}

int
ics_llget_nodenumsize(void)
{
	return ICS_NODENUMSIZE;
}

int
ics_llget_addrlen(void)
{
	return ICS_IPADDRLEN;
}

void
ics_llnode_to_icsinfo(icsinfo_t *infop, clusternode_t node)
{
	(void) ics_llgeticsinfo(node, infop);
}

#ifdef ICS_TCP_SENDPAGE
static int
ics_llsendpages(
	ics_sock_conn_data_t *connp,
	struct page **ppage,
	unsigned pglen,
	unsigned base,
	int msg_flags)
{
	ssize_t (*sendpage)(struct socket *, struct page *, int, size_t, int);
	struct task_struct *t = current;
	unsigned int len, size = pglen;
	int err, retval = 0;
	int interruptible = 1;
	sigset_t saved_sigset;

#ifdef CFS_PAGES_CACHE
	/* On server-side the pagevec cache is recycled immediately */
#ifdef CFS_PAGEVEC_SENDFILE
	extern int cfs_pagevec_isvalid(struct page **);

	if (cfs_pagevec_isvalid(ppage))
#else
	if (ssi_isremote())
#endif
		sendpage = sock_no_sendpage;
	else
#endif
	sendpage = connp->conn_sock->ops->sendpage ? : sock_no_sendpage;

	/* Adaptation of 2.6.11 net/sunrpc/xdr.c:xdr_sendpages() */
	do {
		int flags = msg_flags;

		len = PAGE_CACHE_SIZE;
		if (base)
			len -= base;
		if (pglen < len)
			len = pglen;

		if (pglen != len)
			flags |= MSG_MORE;

		/* Hmm... We might be dealing with highmem pages */
		if (PageHighMem(*ppage))
			sendpage = sock_no_sendpage;
retry:
		err = sendpage(connp->conn_sock, *ppage, base, len, flags);
		if (retval == 0)
			retval = err;
		else if (err > 0)
			retval += err;

		if (err < 0) {
			if (interruptible) {
				if (err != -ERESTARTSYS && err != -EINTR)
					break;
				ssi_block_signals(t, &saved_sigset);
				interruptible = 0;
			} else if (err != -ERESTARTSYS)
				break;
			goto retry;
		} else if (err != len) {
			if (interruptible) {
				ssi_block_signals(t, &saved_sigset);
				interruptible = 0;
			} else if (err == 0) {
				printk(KERN_WARNING
					"%s: failed to send data, retrying\n",
					__FUNCTION__);
			}
			idelay(HZ/4);
			len -= err;
			base += err;
			goto retry;
		}
		base = 0;
		ppage++;
	} while ((pglen -= len) != 0);

	if (!interruptible)
		ssi_unblock_signals(t, &saved_sigset);

	if (err < 0 || retval != size) {
		if ((ics_llnodeinfo[connp->conn_node]->icsni_flag &
		     (ICSNI_GOING_DOWN | ICSNI_NODE_DOWN)) == 0) {
			if (err < 0)
				printk(KERN_WARNING
				       "%s: node %u chan %d, error %d\n",
				       __FUNCTION__,  connp->conn_node,
				       connp->conn_chan, -err);
			else
				printk(KERN_WARNING
				       "%s: node %u chan %d,"
				       " sent %d/%d bytes, error %d\n",
				       __FUNCTION__, connp->conn_node,
				       connp->conn_chan, retval, size,
				       -err);
		}
	}

	return retval;
}

int
ics_llsendmsg(
	ics_sock_conn_data_t *connp,
	struct msghdr *msgp,
	int size,
	struct page **pagevec,
	unsigned page_len)
{
	unsigned pglen, base;
	int retval;

	if (!pagevec)
		return __ics_llsendmsg(connp, msgp, size);

	pglen = (page_len >> PAGE_CACHE_SHIFT);
	size -= pglen;
	BUG_ON(!pglen);

#ifdef DEBUG
	printk(KERN_DEBUG "%s: pid %d sendmsg %d bytes\n",
			__FUNCTION__, current->pid, size);
#endif

	msgp->msg_flags |= MSG_MORE;
	retval = __ics_llsendmsg(connp, msgp, size);
	msgp->msg_flags &= ~MSG_MORE;
	if (retval < 0)
		return retval;

	base = (page_len & ~PAGE_CACHE_MASK);

#ifdef DEBUG
	printk(KERN_DEBUG "%s: pid %d sendpages %d bytes\n",
			__FUNCTION__, current->pid, pglen);
#endif

	return ics_llsendpages(connp, pagevec, pglen, base, msgp->msg_flags);
}

static int
__ics_llsendmsg(ics_sock_conn_data_t *connp, struct msghdr *msgp, int size)
#else
int
ics_llsendmsg(ics_sock_conn_data_t *connp, struct msghdr *msgp, int size)
#endif /* !ICS_TCP_SENDPAGE */
{
	int retval;
	struct task_struct *t = current;
	int tmpret = 0;
	int saved_size;
	int saved_iovlen;
	struct iovec *saved_iov;
	struct iovec saved_iovec;
	struct iovec *saved_iovecp;
	sigset_t saved_sigset;

	add_ics_randomness(connp->conn_snd_rand_statep,
			   ((unsigned long)msgp + size) / sizeof(int));

	retval = sock_sendmsg(connp->conn_sock, msgp, size);
	if (retval == size)
		return 0;
	if (retval < 0) {
		if (retval != -ERESTARTSYS && retval != -EINTR)
			goto out;
		retval = 0;
	}

	saved_size = size;
	saved_iov = saved_iovecp = msgp->msg_iov;
	saved_iovec = *msgp->msg_iov;
	saved_iovlen = msgp->msg_iovlen;
	tmpret = retval;

	ssi_block_signals(t, &saved_sigset);

	for (size -= tmpret; size > 0; size -= tmpret) {
		while (tmpret >= msgp->msg_iov->iov_len) {
			tmpret -= msgp->msg_iov->iov_len;
			msgp->msg_iov++;
			msgp->msg_iovlen--;
			SSI_ASSERT(msgp->msg_iovlen > 0);
		}

		rmb(); /* control dependency on msg_iov */

		if (saved_iovecp != msgp->msg_iov) {
			*saved_iovecp = saved_iovec;
			saved_iovecp = msgp->msg_iov;
			saved_iovec = *msgp->msg_iov;
		}
		msgp->msg_iov->iov_base += tmpret;
		msgp->msg_iov->iov_len -= tmpret;

		tmpret = sock_sendmsg(connp->conn_sock, msgp, size);
		/* if (tmpret == -ERESTARTSYS && retval != -EINTR) */
		if (tmpret == -ERESTARTSYS)
			tmpret = 0;
		if (tmpret < 0) {
			if (retval == 0)
				retval = tmpret;
			break;
		} else if (tmpret == 0) {
			printk(KERN_WARNING
			       "%s: failed to send any data, retrying\n",
				       __FUNCTION__);
			idelay(HZ/4);
		} else
			retval += tmpret;
	}
	ssi_unblock_signals(t, &saved_sigset);

	msgp->msg_iov = saved_iov;
	msgp->msg_iovlen = saved_iovlen;
	*saved_iovecp = saved_iovec;
	size = saved_size;
 out:
	if (retval < 0 || retval != size) {
		if ((ics_llnodeinfo[connp->conn_node]->icsni_flag &
		     (ICSNI_GOING_DOWN | ICSNI_NODE_DOWN)) == 0) {
			if (retval < 0)
				printk(KERN_WARNING
				       "%s: node %u chan %d, error %d\n",
				       __FUNCTION__,  connp->conn_node,
				       connp->conn_chan, -retval);
			else
				printk(KERN_WARNING
				       "%s: node %u chan %d,"
				       " sent %d/%d bytes, error %d\n",
				       __FUNCTION__, connp->conn_node,
				       connp->conn_chan, retval, size,
				       -tmpret);
		}
	}

	return retval;
}

void
__ics_llfreemsg(struct sk_buff *hskbp, struct sk_buff *tskbp)
{
	struct sk_buff *nskbp;

	for (;;) {
		nskbp = hskbp->next;
		kfree_skb(hskbp);
		if (hskbp == tskbp)
			break;
		hskbp = nskbp;
	}
}
