/*
 * 	ICS server-side code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * Server-side ICS code.
 */

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <cluster/nsc.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics/ics_sig.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */

#include <cluster/gen/ics_icssig_protos_gen.h>

/*
 * Data structures used for interaction between icssvr_recv() and the server
 * daemon dealing with running out of memory for replies.
 */
COND_LOCK_T	svrhand_nomem_list_lock;
svr_handle_t	*svrhand_nomem_list;
CONDITION_T	svrhand_nomem_cond;

#ifdef		DEBUG
/*
 * Variable for debugging.
 */
int		icssvr_maxprio;
#endif

/*
 * Data structures dealing with the list of server handles waiting for
 * incoming messages.
 */
typedef struct {
	svr_handle_t	*shwait_first;
	boolean_t	shwait_nohandles;
	boolean_t	shwait_nomem;
} svrhand_wait_list_t;

svrhand_wait_list_t svrhand_wait_list[ICS_MAX_CHANNELS];

SOFTIRQ_SPIN_LOCK_T	svrhand_wait_list_lock __cacheline_aligned_in_smp;

int		icssvr_flowpages_lwm = DEFAULT_FREEPAGES_LWM;
int		icssvr_flowpages_hwm = DEFAULT_FREEPAGES_HWM;
EXPORT_SYMBOL(icssvr_flowpages_lwm);
EXPORT_SYMBOL(icssvr_flowpages_hwm);

#ifdef ICS_SVR_HANDLE_KMEM_CACHE
static kmem_cache_t *svr_handle_cachep;
#endif

/*
 * icssvr_init()
 *	Perform any relevant server initialization.
 */
void
icssvr_init()
{
	int		i;

	/*
	 * Start up the no-more-memory daemon.
	 */
	INIT_COND_LOCK(&svrhand_nomem_list_lock);
	INIT_CONDITION(&svrhand_nomem_cond);

	/*
	 * Initialize array tracking server handles being processed.
	 */
	for (i = 0; i < SVRHAND_ARRAYSZ; i++) {
		INIT_SPIN_LOCK(&svrhand_list[i].shlist_lock);
	}

	/*
	 * Initialize the server data structure lock.
	 */
	INIT_SOFTIRQ_SPIN_LOCK(&svrhand_wait_list_lock);

#ifdef ICS_SVR_HANDLE_KMEM_CACHE
	svr_handle_cachep = kmem_cache_create("ics_svr_handle",
				sizeof(svr_handle_t), 0,
				SLAB_HWCACHE_ALIGN|SLAB_RECLAIM_ACCOUNT|SLAB_PANIC,
				NULL, NULL);
#endif

	/*
	 * Initialize the low-level ICS server code.
	 */
	icssvr_llinit();
	/*
	 * Start up the ICS server process management.
	 */
	icssvr_mgmt_init();
}

/*
 * icssvr_handle_get()
 *	Get a server handle for use in further calls to ICS server code
 *
 * Description:
 *	This routine is called by ICS high level code (and possibly by other
 *	NSC components) to obtain a server handle for subsequent calls to
 *	icssvr_*() routines.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	service		NSC service the handle is to be used for
 *
 * Return value:
 *	Pointer to a server handle. Routine cannot fail.
 */
svr_handle_t *
icssvr_handle_get(
	int		service,
	boolean_t	sleep_flag)
{
	svr_handle_t	*handle;
	int		error;

	/* Keep statistics. */
	LOCK_SPIN_LOCK(&icsstat_lock);
	icsstat.istat_svrhand_create++;
	icsstat.istat_svrhand++;
	UNLOCK_SPIN_LOCK(&icsstat_lock);

	/*
	 * Allocate a server handle structure for use by this operation.
	 */
	if (sleep_flag)
#ifdef ICS_SVR_HANDLE_KMEM_CACHE
		handle = kmem_cache_alloc(svr_handle_cachep, GFP_KERNEL|__GFP_NOFAIL);
	else
		handle = kmem_cache_alloc(svr_handle_cachep, GFP_ATOMIC);
#else
		handle = kmalloc_nofail(sizeof(svr_handle_t));
	else
		handle = kmalloc(sizeof(svr_handle_t), GFP_ATOMIC);
#endif
	SSI_ASSERT(handle != NULL || !sleep_flag);
	if (handle == NULL)
		return(NULL);
	handle->sh_service = service;
	handle->sh_flag = 0;
	handle->sh_perm_prio = 0;
	handle->sh_next = handle->sh_prev = NULL;

	/* Perform any initialization necessary for low-level ICS code. */
	error = icssvr_llhandle_init(handle, sleep_flag);
	if (error) {
#ifdef ICS_SVR_HANDLE_KMEM_CACHE
		kmem_cache_free(svr_handle_cachep, handle);
#else
		kfree((caddr_t)handle);
#endif
		handle = NULL;
	}

	return(handle);
}

/*
 * icssvr_recv()
 *	Arrange for a callback when a message arrives from the client
 *
 * Description:
 *	Arrange for a callback routine to be called when a message arrives
 *	from a client node for the service associated with the specified
 *	handle.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle for which a callback is to be arranged
 *	callback	callback routine that is to be called when an
 *			incoming message arrives; first parameter to the
 *			callback routine is the handle; the second argument is
 *			the callarg argument to icssvr_recv().
 *	callarg		second argument to the callback routine when the
 *			callback routine is invoked
 */
void
icssvr_recv(
	svr_handle_t	*handle,
	void		(*callback)(
				svr_handle_t	*handle,
				long		callarg),
	long		callarg)
{
	svrhand_wait_list_t *shw_p;
	int		error;
	int		dowakeup;
	int		chan = (handle->sh_perm_prio > 0)
				  ? ics_min_prio_chan + handle->sh_perm_prio - 1
				  : ICS_CHAN(handle->sh_service);

	/* Start the flag off with a zero value. */
	handle->sh_flag = 0;

	/* Set up the callback for once the message arrives. */
	handle->sh_callback_recv = callback;
	handle->sh_callarg = callarg;

	/*
	 * Perform low-level ICS code initialization. If the low-level
	 * code initialization can't be done without waiting, then
	 * we pass responsibility for the initialization to a background
	 * daemon (the background daemon should rarely be used).
	 */
	error = icssvr_llhandle_init_for_recv(handle, FALSE);
	if (error != 0)  {
#ifdef DEBUG
		printk(KERN_DEBUG "icssvr_recv: icssvr_llhandle_init_for_recv() failed! Waking up nomem_daemon.\n");
#endif /* DEBUG */
		LOCK_COND_LOCK(&svrhand_nomem_list_lock);
		handle->sh_next = svrhand_nomem_list;
		svrhand_nomem_list = handle;
		UNLOCK_COND_LOCK(&svrhand_nomem_list_lock);
		SIGNAL_CONDITION(&svrhand_nomem_cond);
		return;
	}

	/*
	 * Put the handle on the list of handles waiting for messages.
	 * If we hand previously faced a "no available handles" condition,
	 * then inform low-level code that handles are now available.
	 * Note that the low-level code is informed once all locks have
	 * been released, so that the low-level code can directly perform
	 * an upcall to high-level code with a message, if it likes.
	 */
	shw_p = &svrhand_wait_list[chan];
	LOCK_SOFTIRQ_SPIN_LOCK(&svrhand_wait_list_lock);
	handle->sh_next = shw_p->shwait_first;
	shw_p->shwait_first = handle;
	dowakeup = (shw_p->shwait_nohandles && !shw_p->shwait_nomem);
	shw_p->shwait_nohandles = FALSE;
	UNLOCK_SOFTIRQ_SPIN_LOCK(&svrhand_wait_list_lock);
	if (dowakeup)
		icssvr_llhandles_present(chan);
}

/*
 * icssvr_find_recv_handle()
 *	Find a handle that is performing an icssvr_recv()
 *
 * Description:
 *	Find a handle for the specified service that is ready to receive
 *	a message (i.e. that has performed a call to icssvr_recv()).
 *
 *	If no handles are available then the routine will return NULL
 *	and will call icssvr_llhandles_present() once such a handle
 *	is available.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	ics_prio	the ICS priority of the handle to be obtained.
 *	svc		the specified service.
 *
 * Returns:
 *	The handle, or NULL if there are no handles available.
 */
svr_handle_t *
icssvr_find_recv_handle(
	int		chan,
	int		svc,
	int		flags)
{
	int		prio = (chan >= ics_min_prio_chan &&
				chan <= ics_max_prio_chan)
					? chan - ics_min_prio_chan + 1
					: 0;
	svr_handle_t	*handle;
	svrhand_wait_list_t *shw_p = &svrhand_wait_list[ICS_CHAN(svc)];
	svrhand_wait_list_t *shw_prio_p = &svrhand_wait_list[chan];
#ifdef	DEBUG
	static int	throttled_first_time = FALSE;
#endif	/* DEBUG */

	LOCK_SOFTIRQ_SPIN_LOCK(&svrhand_wait_list_lock);

	/*
	 * See if there is a handle available. If not, make sure that the
	 * low-level ICS code becomes informed when such a handle does become
	 * available.
	 */
	if (shw_p->shwait_first != NULL) {

		/*
		 * First choice is to pick a handle off the list for this
		 * service. This applies even if it is a high priority message.
		 */
		handle = shw_p->shwait_first;
		SSI_ASSERT(ICS_CHAN(handle->sh_service) == ICS_CHAN(svc));
		shw_p->shwait_first = handle->sh_next;
		handle->sh_next = NULL;
		icsstat.istat_svrhand_recv[0][chan]++;

	} else if (prio > 0 && shw_prio_p->shwait_first != NULL) {

		/*
		 * If this is a high priority message and there is a special
		 * high priority handle available, then choose a high
		 * priority handle (and take it off the list). Note that for
		 * high priority handles the sh_service field changes from
		 * use to use.
		 */
		handle = shw_prio_p->shwait_first;
		shw_prio_p->shwait_first = handle->sh_next;
		handle->sh_next = NULL;
		handle->sh_service = svc;
		icsstat.istat_svrhand_recv[prio][ICS_CHAN(svc)]++;

	} else if (prio > 0) {

		/*
		 * If this is a high priority message and there are no handles
		 * of normal priority for this service or of the correct high
		 * priority, then mark the fact that we are waiting for a high
		 * priority handle (of the correct priority) to become
		 * available.
		 */
		shw_prio_p->shwait_nohandles = TRUE;
		shw_p->shwait_nohandles = TRUE;
		icsstat.istat_svrhand_norecv[prio][ICS_CHAN(svc)]++;
		handle = NULL;

	} else {

		/*
		 * If this is a normal priority message and there are no
		 * handles of normal priority for this service, then mark the
		 * fact that we are waiting for a normal priority handle to
		 * become available.
		 */
		SSI_ASSERT(chan == ICS_CHAN(svc));
		shw_p->shwait_nohandles = TRUE;
		icsstat.istat_svrhand_norecv[0][chan]++;
		handle = NULL;
		if (flags & SVR_HANDLE_CRIT_MEM) {
			printk(KERN_WARNING
			       "icssvr_find_recv_handle: out of handles for"
			       " critical process\n");
		}
	}
	UNLOCK_SOFTIRQ_SPIN_LOCK(&svrhand_wait_list_lock);

#if	defined(DEBUG)
	/* Keep track of the maximum priorities encountered. */
	if (prio > icssvr_maxprio)
		icssvr_maxprio = prio;
#endif	/* DEBUG */

	return(handle);
}

/*
 * icssvr_sendup_msg()
 *	Tell ICS high-level code that a message has arrived.
 *
 * Description:
 *	This routine is called by the ICS low-level code when a message
 *	arrives on the server node and the ICS low-level code has associated
 *	the message with a server handle.
 *
 *	This routine is responsible for performing the user-specified
 *	callback routine.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the handle whose reply has arrived
 */
void
icssvr_sendup_msg(
	svr_handle_t	*handle)
{
	int		normalchan = ICS_CHAN(handle->sh_service);
	int		ssvc = ICS_SSVC(handle->sh_service);
	int		prio = (handle->sh_chan >= ics_min_prio_chan &&
				handle->sh_chan <= ics_max_prio_chan)
				       ? handle->sh_chan - ics_min_prio_chan + 1
				       : 0;

	SSI_ASSERT(handle->sh_chan >= 0 && handle->sh_chan < ics_num_channels);
	SSI_ASSERT(normalchan >= 0 && normalchan < ics_num_channels);
	SSI_ASSERT(ssvc >= 0 && ssvc < ICS_MAX_SUBSERVICES);
	SSI_ASSERT(handle->sh_procnum >= 0 &&
	       handle->sh_procnum<icsservice[normalchan].icssvc_ops_cnt[ssvc]);
	SSI_ASSERT(icsstat.istat_svrrecv_proc[normalchan][ssvc] != NULL);

	/* Keep statistics. */
	if ((handle->sh_flag & SVR_HANDLE_NO_REPLY) != 0)
		icsstat.istat_svrmsg++;
	else
		icsstat.istat_svrrpc++;
	icsstat.istat_svrrecv[prio][normalchan]++;
	icsstat.istat_svrrecv_proc[normalchan][ssvc][handle->sh_procnum]++;

	/* Perform the callback routine as specified by the handle. */
	(*handle->sh_callback_recv)(handle, handle->sh_callarg);
}

/*
 * rics_sig_forward()
 *	Handle a signal that was forwarded to this server.
 *
 * Description:
 *	This routine is called when there is a signal that was encountered
 *	by the ICS client code that is forwarded to the server. The server
 *	code searches for a server handle with the matching transaction ID
 *	and posts the signal to the corresponding server proc.
 *
 *	This routine is the server routine for an ICS RPC call from the
 *	client node.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		the current node number
 *	cli_node	the client's node number
 *	transid		transaction ID that we search for
 *	sig_flag	flag for server signal posting - OS-dependant
 *	signo		signal we are to post
 *
 * Return value:
 *	Always zero.
 */
int
rics_sig_forward(
	clusternode_t	node,
	int		*rerror,
	clusternode_t	cli_node,
	long		transid)
{
	svr_handle_t	*handle;
	svrhand_list_t 	*shl_p = &svrhand_list[SVRHAND_HASH(cli_node)];

	/* Keep statistics. */
	*rerror = 0;
	LOCK_SPIN_LOCK(&shl_p->shlist_lock);
	icsstat.istat_svrsigforward++;

	/*
	 * Look for the handle in the list of handles being dealt with.
	 */
	for (handle = shl_p->shlist_first;
	     handle != NULL;
	     handle = handle->sh_next) {
		if (handle->sh_transid == transid &&
		    handle->sh_node == cli_node) {
			/*
			 * We did find a handle with a matching transaction ID.
			 * Attempt to signal the specified server proc.
			 */
			ICSSIG_POST_SERVER_SIGNAL(handle->sh_procp);
			break;
		}
	}
	/*
	 * If we didn't find a handle, return with a failure.
	 */
	if (handle == NULL) {
		*rerror = -ENOENT;
		icsstat.istat_svrsigforward_retry++;
	}
	UNLOCK_SPIN_LOCK(&shl_p->shlist_lock);

	return 0;
}

/*
 * icssvr_reply()
 *	Reply to the client node.
 *
 * Description:
 *	This routine is called to reply to the client node. If the client
 *	sent a plain asynchronous message, then this routine tells the
 *	transport that all incoming arguments have been decoded and the
 *	high-level code is done with the handle. If the client sent the
 *	request side of a request/response message, then this routine
 *	is called to send the response back to the client.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle of the message
 *	callback	callback routine that is called once the transport
 *			is done with the reply (i.e. once the handle can
 *			be used for another message); the server code is
 *			not allowed to call icssvr_handle_release() or
 *			icssvr_recv() until after or during the callback
 *			routine; the first parameter to the callback routine
 *			is the handle; the second parameter to the callback
 *			routine is the callarg parameter to icssvr_reply()
 *			(see below)
 *	callarg		the second parameter to the callback routine when
 *			the callback routine is invoked
 */
void
icssvr_reply(
	svr_handle_t	*handle,
	void		(*callback)(
				svr_handle_t	*handle,
				long		callarg),
	long		callarg)
{
	/*
	 * Fill in the callback so that it can be performed later.
	 */
	handle->sh_callback_reply = callback;
	handle->sh_callarg = callarg;

	/*
	 * Call the ICS low-level code.
	 */
	icssvr_llreply(handle);
}

/*
 * icssvr_sendup_replydone()
 *	Tell ICS high-level code that reply has been sent.
 *
 * Description:
 *	This routine is called by the ICS low-level code when a reply
 *	has been sent from the server, so that the server-specified callback
 *	can be performed.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the handle whose reply has arrived
 *	status		error code indicating whether the reply was
 *			successfully sent (success == 0)
 */
void
icssvr_sendup_replydone(
	svr_handle_t	*handle,
	int		status)
{
	/* Perform the specified callback routine. */
	(*handle->sh_callback_reply)(handle, handle->sh_callarg);
}

/*
 * icssvr_handle_release()
 *	Release a server handle, since we have completed our operation.
 *
 * Description:
 *	This routine is called by ICS high level code (and possibly by other
 *	NSC components) to release a server handle. No subsequent
 *	icssvr_*() routines may be called with this handle.
 *
 *	For plain messages, this routine should not be called until the
 *	callback routine for icssvr_send() has been called (it may be called
 *	either as or during the callback routine for icssvr_send().
 *
 *	For message/response pairs, this routine should not be called
 *	until all output parameters in the response have been decoded.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle to be released
 *
 * Return value:
 */
void
icssvr_handle_release(
	svr_handle_t	*handle)
{
	/* Keep statistics. */
	LOCK_SPIN_LOCK(&icsstat_lock);
	icsstat.istat_svrhand--;
	UNLOCK_SPIN_LOCK(&icsstat_lock);

	/* Perform deinitialization by low-level ICS code. */
	icssvr_llhandle_deinit(handle);

	/* Free the handle. */
#ifdef ICS_SVR_HANDLE_KMEM_CACHE
	kmem_cache_free(svr_handle_cachep, handle);
#else
	kfree(handle);
#endif
}

#if	defined(DEBUG) || defined(DEBUG_TOOLS)
/*
 * print_icssvr()
 *	Print information about active server processes.
 *
 * Description:
 *	This is a routine that is callable from the kernel debugger that prints
 *	information about active server processes.
 *
 * Parameters:
 *	None.
 */
void
print_icssvr(void)
{
	int		i;
	svr_handle_t	*handle;

	printk(KERN_DEBUG "List of active server procs:\n");
	for (i=0; i < SVRHAND_ARRAYSZ; i++) {
		handle = svrhand_list[i].shlist_first;
		while (handle != NULL) {
			if (handle->sh_procp != NULL) {
				printk(KERN_DEBUG "handle 0x%p, "
					" transid 0x%lx, pid %d, node %u\n",
					handle,
					handle->sh_transid,
					handle->sh_procp->pid,
					handle->sh_node);
			}
			else {
				printk(KERN_DEBUG "<sh_procp==NULL; index=%d,"
				       " shlist_first=0x%p>\n",	i, handle);
			}
			handle = handle->sh_next;
		}
	}
}

/*
 * print_icssvr_wait()
 *	Print information about handle wait lists.
 *
 * Description:
 *	This is a routine that is callable from the kernel debugger that prints
 *	information about handle wait lists.
 *
 * Parameters:
 *	None.
 */
void
print_icssvr_wait(void)
{
	int		i, j;
	svr_handle_t	*handle;

	printk(KERN_DEBUG "srvhandle_wait_list:\n");
	for (i=0; i < ics_num_channels; i++) {
		printk(KERN_DEBUG "shwait_first=0x%p\tnohandles=%d\tnomem=%d\n",
			svrhand_wait_list[i].shwait_first,
			svrhand_wait_list[i].shwait_nohandles,
			svrhand_wait_list[i].shwait_nomem);
		j = 0;
		handle = svrhand_wait_list[i].shwait_first;
		while (handle) {
			j++;
			handle = handle->sh_next;
		}
		printk(KERN_DEBUG "chan %d has %d handles free\n", i, j);
	}
}

void
print_icssvr_handle(svr_handle_t *svr)
{
#ifdef NSC_ICSLOG
	printk(KERN_DEBUG "sh_node %u sh_transid 0x%lx sh_uniqueid 0x%04x \n",
		svr->sh_node, svr->sh_transid, svr->sh_uniqueid & 0xFFFF);
#else
	printk(KERN_DEBUG "sh_node %u sh_transid 0x%lx \n",
		svr->sh_node, svr->sh_transid);
#endif
	printk(KERN_DEBUG "sh_chan %d sh_service %d sh_procnum %d\n",
		svr->sh_chan, svr->sh_service, svr->sh_procnum);
	printk(KERN_DEBUG "sh_flag %d \n", svr->sh_flag);
	printk(KERN_DEBUG "sh_callback routine 0x%p sh_callarg 0x%lx \n",
		&(svr->sh_callback), svr->sh_callarg);
	printk(KERN_DEBUG "sh_perm_prio %d \n", svr->sh_perm_prio);
	printk(KERN_DEBUG "pid %d\n",
	       (svr->sh_procp != NULL ? svr->sh_procp->pid : 0));
	printk(KERN_DEBUG "sh_next 0x%p sh_prev 0x%p\n",
		svr->sh_next, svr->sh_prev);

	print_icssvr_llhandle(&svr->sh_llhandle);
}

#endif	/* DEBUG || DEBUG_TOOLS */

