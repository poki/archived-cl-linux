/*
 * 	ICS server-side management code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Portions Copyright 2007,2008 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * Server-side management of server procs ICS code.
 */

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_sig.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */

#include <cluster/gen/ics_icssig_protos_gen.h>

/* Forward references */
static void icssvr_daemon(void *);
static void icssvr_nanny(void *);
static int icssvr_max_prio_handle(void);
static void icssvr_recv_callback(svr_handle_t *, long);
static void icssvr_reply_callback(svr_handle_t *, long);

#ifdef CONFIG_VPROC
extern void svrproc_handle_children(void);
#endif

/*
 * Information kept about ICS server daemons.
 */
typedef struct icssvr {
	struct task_struct	*is_procp;	/* Proc structure for daemon. */
	struct icssvr	*is_next;	/* Chain of sleeping daemons. */
	EVENT_T		is_event;	/* Event daemon sleeps upon. */
	svr_handle_t	*is_svr_handle;	/* Current handle (after waking up). */
	int		is_perm_prio;	/* Permanent ICS priority. */
} icssvr_t;

/*
 * List of ICS daemons that are available for work. List is maintained
 * in LIFO order to maximize cache warming.
 */
icssvr_t	*icsdaemon_avail_list;

/*
 * One ICS daemon per priority level used for higher priority messages.
 */
icssvr_t	*icsdaemon_prio[ICS_NUM_PRIO];

/*
 * Number of ICS daemons that are currently on icsdaemon_avail_list, along
 * with goals (high and low water marks) for how many daemons to keep on the
 * list.
 */
#define ICSDAEMON_LWM_PCT	2
#define ICSDAEMON_HWM_PCT	8
int		icsdaemon_avail_cnt;
int		icsdaemon_avail_lwm = 4;
int		icsdaemon_avail_hwm = 15;

/*
 * List of ICS server handles that have messages but are not assigned to
 * daemons. There is one such list per ICS priority. Each list is a FIFO
 * list, with icssvr_handleq_first[] being the oldest handle on the list
 * (i.e. the first to be removed).
 */
svr_handle_t	*icssvr_handleq_first[ICS_NUM_PRIO];
svr_handle_t	*icssvr_handleq_last[ICS_NUM_PRIO];

/*
 * Size of the fixed pool of server handles that wait for high priority
 * messages.
 *
 * The priority zero element must have a value of "0" because this pool is
 * entirely dynamic. All other elements in the pool must be > zero.
 */
int		icssvr_priohandle_cnt[ICS_NUM_PRIO] = {0, 5, 5, 5, 5};

/*
 * Number of ICS server handles that are actively waiting for a message to
 * arrive (per service), along with goals (high and low water marks, per
 * service) for how many daemons to keep on the list.
 *
 * Note that these numbers are only tracked for the channels that correspond
 * to priority zero service channels.
 */
int		icssvc_handle_cnt[ICS_MAX_CHANNELS];
int		icssvc_handle_lwm[ICS_MAX_CHANNELS];
int		icssvc_handle_hwm[ICS_MAX_CHANNELS];

/*
 * Lock structure used for locking icsdaemon_avail_list, icsdaemon_avail_cnt,
 * icssvr_handleq_*, and icssvc_handle_cnt[].
 */
SOFTIRQ_SPIN_LOCK_T	icssvr_mgmt_lock __cacheline_aligned_in_smp;

/*
 * Number of ICS nanny daemons.
 */
int		icsnannydaemon_cnt = 1;

/*
 * Event that ICS nanny daemons wait upon.
 */
EVENT_T		icssvr_nanny_event;

/*
 * Amount to delay between retries in icssvr_nodedown_svc_wait()
 */
int		icssvr_nodedown_wait_delay = 3;

static int	icssvr_nodedown_wait_svcs = 0;
static int	icssvr_nodedown_wait_list[ICS_MAX_CHANNELS*ICS_MAX_SUBSERVICES];

/*
 * icssvr_mgmt_earlylock_init
 * 	Initialize locks we'll need before icssvr_mgmt_init gets called
 */
void icssvr_mgmt_earlylock_init()
{
	/*
	 * Initialize the lock used for server management.
	 */
	INIT_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);

	 /* Initialize the ICS nanny event */
	INIT_EVENT(&icssvr_nanny_event);
}


/*
 * icssvr_mgmt_init()
 *	Perform any relevant initialization for managing server processes and
 *	the handling of incoming messages to the server.
 */
void
icssvr_mgmt_init()
{
	int		i;
	int		j;
	int		error;
	svr_handle_t	*handle;

	/*
	 * Set up the daemon processes and server handles for high priority
	 * messages.
	 */
	SSI_ASSERT(ICS_NUM_PRIO > 1);
	for (i = 1; i < ICS_NUM_PRIO; i++) {
		error = spawn_daemon_proc("icssvr_daemon",
					  icssvr_daemon,
					  (void *)((long)i));
		if (error != 0)
			panic("icssvr_mgmt_init: spawn_daemon_proc error=%d",
			      error);
	}
	/*
	 * Set up the fixed pool of high priority handles.
	 */
	for (i = 1; i < ICS_NUM_PRIO; i++) {
		SSI_ASSERT((i == 0 &&  icssvr_priohandle_cnt[i] == 0) ||
		      (i > 0 &&  icssvr_priohandle_cnt[i] > 0));
		for (j = 0; j < icssvr_priohandle_cnt[i]; j++) {
			handle = icssvr_handle_get(0, TRUE);
			if (handle == NULL)
				panic("icssvr_mgmt_init: "
				      "priority handle get failed");
			handle->sh_perm_prio = i;
			icssvr_recv(handle, icssvr_recv_callback, 0);
		}
	}

	/*
	 * Start up the ICS nanny daemon(s).
	 * Note: lock initialized above in ics_earlylock_init()
	 */
	SSI_ASSERT(icsnannydaemon_cnt > 0);
	for (i = 0; i < icsnannydaemon_cnt; i++) {
		error = spawn_daemon_proc("icssvr_nanny",
					  icssvr_nanny,
					  NULL);
		if (error != 0)
			panic("icssvr_mgmt_init: spawn_daemon_proc error=%d",
			      error);
	}

	/*
	 * Wake up the nanny daemon for the first time to create the
	 * relevant server procs.
	 */
	SIGNAL_EVENT(&icssvr_nanny_event);
}

/*
 * icssvr_svcmgmt_init()
 *	Perform any relevant server management initialization that is
 *	necessary when a new service (or subservice) is added.
 *
 * Parameters:
 *	service		the NSC service that was added
 *	handle_lwm	the minimum number of handles that we try to keep
 *			in a state ready to receive a message for the service
 *	handle_hwm	the maximum number of handles that we try to keep
 *			in a state ready to receive a message for the service
 *
 * Note: This routine is called for each subservice added into a service.
 * Each call will added to the watermarks, and will result in the nanny
 * being awoken on each call.
 */
void
icssvr_svcmgmt_init(
	int		service,
	int		handle_lwm,
	int		handle_hwm)
{
	int		chan = ICS_CHAN(service);

	/* Validate the LWM and HWM for reasonableness. */
	if (handle_lwm >= handle_hwm) {
		printk(KERN_WARNING
		       "icssvr_mgmt_init: "
		       "service %d has invalid lwm/hwm (%d/%d)",
		       service,
		       handle_lwm,
		       handle_hwm);
		handle_hwm = handle_lwm + 10;
	}

	/* Set up the limits. */
	if (icssvc_handle_lwm[chan] < handle_lwm)
		icssvc_handle_lwm[chan] = handle_lwm;
	if (icssvc_handle_hwm[chan] < handle_hwm)
		icssvc_handle_hwm[chan] = handle_hwm;

	/* Start up a nanny daemon to set up the handles initially. */
	SIGNAL_EVENT(&icssvr_nanny_event);
	LOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
	icssvr_nodedown_wait_list[icssvr_nodedown_wait_svcs++] = service;
	UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
}

/*
 * icssvr_daemon()
 *	Daemon process executing a server loop for ICS.
 *
 * Description:
 *	This routine is the main server loop that is executed for each
 *	server daemon. This routine is called when a server process is
 *	created.
 *
 *	The routine is loops around checking for handles needing processing.
 *	If none are found, then it sleeps waiting for a handle to be assigned
 *	to it. If there are too many sleeping icssvr_daemon() processes, then
 *	the daemon terminates itself.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	prio		the permanent ICS priority of the ICS server; daemons
 *			with permanent priorities > 0 are separate entities
 *			tracked in the icsdaemon_prio[] array and are not
 *			kept on the standard daemon list.
 */
void
icssvr_daemon(
	void		*void_prio)
{
	icssvr_t	*svr_p;
	icsservice_t	*svc_p;
	svr_handle_t	*handle;
	svrhand_list_t 	*shl_p;
	boolean_t	keep_handle;
	boolean_t	keep_daemon;
	int		prio = (int)((long)void_prio);
	int		handle_prio;
	int		normalchan;
	int		ssvc;
	struct k_sigaction *ka;

	nsc_daemonize();

#ifdef CONFIG_SSI
	current->flags |= PF_ICSSVR_DAEMON;
#endif

	/* Keep statistics. */
	LOCK_SPIN_LOCK(&icsstat_lock);
	icsstat.istat_svrdaemon_create++;
	icsstat.istat_svrdaemon++;
	UNLOCK_SPIN_LOCK(&icsstat_lock);

	/*
	 * Allocate and initialize a server structure that is used
	 * exclusively by this daemon.
	 */
	svr_p = kzmalloc_nofail(sizeof(*svr_p));
	SSI_ASSERT(svr_p != NULL);
	INIT_EVENT(&svr_p->is_event);
	svr_p->is_procp = current;
	svr_p->is_perm_prio = prio;

	/*
	 * If this is a special high priority daemon process, then track it
	 * especially.
	 */
	if (prio > 0)
		icsdaemon_prio[prio] = svr_p;
#ifdef NSC_INHERIT_USER_NICE
	else
		(void) set_daemon_prio(0, SCHED_NORMAL);
#endif

	/*
	 * Allow SIGCHLD's
	 */
	spin_lock_irq(&current->sighand->siglock);
	ka = current->sighand->action + SIGCHLD-1;
	ka->sa.sa_handler = SIG_DFL;
	siginitsetinv(&current->blocked, sigmask(SIGCHLD));
	recalc_sigpending();
	spin_unlock_irq(&current->sighand->siglock);

	/*
	 * Loop forever processing arriving messages...
	 */
	for (;;) {
		/* What to do... what to do... */
		LOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
		if (prio > 0 && icssvr_handleq_first[prio] != NULL) {

			/*
			 * If we get here then this is a high priority daemon
			 * and there is a handle queued up at this priority.
			 * When this is the case, we remove the handle from
			 * the list and process the handle.
			 */
			icsstat.istat_svrdaemon_nosleep[prio]++;
			handle = icssvr_handleq_first[prio];
			icssvr_handleq_first[prio] = handle->sh_next;
			if (icssvr_handleq_first[prio] == NULL)
				icssvr_handleq_last[prio] = NULL;
			handle->sh_next = NULL;
			UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);

		} else if (prio > 0) {

			/*
			 * If we get here then this is a high priority daemon
			 * that has no work to do. When this is the case we
			 * make the daemon wait for an event. High priority
			 * daemons that are available are tracked for
			 * availablility via simple pointer. When the daemon
			 * wakes up from the event, then handle to process is
			 * in the svr_p structure.
			 */
			icsstat.istat_svrdaemon_sleep[prio]++;
			icsdaemon_prio[prio] = svr_p;
			UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
			WAIT_EVENT(&svr_p->is_event);
			handle = svr_p->is_svr_handle;

		} else if ((handle_prio = icssvr_max_prio_handle()) >= 0) {

			/*
			 * If we get here then this is a normal priority
			 * daemon that has work to do (normal priority daemons
			 * process the highest priority handle that is queued
			 * up).
			 */
			icsstat.istat_svrdaemon_nosleep[0]++;

			/*
			 * Remove the handle from whichever list it was on.
			 * Mark the daemon as unavailable now.
			 */
			SSI_ASSERT(prio == 0);
			handle = icssvr_handleq_first[handle_prio];
			icssvr_handleq_first[handle_prio] = handle->sh_next;
			if (icssvr_handleq_first[handle_prio] == NULL)
				icssvr_handleq_last[handle_prio] = NULL;
			handle->sh_next = NULL;
			icsdaemon_avail_cnt--;
			UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);

			/*
			 * If there are now insufficient server procs,
			 * then wake up a nanny process to take care of
			 * any shortfalls.
			 */
			if (icsdaemon_avail_cnt < icsdaemon_avail_lwm)
				SIGNAL_EVENT(&icssvr_nanny_event);

		} else {

			/*
			 * If we get here then we have a normal priority daemon
			 * and there are no handles queued up at any priority.
			 * When this is the case, the daemon waits for a
			 * handle.
			 */
			icsstat.istat_svrdaemon_sleep[prio]++;

			/*
			 * Put the daemon on the global availability list.
			 */
			svr_p->is_next = icsdaemon_avail_list;
			icsdaemon_avail_list = svr_p;
			UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);

			/*
			 * Sleep until a handle arrives for the daemon. When
			 * we wake up the handle that we are supposed to
			 * process is in the svr_p structure.
			 */
			WAIT_EVENT(&svr_p->is_event);
			handle = svr_p->is_svr_handle;
		}

		/*
		 * When we get here, we have a handle that is ready to
		 * be processed.
		 */
		normalchan = ICS_CHAN(handle->sh_service);
		ssvc = ICS_SSVC(handle->sh_service);
		svc_p = &icsservice[normalchan];
		SSI_ASSERT(ssvc >= 0 && ssvc < ICS_MAX_SUBSERVICES);
		SSI_ASSERT(handle->sh_procnum >= 0 &&
		       handle->sh_procnum < svc_p->icssvc_ops_cnt[ssvc]);

		/*
		 * Put the handle on the chain of handles that are actively
		 * being processed. Note that we may discover that the node is
		 * going down, in which case we skip much of the remaining
		 * processing. We really MUST wait until the shlist_lock is
		 * locked for the check because of race conditions with
		 * icssvr_mgmt_nodedown().
		 */
		handle->sh_procp = svr_p->is_procp;
		shl_p = &svrhand_list[SVRHAND_HASH(handle->sh_node)];
		LOCK_SPIN_LOCK(&shl_p->shlist_lock);
		if (ics_nodestatus[handle->sh_node]
		    & ~(ICS_NODE_GOING_DOWN | ICS_NODE_DOWN)) {
			if (shl_p->shlist_first != NULL)
				shl_p->shlist_first->sh_prev = handle;
			handle->sh_next = shl_p->shlist_first;
			handle->sh_prev = NULL;
			shl_p->shlist_first = handle;
			UNLOCK_SPIN_LOCK(&shl_p->shlist_lock);

			/*
			 * Set up the ICS priority for any outgoing messages
			 * that might happen when the op is invoked. The
			 * priority should be one higher than the incoming
			 * message, if the incoming message was the send
			 * side of an RPC, and the same as the incoming message
			 * otherwise.
			 */
			SSI_ASSERT(ics_getpriority() == 0);
			handle_prio = (handle->sh_chan >= ics_min_prio_chan &&
				       handle->sh_chan <= ics_max_prio_chan)
					   ? handle->sh_chan-ics_min_prio_chan+1
					   : 0;
			if ((handle->sh_flag & SVR_HANDLE_NO_REPLY) == 0
			    && handle_prio != ICS_MAX_PRIO)
				handle_prio++;
			ics_setpriority(handle_prio);

			/* Track active daemons accurately. */
			LOCK_SPIN_LOCK(&icsstat_lock);
			icsstat.istat_svrdaemon_active++;
			UNLOCK_SPIN_LOCK(&icsstat_lock);

#ifdef NSC_ICSLOG
			if (do_icslog) {
				ics_log(1,
					ICSLOG_SVRFUNC,
					(long)handle, handle->sh_transid,
					handle->sh_uniqueid, handle->sh_node,
					(long)(svc_p->icssvc_ops_tbl[ssvc]
					       [handle->sh_procnum]),
					svr_p->is_procp->pid);
			}
#endif /* NSC_ICSLOG */

#ifdef NSC_INHERIT_USER_NICE
			if (prio == 0) {
				/*
				 * Inherit the client proc's nice level
				 */
				set_user_nice(current, handle->sh_user_nice);
			} else
				set_user_nice(current, 0);
#endif

			/*
			 * Invoke the op associated with the service.
			 */

			(void)(*svc_p->icssvc_ops_tbl[ssvc][handle->sh_procnum])
							(handle);

			/* Put the ICS priority back to zero. */
			ics_setpriority(0);

			/*
			 * Send a reply (if appropriate).
			 */
			icssvr_reply(handle,
				     icssvr_reply_callback,
				     (long) svr_p);
#ifdef NSC_ICSLOG
			if (do_icslog) {
				ics_log(1,
					ICSLOG_SVRRPLY,
					(long)handle, handle->sh_transid,
					handle->sh_uniqueid, handle->sh_node,
					(long)&svr_p->is_event,
					svr_p->is_procp->pid);
			}
#endif /* NSC_ICSLOG */

			/*
			 * Wait for the reply to wake us up.
			 */
			WAIT_EVENT(&svr_p->is_event);

			/* Track active daemons accurately. */
			LOCK_SPIN_LOCK(&icsstat_lock);
			icsstat.istat_svrdaemon_active--;
			UNLOCK_SPIN_LOCK(&icsstat_lock);

			/*
			 * Remove the handle from the chain of handles that are
			 * actively being processed.
			 */
			LOCK_SPIN_LOCK(&shl_p->shlist_lock);
			if (handle->sh_prev != NULL)
				handle->sh_prev->sh_next = handle->sh_next;
			else
				shl_p->shlist_first = handle->sh_next;
			if (handle->sh_next != NULL)
				handle->sh_next->sh_prev = handle->sh_prev;
			UNLOCK_SPIN_LOCK(&shl_p->shlist_lock);
			handle->sh_procp = NULL;

			/*
			 * Discard any signals that may have been forwarded
			 * to this proc.
			 */
			ICSSIG_DISCARD_SERVER_SIGNALS();
		} else {
			/*
			 * The node is actually down. Just continue and
			 * do the handle recycling that is necessary.
			 */
			UNLOCK_SPIN_LOCK(&shl_p->shlist_lock);
			printk(KERN_WARNING
			       "icssvr_daemon: skipping hdl 0x%p from"
			       " down node %u (transid 0x%lx)\n",
			       handle, handle->sh_node, handle->sh_transid);
		}

		/*
		 * Under spin lock control, figure out if we should recycle
		 * the handle or throw it away, and whether we should
		 * re-use the daemon process or not. Note that high priority
		 * daemon processes and high priority handles are not
		 * recycled.
		 */
		LOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
		keep_handle = (handle->sh_perm_prio > 0) ||
			      (icssvc_handle_cnt[normalchan] <
			       			icssvc_handle_hwm[normalchan]);
		if (keep_handle && handle->sh_perm_prio == 0)
			icssvc_handle_cnt[normalchan]++;
		keep_daemon = (prio > 0) ||
			      (icsdaemon_avail_cnt < icsdaemon_avail_hwm);
		if (keep_daemon && prio == 0)
			icsdaemon_avail_cnt++;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);

		/*
		 * Either recycle the handle for the next message, or throw
		 * it away.
		 */
		if (keep_handle)
			icssvr_recv(handle, icssvr_recv_callback, 0);
		else
			icssvr_handle_release(handle);

#ifdef CONFIG_VPROC
		/*
		 * If svr proc has any children then reassigned them
		 * to pid 2 since svr proc will never wait for them.
		 */
		svrproc_handle_children();
#else
		/* CI_XXX: currently havent implemented routine for
		 * svrproc children to be reassigned to init so they
		 * can be reaped.  It basically needs to cycle through
		 * the c_ptr and change its parent to init, so that
		 * init may reap them.
		 */
		if (!list_empty(&current->children))
			printk("svrproc has children that have not been "
				"reassigned to init: feature not "
				"implemented yet.\n");
#endif

		/*
		 * If it's necessary to throw away the daemon, then
		 * simply exit the loop.
		 */
		if (!keep_daemon)
			break;

		allow_reschedule();
	}

	/*
	 * If we get here then the daemon process is extraneous and it can
	 * go away.
	 */
	LOCK_SPIN_LOCK(&icsstat_lock);
	icsstat.istat_svrdaemon--;
	UNLOCK_SPIN_LOCK(&icsstat_lock);

	DEINIT_EVENT(&svr_p->is_event);
	kfree((caddr_t)svr_p);

	exit_daemon_proc();
}

/*
 * icssvr_max_prio_handle()
 *	Find maximum ICS priority that has a handle awaiting processing.
 *
 * Description:
 *	Go through all the ICS handle priority queues and figure out the
 *	highest priority queue for which there is a server handle where the
 *	message has arrived and which has no assigned ICS daemon process.
 *
 *	This routine assumes the icssvr_mgmt_lock is locked.
 *
 * Parameters:
 *	None.
 *
 * Return value:
 *	The priority of the highest priority server handle for which a message
 *	has arrived and has not been assigned to a daemon process. -1 is
 *	returned if there are no handles which are unassigned.
 */
static int
icssvr_max_prio_handle(void)
{
	int		i;

	for (i = (ICS_NUM_PRIO - 1); i >= 0; i--) {
		if (icssvr_handleq_first[i] != NULL)
			return(i);
	}
	return(-1);
}

/*
 * icssvr_recv_callback()
 *	The standard server callback routine for icssvr_recv()
 *
 * Description:
 *	The standard server callback routine for icssvr_recv(). If there are
 *	any icssvr_daemon() processes, it chooses one of them to wake up.
 *	If there are no available daemons, the handle is queued until such
 *	a daemon becomes available.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	Standard parameters specified by icssvr_recv() callback.
 */
static void
icssvr_recv_callback(
	svr_handle_t	*handle,
	long		callarg)
{
	int		chan = ICS_CHAN(handle->sh_service);
	icssvr_t	*svr_p;
	int		prio = (handle->sh_chan >= ics_min_prio_chan &&
				handle->sh_chan <= ics_max_prio_chan)
					? handle->sh_chan-ics_min_prio_chan+1
					:0;

	/*
	 * See if we can assign the handle to a sleeping daemon. If we can't
	 * then queue it up for later processing.
	 */
	LOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
	if (handle->sh_perm_prio == 0)
		icssvc_handle_cnt[chan]--;
	svr_p = icsdaemon_avail_list;
	if (svr_p != NULL) {

		/*
		 * We found a free normal priority daemon (normal priority
		 * daemons can process any priority message). Take the daemon
		 * off its queue and wake it up.
		 */
		icsdaemon_avail_list = svr_p->is_next;
		icsdaemon_avail_cnt--;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
		svr_p->is_svr_handle = handle;
		SIGNAL_EVENT(&svr_p->is_event);

	} else if (icsdaemon_prio[prio] != NULL) {

		/*
		 * This is a high priority message (since icsdaemon_prio[0] is
		 * always NULL), and the relevant high priority daemon is
		 * currently unassigned. Assign the handle to this daemon and
		 * wake the daemon up.
		 */
		SSI_ASSERT(prio > 0);
		svr_p = icsdaemon_prio[prio];
		SSI_ASSERT(svr_p->is_perm_prio == prio);
		icsdaemon_prio[prio] = NULL;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
		svr_p->is_svr_handle = handle;
		SIGNAL_EVENT(&svr_p->is_event);
	} else {

		/*
		 * There are no daemons (high priority or otherwise) that are
		 * unassigned. Put the handle on the queue for its ICS
		 * priority.
		 */
#ifdef NSC_ICSLOG
		if (do_icslog) {
			ics_log(1,
				ICSLOG_SVRNCBK,
				(long)handle, handle->sh_transid,
				handle->sh_uniqueid, handle->sh_node,
				chan, prio);
		}
#endif /* NSC_ICSLOG */
		if (icssvr_handleq_first[prio] == NULL)
			icssvr_handleq_first[prio] = handle;
		else
			icssvr_handleq_last[prio]->sh_next = handle;
		handle->sh_next = NULL;
		icssvr_handleq_last[prio] = handle;
		UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
	}

	/*
	 * If there are insufficient handles or insufficient server procs,
	 * then wake up a nanny process to take care of any shortfalls.
	 */
	if (icsdaemon_avail_cnt < icsdaemon_avail_lwm ||
			icssvc_handle_cnt[chan] < icssvc_handle_lwm[chan]) {
		SIGNAL_EVENT(&icssvr_nanny_event);
	}
}

/*
 * icssvr_reply_callback()
 *	The standard server callback routine for icssvr_reply()
 *
 * Description:
 *	The standard server callback routine for icssvr_reply(). Its
 *	sole duty is to wake up the server daemon.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	Standard parameters specified by icssvr_reply() callback.
 */
static void
icssvr_reply_callback(
	svr_handle_t	*handle,
	long		callarg)
{
	icssvr_t	*svr_p = (icssvr_t *)callarg;

	/* Merely wake the server back up. */
	SIGNAL_EVENT(&svr_p->is_event);
#ifdef NSC_ICSLOG
	if (do_icslog) {
		ics_log(1,
			ICSLOG_SVRCLBK,
			(long)handle, handle->sh_transid,
			handle->sh_uniqueid, handle->sh_node,
			(long)&svr_p->is_event, svr_p->is_procp->pid);
	}
#endif /* NSC_ICSLOG */
}

/*
 * icssvr_nanny()
 *	Daemon that ensure sufficient handles and daemon procs are available.
 *
 * Description:
 *	This is a daemon process that loops around permanently ensuring that
 *	there are sufficient ICS server handles and ICS server procs around
 *	to handle arriving messages/RPCs on the server.
 *
 *	The daemon sleeps on icssvr_nanny_event, which is signalled by other
 *	ICS code when potential shortages are spotted.
 *
 *	Note that more than one nanny daemon may be in operation at any time.
 *
 *	Also note that the nanny daemon is not responsible for getting rid of
 *	extraneous ICS server handles and ICS server procs - the ICS server
 *	procs police themselves in this regard.
 */
void
icssvr_nanny(
	void		*dummy)
{
	svr_handle_t	*handle;
	int		error;
	int		chan;

	nsc_daemonize();

	for (;;) {
		icsstat.istat_svrnanny_run++;

		/*
		 * Wait for one of the icssvr daemons to wake us up.
		 */
		WAIT_EVENT(&icssvr_nanny_event);

		/*
		 * See if we need to create any more handles for any
		 * particular service. If we do, create them and make them
		 * wait for the service. Note that we don't lock while
		 * performing the check, since creating an extra handle
		 * occasionally is no big deal.
		 */
		for (chan = 0; chan < ics_num_channels; chan++) {
			while(icssvc_handle_cnt[chan] <
			      icssvc_handle_lwm[chan]) {
				handle = icssvr_handle_get(ICS_NSC_SVC(chan, 0),
							   TRUE);
				SSI_ASSERT(handle != NULL);
				LOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
				icssvc_handle_cnt[chan]++;
				UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
				icssvr_recv(handle, icssvr_recv_callback, 0);
			}
		}

		/*
		 * See if we need to create more icssvr_daemon() processes.
		 * Note that we keep retrying if it fails.
		 */
		while (icsdaemon_avail_cnt < icsdaemon_avail_lwm) {
			int preverror = 0;

			LOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
			icsdaemon_avail_cnt++;
			UNLOCK_SOFTIRQ_SPIN_LOCK(&icssvr_mgmt_lock);
			do {
				error = spawn_daemon_proc("icssvr_daemon",
							  icssvr_daemon,
							  (void *) 0);
				if (error && error != preverror) {
					printk(KERN_WARNING
					       "icssvr_nanny: spawn_daemon_proc"
					       "error=%d, will be retried",
					       error);
					preverror = error;
				}
				if (error)
					idelay(HZ);
				if (error == 0 && preverror)
					printk(KERN_WARNING
					"icssvr_nanny: spawn_daemon_proc OK");
			} while(error);
		}

		allow_reschedule();
	}
}

/*
 * icssvr_mgmt_nodedown()
 *	Perform nodedown processing for the ICS server management.
 *
 * Description:
 *	Perform any nodedown processing that is neccessary for ICS server
 *	management. This means sending SIGKILL signals to all the server
 *	daemons that are processing messages for the specified node.
 *
 *	This is an internal ICS routine.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		the node that is going down.
 */
void
icssvr_mgmt_nodedown(
	clusternode_t	node)
{
	svrhand_list_t	*shl_p = &svrhand_list[SVRHAND_HASH(node)];
	svr_handle_t	*handle;

	/*
	 * Deliver SIGKILL signals to server procs that are handling
	 * messages from the specified node. In some Unicies, we
	 * needed to send SIGCONT because we did job control on the
	 * server.
	 */
	LOCK_SPIN_LOCK(&shl_p->shlist_lock);
	handle = shl_p->shlist_first;
	while (handle != NULL) {
		if (handle->sh_node == node) {
#ifdef NSC_ICSLOG
			if (do_icslog) {
				ics_log(1,
					ICSLOG_SVRDOWN,
					(long)handle, handle->sh_transid,
					handle->sh_uniqueid, handle->sh_node,
					handle->sh_procp->pid, 0);
			}
#endif /* NSC_ICSLOG */
			ICSSIG_POST_SERVER_SIGNAL(handle->sh_procp);
		}
		handle = handle->sh_next;
	}
	UNLOCK_SPIN_LOCK(&shl_p->shlist_lock);
}

/*
 * icssvr_nodedown_svc_wait()
 *	Wait for NSC servers on a node to clean up.
 *
 * Description:
 *	This routine is called by other NSC components during nodedown
 *	handling to make sure that there are no server daemons that are
 *	processing handles for a specific NSC service for the node that
 *	is going down.
 *
 *	This node must be called from thread context.
 *
 *	NOTE: This routine is a misnomer; it does not care if the
 *	node is going down, it only waits until there are no servers
 *	for the specific service and node. I attempted to add checking
 *	for the state of the node so that I could use this in root failover.
 *	This was wrong! If this is called from a nodedown routine, you
 *	are *guaranteed* that the node is going down and that a node with
 *	the same number will not replace it before nodedown is complete.
 *	There is no such guarantee in other contexts and this routine
 *	would deadlock.
 *
 * Parameters:
 *	node		the node that is going down.
 *	service		the NSC service that is cleaning up.
 */
void
icssvr_nodedown_svc_wait(
	clusternode_t	node,
	int		service)
{
	svrhand_list_t	*shl_p = &svrhand_list[SVRHAND_HASH(node)];
	svr_handle_t	*handle;
	boolean_t	handle_found;

	SSI_ASSERT(node > 0 && node <= NSC_MAX_NODE_VALUE);

	/*
	 * Loop until we find no handles that match...
	 */
	do {
		/*
		 * Go through all the handles on the hash list
		 * looking for matching node/service/subservice.
		 */
		handle_found = FALSE;
		LOCK_SPIN_LOCK(&shl_p->shlist_lock);
		handle = shl_p->shlist_first;
		while (handle != NULL) {
			if (handle->sh_node == node &&
			    handle->sh_service == service) {
				handle_found = TRUE;
				break;
			}
			handle = handle->sh_next;
		}
		UNLOCK_SPIN_LOCK(&shl_p->shlist_lock);
		/* Let other threads run. */
		if (handle_found)
			idelay(HZ/10);
	} while (handle_found);
}

/*
 * icssvr_nodedown_wait()
 *	Wait for all NSC servers on a node to clean up.
 *
 * Description:
 *	This routine is called by CLMS during nodedown handling to make sure
 *	that there are no server daemons that are processing handles for any
 *	NSC services for the node that is going down.  Calls
 *	icssvr_nodedown_svc_wait() to do the dirty work.
 *
 * Parameters:
 *	node		the node that is going down.
 */
void
icssvr_nodedown_wait(
	clusternode_t	node)
{
	int i;

	for (i = 0; i < icssvr_nodedown_wait_svcs; i++)
		icssvr_nodedown_svc_wait(node, icssvr_nodedown_wait_list[i]);
}

#if defined(DEBUG) || defined(DEBUG_TOOLS)

void
print_icssvr_handleq(void)
{
	svr_handle_t	*hp;
	int		i;

	for (i = 0; i < ICS_NUM_PRIO; i++) {
		for (hp = icssvr_handleq_first[i];
		     hp != NULL;
		     hp = hp->sh_next) {
			printk(KERN_DEBUG "handle 0x%p prio %d,"
			       " transid 0x%lx, node %u\n",
			       hp, i, hp->sh_transid, hp->sh_node);
		}
	}
}

#endif /* DEBUG || DEBUG_TOOLS */
