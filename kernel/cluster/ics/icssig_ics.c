/*
 * 	ICSSIG client/server stub routines.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <cluster/icsgen.h>

cluster_svc_t cluster_icssig_svc;

/*
 * Include prototypes, client and server stubs generated by icsgen.
 */
#include <cluster/gen/ics_icssig_protos_gen.h>
#include <cluster/gen/icscli_icssig_gen.c>
#include <cluster/gen/icssvr_icssig_gen.c>

/*
 * Include the server tables and service registration generated by icsgen.
 */
#include <cluster/gen/icssvr_icssig_tables_gen.c>

/*
 * Register ICSSIG components with CI.
 */
static int __init ci_init_icssig(void)
{
	/* Register ICSSIG cluster service */
	return register_cluster_svc(&cluster_icssig_svc,
				    ics_clms_chan,
				    ics_icssig_svc_init);
}

/* XXX: There is currently no option to compile this as a module. */
module_init(ci_init_icssig)
