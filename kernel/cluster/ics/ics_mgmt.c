/*
 * 	ICS initialization/management code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * High-level ICS initialization and general management routines.
 */

#include <linux/signal.h>
#include <cluster/clms.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */
#include <cluster/config.h>
#ifdef NSC_ICSLOG
#include <cluster/log.h>
#endif

/*
 * Node up and node down notification routines.
 */
void (*ics_nodeup_notification_routine)(clusternode_t, icsinfo_t *);
void (*ics_nodedown_notification_routine)(clusternode_t);

/*
 * The real copy of the ICS statistics structure.
 */
struct icsstat icsstat;

/*
 * Lock declarations.
 */
__cacheline_aligned_in_smp SPIN_LOCK_T icsstat_lock;	/* not used for "totals" */

#ifdef NSC_ICSLOG
int do_icslog = 1;
void *icslog_cookie = NULL;
#endif /* NSC_ICSLOG */

int ics_message_version = ICS_MESSAGE_VERSION;

int ics_nodestatus[NSC_MAX_NODE_VALUE + 1];

/* ics_earlylock_init()
 * 	Initialization routine for ICS locks that are used early
 *
 * Description
 *	ICS needs to be called after certain other subsystems have
 * 	already	initialized. But these other services need to register
 *	themselves with ICS, which requires that certain ICS locks be
 *	already initialized. This routine gets called before ics_init(),
 *	and performs those lock initializations.
 *
 * Parameters
 * 	None
 */
void ics_earlylock_init()
{
	icssvr_mgmt_earlylock_init();
}

/*
 * ics_init()
 *	ICS initialization routine
 *
 * Description:
 *	ICS initialization routine, called early in system boot-up. This
 *	is the sole initialization routine called for ICS - this routine
 *	is responsible for doing all the initialization for both client
 *	and server code, and for both high-level ICS code and for low-level
 *	ICS code.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	None.
 */
void
ics_init()
{
#ifdef NSC_ICSLOG
	icslog_cookie = nsc_log_init(ICSLOG_DEPTH, GFP_KERNEL);
	if (icslog_cookie == NULL)
		printk(KERN_WARNING "ics_init: unable to initialize logging\n");
#endif

	/* Initialize locks used across all of ICS. */
	INIT_SPIN_LOCK(&icsstat_lock);
	INIT_COND_LOCK(&ics_nodestatus_lock);
	INIT_CONDITION(&ics_nodestatus_cond);

	memset(ics_nodestatus, 0, sizeof(ics_nodestatus));

	/* Initialize the low-level code. */
	ics_llinit();

	/* Perform the server and client initialization. */
	icssvr_init();

	icscli_init();
}

/*
 * ics_svc_register()
 *	Register a NSC service with ICS
 *
 * Description:
 *	This routine is called once per NSC service, to give ICS servers
 *	the information necessary to deal with ICS messages that arrive
 *	on the server.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	service		integer identifying the NSC service
 *	num_ops		the number of server operations for the service
 *	service_ops	an array of num_ops procedure pointers, with the
 *			actual server operations
 *	lwm_svc_handles	min of waiting service handles created for this service
 *	hwm_svc_handles	max of waiting service handles created for this service
 *	lwm_svc_flowpages num of free pages when this service throttled
 *	hwm_svc_flowpages num of free pages when this service unthrottled
 */
void
ics_svc_register(
	int	 	service,
	char		*service_name,
	int		num_ops,
	int		(*service_ops[])(svr_handle_t *),
	char		*service_op_names[],
	int		lwm_svc_handles,
	int		hwm_svc_handles,
	int		lwm_svc_flowpages,
	int		hwm_svc_flowpages)
{
	int		chan = ICS_CHAN(service);
	int		subservice = ICS_SSVC(service);
	icsservice_t	*svc_p = &icsservice[chan];

	SSI_ASSERT(chan >= 0 && chan < ics_num_channels);
	SSI_ASSERT(subservice >= 0 && subservice < ICS_MAX_SUBSERVICES);

	/*
	 * Set up the sub-service data structure.
	 */
	svc_p->icssvc_ops_tbl[subservice] = service_ops;
	svc_p->icssvc_ops_cnt[subservice] = num_ops;

	/*
	 * Get the statistics table set up for this service.
	 */
	icsstat.istat_proc[chan][subservice] = num_ops;
	icsstat.istat_svc_name[chan][subservice] = service_name;
	icsstat.istat_svc_proc_names[chan][subservice] = service_op_names;
	SSI_ASSERT(icsstat.istat_clisend_proc[chan][subservice] == NULL);
	icsstat.istat_clisend_proc[chan][subservice] = (long *)
			kmalloc_nofail(num_ops * sizeof(long));
	memset(icsstat.istat_clisend_proc[chan][subservice],
	       0,
               num_ops * sizeof(long));
	SSI_ASSERT(icsstat.istat_svrrecv_proc[chan][subservice] == NULL);
	icsstat.istat_svrrecv_proc[chan][subservice] = (long *)
			kmalloc_nofail(num_ops * sizeof(long));
	memset(icsstat.istat_svrrecv_proc[chan][subservice],
	       0,
	       num_ops * sizeof(long));
	/*
	 * Do the initialization for the server handles.
	 */
	icssvr_svcmgmt_init(service, lwm_svc_handles, hwm_svc_handles);
}

/*
 * ics_nodeinfo_callback()
 *	Set up notification callbacks so ICS can perform callbacks when
 *	ICS detects a node has come up or has gone down.
 *
 * Description:
 *	Set up notification callbacks so that ICS can perform callbacks
 *	when the ICS detects that a node has come up or gone down.
 *
 *	This routine is typically used by CLMS during system initialization
 *	so that it can detect nodes changing status.
 *
 *	This routine can be called precisely once during system
 *	initialization. If either of the notification routines specified is
 *	NULL then the notification will not be performed for this node.
 *
 *	When/if the ICS subsystem detects that a node has come up, it will
 *	perform the callback, but it will not allow services (other than
 *	the CLMS service) to communicate with the node until ics_nodeup()
 *	has been called (the CLMS service is an exception - the CLMS may
 *	use this service prior to the call of ics_nodeup()).
 *
 *	When/if the ICS subsystem detects that a node has gone down, it
 *	will perform the callback, but communication requests to the node
 *	that has gone down will not return failures until ics_nodedown() is
 *	called.
 *
 *	This routine must be called from thread context.
 *
 *	The callback routines must be called from thread context.
 *
 * Parameters:
 *	nodeup_notification	routine to call when ICS detects a node has
 *				come up (and ics_nodeup() has not been called
 *				for the node)
 *	nodedown_notification	routine to call when ICS detects communication
 *				problems with a node
 */
void
ics_nodeinfo_callback(
	void		nodeup_notification(
				clusternode_t	node,
				icsinfo_t	*icsinfo),
	void		nodedown_notification(
				clusternode_t	node))
{
	SSI_ASSERT(ics_nodeup_notification_routine == NULL);
	SSI_ASSERT(ics_nodedown_notification_routine == NULL);

	/*
	 * Set up notification routines.
	 */
	ics_nodeup_notification_routine = nodeup_notification;
	ics_nodedown_notification_routine = nodedown_notification;

	/*
	 * Inform the low-level ICS code that high level ICS code is
	 * now ready to accept node up and node down notifications. The
	 * low-level ICS code may wish to complete some initialization
	 * at this time.
	 */
	ics_llaccept_notifications();
}

/*
 * ics_nodeup_notification()
 *	ICS low-level code informs ICS high-level code that a node has come up
 *
 * Description:
 *	This is a routine that is called by the ICS low-level code when it
 *	detects that a node has come up. The ICS low-level code is not
 *	permitted to call this routine until ics_llaccept_notifications()
 *	has been called.
 *
 *	Note that is the registered nodeup callback routine is non-NULL, then
 *	the CLMS component is allowed to communicate with the node.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that low-level ICS code detected coming up
 *	icsinfo_p	ics information of the node
 */
void
ics_nodeup_notification(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p)
{

	/*
	 * Call the registered callback routine.
	 */
	if (ics_nodeup_notification_routine != NULL &&
	    this_node == clms_master_node)
		(*ics_nodeup_notification_routine)(node, icsinfo_p);
}

/*
 * ics_nodedown_notification()
 *	ICS low-level code informs ICS high-level code that a node has gone
 *	down
 *
 * Description:
 *	This is a routine that is called by the ICS low-level code when it
 *	detects that a node has gone down. The ICS low-level code is not
 *	permitted to call this routine until ics_llaccept_notifications()
 *	has been called.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that low-level ICS code detected going down
 */
void
ics_nodedown_notification(
	clusternode_t	node)
{
	/*
	 * Call the registered callback routine.
	 */
	if (ics_nodedown_notification_routine != NULL)
		(*ics_nodedown_notification_routine)(node);
}

/*
 * ics_seticsinfo()
 *	Store the ICS communication information about a node
 *
 * Description:
 *	This routine is called to store the information ICS needs to
 *	make a connection with another node in its local database. This
 *	routine is called by the CLMS when the CLMS (either master or
 *	client) receives information about a node.
 *
 *	This routine is also be used internally to store the ICS information
 *	about this node, the CLMS master node, and about nodes for which
 *	nodeup notifications have been generated.
 *
 *	Until this call is done for a node, ics_nodeup() cannot be called for
 *	a node, nor can CLMS communicate with this node.
 *
 * Parameters:
 *	node		the node about which information is being stored
 *	icsinfo_p	the actual information being stored; treated as
 *			a character array
 */
int
ics_seticsinfo(
	clusternode_t	node,
	icsinfo_t	*icsinfo_p)
{
	int		error;

	/*
	 * Is node number valid?
	 */
	if (node <= 0 || node > NSC_MAX_NODE_VALUE)
		panic("ics_seticsinfo: node %d out of range", (int) node);

	/*
	 * Call the low-level ICS code to do the actual work.
	 */
	error = ics_llseticsinfo(node, icsinfo_p);

	/*
	 * Set the status of the node to "seticsinfo done"
	 */
	if (error == 0) {
		LOCK_COND_LOCK(&ics_nodestatus_lock);
		if (ics_nodestatus[node] & ICS_NODE_GOING_DOWN) {
			/* Node went down while coming up. */
			UNLOCK_COND_LOCK(&ics_nodestatus_lock);
			return (-EAGAIN);
		}
		if (ics_nodestatus[node] & ~(ICS_NODE_NEVER_UP | ICS_NODE_DOWN))
			panic("ics_seticsinfo: node %d has invalid status 0x%x",
			      (int) node,
			      ics_nodestatus[node]);
		ics_nodestatus[node] = ICS_NODE_SETICSINFO_DONE;
		UNLOCK_COND_LOCK(&ics_nodestatus_lock);
	}

	return(error);
}

/*
 * ics_nodeup()
 *	Inform ICS to that it should allow communication with another node
 *
 * Description:
 *	This routine is called (typically by NSC CLMS) to inform the ICS
 *	that it should allow communication by NSC services to a specific
 *	remote node. It may be that ICS had previously detected that this
 *	node came up (i.e. that ICS performed a nodeup notification callback),
 *	but this is not necessary, since the CLMS may have been informed
 *	that a node has come up by other means (i.e. the CLMS master may
 *	have detected that a node has come up and may tell this node about
 *	that fact).
 *
 *	Note that the CLMS service is allowed to communicate with other
 *	nodes even if ics_nodeup() has not been called.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has come up
 *
 * Return value:
 *	Zero if successful, -EAGAIN if the node went down while in the
 *	process of coming up, -EINVAL if the node simply failed to come up.
 */
int
ics_nodeup(
	clusternode_t	node)
{
	int		error;
	int		iskey;

	/*
	 * Set the status of the node to "coming up"
	 */
	if (node <= 0 || node > NSC_MAX_NODE_VALUE)
		panic("ics_nodeup: node %d out of range", (int) node);
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	if (ics_nodestatus[node] & (ICS_NODE_GOING_DOWN | ICS_NODE_DOWN)) {
		UNLOCK_COND_LOCK(&ics_nodestatus_lock);
 		printk(KERN_WARNING
		       "ics_nodeup: node %u went down while coming up.\n", node);
		return (-EAGAIN);
	}
	if (ics_nodestatus[node] & ~ICS_NODE_SETICSINFO_DONE)
		panic("ics_nodeup: node %d has invalid status %d",
		      (int) node,
		      ics_nodestatus[node]);
	ics_nodestatus[node] = ICS_NODE_COMING_UP;
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);

	/*
	 * Call the low-level code.
	 */
	error = ics_llnodeup(node);

	/*
	 * If the low-level code managed to bring the node up, then set the
	 * node status to "up". Note that an -EAGAIN return indicates that
	 * the node went down while coming up, whereas -EINVAL indicates a
	 * bad node number.
	 */
	iskey = clms_iskeynode(this_node);
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	/*
	 * The node may have died while we were trying to bring it up.  Pretend
	 * we failed and leave the ics node status unchanged.
	 */
	if (ics_nodestatus[node] & ICS_NODE_DOWN) {
		UNLOCK_COND_LOCK(&ics_nodestatus_lock);
		return(-EAGAIN);
	}
	if (error == 0)
		ics_nodestatus[node] = ICS_NODE_UP;
	else if (error == -EINVAL)
		ics_nodestatus[node] = ICS_NODE_DOWN;
	else if (error == -EAGAIN)
		ics_nodestatus[node] = ICS_NODE_SETICSINFO_DONE;
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);

	return(error);
}

/*
 * ics_nodeready()
 *	Inform ICS that the node in question is ready for full communication.
 *
 * Description:
 *	This routine is called by CLMS to inform ICS that it should allow
 *	communication by all services to a specific remote node.  Until this
 *	point, only CLMS may communicate with the node; all other communication
 *	is held up by icscli_send().
 *
 * Parameters:
 *	node		node that is ready
 *
 * Return value:
 *	None.
 */
void
ics_nodeready(
	clusternode_t	node)
{

	/*
	 * Set the status of the node to "coming up"
	 */
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	while (ics_nodestatus[node] & ~ICS_NODE_UP) {
		if (ics_nodestatus[node]
		    & (ICS_NODE_COMING_UP | ICS_NODE_SETICSINFO_DONE)) {
			UNLOCK_COND_LOCK(&ics_nodestatus_lock);
			idelay(HZ/100);
			LOCK_COND_LOCK(&ics_nodestatus_lock);
		}
		else
			panic("ics_nodeready: node %d has invalid status 0x%x",
			      (int) node,
			      ics_nodestatus[node]);
	}
	ics_nodestatus[node] = ICS_NODE_READY;
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);
}

/*
 * ics_nodedown()
 *	Inform ICS that a node has come down.
 *
 * Description:
 *	This routine is called (typically by NSC CLMS) to inform the ICS
 *	that it should stop communication with another node, and
 *	cause all outstanding communications with the node to fail.
 *	It may be that ICS had previously detected that this
 *	node went down (i.e. that ICS performed a nodedown notification
 *	callback), but this is not necessary.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_nodedown(
	clusternode_t	node)
{

	/*
	 * Current node can't go down.
	 */
	if (this_node == node)
		panic("ics_nodedown: this_node can't go down");

#ifdef DEBUG
	printk(KERN_NOTICE "!ics_nodedown (START): %d\n", (int) TICKS());
#endif /* DEBUG */

	/*
	 * Set the status of the node to "going down"
	 */
	if (node <= 0 || node > NSC_MAX_NODE_VALUE)
		panic("ics_nodedown: node %d out of range", (int) node);
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	ics_nodestatus[node] = ICS_NODE_GOING_DOWN;
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);

	/*
	 * Call the low-level code.
	 */
	ics_llnodedown(node);

	/*
	 * Do server management cleanup.
	 */
	icssvr_mgmt_nodedown(node);

	/*
	 * Set the status of the node to "down"
	 */
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	ics_nodestatus[node] = ICS_NODE_DOWN;
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);

#ifdef DEBUG
	printk(KERN_NOTICE "!ics_nodedown (END): %d\n", (int) TICKS());
#endif /* DEBUG */

	BROADCAST_CONDITION(&ics_nodestatus_cond);
}

/*
 * ics_nodedown_async_start()
 *	Inform ICS that a node has gone down.  This is the asynchronous
 *	version of ics_nodedown(), split up into a "start" and "end"
 *	routine.  This start routine will set the high and low level ICS
 *	states of the node that had gone down to the "going down" state.
 *	Later, in ics_nodedown_async_end(), we set the states to the
 *	fully "down" state.
 *
 * Description:
 *	This routine is called (typically by NSC CLMS) to inform the ICS
 *	that it should stop communication with another node, and
 *	cause all outstanding communications with the node to fail.
 *	It may be that ICS had previously detected that this
 *	node went down (i.e. that ICS performed a nodedown notification
 *	callback), but this is not necessary.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_nodedown_async_start(
	clusternode_t	node)
{

	/*
	 * Current node can't go down.
	 */
	if (this_node == node)
		panic("ics_nodedown: this_node can't go down");

#ifdef DEBUG
	printk(KERN_NOTICE "!ics_nodedown (START): %d\n", (int) TICKS());
#endif /* DEBUG */

	/*
	 * Set the status of the node to "going down"
	 */
	if (node <= 0 || node > NSC_MAX_NODE_VALUE)
		panic("ics_nodedown: node %d out of range", (int) node);
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	ics_nodestatus[node] = ICS_NODE_GOING_DOWN;
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);

	/*
	 * Call the low-level code.
	 */
	ics_llnodedown_async_start(node);

	/*
	 * Do server management cleanup.
	 */
	icssvr_mgmt_nodedown(node);
}

/*
 * ics_nodedown_async_end()
 *	Inform ICS that a node has gone down.  This is the asynchronous
 *	version of ics_nodedown(), split up into a "start" and "end"
 *	routine.  Previously, in ics_nodedown_async_start(), we set
 *	the high and low level ICS states of the node that has
 *	gone down to to the "going down" state.  Now, having finished
 *	nodedown processing, we can set the high and level ICS
 *	states to the fully "down" state.
 *
 * Description:
 *	This routine is called (typically by NSC CLMS) to inform that
 *	high and low level nodedown processing is finished for the node
 *	that has gone down.
 *
 *	This routine must be called from thread context.
 *
 * Parameters:
 *	node		node that has gone down
 */
void
ics_nodedown_async_end(
	clusternode_t	node)
{
	/*
	 * Set the low-level ICS state to "down"
	 */
	ics_llnodedown_async_end(node);

	/*
	 * Set the high-level ICS state to "down"
	 */
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	ics_nodestatus[node] = ICS_NODE_DOWN;
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);

#ifdef DEBUG
	printk(KERN_NOTICE "!ics_nodedown (END): %d\n", (int) TICKS());
#endif /* DEBUG */

	BROADCAST_CONDITION(&ics_nodestatus_cond);
}

/*
 * ics_waitfor_nodedown()
 *	Waits for a node to be completely down.
 *
 * Description:
 *	When a node joins the cluster, it's ICS Information (e.g. IP Address)
 *	is set and the connections data are stored. However, if the node had
 *	already been part of the cluster, crashed, and rebooted, the node has
 *	to be completely down (i.e. "Node Down" processing for that node has
 *	to be completed by the cluster) before joining again.
 *
 *	The following routine waits for the node to be completely down.
 *	Note that it waits from an ICS perspective only. That is, some
 *	amount of ICS processing (listed in the above paragraph), can be
 *	done after the node has been declared down by the ICS. The rest of
 *	the matter is taken care of by the clms and is not relevant here.
 *
 * Parameters:
 *	clusternode_t	node	- the node number of the node.
 *
 */
void
ics_waitfor_nodedown(clusternode_t node)
{
	LOCK_COND_LOCK(&ics_nodestatus_lock);
	if (ics_nodestatus[node] & ~ICS_NODE_NEVER_UP) {
		while (ics_nodestatus[node] & ~ICS_NODE_DOWN) {
			WAIT_CONDITION(&ics_nodestatus_cond,
				       &ics_nodestatus_lock);
		}
	}
	UNLOCK_COND_LOCK(&ics_nodestatus_lock);
}


void
ics_shoot_node(clusternode_t node)
{
	(void) ics_llshoot_node(node);
}

int
ics_isicsport(unsigned short port)
{
	return ics_llisicsport(port);
}

int
ics_isicsunackport(unsigned short port)
{
	return ics_llisicsunackport(port);
}

void
ics_transport_config(void) {
	ics_lltransport_config();
}


int ics_transport = ICS_TRANSPORT_NONE;

int
ics_get_transport(void) {
	return ics_transport;
}

void
ics_set_transport(int trans)
{
	SSI_ASSERT(trans >= ICS_TRANSPORT_NONE);
	SSI_ASSERT(trans <= ICS_LAST_TRANSPORT);
	ics_transport = trans;
}

int
ics_get_nodenumsize(void)
{
	return ics_llget_nodenumsize();
}

int
ics_get_addrlen(void)
{
	return ics_llget_addrlen();
}

int
ics_geticsinfo(clusternode_t node, icsinfo_t *icsinfo_p)
{
	return ics_llgeticsinfo(node, icsinfo_p);
}

void
ics_node_to_icsinfo(icsinfo_t *infop, clusternode_t node)
{
	ics_llnode_to_icsinfo(infop, node);
}

int
ics_probe_clms(clusternode_t node,
		icsinfo_t addr,
		clusternode_t *master_node,
		icsinfo_t *master_addr,
		int im_a_secondary,
	        int im_a_member)
{
	return ics_llprobe_clms(node, addr,
				master_node, master_addr,
				im_a_secondary,
				im_a_member);
}

int
ics_unack_send(clusternode_t node,
		u_int type,
		void *message,
		size_t length)
{
	return ics_llunack_send(node, type, message, length);
}


/*
 * Register ICS components with CI.
 */
/* ICS Priority Channels */
ics_chan_t ics_min_prio_chan;
ics_chan_t ics_max_prio_chan;
/* ICS Reply Channels (lower-level channels) */
ics_chan_t ics_reply_chan;
ics_chan_t ics_reply_prio_chan;

static int __init ci_init_ics(void)
{
	int ret;
	int i;

	/* Register ICS Priority Channels */
	/* XXX: The ICS channel numbers are hard-coded to ensure consistency
	 * among cluster nodes. This should go away once dynamic channels
	 * are implemented. In the meantime, be sure to update this when
	 * channels are added/deleted. */
	ret = register_ics_channel(&ics_min_prio_chan, 0);
	if (ret < 0)
		return ret;
	for (i = 1; i < ICS_MAX_PRIO-1; i++) {
		ret = register_ics_channel(NULL, -1);
		if (ret < 0)
			return ret;
	}
	ret = register_ics_channel(&ics_max_prio_chan, -1);
	if (ret < 0)
		return ret;

	/* Register ICS Reply Channels */
	ret = register_ics_channel(&ics_reply_chan, -1);
	if (ret < 0)
		return ret;
	ret = register_ics_channel(&ics_reply_prio_chan, -1);
	return ret;
}

/* XXX: There is currently no option to compile this as a module. */
module_init(ci_init_ics)


#if defined(DEBUG) || defined(DEBUG_TOOLS)

void
print_icsnodestatus(void)
{
	int i, j;
	struct flags {
		int flag;
		char *name;
	} icsflags[] = {
		{ ICS_NODE_NEVER_UP, "never up" },
		{ ICS_NODE_GOING_DOWN, "going down" },
		{ ICS_NODE_DOWN, "down" },
		{ ICS_NODE_SETICSINFO_DONE, "seticsinfo done" },
		{ ICS_NODE_COMING_UP, "coming up" },
		{ ICS_NODE_UP, "up" },
		{ ICS_NODE_READY, "ready" },
		{ ICS_NODE_INVALID, "invalid" }
	}, *flg;

	printk(KERN_DEBUG "ICS node status:\n");
	for (i = 0; i < NSC_MAX_NODE_VALUE; i++) {
		j = (int)ics_nodestatus[i];
		flg = icsflags;
		if (j) {
			for (; !(flg->flag & ICS_NODE_INVALID); flg++) {
				if (flg->flag & j)
					break;
			}
		} //else ICS_NODE_NEVER_UP
		printk(KERN_DEBUG "\tnode %d, status %x (%s)\n", i, j, flg->name);
	}
}

#endif /* DEBUG || DEBUG_TOOLS */

#ifdef NSC_ICSLOG
/*
 * Format strings for nsc_log output
 *	- pointers to low-level ics strings should be added when llics
 *	  initializes itseld.
 *	- first arg for all strings is the timestamp
 */
const char *icslog_format[ICSLOG_NUM_MSGS] = {
	"%lx-svrFunc: hdl 0x%lx id 0x%lx/0x%04lx n %ld op 0x%lx pid %ld\n",
	"%lx-svrRply: hdl 0x%lx id 0x%lx/0x%04lx n %ld ev 0x%lx pid %ld\n",
	"%lx-svrClbk: hdl 0x%lx id 0x%lx/0x%04lx n %ld ev 0x%lx pid %ld\n",
	"%lx-cliSend: hdl 0x%lx/0x%04lx n %ld s/p 0x%lx/%ld pid %ld\n",
	"%lx-cliClbk: hdl 0x%lx/0x%04lx n %ld s/p 0x%lx/%ld pid %ld\n",
	"%lx-cliWkup: hdl 0x%lx/0x%04lx n %ld s/p 0x%lx/%ld intr %ld\n",
	"%lx-svrNcbk: hdl 0x%lx/0x%04lx id 0x%lx n %ld chan %ld prio %ld\n",
	"%lx-svrDown: hdl 0x%lx/0x%04lx id 0x%lx n %ld pp 0x%lx\n",
};

int ics_uniqueid;

int icslog;

void
ics_log(
	int level,
	long msgnum,
	long a,
	long b,
	long c,
	long d,
	long e,
	long f)
{
	nsc_log(icslog_cookie, msgnum, jiffies, a, b, c, d, e, f);
}

void
print_icslog(void)
{
	nsc_log_print(icslog_cookie, icslog_format);
}

void
print_icslog_n(int count)
{
	nsc_log_print_n(icslog_cookie, icslog_format, count);
}

void
print_icslog_find(long tag)
{
	nsc_log_search(icslog_cookie, icslog_format, tag);
}

#endif /* NSC_ICSLOG */
