/*
 * 	ICS channels/services configuration file.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <cluster/ics.h>

/*
 * Interface code for registering ICS channels.
 */
typedef struct ics_channel {
	int chan_num_services;
	/* Add more fields in future for true dynamic channels */
} ics_channel_t;

ics_channel_t		*ics_channels[ICS_MAX_CHANNELS];

int 			ics_num_channels = 0;
int			cluster_num_services = 0;

struct ics_svc_init_routine {
	struct ics_svc_init_routine *next;
	int (*ics_svc_init)(void);
} *ics_svc_init_routines = NULL;

/*
 * register_ics_channel()
 * 	Register an ICS channel.
 *
 * Arguments:
 * 	ics_chan	- pointer to ICS channel identifier.
 * 			  (NULL is valid);
 * 	req_num		- specific channel number to use (will become
 * 			  obsolete in the future when we enable true dynamic
 * 			  channels). Use -1 for dynamic assignment.
 *
 * Returns 0 on success, negative value on error.
 */
int
register_ics_channel(ics_chan_t *ics_chan, int req_num)
{
	int i;

	if (ics_num_channels >= ICS_MAX_CHANNELS ||
	    req_num >= ICS_MAX_CHANNELS)
		return -EINVAL;

	if (req_num >= 0) {
		if (ics_channels[req_num] != NULL)
			return -EEXIST;
		else {
			ics_channels[req_num] = (ics_channel_t *)
					kmalloc_nofail(sizeof(ics_channel_t));
			ics_channels[req_num]->chan_num_services = 0;
			ics_num_channels++;
			if (ics_chan)
				*ics_chan = req_num;
			return 0;
		}
	}
	else if (req_num == -1) {
		for (i = 0; i < ICS_MAX_CHANNELS; i++) {
			if (ics_channels[i] == NULL)
				break;
		}
		SSI_ASSERT(i < ICS_MAX_CHANNELS);

		ics_channels[i] = (ics_channel_t *)
					kmalloc_nofail(sizeof(ics_channel_t));
		ics_channels[i]->chan_num_services = 0;
		ics_num_channels++;
		if (ics_chan)
			*ics_chan = i;
		return 0;
	}
	return -EINVAL;
}

/*
 * register_cluster_svc()
 * 	Register a cluster service. Encode cluster service using next available
 * 	subservice in channel chan_num.
 *
 * Arguments:
 * 	cluster_svc	- pointer to cluster service identifier.
 * 	chan_num	- channel over which cluster service should be layered.
 * 	ics_svc_init	- pointer to function to register cluster service
 * 			  with respect to ICS.
 *
 * Returns 0 on success, negative value on error.
 */
int
register_cluster_svc(cluster_svc_t *cluster_svc,
		     ics_chan_t chan_num,
		     int (*ics_svc_init)(void))
{
	cluster_svc_t subservice;
	struct ics_svc_init_routine *init_routine;
static DECLARE_MUTEX(reg_clu_svc_lock);

	down(&reg_clu_svc_lock);

	/* Sanity checks */
	if ((chan_num < 0) || (chan_num >= ICS_MAX_CHANNELS) ||
	    (ics_channels[chan_num] == NULL) ||
	    (ics_channels[chan_num]->chan_num_services >= ICS_MAX_SUBSERVICES)
	    || (ics_svc_init == NULL) || (cluster_svc == NULL))
	{
		up(&reg_clu_svc_lock);
		return -EINVAL;
	}

	subservice = ics_channels[chan_num]->chan_num_services++;
	*cluster_svc = ICS_NSC_SVC(chan_num, subservice);
	cluster_num_services++;

	init_routine = (struct ics_svc_init_routine *)
			kmalloc_nofail(sizeof(struct ics_svc_init_routine));
	init_routine->next = ics_svc_init_routines;
	init_routine->ics_svc_init = ics_svc_init;
	ics_svc_init_routines = init_routine;

	up(&reg_clu_svc_lock);

	return 0;
}

/*
 * ics_cluster_services_init()
 * 	Register all cluster services with ICS.
 */
void
ics_cluster_services_init(void)
{
	struct ics_svc_init_routine *init_routine = ics_svc_init_routines;

	while (init_routine != NULL) {
		init_routine->ics_svc_init();
		init_routine = init_routine->next;
	}
}
