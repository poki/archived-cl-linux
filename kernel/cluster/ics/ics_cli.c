/*
 * 	ICS client-side code.
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	Portions Copyright 2007 Roger Tsang <roger.tsang@gmail.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 * High-level ICS client-side code.
 */

#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <cluster/nsc.h>
#include <cluster/config.h>
#include <cluster/synch.h>
#include <cluster/ics/ics_private.h>
#include <cluster/ics/ics_sig.h>
#include <cluster/ics.h>	/* Must be after <ics_private.h> */
#include <cluster/ics_proto.h>	/* Must be after <ics_private.h> */

#include <cluster/gen/ics_icssig_macros_gen.h>
#include <cluster/gen/ics_icssig_protos_gen.h>


#ifdef	DEBUG
/*
 * For debugging, setting ics_no_sig_forward to non-zero will turn off
 * signal forwarding.
 */
int ics_no_sig_forward;
#endif	/* DEBUG */

#ifdef		DEBUG
/*
 * Variable for debugging.
 */
int		icscli_maxprio;
#endif

#ifdef SSI_NOTUSED
int		icscli_flowpages_lwm = DEFAULT_FREEPAGES_LWM;
int		icscli_flowpages_hwm = DEFAULT_FREEPAGES_HWM;
#endif

#ifdef ICS_CLI_HANDLE_KMEM_CACHE
static kmem_cache_t *cli_handle_cachep;
#endif

/*
 * icscli_init()
 *	Perform relevant initialization for the client code.
 */
void
icscli_init()
{
	int		i;

	/*
	 * Initialize array tracking client handles being processed.
	 */
	for (i=0; i < CLIHAND_ARRAYSZ; i++) {
		INIT_SPIN_LOCK(&clihand_list[i].chlist_lock);
	}

#ifdef ICS_CLI_HANDLE_KMEM_CACHE
	cli_handle_cachep = kmem_cache_create("ics_cli_handle",
				sizeof(cli_handle_t), 0,
				SLAB_HWCACHE_ALIGN|SLAB_RECLAIM_ACCOUNT|SLAB_PANIC,
				NULL, NULL);
#endif

	/*
	 * Call the ics client low level initialization code.
	 */
	icscli_llinit();
}

/*
 * icscli_handle_get()
 *	Get a client handle for use in further calls to ICS client code
 *
 * Description:
 *	This routine is called by ICSGEN generated stubs (and possibly by other
 *	NSC components) to obtain a client handle for subsequent calls to
 *	icscli_*() routines.
 *
 *	If sleep_flag is TRUE, this routine must be called from thread context,
 *	otherwise it may be called from interrupt context.
 *
 * Parameters:
 *	node		the node to which the message is destined
 *	service		NSC service the handle is to be used for
 *	sleep_flag	if TRUE, this routine can sleep waiting for resources
 *
 * Return value:
 *	Pointer to a client handle. This routine will return NULL if sleep_flag
 *	is FALSE and there are insufficient resources/memory to get the handle.
 */
cli_handle_t *
icscli_handle_get(
	clusternode_t	node,
	int		service,
	int		flags)
{
	cli_handle_t	*handle;
	int		sleep_flag;
	int		error;

	if (node == this_node) {
		panic("icscli_handle_get: node sending message to itself!\n");
	}

	sleep_flag = !(flags & ICS_NO_BLOCK);

	/*
	 * Allocate a client handle structure for use by this operation. For
	 * now, simply allocate some kernel memory and don't worry about flow
	 * control.
	 */
	if (sleep_flag)
#ifdef ICS_CLI_HANDLE_KMEM_CACHE
		handle = kmem_cache_alloc(cli_handle_cachep, GFP_KERNEL|__GFP_NOFAIL);
	else
		handle = kmem_cache_alloc(cli_handle_cachep, GFP_ATOMIC);
#else
		handle = kmalloc_nofail(sizeof(cli_handle_t));
	else
		handle = kmalloc(sizeof(cli_handle_t), GFP_ATOMIC);
#endif
	SSI_ASSERT(handle != NULL || !sleep_flag);
	if (handle == NULL)
		return(NULL);

	/*
	 * Store the node and service for the outgoing message.
	 */
	handle->ch_service = service;
	handle->ch_node = node;
	handle->ch_status = 0;

	handle->ch_flag = 0;
	if (flags & ICS_NO_BLOCK)
		handle->ch_flag |= CLI_HANDLE_NO_BLOCK;
	if (flags & ICS_NO_REPLY)
		handle->ch_flag |= CLI_HANDLE_NO_REPLY;

	SSI_ASSERT(ICS_CHAN(service) >= 0 && ICS_CHAN(service) < ics_num_channels);
	SSI_ASSERT(ICS_SSVC(service) >= 0 && ICS_SSVC(service)<ICS_MAX_SUBSERVICES);

	/*
	 * Assign a transaction ID to be used for the duration of the
	 * interaction. The transaction ID is used for identification of
	 * the handle when a reply arrives back on the server, and for
	 * identification of the handle on the server when it comes to signal
	 * forwarding from the client. As long as signal forwarding is done
	 * with RPCs and not IPCs, then using the client handle address as
	 * the transaction ID is good.
	 */
	handle->ch_transid = (long) handle;

	/* Initialize linked list of handles */
	handle->ch_next = handle->ch_prev = NULL;

	/*
	 * Initialize the low-level ICS portion of the handle. If this fails
	 * then back out of everything we've done.
	 */
	error = icscli_llhandle_init(handle, sleep_flag);
	if (error != 0) {
		SSI_ASSERT(!sleep_flag);
#ifdef ICS_CLI_HANDLE_KMEM_CACHE
		kmem_cache_free(cli_handle_cachep, handle);
#else
		kfree((caddr_t)handle);
#endif
		return(NULL);
	}

	/* Intitialize the event portion of the handle. */
	INIT_INTR_EVENT(&handle->ch_event);

	/* Initialize the proc pointer of client process for this handle. */
	handle->ch_procp = current;

#ifdef NSC_INHERIT_USER_NICE
	handle->ch_user_nice = TASK_NICE(current);
#endif

	/* Keep statistics. */
	LOCK_SPIN_LOCK(&icsstat_lock);
#ifdef NSC_ICSLOG
	/* Borrow statistics lock for this. */
	handle->ch_uniqueid = ++ics_uniqueid & 0xFFFF;
#endif
	icsstat.istat_clihand++;
	UNLOCK_SPIN_LOCK(&icsstat_lock);

	return(handle);
}

/*
 * icscli_send()
 *	Send a message from the client to the server
 *
 * Description:
 *	After obtaining a handle and performing the appropriate argument
 *	marshalling, icscli_send() is used to send the message from the
 *	client to the server.
 *
 *	The callback routine is called (by the underlying transport) once
 *	the message has been sent (either successfully or not) to the server,
 *	and (for message/response interactions) the response has been
 *	received. Once the callback routine has been called (or during the
 *	callback routine), output parameters may be decoded (i.e. calls to
 *	icscli_decode_*() can be performed) and the handle can be
 *	released.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the client handle associated with the message
 *	procnum		a "magic cookie" that is typically used as
 *			an indicator of what type of request is being passed
 *			to the server; interpreted by the service; note that
 *			only the bottom 16 bits are used
 *	callback	routine that is called once the message has been
 *			sent (for ICS_NO_REPLY) or once the response for the
 *			message has been received; first parameter to the
 *			callback routine is the handle; the second
 *			parameter is the callarg argument (from below)
 *	callarg		argument provided solely to be supplied as the
 *			fourth argument to the callback routine when the
 *			callback routine is called
 *
 * Return Value:
 *      -EAGAIN
 *      -EREMOTE
 *      0
 */
int
icscli_send(
	cli_handle_t	*handle,
	int		procnum,
	void		(*callback)(
				cli_handle_t	*handle,
				long		callarg),
	long		callarg)
{
	clihand_list_t 	*chl_p = &clihand_list[CLIHAND_HASH(handle->ch_node)];
	int		normalchan = ICS_CHAN(handle->ch_service);
	int		ssvc = ICS_SSVC(handle->ch_service);
	int		prio = ics_getpriority();
	int		nodestatus;
	int		error;

	extern cluster_svc_t cluster_clms_svc;

	/*
	 * Arrange for the callback routine to be called with the correct
	 * parameters.
	 */
	handle->ch_procnum = procnum;
	handle->ch_callback = callback;
	handle->ch_callarg = callarg;
	SSI_ASSERT(procnum >= 0 && procnum < icsservice[normalchan].
					 icssvc_ops_cnt[ssvc]);
	SSI_ASSERT(callback != NULL);
	SSI_ASSERT(icsstat.istat_clisend_proc[normalchan][ssvc] !=NULL);

	/*
	 * Calculate the communication channel that the low-level ICS should
	 * use.
	 */
	if (prio > 0)
		handle->ch_chan = ics_min_prio_chan + prio - 1;
	else
		handle->ch_chan = ICS_CHAN(handle->ch_service);
#if	defined(DEBUG)
	if (prio > icscli_maxprio)
		icscli_maxprio = prio;
#endif	/* DEBUG */
	if (prio >= ICS_NUM_PRIO)
		panic("icscli_send: priority too high (too many hops), "
		      "service=%d, priority=%d",
		      handle->ch_service,
		      prio);

	/*
	 * If this is an RPC, then queue up the message.
	 */
	if ((handle->ch_flag & CLI_HANDLE_NO_REPLY) == 0) {
		handle->ch_flag |= CLI_HANDLE_RPC_SENT;
		LOCK_SPIN_LOCK(&chl_p->chlist_lock);
		if (chl_p->chlist_first != NULL)
			chl_p->chlist_first->ch_prev = handle;
		handle->ch_next = chl_p->chlist_first;
		handle->ch_prev = NULL;
		chl_p->chlist_first = handle;
		UNLOCK_SPIN_LOCK(&chl_p->chlist_lock);
	}

	/*
	 * Make sure the node status allows us to send the message. If we
	 * aren't allowed, then set ch_status to the correct value.
	 * icscli_llsend() will check this value and perform an immediate
	 * callback when it spots it.
	 */

check_status:
	if (handle->ch_node > 0 && handle->ch_node <= NSC_MAX_NODE_VALUE) {
		LOCK_COND_LOCK(&ics_nodestatus_lock);
		nodestatus = ics_nodestatus[handle->ch_node];
		UNLOCK_COND_LOCK(&ics_nodestatus_lock);
		if (nodestatus & (ICS_NODE_DOWN
				  | ICS_NODE_GOING_DOWN
				  | ICS_NODE_NEVER_UP)) {
			handle->ch_status = -EREMOTE;
		} else if (handle->ch_service != cluster_clms_svc &&
			   (nodestatus & (ICS_NODE_COMING_UP
					  | ICS_NODE_SETICSINFO_DONE
					  | ICS_NODE_UP))) {

			if (handle->ch_flag & CLI_HANDLE_NO_BLOCK) {
				/*
				 * This is a non-blocking message send.
				 * We can't delay by sleeping. Instead we
				 * return -EREMOTE. -EAGAIN would probably be
				 * more appropriate, but we can't return it
				 * because it is assumed that mblk input
				 * parameters are unfreed when -EAGAIN
				 * is returned.  Since at this point we've
				 * already marshalled parameters, we can't
				 * make that guarantee anymore.
				 */
				handle->ch_status = -EREMOTE;
			}
			else {
				/*
				 * If the node is "coming up", wait for it to
				 * come fully up to ensure the connection is
				 * set up. Go recheck status after delaying in
				 * case the node transitioned. If this is a
				 * CLMS message, we know the connection is
				 * already set up, so we don't need to wait.
				 */
#ifdef DEBUG
				printk(KERN_DEBUG
				       "icscli_send: delaying send to node %d"
				       " chan %d ssvc %d proc %d\n",
				       (int) handle->ch_node, normalchan, ssvc,
				       procnum);
#endif /* DEBUG */
				idelay(ICS_STATUS_RETRY_DELAY);
				goto check_status;
			}
		}
	}
	else
		handle->ch_status = -EREMOTE;

	/*
	 * Gather statistics.
	 */
	if ((handle->ch_flag & CLI_HANDLE_NO_REPLY) != 0)
		icsstat.istat_climsg++;
	else
		icsstat.istat_clirpc++;
	icsstat.istat_clisend[prio][normalchan]++;
	icsstat.istat_clisend_proc[normalchan][ssvc][procnum]++;
#ifdef NSC_ICSLOG
	if (do_icslog) {
		ics_log(1,
			ICSLOG_CLISEND,
			(long)handle, handle->ch_uniqueid,
			handle->ch_node,
			handle->ch_service, handle->ch_procnum,
			handle->ch_procp->pid);
	}
#endif /* NSC_ICSLOG */

	/*
	 * Call the low-level code to perform the actual send.
	 */
	error = icscli_llsend(handle);

	return error;
}

/*
 * icscli_wait_callback()
 *	Available callback routine for icscli_send().
 *
 * Description:
 *	This is a callback routine for icscli_send() that is available for
 *	the programmer to use (but its use is not required). It is used
 *	in the typical case for RPCs where the programmer wishes to call
 *	icscli_wait() to wait for the RPC reply.
 *
 *	The routine is provided as a convenience to the programmer, and
 *	it is not required that this routine be used as a callback to
 *	icscli_send().
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	See the parameters to the callback of icscli_send().
 */
void
icscli_wait_callback(
	cli_handle_t	*handle,
	long		callarg)
{
#ifdef NSC_ICSLOG
	if (do_icslog) {
		ics_log(1,
			ICSLOG_CLICLBK,
			(long)handle, handle->ch_uniqueid,
			handle->ch_node,
			handle->ch_service, handle->ch_procnum,
			handle->ch_procp->pid);
	}
#endif /* NSC_ICSLOG */

	/* Signal the event which icscli_wait() will use. */
	SIGNAL_INTR_EVENT(&handle->ch_event);
}

/*
 * icscli_find_transid_handle()
 *	Find the client handle associated with a specific transaction ID.
 *
 * Description:
 *	Find the client handle associated with a transaction ID. Transaction
 *	ID's are assigned during icscli_handle_get() and are passed to the
 *	server via icscli_send(), and are then returned in the server's
 *	reply.
 *
 *	This routine is used to associate the reply with the handle that
 *	sent the message.
 *
 *	The current transaction ID scheme is very simple (transaction ID is
 *	equal to the handle address). This may have to change as we start
 *	dealing with nodeup/nodedown and interrupting servers with signals.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	transid		the transaction ID
 *
 * Return value:
 *	Handle associated with the transaction ID.
 */
cli_handle_t *
icscli_find_transid_handle(
	long		transid)
{
	SSI_ASSERT(transid == ((cli_handle_t *)transid)->ch_transid);
	return((cli_handle_t *)transid);
}

/*
 * Routine for *reliably* sending a SIGKILL to the server proc. Deals
 * with any races. When it returns, either the signal has been delivered,
 * the RPC has returned, or the server node has gone down.
 */

static int
icscli_do_signal_forward(cli_handle_t *hp)
{
	int error;
	int rerror;

	for (;;) {
		error = RICS_SIG_FORWARD(hp->ch_node, &rerror,
					 this_node, hp->ch_transid);
		if (error == -EAGAIN) {
			nidelay(HZ/10);
			continue;
		}
		if (error >= 0)
			error = rerror;
		break;
	}

	return error;
}

static int
icscli_signal_forward(cli_handle_t *hp)
{
	int retval = 0;
	int error;

	for (;;) {
		error = icscli_do_signal_forward(hp);
		if (error == 0 || error == -EREMOTE)
			break;
		if (error != -ENOENT)
			printk(KERN_WARNING
			       "%s:handle 0x%p node %u unexpected error %d\n",
			       __FUNCTION__, hp, hp->ch_node, error);
		retval = TRY_INTR_EVENT(&hp->ch_event);
		if (retval)
			break;
		nidelay(HZ/10);
	}

	return retval;
}

/*
 * icscli_wait()
 * 	Wait for a reply to an RPC.
 *
 * Description:
 *	Wait for either a reply to an ICS RPC to arrive. If signal
 *	forwarding is enabled for this RPC, then also forward the signal
 *	to the server.
 *
 *	This routine must be called from process context.
 *
 * Parameters:
 *	handle		handle corresponding to the RPC
 *	flag		ICS_NO_SIG_FORWARD if no signal forwarding
 *
 * Return value:
 *	Status of RPC.
 */
int
icscli_wait(
	cli_handle_t	*handle,
	int		flag)
{
	boolean_t	intr_flag;
	int		intr;
	int		oldprio;
	clihand_list_t 	*chl_p = &clihand_list[CLIHAND_HASH(handle->ch_node)];

	/*
	 * Figure out whether signal forwarding is to take place.
	 */
	SSI_ASSERT((flag & ICS_NO_SIG_FORWARD) == 0 ||
	       (handle->ch_flag & CLI_HANDLE_NO_REPLY) == 0);
	intr_flag = ((flag & ICS_NO_SIG_FORWARD) == 0);

#ifdef DEBUG
	if (ics_no_sig_forward)
		intr_flag = FALSE;
#endif /* DEBUG */

	/*
	 * Make sure that the ICS priority for any signals forwarded
	 * is > 0.
	 */
	oldprio = ics_getpriority();
	if (oldprio == 0)
		ics_setpriority(1);

	/*
	 * Loop waiting until we get a reply from the server.
	 */
	for (;;) {

		/*
		 * Wait for either a signal to arrive or for a reply to
		 * arrive from the server. If a reply has arrived, then
		 * quit the loop.
		 */
		WAIT_INTR_EVENT(&handle->ch_event, intr_flag, &intr);
#ifdef NSC_ICSLOG
		if (do_icslog) {
			ics_log(1,
				ICSLOG_CLIWKUP,
				(long)handle, handle->ch_uniqueid,
				handle->ch_node,
				handle->ch_service, handle->ch_procnum,
				intr);
		}
#endif /* NSC_ICSLOG */
		if (!intr)
			break;
		icsstat.istat_clisigforward++;

		/* A signal was received; kill server process. */
		if (icscli_signal_forward(handle))
			break;

		/* Sleep for RPC completion uninterruptibly. */
		intr_flag = FALSE;
	}

	/*
	 * If we changed the ICS priority, then put it back.
	 */
	if (oldprio == 0)
		ics_setpriority(0);

	/*
	 * Remove the handle from the list of client handles.
	 */
	LOCK_SPIN_LOCK(&chl_p->chlist_lock);
	if (handle->ch_prev != NULL)
		handle->ch_prev->ch_next = handle->ch_next;
	else
		chl_p->chlist_first = handle->ch_next;
	if (handle->ch_next != NULL)
		handle->ch_next->ch_prev = handle->ch_prev;
	UNLOCK_SPIN_LOCK(&chl_p->chlist_lock);

	return(handle->ch_status);
}

/*
 * icscli_get_status()
 * 	Get the status for a completed RPC or IPC.
 *
 * Description:
 *	This routine can be called some time after the callback routine
 *	to icscli_send() is invoked to get the status of the RPC or
 *	IPC. It is used when the client does not wish to use the
 *	icscli_wait_callback() and icscli_wait() routines.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle corresponding to the RPC/IPC.
 *
 * Return value:
 *	Status of RPC.
 */
int
icscli_get_status(
	cli_handle_t	*handle)
{
	return(handle->ch_status);
}

/*
 * icscli_sendup_reply()
 *	Tell ICS high-level code that reply has arrived.
 *
 * Description:
 *	This routine is called by the ICS low-level code when a reply
 *	arrives on the client node so that the client-specified callback
 *	can be performed.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		the handle whose reply has arrived
 */
void
icscli_sendup_reply(
	cli_handle_t	*handle)
{
	/*
	 * Set the flag saying the callback is about to occur (or has
	 * occurred (note that no lock is required because multiple contexts
	 * can't screw it up). This flag is used for signal forwarding
	 * to be able to see that sending a signal to the destination node
	 * is futile because the reply has arrived.
	 */
	handle->ch_flag |= CLI_HANDLE_REPLY_WAITING;

	/* Perform the callback specified by the caller of icscli_send(). */
	(*handle->ch_callback)(handle, handle->ch_callarg);
}

/*
 * icscli_handle_release()
 *	Release a client handle, since we have completed our operation.
 *
 * Description:
 *	This routine is called by ICS high level code (and possibly by other
 *	NSC components) to release a client handle. No subsequent
 *	icscli_*() routines may be called with this handle.
 *
 *	For plain messages, this routine should not be called until the
 *	callback routine for icscli_send() has been called (it may be called
 *	either as or during the callback routine for icscli_send().
 *
 *	For message/response pairs, this routine should not be called
 *	until all output parameters in the response have been decoded.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	handle		handle to be released
 *
 * Return value:
 */
void
icscli_handle_release(
	cli_handle_t	*handle)
{
	/* Keep statistics. */
	LOCK_SPIN_LOCK(&icsstat_lock);
	icsstat.istat_clihand--;
	UNLOCK_SPIN_LOCK(&icsstat_lock);

	/*
	 * Perform any deinitialization necessary for low-level code.
	 */
	icscli_llhandle_deinit(handle);

	/*
	 * Deinitialize the event associated with the handle.
	 */
	DEINIT_INTR_EVENT(&handle->ch_event);

	/* Free the handle. */
#ifdef ICS_CLI_HANDLE_KMEM_CACHE
	kmem_cache_free(cli_handle_cachep, handle);
#else
	kfree(handle);
#endif
}

/*
 * icscli_handle_release_callback()
 *	Identical to icscli_handle_release, except can be used as a callback.
 *
 * Description:
 *	This routine is identical to icscli_handle_release(), except that
 *	it has the extra parameters that are associated with an ICS client
 *	callback routine to icscli_send() (these parameters are ignored).
 *
 *	This routine is provided so that the ICS clients can release a
 *	handle directly using the icscli_send() callback routine, and not
 *	suffer compilation errors.
 *
 *	This routine is typically used as the callback routine for ICS
 *	messages.
 *
 *	This routine may be called from interrupt context.
 *
 * Parameters:
 *	See the parameters to the callback of icscli_send().
 */
void
icscli_handle_release_callback(
	cli_handle_t	*handle,
	long		callarg)
{
	icscli_handle_release(handle);
}

int
icscli_max_ool_data_size(void)
{
	return ICS_MAX_OOL_DATA_SIZE;
}

clusternode_t
icscli_getnode(
	cli_handle_t	*handle)
{
	return handle->ch_node;
}

int
icscli_async_sent(
	cli_handle_t	**hpp)
{
	if (*hpp != NULL) {
		SSI_ASSERT(((*hpp)->ch_flag & CLI_HANDLE_NO_REPLY) == 0);
		if ((*hpp)->ch_flag & CLI_HANDLE_RPC_SENT)
			return 1;
		else {
			icscli_handle_release(*hpp);
			*hpp = NULL;
		}
	}

	return 0;
}

#if	defined(DEBUG) || defined(DEBUG_TOOLS)
/*
 * print_icscli()
 *	Print information about active client processes.
 *
 * Description:
 *	This is a routine that is callable from the kernel debugger that prints
 *	information about active client processes.
 *
 * Parameters:
 *	None.
 */
void
print_icscli(void)
{
	int		i;
	cli_handle_t	*handle;

	printk(KERN_DEBUG "List of active client procs:\n");
	for (i=0; i < CLIHAND_ARRAYSZ; i++) {
		handle = clihand_list[i].chlist_first;
		while (handle != NULL) {
			if (handle->ch_procp != NULL) {
				printk(KERN_DEBUG "handle 0x%p, "
					"transid 0x%lx, pid %d, node %u\n",
					handle,
					handle->ch_transid,
					handle->ch_procp->pid,
					handle->ch_node);
			}
			else {
				printk(KERN_DEBUG "<ch_procp==NULL; index=%d,"
				       " chlist_first=0x%p>\n", i, handle);
			}
			handle = handle->ch_next;
		}
	}
}

void
print_icscli_handle(cli_handle_t *cli)
{
	printk(KERN_DEBUG "ch_flag %d \n", cli->ch_flag);
	printk(KERN_DEBUG "ch_service %d \n", cli->ch_service);
	printk(KERN_DEBUG "ch_node %u \n", cli->ch_node);
	printk(KERN_DEBUG "ch_procnum %d \n", cli->ch_procnum);
	printk(KERN_DEBUG "ch_callarg 0x%lx \n", cli->ch_callarg);
	printk(KERN_DEBUG "ch_status %d \n", cli->ch_status);
#ifdef NSC_ICSLOG
	printk(KERN_DEBUG "ch_transid 0x%lx ch_uniqueid 0x%04x \n",
	       cli->ch_transid, cli->ch_uniqueid);
#else
	printk(KERN_DEBUG "ch_transid 0x%lx \n", cli->ch_transid);
#endif
	printk(KERN_DEBUG "ch_chan %d \n", cli->ch_chan);
	printk(KERN_DEBUG "pid %d\n",
	       (cli->ch_procp != NULL ? cli->ch_procp->pid : 0));

	print_icscli_llhandle(&cli->ch_llhandle);
}

#endif	/* DEBUG || DEBUG_TOOLS */
