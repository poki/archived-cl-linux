/*
 * 	Platform dependent register definitions.
 *	Copyright 2002 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#ifndef _CLUSTER_IA64_ARCH_REGS_H
#define _CLUSTER_IA64_ARCH_REGS_H

#include <linux/ptrace.h>

/* Full register context on stack for process. */
struct arch_regs {
	struct switch_stack arch_switch_stack;
	struct pt_regs arch_pt_regs;
};

/*
 * The extra fields here are set by rproc_arch_data_load_msg() and
 * used by ret_from_rproc().
 */
struct arch_regs_stack {
	unsigned long arch_stack_offset;
	struct arch_regs *arch_regs;
	unsigned long *arch_rbs;
	unsigned long arch_rbs_len;
};

/* Get pt_regs from arch_regs_stack. */
#define ARCH_PT_REGS(_regs)	(&(_regs)->arch_regs->arch_pt_regs)

extern long
do_sys32_ptrace(int, pid_t, unsigned int, unsigned int, struct pt_regs *);

extern long
do_sys_ptrace(long, pid_t, unsigned long, unsigned long, struct pt_regs *);

extern asmlinkage void ret_from_rproc(struct arch_regs_stack *, int)
	 __asm__("ret_from_rproc");

#endif /* _CLUSTER_IA64_ARCH_REGS_H */
