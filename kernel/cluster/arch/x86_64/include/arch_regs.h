/*
 * 	Platform dependent register definitions.
 *	Copyright 2002 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 * x86_64
 */

#ifndef _CLUSTER_X86_64_ARCH_REGS_H
#define _CLUSTER_X86_64_ARCH_REGS_H

#include <linux/ptrace.h>

/* Full register context on stack for process. */
struct arch_regs_stack {
	struct pt_regs arch_pt_regs;
};

/* Get pt_regs from arch_regs_stack. */
#define ARCH_PT_REGS(_regs)	(&(_regs)->arch_pt_regs)

/* The following macros expect a pointer to the pt_regs. */
#define	SYSCALL_ARG1(_regs)	((_regs)->rdi)
#define	SYSCALL_ARG2(_regs)	((_regs)->rsi)
#define	SYSCALL_ARG3(_regs)	((_regs)->rdx)
#define	SYSCALL_ARG4(_regs)	((_regs)->r10)
#define	SYSCALL_ARG5(_regs)	((_regs)->r8)
#define	SYSCALL_ARG6(_regs)	((_regs)->r9)

extern asmlinkage long sys_ptrace(long, long, unsigned long, long);

extern asmlinkage void ret_from_rproc(struct arch_regs_stack *, int)
	 __asm__("ret_from_rproc");

extern void ret_from_rexec(struct pt_regs *);

#endif /* _CLUSTER_X86_64_ARCH_REGS_H */
