#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <cluster/nsc.h>
#include <cluster/clms.h>
#include <cluster/nodelist.h>
#include <cluster/procfs.h>
#include <cluster/node_monitor.h>

struct proc_dir_entry *proc_cluster;

struct fasync_struct *proc_cluster_events_fasync_list;

static int proc_cluster_events_fasync(int fd, struct file *fp, int on)
{
	return fasync_helper(fd, fp, on, &proc_cluster_events_fasync_list);
}

static int proc_cluster_events_release(struct inode *ip, struct file *fp)
{
	(void)proc_cluster_events_fasync(-1, fp, 0);

	return 0;
}

static struct file_operations  proc_cluster_events_fops = {
	fasync:		proc_cluster_events_fasync,
	release:	proc_cluster_events_release,
};

/*
 * Called on initialization to set up the /proc/cluster subtree
 */
void
proc_cluster_init(void)
{
	struct proc_dir_entry *ep;

	proc_cluster = proc_mkdir("cluster", 0);
	ep = create_proc_entry("events",
			       S_IFREG|S_IRUSR|S_IRGRP|S_IROTH,
			       proc_cluster);
	if (ep)
		ep->proc_fops = &proc_cluster_events_fops;

	ep = create_proc_entry("clms_masterlist",
			       S_IFREG|S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH,
			       proc_cluster);
	if (ep)
		ep->proc_fops = &proc_clms_masterlist_operations;

	ep = create_proc_entry("nm_rate",
			       S_IFREG|S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH,
			       proc_cluster);
	if (ep)
		ep->proc_fops = &proc_nm_rate_operations;

	ep = create_proc_entry("nm_nodedown_disabled",
			       S_IFREG|S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH,
			       proc_cluster);
	if (ep)
		ep->proc_fops = &proc_nm_nodedown_disabled_operations;

	ep = create_proc_entry("nm_log_threshold",
			       S_IFREG|S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH,
			       proc_cluster);
	if (ep)
		ep->proc_fops = &proc_nm_log_threshold_operations;

	ep = create_proc_entry("icsstat",
			       S_IFREG|S_IRUSR|S_IRGRP|S_IROTH,
			       proc_cluster);
	if (ep)
		ep->proc_fops = &proc_icsstat_operations;
#ifdef CONFIG_SSI
	proc_cluster_ssi_init();
#endif
}
