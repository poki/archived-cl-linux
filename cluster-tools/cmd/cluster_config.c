/*
 *	cluster_config command
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <getopt.h>

#define CLUSTER_CONF	"/etc/cluster.conf"
#define MAXLINE		200

#define OPT_PREP		0x01
#define OPT_PREROOT		0x02
#define OPT_POSTROOT		0x04
#define OPT_INITPROC		0x08
#define OPT_CONFIGFILE		0x10

char line[MAXLINE];

char *config_file = CLUSTER_CONF;

struct option longopts[] = {
	{"prep", 0, 0, 'p'},
	{"preroot", 0, 0, 'e'},
	{"postroot", 0, 0, 'o'},
	{"initproc", 0, 0, 'i'},
	{"help", 0, 0, 'h'},
	{0, 0, 0, 0}
};

extern int cluster_set_config();
extern int cluster_init_preroot();
extern int cluster_init_postroot();
extern int cluster_initproc();

void usage(void)
{
	fprintf(stderr, "Usage: cluster_config [-peoi] [-f <config_file>]   Configure the cluster.\n");
	fprintf(stderr, " -p, --prep       Prepare cluster with options.\n");
	fprintf(stderr, " -e, --preroot    Run pre-root initialization.\n");
	fprintf(stderr, " -o, --postroot   Run post-root initialization.\n");
	fprintf(stderr, " -i, --initproc   Initialize init process (SSI).\n");
	fprintf(stderr, " -f <config_file> Used with -p option to specify\n");
	fprintf(stderr, "                  cluster configuration file.\n");
	fprintf(stderr, "                  Default is /etc/cluster.conf.\n");
	exit(1);
}

int prep(void)
{
	FILE *fp;
	int error = 0;
	int linenum = 1;
	int done = 0;
	
	fp = fopen(config_file, "r");
	if (fp == NULL) {
		fprintf(stderr, "cluster_config: could not open cluster initialization file '%s'\n", config_file);
		exit(1);
	}

	for(;;) {
		if (fgets(line, MAXLINE, fp) == NULL)
			break;
		error = cluster_set_config(line, MAXLINE);
		if (error < 0) {
			switch(errno) {
			case EINVAL:
				fprintf(stderr, "cluster_config: invalid config line: %d!\n", linenum);
				break;
			case EEXIST:
				fprintf(stderr, "cluster_config: cluster is already configured.\n");
				exit(1);
			case EPERM:
				fprintf(stderr, "cluster_config: can only be run by root.\n");
				exit(1);
			case ENOSYS:
				fprintf(stderr, "cluster_config: this kernel does not support clustering.\n");
				exit(1);
			default:
				fprintf(stderr, "cluster_config: unknown error %d in cluster prep.\n", errno);
				exit(1);
			}
		}
		if (error == 1)
			done = 1;
		linenum++;
	}

	if (!done) {
		fprintf(stderr, "cluster_config: config file missing some cluster parameters.\n");
		exit(1);
	}

	return error;
}

int preroot(void)
{
	int error;

	error = cluster_init_preroot();
	if (error < 0) {
		switch(errno) {
		case EEXIST:
			fprintf(stderr, "cluster_config: cluster has already run pre-root initialization.\n");
			break;
		case EAGAIN:
			fprintf(stderr, "cluster_config: cluster is not yet configured.\n");
			break;
		case EPERM:
			fprintf(stderr, "cluster_config: can only be run by root.\n");
			break;
		case ENOSYS:
			fprintf(stderr, "cluster_config: this kernel does not support clustering.\n");
			break;
		default:
			fprintf(stderr, "cluster_config: unknown error %d in cluster preroot.\n", errno);
			break;
		}
		exit(1);
	}
	return error;
}

int postroot(void)
{
	int error;

	error = cluster_init_postroot();
	if (error < 0) {
		switch(errno) {
		case EEXIST:
			fprintf(stderr, "cluster_config: cluster already running.\n");
			break;
		case EAGAIN:
			fprintf(stderr, "cluster_config: cluster hasn't run pre-root initialization yet.\n");
			break;
		case EPERM:
			fprintf(stderr, "cluster_config: can only be run by root.\n");
			break;
		case ENOSYS:
			fprintf(stderr, "cluster_config: this kernel does not support clustering.\n");
			break;
		default:
			fprintf(stderr, "cluster_config: unknown error %d in cluster postroot.\n", errno);
			break;
		}
		exit(1);
	}
	return error;
}

int initproc(void)
{
	int error;

	error = cluster_initproc();
	if (error < 0) {
		switch(errno) {
		case EEXIST:
			fprintf(stderr, "cluster_config: cluster already ran initproc.\n");
			break;
		case EAGAIN:
			fprintf(stderr, "cluster_config: cluster hasn't run postroot initialization yet.\n");
			break;
		case EPERM:
			fprintf(stderr, "cluster_config: can only be run by root.\n");
			break;
		case ENOSYS:
			fprintf(stderr, "cluster_config: this kernel does not support clustering.\n");
			break;
		default:
			fprintf(stderr, "cluster_config: unknown error %d in initproc.\n", errno);
			break;
		}
		exit(1);
	}
	return error;
}

int main(int argc, char *argv[])
{
	char val;
	int options = 0;

	while ( (val = getopt_long(argc, argv, "peoif:", longopts, 0)) != -1) {
		switch(val) {
			case 'p':
				options |= OPT_PREP;
				break;
			case 'e':
				options |= OPT_PREROOT;
				break;
			case 'o':
				options |= OPT_POSTROOT;
				break;
			case 'i':
				options |= OPT_INITPROC;
				break;
			case 'f':
				options |= OPT_CONFIGFILE;
				config_file = optarg;
				break;
			default:
				usage();
		}
	}

	if (options == 0)
		usage();

	if ((options & OPT_CONFIGFILE) && !(options & OPT_PREP)) {
		fprintf(stderr, "cluster_config: -f option can only be used with -p (--prep) option.\n");
		exit(1);
	}

	if (options & OPT_PREP)
		prep();

	if (options & OPT_PREROOT)
		preroot();

	if (options & OPT_POSTROOT)
		postroot();

	if (options & OPT_INITPROC)
		initproc();

	return 0;
}

