/*
 * 	cluster command
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
/*
 *	cluster.c :
 *
 *	Purpose: Prints a listing of nodes that are currently up.
 *
 *	syntax :
 *		cluster		print all fully-up nodes
 *		cluster -v 	print state for each node
 *		cluster -V	print detailed information for each node
 *		cluster -m	print the maximum number of nodes in the cluster
 *		cluster -n	print the number of fully-up nodes
 *		cluster -r	Print the cluster software release version
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <getopt.h>
#include <sys/utsname.h>
#include <sys/sysinfo.h>
#include <linux/cluster.h>


extern char *optarg;
extern int optind, opterr, optopt;

char *states[] = { "NEVERUP",
		    "COMINGUP",
		    "UP",
		    "SHUTDOWN",
		    "GOINGDOWN",
		    "KCLEANUP",
		    "UCLEANUP",
		    "DOWN"
		 };

char *whys[] =  { "None",
		  "API",
		  "System",
		  "Unexpected"
		};

struct option clusternodeavailopts[] = {
	{"node", 1, 0, 'n'},
	{"help",0,0,'h'},
	{0,0,0,0}
};
struct option clusteropts[] = {
	{"detailedinfo", 0, 0, 'V'},
	{"state", 0, 0, 'v'},
	{"maxnodes", 0, 0, 'm'},
	{"upnodes", 0, 0, 'n'},
	{"version", 0, 0, 'r'},
	{"help",0,0,'h'},
	{0,0,0,0}
};
extern int cluster_ssiconfig(void);

void
pr_usage(
	char	*pname)
{
	if (strncmp(pname, "clusternode_avail", 17) == 0)
		fprintf(stderr, /*  MM_ERROR|MM_NOGET  ,  */
		     "Usage..\n\t%s [option] \n"
		     "\t\t-n,--node=nodenum	Print state of the node\n"
		     "\t\t--help 		Print this help text and exit\n",
		     pname);
	else
		fprintf(stderr,/* MM_ERROR, */
		     "Usage..\n\t%s [option] \n"
		     "\t\t-v,--state 		Print state for each node\n"
		     "\t\t-V, --detailedinfo	Print detailed information for each node\n"
		     "\t\t-m,--maxnodes		Print the maximum number of nodes in the cluster\n"
		     "\t\t-n,--upnodes		Print the number of fully-up nodes\n"
		     "\t\t-r,--version		Print the cluster software release version\n"
		     "\t\t--help 		Print this help text and exit\n",
		     pname);
}

int main(
	int		argc,
	char		*argv[])
{
	char	opt;
	clusternode_t node  = -1;
	clusternode_t	max_nodes;
	int	need_nl;
	int	state = 0;
	int	detailedinfo = 0;
	int	maxnodes = 0;
	int	upnodes = 0;
	int	release = 0;
	clusternode_t	nodes;
	char	*slash;
	char	*myname;
	clusternode_t *nodelist;
	void	do_upnodes(void);
	void	do_release(void);
	void	do_state(clusternode_t, int, int);
	void	do_detailedinfo(clusternode_t, int);

	/*
	 * Make sure we are running on an OpenSSI kernel
	 */
	if (cluster_ssiconfig() != 1) {
		fprintf(stderr,/* MM_HALT  ,  */
		     "This command requires a CI/OpenSSI kernel.\n");
		exit(1);
	}
	max_nodes = cluster_maxnodes();

	/*
	 * If we were invoked as "clusternode_avail," just return the
	 * availability of the node ("UP"/"not UP").
	 */
	if ((slash = strrchr(argv[0], '/')) == NULL)
		myname = argv[0];
	else
		myname = ++slash;
	if (strncmp(myname, "clusternode_avail", 17) == 0) {
		int error = -1; /* Also used to check whether arguments are used */

		while ((opt = getopt_long(argc, argv, "n:h",clusternodeavailopts,NULL)) != EOF) {
			switch(opt) {
				case 'h':
					pr_usage(myname);
					exit(0);
				case 'n':
					node=atoi(optarg);
					error=0;
					break;
				default:
					pr_usage(myname);
					exit(1);


			}
		}

		if(error == -1 ) {
			pr_usage(myname);
			exit(1);
		}


		if (node < 1 || node > max_nodes) {
			fprintf(stderr, /*MM_ERROR, */
			     "%u is an invalid node number.\n",
			     node);
			exit(1);
		}
		error = clusternode_avail(node);
		if (error < 0) {
			fprintf(stderr, /*  MM_ERROR|MM_NOGET  ,  */ 
			     "%s error: %s.\n",
			     myname,
			     strerror(errno));
			exit(1);
		}
		if (error)
			printf("Node %u is UP\n", node);
		else
			printf("Node %u is not up\n", node);
		exit(0);
	}
	while ((opt = getopt_long(argc, argv, "vVmnrh",clusteropts,NULL)) != EOF) {
		switch(opt) {
		case 'v':
			if (detailedinfo || maxnodes || upnodes) {
				pr_usage(myname);
				exit(1);
			}
			state = 1;
			break;
		case 'V':
			if (state || maxnodes || upnodes) {
				pr_usage(myname);
				exit(1);
			}
			detailedinfo = 1;
			break;
		case 'm':
			if (state || detailedinfo || upnodes) {
				pr_usage(myname);
				exit(1);
			}
			maxnodes = 1;
			break;
		case 'n':
			if (state || detailedinfo || maxnodes) {
				pr_usage(myname);
				exit(1);
			}
			upnodes = 1;
			break;
		case 'r':
			if (state || detailedinfo || maxnodes) {
				pr_usage(myname);
				exit(1);
			}
			release = 1;
			break;
		case 'h':
			pr_usage(myname);
			exit(0);
		default :
			pr_usage(myname);
			exit(1);
		}
	}

	if (optind < argc) {
		if (maxnodes || upnodes) {
			pr_usage(myname);
			exit(1);
		}
		nodelist = malloc(max_nodes * sizeof(clusternode_t));
		need_nl = 0;
		nodes = 0;
		for ( ; optind < argc; optind++) {
			node = atoi(argv[optind]);
			if (node < 1 || node > max_nodes) {
				fprintf(stderr,/* MM_ERROR, */
				     "%u is an invalid node number.\n",
				     node);
				need_nl = 1;
			}
			nodelist[nodes++] = node;
		}
		if (need_nl)
			exit(1);
		for (node = 0; node < nodes; node++) {
			if (state) {
				if (nodes == 1)
					do_state(nodelist[node], 1, 1);
				else
					do_state(nodelist[node], 1, 0);
				continue;
			}
			if (detailedinfo) {
				do_detailedinfo(nodelist[node], 1);
				continue;
			}
			if (clusternode_avail(nodelist[node]) == 1) {
				need_nl = 1;
				printf("%u ", nodelist[node]);
			}
		}
		if (need_nl)
			printf("\n");
		exit(0);
	}
	if (maxnodes) {
		printf("%u\n", max_nodes);
		exit(0);
	}
	if (upnodes) {
		do_upnodes();
		exit(0);
	}
	if (release) {
		do_release();
		exit(0);
	}
	need_nl = 0;
	for (node = 1; node <= max_nodes; node++) {
		if (state) {
			do_state(node, 0, 0);
			continue;
		}
		if (detailedinfo) {
			do_detailedinfo(node, 0);
			continue;
		}
		if (clusternode_avail(node) == 1) {
			need_nl = 1;
			printf("%u ", node);
		}
	}
	if (need_nl)
		printf("\n");
	exit(0);
}

void
do_upnodes(void)
{
	int i, upnodes, maxnodes;

	upnodes = 0;
	maxnodes = cluster_maxnodes();
	for (i = 1; i <= maxnodes; i++) {
		if (clusternode_avail(i) == 1)
			upnodes++;
	}
	printf("%d\n", upnodes);
}

void
do_release(void)
{
	printf("This is cluster software release %s\n", VERSION);
}

void
do_state(
	clusternode_t node,
	int force, 	/* include neverups */
	int single)	/* don't bother with node number */
{
	clusternode_info_t ni;

	if (node < 1 || node > cluster_maxnodes())
		return;
	(void)clusternode_info(node, sizeof(ni), &ni);  
	if (force == 0 && ni.node_state == CLUSTERNODE_NEVERUP)
		return;

	if (single)
		printf("%s\n", states[ni.node_state - 1]);
	else
		printf("%d:  %s\n", (int)node, states[ni.node_state - 1]);
}

void
do_detailedinfo(
	clusternode_t node,
	int force)
{
	clusternode_info_t ni;
	char *decode_timeval(struct timeval tv);

	if (node < 1 || node > cluster_maxnodes())
		return;
	(void)clusternode_info(node, sizeof(ni), &ni);
	if (force == 0 && ni.node_state == CLUSTERNODE_NEVERUP)
		return;

	printf("Node %d:\n", (int)node);
	printf("\tState:  %s\n", states[ni.node_state - 1]);
	printf("\tPrevious state:  %s\n",
	       states[ni.node_previous_state - 1]);
	printf("\tReason for last transition:  %s\n",
	       whys[ni.node_transwhy]);
	printf("\tLast transition ID:  %lx\n", 
							ni.node_lasttransid);
	printf("\tLast transition time:  %s",
	       decode_timeval(ni.node_lasttranstime));
	printf("\tFirst transition ID:  %lx\n", 
							ni.node_firsttransid);
	printf("\tFirst transition time:  %s",
	       decode_timeval(ni.node_firsttranstime));
	
	printf("\tNumber of CPUs:  %d\n", 
						ni.node_totalcpus);

	printf("\tNumber of CPUs online:  %d\n", 
						ni.node_onlinecpus);
}

char *
decode_timeval(struct timeval tv)
{
	static char buffer[64];
	char buf1[40];
	char buf2[8];

	ctime_r(&tv.tv_sec, buf1);
	strcpy(buf2, &(buf1[19]));
	buf1[19] = '\0';
	sprintf(buffer, "%s.%06lu%s", buf1, tv.tv_usec, buf2);
	return(buffer);
}
