/*
 * 	clusternode_getstate command
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <linux/cluster.h>

char *states[] = {
	"INVALID",	/* 0 */
	"NEVERUP",	/* 1 */
	"COMINGUP",	/* 2 */
	"UP",		/* 3 */
	"SHUTDOWN",	/* 4 */
	"GOINGDOWN",	/* 5 */
	"KCLEANUP",	/* 6 */
	"UCLEANUP",	/* 7 */
	"DOWN"		/* 8 */
};

void
pr_usage( char	*pname)
{
	fprintf(stderr,/* MM_ACTION, */
	     "Usage...\n\t%s <node>\n", basename(pname));
}

int
main(int argc, char *argv[])
{
	clusternode_t		node;
	int	max_nodes = 0;
	clusternode_info_t ni;

	/*
	 * Make sure we are running on a CI/OpenSSI kernel
	 */
	if (cluster_ssiconfig() != 1) {
		fprintf(stderr,/* MM_HALT, */
		     "This command requires a CI/OpenSSI kernel.\n");
		exit(1);
	}

	max_nodes = cluster_maxnodes();

	if (argc != 2) {
		pr_usage(argv[0]);
		exit(1);
	}

	node = atoi(argv[1]);

	if (node < 1 || node > max_nodes) {
		fprintf(stderr,/* MM_HALT, */
		"Invalid node number %u.\n",node);
		exit(1);
	}

	if(clusternode_info(node, sizeof(ni), &ni) >= 0) {
		printf("%s\n", states[ni.node_state]);
	}
	else {
	 	printf("Error in getting the cluster information \n");
	}
	
	exit(0);
}
