/*
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
#include <errno.h>
#include <linux/cluster.h>
#include <cluster/ssisys.h>

int
cluster_set_config(
	char *s,
	int len)
{
	ssisys_iovec_t	iovec;
	int ret;

	/* must have a valid, non-zero string */
	if (s == NULL || len == 0) {
		errno = EINVAL;
		return -1;
	}

	iovec.tio_id.id_cmd = SSISYS_CLUSTER_SET_CONFIG;
	iovec.tio_id.id_ver = SSISYS_CURVER;
	iovec.tio_udatain = (caddr_t)s;
	iovec.tio_udatainlen = len;
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	ret = ssisys((char *)&iovec, sizeof(iovec));
	
	return ret;
}
