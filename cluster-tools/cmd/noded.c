/*
 * 	noded command
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
/*
 *	noded.c :
 *
 *	Purpose: Executes commands triggered by nodes going down
 *
 *	Syntax : noded
 *
 *	Description:
 *
 *		This daemon sleeps waiting for a node transition to UCLEANUP
 *		to occur.  An appropriate script is executed.  The script
 *		would generally execute clusternode_setstate to mark the
 *		node DOWN before it completes.
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/termios.h>
#include <sys/fcntl.h>
#include <sys/param.h>
#include <linux/cluster.h>

#define	SIGCLUSTER	(SIGIO)

#ifndef SA_NOCLDWAIT
#define SA_NOCLDWAIT 0
#endif

#define MAXFILELINE		250
#define MAXCMDLINE		MAXFILELINE + 6
#ifdef debian
#define NODEUP_SCRIPT           "/etc/init.d/rc.nodeup"
#define NODEDOWN_SCRIPT         "/etc/init.d/rc.nodedown"
#else
#define NODEUP_SCRIPT		"/etc/rc.d/rc.nodeup"
#define NODEDOWN_SCRIPT		"/etc/rc.d/rc.nodedown"
#endif
#define	LOGFILE_NAME		"/var/log/cluster.log"
#define LOGFILE_CFLAGS		O_WRONLY|O_CREAT|O_TRUNC
#define LOGFILE_OFLAGS		O_WRONLY|O_CREAT
#define	LOGFILE_MODE		S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP

/* prototypes */
void	daemonize(void);
void	do_nodedown(clusternode_t);
void	execute_files(clusternode_t, char *);
void	execute_commands(clusternode_t, char *);

char *states[] = { "UNDEFINED",
	"NEVERUP",
	"COMINGUP",
	"UP",
	"SHUTDOWN",
	"GOINGDOWN",
	"KCLEANUP",
	"UCLEANUP",
	"DOWN"
	};

/* Empty handler for SIGCLUSTER */
void
sigcluster(void)
{
}

int
main(int argc, char *argv[])
{
#ifdef CONFIG_SSI
	int	retval;
#endif
	int	error;
	transid_t cookie;
	cluster_transinfo_t trans;

	/*
	 * Make sure we are running on a SSI kernel
	 * SSI_XXX: Really just need to know that CONFIG_CLMS is on for
	 * this kernel.
	 */
	if (!cluster_ssiconfig()) {
		fprintf(stderr, "This daemon requires an CI/OpenSSI kernel.\n");
		exit(1);
	}

	daemonize();

	sighold(SIGCLUSTER);
	if (cluster_events_register_signal(-1, SIGCLUSTER) == -1) {
		fprintf(stderr, "Cannot register SIGCLUSTER.\n");
		exit(1);
	}
	/* Get current cookie so we don't re-process old transitions */
	error = cluster_detailedtransition(
				CLUSTER_TRANSWHICH_LAST,
				   &cookie,
				   sizeof(trans),
				   &trans);
	while(1) {

		sigpause(0);

		do {
			error = cluster_detailedtransition(
						CLUSTER_TRANSWHICH_NEXT,
						   &cookie,
						   sizeof(trans),
						   &trans);
			if (error == 0)
				break;
			if (error == -1) {
				perror("cluster_detailedtransition");
				exit(1);
			}
			
			fprintf(stderr,
			    "received transition, node=%u from %s to %s\n", 
			     trans.transnode, states[trans.fromstate],
		 	     states[trans.tostate]);

			if (trans.tostate == CLUSTERNODE_UCLEANUP)
				do_nodedown(trans.transnode);

		} while(1);
	}
	/* NOTREACHED */
}

void
daemonize()
{
	struct sigaction signal_action;
	pid_t childpid;
	int fd;

	/* ignore terminal stop signals */
	signal_action.sa_handler = SIG_IGN;
	sigaction(SIGTTOU, &signal_action, (struct sigaction *)0);
	sigaction(SIGTTIN, &signal_action, (struct sigaction *)0);
	sigaction(SIGTSTP, &signal_action, (struct sigaction *)0);

	/* do not save zombie children */
	signal_action.sa_handler = SIG_IGN;
	signal_action.sa_flags = SA_NOCLDWAIT;
	sigaction(SIGCHLD, &signal_action, (struct sigaction *)0);

	signal_action.sa_handler = (void *)sigcluster;
	signal_action.sa_flags = 0;
	sigemptyset(&signal_action.sa_mask);
	sigaction(SIGCLUSTER, &signal_action, (struct sigaction *)NULL);

	/* fork to go into the background */
	childpid = fork();
	if ( childpid == -1 ) {
		fprintf(stderr, "HALT: fork failed...\n"
		     "\tReason: %s\n", strerror(errno));
		exit(1);
	}

	if ( childpid > 0 ) {
	        /* parent - exit */
		exit(0);
	}

	/* create new process group */
	setpgid(0, 0);

#ifdef SKIP
	/* disassociate from controlling tty */
	if ((fd = open("/dev/tty",O_RDWR)) >= 0) {
		ioctl(fd,TIOCNOTTY, (char*) NULL);
		close(fd);
	} else {
		fprintf(stderr,
		     "WARNING: unable to disassociate controlling tty...\n"
		     "\tReason: %s\n",
		     strerror(errno));
	}
#endif

	/* 
	 * redirect stderr to a log file
	 *	- if log file does not exist, create it
	 *	- if log file exists, truncate it to 0
	 */
	if ((fd = open(LOGFILE_NAME, LOGFILE_CFLAGS, LOGFILE_MODE)) < 0) {
		fprintf(stderr,
		     "ERROR: unable to open log file...\n"
		     "\tFile: \"%s\"\n"
		     "\tReason: %s\n", 
		     LOGFILE_NAME, strerror(errno));
	} else {
		if (dup2(fd, STDERR_FILENO) != STDERR_FILENO) {
			fprintf(stderr,
			     "ERROR: unable to redirect stderr to log file...\n"
			     "\tReason: %s\n", 
			     strerror(errno));
		}
	}

	/* 
	 * close all open file descriptors
	 */
	for ( fd = 0; fd < NOFILE; fd++) {
		if (fd == STDERR_FILENO)	/* don't close stderr */
			continue;
		else
			close(fd);
	}

	umask(022);
}

void
do_nodedown(clusternode_t node)
{
	pid_t	childpid;

	/*
	 * execute commands on local node
	 */
	if ((childpid = fork()) != 0) {
		if (childpid == -1) {
			fprintf(stderr, "ERROR: fork failed...\n"
			     "\tReason: %s\n", strerror(errno));
		}
	} else {	/* child */

		execute_files(node, NODEDOWN_SCRIPT);

		/*
		 * If the nodedown script execution fails 
		 * announce the node as down ourself
		 */
		clusternode_info_t node_info;
		node_info.node_num 	= node;
		node_info.node_state 	= CLUSTERNODE_DOWN;

		 /* Ignore the error */
		clusternode_setinfo(node, CLUSTERNODE_SETSTATE,
				sizeof(node_info), &node_info);
		/*
		 * child will exit here
		 */
		exit(0);
	}

	/*
	 * parent will return here
	 */
	return;
}

void
execute_files(
	clusternode_t			node, 
	char		*filename)
{
	struct sigaction	sigact;

	/* 
	 * Save zombie children
	 */
	sigact.sa_handler = SIG_DFL;
	sigact.sa_flags &= ~SA_NOCLDWAIT;
	sigaction(SIGCHLD, &sigact, (struct sigaction *)0);

	/*
	 * Execute generic script
	 */
	execute_commands(node, filename);
}

void
execute_commands(
	clusternode_t		node,
	char	*filename)
{
	char *argv[3];
	char nodenum[20];

	sprintf(nodenum, "%u", node);
	argv[0] = filename;
	argv[1] = nodenum;
	argv[2] = NULL;

	execv(filename, argv);

	fprintf(stderr,
	     "WARNING: unable to execute script...\n"
	     "\tScript: \"%s\"\n"
	     "\tReason: %s\n",
	     filename, strerror(errno));
}
