/*
 * 	clusternode_num command
 *	Copyright 2001 Compaq Computer Corporation
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as 
 *	published by the Free Software Foundation; either version 2 of 
 *	the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE
 *	or NON INFRINGEMENT.  See the GNU General Public License for more
 *	details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@opensource.compaq.com
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>
#include <linux/cluster.h>

int
main(int ac, char *av[])
{
	clusternode_t	this_node;
	int		error = 0;

        if (cluster_ssiconfig() != 1) {
                fprintf(stderr,
			"This command requires a CI/OpenSSI kernel.\n");
                exit(1);
        }

	if (ac > 1) {
		fprintf(stderr, "usage: %s\n", basename(av[0]));
		exit(-1);
	}

	if ((this_node = clusternode_num()) != -1) {
		printf("%u\n", this_node);
	} else {
		error = 1;
		fprintf(stderr, "ERROR: clusternode_num() call failed: %s\n",
			 strerror(errno));
	}

	exit(error);
}
