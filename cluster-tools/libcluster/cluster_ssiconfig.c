/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 *	cluster_ssiconfig()
 *		- if an OpenSSI kernel, returns 1
 *		- otherwise, returns 0
 */

extern int cluster_maxnodes();

int
cluster_ssiconfig(void)
{
	if(cluster_maxnodes() > 0)
		return 1;	/* TRUE, i.e. OpenSSI is configured */
	else
		return 0;	/* FALSE, i.e. OpenSSI not configured */
}

