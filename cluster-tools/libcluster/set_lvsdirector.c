/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/cluster.h>
#include <cluster/ssisys.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*
 * FIXME!! The below two data type should match with definition in 
 * cluster/net.h in the kernel source. To make CI build without
 * OpenSSI kernel source adding it here 
 */
enum ipvs_op_type {
	SET_MASTER_DIRECTOR_NODE,
	SET_POSSIBLE_DIRECTOR_NODE,
	DEL_CVIP,
	DEL_POSSIBLE_DIRECTORS,
};

struct ipvs_op {
	enum ipvs_op_type type;
	__u32 daddr;
	clusternode_t dnode;
};

/*
 * Set the nodenum as the master director node for
 * the CVIP specified
 *  Returns -1 on error
 */ 
int set_mdirector(clusternode_t nodenum,  char *ipaddr)
{
	ssisys_iovec_t  iovec;
	struct ipvs_op inargs;
	struct in_addr inp;

	inargs.dnode = nodenum;
	inargs.type = SET_MASTER_DIRECTOR_NODE;

	if(inet_aton(ipaddr,&inp) < 0  ) 
		return -1;

	inargs.daddr = inp.s_addr;
	
	iovec.tio_id.id_cmd = SSISYS_LVS;
	iovec.tio_id.id_ver = SSISYS_CURVER; 
	iovec.tio_udatain = (caddr_t)&inargs;
	iovec.tio_udatainlen = sizeof(inargs);
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	return ssisys((char *)&iovec, sizeof(iovec));
}
/*
 * Set the nodenum as the possible director node for
 * the CVIP specified
 *  Returns -1 on error
 *  a nodenum zero will clear the  array of possible set of 
 *  director nodes inside the kernel
 */ 
int set_pdirector(clusternode_t nodenum,  char *ipaddr)
{
	ssisys_iovec_t  iovec;
	struct ipvs_op inargs;
	struct in_addr inp;

	inargs.dnode = nodenum;
	inargs.type = SET_POSSIBLE_DIRECTOR_NODE;

	if(inet_aton(ipaddr,&inp) < 0  ) 
		return -1;

	inargs.daddr = inp.s_addr;
	
	iovec.tio_id.id_cmd = SSISYS_LVS;
	iovec.tio_id.id_ver = SSISYS_CURVER; 
	iovec.tio_udatain = (caddr_t)&inargs;
	iovec.tio_udatainlen = sizeof(inargs);
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	return ssisys((char *)&iovec, sizeof(iovec));
}

int del_cvip(char *ipaddr)
{
	ssisys_iovec_t  iovec;
	struct ipvs_op inargs;
	struct in_addr inp;

	inargs.dnode = -1;
	inargs.type = DEL_CVIP;

	if(inet_aton(ipaddr,&inp) < 0  ) 
		return -1;

	inargs.daddr = inp.s_addr;
	
	iovec.tio_id.id_cmd = SSISYS_LVS;
	iovec.tio_id.id_ver = SSISYS_CURVER; 
	iovec.tio_udatain = (caddr_t)&inargs;
	iovec.tio_udatainlen = sizeof(inargs);
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	return ssisys((char *)&iovec, sizeof(iovec));
}

int del_pdirectors(char *ipaddr)
{
	ssisys_iovec_t  iovec;
	struct ipvs_op inargs;
	struct in_addr inp;

	inargs.dnode = -1;
	inargs.type = DEL_POSSIBLE_DIRECTORS;

	if(inet_aton(ipaddr,&inp) < 0  ) 
		return -1;

	inargs.daddr = inp.s_addr;
	
	iovec.tio_id.id_cmd = SSISYS_LVS;
	iovec.tio_id.id_ver = SSISYS_CURVER; 
	iovec.tio_udatain = (caddr_t)&inargs;
	iovec.tio_udatainlen = sizeof(inargs);
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	return ssisys((char *)&iovec, sizeof(iovec));
}
