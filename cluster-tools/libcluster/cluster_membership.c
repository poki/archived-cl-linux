/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/cluster.h>
#include <cluster/ssisys.h>

int
cluster_membership(
	unsigned long *transitionid,
	int arraylen,
	clusternode_t *membershiparray)
{
 	ssisys_iovec_t  iovec;
        cls_membership_args_t   inargs;
        transid_t       transid;
        int             ret;

        inargs.arraylen = arraylen;
        inargs.memberarray = membershiparray;
        iovec.tio_id.id_cmd = SSISYS_CLUSTER_MEMBERSHIP;
        iovec.tio_id.id_ver = SSISYS_CURVER;
        iovec.tio_udatain = (caddr_t)&inargs;
        iovec.tio_udatainlen = sizeof(inargs);
        iovec.tio_udataout = (caddr_t)&transid;
        iovec.tio_udataoutlen = sizeof(transid_t);

        ret = ssisys((char *)&iovec, sizeof(iovec));

        if (transitionid != NULL)
                *transitionid = transid;

        return ret;
}
