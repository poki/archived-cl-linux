/*
 *	Copyright (C) 2003 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/cluster.h>
#include <cluster/ssisys.h>

int
cluster_events_register_signal(int desired_fd, int signum)
{
	int retval = 0;
	int fd;
	int save_errno;

	retval = open("/proc/cluster/events", O_RDONLY);
	fd = retval;
	if (retval != -1 && desired_fd != -1 && desired_fd != fd) {
		retval = dup2(fd, desired_fd);
		if (retval != -1) {
			retval = close(fd);
			fd = desired_fd;
		}
	}
	if (retval != -1)
		retval = fcntl(fd, F_SETFD, FD_CLOEXEC);
	if (retval != -1)
		retval = fcntl(fd, F_SETOWN, getpid());
	if (retval != -1)
		retval = fcntl(fd, F_SETSIG, signum);
	if (retval != -1)
		retval = fcntl(fd, F_SETFL, O_ASYNC | O_NONBLOCK);
	if (retval != -1)
		retval = fd;
	else if (fd != -1) {
		save_errno = errno;
		(void)close(fd);
		errno = save_errno;
		retval = -1;
	}

	return retval;
}
