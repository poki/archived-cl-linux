/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <errno.h>
#include <linux/cluster.h>
#include <cluster/ssisys.h>

int
cluster_detailedtransition(
	int  which,
	ulong *transitionid,
	int	info_len,
	cluster_transinfo_t *info)
{
	ssisys_iovec_t	iovec;
	cls_transition_args_t inargs;
	int ret;

	/* must have a transitionid pointer to fill in */
	if (transitionid == NULL || info == NULL) {
		errno = EINVAL;
		return -1;
	}
	inargs.transid = transitionid;
	inargs.which = which;
	inargs.trans_len = info_len;
	iovec.tio_id.id_cmd = SSISYS_CLUSTER_DETAILEDTRANS;
	iovec.tio_id.id_ver = SSISYS_CURVER;
	iovec.tio_udatain = (caddr_t)&inargs;
	iovec.tio_udatainlen = sizeof(inargs);
	iovec.tio_udataout = (caddr_t)info;
	iovec.tio_udataoutlen = sizeof(info);

	ret = ssisys((char *)&iovec, sizeof(iovec));
	
	return ret;
}
