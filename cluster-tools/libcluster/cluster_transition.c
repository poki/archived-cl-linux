/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/cluster.h>

int
cluster_transition(
	ulong	*transitionid,
	int	transinfo_len,
	cluster_transinfo_t *transinfo)
{
	ulong curr_transitionid = *transitionid;
	cluster_transinfo_t curr_transinfo = *transinfo;
	int ret;

	while (1) {
		ret = cluster_detailedtransition(
				CLUSTER_TRANSWHICH_NEXT,
				transitionid,
				transinfo_len,
				transinfo		);

		if (ret <= 0) {
			break;
		}

		if (	(transinfo->fromstate == CLUSTERNODE_UP) ||
			(transinfo->tostate == CLUSTERNODE_UP)	) {

			break;
		}
	}

	if (ret == 0) {
		*transitionid = curr_transitionid;
		*transinfo = curr_transinfo;
	}

	return ret;
}
