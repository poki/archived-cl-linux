/* Copyright (C) 1991, 1992, 1997 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the GNU C Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <linux/cluster.h>

extern int __rexecve(const char *, char *const *, char **, clusternode_t);

#ifndef	HAVE_GNU_LD
# define __environ	environ
#endif

/* Execute PATH with arguments ARGV and environment from `environ'.  */
int
rexecv (const char *path, char *const argv[], clusternode_t node)
{
  return __rexecve (path, argv, __environ, node);
}
