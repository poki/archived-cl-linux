/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/utsname.h>
#include <linux/cluster.h>

char *
cluster_name(void) {
	char *fileName, *namePtr = NULL, *p;
	char name[SYS_NMLN];
	FILE *fp;

	if (access(CLUSTERNAME, F_OK) == 0) {
		fileName = CLUSTERNAME;
	} else if (access(NODENAME, F_OK) == 0) {
		fileName = NODENAME;
	} else {
		return NULL;
	}

	fp = fopen(fileName, "r");
	if (fp == NULL) {
		return NULL;
	}

	if( fgets(name, SYS_NMLN, fp) == NULL ) {
		fclose(fp);
		return NULL;

	}
	if( (p = strchr(name,'\n'))  != NULL ) {
		/* Remove the new line if there*/
		*p='\0';
	}

	namePtr = (char *) malloc(strlen(name) + 1);
	if( namePtr == NULL ) 
		goto err_ret;
		
	strcpy(namePtr, name);

err_ret:
	fclose(fp);
	return namePtr;
}
