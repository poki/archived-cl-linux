/*
 *	Copyright (C) 2002 Hewlett-Packard Company
 *	
 *	Date   :	Aug 31 2002
 *	Authors:	Aneesh Kumar K.V ( aneesh.kumar@digital.com )
 *
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <mntent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/cluster.h>

#define TRUE 1
#define FALSE 0

#define FSLOCK_DIR "/cluster/.fslock"
#define ROOTDISKNODE  "/cluster/rootdisknode"

/* SSI_XXX: Remove in a later release.  Don't want to chance having a mount
 * command that won't run.
 */
int clustermount = 0;

/*
 * This removes from start to end
 * from the from_string
 * start and end must be pointers within the from_string
 */

static char * remove_opt(char *from_string, char * start, char * end)
{
	int fromlen, rlen;
	char *rstring ;

	fromlen = strlen(from_string);
	if (*end == ',') end++;		/* don't copy beginning comma */
	rlen = fromlen - (end - start) + 1;

	/* Return something which is no bigger than node=N in the case
	 * when that is the only option specified.  Would have prefered
	 * returning "defaults" but it must fit in the same string.
	 */
	if (rlen == 1) {
		return strdup("rw");
	}

	rstring = (char *)malloc(rlen);
	if (rstring == NULL) {
		fprintf(stderr, "remove_opt: malloc failed\n");
		return NULL;
	}
	memset(rstring, 0, rlen);

	/* Copy into the new string */
	strncpy(rstring, from_string, start - from_string);
	strcat(rstring, end);

	/* Remove trailing comma if any */
	rlen = strlen(rstring);
	if (rstring[rlen - 1] == ',') rstring[rlen - 1] = '\0';

	return rstring;
}


/* This API is used to check whether a mount point
 * belong to this node .To indicate the mount
 * point belong this node the opt string should
 * carry node=# or node=*. In the later case the
 * entry will returned on all nodes
 */

static clusternode_t this_node = 0;
static clusternode_t rootdisk_node = 0;

int ssi_thisnode_entry(char *opt_string)
{
	clusternode_t *nodes;
	int node_count;
	int i;
	int max_nodes;
	int openssi;

	if ((openssi = cluster_ssiconfig())) {
		max_nodes = cluster_maxnodes();
		this_node = clusternode_num();
	} else {
		FILE *rootfp;
		max_nodes = 32768;
		if( (rootfp = fopen(ROOTDISKNODE,"r") ) != NULL ) {
			fscanf(rootfp,"%u",&rootdisk_node);
			fclose(rootfp);
			char clusterdir[32];
			sprintf(clusterdir, "/cluster/node%u", rootdisk_node);
			if (access(clusterdir, F_OK) == 0)
				this_node = rootdisk_node;
			else 
				fprintf(stderr, "Error in parsing %s\n", ROOTDISKNODE);
		} else {
			fprintf(stderr,"Error in opening %s\n",ROOTDISKNODE);
		}
		if (!this_node) {
			fprintf(stderr,
"The %s file should contain the node number\n"
"of the original installation node. This node number is typically 1,\n"
"unless you selected a different node number during the installation\n"
"of OpenSSI. Without a valid node number in %s,\n"
"/etc/fstab cannot be parsed, so you must specify a device name\n"
"to the mount, fsck.ext3 and other fsck.* commands until you fix\n"
"%s.\n",
				ROOTDISKNODE,ROOTDISKNODE,ROOTDISKNODE);
			exit(2);
		}
	}

	nodes = malloc(sizeof(*nodes) * max_nodes);
	if (nodes == NULL) {
		fprintf(stderr, "ssi_thisnode_entry: malloc failed\n");
		return FALSE;
	}

	node_count = ssi_getnodes_entry(opt_string, nodes);
	if (node_count <= 0) {
		fprintf(stderr, "ssi_thisnode_entry: Could not get node list\n");
		free(nodes);
		return FALSE;
	}

	for (i = 0; i < node_count; i++) {
		if (nodes[i] == this_node) {
		     free(nodes);
		     return TRUE;
		}
	}
	free(nodes);
	return FALSE;
}

/* This routine parses the option string and returns the number of nodes
 * in the node=.... option. The nodenumbers are returned in "nodes"
 */
int ssi_getnodes_entry(char * opt_string, clusternode_t * nodes)
{
	clusternode_t nodenum;
	char *nodeopt, *nodestr, *nodelist;
	char *s, *index, *end;
	char *rstring;
	int i, maxnodes, nodes_len;
	int count = 0;

	if (nodes == NULL) {
		fprintf(stderr, "ssi_getnodes_entry: Invalid argument\n");
		return -1;
	}

	nodeopt = strstr(opt_string, "node=");
	if (nodeopt == NULL) {
		/* "node=" not present in the line -
		 * if SSI, then print warning
		 * otherwise return node 1
		 */
		if (cluster_ssiconfig()) {
		     fprintf(stderr, "ssi_getnodes_entry: "
			     "warning: no node option specified in option string %s\n", opt_string);
		     return -1;
		} else {
		     *nodes = rootdisk_node;
		     return 1;
		}
	}

	nodestr = strchr(nodeopt, '=') + 1;
	if ((end = strchr(nodestr, ',')) == NULL)
		end = nodestr + strlen(nodestr);

	if (nodestr == end) {
		/* the node list is empty */
		fprintf(stderr, "ssi_getnodes_entry: empty node list\n");
		return 0;
	}

	nodes_len = end - nodestr;
	nodelist = (char *)malloc(nodes_len + 1);
	if (nodelist == NULL) {
		fprintf(stderr, "ssi_getnodes_entry: malloc failed\n");
		return -1;
	}
	memset(nodelist, 0, nodes_len + 1);

	strncpy(nodelist, nodestr, nodes_len);

	/* remove the node=XXX option from the opt_string */
	rstring = remove_opt(opt_string, nodeopt, end);
	if (rstring == NULL)
		strcpy(opt_string, "rw");	/* Don't want empty opts */
	else {
		strcpy(opt_string, rstring);
		free(rstring);
	}

	/* If node=* return all UP nodes */
	if (*nodelist == '*') {
		if ((maxnodes = cluster_maxnodes()) <= 0) {
			/* 
			 * Since we use same /etc/fstab  return 
			 * /cluster/rootnodenum 
			 * for entries of format node=* 
			 */
			nodes[0]=rootdisk_node;
			free(nodelist);
			return 1;
		}
		for (i = 1; i < maxnodes; i++) {
		     if (clusternode_avail(i)) {
			  nodes[count] = i;
			  count++;
		     }
		}
		free(nodelist);
		return count;
	}

	/* parse the node=1:2:3 option */
	index = nodelist;
	do {
		s = strchr(index, ':');
		if (s != NULL) *s = '\0';
		nodenum = strtoul(index, &end, 10);
		if (nodenum == 0) {
		     fprintf(stderr, "ssi_getnodes_entry: Invalid node number\n");
		     free(nodelist);
		     return -1;
		}
		nodes[count] = nodenum;
		count++;
		index = s + 1;
	} while (s);
	free(nodelist);
	return count;
}

char *
ssi_qualify_dev(char * spec, clusternode_t node)
{
	char * nspec;
	char * index;
	char nodestr[256];
	int len;

	if (spec == NULL)
		return NULL;

	if (!strncmp(spec, "/cluster/node", 13)) {
		index = spec + 13;
		/* Skip node number */
		while(isdigit(*index))
			index++;

		/* Ignore strange names */
		if (*index != '/' || index == spec + 13)
		     return strdup(spec);

		/* We've slid forward to the /dev/NNN part */
		if (!strncmp(index, "/dev/", 5)) {
			spec = index;
			goto qualify;
		}
	} else if (!strncmp(spec, "/dev/", 5)) {
		for (index = spec + 5; *index != '/'; index++) {
		     if (!isdigit(*index)) break;
		}
		if (*index == '/')
		     return strdup(spec);

qualify:
		/* The device is not already qualified */
		sprintf(nodestr, "%u", node);
		len = strlen(spec) + strlen(nodestr) + 2;
		nspec = (char *)malloc(len);
		memset(nspec, 0, len);
		strncpy(nspec, spec, 5);
		strcat(nspec, nodestr);
		strcat(nspec, spec + 4);
		return nspec;
	}
	return strdup(spec);
}

clusternode_t ssi_get_dev_node(char *spec)
{
	if (!strncmp(spec, "/dev/", 5))
		return strtoul(spec + 5, NULL, 10);
	return 0;
}

const char * get_fstab()
{
	fprintf(stderr, "Program calling deprecated function get_fstab()\n");
	return "/etc/fstab";
}

int lock_fstab_entry(char *spec, int *fd)
{
	char fname[1024];
	char tmpspec[1024];
	struct flock lk;
	int i, speclen;

	/* Replace all / characters with - for the file name */
	speclen = strlen(spec);
	for (i = 0; i <= speclen; i++) {
		if (spec[i] == '/')
			tmpspec[i] = '-';
		else
			tmpspec[i] = spec[i];
	}

	/* Open/create the file */
	sprintf(fname, "%s/%s", FSLOCK_DIR, tmpspec);
	*fd = open(fname, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	if (*fd < 0) {
		/* If the root fs is read only, allow caller to continue */
		if (errno == EROFS)
			return 0;
		perror("lock_fstab_entry");
		return -1;
	}

	/* Lock the file exclusively */
	lk.l_type = F_WRLCK;
	lk.l_whence = SEEK_SET;
	lk.l_start = 0;
	lk.l_len = 0;
	
	if (fcntl(*fd, F_SETLKW, &lk) < 0) {
		perror("lock_fstab_entry");
		close(*fd);
		return -1;
	}
	return 0;
}

int unlock_fstab_entry(int fd)
{
	struct flock lk;

	if (fd < 0)
		return 0;

	/* Simple call unlock */
	lk.l_type = F_UNLCK;
	lk.l_whence = SEEK_SET;
	lk.l_start = 0;
	lk.l_len = 0;
	
	if (fcntl(fd, F_SETLKW, &lk) < 0) {
		perror("unlock_fstab_entry");
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}

