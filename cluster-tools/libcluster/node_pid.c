/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/cluster.h>
#include <cluster/ssisys.h>

clusternode_t
node_pid(pid_t pid)
{
	ssisys_iovec_t	iovec;
	ts_nodepid_inargs_t     inargs;

        /* setup inargs structure */
        /* NOTE: id field currently not used */
        inargs.tnp_pid = pid;

       
        /* set up ssisys() iovec structure */
        iovec.tio_id.id_cmd = SSISYS_NODE_PID;  /* ssisys() subcmd */
        iovec.tio_id.id_ver = SSISYS_CURVER;    /* ssisys() version */
        iovec.tio_udatain = (caddr_t)&inargs;
        iovec.tio_udatainlen = sizeof(inargs);
        iovec.tio_udataout = (caddr_t)NULL;
        iovec.tio_udataoutlen = 0;

        return ssisys((char *)&iovec, sizeof(iovec));
}
