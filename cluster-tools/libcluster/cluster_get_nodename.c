/*
 *	Copyright (C) 2003 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/cluster.h>
#include <errno.h>
#include <cluster/ssisys.h>

int
cluster_get_nodename(char *str, int len)
{
	ssisys_iovec_t	iovec;
	int ret;

	if (str == NULL || len < 10) {
		errno = EINVAL;
		return -1;
	}

	iovec.tio_id.id_cmd = SSISYS_GET_NODENAME;
	iovec.tio_id.id_ver = SSISYS_CURVER;
	iovec.tio_udatain = (caddr_t)NULL;
	iovec.tio_udatainlen = 0;
	iovec.tio_udataout = (caddr_t)str;
	iovec.tio_udataoutlen = len;

	ret = ssisys((char *)&iovec, sizeof(iovec));
	
	return ret;
}
