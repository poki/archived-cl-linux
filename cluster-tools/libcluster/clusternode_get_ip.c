/*
 *	Copyright (C) 2003 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 *	clusternode_get_ip()
 *		- returns ICS IP addr in a sockaddr struct.
 */


#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/cluster.h>



/*
 * FIXME !!!! We should actually get this from kernel 
 * Look at ics_geticsinfo in the kernel
 */
int
clusternode_get_ip(clusternode_t node, struct sockaddr *sa, int *size)
{
	char d1[64], d2[64], d3[64];
	int is_m;
	FILE *fp;
	int ret = 0;
	char *line = NULL;
	size_t line_size = 0;
	char ipaddr[16];
	char *s;
	clusternode_t nodenum;
	int found = 0;

	fp = fopen("/etc/clustertab", "r");
	if (!fp){
		perror("/etc/clustertab");
		errno = ENOENT;
		return -1;
	}

	sa->sa_family = 0;
	while ( getline(&line,&line_size,fp) != -1) {

		for(s = line; *s == ' ' || *s == '\t'; s++);
		if (*s == '#' || *s == '\n') 
			continue;
	
		ret = sscanf(line, "%u %s %s %s %d %s\n", &nodenum, ipaddr,
					d1, d2,&is_m, d3);


		if( line )
			free(line);
		line = NULL;
		line_size = 0;

		if( ret == EOF || ret < 5 ) {
			fclose(fp);
			errno = EINVAL;
			return -1;
		}
		if (nodenum == node) {
			found = 1;
			break;
		}
	}

	fclose(fp);

	if (found) {

		struct in_addr saddr;

		*size = sizeof(struct sockaddr); 
		if (inet_aton(ipaddr, &saddr) == 0) {
			errno = EINVAL;
			return -1;
		}
		
		((struct sockaddr_in *)sa)->sin_addr = saddr;
		return 0;
	}
	errno = EINVAL;
	return -1;
}
