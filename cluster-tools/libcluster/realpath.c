/*
 * realpath.c -- canonicalize pathname by removing symlinks
 * Copyright (C) 1993 Rick Sladkey <jrs@world.std.com>
 *
 * Based on realpath in umount,
 * Modified for OPENSSI by John Hughes <john@calva.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library Public License for more details.
 */

#include <stdio.h>
#include <limits.h>		/* for PATH_MAX */
#ifndef PATH_MAX
#define PATH_MAX 8192
#endif
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/cluster.h>

#define MAX_READLINKS 32

char *
ssi_realpath(const char *path, char *resolved_path, int maxreslth, int full) 
{
	int readlinks = 0;
	char *npath;
	char link_path[PATH_MAX+1];
	int n;
	char *buf;
	char *tmp = NULL;
	int m;
	char *p;

	npath = resolved_path;

	/* If it's a relative pathname use getcwd for starters. */
	if (*path != '/') {
		if (!getcwd(npath, maxreslth-2))
			return NULL;
		npath += strlen(npath);
		if (npath[-1] != '/')
			*npath++ = '/';
	} else {
		*npath++ = '/';
		path++;
	}

	/* Expand each slash-separated pathname component. */
	while (*path != '\0') {
		/* Ignore stray "/" */
		if (*path == '/') {
			path++;
			continue;
		}
		if (*path == '.' && (path[1] == '\0' || path[1] == '/')) {
			/* Ignore "." */
			path++;
			continue;
		}
		if (*path == '.' && path[1] == '.' &&
		    (path[2] == '\0' || path[2] == '/')) {
			/* Backup for ".." */
			path += 2;
			while (npath > resolved_path+1 &&
			       (--npath)[-1] != '/')
				;
			continue;
		}
		/* Safely copy the next pathname component. */
		while (*path != '\0' && *path != '/') {
			if (npath-resolved_path > maxreslth-2) {
				errno = ENAMETOOLONG;
				goto fail;
			}
			*npath++ = *path++;
		}

		/* Protect against infinite loops. */
		if (readlinks++ > MAX_READLINKS) {
			errno = ELOOP;
			goto fail;
		}

		/* See if last pathname component is a symlink. */
		*npath = '\0';
		n = readlink(resolved_path, link_path, PATH_MAX);
		if (n < 0) {
			/* EINVAL means the file exists but isn't a symlink. */
			if (errno != EINVAL)
				goto fail;
		} else {
			/* SSI_XXX: This is wrong, clusternode_num
			   returns the current node, but {nodenum} is
			   supposed to use the _context_, i.e. doesn't
			   change if the process gets migrated */
			int node = clusternode_num ();
			if (node > 0 && (p = strstr (link_path, "{nodenum}"))) {
				char *q = p + strlen ("{nodenum}");
				p += sprintf (p, "%d", node);
				strcat (p, q);
				n -= (q - p);
			}
			else if (!full) {
				goto no_symlink;
			}

			/* Note: readlink doesn't add the null byte. */
			link_path[n] = '\0';
			if (*link_path == '/')
				/* Start over for an absolute symlink. */
				npath = resolved_path;
			else
				/* Otherwise back up over this component. */
				while (*(--npath) != '/')
					;

			/* Insert symlink contents into path. */
			m = strlen(path);
			buf = malloc(m + n + 1);
			memcpy(buf, link_path, n);
			memcpy(buf + n, path, m + 1);
			path = buf;
			free (tmp);
			tmp = buf;
		    no_symlink: ;
		}
		*npath++ = '/';
	}
	/* Delete trailing slash but don't whomp a lone slash. */
	if (npath != resolved_path+1 && npath[-1] == '/')
		npath--;
	/* Make sure it's null terminated. */
	*npath = '\0';
	free (tmp);

	/* Well, all that was nice, but if it's a device make it
 	   prettier, /cluster/node7/dev/xxx => /dev/7/xxx */

	if (strncmp (resolved_path, "/cluster/node", 13) == 0 && 
	    (tmp = ssi_qualify_dev (resolved_path, 0)))
	{
		if (strncmp (tmp, "/dev/", 5) == 0) {
			if (strlen (tmp) >= maxreslth) {
				errno = ENAMETOOLONG;
				goto fail;
			}
			strcpy (resolved_path, tmp);
		}
		free (tmp);
	}
		
	return resolved_path;

fail:
	free (tmp);
	return NULL;
}
