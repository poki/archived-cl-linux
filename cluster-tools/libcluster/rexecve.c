/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 *      rexecve (file, argv, envp, node)
 *
 *	where argv is a vector argv[0] ... argv[x], NULL
 *	last vector element must be NULL
 */

/*  #include "synonyms.h"   */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <linux/unistd.h>
#include <linux/cluster.h>  

int rexecve( const char *path, char *const argv[], char* const envp[], clusternode_t node)
{
	return syscall(__NR_rexecve, path, argv, envp, node);
}
	

int
__rexecve (path, argv, envp, node)
     const char *path;
     char *const argv[];
     char **envp;
     clusternode_t node;
{
	if (path == NULL || argv == NULL || envp == NULL)
	{
		errno = EINVAL;
		return -1;
	}

	return(rexecve(path, argv, envp, node));
}
