/*
 *	Copyright (C) 2003 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
/*
 *	cluster_getnodebyname()
 *		- Returns the node number given a node name
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <linux/cluster.h>


clusternode_t cluster_getnodebyname( char *) ;
int check_if_nodedir( const struct dirent *);

/*
 * Checks if the dir entry passed as argument is a nodedirectory 
 * like "node1" ..
 * This function is an argument to scandir().
 */
int 
check_if_nodedir( const struct dirent *dir_entry )
{
	struct stat 	stat_buf ;
	char 			*dirpath ;

	if ( !dir_entry )
		return 0;

	/*
	 * Ignore "." and ".."
	 */
	if ( !strcmp(dir_entry->d_name,".") || 
				!strcmp(dir_entry->d_name,"..") )
		return 0;

	/*
	 * Check if name starts with "node"  and the next character in
	 * the name is a digit 
	 */
	if ( dir_entry->d_name && strlen(dir_entry->d_name) > 4 && 
					!strncmp(dir_entry->d_name,"node",4) ) {

		int i;

		/*
		 * Check if all next characters in the name are numbers 
		 */
		i = 4;
		while ( *(dir_entry->d_name + i ) != '\0' ) {
			if( !isdigit(*(dir_entry->d_name + i)) ) 
					return 0;
			i++;
		} /* end while() */

		/*
		 * get the status of the dir_entry 
		 */
		dirpath = (char *)malloc((strlen("/cluster/ ") + 
				strlen(dir_entry->d_name) + 1) * sizeof(char));
		if( !dirpath ) 
			return 0;

		sprintf(dirpath,"/cluster/%s",dir_entry->d_name);

		if ( stat(dirpath,&stat_buf) < 0 ) {
			free(dirpath);
			return 0;
		}
		free(dirpath);

		/*
		 * Check if directory
		 */
		if( S_ISDIR(stat_buf.st_mode) ) {
			return 1;
		}
	}
	return 0;
}	/* end of func select() */


clusternode_t 
cluster_getnodebyname( char *nodename ) 
{
	char 	*filename = NULL;
	char 	*str = NULL;
	int	str_size = 0;
	FILE 	*fd;
	int 	i , num_dentries;
	int 	node_num = -1;
	int 	(*select_fn)(const struct dirent *);
	int 	found = -1;
	struct dirent **dentry_list = NULL;


	/*
 	 * Check if argument passed is valid 
  	 */
	if( !nodename ) {
		errno = EINVAL;
		return -1;
	}	

	/*
	 * assign function pointer "select_fn" to check_if_nodedir()
	 */
	select_fn = check_if_nodedir;

	/*
	 * Scan dir "/cluster" 
	 */
	if( (num_dentries = scandir("/cluster",&dentry_list,select_fn,NULL)) <= 0) {
		perror("scandir on /cluster");
		return -1;
	}

	/*
	 * Scan through the /cluster entries 
	 */
	for ( i=0; i < num_dentries ; i++) {

		/*
		 * Read the node number from the dir entry
		 */
		sscanf(dentry_list[i]->d_name,"node%d",&node_num);

		filename = (char *)malloc(strlen(dentry_list[i]->d_name) + 
					strlen("/cluster//etc/nodename") + 1 );

		if( !filename ) {
			errno = ENOMEM; 
			return -1;
		}

		sprintf(filename,"/cluster/%s/etc/nodename",dentry_list[i]->d_name);
		/*
		 * Open the nodename file 
		 */
		fd = fopen ( filename,"r" );
		if ( !fd ) {
			free(filename);
			filename = NULL; 
			continue ;
		}

		/*
		 * Read the nodename from the file
		 */
		while ( getline(&str,&str_size,fd) != -1 ){

			char name[80];
			int  ret;
			char *ptr = NULL;

			ret = sscanf(str,"%s",name);
			if( ret == EOF || ret < 1 ) {
				if (str)
					free(str);
				errno = EINVAL;
				return -1;
			}

			/* free for getline above */
			free(str);
			str = NULL;
			str_size = 0;

			/*
			 * Compare the nodename with the name read
			 * from /etc/nodename
			 */
			if(((strchr(name,'.') == NULL && 
					strchr(nodename,'.') == NULL)) ||
					((strchr(name,'.') != NULL && 
					strchr(nodename,'.') != NULL))){
				/*
				 * If name and nodename both have domain
				 * names or both are without domain names
				 * do a direct strcmp
				 */
				if ( !strcmp(name,nodename) ) {
					found = 1;
					break;
				}
			}
			else if((ptr = strchr(name,'.')) != NULL &&
							(strlen(ptr) > 1)) {
				/*
				 * If the name read from /etc/nodename has a
				 * domain name
				 */
				name[strlen(name) - strlen(ptr)] = '\0';
				if( !strcmp(name,nodename) ) {
					found = 1;
					break;
				}
			}
			else if((ptr = strchr(nodename,'.')) != NULL &&
							(strlen(ptr) > 1)){
				/*
				 * If the nodename passed as argument has a
				 * domain name
				 */
				if(!strncmp(nodename,name,strlen(nodename) -
							strlen(ptr)) ) {
					found = 1;
					break;
				}
			}
			/*
			 * break here, as there is supposed to be only
			 * one line in the /etc/nodename file.
			 */
			break;

		} /* end while() */

		fclose(fd);

		if( filename )
			free(filename);
		filename = NULL;

		if( found == 1 ) 
			return node_num;

  	}	/* end for() */

	errno = EINVAL;
	return -1;
} /* end of func cluster_getnodebyname() */

