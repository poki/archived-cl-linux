/*
 *	Copyright (C) 2001 Hewlett-Packard Company
 *	
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *	
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *	
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, 
 *	Boston, MA  02111-1307  USA
 *
 *	Questions/Comments/Bugfixes to ci-linux-devel@lists.sourceforge.net
 *
 */
#include <linux/cluster.h>
#include <cluster/ssisys.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

/* SSI_XXX This should match the def in net/ip_vs.h */
#define IP_VS_SCHEDNAME_MAXLEN  16



/* This should actually go in a header */
struct portweight {
	u_short type;
	u_short start_port;
	u_short end_port;
	int weight;
	char lvs_sched[IP_VS_SCHEDNAME_MAXLEN];
};


/*
 *  Returns -1 on error
 */ 
int set_ipvsportweight(u_short sport, u_short eport,int weight,
		u_short type, char *lvs_sched)
{
	ssisys_iovec_t  iovec;
	struct portweight inargs;

	if( eport < sport ) {
		/* end port should always be greater than start port */
		errno = EINVAL;
		return -1;
	}

	inargs.type 	  = type;
	inargs.start_port = sport;
	inargs.end_port   = eport;
	inargs.weight     = weight;
	strncpy(inargs.lvs_sched, lvs_sched, IP_VS_SCHEDNAME_MAXLEN);
	inargs.lvs_sched[IP_VS_SCHEDNAME_MAXLEN-1] = '\0';

	iovec.tio_id.id_cmd = SSISYS_SET_IPVSPORTWEIGHT;
	iovec.tio_id.id_ver = SSISYS_CURVER; 
	iovec.tio_udatain = (caddr_t)&inargs;
	iovec.tio_udatainlen = sizeof(inargs);
	iovec.tio_udataout = (caddr_t)NULL;
	iovec.tio_udataoutlen = 0;

	return ssisys((char *)&iovec, sizeof(iovec));
}

