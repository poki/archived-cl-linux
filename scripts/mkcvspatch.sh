#!/bin/bash

usage () {
	echo "Usage: $0 <source path> <CVS tag>"
	exit 1
}

SRCDIR=$1
CVSTAG=$2

if [ -z "$SRCDIR" -o -z "$CVSTAG" ]; then
	usage
fi
if [ ! -d $SRCDIR ]; then
	# We are only executing the prep stage to get the sources files.
	# Do not verify build dependencies at this point.
	rpmbuild -bp --nodeps /usr/src/packages/SPECS/${SRCDIR}.spec
	cp -pr /usr/src/packages/BUILD/${SRCDIR}* ./$SRCDIR
	rm -fr /usr/src/packages/BUILD/${SRCDIR}* 
	if [ ! -d $SRCDIR ]; then
		echo "Missing directory: $SRCDIR"
		exit 1
 	fi
fi

MYDIR=`pwd`
cd `dirname $SRCDIR`
SRCNAME=`basename $SRCDIR`
BASENAME=${SRCNAME}.base
if [ "$SRCDIR" != "udev" ] && [ "$SRCDIR" != "klibc" ]; then
	if [ ! -d $BASENAME ]; then
		cvs -z3 co -d $BASENAME -r $CVSTAG openssi/$SRCDIR
			if [ $? -ne 0 ]; then
				echo "CVS checkout failed"
				exit 1
			fi
	fi

	mkdir -p $MYDIR/patches
	diff -Naur -xCVS $BASENAME $SRCNAME \
		>$MYDIR/patches/${SRCNAME}-ssi.patch
else
	mkdir -p $MYDIR/patches
	touch $MYDIR/patches/${SRCNAME}-ssi.patch
fi
exit 0
