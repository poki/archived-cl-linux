#!/bin/bash


usage () {
	echo "Usage: $0 [-n] <target dir> [<target dir>...]"
	exit 1
}

while getopts 'n' opt; do
	case $opt in
	n)
		NOCVS=1
		;;
	*)
		usage
		;;
	esac
done
shift `expr $OPTIND - 1`

if [ $# -eq 0 ]; then
	usage
fi

while [ $# -gt 0 ]; do
	TARGET=$1; shift
	if [ ! -d $TARGET ]; then
		echo "Missing directory: $TARGET"
		continue
	fi

	find $TARGET -name '*~' -exec rm {} \;
	find $TARGET -name '.#*' -exec rm {} \;
	find $TARGET -name '*.orig' -exec rm {} \;
	find $TARGET -name '*.rej' -exec rm {} \;
	if [ -z "$NOCVS" ]; then
		find $TARGET -name 'CVS' -prune -exec rm -rf {} \;
	fi
done
