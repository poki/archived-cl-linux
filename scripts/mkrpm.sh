#!/bin/bash

RHDIR=`rpm --eval='%{_topdir}'`

usage () {
	echo "Usage: $0 <rpm options> <specfile> [<source file>...]"
	exit 1
}

if [ $# -lt 3 ]; then
	usage
fi

OPTIONS="$1"
SPECPATH=$2
SPECFILE=`basename $SPECPATH`
if [ ! -d specs ]; then
	echo "Must be run from top-level directory of sandbox"
	exit 1
fi
if [ ! -f $SPECPATH ]; then
	echo "No such file $SPECFILE"
	exit 1
fi

cp -a $SPECPATH $RHDIR/SPECS/
chown root.root $RHDIR/SPECS/$SPECFILE

shift 2
while [ $# -gt 0 ]; do
	SRCFILE=$1; shift
	if [ ! -f $SRCFILE ]; then
		echo "No such file $SRCFILE"
		exit 1
	fi
	cp $SRCFILE $RHDIR/SOURCES
done

echo "Building the RPM for $SPECFILE"
rpmbuild -ba $OPTIONS $RHDIR/SPECS/$SPECFILE
if [ $? != 0 ]; then
	exit 1
fi
