#!/bin/bash

usage () {
	echo "Usage: $0 <pkg name> <new suffix>"
	exit 1
}

PKGNAME=$1
NEWNAME=$2

if [ -z "$PKGNAME" -o -z "$NEWNAME" ]; then
	usage
fi

BASEDIR=${PKGNAME}
if [ ! -d ../$BASEDIR ]; then
	echo "Missing directory: ../$BASEDIR"
	exit 1
fi

NEWDIR=${PKGNAME}-${NEWNAME}
if [ ! -d ../$NEWDIR ]; then
	echo "Missing directory: ../$NEWDIR"
	exit 1
fi

mkdir -p patches
MYDIR=`pwd`; cd ..
diff -Naur -xCVS $BASEDIR $NEWDIR \
	>$MYDIR/patches/${PKGNAME}-${NEWNAME}.patch

exit 0
