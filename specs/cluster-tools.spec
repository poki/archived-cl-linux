Summary: Tools for retrieving information about CI and OpenSSI clusters
Name: cluster-tools
Version: 2.0.0
Release: 4
License: GPL
Group: Applications/Systems
Source: %{name}-%{version}.tar.bz2
BuildRequires: autoconf automake
Buildroot: %{_tmppath}/%{name}-%{version}-root

%description
Complements CI and OpenSSI kernels with utilities and a library for retrieving
information about the state of the cluster.

%prep
%setup -q

%build
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/lib
mkdir -p $RPM_BUILD_ROOT/lib64
mkdir -p $RPM_BUILD_ROOT/sbin
mkdir -p $RPM_BUILD_ROOT/usr/sbin

%ifarch x86_64 ia32e
ln -s asm-x86_64 kernel/include/asm
%endif
%ifarch i386 ia486 i586 i686 athlon
ln -s asm-i386 kernel/include/asm
%endif

./configure --with-linux_srcdir=`pwd`/kernel --libdir=/%{_lib}

make

%install
make install DESTDIR=${RPM_BUILD_ROOT}


%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
/bin
/cluster
/%{_lib}
/sbin
/usr/include
/usr/sbin
/usr/share

%doc README
%doc COPYING
%doc ChangeLog
%doc docs/*

%changelog
* Tue Jul 01 2003 Brian Watson <Brian.J.Watson@hp.com>
- Cut out stuff to reflect the new stripped-down cluster-tools

* Mon Apr 14 2003 Brian Watson <Brian.J.Watson@hp.com>
- Add a few bug patches
- Add the patch and nfs-utils packages as dependencies

* Mon Mar 31 2003 Brian Watson <Brian.J.Watson@hp.com>
- Create some additional directories
- Add postinstall and preuninstall scripts to do additional setup and teardown

* Fri Dec 20 2002 Brian Watson <Brian.J.Watson@hp.com>
- New version no longer needs syscall-renumber.patch

* Wed Nov 27 2002 Brian Watson <Brian.J.Watson@hp.com>
- Initial version
