Summary: The Linux kernel (the core of the Linux operating system)

# What parts do we want to build?  We must build at least one kernel.
# These are the kernels that are built IF the architecture allows and
# no contrary --with/--without arguments are given on the command line.

%define buildup 1
%define buildsmp 1
%define buildBOOT 1
%define buildbigmem 0
%define buildsource 1


#
# Some things in the spec are needed for RHL 7.x compatibility
# this define will enable those
#
%define sevenexcompat 0

# Versions of various parts

#
# Polite request for people who spin their own kernel rpms:
# please modify the "release" field in a way that identifies
# that the kernel isn't the stock RHL kernel, for example by
# adding some text to the end of the version number.
#
%define release %(R="$Revision: 1.2199 $"; RR="${R##: }"; echo ${RR%%?}).nptl
%define sublevel 22
%define kversion 2.4.%{sublevel}
# /usr/src/%{kslnk} -> /usr/src/linux-%{KVERREL}
%define kslnk linux-2.4
%define KVERREL %{PACKAGE_VERSION}-%{PACKAGE_RELEASE}

# groups of related archs
%define all_x86 i386 i686 i586 athlon x86_64

#
# RPM magic
#
%define _missing_doc_files_terminate_build    0
%define _unpackaged_files_terminate_build 0
# disable build root strip policy
%define __spec_install_post /usr/lib/rpm/brp-compress || :


# Override generic defaults with per-arch defaults 
# These must ONLY be "0", never "1"

# First, architecture-specific kernels off on all other archs (ifnarch)
%ifnarch i686
%define buildbigmem 0
%endif
%ifnarch i386 
%define buildBOOT 0
%endif
%ifnarch i386 x86_64
%define buildsource 0
%endif

# Second, per-architecture exclusions (ifarch)
%ifarch i386
%define buildsmp 0
%define buildup 0
%endif

#
# Three sets of minimum package version requirements in the form of Conflicts:
# to versions below the minimum
#

#
# First the general kernel 2.4 required versions as per
# Documentation/Changes
#
%define kernel_dot_org_conflicts  ppp <= 2.3.15, pcmcia-cs <= 3.1.20, isdn4k-utils <= 3.0, mount < 2.10r-5, nfs-utils < 0.3.1, e2fsprogs < 1.22 

# 
# Then a series of requirements that are Fedora specific, either 
# because we add patches for something, or the older versions have 
# problems with the newer kernel or lack certain things that make 
# integration in the distro harder than needed.
#
%define package_conflicts  cipe < 1.4.5, tux < 2.1.0, kudzu <= 0.92, initscripts < 6.41, dev < 3.2-7, iptables < 1.2.5-3, nvidia-rh72 <= 1.0, oprofile < 0.4

#
# Several packages had bugs in them that became obvious when the NPTL
# threading code got integrated. 
#
%define nptl_conflicts SysVinit < 2.84-13, pam < 0.75-48, vixie-cron < 3.0.1-73, privoxy < 3.0.0-8, spamassassin < 2.44-4.8.x,  cups < 1.1.17-13

#
# Things that need to be installed before the kernel is, because the %post
# scripts use them.
#
%define kernel_prereq  fileutils, modutils >=  2.4.18, initscripts >= 5.83, mkinitrd >= 3.2.6


Name: kernel
Group: System Environment/Kernel
License: GPLv2
Version: %{kversion}
Release: %{release}
ExclusiveArch: %{all_x86}
ExclusiveOS: Linux
Obsoletes: kernel-modules
Provides: kernel = %{version}
Provides: kernel-drm = 4.1.0, kernel-drm = 4.2.0, kernel-drm = 4.3.0 
Autoreqprov: no
Prereq: %{kernel_prereq}
Conflicts: %{kernel_dot_org_conflicts}
Conflicts: %{package_conflicts}
Conflicts: %{nptl_conflicts}

BuildPreReq: modutils >= 2.4.21-1, patch >= 2.5.4, bash >= 2.03, sh-utils, tar
BuildPreReq: bzip2, findutils, dev, gzip, m4, perl
# BuildProvides: rhbuildsys(ParallelBuild) <= 8
BuildConflicts: rhbuildsys(DiskFree) < 500Mb
BuildRequires: gcc >= 2.96-98
BuildRequires: gcc32

Source0: ftp://ftp.kernel.org/pub/linux/kernel/v2.4/linux-%{kversion}.tar.bz2

Source10: COPYING.modules
Source11: linux-rhconfig.h
Source12: linux-merge-config.awk
Source13: linux-merge-modules.awk
Source14: linux-modules-unsupported

Source20: kernel-%{kversion}-i386.config
Source21: kernel-%{kversion}-i386-BOOT.config
Source22: kernel-%{kversion}-i686.config
Source23: kernel-%{kversion}-i686-smp.config
Source24: kernel-%{kversion}-i586.config
Source25: kernel-%{kversion}-i586-smp.config
Source26: kernel-%{kversion}-i686-bigmem.config
Source27: kernel-%{kversion}-athlon.config
Source28: kernel-%{kversion}-athlon-smp.config
Source29: kernel-%{kversion}-x86_64.config
Source30: kernel-%{kversion}-x86_64-smp.config


#
# Patches 0 through 100 are meant for core subsystem upgrades
#

Patch2: patch-2.4.22-ac1.bz2
Patch3: linux-2.4.22-drop-ac-bits.patch

Patch10: linux-2.4.22-ac1-nptl-A1.patch
Patch11: linux-2.4.20-x86-syscalls.patch
Patch12: linux-2.4.22-ac1-scheduler-update.patch

# 
# Patches 100 through 400 are architecture specific patches
# Each architecture has 20 slots reserved.
#

# x86 is 100 - 120
Patch100: linux-2.4.22-440gx.patch

# X86-64 is 220 - 239
Patch220: linux-2.4.23-amd64.patch
Patch221: linux-2.4.23pre5-x86-64-1.patch
Patch222: linux-2.4.22-amd64-compat.patch

#
# Patches 400 through 599 are the TUX patches
#

Patch400: linux-2.4.22-tux.patch


#
# Patches 600 through 1000 are reserved for bugfixes to the core system
# and patches related to how RPMs are build
#

Patch600: linux-2.4.0-nonintconfig.patch
Patch601: linux-2.4.22-forcegcc32.patch
Patch610: linux-2.4.18-flock.patch
Patch620: linux-2.4.22-unixsocketpollleak.patch
Patch630: linux-2.2.16-rhconfig.patch
Patch640: linux-2.4.2-changeloop.patch
Patch650: linux-2.4.18-loopfixes.patch
Patch660: linux-2.4.2-scsi_scan.patch
Patch661: linux-2.4.22-scsi-whitelist-update.patch

Patch670: linux-2.4.9-kksymoops.patch
Patch680: linux-2.4.9-kallsyms.patch
Patch690: linux-2.4.18-dmi-hall-of-shame.patch

Patch710: linux-2.4.18-laptopbits.patch
Patch720: linux-2.4.22-ac1-nptl-exec-shield.patch
Patch730: linux-2.4.22-striptofile.patch
Patch740: linux-2.4.23-libata.patch

Patch750: linux-2.4.22-acpion.patch
Patch751: linux-2.4.23-ac3-acpi-irq-fixes.patch
Patch752: linux-2.4.23-acpi-read-twice.patch
Patch753: linux-2.4.23-acpi-poweroff.patch

Patch850: linux-2.4.23pre-selected-patches.patch
Patch851: linux-2.4.23-vm.patch
Patch852: linux-2.4.24pre-selected-patches.patch
Patch853: linux-2.4.25pre-selected-patches.patch
Patch854: linux-2.4.26pre-selected-patches.patch
Patch855: linux-2.4.27pre-selected-patches.patch

Patch860: linux-2.4.23-aic-parity.patch
Patch870: linux-2.4.22-ac1-clearfpuexception.patch
Patch880: linux-2.4.27-viro-sparse.patch

#
# Patches 1000 to 5000 are reserved for bugfixes to drivers and filesystems
#

Patch1010: linux-2.4.0-test11-vidfail.patch
Patch1020: linux-2.4.0-e820.patch
Patch1030: linux-2.4.2-page_bitmap.patch
Patch1040: linux-2.4.0-apic-quiet.patch
Patch1050: linux-2.4.20-irixnfs.patch
Patch1060: linux-2.4.3-pcipenalty.patch
Patch1070: linux-2.4.2-keyboardsilence.patch
Patch1080: linux-2.4.7-scsitimeout.patch
Patch1090: linux-2.4.9-freevxfs.patch
Patch1100: linux-2.4.21-oprofile.patch
Patch1101: linux-2.4.21-amd64-oprofile.patch
Patch1102: linux-2.4.21-x86-oprofile.patch
Patch1110: linux-2.4.18-moreunnamed.patch
Patch1120: linux-2.4.20-lvm-updates.patch
Patch1121: linux-2.4.17-lvm-bulkcopy.patch
Patch1122: linux-2.4.17-lvm-pvmove.patch
Patch1123: linux-2.4.18-lvm-VFSlock.patch
Patch1130: linux-2.4.18-input-35215.patch
Patch1140: linux-2.4.20-odirect.patch
Patch1160: linux-2.4.9-scsilun0.patch
Patch1170: linux-2.4.18-netdump.patch
Patch1180: linux-2.4.20-cenatek.patch
Patch1181: linux-2.4.21-intel-esb-drivers.patch
Patch1190: linux-2.4.9-fstat.patch
Patch1220: linux-2.4.7-suspend.patch
Patch1230: linux-2.4.18-orinoco.patch
Patch1250: linux-2.4.18-scsidevices.patch

Patch1260: linux-2.4.18-usb-bug56856.patch
Patch1261: linux-2.4.20-usb-bug82546.patch
Patch1262: linux-2.4.23-usb-bug-110307.patch
Patch1263: linux-2.4.23-usb-bug-90442.patch
Patch1264: linux-2.4.23-usb-bug-107929.patch
Patch1265: linux-2.4.23-usb-bug-110872.patch
Patch1266: linux-2.4.23-usb-bug-storage240.patch
Patch1267: linux-2.4.22-usb-storage-deadlock.patch

Patch1350: linux-2.4.20-laptop-mode.patch
Patch1410: linux-2.4.20-modulesoff.patch
Patch1440: linux-2.4.22-taint.patch
Patch1450: linux-2.4.22-nfsd-flowcontrol.patch
Patch1460: linux-2.4.22-nfsd-tcpcloseonce.patch
Patch1470: linux-2.4.22-lockd-blockedlks.patch
Patch1480: linux-2.4.21-lockd-sysctlif.patch
Patch1490: linux-2.4.22-nfs-accesscache.patch
Patch1500: linux-2.4.22ac4-idescsi-unitialisedvar.patch
Patch1510: linux-2.4.22-cpufreq-updates.patch
Patch1520: linux-2.4.23pre5-ACPI-updates.patch
Patch1530: linux-2.4.23pre-sbp2-init.patch
Patch1540: linux-2.4.23pre-maestrofixes.patch
Patch1550: linux-2.4.22-nfs-retrans.patch
Patch1560: linux-2.4.22-cmpci-leak.patch
Patch1570: linux-2.4.22-nf3dma.patch
Patch1580: linux-2.4.23-nfs-rtomin.patch

#
# Patches 5000 to 6000 are reserved for new drivers
#
Patch5000: linux-2.4.9-addon.patch
Patch5020: linux-2.4.18-wvlan-cs.patch
Patch5030: linux-2.4.18-ecc.patch
Patch5050: linux-2.4.7-tulip.patch
Patch5070: linux-2.4.20-lm_sensors.patch
Patch5080: linux-2.4.9-qla2200.patch
Patch5090: linux-2.4.9-aep.patch
Patch5110: linux-2.4.18-audigy.patch
Patch5190: linux-2.4.0-cipe-1.4.5.patch
Patch5191: linux-2.4.2-cipe.patch
Patch5192: linux-2.4.9-cipenat.patch
Patch5193: linux-2.4.18-cipe-moreinterfaces.patch

#
# Patches 6000 and later are reserved for %if {something} patches
# but don't do that!
#



#
# 10000 to 20000 is for stuff that has to come last due to the
# amount of drivers they touch. But only theese should go here. 
# Not patches you're too lazy for to put in the proper place.
#

# fix a lot of warnings in the plain kernel and previous patches
Patch10010: linux-2.4.1-compilefailure.patch

# several small tweaks not worth their own patch
Patch10020: linux-2.4.18-smallpatches.patch

# add missing MODULE_LICENSE() tags
Patch10030: linux-2.4.18-missing-license-tags.patch

# Numerous fpos race fixes from 2.4.27
Patch10040: linux-2.4.27-fpos-races.patch

# END OF PATCH DEFINITIONS

BuildRoot: %{_tmppath}/kernel-%{KVERREL}-root

%package source
Summary: The source code for the Linux kernel.
Group: Development/System
Prereq: fileutils
Requires: gawk, make
Requires: gcc >= 2.96-98
Requires: gcc32

%package doc
Summary: Various documentation bits found in the kernel source.
Group: Documentation

%description 
The kernel package contains the Linux kernel (vmlinuz), the core of your
Fedora Core Linux operating system.  The kernel handles the basic functions
of the operating system:  memory allocation, process allocation, device
input and output, etc.

%description source
The kernel-source package contains the source code files for the Linux
kernel. These source files are needed to build custom/third party device
drivers. The source files can also be used to build a custom kernel that is
better tuned to your particular hardware, if you are so inclined (and you
know what you're doing).

%description doc
This package contains documentation files form the kernel
source. Various bits of information about the Linux kernel and the
device drivers shipped with it are documented in these files. 

You'll want to install this package if you need a reference to the
options that can be passed to Linux kernel modules at load time.

%package smp
Summary: The Linux kernel compiled for SMP machines.
Group: System Environment/Kernel
Provides: kernel = %{version}
Provides: kernel-drm = 4.1.0, kernel-drm = 4.2.0, kernel-drm = 4.3.0
Prereq: %{kernel_prereq}
Conflicts: %{kernel_dot_org_conflicts}
Conflicts: %{package_conflicts}
Conflicts: %{nptl_conflicts}

%description smp
This package includes a SMP version of the Linux kernel. It is
required only on machines with two or more CPUs.

Install the kernel-smp package if your machine uses two or more CPUs.

%package bigmem
Summary: The Linux Kernel for machines with more than 4 Gigabyte of memory.
Group: System Environment/Kernel
Provides: kernel = %{version}
Provides: kernel-drm = 4.1.0, kernel-drm = 4.2.0, kernel-drm = 4.3.0
Prereq: %{kernel_prereq}
Conflicts: %{kernel_dot_org_conflicts}
Conflicts: %{package_conflicts}
Conflicts: %{nptl_conflicts}
# upto and including kernel 2.4.9 rpms, the 4Gb+ kernel was called kernel-enterprise
Obsoletes: kernel-enterprise < 2.4.10


%description bigmem
This package includes a kernel that has appropriate configuration options
enabled for Pentium III machines with 4 Gigabyte of memory or more.

%package BOOT
Summary: The version of the Linux kernel used on installation boot disks.
Group: System Environment/Kernel
Provides: kernel = %{version}
Prereq: %{kernel_prereq}
Conflicts: %{kernel_dot_org_conflicts}
Conflicts: %{package_conflicts}
Conflicts: %{nptl_conflicts}

%description BOOT
This package includes a trimmed down version of the Linux kernel.
This kernel is used on the installation boot disks only and should not
be used for an installed system, as many features in this kernel are
turned off because of the size constraints of floppy disks.

%prep

%setup -q -n %{name}-%{version} -c
cd linux-%{version}

#
# Patches 0 through 100 are meant for core subsystem upgrades
# 

# update to recent upstream
%patch2 -p1

# Drop selected bits of -ac
%patch3 -p1

#
# The big NPTL patch
#
%patch10 -p1
%patch11 -p1
%patch12 -p1

# 
# Patches 100 through 400 are architecture specific patches
# Each architecture has 20 slots reserved.
#

# x86
# Fix up 440GX interrupt routing.
%patch100 -p1

#
# X86-64
#

# amd64 megapatch.
%patch220 -p1
# Upstream x86-64.org patch
%patch221 -p1
# AMD64 ia32 syscall patch
%patch222 -p1

#
# Patches 400 through 500 are the TUX patches
#
%patch400 -p1

#
# Patches 600 through 1000 are reserved for bugfixes to the core system
# and patches related to how RPMs are build
#


# This patch adds a "make oldconfig_nonint" which is non-interactive and
# also gives a list of missing options at the end. Useful for automated
# builds (as used in the Fedora buildsystem).
%patch600 -p1

#
# Force usage of gcc32 to build the kernel.
#
%patch601 -p1

#
# flock() accounting is broken; disable for 2.4 kernels
#
%patch610 -p1

#
# Unix socket descriptor leak with poll() fix.
#
%patch620 -p1

#
# Make "make oldconfig" smarter and have it use the config file for the
# currently running kernel as default.
#
%patch630 -p1
# Al Viro's loopback patch 
%patch640 -p1
%patch650 -p1

# fix lun probing on multilun RAID chassis
%patch660 -p1
# update SCSI whitelist.
%patch661 -p1

#
# kksymoops - automatically decode oopses/backtraces from within the kernel
%patch670 -p1
%patch680 -p1

#
#
# update the list of vendors who can't program a bios
#
# %patch690 -p1

#
# add some laptop power consumption improvements
#
%patch710 -p1

#
# Non exec stack patch
#
%ifnarch x86_64
%patch720 -p1
%endif

#
# Jakub's __ksymtab stripping fix.
#
%patch730 -p1

#
# Enable (and fix) compilation of libata for SATA devices.
#
%patch740 -p1

#
# Default ACPI to off.  "acpi=on" will turn on.
#
%patch750 -p1

#
# Various IRQ routing fixes for ACPI
#
%patch751 -p1

#
# Don't execute acpi code twice. (upstream bugzilla #726)
#
%patch752 -p1

#
# Fix power-off on shutdown with ACPI
#
%patch753 -p1

#
# Various small fixes from 2.4.23pre
#
%patch850 -p1

#
# VM changes from 2.4.23
#
%patch851 -p1

#
# Various fixes from 2.4.24->27
#
%patch852 -p1
%patch853 -p1
%patch854 -p1
%patch855 -p1

#
# Fix aic7xxx/79xx causes PCI PARITY ERROR
#
%patch860 -p1

#
# Fix FPU exception handling local DoS
#
%patch870 -p1

# Bugs fixed with sparse by Al Viro.
%patch880 -p1

#
# Patches 1000 to 5000 are reserved for bugfixes to drivers and filesystems
#


# add vidfail capability; 
# without this patch specifying a framebuffer on the kernel prompt would
# make the boot stop if there's no supported framebuffer device; this is bad
# for the installer floppy that wants to automatically fall back to textmode
# in that case
%patch1010 -p1

#
# add e820 reporting in /proc; needed for anaconda to find out which
# kernels to install
#
%patch1020 -p1

# conservative zone bitmaps to prevent potential memory corruption
%patch1030 -p1
#
# Silence the APIC errors a bit; they are harmless mostly and can happen
# in normal use.
#
%patch1040 -p1

#
# NFS patch fron trond:
# A patch that papers over an IRIX bug. In glibc-2.2 and above, they use
# the new getdents64() syscall even for the 32-bit readdir. As a result, they
# have problems coping with the unsigned 64-bit d_off values that may get
# invalidly returned by some NFSv3 servers, since the values are specified
# to be signed.
# NOTE: You might still have to set the '32bitclients' export option on
# some IRIX servers.

%patch1050 -p1

#
# change IRQ penalty for IRQ 12
# This tries to avoid assigning cardbus irq's to irq12 which is "special"
# for certain laptops that emulate PS/2 hardware via SMM mode.
#
%patch1060 -p1

#
# Silence the PS/2 code for USB keyboards
# It's not uncommon to have no PS/2 keyboard these days
#
%patch1070 -p1

#
# make the scsi timeout longer; 30 seconds is too short for big raid arrays
# with lots of large concurrent requests in flight
#
%patch1080 -p1

#
# make freevxfs autoload when doing mount -t vxfs /dev/foo /mnt/bar
#
%patch1090 -p1


#
# Oprofile.
#
%patch1100 -p1
%patch1101 -p1
%patch1102 -p1

#
# allow more unnamed mounts by borrowing old, unused major devices
# This is needed if you want more than 255 NFS mounts; some people use the
# "one mount per user" automount setup and they need this if they have more
# than 255 users.
#
%patch1110 -p1



#
# LVM updates:
#
# add important bugfixes from Sistina's LVM-1.0.3
%patch1120 -p1
# Add bulk-copy-via-kiobufs code
%patch1121 -p1
# Add locked copy-and-remap pvmove code
%patch1122 -p1
# LVM atomic snapshot patch: quiesce the filesystem automatically when
# taking snapshots.
%patch1123 -p1

#
# Gamepad (all kbds and mice were affected, but we kludged arount it in hplug)
# autoload the proper modules
%patch1130 -p1

#
# change O_DIRECT handling for 2.4.9 compatibility
#
# %patch1140 -p1


#
# allow ioctls to lun0 even if there's no lun0 (needed for some FC kit)
#
%patch1160 -p1

#
# Netdump / Netconsole - crashdumps and kernel messages over
# the network via a lightweight UDP stack that can run with interrupts
# disabled
#
%patch1170 -p1

#
# add cenatek PCI ID's to the ide layer
#
%patch1180 -p1
# add Intel ICH ESB DMA support (and i810 audio)
%patch1181 -p1


#
# some broken and defective applications assume that they can fstat() on a
# pipe and meaningful interpret the value of i_size. Apparently the
# arbitrary value 2.2 put there was good enough so we try to emulate that,
# since we prefer to keep compatibility within the RHL 7.x series. This is
# a gross hack that will go away as soon as possible.
#
%if %{sevenexcompat}
%patch1190 -p1
%endif


# 
# Some bioses "forget" to restore pci config space properly on resume;
# the following patch helps them a bit with this
#
%patch1220 -p1

#
# Older orinoco_cs driver that fixes some laptops
#
%patch1230 -p1


#
# Make use of all of the 256 scsi devices available
#
%patch1250 -p1



# 56856 - SDDR-31 media check
%patch1260 -p1
# 82546 - Yukihiro's pet, will disappear eventually
%patch1261 -p1
# 110307 - Printer problem
%patch1262 -p1
# USB serial helper bug (#90442)
%patch1263 -p1
# USB serial visors bug (#107929)
%patch1264 -p1
# USB storage (#110872)
%patch1265 -p1
# Storage 240
%patch1266 -p1
# USB Storage PF_MEMALLOC deadlock avoidance.
%patch1267 -p1

# "laptop mode" that tries to batch IO's in order to avoid spurious
# spin-ups of laptop disks
%patch1350 -p1

#
# add the much requested "off" option for moduleloading
#
%patch1410 -p1

#
# The silicon image proprietary software raid module accidentally had
# an incorrect MODULE_LICENSE; fix that up.
#
%patch1440 -p1

#
# Reworks flowcontrol for TCP mounts
#
%patch1450 -p1

#
# Make sure nfs/tcp socket only gets closed once
#
%patch1460 -p1

#
# kNFSd/lockd - Blocked lockd not being purged correctly 
#
%patch1470 -p1

#
# Make lockd ports configurable with sysctl
#
%patch1480 -p1

#
# NFS permission bits efficiency patch
#
%patch1490 -p1

#
# ide-scsi was using an uninitialised variable.
#
%patch1500 -p1

#
# Fixes for cpufreq.
#
%patch1510 -p1

#
# More ACPI patches from 2.4.23pre5
#
%patch1520 -p1

#
# Firewire use after free fix.
#
%patch1530 -p1

#
# Fix up some problems in the Maestro sound driver
#
%patch1540 -p1

#
# Reduce NFS retransmits on udp reads by ~4-5%
# Also includes UDP RTO fixes.
#
%patch1550 -p1

# cmpci driver leaks waitqueues.
%patch1560 -p1

# NForce3 IDE driver patches.
%patch1570 -p1

# increase the Minimal Round Trip Timeout to cut down on
# annoying "NFS server not responding" messages.
%patch1580 -p1


#
# Patches 5000 to 6000 are reserved for new drivers
#
# generic drivers/addon infrastructure
%patch5000 -p1
# old wvlan_cs driver
%patch5020 -p1
# ECC reporting module
%patch5030 -p1
# add tulip_old driver (2.4.3 version)
%patch5050 -p1
# LM-Sensors 2.6.5
%patch5070 -p1
# qla2x00
%patch5080 -p1
# AEP SSL Accelerator card
%patch5090 -p1
# cipe
%patch5190 -p1
%patch5191 -p1
%patch5192 -p1
%patch5193 -p1

#
# Soundblaster Live! Audigy driver
#
%patch5110 -p1


#
# final stuff
#

#
# lots of small fixes to warnings and other silly bugs
#
%patch10010 -p1

# several small tweaks
%patch10020 -p1

# 
# Add MODULE_LICENSE() to the modules missing it
#
%patch10030 -p1

# fpos race fixes from 2.4.27
%patch10040 -p1

# END OF PATCH APPLICATIONS

cp %{SOURCE10} Documentation/

mkdir configs

cp -fv $RPM_SOURCE_DIR/kernel-%{kversion}-i?86*.config configs
cp -fv $RPM_SOURCE_DIR/kernel-%{kversion}-athlon*.config configs
cp -fv $RPM_SOURCE_DIR/kernel-%{kversion}-x86_64*.config configs

# make sure the kernel has the sublevel we know it has...
perl -p -i -e "s/^SUBLEVEL.*/SUBLEVEL = %{sublevel}/" Makefile

# get rid of unwanted files
find . -name "*.orig" -exec rm -fv {} \;
find . -name "*~" -exec rm -fv {} \;

###
### build
###
%build

cd linux-%{version}

DependKernel() {
    # is this a special kernel we want to build?
    if [ -n "$2" ] ; then
	Config=$1-$2
	KernelVer=%{version}-%{release}$2
	echo MAKE DEPEND FOR $2 $1 KERNEL...
    else
	Config=$1
	KernelVer=%{version}-%{release}
	echo MAKE DEPEND FOR up $1 KERNEL...
    fi
    make -s mrproper
# We used to copy the config file to arch/.../defconfig, but for some time now
# the Configure script has been smart enough to override this with the
# config file found in configs/kernel-%{kversion}-${config}.config when it
# finds a valid /boot/kernel.h file.  As a result, making an RPM on a
# machine that has a valid kernel.h file will result in scripts/Configure
# overriding our config entries with whatever it thinks is appropriate
# (aka, if you try an i386 build on a machine with an i686 smp kernel.h file
# then you will get an i686 smp kernel in an i386 package).  So, instead we
# put our config file in .config, which is considered the ultimate source
# of default config information by make oldconfig.  Also, copy the config
# files out of the configs directory instead of the RPM_SOURCE_DIR.  It
# doesn't make any sense to put them in there and then not use them,
# especially when we could have a patch that modifies the config files
# as part of the bigmem patch set (or any other patch, it just makes
# us more flexible to use the configs from the configs directory).  Finally,
# since make mrproper wants to wipe out .config files, we move our mrproper
# up before we copy the config files around.
    cp configs/kernel-%{kversion}-$Config.config .config
    # make sure EXTRAVERSION says what we want it to say
    perl -p -i -e "s/^EXTRAVERSION.*/EXTRAVERSION = -%{release}$2/" Makefile

    # We do make oldconfig twice, because make oldconfig tends to get stuff wrong
    # otherwise in the light of cyclic dependencies

    make -s oldconfig_nonint
    make -s oldconfig_nonint
    make -s %{?_smp_mflags} dep
    make -s include/linux/version.h 
}

BuildKernel() {
    if [ -n "$1" ] ; then
	Config=%{_target_cpu}-$1
	KernelVer=%{version}-%{release}$1
	DependKernel %{_target_cpu} $1
	echo BUILDING A KERNEL FOR $1 %{_target_cpu}...
    else
	Config=%{_target_cpu}
	KernelVer=%{version}-%{release}
	DependKernel %{_target_cpu}
	echo BUILDING THE NORMAL KERNEL for %{_target_cpu}...
    fi

    make -s %{?_smp_mflags} CC=gcc32 CFLAGS_KERNEL="-Wno-unused -g" bzImage 
    make -s %{?_smp_mflags} CC=gcc32 CFLAGS_KERNEL="-Wno-unused" modules || exit 1
    
    # first make sure we are not loosing any .ver files to make mrporper's
    # removal of zero sized files.
    find include/linux/modules -size 0 | while read file ; do \
	echo > $file
    done
    # Start installing stuff
    mkdir -p $RPM_BUILD_ROOT/boot
    install -m 644 System.map $RPM_BUILD_ROOT/boot/System.map-$KernelVer
    install -m 644 configs/kernel-%{kversion}-$Config.config $RPM_BUILD_ROOT/boot/config-$KernelVer
    mkdir -p $RPM_BUILD_ROOT/dev/shm
    cp arch/*/boot/bzImage $RPM_BUILD_ROOT/boot/vmlinuz-$KernelVer

    mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer
    make -s INSTALL_MOD_PATH=$RPM_BUILD_ROOT modules_install KERNELRELEASE=$KernelVer


    cp vmlinux $RPM_BUILD_ROOT/lib/modules/$KernelVer
    ln -s ../lib/modules/$KernelVer/vmlinux $RPM_BUILD_ROOT/boot/vmlinux-$KernelVer

#   move the modules in the SOURCE14 file to the unsupported/ directory
    mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/unsupported
    pushd $RPM_BUILD_ROOT/lib/modules/$KernelVer/kernel ; {
	 tar cf - `cat %{SOURCE14}`  | tar xf - -C ../unsupported
	 rm -rf `cat %{SOURCE14}` 
    } ; popd


#   mark the modules executable
    if [ "$1" != "BOOT" ] ; then
     	find $RPM_BUILD_ROOT/lib/modules/$KernelVer -name "*.o" -type f  | xargs chmod u+x
    fi

#   now separate out the debug info from the modules/vmlinux
   /usr/lib/rpm/find-debuginfo.sh %{_builddir}/%{?buildsubdir} || :

#   and compress the modules for space saving
#    if [ "$1" != "BOOT" ] ; then
#    	find $RPM_BUILD_ROOT/lib/modules/$KernelVer -name "*.o" -type f  | xargs gzip -9
#    fi

#   remove files that will be auto generated by depmod at rpm -i time
    rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.*
#   remove legacy pcmcia symlink that's no longer useful
    rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/pcmcia


}

SaveHeaders() {
    echo "SAVING HEADERS for $1 $2"
    # deal with the kernel headers that are version specific
    mkdir -p $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/savedheaders/$2/$1
    install -m 644 include/linux/autoconf.h \
	$RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/savedheaders/$2/$1/autoconf.h
    install -m 644 include/linux/version.h \
	$RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/savedheaders/$2/$1/version.h
    mv include/linux/modules \
	$RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/savedheaders/$2/$1/
    echo $2 $1 ../../savedheaders/$2/$1/ >> $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/savedheaders/list
}

###
# DO it...
###

rm -rf $RPM_BUILD_ROOT

%if %{buildbigmem}
BuildKernel bigmem
SaveHeaders bigmem %{_target_cpu}
%endif

%if %{buildsmp}
BuildKernel smp
SaveHeaders smp %{_target_cpu}
%endif

%if %{buildBOOT}
BuildKernel BOOT
SaveHeaders BOOT %{_target_cpu}
%endif

# UNIPROCESSOR KERNEL
%if %{buildup}
BuildKernel
SaveHeaders up %{_target_cpu}
%endif

#
# i386 is a special case; the kernel source there is supposed
# to also have the modversions/headers/config of i586/i686/athlon
#
%ifarch i386
DependKernel i586
SaveHeaders up i586
DependKernel i686
SaveHeaders up i686
DependKernel athlon
SaveHeaders up athlon
DependKernel i586 smp
SaveHeaders smp i586
DependKernel i686 smp
SaveHeaders smp i686
DependKernel i686 bigmem
SaveHeaders bigmem i686
DependKernel athlon smp
SaveHeaders smp athlon
%endif




###
### install
###

%install

cd linux-%{version}
mkdir -p $RPM_BUILD_ROOT/{boot,sbin}

for i in $RPM_BUILD_ROOT/lib/modules/*; do
  rm -f $i/build 
  ln -sf ../../../usr/src/linux-%{KVERREL} $i/build
done

# architectures that don't build kernel-source (i586/i686/athlon) are done here
# and can skip the next steps; the source building will do the header magic instead.

%if %{buildsource}

mkdir -p $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}
# generated binaries should be removed; we don't want kernel-source to depend on glibc
rm -f drivers/net/hamradio/soundmodem/gentbl scripts/mkdep
# copy the source over
tar cf - . | tar xf - -C $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}
# set the EXTRAVERSION to <version>custom, so that people who follow a kernel building howto
# don't accidentally overwrite their currently working moduleset
perl -p -i -e "s/^EXTRAVERSION.*/EXTRAVERSION = -%{release}custom/" $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/Makefile
install -m 644 %{SOURCE10}  $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}

#clean up the destination
make -s mrproper -C $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}
cp configs/kernel-%{kversion}-%{_target_cpu}.config $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/.config
make -s oldconfig_nonint -C $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}
make -s symlinks -C $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}
make -s include/linux/version.h -C $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}

#this generates modversions info which we want to include and we may as
#well include the depends stuff as well, after we fix the paths
make -s depend -C $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}
find $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL} -name ".*depend" | \
while read file ; do
    mv $file $file.old
    sed -e "s|[^ ]*\(/usr/src/linux\)|\1|g" < $file.old > $file
    rm -f $file.old
done

# Try to put some smarter autoconf.h and version.h files in place
pushd $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/include/linux ; {
rm -rf modules modversions.h autoconf.h version.h
cat > modversions.h <<EOF
#ifndef _LINUX_MODVERSIONS_H
#define _LINUX_MODVERSIONS_H
#include <linux/rhconfig.h>
#include <linux/modsetver.h>
EOF
echo '#include <linux/rhconfig.h>' > autoconf.h
list=`find ../../savedheaders/* -name '*.ver' -exec basename '{}' \; | sort`
mkdir modules
for l in $list; do
    sed 's,$,modules/'$l, ../../savedheaders/list | awk -f %{SOURCE13} > modules/$l
    touch -r modules/$l modules/`basename $l .ver`.stamp
    echo '#include <linux/modules/'$l'>' >> modversions.h
done
echo '#endif' >> modversions.h
sed 's,$,autoconf.h,' ../../savedheaders/list | awk -f %{SOURCE12} >> autoconf.h
install -m 644 %{SOURCE11} rhconfig.h
echo "#include <linux/rhconfig.h>" >> version.h
keyword=if
for i in smp BOOT bigmem up ; do
# When we build in an i386, we don't have an bigmem header directory
# in savedheaders/i386/bigmem.  We also don't have a BOOT directory
# anywhere except in savedheaders/i386.  So, we need to use this method
# of determining if a kernel version string needs to be included in the
# version.h file
    verh=`echo ../../savedheaders/*/$i/version.h | awk ' { print $1 } '`
    if [ -n "$verh" -a -f "$verh" ]; then
	if [ "$i" = up ]; then
	    if [ "$keyword" = if ]; then
		echo "#if 0" >> version.h
	    fi
  	    echo "#else" >> version.h
	else
	    echo "#$keyword defined(__module__$i)" >> version.h
	    keyword=elif
	fi
	grep UTS_RELEASE $verh >> version.h
    fi
done
echo "#endif" >> version.h
if [ -f ../../savedheaders/%{_target_cpu}/up/version.h ] ; then
    # keep to a standard normally
    HEADER_FILE=../../savedheaders/%{_target_cpu}/up/version.h
else
    # test build not including uniprocessor, must get info from somewhere
    HEADER_FILE=$(ls ../../savedheaders/%{_target_cpu}/*/version.h | head -1)
fi
grep -v UTS_RELEASE $HEADER_FILE >> version.h
rm -rf ../../savedheaders
} ; popd
touch $RPM_BUILD_ROOT/boot/kernel.h-%{kversion}

for i in $RPM_BUILD_ROOT/lib/modules/*; do
  rm -f $i/modules.*
done

%endif
rm -rf $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/savedheaders

# fix up the tmp_include_depends file wrt the buildroot path
perl -p -i -e "s|$RPM_BUILD_ROOT||g" $RPM_BUILD_ROOT/usr/src/linux-%{KVERREL}/tmp_include_depends

###
### clean
###

%clean
rm -rf $RPM_BUILD_ROOT

###
### scripts
###

# do this for upgrades...in case the old modules get removed we have
# loopback in the kernel so that mkinitrd will work.
%pre 
/sbin/modprobe loop 2> /dev/null > /dev/null  || :
exit 0

%pre smp
/sbin/modprobe loop 2> /dev/null > /dev/null  || :
exit 0

%pre bigmem
/sbin/modprobe loop 2> /dev/null > /dev/null  || :
exit 0

%post 
cd /boot
ln -sf vmlinuz-%{KVERREL} vmlinuz
ln -sf System.map-%{KVERREL} System.map
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade
[ -x /sbin/mkkerneldoth ] && /sbin/mkkerneldoth
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --mkinitrd --depmod --install %{KVERREL}


%post smp
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade
[ -x /sbin/mkkerneldoth ] && /sbin/mkkerneldoth
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --mkinitrd --depmod --install %{KVERREL}smp

%post bigmem
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade
[ -x /sbin/mkkerneldoth ] && /sbin/mkkerneldoth
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --mkinitrd --depmod --install %{KVERREL}bigmem

%post BOOT
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade
[ -x /sbin/mkkerneldoth ] && /sbin/mkkerneldoth
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --mkinitrd --depmod --install %{KVERREL}BOOT


# Allow clean removal of modules directory
%preun 
/sbin/modprobe loop 2> /dev/null > /dev/null  || :
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --rminitrd --rmmoddep --remove %{KVERREL}


%preun smp
/sbin/modprobe loop 2> /dev/null > /dev/null  || :
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --rminitrd --rmmoddep --remove %{KVERREL}smp


%preun bigmem
/sbin/modprobe loop 2> /dev/null > /dev/null  || :
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --rminitrd --rmmoddep --remove %{KVERREL}bigmem


%preun BOOT
/sbin/modprobe loop 2> /dev/null > /dev/null  || :
[ -x /sbin/new-kernel-pkg ] && /sbin/new-kernel-pkg --rminitrd --rmmoddep --remove %{KVERREL}BOOT


# We need this here because we don't prereq kudzu; it could be
# installed after the kernel during an OS upgrade
%triggerin -- kudzu
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade || :

%triggerin smp -- kudzu
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade || :

%triggerin bigmem -- kudzu
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade || :

%triggerin BOOT -- kudzu
[ -x /usr/sbin/module_upgrade ] && /usr/sbin/module_upgrade || :


# Old kernel-headers packages owned include symlinks; new
# ones just make them so that we can have multiple kernel-headers
# packages installed.

%triggerpostun source -- kernel-headers < 2.2.16
cd /usr/src
rm -f %{kslnk}
ln -snf linux-%{KVERREL} %{kslnk}
exit 0

%post source
cd /usr/src
rm -f %{kslnk}
ln -snf linux-%{KVERREL} %{kslnk}

%postun source
#
# fix up the /usr/src/linux-2.4 symlink to point to another kernel-source dir if possible
#
if [ -L /usr/src/%{kslnk} ]; then 
    if [ -L /usr/src/%{kslnk} -a `ls -ld /usr/src/%{kslnk} 2>/dev/null| awk '{ print $11 }'` = "linux-%{KVERREL}" ]; then
	[ $1 = 0 ] && rm -f /usr/src/%{kslnk}
    fi
fi
exit 0

###
### file lists
###

%if %{buildup}
%files 
%defattr(-,root,root)
/boot/vmlinux-%{KVERREL}
/boot/vmlinuz-%{KVERREL}
/boot/System.map-%{KVERREL}
/boot/config-%{KVERREL}
%dir /lib/modules
%dir /dev/shm
/lib/modules/%{KVERREL}/
%endif

%if %{buildsmp}
%files smp
%defattr(-,root,root)
/boot/vmlinux-%{KVERREL}smp
/boot/vmlinuz-%{KVERREL}smp
/boot/System.map-%{KVERREL}smp
/boot/config-%{KVERREL}smp
%dir /lib/modules
%dir /dev/shm
/lib/modules/%{KVERREL}smp/
%endif

%if %{buildbigmem}
%files bigmem
%defattr(-,root,root)
/boot/vmlinux-%{KVERREL}bigmem
/boot/vmlinuz-%{KVERREL}bigmem
/boot/System.map-%{KVERREL}bigmem
/boot/config-%{KVERREL}bigmem
%dir /lib/modules
%dir /dev/shm
/lib/modules/%{KVERREL}bigmem/
%endif

%if %{buildBOOT}
%files BOOT
%defattr(-,root,root)
/boot
%dir /lib/modules
%dir /dev/shm
/lib/modules/%{KVERREL}BOOT

%endif

# only some architectures need kernel-source and kernel-doc

%if %{buildsource}

%files source
%defattr(-,root,root)
%dir /usr/src/linux-%{KVERREL}
/usr/src/linux-%{KVERREL}/COPYING*
/usr/src/linux-%{KVERREL}/CREDITS
/usr/src/linux-%{KVERREL}/Documentation
/usr/src/linux-%{KVERREL}/MAINTAINERS
/usr/src/linux-%{KVERREL}/Makefile
/usr/src/linux-%{KVERREL}/README
/usr/src/linux-%{KVERREL}/REPORTING-BUGS
/usr/src/linux-%{KVERREL}/Rules.make
/usr/src/linux-%{KVERREL}/arch
/usr/src/linux-%{KVERREL}/drivers
/usr/src/linux-%{KVERREL}/crypto
/usr/src/linux-%{KVERREL}/fs
/usr/src/linux-%{KVERREL}/init
/usr/src/linux-%{KVERREL}/ipc
/usr/src/linux-%{KVERREL}/kernel
/usr/src/linux-%{KVERREL}/lib
/usr/src/linux-%{KVERREL}/mm
/usr/src/linux-%{KVERREL}/net
/usr/src/linux-%{KVERREL}/scripts
/usr/src/linux-%{KVERREL}/configs
/usr/src/linux-%{KVERREL}/include
/usr/src/linux-%{KVERREL}/tmp_include_depends
/usr/src/linux-%{KVERREL}/.depend
/usr/src/linux-%{KVERREL}/.hdepend

%files doc
%defattr(-,root,root)
%doc linux-%{version}/Documentation/*

%endif

%changelog
* Wed Aug  4 2004 Dave Jones <davej@redhat.com>
- Fix various fpos races. (CAN-2004-0415)

* Wed Jul  7 2004 Dave Jones <davej@redhat.com>
- Updates to usbserial post_helper (Pete Zaitcev)

* Thu Jul  1 2004 Dave Jones <davej@redhat.com>
- add patch to fix missing checks in fchown() (CAN-2004-0497)
- Drop Broadcom 5820 driver due to code quality concerns.

* Sat Jun 19 2004 Dave Jones <davej@redhat.com>
- Reenable support for >4GB of memory in the SMP kernel. (#122690)

* Thu Jun 17 2004 Dave Jones <davej@redhat.com>
- Drop bogus fix for FBIOGETCMAP ioctl which was unneeded.
- Numerous userspace pointer reference bugs found with
  the sparse tool by Al Viro (CAN-2004-0495)

* Mon Jun 14 2004 Dave Jones <davej@redhat.com>
- Fix FPU exception handling local DoS
- Fix memory leak to userspace in e1000 driver. (CAN-2004-0535)
- Fix incorrect permissions on /proc/scsi/qla2300/HbaApiNode
- Fix potential memory access to free memory in /proc handling

* Wed May 12 2004 Dave Jones <davej@redhat.com>
- Build the SMP 586 kernel again (#111871)

* Wed Apr 28 2004 Dave Jones <davej@redhat.com>
- refile_inodes() stability fix. (#121732)

* Wed Apr 21 2004 Dave Jones <davej@redhat.com>
- Fix memory leak in do_fork() error path
- Really fix CAN-2004-0109 and previous mremap issue.
  These patches were not applied in the previous errata.
- Fix information leak in XFS (CAN-2004-0133)
- Fix potential local denial of service in sb16 driver (CAN-2004-0178)
- Fix information leak in JFS (CAN-2004-0181)
- Add range checking to i810_dma() in DRM driver.
- Make ioctl(FBIOGETCMAP) use copy_to_user() rather than memcpy()
- Fix information leak in cpufreq userspace ioctl. (CAN-2004-0228)
- Fix possible buffer overflow in panic() (CAN-2004-0394)
- Fix setsockopt MCAST_MSFILTER integer overflow. (CAN-2004-0424)

* Tue Apr 13 2004 Dave Jones <davej@redhat.com>
- mremap NULL pointer dereference fix
- Disable low latency patch, pending investigation into crashes.
- Additional r128 DRM check. (CAN-2004-0003)
- Bounds checking in ISO9660 filesystem. (CAN-2004-0109)
- Fix Information leak in EXT3 (CAN-2004-0177)

* Wed Feb 25 2004 Dave Jones <davej@redhat.com>
- Default 8139too driver to PIO as some machines lock up.

* Wed Feb 18 2004 Dave Jones <davej@redhat.com>
- Fix security problem in gamma DRI driver.
- Drop broken fix for 92129

* Tue Feb 17 2004 Dave Jones <davej@redhat.com>
- Fix leak in SSTFB driver.

* Sat Feb 14 2004 Dave Jones <davej@redhat.com>
- aacraid fix for #92129

* Fri Feb 13 2004 Dave Jones <davej@redhat.com>
- Fix building of vt8231.o

* Thu Feb  5 2004 Dave Jones <davej@redhat.com>
- Check do_mremap return values (CAN-2004-0077)

* Mon Feb 02 2004 Dave Jones <davej@redhat.com>
- Disable stack overflow checking.
- More bits from 2.4.25pre
  - Fix ipt_conntrack/ipt_state module refcounting.
  - Zero last byte of mount option page
  - AMD64 update
  - Fix deep stack usage in ncpfs

* Mon Jan 26 2004 Dave Jones <davej@redhat.com>
- Fix error in wan config files that broke some configurators.
- Reenable VIA DRI.

* Fri Jan 16 2004 Dave Jones <davej@redhat.com>
- Merge VM updates from post 2.4.22
- Fix AMD64 ptrace security hole. (CAN-2004-0001)
- Fix NPTL SMP hang.
- Merge bits from 2.4.25pre
  - R128 DRI limits checking. (CAN-2004-0003)
  - Various ymfpci fixes.
  - tmpfs readdir does not update dir atime
  - Minor IPV4/Netfilter changes.
  - Fix userspace dereferencing bug in USB Vicam driver.
- Merge a few more bits from 2.4.23pre
  - Numerous tmpfs fixes.
  - Use list_add_tail in buffer_insert_list
  - Correctly dequeue SIGSTOP signals in kupdated
- Update laptop-mode patch to match mainline.

* Wed Jan 14 2004 Dave Jones <davej@redhat.com>
- Merge a few more missing netfilter fixes from upstream.

* Tue Jan 13 2004 Dave Jones <davej@redhat.com>
- Reenable Tux.
- Lots of updates from the 2.4.23 era.

* Mon Jan 12 2004 Dave Jones <davej@redhat.com>
- Avoid deadlocks in USB storage.

* Fri Jan  9 2004 Dave Jones <davej@redhat.com>
- Fix thread creation race.

* Thu Jan  8 2004 Dave Jones <davej@redhat.com>
- USB storage: Make Pentax Optio S4 work
- Config file tweaking. Only enable CONFIG_SIBLINGS_2
  on the kernels that need it.

* Wed Jan  7 2004 Dave Jones <davej@redhat.com>
- Merge several EXT2/3 fixes from 2.4.25pre
  - EXT2/3 fixes.
    - Reclaim pages in truncate
    - 2.6 EA symlink compatibility
    - forward-compatibility: online resizing
    - Allow filesystems with expanded inodes to be mounted
    - Handle j_commit_interval == 0
  - IDE timeout race fix
- Merge some 2.4.23pre patches that were missed.
  - Make root a special case for per-user process limits.
  - out_of_memory() locking
  - Drop module count if lockd reclaimer thread failed to start
  - Fix potential fsync() race condition
- s/Red Hat/Fedora/ in specfile (#112992)
- Add PCI ident for new Intel e1000 card. (#105046)
- Actually wire up 3c59x ethtool ioctl.
- Fix up numeric sysctls to match mainline.

* Tue Jan  6 2004 Dave Jones <davej@redhat.com>
- Fix leaks in rtc drivers (CAN-2003-0984) (#112893)

* Wed Dec 24 2003 Dave Jones <davej@redhat.com>
- Fix mremap corner case.

* Tue Dec 23 2003 Dave Jones <davej@redhat.com>
- Numerous USB fixes (#110307, #90442, #107929, #110872)

* Tue Dec 16 2003 Dave Jones <davej@redhat.com>
- Fix leak in CDROM IOCTL. (#112249)

* Mon Dec 15 2003 Dave Jones <davej@redhat.com>
- Upgrade to latest libata

* Tue Dec 09 2003 Dave Jones <davej@redhat.com>
- Merge Tux.
- Enable support for G450/G550 dualhead framebuffer.

* Fri Dec 05 2003 Dave Jones <davej@redhat.com>
- increase the Minimal Round Trip Timeout to cut down on annoying
  "NFS server not responding" messages.
- Increase Exec-Shield strictness.

* Mon Dec 01 2003 Dave Jones <davej@redhat.com>
- sys_tgkill wasn't enabled on IA32.

* Sun Nov 30 2003 Dave Jones <davej@redhat.com>
- Process scheduler fix.
  When doing sync wakeups we must not skip the notification of other cpus if
  the task is not on this runqueue.

* Wed Nov 26 2003 Justin M. Forbes <64bit_fedora@comcast.net>
- Merge required ia32 syscalls for AMD64
- [f]truncate64 for 32bit code fix

* Mon Nov 24 2003 Dave Jones <davej@redhat.com>
- Fix power-off on shutdown with ACPI.
- Add missing part of recent cmpci fix
- Drop CONFIG_NR_CPUS patch which was problematic.
- Fold futex-fix into main futex patch.
- Fix TG3 tqueue initialisation.
- Various NPTL fixes.

* Fri Nov 14 2003 Dave Jones <davej@redhat.com>
- Drop netfilter change which proved to be bad upstream.

* Thu Nov 13 2003 Justin M. Forbes <64bit_fedora@comcast.net>
- Fix NForce3 DMA and ATA133 on AMD64

* Wed Nov 12 2003 Dave Jones <davej@redhat.com>
- Fix syscall definitions on AMD64

* Tue Nov 11 2003 Dave Jones <davej@redhat.com>
- Fix Intel 440GX Interrupt routing.
- Fix waitqueue leak in cmpci driver.

* Mon Nov 10 2003 Dave Jones <davej@redhat.com>
- Kill noisy warnings in the DRM modules.
- Merge munged upstream x86-64.org patch for various AMD64 fixes.

* Mon Nov 3 2003 Dave Jones <davej@redhat.com>
- Further cleanups related to AMD64 build.

* Fri Oct 31 2003 Dave Jones <davej@redhat.com>
- Make AMD64 build.

* Wed Oct 29 2003 Dave Jones <davej@redhat.com>
- Back out part of the 2.4.23pre ACPI changes as per upstream.
- Fix up typo in orlov patch.
- Remove orlov patch for the time being.

* Tue Oct 28 2003 Dave Jones <davej@redhat.com>
- Fix aic7xxx/79xx causes PCI PARITY ERROR
- Disable SLAB_DEBUG & HIGHMEM_DEBUG

* Mon Oct 27 2003 Dave Jones <davej@redhat.com>
- DAC960 is unsupported.
- Force usage of gcc32 to build kernel.

* Mon Oct 27 2003 Michael K. Johnson <johnsonm@redhat.com>
- kernel-source requires gcc32

* Wed Oct 22 2003 Dave Jones <davej@redhat.com>
- More 2.4.23pre resyncs.
  - mprotect LSB compliance fix.
  - Longer i810 audio timeouts.
  - Various APIC fixes.
- Various fixes to the Maestro sound driver.
- paep and bcm5820 drivers building (Nalin Dahyabhai)
- Fix kon crash (#106432) (Al Viro)
- Reduce NFS UDP retransmits (Steve Dickson)
- Fix formatting of runqueue in /proc/cpuinfo (#107762)

* Tue Oct 21 2003 Dave Jones <davej@redhat.com>
- Exec-sheild update to -G4.
- Fix up d_move corner case (Al Viro)
- Firewire use-after-free bugfix (Alexandre Oliva)
- Add support for Treo600 to visor driver
- More 2.4.23pre resyncs.
  - Update TG3 driver
  - Unlink qdiscs in qdisc_destroy even when CONFIG_NET_SCHED is not enabled.
- Consolidate some of the ACPI patches so we can see whats upstream
  and what isn't a little easier.

* Sun Oct 17 2003 Dave Jones <davej@redhat.com>
- Disable DRM for S3/VIA.

* Fri Oct 15 2003 Dave Jones <davej@redhat.com>
- Drop kupdated 'fix' that broke things with nptl.
- Fix up unresolved symbols.

* Thu Oct 14 2003 Dave Jones <davej@redhat.com>
- More merges from 2.4.23pre
  - Update ACPI CA to 20031002
  - Various network driver updates.

* Tue Oct 13 2003 Dave Jones <davej@redhat.com>
- Back out previous unnecessary commit.
- More changes from 2.4.23pre
  - Fix ACPI hang with sysrq & o
  - Reenable laptopmode (but not AAM).
  - Fix pci_generic_prep_mwi export breakage
  - Small ACPI tweaks
  - attach_mnt fix
  - fix 2.4.x incorrect argv[0] for init
  - kupdated: correctly dequeue SIGSTOP signals
  - Assorted VM fixes.
    - PageReserved memory counting fix
	- page->flags corruption fix
	- Remove racy optimization from exec_mmap()
  - Fix quota counter overflow
  - Fix do_proc_readlink failpath
  - cciss update: support new controller
  - Numerous networking updates.
  - Fix potential PPPoE oops
  - Clear all flags in exec_usermodehelper
  - Clear IRQ_INPROGRESS in setup_irq()
  - Numerous NetFilter fixes/improvements.

* Fri Oct 10 2003 Dave Jones <davej@redhat.com>
- Only enable DMA on hard disks in the BOOT kernel.

* Fri Oct 3 2003 Dave Jones <davej@redhat.com>
- Disable laptop_mode
- Remove scsi add/remove hack

* Wed Oct 1 2003 Dave Jones <davej@redhat.com>
- Include hack to improve firewire hotplug.
- More merges from upstream 2.4.23pre
  - deal with lack of acpi prt entries gracefully
  - [IPV4]: In arp_rcv() do not inspect ARP header until packet length and linearity is verified.
  - [NET]: Fix HW_FLOWCONTROL on SMP.
  - Update various ACPI patches to follow mainline.

* Tue Sep 30 2003 Dave Jones <davej@redhat.com>
- Remove broken cdrom blocksize patch.
- More random ACPI fixes.
- b44 lockup fix.
- config file cleanups

* Mon Sep 29 2003 Dave Jones <davej@redhat.com>
- Fix up some exec-sheild corner cases. (-G3)

* Sun Sep 27 2003 Dave Jones <davej@redhat.com>
- Don't execute ACPI code twice when reading battery. (Upstream bugzilla #726)
- hpt372n fixes (#97824)

* Fri Sep 26 2003 Michael K. Johnson <johnsonm@redhat.com>
- Incorporate Ingo's fix for scheduler bug on SMP (#104922/#104958)

* Thu Sep 24 2003 Dave Jones <davej@redhat.com>
- Bump initrd size to 8192 (Jeremy Katz)
- More small fixes from 2.4.23pre
  - IDE CD capacity bugfix
  - revert ACPI EC timeout changes.
  - Fix ieee1394 smp hang with kudzu
  - Add missing Intel cachesize descriptors.
  - Fix igmp route leak
  - Bump tx_queue_len to 1000
  - Fix race between modifying entry->vccs and clip_start_xmit()
  - Fix ide-scsi initialization lockup
  - acpi_pci_link_get_irq return fix.
  - ACPI was using __initdata after free.
  - Fix SCI storm on out of spec boards like Tyan
- Remove inode cpu usage patch.
- Fix up memory leaks in error paths of LVM.
- Fix cdrom blocksize reset bug.
- process migration fix for SMP kernel on UP.
- Back out broken i82092 diff in -ac

* Tue Sep 23 2003 Dave Jones <davej@redhat.com>
- Change Toshiba/ASUS ACPI extras to be modules.

* Mon Sep 22 2003 Dave Jones <davej@redhat.com>
- Fix pci_pool_create redefinition in atm/he driver.
- Fix up missing includes in cpufreq

* Sun Sep 21 2003 Dave Jones <davej@redhat.com>
- Fix up CPUFREQ .config options.

* Sat Sep 20 2003 Dave Jones <davej@redhat.com>
- Backport various cpufreq fixes from 2.6.
  Longhaul & Powernow-k7 should work correctly now.
- Include several ACPI fixes from 2.4.23pre5
- Process scheduler update.

* Fri Sep 19 2003 Dave Jones <davej@redhat.com>
- Disable shared runqueue for Athlon kernels.

* Thu Sep 18 2003 Dave Jones <davej@redhat.com>
- More bits from 2.4.23pre
  - Handle get_block errors correctly in block_read_full_page()
  - fix possible busywait in rtc_read()
  - Avoid potentially leaking pagetables into the per-cpu queues
  - tty oops fix
  - Fix possible IRQ handling SMP race
  - Fix circular dependancy in include files
  - ISDN Config.in fix
  - Fix copy_namespace() usage in fork()
  - inodes_stat.nr_inodes race fix
  - update Memory-Write-Invalidate (MWI) transaction support

* Wed Sep 17 2003 Dave Jones <davej@redhat.com>
- ACPI IRQ routing fixes.
- ide-scsi was using an uninitialised variable.

* Tue Sep 16 2003 Dave Jones <davej@redhat.com>
- Merge important bits from 2.4.23pre
  - Repair mpparse for older dual pentium systems.
  - Add locking to SMI handling in io_apic code.
  - Add missing VIA IRQ router.

* Mon Sep 15 2003 Dave Jones <davej@redhat.com>
- Exec-shield -G2
- Various NFS fixes.
  - add a sysctl interface which allow the setting of the ports lockd listens
    on. Very useful when dealing with firewalls.
  - Clean up blocked locks when NFS server brought down
  - Fixes the access cache which was seriously broken.
    It decrease over nfs traffic by 2% when running the connectathon test suite.
  - detect flowcontrol conditions for TCP mounts
  - Fix an oops with TCP NFS mounts.

* Sun Sep 14 2003 Dave Jones <davej@redhat.com>
- Exec-shield -E2.
  VM_MAYEXEC still needs to be set for the stack,
  so that mprotect() can be done (if needed, later on).

* Fri Sep 12 2003 Dave Jones <davej@redhat.com>
- exec-shield E1. Should fix the DRI problems.

* Thu Sep 11 2003 Dave Jones <davej@redhat.com>
- aic7xxx/aic79xx configuration changes.

* Wed Sep 10 2003 Dave Jones <davej@redhat.com>
- Enable MTD/JFFS

* Tue Sep 9 2003 Dave Jones <davej@redhat.com>
- Fix multiple defintion of CONFIG_SHARE_RUNQUEUE
- futex fix from Ingo.
- libata updates.

* Mon Sep 08 2003 Michael K. Johnson <johnsonm@redhat.com>
- Get rid of -unsupported packages but still distinguish modules that
  we consider second tier

* Mon Sep 8 2003 Dave Jones <davej@redhat.com>
- Execsheild updates to -D4 (Fixes TuxRacer problems among others).
- Add syscall table entry for sys_lookup_dcookie.
- Add IDE DMA & i810 audio support for Intel ICH ESB chipset.
- Fix unix socket descriptor leak.
- Disable VBLANK driver.
- update SCSI whitelist.
- Enable building of libata, and fix up locking for nptl
- gcc3 compile fixes.
- Default acpi to off. Enable with acpi=on

* Fri Sep 5 2003 Dave Jones <davej@redhat.com>
- Execshield updates to -B5

* Thu Sep 4 2003 Dave Jones <davej@redhat,com>
- Fix for EXPORT_NO_SYMBOLS
- Merge oprofile patches.
- Removed unnecessary .20. from revision number.

* Wed Sep 3 2003 Dave Jones <davej@redhat.com>
- Exec shield fixes B3

* Tue Sep 2 2003 Dave Jones <davej@redhat.com>
- Update exec-sheild to A6, fixing bug which prevented dynamic binaries
  from being randomized properly.
- Add HT scheduler patch.

* Mon Sep 1 2003 Dave Jones <davej@redhat.com>
- Merge in Ingo's resynced Exec-shield and nptl patches.
- Fix up non UTF-8 characters in visor module.

* Fri Aug 29 2003 Dave Jones <davej@redhat.com>
- Rebase to 2.4.22-ac1

* Wed Aug 27 2003 Dave Jones <davej@redhat.com>
- Add missing hunk of VFSlock patch.

* Tue Aug 19 2003 Dave Jones <davej@redhat.com>
- Chasing build failures. (NPTL)

* Mon Aug 18 2003 Dave Jones <davej@redhat.com>
- Rebase against 2.4.22rc2-ac3
- Reenable LVM patches.

* Wed Aug 13 2003 Dave Jones <davej@redhat.com>
- Increase number of mountable NCPFS volumes. Needed for clustered volumes.
- Remove Dell MegaRAC driver on their request.
- x86_64 interrupt gate security fix.
- Fix signal handling race condition causing reboot hangs. (Ernie Petrides)

* Fri Aug 08 2003 Dave Jones <davej@redhat.com>
- Avoid annoying "Can't emulate rawmode" msgs with logitech cordless mice

* Tue Jun 10 2003 Bill Nottingham <notting@redhat.com>
- Serial ATA support (Jeff Garzik)
- ipsec updates (David Miller)

* Thu Jun 05 2003 Michael K. Johnson <johnsonm@redhat.com>
- add missing braces in megaraid

* Wed May 28 2003 Arjan van de Ven <arjanv@redhat.com>
- implement "echo off > /proc/modules" to disable module loading

* Tue May 27 2003 Arjan van de Ven <arjanv@redhat.com>
- add battery driver for Dell Inspiron 2600 laptops
- make /dev/mem be need-to-know only

* Sat May 24 2003 Arjan van de Ven <arjanv@redhat.com>
- fix the cpu_sibbling_map to be properly filled on HT machines

* Thu May 22 2003 Arjan van de Ven <arjanv@redhat.com>
- fix the lowlatecy patchkit
- fix make rpm

* Tue May 20 2003 Arjan van de Ven <arjanv@redhat.com>
- add Andrew Morton's lowlatency patchkit
 
* Mon May 19 2003 Arjan van de Ven <arjanv@redhat.com>
- update cpufreq codebase to latest

* Fri May 16 2003 Arjan van de Ven <arjanv@redhat.com>
- experimental acpitimer code 

* Thu May 15 2003 Arjan van de Ven <arjanv@redhat.com>
- add ipsec backport from DaveM

* Wed May 14 2003 Arjan van de Ven <arjanv@redhat.com>
- update exec-shield patch to latest
- add Jens Axboe's laptop patches

* Mon May 5 2003 Arjan van de Ven <arjanv@redhat.com>
- exec-shield
- merge upto 2.4.21-rc1-ac4

* Fri Apr 25 2003 Arjan van de Ven <arjanv@redhat.com>
- add NPTL patch 

* Thu Apr 24 2003 Arjan van de Ven <arjanv@redhat.com>
- update to 2.4.21-rc1-ac1

* Wed Apr 16 2003 Arjan van de Ven <arjanv@redhat.com>
- update to 2.4.21-pre7-ac1

* Mon Apr 14 2003 Arjan van de Ven <arjanv@redhat.com>
- add ext3 updates

* Fri Mar 21 2003 Steve Dickson <SteveD@redhat.com>
- Added three NFS performance patches

* Fri Mar 21 2003 Arjan van de Ven <arjanv@redhat.com>
- add a timer for aging requests up

* Thu Mar 20 2003 Arjan van de Ven <arjanv@redhat.com>
- fix the sorting in the io elevator

* Wed Mar 19 2003 Arjan van de Ven <arjanv@redhat.com>
- remerge ext3 orlov allocator and enable
- more priority io elevator infrastructure work

* Sat Mar 08 2003 Arjan van de Ven <arjanv@redhat.com>
- merge ext3 orlov allocator

* Thu Mar 04 2003 Arjan van de Ven <arjanv@redhat.com>
- go to 2.4.21-pre5-ac1
- split the rpm into a primary fileset and secondary (rare modules) fileset

* Wed Feb 26 2003 Arjan van de Ven <arjanv@redhat.com>
- go to 2.4.21-pre4-ac6
- drop lots of patches
- trim changelog to not have 2000, 2001 and 2002 anymore


* Mon Feb 17 2003 Arjan van de Ven <arjanv@redhat.com>
- downgrade pwc webcam driver one notch to the last known working version
- work around vm livelock
- make the cciss driver not enable irq's in it's irq handler

* Thu Feb 13 2003 Benjamin LaHaise <bcrl@redhat.com>
- fix for shared statistic < 0

* Thu Feb 13 2003 Arjan van de Ven <arjanv@redhat.com>
- Signal fixes for LSB testsuite
- merge patch to fix init=/bin/sh
- 2.4.21-pre4-ac4 bugfixes (mostly IDE crashes)

* Mon Feb 09 2003 Arjan van de Ven <arjanv@redhat.com>
- merge 2.4.21-pre4-ac3 bugfixes 

* Tue Feb  4 2003 Stephen C. Tweedie <sct@redhat.com>
- Add ext3 fix for truncating negative i_blocks

* Mon Feb 03 2003 Benjamin LaHaise <bcrl@redhat.com>
- add oopsmeharder for debugging hard hangs

* Mon Feb 03 2003 Arjan van de Ven <arjanv@redhat.com>
- merge rmap15d
- set the sbp2 command buffer to 64 elements instead of 8

* Wed Jan 29 2003 Arjan van de Ven <arjanv@redhat.com>
- merge tcp stall fix from DaveM
- merge bugfixes from 2.4.21-pre4

* Mon Jan 13 2003 Steve Dickson <SteveD@RedHat.com>
- Merge in the ACL and XATTR patches from the UL and BestBits trees.

* Sat Jan 11 2003 Stephen C. Tweedie <sct@redhat.com>
- Merge in the LVM pvmove fix to current tree

* Wed Jan 8 2003 Benjamin LaHaise <bcrl@redhat.com>
- add AMD Golem fixup

