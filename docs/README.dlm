Howto Build DLM with CI

The OpenDLM code has been adapted to work with CI. It can be obtained from:
	http://sourceforge.net/projects/opendlm

For questions/comments/bugfixes, please send email to:
	ci-linux-devel@lists.sourceforge.net
		or
	aneesh.kumar@digital.com 
